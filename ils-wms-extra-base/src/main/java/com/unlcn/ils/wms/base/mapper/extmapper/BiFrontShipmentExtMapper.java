package com.unlcn.ils.wms.base.mapper.extmapper;

import com.unlcn.ils.wms.base.model.bigscreen.BiFrontShipment;
import com.unlcn.ils.wms.base.model.bigscreen.BiFrontShipmentExample;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface BiFrontShipmentExtMapper {
    /**
     * 前端发运推移图统计
     * @return
     */
    List<BiFrontShipment> shipmentChartCount();
}