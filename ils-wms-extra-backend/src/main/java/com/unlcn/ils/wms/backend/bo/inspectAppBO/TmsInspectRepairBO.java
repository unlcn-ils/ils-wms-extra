package com.unlcn.ils.wms.backend.bo.inspectAppBO;

/**
 * @Author ：Ligl
 * @Date : 2017/9/15.
 */
public class TmsInspectRepairBO {

    private Long id ;        //主键
    private Long inspectId;        //验车单ID
    private String repairCode;   //返修单编码
    private Long repairTimeId;     //返修时间ID
    private Integer status;      //状态
    private String gmtCreate;   //创建时间

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getInspectId() {
        return inspectId;
    }

    public void setInspectId(Long inspectId) {
        this.inspectId = inspectId;
    }

    public String getRepairCode() {
        return repairCode;
    }

    public void setRepairCode(String repairCode) {
        this.repairCode = repairCode;
    }

    public Long getRepairTimeId() {
        return repairTimeId;
    }

    public void setRepairTimeId(Long repairTimeId) {
        this.repairTimeId = repairTimeId;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getGmtCreate() {
        return gmtCreate;
    }

    public void setGmtCreate(String gmtCreate) {
        this.gmtCreate = gmtCreate;
    }

    @Override
    public String toString() {
        return "TmsInspectRepairBO{" +
                "id=" + id +
                ", inspectId=" + inspectId +
                ", repairCode='" + repairCode + '\'' +
                ", repairTimeId=" + repairTimeId +
                ", status=" + status +
                ", gmtCreate='" + gmtCreate + '\'' +
                '}';
    }
}
