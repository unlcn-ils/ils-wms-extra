package com.unlcn.ils.wms.base.mapper.extmapper;

import com.unlcn.ils.wms.base.model.stock.WmsVehicleTracking;
import com.unlcn.ils.wms.base.model.stock.WmsVehicleTrackingWithBLOBs;

import java.util.HashMap;
import java.util.List;

public interface WmsVehicleTrackingExtMapper {

    List<WmsVehicleTrackingWithBLOBs> selectByParam(HashMap<String, Object> paramTracking);

    Integer selectCountByParam(HashMap<String, Object> paramTracking);

    List<WmsVehicleTrackingWithBLOBs> selectAllRecords(HashMap<String, Object> paramTracking);

    void insertOrUpdate(WmsVehicleTracking tracking);
}
