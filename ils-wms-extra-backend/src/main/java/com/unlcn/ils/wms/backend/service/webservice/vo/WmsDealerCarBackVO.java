package com.unlcn.ils.wms.backend.service.webservice.vo;

/**
 * 经销商退车
 * Created by DELL on 2017/10/11.
 */
public class WmsDealerCarBackVO {

    /**
     * 退车申请单号
     */
    private String cbApplyNo;

    /**
     * 经销商代码
     */
    private String cbDealerCode;

    /**
     * 经销商名称
     */
    private String cbDealerName;

    /**
     * 物料代码
     */
    private String cbMaterialCode;

    /**
     * 物料名称
     */
    private String cbMaterialName;

    /**
     * 车辆识别码VIN
     */
    private String cbVin;

    /**
     * 退车仓库CODE
     */
    private String cbWhName;

    /**
     * 退车时间
     */
    private String cbCarBackTime;

    /**
     * 行号
     */
    private String cbLineId;

    public String getCbApplyNo() {
        return cbApplyNo;
    }

    public void setCbApplyNo(String cbApplyNo) {
        this.cbApplyNo = cbApplyNo;
    }

    public String getCbDealerCode() {
        return cbDealerCode;
    }

    public void setCbDealerCode(String cbDealerCode) {
        this.cbDealerCode = cbDealerCode;
    }

    public String getCbDealerName() {
        return cbDealerName;
    }

    public void setCbDealerName(String cbDealerName) {
        this.cbDealerName = cbDealerName;
    }

    public String getCbMaterialCode() {
        return cbMaterialCode;
    }

    public void setCbMaterialCode(String cbMaterialCode) {
        this.cbMaterialCode = cbMaterialCode;
    }

    public String getCbMaterialName() {
        return cbMaterialName;
    }

    public void setCbMaterialName(String cbMaterialName) {
        this.cbMaterialName = cbMaterialName;
    }

    public String getCbVin() {
        return cbVin;
    }

    public void setCbVin(String cbVin) {
        this.cbVin = cbVin;
    }

    public String getCbWhName() {
        return cbWhName;
    }

    public void setCbWhName(String cbWhName) {
        this.cbWhName = cbWhName;
    }

    public String getCbCarBackTime() {
        return cbCarBackTime;
    }

    public void setCbCarBackTime(String cbCarBackTime) {
        this.cbCarBackTime = cbCarBackTime;
    }

    public String getCbLineId() {
        return cbLineId;
    }

    public void setCbLineId(String cbLineId) {
        this.cbLineId = cbLineId;
    }

    @Override
    public String toString() {
        return "WmsDealerCarBackBO{" +
                ", cbApplyNo='" + cbApplyNo + '\'' +
                ", cbDealerCode='" + cbDealerCode + '\'' +
                ", cbDealerName='" + cbDealerName + '\'' +
                ", cbMaterialCode='" + cbMaterialCode + '\'' +
                ", cbMaterialName='" + cbMaterialName + '\'' +
                ", cbVin='" + cbVin + '\'' +
                ", cbWhName='" + cbWhName + '\'' +
                ", cbCarBackTime=" + cbCarBackTime +
                ", cbLineId='" + cbLineId + '\'' +
                '}';
    }
}
