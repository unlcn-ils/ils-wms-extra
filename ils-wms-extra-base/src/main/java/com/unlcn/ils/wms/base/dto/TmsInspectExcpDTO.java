package com.unlcn.ils.wms.base.dto;

import java.io.Serializable;
import java.util.Date;

public class TmsInspectExcpDTO implements Serializable {

    private Long excpId;
    private String constantBodyName;
    private String constantHurtName;
    private Integer constantBodyCode;
    private Integer constantHurtCode;
    private Integer positionId;
    private String constantPositionName;
    private Integer constantPositionSort;
    private Long inspectId;
    private Integer countExcp;
    private String picKey;
    private String picUrl;
    private Integer inspectStatus;
    private String vin;
    private Date excpTime;

    public Date getExcpTime() {
        return excpTime;
    }

    public void setExcpTime(Date excpTime) {
        this.excpTime = excpTime;
    }

    public String getVin() {
        return vin;
    }

    public void setVin(String vin) {
        this.vin = vin;
    }

    public Integer getInspectStatus() {
        return inspectStatus;
    }

    public void setInspectStatus(Integer inspectStatus) {
        this.inspectStatus = inspectStatus;
    }

    public String getPicKey() {
        return picKey;
    }

    public void setPicKey(String picKey) {
        this.picKey = picKey;
    }

    public String getPicUrl() {
        return picUrl;
    }

    public void setPicUrl(String picUrl) {
        this.picUrl = picUrl;
    }

    public Long getExcpId() {
        return excpId;
    }

    public void setExcpId(Long excpId) {
        this.excpId = excpId;
    }

    public Integer getCountExcp() {
        return countExcp;
    }

    public void setCountExcp(Integer countExcp) {
        this.countExcp = countExcp;
    }

    public String getConstantPositionName() {
        return constantPositionName;
    }

    public void setConstantPositionName(String constantPositionName) {
        this.constantPositionName = constantPositionName;
    }

    public Integer getConstantPositionSort() {
        return constantPositionSort;
    }

    public void setConstantPositionSort(Integer constantPositionSort) {
        this.constantPositionSort = constantPositionSort;
    }

    public String getConstantBodyName() {
        return constantBodyName;
    }

    public void setConstantBodyName(String constantBodyName) {
        this.constantBodyName = constantBodyName;
    }

    public String getConstantHurtName() {
        return constantHurtName;
    }

    public void setConstantHurtName(String constantHurtName) {
        this.constantHurtName = constantHurtName;
    }

    public Integer getConstantBodyCode() {
        return constantBodyCode;
    }

    public void setConstantBodyCode(Integer constantBodyCode) {
        this.constantBodyCode = constantBodyCode;
    }

    public Integer getConstantHurtCode() {
        return constantHurtCode;
    }

    public void setConstantHurtCode(Integer constantHurtCode) {
        this.constantHurtCode = constantHurtCode;
    }

    public Integer getPositionId() {
        return positionId;
    }

    public void setPositionId(Integer positionId) {
        this.positionId = positionId;
    }

    public Long getInspectId() {
        return inspectId;
    }

    public void setInspectId(Long inspectId) {
        this.inspectId = inspectId;
    }
}
