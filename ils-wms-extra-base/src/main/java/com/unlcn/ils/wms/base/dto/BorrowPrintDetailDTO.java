package com.unlcn.ils.wms.base.dto;

import com.unlcn.ils.wms.base.model.stock.WmsBorrowCarDetail;

import java.util.Date;
import java.util.List;

/**
 * Created by lenovo on 2017/10/20.
 */
public class BorrowPrintDetailDTO {
    // 借车单id
    private Long bdId;

    // 借车单id
    private Long bcId;

    // 借车单号
    private String bcBorrowNo;

    // 借用人姓名
    private String bcBorrowName;

    // 借用部门
    private String bcBorrowDepartment;

    // 联系电话
    private String bcBorrowTel;

    // 借用日期
    private Date bcBorrowDate;

    // 格式化借用日期
    private String borrowDate;

    // 预计还车日期
    private Date bcEstimatedReturnDate;

    // 借车原因
    private String bcBorrowReason;

    // 借车单详情
    List<BorrowCarDetailDTO> borrowCarDetailList;

    // 条形码
    private String base64Img;

    public Long getBdId() {
        return bdId;
    }

    public void setBdId(Long bdId) {
        this.bdId = bdId;
    }

    public Long getBcId() {
        return bcId;
    }

    public void setBcId(Long bcId) {
        this.bcId = bcId;
    }

    public String getBcBorrowNo() {
        return bcBorrowNo;
    }

    public void setBcBorrowNo(String bcBorrowNo) {
        this.bcBorrowNo = bcBorrowNo;
    }

    public String getBcBorrowName() {
        return bcBorrowName;
    }

    public void setBcBorrowName(String bcBorrowName) {
        this.bcBorrowName = bcBorrowName;
    }

    public String getBcBorrowDepartment() {
        return bcBorrowDepartment;
    }

    public void setBcBorrowDepartment(String bcBorrowDepartment) {
        this.bcBorrowDepartment = bcBorrowDepartment;
    }

    public String getBcBorrowTel() {
        return bcBorrowTel;
    }

    public void setBcBorrowTel(String bcBorrowTel) {
        this.bcBorrowTel = bcBorrowTel;
    }

    public Date getBcBorrowDate() {
        return bcBorrowDate;
    }

    public void setBcBorrowDate(Date bcBorrowDate) {
        this.bcBorrowDate = bcBorrowDate;
    }

    public String getBorrowDate() {
        return borrowDate;
    }

    public void setBorrowDate(String borrowDate) {
        this.borrowDate = borrowDate;
    }

    public Date getBcEstimatedReturnDate() {
        return bcEstimatedReturnDate;
    }

    public void setBcEstimatedReturnDate(Date bcEstimatedReturnDate) {
        this.bcEstimatedReturnDate = bcEstimatedReturnDate;
    }

    public String getBcBorrowReason() {
        return bcBorrowReason;
    }

    public void setBcBorrowReason(String bcBorrowReason) {
        this.bcBorrowReason = bcBorrowReason;
    }

    public List<BorrowCarDetailDTO> getBorrowCarDetailList() {
        return borrowCarDetailList;
    }

    public void setBorrowCarDetailList(List<BorrowCarDetailDTO> borrowCarDetailList) {
        this.borrowCarDetailList = borrowCarDetailList;
    }

    public String getBase64Img() {
        return base64Img;
    }

    public void setBase64Img(String base64Img) {
        this.base64Img = base64Img;
    }

    @Override
    public String toString() {
        return "BorrowPrintDetailDTO{" +
                "bdId=" + bdId +
                ", bcId=" + bcId +
                ", bcBorrowNo='" + bcBorrowNo + '\'' +
                ", bcBorrowName='" + bcBorrowName + '\'' +
                ", bcBorrowDepartment='" + bcBorrowDepartment + '\'' +
                ", bcBorrowTel='" + bcBorrowTel + '\'' +
                ", bcBorrowDate=" + bcBorrowDate +
                ", borrowDate='" + borrowDate + '\'' +
                ", bcEstimatedReturnDate=" + bcEstimatedReturnDate +
                ", bcBorrowReason='" + bcBorrowReason + '\'' +
                ", borrowCarDetailList=" + borrowCarDetailList +
                ", base64Img='" + base64Img + '\'' +
                '}';
    }
}
