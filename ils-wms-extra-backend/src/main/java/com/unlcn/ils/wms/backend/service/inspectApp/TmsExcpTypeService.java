package com.unlcn.ils.wms.backend.service.inspectApp;


import com.unlcn.ils.wms.backend.bo.inspectAppBO.TmsExcpBO;

import java.util.List;

/**
 * Created by houjianhui on 2017/7/24.
 */
public interface TmsExcpTypeService {
    /**
     * 查询异常信息
     *
     * @param id 异常ID
     * @return
     * @throws Exception
     */
    List<TmsExcpBO> listTmsExcpBOByPId(Long id) throws Exception;
}
