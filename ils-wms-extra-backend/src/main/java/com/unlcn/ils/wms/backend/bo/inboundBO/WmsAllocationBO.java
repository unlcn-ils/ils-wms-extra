package com.unlcn.ils.wms.backend.bo.inboundBO;

/**
 * Created by DELL on 2017/8/15.
 */
public class WmsAllocationBO {
    /**
     * 订单ID
     */
    private String alOdId;
    /**
     * 货主ID
     */
    private String alCustomerId;

    /**
     * 货主CODE
     */
    private String alCustomerCode;

    /**
     * 运单号
     */
    private String alWaybillNo ="";

    /**
     * 车型CODE
     */
    private String alVehicleSpecCode;

    /**
     * 车型名称
     */
    private String alVehicleSpecName;

    /**
     * 底盘号
     */
    private String alVin;

    /**
     * 库区ID
     */
    private String alTargetZoneId;

    /**
     * 库位ID
     */
    private String alTargetLocId;

    /**
     * 目的库区CODE
     */
    private String alTargetZoneCode;

    /**
     * 目的库位CODE
     */
    private String alTargetLocCode;

    /**
     * 目的库区
     */
    private String alTargetZoneName;

    /**
     * 目的库位
     */
    private String alTargetLocName;

    /**
     * 分配规则
     */
    private String stgType;



    public String getAlOdId() {
        return alOdId;
    }

    public void setAlOdId(String alOdId) {
        this.alOdId = alOdId;
    }

    public String getAlCustomerId() {
        return alCustomerId;
    }

    public void setAlCustomerId(String alCustomerId) {
        this.alCustomerId = alCustomerId;
    }

    public String getAlCustomerCode() {
        return alCustomerCode;
    }

    public void setAlCustomerCode(String alCustomerCode) {
        this.alCustomerCode = alCustomerCode;
    }

    public String getAlWaybillNo() {
        return alWaybillNo;
    }

    public void setAlWaybillNo(String alWaybillNo) {
        this.alWaybillNo = alWaybillNo;
    }

    public String getAlVehicleSpecCode() {
        return alVehicleSpecCode;
    }

    public void setAlVehicleSpecCode(String alVehicleSpecCode) {
        this.alVehicleSpecCode = alVehicleSpecCode;
    }

    public String getAlVehicleSpecName() {
        return alVehicleSpecName;
    }

    public void setAlVehicleSpecName(String alVehicleSpecName) {
        this.alVehicleSpecName = alVehicleSpecName;
    }

    public String getAlVin() {
        return alVin;
    }

    public void setAlVin(String alVin) {
        this.alVin = alVin;
    }

    public String getAlTargetZoneId() {
        return alTargetZoneId;
    }

    public void setAlTargetZoneId(String alTargetZoneId) {
        this.alTargetZoneId = alTargetZoneId;
    }

    public String getAlTargetLocId() {
        return alTargetLocId;
    }

    public void setAlTargetLocId(String alTargetLocId) {
        this.alTargetLocId = alTargetLocId;
    }

    public String getAlTargetZoneCode() {
        return alTargetZoneCode;
    }

    public void setAlTargetZoneCode(String alTargetZoneCode) {
        this.alTargetZoneCode = alTargetZoneCode;
    }

    public String getAlTargetLocCode() {
        return alTargetLocCode;
    }

    public void setAlTargetLocCode(String alTargetLocCode) {
        this.alTargetLocCode = alTargetLocCode;
    }

    public String getAlTargetZoneName() {
        return alTargetZoneName;
    }

    public void setAlTargetZoneName(String alTargetZoneName) {
        this.alTargetZoneName = alTargetZoneName;
    }

    public String getAlTargetLocName() {
        return alTargetLocName;
    }

    public void setAlTargetLocName(String alTargetLocName) {
        this.alTargetLocName = alTargetLocName;
    }

    public String getStgType() {
        return stgType;
    }

    public void setStgType(String stgType) {
        this.stgType = stgType;
    }

    @Override
    public String toString() {
        return "WmsAllocationVO{" +
                "alOdId='" + alOdId + '\'' +
                ", alCustomerId='" + alCustomerId + '\'' +
                ", alCustomerCode='" + alCustomerCode + '\'' +
                ", alWaybillNo='" + alWaybillNo + '\'' +
                ", alVehicleSpecCode='" + alVehicleSpecCode + '\'' +
                ", alVehicleSpecName='" + alVehicleSpecName + '\'' +
                ", alVin='" + alVin + '\'' +
                ", alTargetZoneId='" + alTargetZoneId + '\'' +
                ", alTargetLocId='" + alTargetLocId + '\'' +
                ", alTargetZoneCode='" + alTargetZoneCode + '\'' +
                ", alTargetLocCode='" + alTargetLocCode + '\'' +
                ", alTargetZoneName='" + alTargetZoneName + '\'' +
                ", alTargetLocName='" + alTargetLocName + '\'' +
                ", stgType='" + stgType + '\'' +
                '}';
    }
}
