package com.unlcn.ils.wms.web.vo.inbound;

/**
 * Created by DELL on 2017/9/13.
 */
public class WmsInboundAllocationVO {

    /**
     * 分配ID
     */
    private Long alId;

    /**
     * 分配code
     */
    private String alCode;

    /**
     * 分配类型
     */
    private String alType;

    /**
     * 分配状态
     */
    private String alStatus;

    /**
     * 仓库id
     */
    private String alWhId;

    /**
     * 订单id
     */
    private String alOdId;

    /**
     * 货主id
     */
    private String alCustomerId;

    /**
     * 货主code
     */
    private String alCustomerCode;

    /**
     * 货主name
     */
    private String alCustomerName;

    /**
     * 原库区id
     */
    private String alSourceZoneId;

    /**
     * 原库区code
     */
    private String alSourceZoneCode;

    /**
     * 原库区name
     */
    private String alSourceZoneName;

    /**
     * 目标库区
     */
    private String alTargetZoneId;

    /**
     * 目标库区code
     */
    private String alTargetZoneCode;

    /**
     * 目标库区name
     */
    private String alTargetZoneName;

    /**
     * 原库位id
     */
    private String alSourceLocId;

    /**
     * 原库位code
     */
    private String alSourceLocCode;

    /**
     * 原库位name
     */
    private String alSourceLocName;

    /**
     * 目标库位主键
     */
    private String alTargetLocId;

    /**
     * 目标库位code
     */
    private String alTargetLocCode;

    /**
     * 目标库位name
     */
    private String alTargetLocName;

    /**
     * 运单号
     */
    private String alWaybillNo;

    /**
     * 底盘号
     */
    private String alVin;

    /**
     * 车型code
     */
    private String alVehicleSpecCode;

    /**
     * 车型name
     */
    private String alVehicleSpecName;

    public Long getAlId() {
        return alId;
    }

    public void setAlId(Long alId) {
        this.alId = alId;
    }

    public String getAlCode() {
        return alCode;
    }

    public void setAlCode(String alCode) {
        this.alCode = alCode;
    }

    public String getAlType() {
        return alType;
    }

    public void setAlType(String alType) {
        this.alType = alType;
    }

    public String getAlStatus() {
        return alStatus;
    }

    public void setAlStatus(String alStatus) {
        this.alStatus = alStatus;
    }

    public String getAlWhId() {
        return alWhId;
    }

    public void setAlWhId(String alWhId) {
        this.alWhId = alWhId;
    }

    public String getAlOdId() {
        return alOdId;
    }

    public void setAlOdId(String alOdId) {
        this.alOdId = alOdId;
    }

    public String getAlCustomerId() {
        return alCustomerId;
    }

    public void setAlCustomerId(String alCustomerId) {
        this.alCustomerId = alCustomerId;
    }

    public String getAlCustomerCode() {
        return alCustomerCode;
    }

    public void setAlCustomerCode(String alCustomerCode) {
        this.alCustomerCode = alCustomerCode;
    }

    public String getAlCustomerName() {
        return alCustomerName;
    }

    public void setAlCustomerName(String alCustomerName) {
        this.alCustomerName = alCustomerName;
    }

    public String getAlSourceZoneId() {
        return alSourceZoneId;
    }

    public void setAlSourceZoneId(String alSourceZoneId) {
        this.alSourceZoneId = alSourceZoneId;
    }

    public String getAlSourceZoneCode() {
        return alSourceZoneCode;
    }

    public void setAlSourceZoneCode(String alSourceZoneCode) {
        this.alSourceZoneCode = alSourceZoneCode;
    }

    public String getAlSourceZoneName() {
        return alSourceZoneName;
    }

    public void setAlSourceZoneName(String alSourceZoneName) {
        this.alSourceZoneName = alSourceZoneName;
    }

    public String getAlTargetZoneId() {
        return alTargetZoneId;
    }

    public void setAlTargetZoneId(String alTargetZoneId) {
        this.alTargetZoneId = alTargetZoneId;
    }

    public String getAlTargetZoneCode() {
        return alTargetZoneCode;
    }

    public void setAlTargetZoneCode(String alTargetZoneCode) {
        this.alTargetZoneCode = alTargetZoneCode;
    }

    public String getAlTargetZoneName() {
        return alTargetZoneName;
    }

    public void setAlTargetZoneName(String alTargetZoneName) {
        this.alTargetZoneName = alTargetZoneName;
    }

    public String getAlSourceLocId() {
        return alSourceLocId;
    }

    public void setAlSourceLocId(String alSourceLocId) {
        this.alSourceLocId = alSourceLocId;
    }

    public String getAlSourceLocCode() {
        return alSourceLocCode;
    }

    public void setAlSourceLocCode(String alSourceLocCode) {
        this.alSourceLocCode = alSourceLocCode;
    }

    public String getAlSourceLocName() {
        return alSourceLocName;
    }

    public void setAlSourceLocName(String alSourceLocName) {
        this.alSourceLocName = alSourceLocName;
    }

    public String getAlTargetLocId() {
        return alTargetLocId;
    }

    public void setAlTargetLocId(String alTargetLocId) {
        this.alTargetLocId = alTargetLocId;
    }

    public String getAlTargetLocCode() {
        return alTargetLocCode;
    }

    public void setAlTargetLocCode(String alTargetLocCode) {
        this.alTargetLocCode = alTargetLocCode;
    }

    public String getAlTargetLocName() {
        return alTargetLocName;
    }

    public void setAlTargetLocName(String alTargetLocName) {
        this.alTargetLocName = alTargetLocName;
    }

    public String getAlWaybillNo() {
        return alWaybillNo;
    }

    public void setAlWaybillNo(String alWaybillNo) {
        this.alWaybillNo = alWaybillNo;
    }

    public String getAlVin() {
        return alVin;
    }

    public void setAlVin(String alVin) {
        this.alVin = alVin;
    }

    public String getAlVehicleSpecCode() {
        return alVehicleSpecCode;
    }

    public void setAlVehicleSpecCode(String alVehicleSpecCode) {
        this.alVehicleSpecCode = alVehicleSpecCode;
    }

    public String getAlVehicleSpecName() {
        return alVehicleSpecName;
    }

    public void setAlVehicleSpecName(String alVehicleSpecName) {
        this.alVehicleSpecName = alVehicleSpecName;
    }

    @Override
    public String toString() {
        return "WmsInboundAllocationVO{" +
                "alId=" + alId +
                ", alCode='" + alCode + '\'' +
                ", alType='" + alType + '\'' +
                ", alStatus='" + alStatus + '\'' +
                ", alWhId='" + alWhId + '\'' +
                ", alOdId='" + alOdId + '\'' +
                ", alCustomerId='" + alCustomerId + '\'' +
                ", alCustomerCode='" + alCustomerCode + '\'' +
                ", alCustomerName='" + alCustomerName + '\'' +
                ", alSourceZoneId='" + alSourceZoneId + '\'' +
                ", alSourceZoneCode='" + alSourceZoneCode + '\'' +
                ", alSourceZoneName='" + alSourceZoneName + '\'' +
                ", alTargetZoneId='" + alTargetZoneId + '\'' +
                ", alTargetZoneCode='" + alTargetZoneCode + '\'' +
                ", alTargetZoneName='" + alTargetZoneName + '\'' +
                ", alSourceLocId='" + alSourceLocId + '\'' +
                ", alSourceLocCode='" + alSourceLocCode + '\'' +
                ", alSourceLocName='" + alSourceLocName + '\'' +
                ", alTargetLocId='" + alTargetLocId + '\'' +
                ", alTargetLocCode='" + alTargetLocCode + '\'' +
                ", alTargetLocName='" + alTargetLocName + '\'' +
                ", alWaybillNo='" + alWaybillNo + '\'' +
                ", alVin='" + alVin + '\'' +
                ", alVehicleSpecCode='" + alVehicleSpecCode + '\'' +
                ", alVehicleSpecName='" + alVehicleSpecName + '\'' +
                '}';
    }
}
