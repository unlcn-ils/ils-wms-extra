package com.unlcn.ils.wms.backend.dto.inboundDTO;

import java.util.Date;

/**
 * Created by DELL on 2017/8/18.
 */
public class WmsInboundRepairDTO {

    /**
     * 维修单ID
     */
    private Long rpId;

    /**
     * 订单ID
     */
    private String rpOdId;

    /**
     * 货主CODE
     */
    private String rpCustomerNo;

    /**
     * 货主名
     */
    private String rpCustomerName;

    /**
     * 调度单号
     */
    private String rpDispatchNo;

    /**
     * 运单号
     */
    private String rpWaybillNo;

    /**
     * 维修出库单号
     */
    private String rpOutbillNo;

    /**
     * 仓库ID
     */
    private String rpWhId;

    /**
     * 车型code
     */
    private String rpVehicleSpecCode;

    /**
     * 车型名称
     */
    private String rpVehicleSpecName;

    /**
     * 车型描述
     */
    private String rpVehicleSpecDesc;

    /**
     * 底盘号
     */
    private String rpVin;

    /**
     * 发动机
     */
    private String rpEngine;

    /**
     * 维修类型
     */
    private String rpType;

    /**
     * 维修状态
     */
    private String rpStatus;

    /**
     * 质损单号
     */
    private String rpZsNo;

    /**
     * 维修单号
     */
    private String rpRepairNo;

    /**
     * 目的地
     */
    private String rpDest;

    /**
     * 货物说明
     */
    private String rpGoodsExplain;

    /**
     * 检验结果
     */
    private String rpCheckResult;

    /**
     * 车辆问题描述
     */
    private String rpProblemDescribe;

    /**
     * 责任方
     */
    private String rpResponsibleParty;

    /**
     * 维修方
     */
    private String rpRepairParty;

    /**
     * 维修结果
     */
    private String rpRepairResult;

    /**
     * 维修地址
     */
    private String rpAddress;

    /**
     * 维修开始日期
     */
    private String rpStartDate;

    /**
     * 维修结束日期
     */
    private String rpEndDate;

    /**
     * 维修厂
     */
    private String rpPlant;

    /**
     * 备注
     */
    private String remark;

    /**
     * 创建人用户id
     */
    private String createUserId;

    /**
     * 创建人用户名
     */
    private String createUserName;

    /**
     * 修改人用户id
     */
    private String modifyUserId;

    /**
     * 修改人用户名
     */
    private String modifyUserName;

    /**
     * 创建时间
     */
    private Date gmtCreate;

    /**
     * 修改时间
     */
    private Date gmtUpdate;

    /**
     * 逻辑删除
     */
    private Byte isDeleted;

    /**
     * 版本号
     */
    private Long versions;

    public Long getRpId() {
        return rpId;
    }

    public void setRpId(Long rpId) {
        this.rpId = rpId;
    }

    public String getRpOdId() {
        return rpOdId;
    }

    public void setRpOdId(String rpOdId) {
        this.rpOdId = rpOdId;
    }

    public String getRpCustomerNo() {
        return rpCustomerNo;
    }

    public void setRpCustomerNo(String rpCustomerNo) {
        this.rpCustomerNo = rpCustomerNo;
    }

    public String getRpCustomerName() {
        return rpCustomerName;
    }

    public void setRpCustomerName(String rpCustomerName) {
        this.rpCustomerName = rpCustomerName;
    }

    public String getRpDispatchNo() {
        return rpDispatchNo;
    }

    public void setRpDispatchNo(String rpDispatchNo) {
        this.rpDispatchNo = rpDispatchNo;
    }

    public String getRpWaybillNo() {
        return rpWaybillNo;
    }

    public void setRpWaybillNo(String rpWaybillNo) {
        this.rpWaybillNo = rpWaybillNo;
    }

    public String getRpOutbillNo() {
        return rpOutbillNo;
    }

    public void setRpOutbillNo(String rpOutbillNo) {
        this.rpOutbillNo = rpOutbillNo;
    }

    public String getRpWhId() {
        return rpWhId;
    }

    public void setRpWhId(String rpWhId) {
        this.rpWhId = rpWhId;
    }

    public String getRpVehicleSpecCode() {
        return rpVehicleSpecCode;
    }

    public void setRpVehicleSpecCode(String rpVehicleSpecCode) {
        this.rpVehicleSpecCode = rpVehicleSpecCode;
    }

    public String getRpVehicleSpecName() {
        return rpVehicleSpecName;
    }

    public void setRpVehicleSpecName(String rpVehicleSpecName) {
        this.rpVehicleSpecName = rpVehicleSpecName;
    }

    public String getRpVehicleSpecDesc() {
        return rpVehicleSpecDesc;
    }

    public void setRpVehicleSpecDesc(String rpVehicleSpecDesc) {
        this.rpVehicleSpecDesc = rpVehicleSpecDesc;
    }

    public String getRpVin() {
        return rpVin;
    }

    public void setRpVin(String rpVin) {
        this.rpVin = rpVin;
    }

    public String getRpEngine() {
        return rpEngine;
    }

    public void setRpEngine(String rpEngine) {
        this.rpEngine = rpEngine;
    }

    public String getRpType() {
        return rpType;
    }

    public void setRpType(String rpType) {
        this.rpType = rpType;
    }

    public String getRpStatus() {
        return rpStatus;
    }

    public void setRpStatus(String rpStatus) {
        this.rpStatus = rpStatus;
    }

    public String getRpZsNo() {
        return rpZsNo;
    }

    public void setRpZsNo(String rpZsNo) {
        this.rpZsNo = rpZsNo;
    }

    public String getRpRepairNo() {
        return rpRepairNo;
    }

    public void setRpRepairNo(String rpRepairNo) {
        this.rpRepairNo = rpRepairNo;
    }

    public String getRpDest() {
        return rpDest;
    }

    public void setRpDest(String rpDest) {
        this.rpDest = rpDest;
    }

    public String getRpGoodsExplain() {
        return rpGoodsExplain;
    }

    public void setRpGoodsExplain(String rpGoodsExplain) {
        this.rpGoodsExplain = rpGoodsExplain;
    }

    public String getRpCheckResult() {
        return rpCheckResult;
    }

    public void setRpCheckResult(String rpCheckResult) {
        this.rpCheckResult = rpCheckResult;
    }

    public String getRpProblemDescribe() {
        return rpProblemDescribe;
    }

    public void setRpProblemDescribe(String rpProblemDescribe) {
        this.rpProblemDescribe = rpProblemDescribe;
    }

    public String getRpResponsibleParty() {
        return rpResponsibleParty;
    }

    public void setRpResponsibleParty(String rpResponsibleParty) {
        this.rpResponsibleParty = rpResponsibleParty;
    }

    public String getRpRepairParty() {
        return rpRepairParty;
    }

    public void setRpRepairParty(String rpRepairParty) {
        this.rpRepairParty = rpRepairParty;
    }

    public String getRpRepairResult() {
        return rpRepairResult;
    }

    public void setRpRepairResult(String rpRepairResult) {
        this.rpRepairResult = rpRepairResult;
    }

    public String getRpAddress() {
        return rpAddress;
    }

    public void setRpAddress(String rpAddress) {
        this.rpAddress = rpAddress;
    }

    public String getRpStartDate() {
        return rpStartDate;
    }

    public void setRpStartDate(String rpStartDate) {
        this.rpStartDate = rpStartDate;
    }

    public String getRpEndDate() {
        return rpEndDate;
    }

    public void setRpEndDate(String rpEndDate) {
        this.rpEndDate = rpEndDate;
    }

    public String getRpPlant() {
        return rpPlant;
    }

    public void setRpPlant(String rpPlant) {
        this.rpPlant = rpPlant;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public String getCreateUserId() {
        return createUserId;
    }

    public void setCreateUserId(String createUserId) {
        this.createUserId = createUserId;
    }

    public String getCreateUserName() {
        return createUserName;
    }

    public void setCreateUserName(String createUserName) {
        this.createUserName = createUserName;
    }

    public String getModifyUserId() {
        return modifyUserId;
    }

    public void setModifyUserId(String modifyUserId) {
        this.modifyUserId = modifyUserId;
    }

    public String getModifyUserName() {
        return modifyUserName;
    }

    public void setModifyUserName(String modifyUserName) {
        this.modifyUserName = modifyUserName;
    }

    public Date getGmtCreate() {
        return gmtCreate;
    }

    public void setGmtCreate(Date gmtCreate) {
        this.gmtCreate = gmtCreate;
    }

    public Date getGmtUpdate() {
        return gmtUpdate;
    }

    public void setGmtUpdate(Date gmtUpdate) {
        this.gmtUpdate = gmtUpdate;
    }

    public Byte getIsDeleted() {
        return isDeleted;
    }

    public void setIsDeleted(Byte isDeleted) {
        this.isDeleted = isDeleted;
    }

    public Long getVersions() {
        return versions;
    }

    public void setVersions(Long versions) {
        this.versions = versions;
    }

    @Override
    public String toString() {
        return "WmsInboundRepairDTO{" +
                "rpId=" + rpId +
                ", rpOdId='" + rpOdId + '\'' +
                ", rpCustomerNo='" + rpCustomerNo + '\'' +
                ", rpCustomerName='" + rpCustomerName + '\'' +
                ", rpDispatchNo='" + rpDispatchNo + '\'' +
                ", rpWaybillNo='" + rpWaybillNo + '\'' +
                ", rpOutbillNo='" + rpOutbillNo + '\'' +
                ", rpWhId='" + rpWhId + '\'' +
                ", rpVehicleSpecCode='" + rpVehicleSpecCode + '\'' +
                ", rpVehicleSpecName='" + rpVehicleSpecName + '\'' +
                ", rpVehicleSpecDesc='" + rpVehicleSpecDesc + '\'' +
                ", rpVin='" + rpVin + '\'' +
                ", rpEngine='" + rpEngine + '\'' +
                ", rpType='" + rpType + '\'' +
                ", rpStatus='" + rpStatus + '\'' +
                ", rpZsNo='" + rpZsNo + '\'' +
                ", rpRepairNo='" + rpRepairNo + '\'' +
                ", rpDest='" + rpDest + '\'' +
                ", rpGoodsExplain='" + rpGoodsExplain + '\'' +
                ", rpCheckResult='" + rpCheckResult + '\'' +
                ", rpProblemDescribe='" + rpProblemDescribe + '\'' +
                ", rpResponsibleParty='" + rpResponsibleParty + '\'' +
                ", rpRepairParty='" + rpRepairParty + '\'' +
                ", rpRepairResult='" + rpRepairResult + '\'' +
                ", rpAddress='" + rpAddress + '\'' +
                ", rpStartDate='" + rpStartDate + '\'' +
                ", rpEndDate='" + rpEndDate + '\'' +
                ", rpPlant='" + rpPlant + '\'' +
                ", remark='" + remark + '\'' +
                ", createUserId='" + createUserId + '\'' +
                ", createUserName='" + createUserName + '\'' +
                ", modifyUserId='" + modifyUserId + '\'' +
                ", modifyUserName='" + modifyUserName + '\'' +
                ", gmtCreate=" + gmtCreate +
                ", gmtUpdate=" + gmtUpdate +
                ", isDeleted=" + isDeleted +
                ", versions=" + versions +
                '}';
    }
}
