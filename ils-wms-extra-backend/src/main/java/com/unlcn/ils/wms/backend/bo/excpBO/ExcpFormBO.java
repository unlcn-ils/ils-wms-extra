package com.unlcn.ils.wms.backend.bo.excpBO;

import cn.huiyunche.commons.domain.PageVo;

/**
 * @Auther linbao
 * @Date 2017-11-21
 * 异常信息
 */
public class ExcpFormBO extends PageVo {

    /**
     * 单ID
     */
    private Long orderId;

    private String orderNo;//订单号

    /**
     * 车架号
     */
    private String vin;

    /**
     * 仓库code
     */
    private String whCode;

    /**
     * 异常分类 10-入库异常, 20-出库异常, 30-备料异常
     */
    private String excpType;

    /**
     * 异常类型代码
     */
    private String excpTypeCode;

    /**
     * 异常描述
     */
    private String excpDesc;

    /**
     * 开始异常发生时间
     */
    private String startGmtCreate;

    /**
     * 结束异常发生时间
     */
    private String endGmtCreate;

    /**
     * 开始异常完成时间
     */
    private String startDealEndTime;

    /**
     * 结束异常完成时间
     */
    private String endDealEndTime;

    /**
     * 异常状态
     */
    private String excpStatus;

    /**
     * 处理类型
     */
    private String dealType;

    /**
     * 维修单位
     */
    private String repairDepart;

    /**
     * 维修要求完成时间
     */
    private String repairEndTime;

    /**
     * 维修备注说明
     */
    private String repairRemark;

    /**
     * 处理结果描述
     */
    private String dealResultDesc;

    /**
     * 时间戳
     */
    private String excpTimestamp;

    private String inspectUserName;

    public String getInspectUserName() {
        return inspectUserName;
    }

    public void setInspectUserName(String inspectUserName) {
        this.inspectUserName = inspectUserName;
    }

    public String getOrderNo() {
        return orderNo;
    }

    public void setOrderNo(String orderNo) {
        this.orderNo = orderNo;
    }

    public String getExcpTimestamp() {
        return excpTimestamp;
    }

    public void setExcpTimestamp(String excpTimestamp) {
        this.excpTimestamp = excpTimestamp;
    }

    public Long getOrderId() {
        return orderId;
    }

    public void setOrderId(Long orderId) {
        this.orderId = orderId;
    }

    public String getVin() {
        return vin;
    }

    public void setVin(String vin) {
        this.vin = vin;
    }

    public String getWhCode() {
        return whCode;
    }

    public void setWhCode(String whCode) {
        this.whCode = whCode;
    }

    public String getExcpType() {
        return excpType;
    }

    public void setExcpType(String excpType) {
        this.excpType = excpType;
    }

    public String getExcpTypeCode() {
        return excpTypeCode;
    }

    public void setExcpTypeCode(String excpTypeCode) {
        this.excpTypeCode = excpTypeCode;
    }

    public String getExcpDesc() {
        return excpDesc;
    }

    public void setExcpDesc(String excpDesc) {
        this.excpDesc = excpDesc;
    }

    public String getStartGmtCreate() {
        return startGmtCreate;
    }

    public void setStartGmtCreate(String startGmtCreate) {
        this.startGmtCreate = startGmtCreate;
    }

    public String getEndGmtCreate() {
        return endGmtCreate;
    }

    public void setEndGmtCreate(String endGmtCreate) {
        this.endGmtCreate = endGmtCreate;
    }

    public String getStartDealEndTime() {
        return startDealEndTime;
    }

    public void setStartDealEndTime(String startDealEndTime) {
        this.startDealEndTime = startDealEndTime;
    }

    public String getEndDealEndTime() {
        return endDealEndTime;
    }

    public void setEndDealEndTime(String endDealEndTime) {
        this.endDealEndTime = endDealEndTime;
    }

    public String getExcpStatus() {
        return excpStatus;
    }

    public void setExcpStatus(String excpStatus) {
        this.excpStatus = excpStatus;
    }

    public String getDealType() {
        return dealType;
    }

    public void setDealType(String dealType) {
        this.dealType = dealType;
    }

    public String getRepairDepart() {
        return repairDepart;
    }

    public void setRepairDepart(String repairDepart) {
        this.repairDepart = repairDepart;
    }

    public String getRepairEndTime() {
        return repairEndTime;
    }

    public void setRepairEndTime(String repairEndTime) {
        this.repairEndTime = repairEndTime;
    }

    public String getRepairRemark() {
        return repairRemark;
    }

    public void setRepairRemark(String repairRemark) {
        this.repairRemark = repairRemark;
    }

    public String getDealResultDesc() {
        return dealResultDesc;
    }

    public void setDealResultDesc(String dealResultDesc) {
        this.dealResultDesc = dealResultDesc;
    }

    @Override
    public String toString() {
        return "ExcpFormBO{" +
                "orderId=" + orderId +
                ", vin='" + vin + '\'' +
                ", whCode='" + whCode + '\'' +
                ", excpType='" + excpType + '\'' +
                ", excpTypeCode='" + excpTypeCode + '\'' +
                ", excpDesc='" + excpDesc + '\'' +
                ", startGmtCreate='" + startGmtCreate + '\'' +
                ", endGmtCreate='" + endGmtCreate + '\'' +
                ", startDealEndTime='" + startDealEndTime + '\'' +
                ", endDealEndTime='" + endDealEndTime + '\'' +
                ", excpStatus='" + excpStatus + '\'' +
                ", dealType='" + dealType + '\'' +
                ", repairDepart='" + repairDepart + '\'' +
                ", repairEndTime='" + repairEndTime + '\'' +
                ", repairRemark='" + repairRemark + '\'' +
                ", dealResultDesc='" + dealResultDesc + '\'' +
                '}';
    }
}
