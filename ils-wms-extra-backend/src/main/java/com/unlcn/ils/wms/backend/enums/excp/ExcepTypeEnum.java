package com.unlcn.ils.wms.backend.enums.excp;

/**
 * @Auther linbao
 * @Date 2017-11-23
 * 异常分类
 */
public enum ExcepTypeEnum {
    IN_EXCEP("10", "入库异常"),
    OUT_EXCEP("20", "出库异常"),
    PREPARE_EXCEP("30", "备料异常");

    private final String code;

    private final String text;

    ExcepTypeEnum(String code, String text) {
        this.code = code;
        this.text = text;
    }

    public String getCode() {
        return code;
    }

    public String getText() {
        return text;
    }
}
