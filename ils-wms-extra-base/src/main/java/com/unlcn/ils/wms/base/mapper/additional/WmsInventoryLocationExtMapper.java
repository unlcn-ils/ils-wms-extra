package com.unlcn.ils.wms.base.mapper.additional;

import com.unlcn.ils.wms.base.dto.InboundExcelDTO;
import com.unlcn.ils.wms.base.dto.SelectChangeLocCodeFromGetLocationDTO;
import com.unlcn.ils.wms.base.dto.WmsInventoryLocationChangePrepareByVinDTO;
import com.unlcn.ils.wms.base.dto.WmsInventoryLocationFreezeDTO;
import com.unlcn.ils.wms.base.model.stock.WmsInventoryLocation;
import org.apache.ibatis.annotations.Param;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by DELL on 2017/8/29.
 */
public interface WmsInventoryLocationExtMapper {
    List<WmsInventoryLocation> selectInfoByLocation(Map<String, Object> paramMap);

    List<WmsInventoryLocation> listInventoryLocationByInvId(String invId);

    /**
     * 根据参数查询库存明细
     *
     * @param paramsMap
     * @return
     */
    List<WmsInventoryLocation> findParams(@Param("paramsMap") Map<String, Object> paramsMap);

    /**
     * 根据车架号更新状态
     *
     * @param status
     * @param vinList
     * @return
     */
    Integer updateStatusByVinList(@Param("status") Integer status, @Param("vinList") List<String> vinList);

    /**
     * 查询某时间段的入库车辆信息
     *
     * @param startTime
     * @param endTime
     * @param normalCode
     * @return
     */
    List<InboundExcelDTO> inboundExcelData(@Param("startTime") String startTime, @Param("endTime") String endTime, @Param("invlocWhCode") String invlocWhCode, @Param("normal") String normalCode);

    void updateCancelStatusByParam(HashMap<String, Object> paramMap);

    List<InboundExcelDTO> inboundExcelDataNew(HashMap<String, Object> paramMap);

    int updateInvtoryToNormalListForJM(HashMap<String, Object> paramMap);

    void updateFreezeStatusByIds(@Param("params") HashMap<String, Object> params);

    List<WmsInventoryLocation> selectNormalListByIds(List<String> ids);

    List<WmsInventoryLocationFreezeDTO> selectFreezeListByIds(List<String> ids);

    void updateNormalStatusByIds(@Param("params") HashMap<String, Object> params);

    List<WmsInventoryLocationChangePrepareByVinDTO> selectChangePrepareByVin(HashMap<String, Object> c_params);

    List<Map<String, Object>> selectInboundRecordsForManageByParam(HashMap<String, Object> params);

    void updateSQLMode();

    Long countInboundRecordsForManageByParam(HashMap<String, Object> params);

    void updateInventoryLocCodeNew(HashMap<String, Object> params);

    List<SelectChangeLocCodeFromGetLocationDTO> selectChangeCodeNewFromGetLocation(HashMap<String, Object> params);

}
