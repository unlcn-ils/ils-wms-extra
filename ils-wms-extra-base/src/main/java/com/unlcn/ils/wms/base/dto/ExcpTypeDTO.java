package com.unlcn.ils.wms.base.dto;

/**
 * @Auther linbao
 * @Date 2017-11-23
 */
public class ExcpTypeDTO {

    /**
     * 异常区域 一级
     */
    private String excpPositionName;

    /**
     * 异常位置 二级
     */
    private String excpBodyName;

    /**
     * 具体异常 三级
     */
    private String excpHurtName;

    /**
     * 图片key
     */
    private String picKey;

    public String getExcpPositionName() {
        return excpPositionName;
    }

    public void setExcpPositionName(String excpPositionName) {
        this.excpPositionName = excpPositionName;
    }

    public String getExcpBodyName() {
        return excpBodyName;
    }

    public void setExcpBodyName(String excpBodyName) {
        this.excpBodyName = excpBodyName;
    }

    public String getExcpHurtName() {
        return excpHurtName;
    }

    public void setExcpHurtName(String excpHurtName) {
        this.excpHurtName = excpHurtName;
    }

    public String getPicKey() {
        return picKey;
    }

    public void setPicKey(String picKey) {
        this.picKey = picKey;
    }
}
