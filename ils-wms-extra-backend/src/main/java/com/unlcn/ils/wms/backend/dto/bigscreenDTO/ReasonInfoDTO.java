package com.unlcn.ils.wms.backend.dto.bigscreenDTO;

/**
 * Created by lenovo on 2017/11/8.
 */
public class ReasonInfoDTO {
    // 数量
    private Integer value;
    // 类型
    private String name;

    public Integer getValue() {
        return value;
    }

    public void setValue(Integer value) {
        this.value = value;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "ReasonInfoDTO{" +
                "value=" + value +
                ", name='" + name + '\'' +
                '}';
    }
}
