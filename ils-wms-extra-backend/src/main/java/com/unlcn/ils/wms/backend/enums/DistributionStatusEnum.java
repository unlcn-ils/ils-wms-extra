package com.unlcn.ils.wms.backend.enums;

/**
 * 配送状态枚举
 */
public enum DistributionStatusEnum {
    NORMAl_TRANSIT(1, "正常在途"),
    OVERDUE_TRANSIT(2, "超期在途"),
    NORMAL_ARRIVAL(3, "正常运抵"),
    EXTENDED_ARRIVAL(4, "超期运抵");

    private final Integer code;
    private final String value;

    public Integer getCode() {
        return code;
    }

    public String getValue() {
        return value;
    }

    DistributionStatusEnum(Integer code, String value) {
        this.code = code;
        this.value = value;
    }
}
