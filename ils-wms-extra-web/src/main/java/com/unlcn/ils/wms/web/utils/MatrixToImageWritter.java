package com.unlcn.ils.wms.web.utils;

import com.google.zxing.common.BitMatrix;

import javax.imageio.ImageIO;  
import javax.servlet.http.HttpServletRequest;

import java.io.File;  
import java.io.OutputStream;  
import java.io.IOException;  
import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Font;
import java.awt.FontFormatException;
import java.awt.Graphics2D;
import java.awt.font.FontRenderContext;
import java.awt.geom.Rectangle2D;
import java.awt.image.BufferedImage;  
   
   
public final class MatrixToImageWritter {  

	private static final int BLACK = 0xFF000000;
	private static final int WHITE = 0xFFFFFFFF;

	private MatrixToImageWritter() {
	}

	public static BufferedImage toBufferedImage(BitMatrix matrix) {
		int width = matrix.getWidth();
		int height = matrix.getHeight();
		BufferedImage image = new BufferedImage(width, height,
				BufferedImage.TYPE_INT_RGB);
		for (int x = 0; x < width; x++) {
			for (int y = 0; y < height; y++) {
				image.setRGB(x, y, matrix.get(x, y) ? BLACK : WHITE);
			}
		}
		return image;
	}

	public static void writeToFile(BitMatrix matrix, String format, File file)
			throws IOException {
		BufferedImage image = toBufferedImage(matrix);
		if (!ImageIO.write(image, format, file)) {
			throw new IOException("Could not write an image of format "
					+ format + " to " + file);
		}
	}
	
	public static void writeToFile(BufferedImage image, String format, File file)
			throws IOException {
		if (!ImageIO.write(image, format, file)) {
			throw new IOException("Could not write an image of format "
					+ format + " to " + file);
		}
	}

	public static void writeToStream(BitMatrix matrix, String format,
			OutputStream stream) throws IOException {
		BufferedImage image = toBufferedImage(matrix);
		if (!ImageIO.write(image, format, stream)) {
			throw new IOException("Could not write an image of format "
					+ format);
		}
	}
  
	public static void writeToStream(BufferedImage image, String format,
			OutputStream stream) throws IOException {
		if (!ImageIO.write(image, format, stream)) {
			throw new IOException("Could not write an image of format "
					+ format);
		}
	}
	
	public static BitMatrix deleteWhite(BitMatrix matrix) {
		int[] rec = matrix.getEnclosingRectangle();
		int resWidth = rec[2] + 1;
		int resHeight = rec[3] + 1;

		BitMatrix resMatrix = new BitMatrix(resWidth, resHeight);
		resMatrix.clear();
		for (int i = 0; i < resWidth; i++) {
			for (int j = 0; j < resHeight; j++) {
				if (matrix.get(i + rec[0], j + rec[1]))
					resMatrix.set(i, j);
			}
		}
		return resMatrix;
	}
   
	public static BufferedImage combineTwoBufferedImage(BufferedImage up, BufferedImage down) {
		int width = up.getWidth();
		int height = up.getHeight() + down.getHeight();
		
		BufferedImage returnImage = new BufferedImage(width, height,
				BufferedImage.TYPE_INT_RGB);

		for (int x = 0; x < up.getWidth(); x++) {
			for (int y = 0; y < up.getHeight(); y++) {
				returnImage.setRGB(x, y, up.getRGB(x, y));
			}
			for (int y = 0; y < down.getHeight(); y++) {
				returnImage.setRGB(x, y + up.getHeight(), down.getRGB(x, y));
			}
		}
		
		return returnImage;
	};
	

	
	public static BufferedImage getBufferImageForString(int width, int height, String content,HttpServletRequest request) {
		BufferedImage image = new BufferedImage(width, height,
				BufferedImage.TYPE_INT_RGB);
		
		// TODO Auto-generated method stub
		Font ttfFont;
		try {
			ttfFont = Font.createFont(Font.TRUETYPE_FONT, new File(
					request.getRealPath("/resources/default/font/")+ "/simsun.ttc"));

			Font ttfReal = ttfFont.deriveFont(Font.PLAIN, 12);
			image = new BufferedImage(width, height, BufferedImage.TYPE_INT_RGB);
			Graphics2D g2 = (Graphics2D) image.getGraphics();
			g2.setBackground(Color.WHITE);
			g2.clearRect(0, 0, width, height);
			g2.setPaint(Color.BLACK);
			FontRenderContext context = g2.getFontRenderContext();
			Rectangle2D bounds = ttfReal.getStringBounds(content, context);
			double x = (width - bounds.getWidth()) / 2;
			double y = (height - bounds.getHeight()) / 2;
			double ascent = -bounds.getY();
			double baseY = y + ascent;
			g2.setFont(ttfReal);
			g2.drawString(content, (int) x, (int) baseY);
	
//			if (!ImageIO.write(image, "gif", new File("c:" + File.separator
//					+ "Personal" + File.separator + "YueHuaWBS" + File.separator
//					+ "new.gif"))) {
//				throw new IOException("Could not write an image of format ");
//			}

		} catch (FontFormatException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return image;
	}
	
	public static void addLogoToBarCodeImage(BufferedImage image, File logoPic) {
		try {
			LogoConfig logoConfig = new LogoConfig();
			if (!logoPic.isFile()) {
				System.out.print("file not find !");
				System.exit(0);
			}

			/**
			 * 读取二维码图片，并构建绘图对象
			 */
			Graphics2D g = image.createGraphics();

			/**
			 * 读取Logo图片
			 */
			BufferedImage logo = ImageIO.read(logoPic);

			int widthLogo = logo.getWidth(), heightLogo = logo.getHeight();

			// 计算图片放置位置
			int x = (image.getWidth() - widthLogo) / 2;
			int y = (image.getHeight() - logo.getHeight()) / 2;

			// 开始绘制图片
			g.drawImage(logo, x, y, widthLogo, heightLogo, null);
			g.drawRoundRect(x, y, widthLogo, heightLogo, 15, 15);
			g.setStroke(new BasicStroke(logoConfig.getBorder()));
			g.setColor(logoConfig.getBorderColor());
			g.drawRect(x, y, widthLogo, heightLogo);

			g.dispose();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}


