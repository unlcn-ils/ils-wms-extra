package com.unlcn.ils.wms.base.mapper.additional;

import com.unlcn.ils.wms.base.businessDTO.baseData.WmsEmptyLocationDTO;
import com.unlcn.ils.wms.base.businessDTO.stock.WmsInventoryDTO;
import com.unlcn.ils.wms.base.model.stock.WmsLocation;

import java.util.List;
import java.util.Map;

/**
 * 库位
 * Created by DELL on 2017/8/14.
 */
public interface WmsLocationExtMapper {
    /**
     * 查询空库区
     * @param paramMap
     * @return
     */
    List<WmsEmptyLocationDTO> getWmsEmptyZone(Map<String,Object> paramMap);

    /**
     * 查询空库位
     * @param paramMap
     * @return
     */
    List<WmsEmptyLocationDTO> getWmsEmptyLocation(Map<String,Object> paramMap);
    //List<WmsInventoryDTO> queryWmsInventoryByInfo(Map<String, Object> paramMap);

    /**
     * 根据条件获得库位信息
     * @param whId
     * @return
     */
    List<WmsLocation> getAllLocation(String whId);

    /**
     * 查询是否库位已经被占用
     * @param paramMap
     * @return
     */
    int getInfoByZoneIdAndLocationId(Map<String,Object> paramMap);


    /**
     * 分页数据
     * @param paramMap
     * @return
     */
    List<WmsLocation> queryForList(Map<String,Object> paramMap);

    /**
     * 分页数据总数
     * @param paramMap
     * @return
     */
    Integer queryForCount(Map<String,Object> paramMap);

    /**
     * 启用禁用库位
     * @param paramMap
     */
    void enableLocation(Map<String,Object> paramMap);

    /**
     * 根据仓库以及类型查询库区
     * @param paramMap
     * @return
     */
    List<WmsEmptyLocationDTO> queryQualifiedZone(Map<String,Object> paramMap);
}
