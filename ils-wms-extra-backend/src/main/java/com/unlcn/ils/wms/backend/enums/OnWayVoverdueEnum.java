package com.unlcn.ils.wms.backend.enums;

/**
 * 前端发运数据超期状态枚举
 */
public enum OnWayVoverdueEnum {

    NORMAL("HAS_QUIT", "正常"),
    WARNING("WARNING", "预警"),
    WATER("OVERDUE-WARNING", "超期预警");

    private final String value;
    private final String text;

    OnWayVoverdueEnum(String value, String text) {
        this.value = value;
        this.text = text;
    }

    public String getValue() {
        return value;
    }

    public String getText() {
        return text;
    }

    public static OnWayVoverdueEnum getByValue(String value) {
        for (OnWayVoverdueEnum temp : OnWayVoverdueEnum.values()) {
            if (temp.getValue().equals(value)) {
                return temp;
            }
        }
        return null;
    }

}
