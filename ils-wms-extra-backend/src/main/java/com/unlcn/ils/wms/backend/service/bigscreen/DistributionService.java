package com.unlcn.ils.wms.backend.service.bigscreen;

import com.unlcn.ils.wms.backend.dto.bigscreenDTO.DistributionChartDTO;
import com.unlcn.ils.wms.base.model.bigscreen.BiDistribution;

import java.util.List;

/**
 * Created by lenovo on 2017/11/13.
 */
public interface DistributionService {

    /**
     * 配送推移图统计
     * @return
     */
    List<DistributionChartDTO> distributionChartCount();

    /**
     * 配送列表统计
     * @return
     */
    List<BiDistribution> distributionLineCount();

    List<BiDistribution> getDisLineAndGateTime();

    /**
     * 配送统计图
     */
    void addDistributionChart();

    /**
     * 配送统计列表
     */
    void addDistributionLine();
}
