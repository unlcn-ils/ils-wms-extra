package com.unlcn.ils.wms.base.businessDTO.baseData;

import com.unlcn.ils.wms.base.businessDTO.baseData.WmsEmptyLocationDTO;

import java.util.List;

/**
 * Created by DELL on 2017/8/15.
 */
public class WmsEmptyZoneDTO {
    /**
     * 仓库ID
     */
    private String whId;

    /**
     * 仓库CODE
     */
    private String whCode;

    /**
     * 库区ID
     */
    private String zoneId;

    /**
     * 库区名称
     */
    private String zoneName;

    /**
     * 库区代码
     */
    private String zoneCode;

    /**
     * 库位信息
     */
    private List<WmsEmptyLocationDTO> wmsEmptyLocationDTOList = null;

    public String getWhId() {
        return whId;
    }

    public void setWhId(String whId) {
        this.whId = whId;
    }

    public String getWhCode() {
        return whCode;
    }

    public void setWhCode(String whCode) {
        this.whCode = whCode;
    }

    public String getZoneId() {
        return zoneId;
    }

    public void setZoneId(String zoneId) {
        this.zoneId = zoneId;
    }

    public String getZoneName() {
        return zoneName;
    }

    public void setZoneName(String zoneName) {
        this.zoneName = zoneName;
    }

    public String getZoneCode() {
        return zoneCode;
    }

    public void setZoneCode(String zoneCode) {
        this.zoneCode = zoneCode;
    }

    public List<WmsEmptyLocationDTO> getWmsEmptyLocationDTOList() {
        return wmsEmptyLocationDTOList;
    }

    public void setWmsEmptyLocationDTOList(List<WmsEmptyLocationDTO> wmsEmptyLocationDTOList) {
        this.wmsEmptyLocationDTOList = wmsEmptyLocationDTOList;
    }

    @Override
    public String toString() {
        return "WmsEmptyZoneDTO{" +
                "whId='" + whId + '\'' +
                ", whCode='" + whCode + '\'' +
                ", zoneId='" + zoneId + '\'' +
                ", zoneName='" + zoneName + '\'' +
                ", zoneCode='" + zoneCode + '\'' +
                ", wmsEmptyLocationDTOList=" + wmsEmptyLocationDTOList +
                '}';
    }
}
