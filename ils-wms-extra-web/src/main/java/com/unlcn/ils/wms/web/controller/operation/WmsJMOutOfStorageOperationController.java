package com.unlcn.ils.wms.web.controller.operation;

import cn.huiyunche.commons.domain.PageVo;
import cn.huiyunche.commons.domain.ResultDTO;
import cn.huiyunche.commons.domain.ResultDTOWithPagination;
import cn.huiyunche.commons.exception.BusinessException;
import com.unlcn.ils.wms.backend.service.operation.WmsJmOutOfStorageOperationService;
import com.unlcn.ils.wms.base.dto.WmsJmOutOfStorageExcpOperationDTO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;

@Controller
@RequestMapping("/outOfStorageOperation")
@ResponseBody
/**
 * <p>
 *     2018-3-10 增加webservice同步君马dcs/sap接口的运维接口
 * </p>
 */
public class WmsJMOutOfStorageOperationController {
    private Logger logger = LoggerFactory.getLogger(getClass());

    private WmsJmOutOfStorageOperationService operationService;

    @Autowired
    public void setOperationService(WmsJmOutOfStorageOperationService operationService) {
        this.operationService = operationService;
    }

    /**
     * 获取出入库接口数据列表
     *
     * @param dto 参数封装
     */
    @RequestMapping(value = "/queryList", method = RequestMethod.POST)
    public ResultDTOWithPagination<Object> queryExcpList(@RequestBody WmsJmOutOfStorageExcpOperationDTO dto, HttpServletRequest request) {
        logger.info("WmsJMOutOfStorageOperationController.queryExcpList params:{}", dto);
        ResultDTOWithPagination<Object> result = new ResultDTOWithPagination<>(true, null, "查询成功!");
        try {
            String whCode = request.getHeader("whCode");
            dto.setWhCode(whCode);
            HashMap<String, Object> data = operationService.listOutOfStorageExcp(dto);
            result.setData(data.get("data"));
            result.setPageVo((PageVo) data.get("pageVo"));
        } catch (BusinessException e) {
            logger.error("WmsJMOutOfStorageOperationController.queryExcpList BusinessException:", e);
            result.setMessage(e.getMessage());
        } catch (Exception e) {
            logger.error("WmsJMOutOfStorageOperationController.queryExcpList error:", e);
            result.setMessage("查询异常");
        }
        return result;
    }


    /**
     * 再重发送到dcs
     */
    @RequestMapping(value = "/sendDcsAgain", method = RequestMethod.POST)
    public ResultDTO<Object> updateSendToDCSAgain(@RequestBody WmsJmOutOfStorageExcpOperationDTO dto, HttpServletRequest request) {
        logger.info("WmsJMOutOfStorageOperationController.updateSendToDCSAgain params:{}", dto);
        ResultDTO<Object> result = new ResultDTO<>(true, null, "操作成功!");
        String whCode = request.getHeader("whCode");
        String userId = request.getHeader("userId");
        dto.setWhCode(whCode);
        dto.setUserId(userId);
        try {
            operationService.updateSendToDCSAgain(dto);
        } catch (BusinessException e) {
            logger.error("WmsJMOutOfStorageOperationController.sendDcsAgain BusinessException:", e);
            result.setMessage(e.getMessage());
        } catch (Exception e) {
            logger.error("WmsJMOutOfStorageOperationController.sendDcsAgain error:", e);
            result.setMessage("操作异常");
        }
        return result;
    }


    /**
     * 再重发送到dcs
     */
    @RequestMapping(value = "/sendSapAgain", method = RequestMethod.POST)
    public ResultDTO<Object> updateSendToSAPAgain(@RequestBody WmsJmOutOfStorageExcpOperationDTO dto, HttpServletRequest request) {
        logger.info("WmsJMOutOfStorageOperationController.updateSendToSAPAgain params:{}", dto);
        ResultDTO<Object> result = new ResultDTO<>(true, null, "操作成功!");
        String whCode = request.getHeader("whCode");
        String userId = request.getHeader("userId");
        dto.setWhCode(whCode);
        dto.setUserId(userId);
        try {
            operationService.updateSendToSapAgain(dto);
        } catch (BusinessException e) {
            logger.error("WmsJMOutOfStorageOperationController.updateSendToSAPAgain BusinessException:", e);
            result.setMessage(e.getMessage());
        } catch (Exception e) {
            logger.error("WmsJMOutOfStorageOperationController.updateSendToSAPAgain error:", e);
            result.setMessage("操作异常");
        }
        return result;
    }

    /**
     * 手动调整sap的接口数据为成功
     */
    @RequestMapping(value = "/updateDcsToSuccess", method = RequestMethod.POST)
    public ResultDTO<Object> updateDcsToSuccess(@RequestBody WmsJmOutOfStorageExcpOperationDTO dto, HttpServletRequest request) {
        logger.info("WmsJMOutOfStorageOperationController.updateDcsToSuccess params:{}", dto);
        ResultDTO<Object> result = new ResultDTO<>(true, null, "操作成功!");
        String whCode = request.getHeader("whCode");
        String userId = request.getHeader("userId");
        dto.setWhCode(whCode);
        dto.setUserId(userId);
        try {
            operationService.updateDcsToSuccess(dto);
        } catch (BusinessException e) {
            logger.error("WmsJMOutOfStorageOperationController.updateDcsToSuccess BusinessException:", e);
            result.setMessage(e.getMessage());
        } catch (Exception e) {
            logger.error("WmsJMOutOfStorageOperationController.updateDcsToSuccess error:", e);
            result.setMessage("操作异常");
        }
        return result;
    }

    /**
     * 手动调整sap的接口数据为成功
     */
    @RequestMapping(value = "/updateSapToSuccess", method = RequestMethod.POST)
    public ResultDTO<Object> updateSapToSuccess(@RequestBody WmsJmOutOfStorageExcpOperationDTO dto, HttpServletRequest request) {
        logger.info("WmsJMOutOfStorageOperationController.updateSapToSuccess params:{}", dto);
        ResultDTO<Object> result = new ResultDTO<>(true, null, "操作成功!");
        String whCode = request.getHeader("whCode");
        String userId = request.getHeader("userId");
        dto.setWhCode(whCode);
        dto.setUserId(userId);
        try {
            operationService.updateSendToSapSuccess(dto);
        } catch (BusinessException e) {
            logger.error("WmsJMOutOfStorageOperationController.updateSapToSuccess BusinessException:", e);
            result.setMessage(e.getMessage());
        } catch (Exception e) {
            logger.error("WmsJMOutOfStorageOperationController.updateSapToSuccess error:", e);
            result.setMessage("操作异常");
        }
        return result;
    }

}
