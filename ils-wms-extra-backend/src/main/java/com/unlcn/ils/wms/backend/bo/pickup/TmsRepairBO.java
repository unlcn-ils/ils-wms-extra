package com.unlcn.ils.wms.backend.bo.pickup;

/**
 * Created by zhiche024 on 2017/7/22.
 */
public class TmsRepairBO {

    private Long id ;        //主键
    private Long pickId;        //验车信息ID
    private String repairCode;   //返修单编码
    private Long repairTimeId;     //返修时间ID
    private Integer status;      //状态
    private String gmtCreate;   //创建时间

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getPickId() {
        return pickId;
    }

    public void setPickId(Long pickId) {
        this.pickId = pickId;
    }

    public String getRepairCode() {
        return repairCode;
    }

    public void setRepairCode(String repairCode) {
        this.repairCode = repairCode;
    }

    public Long getRepairTimeId() {
        return repairTimeId;
    }

    public void setRepairTimeId(Long repairTimeId) {
        this.repairTimeId = repairTimeId;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getGmtCreate() {
        return gmtCreate;
    }

    public void setGmtCreate(String gmtCreate) {
        this.gmtCreate = gmtCreate;
    }

    @Override
    public String toString() {
        return "TmsRepairBO{" +
                "id=" + id +
                ", pickId=" + pickId +
                ", repairCode='" + repairCode + '\'' +
                ", repairTimeId=" + repairTimeId +
                ", status=" + status +
                ", gmtCreate='" + gmtCreate + '\'' +
                '}';
    }
}
