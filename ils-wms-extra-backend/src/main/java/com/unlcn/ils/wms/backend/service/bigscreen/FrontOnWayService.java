package com.unlcn.ils.wms.backend.service.bigscreen;

import cn.huiyunche.commons.domain.ResultDTOWithPagination;
import com.unlcn.ils.wms.base.dto.WmsOnWayLineDTO;
import com.unlcn.ils.wms.base.model.bigscreen.BiFrontOnWay;

import java.util.List;

/**
 * Created by lenovo on 2017/11/7.
 */
public interface FrontOnWayService {
    /**
     * 前端在途数据列表
     */
    List<BiFrontOnWay> onWayLine();

    /**
     * 添加前端在途
     */
    void addFrontOnWay();

    ResultDTOWithPagination<List<BiFrontOnWay>> onWayLineWithPagiation(WmsOnWayLineDTO dto) throws Exception;
}
