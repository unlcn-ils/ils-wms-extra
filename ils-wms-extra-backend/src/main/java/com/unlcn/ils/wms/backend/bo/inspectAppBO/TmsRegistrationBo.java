package com.unlcn.ils.wms.backend.bo.inspectAppBO;

import java.io.Serializable;

public class TmsRegistrationBo implements Serializable {
    private String vin;
    private Long InspectId;
    private String userId;

    public String getVin() {
        return vin;
    }

    public void setVin(String vin) {
        this.vin = vin;
    }

    public Long getInspectId() {
        return InspectId;
    }

    public void setInspectId(Long inspectId) {
        InspectId = inspectId;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }
}
