package com.unlcn.ils.wms.backend.bo.biBO;

import java.io.Serializable;

/**
 * Created by Nicky on 17/11/9.
 */
public class BiDRISTIONLISTBO implements Serializable {

    /**
     * 指令号
     */
    private String shipno;

    /**
     * 台数
     */
    private String qty;

    /**
     *承运车牌
     */
    private String vehicle;

    /**
     *分供方
     */
    private String supplier;

    /**
     *司机
     */
    private String driver;

    /**
     *联系电话
     */
    private String mobile;

    /**
     *指令起运地
     */
    private String route_start;

    /**
     * 指令目的地
     */
    private String route_end;

    /**
     * 指令发运时间,yyyyMMdd hh24mi
     */
    private String out_date;

    /**
     * 预计指令抵达时间
     */
    private String est_arrival_date;

    /**
     *ERP登记运抵时间
     */
    private String act_arrival_date;

    /**
     * 状态，备选值[正常在途，超期在途，正常运抵，超期运抵]
     */
    private String status;

    /**
     * 车辆当前位置
     */
    private String position;

    /**
     *经度
     */
    private String latitude;

    /**
     *纬度
     */
    private String longitude;

    public String getShipno() {
        return shipno;
    }

    public void setShipno(String shipno) {
        this.shipno = shipno;
    }

    public String getQty() {
        return qty;
    }

    public void setQty(String qty) {
        this.qty = qty;
    }

    public String getVehicle() {
        return vehicle;
    }

    public void setVehicle(String vehicle) {
        this.vehicle = vehicle;
    }

    public String getSupplier() {
        return supplier;
    }

    public void setSupplier(String supplier) {
        this.supplier = supplier;
    }

    public String getDriver() {
        return driver;
    }

    public void setDriver(String driver) {
        this.driver = driver;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getRoute_start() {
        return route_start;
    }

    public void setRoute_start(String route_start) {
        this.route_start = route_start;
    }

    public String getRoute_end() {
        return route_end;
    }

    public void setRoute_end(String route_end) {
        this.route_end = route_end;
    }

    public String getOut_date() {
        return out_date;
    }

    public void setOut_date(String out_date) {
        this.out_date = out_date;
    }

    public String getEst_arrival_date() {
        return est_arrival_date;
    }

    public void setEst_arrival_date(String est_arrival_date) {
        this.est_arrival_date = est_arrival_date;
    }

    public String getAct_arrival_date() {
        return act_arrival_date;
    }

    public void setAct_arrival_date(String act_arrival_date) {
        this.act_arrival_date = act_arrival_date;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getPosition() {
        return position;
    }

    public void setPosition(String position) {
        this.position = position;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    @Override
    public String toString() {
        return "BiDRISTIONLISTBO{" +
                "shipno='" + shipno + '\'' +
                ", qty='" + qty + '\'' +
                ", vehicle='" + vehicle + '\'' +
                ", supplier='" + supplier + '\'' +
                ", driver='" + driver + '\'' +
                ", mobile='" + mobile + '\'' +
                ", route_start='" + route_start + '\'' +
                ", route_end='" + route_end + '\'' +
                ", out_date='" + out_date + '\'' +
                ", est_arrival_date='" + est_arrival_date + '\'' +
                ", act_arrival_date='" + act_arrival_date + '\'' +
                ", status='" + status + '\'' +
                ", position='" + position + '\'' +
                ", latitude='" + latitude + '\'' +
                ", longitude='" + longitude + '\'' +
                '}';
    }
}
