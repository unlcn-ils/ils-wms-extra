package com.unlcn.ils.wms.backend.service.outbound;

import com.unlcn.ils.wms.backend.bo.outboundBO.WmsShipmentPlanRejectBO;
import com.unlcn.ils.wms.base.dto.WmsChangePreparaByVinDTO;
import com.unlcn.ils.wms.base.dto.WmsRejectUndealDTO;
import com.unlcn.ils.wms.base.dto.WmsShipmentPlanRejectDetailDTO;
import com.unlcn.ils.wms.base.model.stock.WmsShipmentPlan;

import java.util.List;
import java.util.Map;

/**
 * @Auther linbao
 * @Date 2017-10-27
 */
public interface WmsShipmentPlanRejectService {

    /**
     * 列表查询
     * @param shipmentPlanRejectBO
     * @return
     * @throws Exception
     */
    Map<String, Object> queryRejectList(WmsShipmentPlanRejectBO shipmentPlanRejectBO) throws Exception;

    /**
     * 发运计划驳回
     *
     * @param spId
     * @throws Exception
     */
    void updateToReject(Long spId) throws Exception;

    /**
     * 获取详情列表
     * @param spId
     * @throws Exception
     */
    List<WmsShipmentPlanRejectDetailDTO> queryDetailList(Long spId) throws Exception;

    /**
     * 取消任务
     *
     * @param wmsShipmentPlanRejectBO
     * @throws Exception
     */
    void updateToCancleTask(WmsShipmentPlanRejectBO wmsShipmentPlanRejectBO) throws Exception;

    /**
     * 备料替换
     *
     * @return
     * @throws Exception
     */
    Map<String, String> updateToReplace(WmsShipmentPlanRejectBO wmsShipmentPlanRejectBO) throws Exception;

    Map<String, String> updateChangeDetailByVin(WmsChangePreparaByVinDTO byVinDTO) throws Exception;

    void updateToRejectNotHandle(WmsRejectUndealDTO dto) throws Exception;

    List<WmsShipmentPlan> getNotHandleRejectList(Long spId, String whCode) throws Exception;
}
