package com.unlcn.ils.wms.base.mapper.extmapper;

import com.unlcn.ils.wms.base.model.bigscreen.BiOverReason;
import com.unlcn.ils.wms.base.model.bigscreen.BiOverReasonExample;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface BiOverReasonExtMapper {

    /**
     * 超期原因分析图统计
     * @return
     */
    List<BiOverReason> reasonChartCount();
}