package com.unlcn.ils.wms.backend.dto.outboundDTO;

import java.util.List;

/**
 * @Auther linbao
 * @Date 2017-10-23
 * 交接单
 */
public class HandoverDTO {


    /**
     * 条形码
     */
    private String image;

    /**
     * 运单号
     */
    private String waybillNo;

    /**
     * 仓库
     */
    private String whName;

    /**
     * 装车道
     */
    private String estimateLane;

    /**
     * 运输期限
     */
    private String transportTime;

    /**
     * 收车地址
     */
    private String destAddress;

    /**
     * 承运车牌
     */
    private String vehiclePlate;

    /**
     * 运输类型
     */
    private String type;

    /**
     * 联系人
     */
    private String logiPerson;

    /**
     * 联系电话
     */
    private String logiTel;

    /**
     * 司机
     */
    private String driver;

    /**
     * 司机电话
     */
    private String driverTel;

    /**
     * 经销商
     */
    private String dealeName;

    /**
     * 承运商
     */
    private String logiName;

    /**
     * 合格本
     */
    private Integer standardNum;

    /**
     * 条数
     */
    private Integer allNum;

    /**
     * 收车单位
     */
    private String collectUnit;

    /**
     * 详情
     */
    private List<HandoverDetailDTO> detailList;

    public String getWaybillNo() {
        return waybillNo;
    }

    public void setWaybillNo(String waybillNo) {
        this.waybillNo = waybillNo;
    }

    public String getWhName() {
        return whName;
    }

    public void setWhName(String whName) {
        this.whName = whName;
    }

    public String getEstimateLane() {
        return estimateLane;
    }

    public void setEstimateLane(String estimateLane) {
        this.estimateLane = estimateLane;
    }

    public String getTransportTime() {
        return transportTime;
    }

    public void setTransportTime(String transportTime) {
        this.transportTime = transportTime;
    }

    public String getDestAddress() {
        return destAddress;
    }

    public void setDestAddress(String destAddress) {
        this.destAddress = destAddress;
    }

    public String getVehiclePlate() {
        return vehiclePlate;
    }

    public void setVehiclePlate(String vehiclePlate) {
        this.vehiclePlate = vehiclePlate;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getLogiPerson() {
        return logiPerson;
    }

    public void setLogiPerson(String logiPerson) {
        this.logiPerson = logiPerson;
    }

    public String getLogiTel() {
        return logiTel;
    }

    public void setLogiTel(String logiTel) {
        this.logiTel = logiTel;
    }

    public String getDriver() {
        return driver;
    }

    public void setDriver(String driver) {
        this.driver = driver;
    }

    public String getDriverTel() {
        return driverTel;
    }

    public void setDriverTel(String driverTel) {
        this.driverTel = driverTel;
    }

    public String getDealeName() {
        return dealeName;
    }

    public void setDealeName(String dealeName) {
        this.dealeName = dealeName;
    }

    public String getLogiName() {
        return logiName;
    }

    public void setLogiName(String logiName) {
        this.logiName = logiName;
    }

    public List<HandoverDetailDTO> getDetailList() {
        return detailList;
    }

    public void setDetailList(List<HandoverDetailDTO> detailList) {
        this.detailList = detailList;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public Integer getStandardNum() {
        return standardNum;
    }

    public void setStandardNum(Integer standardNum) {
        this.standardNum = standardNum;
    }

    public Integer getAllNum() {
        return allNum;
    }

    public void setAllNum(Integer allNum) {
        this.allNum = allNum;
    }

    public String getCollectUnit() {
        return collectUnit;
    }

    public void setCollectUnit(String collectUnit) {
        this.collectUnit = collectUnit;
    }
}
