package com.unlcn.ils.wms.backend.enums;

/**
 * Created by houjianhui on 2017/6/10.
 */
public enum WayBillPickUpStatusEnum {

    WAYBILL_INIT(10, "未提车"),
    WAYBILL_ALREADY(20, "已提车"),
    WAYBILL_CANCEL(30, "取消提车");

    private final int value;
    private final String text;

    WayBillPickUpStatusEnum(int value, String text) {
        this.value = value;
        this.text = text;
    }

    public int getValue() {
        return value;
    }

    public String getText() {
        return text;
    }

    public static WayBillPickUpStatusEnum getByValue(int value){
        for (WayBillPickUpStatusEnum temp : WayBillPickUpStatusEnum.values()) {
            if(temp.getValue() == value){
                return temp;
            }
        }
        return null;
    }
}
