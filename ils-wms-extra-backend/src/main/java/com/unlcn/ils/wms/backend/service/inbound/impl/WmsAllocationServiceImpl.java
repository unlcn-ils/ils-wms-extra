package com.unlcn.ils.wms.backend.service.inbound.impl;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.unlcn.ils.wms.backend.bo.inboundBO.WmsInboundAllocationBO;
import com.unlcn.ils.wms.backend.bo.inboundBO.WmsInboundOrderBO;
import com.unlcn.ils.wms.backend.service.inbound.WmsAllocationService;
import com.unlcn.ils.wms.backend.util.ObjectToMapUtils;
import com.unlcn.ils.wms.base.businessDTO.inbound.WmsInboundAllocationQueryDTO;
import com.unlcn.ils.wms.base.mapper.additional.WmsAllocationExtMapper;
import com.unlcn.ils.wms.base.mapper.additional.WmsInboundAllocationExtMapper;
import com.unlcn.ils.wms.base.mapper.additional.WmsInboundAsnOrderAddMapper;
import com.unlcn.ils.wms.base.mapper.inbound.WmsInboundAllocationMapper;
import com.unlcn.ils.wms.base.mapper.inbound.WmsInboundOrderDetailMapper;
import com.unlcn.ils.wms.base.model.inbound.WmsInboundAllocation;
import com.unlcn.ils.wms.base.model.inbound.WmsInboundOrderDetail;
import org.apache.commons.collections4.CollectionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;
import java.util.Objects;

/**
 * 分配service
 * Created by DELL on 2017/8/8.
 */
@Service
public class WmsAllocationServiceImpl implements WmsAllocationService {
    private Logger LOGGER = LoggerFactory.getLogger(AsnOrderServiceImpl.class);
    @Autowired
    private WmsInboundAllocationMapper wmsInboundAllocationMapper;
    @Autowired
    private WmsAllocationExtMapper wmsAllocationExtMapper;
    @Autowired
    private WmsInboundAllocationExtMapper wmsInboundAllocationExtMapper;
    @Autowired
    private WmsInboundOrderDetailMapper wmsInboundOrderDetailMapper;
    @Autowired
    private WmsInboundAsnOrderAddMapper wmsInboundAsnOrderAddMapper;

    @Override
    public void addAllocation(WmsInboundAllocation wmsInboundAlloc) {

    }

    @Override
    public void updateAllocationByAsnId(List<WmsInboundOrderBO> idList, String status) {
        for (WmsInboundOrderBO wmsInboundOrderBO : idList) {
            Map<String, Object> paramMap = Maps.newHashMap();
            paramMap.put("alStatus", status);
            paramMap.put("alAsnOrderId", String.valueOf(wmsInboundOrderBO.getOdId()));
            wmsAllocationExtMapper.updateStatusByAsnOrderId(paramMap);
        }
    }

    @Override
    public void deleteAllocationByAsnId(String asnId) {

    }

    @Override
    public void updateAllocationByOdId(WmsInboundAllocationBO wmsInboundAllocationBO) {
        com.unlcn.ils.wms.base.model.inbound.WmsInboundAllocation wmsInboundAllocation = new WmsInboundAllocation();
        BeanUtils.copyProperties(wmsInboundAllocationBO, wmsInboundAllocation);
        WmsInboundOrderDetail wmsInboundOrderDetail = new WmsInboundOrderDetail();
        wmsInboundOrderDetail.setOddOdId(Long.valueOf(wmsInboundAllocationBO.getAlOdId()));
        wmsInboundOrderDetail.setOddWhZoneCode(wmsInboundAllocationBO.getAlSourceZoneCode());
        wmsInboundOrderDetail.setOddWhZoneName(wmsInboundAllocationBO.getAlSourceZoneName());
        wmsInboundOrderDetail.setOddWhLocCode(wmsInboundAllocationBO.getAlSourceLocCode());
        wmsInboundOrderDetail.setOddWhLocName(wmsInboundAllocationBO.getAlSourceLocName());
        wmsInboundAsnOrderAddMapper.updateOrderDetailByPrimaryKeySelective(wmsInboundOrderDetail);
        wmsAllocationExtMapper.updateAllocationByOdIdOrWaybillNo(wmsInboundAllocation);
    }

    @Override
    public Map<String, Object> selectStockMoveRecord(WmsInboundAllocationBO wmsInboundAllocationBO) throws IllegalAccessException {
        if (Objects.equals(wmsInboundAllocationBO, null)) {
            LOGGER.info("AsnOrderServiceImpl.wmsInboundOrderBO pageVo must not be null");
            throw new IllegalArgumentException("分页参数不能为空");
        }
        WmsInboundAllocationQueryDTO wmsInboundAllocationQueryDTO = new WmsInboundAllocationQueryDTO();

        wmsInboundAllocationQueryDTO.setLimitStart(wmsInboundAllocationQueryDTO.getStartIndex());
        wmsInboundAllocationQueryDTO.setLimitEnd(wmsInboundAllocationQueryDTO.getPageSize());

        Map<String, Object> resultMap = Maps.newHashMap();

        Map<String, Object> paramMap = ObjectToMapUtils.objectToMap(wmsInboundAllocationBO);
        System.out.println("===============paramMap=" + paramMap);

        List<WmsInboundAllocation> wmsInboundAllocationList = wmsInboundAllocationExtMapper.queryForList(paramMap);
        List<WmsInboundAllocationBO> bos = Lists.newArrayList();
        if (CollectionUtils.isNotEmpty(wmsInboundAllocationList)) {
            wmsInboundAllocationList.stream().forEach(v -> {
                WmsInboundAllocationBO bo = new WmsInboundAllocationBO();
                BeanUtils.copyProperties(v, bo);
                bos.add(bo);
            });
            resultMap.put("wmsInboundAllocationBOList", bos);
        }
        wmsInboundAllocationQueryDTO.setPageNo(wmsInboundAllocationQueryDTO.getStartIndex());
        wmsInboundAllocationQueryDTO.setPageSize(wmsInboundAllocationQueryDTO.getPageSize());
        wmsInboundAllocationQueryDTO.setTotalRecord(wmsInboundAllocationExtMapper.queryForCount(paramMap));
        resultMap.put("wmsInboundAllocationQueryInfos", wmsInboundAllocationQueryDTO);
        return resultMap;
    }

}
