package com.unlcn.ils.wms.backend.enums;

import java.util.Objects;

public enum WmsFetchFromTmsTypeEnum {
    INBOUND("I", "入库指令"),
    OUTBOUND("O", "出库指令");

    private final String value;
    private final String text;

    WmsFetchFromTmsTypeEnum(String value, String text) {
        this.value = value;
        this.text = text;
    }

    public String getValue() {
        return value;
    }

    public String getText() {
        return text;
    }

    public static WmsFetchFromTmsTypeEnum getByValue(String value) {
        for (WmsFetchFromTmsTypeEnum temp : WmsFetchFromTmsTypeEnum.values()) {
            if (Objects.equals(temp.getValue(), value)) {
                return temp;
            }
        }
        return null;
    }

}
