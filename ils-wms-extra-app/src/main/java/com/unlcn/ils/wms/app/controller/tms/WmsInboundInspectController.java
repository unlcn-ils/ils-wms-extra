package com.unlcn.ils.wms.app.controller.tms;

import cn.huiyunche.commons.domain.ResultDTO;
import cn.huiyunche.commons.exception.BusinessException;
import com.unlcn.ils.wms.app.vo.tms.TmsInspectExcpVO;
import com.unlcn.ils.wms.app.vo.tms.WmsInboundInspectVO;
import com.unlcn.ils.wms.backend.bo.inspectAppBO.TmsInspectExcpBO;
import com.unlcn.ils.wms.backend.service.inspectApp.TmsInspectComponentMissService;
import com.unlcn.ils.wms.backend.service.inspectApp.TmsInspectExcpService;
import com.unlcn.ils.wms.backend.service.inspectApp.TmsInspectService;
import com.unlcn.ils.wms.base.dto.WmsInspectForAppDTO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.Map;
import java.util.Objects;

/**
 * 入库扫码验车
 *
 * @author user
 */
@Controller
@RequestMapping("/warehousing")
public class WmsInboundInspectController {

    private Logger logger = LoggerFactory.getLogger(WmsInboundInspectController.class);

    @Autowired
    private TmsInspectService tmsInspectService;
    @Autowired
    private TmsInspectExcpService tmsInspectExcpService;
    @Autowired
    private TmsInspectComponentMissService tmsInspectComponentMissService;


    /**
     * 查询验车单信息及区域异常信息
     * <p>
     * 2018-1-31 新需求:业务需要支持车架号模糊匹配.(新增定时任务抓取数据和tms调用wms接口进行入库车辆信息更新)
     * 在进行模糊查询车架号的时候  需要判断类型 获取数据的流程-->order-->rest方式修改-->http
     * 2018-4-19 入库通知单重构   此新方法
     * 2018-5-8 异常项目按照仓库code区分
     * </p>
     *
     * @return 返回值
     */
    @RequestMapping(value = "/fetchInspectBillAndPositionExcpCount", method = RequestMethod.POST)
    @ResponseBody
    public ResultDTO<Object> fetchInspectBill(WmsInspectForAppDTO appDTO) {
        logger.info("WmsInboundInspectController.fetchInspectBill param:{}", appDTO);
        ResultDTO<Object> result = new ResultDTO<>(true, null, "查询成功");
        try {
            Map<String, Object> resultMap = tmsInspectService.updateFetchInspectBill(appDTO);
            result.setData(resultMap);
        } catch (BusinessException e) {
            logger.error("WmsInboundInspectController.fetchInspectBill BusinessException: {}", e);
            result.setMessage(e.getMessage());
            result.setSuccess(false);
            result.setData(null);
            //throw new BusinessException(e.getMessage());
        } catch (Exception ex) {
            result.setMessage(ex.getMessage());
            result.setSuccess(false);
            result.setData(null);
            logger.error("WmsInboundInspectController.fetchInspectBill error: {}", ex);
            //throw new BusinessException("接口内部异常!");
        }
        return result;
    }


    /**
     * 列表展示车辆九大位置信息
     * <p>
     * 2018-5-8 异常项目按照仓库code区分
     * </p>
     *
     * @param appDTO 异常位置id
     * @return 返回值
     */
    @RequestMapping(value = "/getInspectExcpList", method = RequestMethod.GET)
    @ResponseBody
    public ResultDTO<Object> getInspectExcpList(WmsInspectForAppDTO appDTO) {
        logger.info("WmsInboundInspectController.getInspectExcpList param:{}.", appDTO);
        ResultDTO<Object> result = new ResultDTO<>(true, null, "查询成功");
        try {
            result.setData(tmsInspectExcpService.getInspectExcpList(appDTO));
        } catch (BusinessException e) {
            logger.error("WmsInboundInspectController.getInspectExcpList error: {},{}.", e);
            result.setMessage(e.getMessage());
            result.setSuccess(false);
            result.setData(null);
        } catch (Exception e) {
            logger.error("WmsInboundInspectController.getInspectExcpList error: {},{}.", e);
            result.setMessage("系统异常");
            result.setSuccess(false);
            result.setData(null);
        }
        return result;
    }


    /**
     * 获取区域的异常信息
     * <p>
     * 2018-5-8 异常项目按照仓库code区分
     * </p>
     *
     * @param vo 传入异常位置positionId/验车单id
     * @return 返回值
     */
    @RequestMapping(value = "/getInspectExcpListByPositionId", method = RequestMethod.POST)
    @ResponseBody
    public ResultDTO<Object> getInspectExcpListByPositionId(TmsInspectExcpVO vo) {
        logger.info("WmsInboundInspectController.getInspectExcpListByPositionId param:{}.", vo);
        ResultDTO<Object> result = new ResultDTO<>(true, null, "查询成功");
        try {
            TmsInspectExcpBO bo = new TmsInspectExcpBO();
            BeanUtils.copyProperties(vo, bo);
            result.setData(tmsInspectExcpService.getInspectExcpListByPositionId(bo));
        } catch (BusinessException e) {
            logger.error("WmsInboundInspectController.getInspectExcpListByPositionId BusinessException: {}.", e);
            result.setMessage(e.getMessage());
            result.setSuccess(false);
            result.setData(null);
        } catch (Exception e) {
            logger.error("WmsInboundInspectController.getInspectExcpListByPositionId error: {}.", e);
            result.setMessage("系统异常");
            result.setSuccess(false);
            result.setData(null);
        }
        return result;
    }

    /**
     * 标记异常
     * <p>
     * 2018-1-29 新需求:重庆库需支持自定义异常选项
     * 2018-5-9 增加仓库code参数区分异常类型
     * </p>
     *
     * @param vo 参数封装
     * @return 返回值
     */
    @RequestMapping(value = "/noteInspectExcp", method = RequestMethod.POST)
    @ResponseBody
    public ResultDTO<Object> noteInspectExcp(TmsInspectExcpVO vo) {
        logger.info("WmsInboundInspectController.noteInspectExcp param:{}.", vo);
        ResultDTO<Object> result = new ResultDTO<>(true, null, "保存成功");
        try {
            TmsInspectExcpBO bo = new TmsInspectExcpBO();
            BeanUtils.copyProperties(vo, bo);
            tmsInspectExcpService.saveNoteExcp(bo);
        } catch (BusinessException e) {
            logger.error("WmsInboundInspectController.noteInspectExcp BusinessException: {}.", e);
            result.setMessage(e.getMessage());
            result.setSuccess(false);
            result.setData(null);
        } catch (Exception e) {
            logger.error("WmsInboundInspectController.noteInspectExcp error: {}.", e);
            result.setMessage("系统异常");
            result.setSuccess(false);
            result.setData(null);
        }
        return result;
    }

    /**
     * 验车登记  对应状态的提车单
     * 异常的话--->锁定异常状态
     * 正常的--->合格
     *
     * @param inspectId 运单id
     * @param userId    操作人id
     * @return result  返回值
     */
    @RequestMapping(value = "/inspectRegister", method = RequestMethod.POST)
    @ResponseBody
    public ResultDTO<Object> inspectRegister(Long inspectId, String userId) {
        logger.info("WmsInboundInspectController.inspectRegister param: {}", inspectId, userId);
        ResultDTO<Object> result = new ResultDTO<>(true, null, "登记成功");
        try {
            tmsInspectService.updateInspectStatusById(inspectId, userId);
        } catch (BusinessException e) {
            logger.error("WmsInboundInspectController.inspectRegister error: {},{}.", e);
            result.setMessage(e.getMessage());
            result.setSuccess(false);
            result.setData(null);
        } catch (Exception e) {
            logger.error("WmsInboundInspectController.inspectRegister error: {},{}.", e);
            result.setMessage("系统异常");
            result.setSuccess(false);
            result.setData(null);
        }
        return result;
    }

    /**
     * 取消验车  对应状态的提车单
     *
     * @param inspectId 运单id
     * @param userId    操作人id
     * @return result 返回值
     */
    @RequestMapping(value = "/inspectCancel", method = RequestMethod.POST)
    @ResponseBody
    public ResultDTO<Object> inspectCancel(Long inspectId, String userId) {
        logger.info("WmsInboundInspectController.inspectCancel param: {}", inspectId, userId);
        ResultDTO<Object> result = new ResultDTO<>(true, null, "取消成功!");
        try {
            tmsInspectService.updateToInspectCancel(inspectId, userId);
        } catch (BusinessException e) {
            logger.error("WmsInboundInspectController.inspectCancel error: {},{}.", e);
            result.setMessage(e.getMessage());
            result.setSuccess(false);
            result.setData(null);
        } catch (Exception e) {
            logger.error("WmsInboundInspectController.inspectCancel error: {},{}.", e);
            result.setMessage("系统异常");
            result.setSuccess(false);
            result.setData(null);
        }
        return result;
    }

    /**
     * 获取所有的部件列表,并标注为是否缺失
     * <p>
     * 2018-5-9 缺件数据按照仓库code区分
     * </p>
     */
    @RequestMapping(value = "/getLackOfPartsList", method = RequestMethod.GET)
    @ResponseBody
    public ResultDTO<Object> getComponentMissList(WmsInspectForAppDTO dto) {
        logger.info("WmsInboundInspectController.getComponentMissList param dto:{}", dto);
        ResultDTO<Object> result = new ResultDTO<>(true, null, "查询缺件列表成功!");
        try {
            if (dto == null) {
                throw new BusinessException("参数不能为空");
            }
            result.setData(tmsInspectComponentMissService.getComponentMissListNew(dto.getVin(), dto.getWhCode()));
        } catch (BusinessException e) {
            logger.error("WmsInboundInspectController.getComponentMissList BusinessException: {}.", e);
            result.setMessage(e.getMessage());
            result.setSuccess(false);
            result.setData(null);
        } catch (Exception e) {
            logger.error("WmsInboundInspectController.getComponentMissList error: {}.", e);
            result.setMessage("系统异常");
            result.setSuccess(false);
            result.setData(null);
        }
        return result;
    }


    /**
     * 标记缺件异常
     * <p>
     * 2015-5-11 异常类型按照仓库code区分
     * </p>
     *
     * @return result
     */
    @RequestMapping(value = "/saveLackParts", method = RequestMethod.POST)
    @ResponseBody
    public ResultDTO<Object> saveComponentMiss(WmsInspectForAppDTO dto) {
        logger.info("WmsInboundInspectController.saveComponentMiss param {}", dto);
        ResultDTO<Object> result = new ResultDTO<>(true, null, "标记缺件异常成功!");
        try {
            tmsInspectComponentMissService.saveComponentMiss(dto);
        } catch (BusinessException e) {
            logger.error("WmsInboundInspectController.saveComponentMiss error: {},{}.", e);
            result.setMessage(e.getMessage());
            result.setSuccess(false);
            result.setData(null);
        } catch (Exception e) {
            logger.error("WmsInboundInspectController.saveComponentMiss error: {},{}.", e);
            result.setMessage("系统异常");
            result.setSuccess(false);
            result.setData(null);
        }
        return result;
    }


    /**
     * <p>
     * 2018-5-11 新增一键删除异常接口
     * </p>
     *
     * @param dto 参数封装
     * @return 统一返回结果
     */
    @RequestMapping("/deleteAllExp")
    @ResponseBody
    public ResultDTO<Object> deleteAllException(WmsInspectForAppDTO dto) {
        if (logger.isInfoEnabled()) {
            logger.info("WmsInboundInspectController.deleteAllException param:{}", dto);
        }
        ResultDTO<Object> result = new ResultDTO<>(true, null, "操作成功!");
        try {
            tmsInspectExcpService.deleteAllException(dto);
        } catch (BusinessException e) {
            logger.error("WmsInboundInspectController.deleteAllException BusinessException:{}", e);
            result.setMessage(e.getMessage());
            result.setSuccess(false);
        } catch (Exception e) {
            logger.error("WmsInboundInspectController.deleteAllException error:", e);
            result.setMessage("操作异常");
            result.setSuccess(false);
        }
        return result;
    }


    /**
     * 查询验车单信息及区域异常信息
     * <p>
     * 2018-1-31 新需求:业务需要支持车架号模糊匹配.(新增定时任务抓取数据和tms调用wms接口进行入库车辆信息更新)
     * 在进行模糊查询车架号的时候  需要判断类型 获取数据的流程-->order-->rest方式修改-->http
     * 2018-4-19 入库通知单重构   此方法过时，新方法 {@link WmsInboundInspectController#fetchInspectBill}
     * </p>
     *
     * @param vo 参数封装
     * @return 返回值
     */
    @RequestMapping(value = "/fetchInspectBillAndPositionExcpCount-Old", method = RequestMethod.POST)
    @ResponseBody
    @Deprecated
    public ResultDTO<Object> fetchInspectBillOld(WmsInboundInspectVO vo) {
        logger.info("WmsInboundInspectController.fetchInspectBillOld param vo:{}", vo);
        ResultDTO<Object> result = new ResultDTO<>(true, null, "查询运单数据成功");
        try {
            if (Objects.equals(vo, null)) {
                throw new BusinessException("传入数据为空!");
            }
            String vin = vo.getVin();
            Map<String, Object> resultMap = tmsInspectService.updateFetchInspectBillOld(vin, vo.getWhCode(), vo.getWhName());
            result.setData(resultMap);
        } catch (BusinessException e) {
            logger.error("WmsInboundInspectController.fetchInspectBillOld BusinessException: {}", e);
            result.setMessage(e.getMessage());
            result.setSuccess(false);
            result.setData(null);
            //throw new BusinessException(e.getMessage());
        } catch (Exception ex) {
            result.setMessage(ex.getMessage());
            result.setSuccess(false);
            result.setData(null);
            logger.error("WmsInboundInspectController.fetchInspectBillOld error: {}", ex);
            //throw new BusinessException("接口内部异常!");
        }
        return result;
    }
}
