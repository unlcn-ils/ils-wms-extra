package com.unlcn.ils.wms.backend.service.webservice.client;

import com.unlcn.ils.wms.base.model.junmadcs.WmsHandoverOrder;

import java.util.List;

public interface WmsJmCRMService {

    void saveHandoverOrderToCRM(List<WmsHandoverOrder> handoverOrderList) throws Exception;
}
