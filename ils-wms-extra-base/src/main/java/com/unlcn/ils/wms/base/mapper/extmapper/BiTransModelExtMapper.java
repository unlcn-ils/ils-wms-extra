package com.unlcn.ils.wms.base.mapper.extmapper;

import com.unlcn.ils.wms.base.model.bigscreen.BiPickData;
import com.unlcn.ils.wms.base.model.bigscreen.BiTransModel;
import com.unlcn.ils.wms.base.model.bigscreen.BiTransModelExample;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface BiTransModelExtMapper {
    /**
     * 查询发运模式推移图数据
     * @return
     */
    List<BiTransModel> transModelChartCount();
}