package com.unlcn.ils.wms.base.dto;

/**
 * Created by lenovo on 2017/11/12.
 */
public class InvoicingStatisticsDTO {
    // 时间段
    private String timeSlot;

    // 车型
    private String vehicleType;

    // 数量
    private Integer num;

    public String getTimeSlot() {
        return timeSlot;
    }

    public void setTimeSlot(String timeSlot) {
        this.timeSlot = timeSlot;
    }

    public String getVehicleType() {
        return vehicleType;
    }

    public void setVehicleType(String vehicleType) {
        this.vehicleType = vehicleType;
    }

    public Integer getNum() {
        return num;
    }

    public void setNum(Integer num) {
        this.num = num;
    }

    @Override
    public String toString() {
        return "InvoicingStatisticsDTO{" +
                "timeSlot='" + timeSlot + '\'' +
                ", vehicleType='" + vehicleType + '\'' +
                ", num=" + num +
                '}';
    }
}
