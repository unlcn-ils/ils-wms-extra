package com.unlcn.ils.wms.backend.enums;

public enum WebServiceWhcodeEnum  {
    CS01("CS01","君马长沙库"),
    XY01("XY01","君马襄阳库");

    private final String value;
    private final String text;

    WebServiceWhcodeEnum(String value, String text) {
        this.value = value;
        this.text = text;
    }

    /**
     * @return the value
     */
    public String getValue() {
        return value;
    }

    /**
     * @return the text
     */
    public String getText() {
        return text;
    }

    /**
     *
     * @Title: getByValue
     * @Description: 获取值
     * @param @param value
     * @param @return
     * @return StatusEnum    返回类型
     * @throws
     *
     */
    public static WebServiceWhcodeEnum getByValue(String value) {
        for (WebServiceWhcodeEnum temp : WebServiceWhcodeEnum.values()) {
            if (temp.getValue().equals(value)) {
                return temp;
            }
        }
        return null;
    }
}
