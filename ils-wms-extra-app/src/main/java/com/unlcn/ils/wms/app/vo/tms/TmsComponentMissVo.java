package com.unlcn.ils.wms.app.vo.tms;

import java.io.Serializable;

public class TmsComponentMissVo implements Serializable {
    private Long componentId;
    private String vin;

    public Long getComponentId() {
        return componentId;
    }

    public void setComponentId(Long componentId) {
        this.componentId = componentId;
    }

    public String getVin() {
        return vin;
    }

    public void setVin(String vin) {
        this.vin = vin;
    }
}
