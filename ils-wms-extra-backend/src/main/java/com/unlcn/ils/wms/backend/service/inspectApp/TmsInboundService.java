package com.unlcn.ils.wms.backend.service.inspectApp;

import cn.huiyunche.commons.domain.PageVo;
import com.unlcn.ils.wms.backend.bo.inspectAppBO.TmsInboundMoveCarBO;
import com.unlcn.ils.wms.backend.bo.inspectAppBO.WmsInboundConfirmBO;
import com.unlcn.ils.wms.base.dto.WmsInboundNotMoveCarDTO;
import com.unlcn.ils.wms.base.dto.WmsWaybillInfoParamDTO;

import java.util.List;
import java.util.Map;

/**
 * @date 2017/9/27
 * @desc 入库业务功能
 **/
public interface TmsInboundService {

    Map<String, Object> getInboundInspectInfo(WmsWaybillInfoParamDTO paramDTO) throws Exception;

    TmsInboundMoveCarBO updateMoveCar(String waybillCode, Long vin, String userId) throws Exception;

    TmsInboundMoveCarBO saveWareHousingConfirm(WmsInboundConfirmBO bo) throws Exception;

    List<WmsInboundNotMoveCarDTO> getNotMoveCarList(String whcode, PageVo vo) throws Exception;

    void fetchInboundOrderByScheduleOld() throws Exception;

    void fetchInboundOrderBySchedule();

}
