package com.unlcn.ils.wms.base.dto;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by lenovo on 2017/10/26.
 */
public class InboundInfoDTO {
    // 底盘号
    private String vin;

    // 创建时间
    private Date gmtCreate;

    // 入库仓库
    private String warehousingWarehouse;

    // 入库人
    private String storagePerson;


    public String getVin() {
        return vin;
    }

    public void setVin(String vin) {
        this.vin = vin;
    }

    public Date getGmtCreate() {
        return gmtCreate;
    }

    public void setGmtCreate(Date gmtCreate) {
        this.gmtCreate = gmtCreate;
    }

    public String getWarehousingWarehouse() {
        return warehousingWarehouse;
    }

    public void setWarehousingWarehouse(String warehousingWarehouse) {
        this.warehousingWarehouse = warehousingWarehouse;
    }

    public String getStoragePerson() {
        return storagePerson;
    }

    public void setStoragePerson(String storagePerson) {
        this.storagePerson = storagePerson;
    }

    @Override
    public String toString() {
        return "InboundInfoDTO{" +
                "vin='" + vin + '\'' +
                ", gmtCreate=" + gmtCreate +
                ", warehousingWarehouse='" + warehousingWarehouse + '\'' +
                ", storagePerson='" + storagePerson + '\'' +
                '}';
    }
}
