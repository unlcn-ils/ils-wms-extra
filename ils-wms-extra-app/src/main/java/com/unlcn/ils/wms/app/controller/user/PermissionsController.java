package com.unlcn.ils.wms.app.controller.user;

import cn.huiyunche.commons.domain.ResultDTO;
import cn.huiyunche.commons.exception.BusinessException;
import com.unlcn.ils.wms.app.vo.wms.PermissionsVO;
import com.unlcn.ils.wms.backend.bo.sys.PermissionsBO;
import com.unlcn.ils.wms.backend.service.sys.PermissionsService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * 
 * @ClassName: PermissionsController 
 * @Description: 权限
 * @author laishijian
 * @date 2017年8月8日 下午3:47:37 
 *
 */
@Controller
@RequestMapping("/permissions")
@ResponseBody
public class PermissionsController {
	
	private Logger LOGGER = LoggerFactory.getLogger(PermissionsController.class);
	
	@Autowired
	private PermissionsService permissionsService;
	
	/**
	 * 
	 * @Title: add 
	 * @Description: 新增权限
	 * @param @param permissionsVO
	 * @param @return     
	 * @return ResultDTO<Object>    返回类型 
	 * @throws 
	 *
	 */
	@RequestMapping(value="/add",method=RequestMethod.POST)
	public ResultDTO<Object> add(@RequestBody PermissionsVO permissionsVO){
		
		LOGGER.info("PermissionsController.add param : {}", permissionsVO);
		
		ResultDTO<Object> result = new ResultDTO<>(true);
		try {
			
			PermissionsBO permissionsBO = new PermissionsBO();
			BeanUtils.copyProperties(permissionsVO, permissionsBO);

			permissionsService.add(permissionsBO);
			
			result.setMessage("新增权限成功");
			
		}catch(BusinessException be) {
			result.setSuccess(false);
			result.setMessage(be.getMessage());
			LOGGER.error("PermissionsController.add error: {}", be);
		}catch(Exception e) {
			result.setSuccess(false);
			result.setMessage("系统异常");
			LOGGER.error("PermissionsController.add error: {}", e);
		}
		return result;
	}
	
	
}
