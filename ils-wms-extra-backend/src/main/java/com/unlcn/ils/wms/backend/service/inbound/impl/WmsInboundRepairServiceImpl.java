package com.unlcn.ils.wms.backend.service.inbound.impl;

import cn.huiyunche.commons.exception.BusinessException;
import com.unlcn.ils.wms.backend.enums.OrderHeadEnum;
import com.unlcn.ils.wms.backend.service.inbound.WmsInboundRepairService;
import com.unlcn.ils.wms.backend.util.DateUtils;
import com.unlcn.ils.wms.base.mapper.inbound.WmsInboundRepairMapper;
import com.unlcn.ils.wms.base.model.inbound.WmsInboundRepair;
import com.unlcn.ils.wms.base.model.inbound.WmsInboundRepairExample;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @Auther linbao
 * @Date 2017-10-24
 */
@Service
public class WmsInboundRepairServiceImpl implements WmsInboundRepairService {

    private Logger LOGGER = LoggerFactory.getLogger(WmsInboundRepairServiceImpl.class);

    @Autowired
    private WmsInboundRepairMapper inboundRepairMapper;

    /**
     * 根据车架号获取维修单信息
     *
     * @param vin
     * @return
     * @throws Exception
     */
    @Override
    public WmsInboundRepair getWmsInboundRepairByVin(String vin) throws Exception {
        LOGGER.info("WmsInboundRepairServiceImpl.getWmsInboundRepairByVin vin: {}", vin);
        if (StringUtils.isNotBlank(vin)) {

            WmsInboundRepairExample example = new WmsInboundRepairExample();
            example.createCriteria().andRpVinEqualTo(vin);

            List<WmsInboundRepair> inboundRepairList = inboundRepairMapper.selectByExample(example);
            if (CollectionUtils.isNotEmpty(inboundRepairList)) {
                return inboundRepairList.get(0);
            }
        }
        return null;
    }

    /**
     * 根据维修单号获取对应的维修单集合
     *
     * @param repairNo
     * @return
     * @throws Exception
     */
    @Override
    public List<WmsInboundRepair> getRepairListByRepairNo(String repairNo) throws Exception {
        LOGGER.info("WmsInboundRepairServiceImpl.getRepairListByRepairNo repairNo: {}", repairNo);
        if (StringUtils.isNotBlank(repairNo)) {

            WmsInboundRepairExample example = new WmsInboundRepairExample();
            example.createCriteria().andRpRepairNoEqualTo(repairNo);
            return inboundRepairMapper.selectByExample(example);
        }
        return null;
    }

    /**
     * 根据车架号更新出库状态
     *
     * @param vin
     * @param status
     * @throws Exception
     */
    @Override
    public void updateInOutStatusByVin(String vin, String status) throws Exception {
        LOGGER.info("WmsInboundRepairServiceImpl.updateInOutStatusByVin vin: {}", vin);
        if (StringUtils.isNotBlank(vin) && StringUtils.isNotBlank(status)) {
            WmsInboundRepair repair = new WmsInboundRepair();
            repair.setRpInOutStatus(status);

            WmsInboundRepairExample example = new WmsInboundRepairExample();
            example.createCriteria().andRpVinEqualTo(vin);
            inboundRepairMapper.selectByExample(example);
        }
    }

    /**
     * 新增维修单
     *
     * @param wmsInboundRepair
     * @throws Exception
     */
    @Override
    public void addInboundRepair(WmsInboundRepair wmsInboundRepair) throws Exception {
        LOGGER.info("WmsInboundRepairServiceImpl.addInboundRepair wmsInboundRepair: {}", wmsInboundRepair);
        if (wmsInboundRepair != null) {
            inboundRepairMapper.insertSelective(wmsInboundRepair);
            String repairNo = getRepairNo(wmsInboundRepair.getRpId());
            if (StringUtils.isNotBlank(repairNo)) {
                wmsInboundRepair.setRpRepairNo(repairNo);
                inboundRepairMapper.updateByPrimaryKeySelective(wmsInboundRepair);
            }
        }
    }

    /**
     * 生成维修单号
     *
     * @param id
     * @return
     */
    private String getRepairNo(Long id) {
        //OrderHeadEnum
        if (id == null) {
            throw new BusinessException("生成维修单号错误");
        }
        StringBuilder repairNo = new StringBuilder(OrderHeadEnum.REPIR.getCode());
        String currentZeroDate = DateUtils.getCurrentZeroDate(DateUtils.FORMAT_YYYYMMDD);
        repairNo.append(currentZeroDate);
        repairNo.append(id);
        return repairNo.toString();
    }
}
