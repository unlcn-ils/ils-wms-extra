package com.unlcn.ils.wms.backend.bo.inspectAppBO;

import com.unlcn.ils.wms.base.dto.TmsInspectExcpDTO;

import java.io.Serializable;
import java.util.Date;
import java.util.List;


public class TmsInboundInspectBO implements Serializable {
    private Long id;
    private String orderno;
    private String custOrderno;
    private String custShipno;
    private String waybillCode;
    private String customer;
    private String origin;
    private String dest;
    private String vin;
    private String engine;
    private String style;
    private String styleDesc;
    private Integer inspectStatusValue;
    private String inspectUserId;
    private String shipno;
    private String supplier;//分供方名称对应交货方名称
    private String vehicle;//板车车牌号
    private Date gmtCreate;
    private Date gmtUpdate;
    private String inspectStatusText;
    private String factory_whno;
    private String styleId;//车型id
    private List<TmsInspectExcpDTO> positionAndExcpCount;
    private String missComponentNames;
    private String rpInAndOutType;//维修出入库类型
    private String rpId;
    private String stocktransfer;  //是否移库单标识

    public String getStocktransfer() {
        return stocktransfer;
    }

    public void setStocktransfer(String stocktransfer) {
        this.stocktransfer = stocktransfer;
    }

    public String getRpId() {
        return rpId;
    }

    public void setRpId(String rpId) {
        this.rpId = rpId;
    }

    public String getRpInAndOutType() {
        return rpInAndOutType;
    }

    public void setRpInAndOutType(String rpInAndOutType) {
        this.rpInAndOutType = rpInAndOutType;
    }

    public String getFactory_whno() {
        return factory_whno;
    }

    public void setFactory_whno(String factory_whno) {
        this.factory_whno = factory_whno;
    }

    public String getSupplier() {
        return supplier;
    }

    public void setSupplier(String supplier) {
        this.supplier = supplier;
    }

    public String getVehicle() {
        return vehicle;
    }

    public void setVehicle(String vehicle) {
        this.vehicle = vehicle;
    }

    public String getShipno() {
        return shipno;
    }

    public void setShipno(String shipno) {
        this.shipno = shipno;
    }

    public String getMissComponentNames() {
        return missComponentNames;
    }

    public void setMissComponentNames(String missComponentNames) {
        this.missComponentNames = missComponentNames;
    }

    public String getEngine() {
        return engine;
    }

    public void setEngine(String engine) {
        this.engine = engine;
    }

    public String getWaybillCode() {
        return waybillCode;
    }

    public void setWaybillCode(String waybillCode) {
        this.waybillCode = waybillCode;
    }

    public List<TmsInspectExcpDTO> getPositionAndExcpCount() {
        return positionAndExcpCount;
    }

    public void setPositionAndExcpCount(List<TmsInspectExcpDTO> positionAndExcpCount) {
        this.positionAndExcpCount = positionAndExcpCount;
    }

    public Integer getInspectStatusValue() {
        return inspectStatusValue;
    }

    public void setInspectStatusValue(Integer inspectStatusValue) {
        this.inspectStatusValue = inspectStatusValue;
    }

    public String getInspectStatusText() {
        return inspectStatusText;
    }

    public void setInspectStatusText(String inspectStatusText) {
        this.inspectStatusText = inspectStatusText;
    }

    private static final long serialVersionUID = 1L;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getOrderno() {
        return orderno;
    }

    public void setOrderno(String orderno) {
        this.orderno = orderno;
    }

    public String getCustOrderno() {
        return custOrderno;
    }

    public void setCustOrderno(String custOrderno) {
        this.custOrderno = custOrderno;
    }

    public String getCustShipno() {
        return custShipno;
    }

    public void setCustShipno(String custShipno) {
        this.custShipno = custShipno;
    }

    public String getCustomer() {
        return customer;
    }

    public void setCustomer(String customer) {
        this.customer = customer;
    }

    public String getOrigin() {
        return origin;
    }

    public void setOrigin(String origin) {
        this.origin = origin;
    }

    public String getDest() {
        return dest;
    }

    public void setDest(String dest) {
        this.dest = dest;
    }

    public String getVin() {
        return vin;
    }

    public void setVin(String vin) {
        this.vin = vin;
    }

    public String getStyle() {
        return style;
    }

    public void setStyle(String style) {
        this.style = style;
    }

    public String getStyleDesc() {
        return styleDesc;
    }

    public void setStyleDesc(String styleDesc) {
        this.styleDesc = styleDesc;
    }

    public String getInspectUserId() {
        return inspectUserId;
    }

    public void setInspectUserId(String inspectUserId) {
        this.inspectUserId = inspectUserId;
    }

    public Date getGmtCreate() {
        return gmtCreate;
    }

    public void setGmtCreate(Date gmtCreate) {
        this.gmtCreate = gmtCreate;
    }

    public Date getGmtUpdate() {
        return gmtUpdate;
    }

    public void setGmtUpdate(Date gmtUpdate) {
        this.gmtUpdate = gmtUpdate;
    }

    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    public String getStyleId() {
        return styleId;
    }

    public void setStyleId(String styleId) {
        this.styleId = styleId;
    }
}
