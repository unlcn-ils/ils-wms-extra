package com.unlcn.ils.wms.backend.util.webservice;

import org.apache.commons.codec.binary.Base64;
import org.apache.commons.io.IOUtils;

import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;

/**
 * 发送soap报文定时请求数据
 * Created by DELL on 2017/9/20.
 */
public class WebServiceSoapUtils {
    /**
     * 模拟http发送 post请求访问webservice服务并根据传递参数不同返回不同结果
     *
     * @param soapUrl    soap请求的url（从soapUI的request1的RAW标签的POST获取）
     * @param soapHost   soap请求的Host
     * @param soapAction soap请求的Action
     * @param soapXml    soap请求报文内容串
     * @throws Exception
     */
    public static String postSoap(String soapUrl, String soapHost, String soapXml, String soapAction) throws Exception {

        URL url = new URL(soapUrl);
        HttpURLConnection conn = (HttpURLConnection) url.openConnection();
        conn.setDoInput(true);
        conn.setDoOutput(true);
        conn.setUseCaches(false);
        conn.setDefaultUseCaches(false);
        //Host，Content-Type，SOAPAction从soapUI的request1的RAW标签的Host，Content-Typ，SOAPActione获取
        conn.setRequestProperty("Host", soapHost);
        conn.setRequestProperty("Content-Type", "application/json;charset=UTF-8");
        conn.setRequestProperty("Content-Length", String.valueOf(soapXml.length()));
        conn.setRequestProperty("SOAPAction", soapAction);
        conn.setRequestMethod("POST");
        //定义输出流
        OutputStream output = conn.getOutputStream();
        if (null != soapXml) {
            byte[] b = soapXml.getBytes("utf-8");
            //发送soap请求报文
            output.write(b, 0, b.length);
        }
        output.flush();
        output.close();
        //定义输入流，获取soap响应报文
        InputStream input = conn.getInputStream();
        String result = IOUtils.toString(input, "UTF-8");
        input.close();
        return result;
    }


    /**
     * 模拟http发送 post请求访问SAP webservice服务并根据传递参数不同返回不同结果
     *
     * @param soapUrl    soap请求的url（从soapUI的request1的RAW标签的POST获取）
     * @param soapHost   soap请求的Host
     * @param soapAction soap请求的Action
     * @param soapXml    soap请求报文内容串
     */
    public static String postSoapToPi(String soapUrl, String soapHost, String soapXml, String soapAction, String username, String password) throws Exception {

        URL url = new URL(soapUrl);
        HttpURLConnection conn = (HttpURLConnection) url.openConnection();
        conn.setDoInput(true);
        conn.setDoOutput(true);
        conn.setUseCaches(false);
        conn.setDefaultUseCaches(false);
        conn.setInstanceFollowRedirects(true);
        //Host，Content-Type，SOAPAction从soapUI的request1的RAW标签的Host，Content-Typ，SOAPActione获取
        conn.setRequestProperty("Host", soapHost);
        conn.setRequestProperty("Content-Type", "text/xml;charset=UTF-8");
        conn.setRequestProperty("Content-Length", String.valueOf(soapXml.length()));
        conn.setRequestProperty("SOAPAction", soapAction);
        //String username = "hx-mjj";
        //String password = "mjj1234!";
        String token = username + ":" + password;
        String encode = new Base64().encodeToString(token.getBytes());
        //String encode = new BASE64Encoder().encode(token.getBytes());
        conn.setRequestProperty("Authorization", "Basic " + encode);
        conn.setRequestMethod("POST");
        conn.connect();
        //定义输出流
        OutputStream output = conn.getOutputStream();
        if (null != soapXml) {
            byte[] b = soapXml.getBytes("utf-8");
            //发送soap请求报文
            output.write(b, 0, b.length);
        }
        output.flush();
        output.close();
        //定义输入流，获取soap响应报文
        InputStream input = conn.getInputStream();
        String result = IOUtils.toString(input, "UTF-8");
        input.close();
        return result;
    }
}
