package com.unlcn.ils.wms.web.vo.stock;

import java.io.Serializable;

public class WmsVehicleTranckingVO implements Serializable {
    private String orderno;
    private String vin;
    private String orderBegin;
    private String orderEnd;
    private String inboundBegin;
    private String inboundEnd;
    private String pageNo;
    private String pageSize;
    private String whCode;
    private String userId;

    public String getWhCode() {
        return whCode;
    }

    public void setWhCode(String whCode) {
        this.whCode = whCode;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getPageNo() {
        return pageNo;
    }

    public void setPageNo(String pageNo) {
        this.pageNo = pageNo;
    }

    public String getPageSize() {
        return pageSize;
    }

    public void setPageSize(String pageSize) {
        this.pageSize = pageSize;
    }

    public String getOrderno() {
        return orderno;
    }

    public void setOrderno(String orderno) {
        this.orderno = orderno;
    }

    public String getVin() {
        return vin;
    }

    public void setVin(String vin) {
        this.vin = vin;
    }

    public String getOrderBegin() {
        return orderBegin;
    }

    public void setOrderBegin(String orderBegin) {
        this.orderBegin = orderBegin;
    }

    public String getOrderEnd() {
        return orderEnd;
    }

    public void setOrderEnd(String orderEnd) {
        this.orderEnd = orderEnd;
    }

    public String getInboundBegin() {
        return inboundBegin;
    }

    public void setInboundBegin(String inboundBegin) {
        this.inboundBegin = inboundBegin;
    }

    public String getInboundEnd() {
        return inboundEnd;
    }

    public void setInboundEnd(String inboundEnd) {
        this.inboundEnd = inboundEnd;
    }
}
