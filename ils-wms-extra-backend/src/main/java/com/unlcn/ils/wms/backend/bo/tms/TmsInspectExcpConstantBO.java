package com.unlcn.ils.wms.backend.bo.tms;

import java.util.List;

/**
 * @Author ：Ligl
 * @Date : 2017/9/14.
 */
public class TmsInspectExcpConstantBO {

    private Integer code;


    private String name;


    private Integer parentCode;


    private Integer level;


    private Integer sort;


    private Boolean enable;

    private List<TmsInspectExcpConstantBO>  childExcp;

    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getParentCode() {
        return parentCode;
    }

    public void setParentCode(Integer parentCode) {
        this.parentCode = parentCode;
    }

    public Integer getLevel() {
        return level;
    }

    public void setLevel(Integer level) {
        this.level = level;
    }

    public Integer getSort() {
        return sort;
    }

    public void setSort(Integer sort) {
        this.sort = sort;
    }

    public Boolean getEnable() {
        return enable;
    }

    public void setEnable(Boolean enable) {
        this.enable = enable;
    }

    public List<TmsInspectExcpConstantBO> getChildExcp() {
        return childExcp;
    }

    public void setChildExcp(List<TmsInspectExcpConstantBO> childExcp) {
        this.childExcp = childExcp;
    }

    @Override
    public String toString() {
        return "TmsInspectExcpConstantBO{" +
                "code=" + code +
                ", name='" + name + '\'' +
                ", parentCode=" + parentCode +
                ", level=" + level +
                ", sort=" + sort +
                ", enable=" + enable +
                ", childExcp=" + childExcp +
                '}';
    }
}
