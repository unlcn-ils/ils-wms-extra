package com.unlcn.ils.wms.web.vo.inbound;

/**
 * Created by DELL on 2017/9/13.
 */
public class WmsInboundCheckDetailVO {
    /**
     * 主键id
     */
    private Long ckId;

    /**
     * 质检code
     */
    private String ckCode;

    /**
     * 订单id
     */
    private String ckOdId;

    /**
     * 单据号
     */
    private String ckBillNo;

    /**
     * 运单号
     */
    private String ckWaybillNo;

    /**
     * 车型code
     */
    private String ckVehicleSpecCode;

    /**
     * 车型名称
     */
    private String ckVehicleSpecName;

    /**
     * 底盘号
     */
    private String ckVin;

    /**
     * 发动机号
     */
    private String ckEngine;

    /**
     * 质检结果
     */
    private String ckCheckResult;

    /**
     * 质检描述
     */
    private String ckCheckDesc;

    public Long getCkId() {
        return ckId;
    }

    public void setCkId(Long ckId) {
        this.ckId = ckId;
    }

    public String getCkCode() {
        return ckCode;
    }

    public void setCkCode(String ckCode) {
        this.ckCode = ckCode;
    }

    public String getCkOdId() {
        return ckOdId;
    }

    public void setCkOdId(String ckOdId) {
        this.ckOdId = ckOdId;
    }

    public String getCkBillNo() {
        return ckBillNo;
    }

    public void setCkBillNo(String ckBillNo) {
        this.ckBillNo = ckBillNo;
    }

    public String getCkWaybillNo() {
        return ckWaybillNo;
    }

    public void setCkWaybillNo(String ckWaybillNo) {
        this.ckWaybillNo = ckWaybillNo;
    }

    public String getCkVehicleSpecCode() {
        return ckVehicleSpecCode;
    }

    public void setCkVehicleSpecCode(String ckVehicleSpecCode) {
        this.ckVehicleSpecCode = ckVehicleSpecCode;
    }

    public String getCkVehicleSpecName() {
        return ckVehicleSpecName;
    }

    public void setCkVehicleSpecName(String ckVehicleSpecName) {
        this.ckVehicleSpecName = ckVehicleSpecName;
    }

    public String getCkVin() {
        return ckVin;
    }

    public void setCkVin(String ckVin) {
        this.ckVin = ckVin;
    }

    public String getCkEngine() {
        return ckEngine;
    }

    public void setCkEngine(String ckEngine) {
        this.ckEngine = ckEngine;
    }

    public String getCkCheckResult() {
        return ckCheckResult;
    }

    public void setCkCheckResult(String ckCheckResult) {
        this.ckCheckResult = ckCheckResult;
    }

    public String getCkCheckDesc() {
        return ckCheckDesc;
    }

    public void setCkCheckDesc(String ckCheckDesc) {
        this.ckCheckDesc = ckCheckDesc;
    }

    @Override
    public String toString() {
        return "WmsInboundCheckDetailVO{" +
                "ckId=" + ckId +
                ", ckCode='" + ckCode + '\'' +
                ", ckOdId='" + ckOdId + '\'' +
                ", ckBillNo='" + ckBillNo + '\'' +
                ", ckWaybillNo='" + ckWaybillNo + '\'' +
                ", ckVehicleSpecCode='" + ckVehicleSpecCode + '\'' +
                ", ckVehicleSpecName='" + ckVehicleSpecName + '\'' +
                ", ckVin='" + ckVin + '\'' +
                ", ckEngine='" + ckEngine + '\'' +
                ", ckCheckResult='" + ckCheckResult + '\'' +
                ", ckCheckDesc='" + ckCheckDesc + '\'' +
                '}';
    }
}
