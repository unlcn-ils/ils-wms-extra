package com.unlcn.ils.wms.api.inbound.quartz;


import cn.huiyunche.commons.exception.BusinessException;
import com.unlcn.ils.wms.backend.service.inspectApp.TmsInboundService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * wms入库定时任务执行类
 * <p>
 * 2018-1-30 新需求:新增定时任务从tms抓取入库信息;
 * </p>
 */
@Component(value = "inboundJob")
public class WmsInboundJobDetail {
    private final Logger LOGGER = LoggerFactory.getLogger(WmsInboundJobDetail.class);

    private TmsInboundService tmsInboundService;

    @Autowired
    public void setTmsInboundService(TmsInboundService tmsInboundService) {
        this.tmsInboundService = tmsInboundService;
    }


    /**
     * <p>
     * 2018-1-30 新需求:新增定时任务从tms抓取入库信息;
     * 2018-4-18 新需求:针对入库通知单 重构数据库   新方法{@link WmsInboundJobDetail#fetchInboundOrderBySchedule}
     * </p>
     */
    @Deprecated
    public void fetchInboundOrderByScheduleOld() {
        LOGGER.info("WmsInboundJobDetail.fetchInboundOrderByScheduleOld start:");
        try {
            tmsInboundService.fetchInboundOrderByScheduleOld();
        } catch (BusinessException e) {
            LOGGER.error("WmsInboundJobDetail.fetchInboundOrderByScheduleOld BusinessException:", e);
        } catch (Exception ex) {
            LOGGER.error("WmsInboundJobDetail.fetchInboundOrderByScheduleOld error:", ex);
        }
    }

    /**
     * <p>
     * 2018-4-18 新需求:针对入库通知单重新设计前段接口及导入的asn记录表;
     * </p>
     */
    public void fetchInboundOrderBySchedule() {
        LOGGER.info("WmsInboundJobDetail.fetchInboundOrderBySchedule start:");
        try {
            tmsInboundService.fetchInboundOrderBySchedule();
        } catch (BusinessException e) {
            LOGGER.error("WmsInboundJobDetail.fetchInboundOrderBySchedule BusinessException:", e);
        } catch (Exception ex) {
            LOGGER.error("WmsInboundJobDetail.fetchInboundOrderBySchedule error:", ex);
        }
    }
}
