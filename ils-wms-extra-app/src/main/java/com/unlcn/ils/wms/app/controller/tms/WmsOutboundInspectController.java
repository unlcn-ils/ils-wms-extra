package com.unlcn.ils.wms.app.controller.tms;

import cn.huiyunche.commons.domain.ResultDTO;
import cn.huiyunche.commons.exception.BusinessException;
import com.unlcn.ils.wms.app.vo.tms.TmsInspectExcpVO;
import com.unlcn.ils.wms.app.vo.wms.WmsOutboundDamageDispatchVO;
import com.unlcn.ils.wms.backend.bo.inspectAppBO.TmsInspectExcpBO;
import com.unlcn.ils.wms.backend.bo.inspectAppBO.WmsOutboundDamageDispatchBO;
import com.unlcn.ils.wms.backend.service.inspectApp.WmsOutboundComponentMissService;
import com.unlcn.ils.wms.backend.service.inspectApp.WmsOutboundInspectExcpService;
import com.unlcn.ils.wms.backend.service.inspectApp.WmsOutboundInspectService;
import com.unlcn.ils.wms.base.dto.TmsInspectComponentMissDTO;
import com.unlcn.ils.wms.base.dto.WmsInspectForAppDTO;
import com.unlcn.ils.wms.base.dto.WmsOutboundInspectDTO;
import com.unlcn.ils.wms.base.dto.WmsOutboundInspectExcpDTO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;
import java.util.Objects;

/**
 * 出库验车
 */
@Controller
@RequestMapping(value = "/checkout")
public class WmsOutboundInspectController {
    private Logger logger = LoggerFactory.getLogger(WmsInboundController.class);

    @Autowired
    private WmsOutboundInspectService wmsOutboundInspectService;
    @Autowired
    private WmsOutboundInspectExcpService wmsOutboundInspectExcpService;
    @Autowired
    private WmsOutboundComponentMissService wmsOutboundComponentMissService;

    /**
     * 查询验车单信息及区域异常信息
     *
     * @param vin 车架号
     */
    @RequestMapping(value = "/fetchInspectBillAndPositionExcpCount", method = RequestMethod.GET)
    @ResponseBody
    public ResultDTO<Object> fetchOutboundBill(String vin, String whcode) {
        logger.info("WmsOutboundInspectController.fetchOutboundBill param vin: {}.", vin);
        ResultDTO<Object> result = new ResultDTO<>(true, null, "查询成功");
        if (vin.endsWith("\n")) {
            vin = vin.replaceAll("\n", "");
        }
        try {
            WmsOutboundInspectDTO outboundDTO = wmsOutboundInspectService.selectFetchOutboundBill(vin, whcode);
            if (!Objects.equals(outboundDTO, null)) {
                List<WmsOutboundInspectExcpDTO> positionCountList = wmsOutboundInspectExcpService.getPositionExcpCountByInspectId(outboundDTO.getId(), whcode);
                outboundDTO.setPositionAndExcpCount(positionCountList);
                //String missComponentNames = wmsOutboundComponentMissService.getMissComponentNames(vin);
                String missComponentNames = wmsOutboundComponentMissService.getMissComponentNamesNew(outboundDTO.getId(), whcode);
                outboundDTO.setMissComponentNames(missComponentNames);
            }
            result.setData(outboundDTO);
        } catch (BusinessException e) {
            logger.error("WmsOutboundInspectController.fetchOutboundBill error: {}", e);
            result.setMessage(e.getMessage());
            result.setSuccess(false);
            result.setData(null);
        } catch (Exception e) {
            logger.error("WmsOutboundInspectController.fetchOutboundBill error: {}", e);
            result.setMessage("系统异常");
            result.setSuccess(false);
            result.setData(null);
        }
        return result;
    }


    /**
     * 列表展示车辆九大位置信息
     *
     * @param dto 参数封装
     */
    @RequestMapping(value = "/getInspectExcpList", method = RequestMethod.GET)
    @ResponseBody
    public ResultDTO<Object> getInspectExcpList(WmsInspectForAppDTO dto) {
        logger.info("WmsOutboundInspectController.getInspectExcpList param:{}.", dto);
        ResultDTO<Object> result = new ResultDTO<>(true, null, "查询成功");
        try {
            result.setData(wmsOutboundInspectService.getInspectExcpList(dto));
        } catch (BusinessException e) {
            logger.error("WmsOutboundInspectController.getInspectExcpList error: {}", e);
            result.setMessage(e.getMessage());
            result.setSuccess(false);
            result.setData(null);
        } catch (Exception e) {
            logger.error("WmsOutboundInspectController.getInspectExcpList error: {}", e);
            result.setMessage("系统异常");
            result.setSuccess(false);
            result.setData(null);
        }
        return result;
    }

    /**
     * 获取区域的异常信息
     *
     * @param excpVO 传入异常位置positionId/验车单id
     */
    @RequestMapping(value = "/getInspectExcpListByPositionId", method = RequestMethod.POST)
    @ResponseBody
    public ResultDTO<Object> getInspectExcpListByPositionId(TmsInspectExcpVO excpVO) {
        logger.info("WmsOutboundInspectController.getInspectExcpListByPositionId param:{}.", excpVO);
        ResultDTO<Object> result = new ResultDTO<>(true, null, "查询成功");
        try {
            TmsInspectExcpBO bo = new TmsInspectExcpBO();
            BeanUtils.copyProperties(excpVO, bo);
            result.setData(wmsOutboundInspectExcpService.getInspectExcpListByPositionId(bo));
        } catch (BusinessException e) {
            logger.error("WmsOutboundInspectController.getInspectExcpListByPositionId error: {}", e);
            result.setMessage(e.getMessage());
            result.setSuccess(false);
            result.setData(null);
        } catch (Exception e) {
            logger.error("WmsOutboundInspectController.getInspectExcpListByPositionId error: {}", e);
            result.setMessage("系统异常");
            result.setSuccess(false);
            result.setData(null);
        }
        return result;
    }


    /**
     *
     * 标记异常
     * <p>
     * 2018-5-10 新增仓库code区分
     * </p>
     *
     */
    @RequestMapping(value = "/noteInspectExcp", method = RequestMethod.POST)
    @ResponseBody
    public ResultDTO<Object> noteInspectExcp(TmsInspectExcpVO vo) {
        logger.info("WmsOutboundInspectController.noteInspectExcp param:{}.", vo);
        ResultDTO<Object> result = new ResultDTO<>(true, null, "保存成功");
        TmsInspectExcpBO bo = new TmsInspectExcpBO();
        BeanUtils.copyProperties(vo, bo);
        try {
            wmsOutboundInspectExcpService.saveNoteExcp(bo);
        } catch (BusinessException e) {
            logger.error("WmsOutboundInspectController.noteInspectExcp error: {}", e);
            result.setMessage(e.getMessage());
            result.setSuccess(false);
            result.setData(null);
        } catch (Exception e) {
            logger.error("WmsOutboundInspectController.noteInspectExcp error: {}", e);
            result.setMessage("系统异常");
            result.setSuccess(false);
            result.setData(null);
        }
        return result;
    }

    /**
     * <p>
     * 2018-5-11 新增一键删除异常接口
     * </p>
     *
     * @param dto 参数封装
     * @return 统一返回结果
     */
    @RequestMapping("/deleteAllExp")
    @ResponseBody
    public ResultDTO<Object> deleteAllException(WmsInspectForAppDTO dto) {
        if (logger.isInfoEnabled()) {
            logger.info("WmsOutboundInspectController.deleteAllException param:{}", dto);
        }
        ResultDTO<Object> result = new ResultDTO<>(true, null, "操作成功!");
        try {
            wmsOutboundInspectExcpService.deleteAllException(dto);
        } catch (BusinessException e) {
            logger.error("WmsOutboundInspectController.deleteAllException BusinessException:{}", e);
            result.setMessage(e.getMessage());
            result.setSuccess(false);
        } catch (Exception e) {
            logger.error("WmsOutboundInspectController.deleteAllException error:", e);
            result.setMessage("操作异常");
            result.setSuccess(false);
        }
        return result;
    }



    /**
     * <p>
     * 2018-5-10 新增仓库code区分
     * </p>
     * 获取所有的部件列表,并标注为是否缺失
     *
     * @return result
     */
    @RequestMapping(value = "/getLackOfPartsList", method = RequestMethod.GET)
    @ResponseBody
    public ResultDTO<Object> getComponentMissList(WmsInspectForAppDTO dto) {
        logger.info("WmsOutboundInspectController.getLackOfPartsList param {}", dto);
        ResultDTO<Object> result = new ResultDTO<>(true, null, "查询缺件列表成功!");
        try {
            List<TmsInspectComponentMissDTO> data = wmsOutboundComponentMissService.getComponentMissList(dto.getVin(), dto.getWhCode());
            result.setData(data);
        } catch (BusinessException e) {
            logger.error("WmsOutboundInspectController.getLackOfPartsList error: {}", e);
            result.setMessage(e.getMessage());
            result.setSuccess(false);
            result.setData(null);
        } catch (Exception e) {
            logger.error("WmsOutboundInspectController.getLackOfPartsList error: {}", e);
            result.setMessage("系统异常");
            result.setSuccess(false);
            result.setData(null);
        }
        return result;
    }


    /**
     * <p>
     * 2018-5-10 新增仓库code区分
     * </p>
     * 标记缺件异常
     *
     * @return result
     */
    @RequestMapping(value = "/saveLackParts", method = RequestMethod.POST)
    @ResponseBody
    public ResultDTO<Object> saveComponentMiss(WmsInspectForAppDTO dto) {
        logger.info("WmsOutboundInspectController.saveLackParts param:{}", dto);
        ResultDTO<Object> result = new ResultDTO<>(true, null, "标记缺件异常成功!");
        try {
            wmsOutboundComponentMissService.saveComponentMiss(dto);
        } catch (BusinessException e) {
            logger.error("WmsOutboundInspectController.saveLackParts error: {}", e);
            result.setMessage(e.getMessage());
            result.setSuccess(false);
            result.setData(null);
        } catch (Exception e) {
            logger.error("WmsOutboundInspectController.saveLackParts error: {}", e);
            result.setMessage("系统异常");
            result.setSuccess(false);
            result.setData(null);
        }
        return result;
    }


    /**
     * 正常发运--重庆库
     * 出库验车登记--君马库
     *
     * @return result
     */
    @RequestMapping(value = "/dispatch", method = RequestMethod.POST)
    @ResponseBody
    public ResultDTO<Object> saveDispatch(Long inspectId, String userId) {
        logger.info("WmsOutboundInspectController.saveDispatch param :{},{},{}", inspectId, userId);
        ResultDTO<Object> result = new ResultDTO<>(true, null, "正常发运成功!");
        try {
            wmsOutboundComponentMissService.updateDispatch(inspectId, userId);
        } catch (BusinessException e) {
            logger.error("WmsOutboundInspectController.saveDispatch error: {}", e);
            result.setMessage(e.getMessage());
            result.setSuccess(false);
            result.setData(null);
        } catch (Exception e) {
            logger.error("WmsOutboundInspectController.saveDispatch error: {}", e);
            result.setMessage("系统异常");
            result.setSuccess(false);
            result.setData(null);
        }
        return result;
    }


    /**
     * 带伤发运--重庆库
     *
     * @return result
     */
    @RequestMapping(value = "/damageDispatch ", method = RequestMethod.POST)
    @ResponseBody
    public ResultDTO<Object> saveDamageDispatch(WmsOutboundDamageDispatchVO vo) {
        logger.info("WmsOutboundInspectController.saveDamageDispatch param :{}", vo);
        ResultDTO<Object> result = new ResultDTO<>(true, null, "带伤发运成功!");
        try {
            WmsOutboundDamageDispatchBO bo = new WmsOutboundDamageDispatchBO();
            BeanUtils.copyProperties(vo, bo);
            wmsOutboundComponentMissService.updateDamageDispatch(bo);
        } catch (BusinessException e) {
            logger.error("WmsOutboundInspectController.saveDamageDispatch error: {}", e);
            result.setMessage(e.getMessage());
            result.setSuccess(false);
            result.setData(null);
        } catch (Exception e) {
            logger.error("WmsOutboundInspectController.saveDamageDispatch error: {}", e);
            result.setMessage("系统异常");
            result.setSuccess(false);
            result.setData(null);
        }
        return result;
    }

    /**
     * 验车登记  君马库业务
     * 异常的话--->锁定异常状态
     * 正常的--->合格
     *
     * @param inspectId 运单id
     * @param userId    操作人id
     * @return result 返回值
     */
    @RequestMapping(value = "/inspectConfirm", method = RequestMethod.POST)
    @ResponseBody
    public ResultDTO<Object> inspectConfirm(Long inspectId, String userId, String whCode) {
        logger.info("WmsOutboundInspectController.inspectConfirm param: {},{},{}", inspectId, userId, whCode);
        ResultDTO<Object> result = new ResultDTO<>(true, null, "登记成功");
        try {
            wmsOutboundComponentMissService.updateInspectStatusById(inspectId, userId, whCode);
        } catch (BusinessException e) {
            logger.error("WmsOutboundInspectController.inspectConfirm error: {}", e);
            result.setMessage(e.getMessage());
            result.setSuccess(false);
            result.setData(null);
        } catch (Exception e) {
            logger.error("WmsOutboundInspectController.inspectConfirm error: {}", e);
            result.setMessage("系统异常");
            result.setSuccess(false);
            result.setData(null);
        }
        return result;
    }

}
