package com.unlcn.ils.wms.backend.service.inbound.impl;

import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;

public class TestCalendar {

    public void test(String[] args) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(new Date());//当前时间为2018-5-17 08:56:XX
        cal.add(Calendar.DATE, -9); //当前时间前推9天
        Date time = cal.getTime();
        cal.set(Calendar.HOUR, 0);  //设置前推时间0点
        int i = cal.get(Calendar.HOUR); //获取设置0点后的小时数
        cal.set(Calendar.MINUTE, 0);
        cal.set(Calendar.SECOND, 0);
        Date startTime = cal.getTime();
        TimeZone timeZone = cal.getTimeZone();
        int year = cal.get(Calendar.YEAR);
        int mon = cal.get(Calendar.MONTH);
        int day = cal.get(Calendar.DATE);
        cal.set(year, mon, day, 0, 0, 0);
        Date otime = cal.getTime();
        cal.set(Calendar.HOUR, 23);
        cal.set(Calendar.MINUTE, 59);
        cal.set(Calendar.SECOND, 59);
        Date endTime = cal.getTime();
    }

}
