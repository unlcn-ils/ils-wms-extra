package com.unlcn.ils.wms.backend.bo;

/**
 * Created by houjianhui on 2017/5/6.
 */
public class WayBillBO {
    private Integer id;
    private String waybillCode;
    private String gmtCreate;
    private String vcStartCityName;
    private String vcEndCityName;
    private Integer amount;
    private String vcvin;
    private String vcTypeName;
    private String vcDriverName;
    private String wareHouseName;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getWaybillCode() {
        return waybillCode;
    }

    public void setWaybillCode(String waybillCode) {
        this.waybillCode = waybillCode;
    }

    public String getGmtCreate() {
        return gmtCreate;
    }

    public void setGmtCreate(String gmtCreate) {
        this.gmtCreate = gmtCreate;
    }

    public String getVcStartCityName() {
        return vcStartCityName;
    }

    public void setVcStartCityName(String vcStartCityName) {
        this.vcStartCityName = vcStartCityName;
    }

    public String getVcEndCityName() {
        return vcEndCityName;
    }

    public void setVcEndCityName(String vcEndCityName) {
        this.vcEndCityName = vcEndCityName;
    }

    public Integer getAmount() {
        return amount;
    }

    public void setAmount(Integer amount) {
        this.amount = amount;
    }

    public String getVcvin() {
        return vcvin;
    }

    public void setVcvin(String vcvin) {
        this.vcvin = vcvin;
    }

    public String getVcTypeName() {
        return vcTypeName;
    }

    public void setVcTypeName(String vcTypeName) {
        this.vcTypeName = vcTypeName;
    }

    public String getVcDriverName() {
        return vcDriverName;
    }

    public void setVcDriverName(String vcDriverName) {
        this.vcDriverName = vcDriverName;
    }

    public String getWareHouseName() {
        return wareHouseName;
    }

    public void setWareHouseName(String wareHouseName) {
        this.wareHouseName = wareHouseName;
    }

    @Override
    public String toString() {
        return "WayBillBO{" +
                "id=" + id +
                ", waybillCode='" + waybillCode + '\'' +
                ", gmtCreate='" + gmtCreate + '\'' +
                ", vcStartCityName='" + vcStartCityName + '\'' +
                ", vcEndCityName='" + vcEndCityName + '\'' +
                ", amount=" + amount +
                ", vcvin='" + vcvin + '\'' +
                ", vcTypeName='" + vcTypeName + '\'' +
                ", vcDriverName='" + vcDriverName + '\'' +
                ", wareHouseName='" + wareHouseName + '\'' +
                '}';
    }
}
