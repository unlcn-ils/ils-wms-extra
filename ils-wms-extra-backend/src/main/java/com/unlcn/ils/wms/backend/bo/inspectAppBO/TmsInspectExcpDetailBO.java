package com.unlcn.ils.wms.backend.bo.inspectAppBO;


import java.io.Serializable;
import java.util.Date;
import java.util.List;

public class TmsInspectExcpDetailBO implements Serializable {

    private Long excpId;
    private String vin;
    private Long inspectId;
    private Integer inspectStatus;
    private String constantBodyName;
    private String constantHurtName;
    private Integer positionId;
    private Integer constantBodyCode;
    private Integer constantHurtCode;
    private Date excpTime;
    private String attachs;  //图片的key,多个用逗号隔开
    private List<TmsInspectExcpAttachBO> attachBOS;  //封装图片的url列表

    public Date getExcpTime() {
        return excpTime;
    }

    public void setExcpTime(Date excpTime) {
        this.excpTime = excpTime;
    }

    public Integer getInspectStatus() {
        return inspectStatus;
    }

    public void setInspectStatus(Integer inspectStatus) {
        this.inspectStatus = inspectStatus;
    }

    public String getVin() {
        return vin;
    }

    public void setVin(String vin) {
        this.vin = vin;
    }

    public Long getInspectId() {
        return inspectId;
    }

    public void setInspectId(Long inspectId) {
        this.inspectId = inspectId;
    }

    public List<TmsInspectExcpAttachBO> getAttachBOS() {
        return attachBOS;
    }

    public void setAttachBOS(List<TmsInspectExcpAttachBO> attachBOS) {
        this.attachBOS = attachBOS;
    }

    public Long getExcpId() {
        return excpId;
    }

    public void setExcpId(Long excpId) {
        this.excpId = excpId;
    }

    public String getConstantBodyName() {
        return constantBodyName;
    }

    public void setConstantBodyName(String constantBodyName) {
        this.constantBodyName = constantBodyName;
    }

    public String getConstantHurtName() {
        return constantHurtName;
    }

    public void setConstantHurtName(String constantHurtName) {
        this.constantHurtName = constantHurtName;
    }

    public Integer getPositionId() {
        return positionId;
    }

    public void setPositionId(Integer positionId) {
        this.positionId = positionId;
    }

    public Integer getConstantBodyCode() {
        return constantBodyCode;
    }

    public void setConstantBodyCode(Integer constantBodyCode) {
        this.constantBodyCode = constantBodyCode;
    }

    public Integer getConstantHurtCode() {
        return constantHurtCode;
    }

    public void setConstantHurtCode(Integer constantHurtCode) {
        this.constantHurtCode = constantHurtCode;
    }

    public String getAttachs() {
        return attachs;
    }

    public void setAttachs(String attachs) {
        this.attachs = attachs;
    }
}
