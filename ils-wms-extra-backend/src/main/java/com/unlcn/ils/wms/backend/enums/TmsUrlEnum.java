package com.unlcn.ils.wms.backend.enums;

/**
 * The type Tms url constant.
 */
public enum  TmsUrlEnum {

    BI_PICK_DATE(10,"/statisGetCar.jspx"),
    BI_FRONT_SHIPMENT(20,"/statisForeCache.jspx"),
    BI_OVERREASON(30,"/statisGetCarException.jspx"),
    BI_TRANSMODE(40,"/statisTransmode.jspx"),
    BI_FOREPROGRESS(50,"/statisForeProgress.jspx"),
    BI_INOUTDRAYCHART(60,""),
    BI_INOUTDRAYLIST(70,"/statisTerminalShip.jspx"),
    BI_DISTRIBUTIONCHART(80,"/statisTerminalOut.jspx"),
    BI_DISTRIBUTIONLINE(90,"/statisTerminalOutDetail.jspx");

    private final int value;
    private final String text;

    public int getValue() {
        return value;
    }

    public String getText() {
        return text;
    }


    TmsUrlEnum(int value, String text){
        this.value = value;
        this.text = text;
    }

    public static TmsUrlEnum getByValue(int value){
        for (TmsUrlEnum temp : TmsUrlEnum.values()) {
            if(temp.getValue() == value){
                return temp;
            }
        }
        return null;
    }



}
