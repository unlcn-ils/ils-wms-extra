package com.unlcn.ils.wms.backend.webservice.client;

import cn.huiyunche.commons.exception.BusinessException;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.google.common.collect.Maps;
import com.google.gson.Gson;
import com.unlcn.ils.wms.backend.enums.DispatchTypeEnum;
import com.unlcn.ils.wms.backend.enums.HandoverSendStatusEnum;
import com.unlcn.ils.wms.backend.enums.SendStatusEnum;
import com.unlcn.ils.wms.backend.enums.WmsGateControllerTypeEnum;
import com.unlcn.ils.wms.backend.service.webservice.bo.WmsResultDealerCarbackSapBO;
import com.unlcn.ils.wms.backend.service.webservice.bo.WmsResultInorOutFromSapBO;
import com.unlcn.ils.wms.backend.service.webservice.client.WmsJmSapService;
import com.unlcn.ils.wms.backend.service.webservice.client.enums.WmsWebserviceInterfaceEnum;
import com.unlcn.ils.wms.backend.service.webservice.client.enums.WmsWebserviceReceiverAndSenderEnum;
import com.unlcn.ils.wms.backend.util.webservice.WebServiceSoapUtils;
import com.unlcn.ils.wms.backend.util.webservice.XmlParseUtils;
import com.unlcn.ils.wms.base.mapper.junmadcs.*;
import com.unlcn.ils.wms.base.mapper.outbound.WmsDealerCarBackMapper;
import com.unlcn.ils.wms.base.model.junmadcs.*;
import com.unlcn.ils.wms.base.model.outbound.WmsDealerCarBack;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringEscapeUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Objects;

/**
 * 对接君马sap系统的接口
 */
@Service
public class WmsJmSapServiceImpl implements WmsJmSapService {

    private Logger LOGGER = LoggerFactory.getLogger(WmsJmSapServiceImpl.class);

    private static final SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");

    private static final int TOTAL_COUNT = 5;

    @Value("${wms.webservice.sap.host.url}")
    private String sapHost;
    @Value("${wms.webservice.username}")
    private String username;
    @Value("${wms.webservice.password}")
    private String password;


    @Autowired
    private WmsOutOfStorageMapper wmsOutOfStorageMapper;

    @Autowired
    private WmsDealerCarBackMapper wmsDealerCarBackMapper;

    @Autowired
    private WmsHandoverOrderMapper wmsHandoverOrderMapper;

    @Autowired
    private WmsOutOfStorageExcpMapper wmsOutOfStorageExcpMapper;

    @Autowired
    private WmsHandoverOrderExcpMapper wmsHandoverOrderExcpMapper;

    @Autowired
    private WmsDealerBackExcpMapper wmsDealerBackExcpMapper;

    @Autowired
    private WmsSapLogMapper wmsSapLogMapper;

    /**
     * 出入库接口(公用)  地址:http://58.144.142.90:50000/dir/wsdl?p=sa/ce4f72569b363ba6a7579cbfcfa772c4
     *
     * @param wmsOutOfStorageList
     * @return
     * @throws Exception
     */
    @Override
    public void updateSendInOrOutbounRequest(List<WmsOutOfStorage> wmsOutOfStorageList) throws Exception {
        LOGGER.info("WmsJmSapServiceImpl.updateSendInOrOutboundRequest param{}", wmsOutOfStorageList);
        if (CollectionUtils.isEmpty(wmsOutOfStorageList)) {
            throw new BusinessException("传入的调用sap接口入库数据为空!");
        }
        StringBuilder soapXmlBuf = new StringBuilder();
        //xml头
        soapXmlBuf.append("<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:ecc=\"http://zjunma.com/TRAUM/WMS/ECC\">\n")
                .append(" <soapenv:Header/>\n")
                .append("<soapenv:Body>\n")
                .append("<ecc:MT_GIGR>\n")
                .append("<TP_HEADER>\n")
                .append("<MSGID>" + wmsOutOfStorageList.get(0).getDataId() + "</MSGID>\n")//消息id使用dataId
                .append("<BUSID>" + wmsOutOfStorageList.get(0).getDataId() + "</BUSID>")//业务id
                .append("<TLGID>" + WmsWebserviceInterfaceEnum.INBOUND_OUT_SAP.getValue() + "</TLGID>\n")//接口id
                .append("<TLGNAME>" + WmsWebserviceInterfaceEnum.INBOUND_OUT_SAP.getText() + "</TLGNAME>\n")//接口名称
                .append("<DTSEND>" + sdf.format(new Date()) + "</DTSEND>\n")//发送时间
                .append("<SENDER>" + WmsWebserviceReceiverAndSenderEnum.SENDER.getText() + "</SENDER>\n")//发送方
                .append("<RECEIVER>" + WmsWebserviceReceiverAndSenderEnum.RECEIVER_SAP.getText() + "</RECEIVER>")//接收方
                .append("<FREEUSE>备用</FREEUSE>")//备用 (选填)
                .append("</TP_HEADER>\n");

        for (WmsOutOfStorage wmsOutOfStorage : wmsOutOfStorageList) {
            //if (StringUtils.isBlank(wmsOutOfStorage.getMblnr())) {
            //    throw new BusinessException(wmsOutOfStorage.getDataId() + "调拨单号为空!");
            //}
            if (StringUtils.isBlank(wmsOutOfStorage.getZaction())) {
                throw new BusinessException(wmsOutOfStorage.getDataId() + "出入库类型为空!");
            }
            if (StringUtils.isBlank(wmsOutOfStorage.getZtype())) {
                throw new BusinessException(wmsOutOfStorage.getDataId() + "业务类型为空!");
            }
            if (Objects.equals(wmsOutOfStorage.getBldat(), null)) {
                throw new BusinessException(wmsOutOfStorage.getDataId() + "出入库日期为空!");
            }
            if (StringUtils.isBlank(wmsOutOfStorage.getSernr())) {
                throw new BusinessException(wmsOutOfStorage.getDataId() + "出入库车架号为空!");
            }

            //xml参数
            soapXmlBuf.append("<SPPO>")
                    //必填
                    .append("<DATA_ID>" + wmsOutOfStorage.getDataId() + "</DATA_ID>")//唯一标识
                    .append("<MBLNR>" + wmsOutOfStorage.getMblnr() + "</MBLNR>")//调拨单号
                    .append("<SERNR>" + wmsOutOfStorage.getSernr() + "</SERNR>")//车架号
                    .append("<MATNR>" + wmsOutOfStorage.getMatnr() + "</MATNR>")//物料编码
                    .append("<ZACTION>" + wmsOutOfStorage.getZaction() + "</ZACTION>")//出入库
                    .append("<ZTYPE>" + wmsOutOfStorage.getZtype() + "</ZTYPE>")//业务类型
                    //选填
                    .append("<BLDAT>" + sdf.format(wmsOutOfStorage.getBldat()) + "</BLDAT>")//出入库日期
                    .append("<WERKS>" + "" + "</WERKS>")//出入库工厂sap默认
                    .append("<LGORT>" + "" + "</LGORT>")//出入库仓库sap默认
                    .append("<UMWRK>" + "" + "</UMWRK>")//工厂
                    .append("<UMLGO>" + wmsOutOfStorage.getUmlgo() + "</UMLGO>")//库存地点
                    .append("<ZTFLAG>" + wmsOutOfStorage.getZtflag() + "</ZTFLAG>")//特殊车辆标识
                    .append("<ZDEMO>" + wmsOutOfStorage.getZdemo() + "</ZDEMO>")//备注
                    .append("<FREEUSE1>" + "" + "</FREEUSE1>\n")//备用
                    .append("<FREEUSE2>" + "" + "</FREEUSE2>\n")//备用
                    .append("<FREEUSE3>" + "" + "</FREEUSE3>\n")//备用
                    .append("<FREEUSE4>" + "" + "</FREEUSE4>\n")//备用
                    .append("<FREEUSE5>" + "" + "</FREEUSE5>\n")//备用
                    .append("</SPPO>");
        }
        soapXmlBuf.append("</ecc:MT_GIGR>\n")
                .append("</soapenv:Body>\n")
                .append("</soapenv:Envelope>");
        String soapUrl = "http://" + sapHost + "/XISOAPAdapter/MessageServlet?senderParty=&senderService=BC_WMS&receiverParty=&receiverService=&interface=SI_DCS2SAP_GIGR_OUT&interfaceNamespace=http://zjunma.com/TRAUM/WMS/ECC";
        String soapHost = sapHost;
        String soapAction = "http://sap.com/xi/WebService/soap1.1";
        System.err.println(soapXmlBuf.toString());
        LOGGER.info("WmsJmSapServiceImpl.updateSendInOrOutboundRequest param:{}", soapXmlBuf.toString());
        Runnable insert_log = () -> {
            try {
                WmsSapLogWithBLOBs log = new WmsSapLogWithBLOBs();
                log.setGmtCreate(new Date());
                log.setGmtUpdate(new Date());
                log.setUrl("updateSendInOrOutbounRequest_wms2sap");
                log.setSendType(WmsGateControllerTypeEnum.GATE_OUT.getCode());
                log.setParam(soapXmlBuf.toString());
                wmsSapLogMapper.insertSelective(log);
                LOGGER.info("WmsJmSapServiceImpl.updateSendInOrOutboundRequest 2SAP success");
            } catch (Exception e) {
                LOGGER.error("WmsJmSapServiceImpl.updateSendInOrOutboundRequest 2SAP error:", e);
            }

        };
        new Thread(insert_log).start();

        String result = WebServiceSoapUtils.postSoapToPi(soapUrl, soapHost, soapXmlBuf.toString(), soapAction, username, password);
        //转义返回的字符串
        String xmlResult = StringEscapeUtils.unescapeHtml3(result);
        System.out.println("入库调用回执:" + xmlResult);
        //return xmlResult;

    }

    /**
     * 处理出入库接口返回的结果(WMS提供服务)
     *
     * @param wmsResultInorOutFromSapBO 参数对象
     * @param s_body                    消息体
     * @param s_head                    消息头
     * @throws Exception 异常
     */
    @Override
    public Map<String, Object> updateOutofStorageResult(WmsResultInorOutFromSapBO wmsResultInorOutFromSapBO, String s_body, String s_head) throws Exception {
        LOGGER.info("WmsJmSapServiceImpl.updateOutofStorageResult param{}", JSONObject.toJSONString(wmsResultInorOutFromSapBO));

        Map<String, Object> resultInfo = Maps.newHashMap();
        String checkresult = checkResultInorOut(wmsResultInorOutFromSapBO);
        if (StringUtils.isBlank(checkresult)) {
            //校验通过更新对应状态
            WmsOutOfStorage wmsOutOfStorage = null;
            try {
                wmsOutOfStorage = wmsOutOfStorageMapper.selectByPrimaryKey(Long.valueOf(wmsResultInorOutFromSapBO.getDataId()));
            } catch (NumberFormatException e) {
                throw new BusinessException("输入的dataId不是数字格式!");
            }
            //插入数据到接口异常表中
            WmsOutOfStorageExcpExample excpExample = new WmsOutOfStorageExcpExample();
            if (StringUtils.isNotBlank(wmsResultInorOutFromSapBO.getDataId())) {
                excpExample.createCriteria().andDataIdEqualTo(Long.valueOf(wmsResultInorOutFromSapBO.getDataId()));
                List<WmsOutOfStorageExcp> wmsOutOfStorageExcps = wmsOutOfStorageExcpMapper.selectByExample(excpExample);
                if ("E".equals(wmsResultInorOutFromSapBO.getZstatus())) {
                    //更新状态为异常
                    wmsOutOfStorage.setSendStatusSap(HandoverSendStatusEnum.SEND_FAIL.getStatusCode());
                    wmsOutOfStorage.setGmtUpdate(new Date());
                    wmsOutOfStorage.setZmsgSap(wmsResultInorOutFromSapBO.getReturnMsg());
                    wmsOutOfStorageMapper.updateByPrimaryKeySelective(wmsOutOfStorage);
                    if (CollectionUtils.isEmpty(wmsOutOfStorageExcps)) {
                        //往接口异常表里写入数据,
                        WmsOutOfStorageExcp wmsOutOfStorageExcp = new WmsOutOfStorageExcp();
                        BeanUtils.copyProperties(wmsOutOfStorage, wmsOutOfStorageExcp);
                        wmsOutOfStorageExcp.setDcsSendStatus(wmsOutOfStorage.getSendStatus());
                        wmsOutOfStorageExcp.setZmsg(wmsOutOfStorage.getZmsg());
                        wmsOutOfStorageExcp.setSendDcsCount(1);
                        wmsOutOfStorageExcp.setSendSapCount(1);
                        wmsOutOfStorageExcp.setDataId(wmsOutOfStorage.getDataId());
                        wmsOutOfStorageExcp.setZmsgSap(wmsResultInorOutFromSapBO.getReturnMsg());
                        wmsOutOfStorageExcp.setSendStatusSap(SendStatusEnum.SEND_FAILED.getValue());
                        wmsOutOfStorageExcpMapper.insertSelective(wmsOutOfStorageExcp);
                    } else {
                        //增加调用次数
                        WmsOutOfStorageExcp wmsOutOfStorageExcp = wmsOutOfStorageExcps.get(0);
                        if (wmsOutOfStorageExcp.getSendSapCount() <= TOTAL_COUNT) {
                            wmsOutOfStorageExcp.setSendStatusSap(SendStatusEnum.SEND_FAILED.getValue());
                            wmsOutOfStorageExcp.setGmtUpdate(new Date());
                            if (wmsOutOfStorageExcp.getSendSapCount() == TOTAL_COUNT) {
                                wmsOutOfStorageExcp.setSapFinalStatus(SendStatusEnum.SEND_FAILED.getValue());
                            } else {
                                wmsOutOfStorageExcp.setSendSapCount(wmsOutOfStorageExcp.getSendSapCount() + 1);
                            }
                            wmsOutOfStorageExcp.setZmsgSap(wmsResultInorOutFromSapBO.getReturnMsg());
                            wmsOutOfStorageExcpMapper.updateByPrimaryKeySelective(wmsOutOfStorageExcp);
                        }
                    }
                }
                if ("S".equals(wmsResultInorOutFromSapBO.getZstatus())) {
                    //更新状态为成功
                    wmsOutOfStorage.setSendStatusSap(HandoverSendStatusEnum.SEND_SUCCESS.getStatusCode());
                    wmsOutOfStorage.setGmtUpdate(new Date());
                    wmsOutOfStorage.setZmsgSap(wmsResultInorOutFromSapBO.getReturnMsg());
                    wmsOutOfStorageMapper.updateByPrimaryKeySelective(wmsOutOfStorage);
                    if (CollectionUtils.isNotEmpty(wmsOutOfStorageExcps)) {
                        //最后异常的执行数据为成功
                        WmsOutOfStorageExcp wmsOutOfStorageExcp = wmsOutOfStorageExcps.get(0);
                        wmsOutOfStorageExcp.setZmsgSap(wmsResultInorOutFromSapBO.getReturnMsg());
                        wmsOutOfStorageExcp.setSendStatusSap(SendStatusEnum.SEND_SUCCESS.getValue());
                        wmsOutOfStorageExcp.setGmtUpdate(new Date());
                        if (wmsOutOfStorageExcp.getSendSapCount() < TOTAL_COUNT) {
                            wmsOutOfStorageExcp.setSendSapCount(wmsOutOfStorageExcp.getSendSapCount() + 1);
                        }
                        wmsOutOfStorageExcp.setSapFinalStatus(SendStatusEnum.SEND_SUCCESS.getValue());
                        wmsOutOfStorageExcpMapper.updateByPrimaryKeySelective(wmsOutOfStorageExcp);
                    }
                }
            }
            resultInfo.put("returnCode", "S");
            resultInfo.put("returnMsg", "更新数据成功");
            resultInfo.put("dataId", wmsResultInorOutFromSapBO.getDataId());
        } else {
            throw new BusinessException(checkresult);
        }
        return resultInfo;
    }

    /**
     * 出入库非空校验
     *
     * @param wmsResultInorOutFromSapBO
     * @return
     */

    private String checkResultInorOut(WmsResultInorOutFromSapBO wmsResultInorOutFromSapBO) {
        StringBuilder stringBuilder = new StringBuilder();
        if (StringUtils.isBlank(wmsResultInorOutFromSapBO.getDataId())) {
            stringBuilder.append("返回dataId为空!");
        }
        if (StringUtils.isBlank(wmsResultInorOutFromSapBO.getZstatus())) {
            stringBuilder.append("返回zstatus为空");
        }
        //if (StringUtils.isBlank(wmsResultInorOutFromSapBO.getVin())) {
        //    stringBuilder.append("返回车架号Vin为空");
        //}
        if (StringUtils.isBlank(wmsResultInorOutFromSapBO.getReturnMsg())) {
            stringBuilder.append("返回消息为空!");
        }
        return stringBuilder.toString();
    }


    /**
     * 出交接单接口(同步接口) 地址:http://58.144.142.90:50000/dir/wsdl?p=sa/4cd15e583f86306ebd80737644e8d0bb
     *
     * @param handoverOrderList
     * @return
     * @throws Exception
     */
    @Override
    public void saveHandoverOrderToSap(List<WmsHandoverOrder> handoverOrderList) throws Exception {
        LOGGER.info("WmsJmSapServiceImpl.saveHandoverOrderToSap param{}", handoverOrderList);
        //加入spring的上下文
        //SpringBeanAutowiringSupport.processInjectionBasedOnCurrentContext(this);
        StringBuilder soapXmlBuf = new StringBuilder();
        WmsHandoverOrder handoverOrder = handoverOrderList.get(0);
        //xml头
        soapXmlBuf.append("<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:ecc=\"http://zjunma.com/TRAUM/WMS/ECC\">\n")
                .append(" <soapenv:Header/>\n")
                .append("<soapenv:Body>\n")
                .append("<ecc1:MT_VEHICLE_PGI_REQ xmlns:ecc1=\"http://zjunma.com/TRAUM/DCS/ECC\">")
                .append("<TP_HEADER>\n")
                .append("<MSGID>" + handoverOrder.getDataId() + "</MSGID>\n")//消息id
                .append("<BUSID>" + handoverOrder.getDataId() + "</BUSID>")//业务id
                .append("<TLGID>" + WmsWebserviceInterfaceEnum.HANDOVER_SAP.getValue() + "</TLGID>\n")//接口id
                .append("<TLGNAME>" + WmsWebserviceInterfaceEnum.HANDOVER_SAP.getText() + "</TLGNAME>\n")//接口名称
                .append("<DTSEND>" + sdf.format(new Date()) + "</DTSEND>\n")//发送时间
                .append("<SENDER>" + WmsWebserviceReceiverAndSenderEnum.SENDER.getText() + "</SENDER>\n")//发送方
                .append("<RECEIVER>" + WmsWebserviceReceiverAndSenderEnum.RECEIVER_SAP.getText() + "</RECEIVER>")//接收方
                .append("<FREEUSE>备用</FREEUSE>")//备用 (选填)
                .append("</TP_HEADER>\n");
        //参数拼接
        for (int i = 0, j = handoverOrderList.size(); i < j; i++) {
            System.out.println(i);
            WmsHandoverOrder wmsHandoverOrder = handoverOrderList.get(i);
            //}
            //for (WmsHandoverOrder wmsHandoverOrder : handoverOrderList) {
            if (Objects.equals(wmsHandoverOrder.getDataId(), null)) {
                throw new BusinessException(wmsHandoverOrder.getDataId() + "数据唯一标识Id为空!");
            }
            if (StringUtils.isBlank(wmsHandoverOrder.getVbeln())) {
                throw new BusinessException(wmsHandoverOrder.getDataId() + "订单号为空");
            }
            if (Objects.equals(wmsHandoverOrder.getPosnr(), null)) {
                throw new BusinessException(wmsHandoverOrder.getDataId() + "订单行ID为空");
            }
            if (StringUtils.isBlank(wmsHandoverOrder.getZaction())) {
                throw new BusinessException(wmsHandoverOrder.getDataId() + "发运类型为空");
            }
            if (StringUtils.isBlank(wmsHandoverOrder.getLgort())) {
                throw new BusinessException(wmsHandoverOrder.getDataId() + "发货仓库CODE为空");
            }
            //if (StringUtils.isBlank(wmsHandoverOrder.getZlgort())) {
            //    throw new BusinessException(wmsHandoverOrder.getDataId() + "接收仓库CODE为空");
            //}
            if (StringUtils.isBlank(wmsHandoverOrder.getSernr())) {
                throw new BusinessException(wmsHandoverOrder.getDataId() + "原车架号VIN为空");
            }
            if (Objects.equals(wmsHandoverOrder.getMbdat(), null)) {
                throw new BusinessException(wmsHandoverOrder.getDataId() + "交接日期为空");
            }

            soapXmlBuf.append("<VEHICLE_PGI_REQ>\n")
                    .append("<DATA_ID>" + wmsHandoverOrder.getDataId() + "</DATA_ID>")//数据唯一标识1
                    .append("<VBELN>" + wmsHandoverOrder.getVbeln() + "</VBELN>")//订单号1
                    .append("<POSNR>" + wmsHandoverOrder.getPosnr() + "</POSNR>")//订单行ID1
                    .append("<ZACTION>" + wmsHandoverOrder.getZaction() + "</ZACTION>")//发运类型1
                    .append("<LGORT>" + wmsHandoverOrder.getLgort() + "</LGORT>")//发货仓库CODE1
                    .append("<ZLGORT>" + (DispatchTypeEnum.A2.getCode().equals(wmsHandoverOrder.getZaction()) ? wmsHandoverOrder.getZlgort() : "") + "</ZLGORT>")//接收仓库CODE1
                    .append("<MATNR>" + wmsHandoverOrder.getMatnr() + "</MATNR>")//物料代码1
                    .append("<WERKS>" + (StringUtils.isBlank(wmsHandoverOrder.getWerks()) ? "" : wmsHandoverOrder.getWerks()) + "</WERKS>")//交货工厂
                    .append("<VSTEL>" + (StringUtils.isBlank(wmsHandoverOrder.getVstel()) ? "" : wmsHandoverOrder.getVstel()) + "</VSTEL>")//装运点
                    .append("<SERNR>" + wmsHandoverOrder.getSernr() + "</SERNR>")//原车架号VIN1
                    .append("<ZSERNR>" + (StringUtils.isBlank(wmsHandoverOrder.getZsernr()) ? "" : wmsHandoverOrder.getZsernr()) + "</ZSERNR>")//替换车架号VIN
                    .append("<MBDAT>" + sdf.format(wmsHandoverOrder.getMbdat()) + "</MBDAT>")//交接日期1
                    .append("<FREEUSE1>" + "" + "</FREEUSE1>")//备用字段
                    .append("<FREEUSE2>" + "" + "</FREEUSE2>")//备用字段
                    .append("<FREEUSE3>" + "" + "</FREEUSE3>")//备用字段
                    .append("<FREEUSE4>" + "" + "</FREEUSE4>")//备用字段
                    .append("<FREEUSE5>" + "" + "</FREEUSE5>")//备用字段
                    .append("</VEHICLE_PGI_REQ>\n");
        }
        soapXmlBuf.append("</ecc1:MT_VEHICLE_PGI_REQ>\n")
                .append("</soapenv:Body>")
                .append("</soapenv:Envelope>");
        String soapUrl = "http://" + sapHost + "/XISOAPAdapter/MessageServlet?senderParty=&senderService=BC_DCS&receiverParty=&receiverService=&interface=SI_DCS2SAP_VEHICLE_PGI_OUT&interfaceNamespace=http://zjunma.com/TRAUM/DCS/ECC";
        String soapHost = sapHost;
        String soapAction = "http://client.com/xi/WebService/soap1.1";
        System.err.println(soapXmlBuf.toString());
        LOGGER.info("交接单调用sap接口参数:", soapXmlBuf.toString());
        String result = WebServiceSoapUtils.postSoapToPi(soapUrl, soapHost, soapXmlBuf.toString(), soapAction, username, password);
        String xmlResult = StringEscapeUtils.unescapeHtml3(result);
        System.out.println(xmlResult);
        LOGGER.info("交接单调用sap接口结果:", xmlResult);
        //记录日志
        Runnable runnable = () -> {
            WmsSapLogWithBLOBs log = new WmsSapLogWithBLOBs();
            log.setMsgBody(xmlResult);
            log.setParam(soapXmlBuf.toString());
            log.setMsgHead(null);
            log.setSendType("OUT");
            log.setGmtCreate(new Date());
            log.setGmtUpdate(new Date());
            log.setUrl("WmsJmSapServiceImpl.saveHandoverOrderToSap");
            wmsSapLogMapper.insertSelective(log);
            LOGGER.info("WmsJmSapServiceImpl.saveHandoverOrderToSap  保存发送sap交接单日志成功");
        };
        new Thread(runnable).start();
        Map<String, Object> objectMap = XmlParseUtils.xml2map(xmlResult);
        //获取返回结果
        String json = new Gson().toJson(objectMap);
        JSONObject jsonObject = JSONObject.parseObject(json);
        String en_str = jsonObject.getString("Body");
        JSONObject jsonObject1 = JSONObject.parseObject(en_str);
        String body_str = jsonObject1.getString("Body");
        JSONObject jsonObject2 = JSONObject.parseObject(body_str);
        String er_str = jsonObject2.getString("MT_VEHICLE_PGI_RSP");
        if (StringUtils.isBlank(er_str)) {
            er_str = jsonObject2.getString("VEHICLE_PGI_RSP");
        }
        if (StringUtils.isNotBlank(er_str)) {
            if (!er_str.startsWith("[")) {
                er_str = "[" + er_str + "]";
            }
        }
        JSONArray jsonArray = JSONObject.parseArray(er_str);
        for (int i = 0, j = jsonArray.size(); i < j; i++) {
            JSONObject jsonObject6 = JSONObject.parseObject(jsonArray.get(i).toString());
            String zstatus = jsonObject6.getString("ZSTATUS");
            JSONObject jsonObject4 = JSONObject.parseObject(zstatus);
            String returnCode_str = jsonObject4.getString("ZSTATUS");
            String data_id = jsonObject6.getString("DATA_ID");
            JSONObject jsonObject3 = JSONObject.parseObject(data_id);
            String dataId = jsonObject3.getString("DATA_ID");
            String zmsg = jsonObject6.getString("ZMSG");
            JSONObject jsonObject7 = JSONObject.parseObject(zmsg);
            String de_str = jsonObject7.getString("ZMSG");
            if (StringUtils.isNotBlank(returnCode_str)) {
                if ("N".equals(returnCode_str)) {
                    //失败
                    if (StringUtils.isNotBlank(dataId)) {
                        WmsHandoverOrder wmsHandoverOrder = wmsHandoverOrderMapper.selectByPrimaryKey(Long.valueOf(dataId));
                        //跳过之前已经成功的数据
                        if ((wmsHandoverOrder != null)
                                && (!String.valueOf(SendStatusEnum.SEND_SUCCESS.getValue()).equals(wmsHandoverOrder.getSendStatusSap()))) {
                            wmsHandoverOrder.setSendStatusSap(SendStatusEnum.SEND_FAILED.getValue());
                            wmsHandoverOrder.setResultMsgSap(de_str);
                            wmsHandoverOrder.setGmtUpdate(new Date());
                            wmsHandoverOrderMapper.updateByPrimaryKeySelective(wmsHandoverOrder);
                            //查询接口异常表
                            WmsHandoverOrderExcpExample excpExample = new WmsHandoverOrderExcpExample();
                            excpExample.createCriteria().andDataIdEqualTo(wmsHandoverOrder.getDataId());
                            List<WmsHandoverOrderExcp> wmsHandoverOrderExcps = wmsHandoverOrderExcpMapper.selectByExample(excpExample);
                            if (CollectionUtils.isEmpty(wmsHandoverOrderExcps)) {
                                //往接口异常表里写入数据
                                WmsHandoverOrderExcp handoverOrderExcp = new WmsHandoverOrderExcp();
                                BeanUtils.copyProperties(wmsHandoverOrder, handoverOrderExcp);
                                handoverOrderExcp.setSendCountDcs(1);
                                handoverOrderExcp.setResultMsgDcs(wmsHandoverOrder.getResultMsgDcs());
                                handoverOrderExcp.setSendStatus(wmsHandoverOrder.getSendStatus());
                                handoverOrderExcp.setSendStatusTms(wmsHandoverOrder.getSendStatusTms());
                                handoverOrderExcp.setSendCountTms(1);
                                handoverOrderExcp.setResultMsgTms(wmsHandoverOrder.getResultMsgTms());
                                handoverOrderExcp.setSendCountSap(1);
                                handoverOrderExcp.setSendStatusSap(SendStatusEnum.SEND_FAILED.getValue());
                                handoverOrderExcp.setResultMsgSap(de_str);
                                handoverOrderExcp.setSendCountCrm(1);
                                handoverOrderExcp.setSendStatusCrm(wmsHandoverOrder.getSendStatusCrm());
                                handoverOrderExcp.setResultMsgCrm(wmsHandoverOrder.getResultMsgCrm());
                                handoverOrderExcp.setDataId(wmsHandoverOrder.getDataId());
                                handoverOrderExcp.setGmtUpdate(new Date());
                                wmsHandoverOrderExcpMapper.insertSelective(handoverOrderExcp);
                            } else {
                                //增加调用次数
                                WmsHandoverOrderExcp wmsHandoverOrderExcp = wmsHandoverOrderExcps.get(0);
                                if (wmsHandoverOrderExcp != null) {
                                    if (wmsHandoverOrderExcp.getSendCountSap() <= TOTAL_COUNT) {
                                        wmsHandoverOrderExcp.setSendStatusSap(SendStatusEnum.SEND_FAILED.getValue());
                                        wmsHandoverOrderExcp.setGmtUpdate(new Date());
                                        if (wmsHandoverOrderExcp.getSendCountSap() == TOTAL_COUNT) {
                                            wmsHandoverOrderExcp.setFinalSendStatusSap(SendStatusEnum.SEND_FAILED.getValue());
                                        } else {
                                            wmsHandoverOrderExcp.setSendCountSap(wmsHandoverOrderExcp.getSendCountSap() + 1);
                                        }
                                        wmsHandoverOrderExcp.setResultMsgSap(de_str);
                                        wmsHandoverOrderExcpMapper.updateByPrimaryKeySelective(wmsHandoverOrderExcp);
                                    }
                                }
                            }
                        }
                    }
                }
                if ("Y".equals(returnCode_str)) {
                    //成功
                    if (StringUtils.isNotBlank(dataId)) {
                        WmsHandoverOrder wmsHandoverOrder = wmsHandoverOrderMapper.selectByPrimaryKey(Long.valueOf(dataId));
                        //跳过之前已经成功的数据
                        if ((wmsHandoverOrder != null)
                                && (!String.valueOf(SendStatusEnum.SEND_SUCCESS.getValue()).equals(wmsHandoverOrder.getSendStatusSap()))) {
                            wmsHandoverOrder.setSendStatusSap(SendStatusEnum.SEND_SUCCESS.getValue());
                            wmsHandoverOrder.setResultMsgSap(de_str);
                            wmsHandoverOrder.setGmtUpdate(new Date());
                            wmsHandoverOrderMapper.updateByPrimaryKeySelective(wmsHandoverOrder);
                            //查询接口异常表
                            WmsHandoverOrderExcpExample excpExample = new WmsHandoverOrderExcpExample();
                            excpExample.createCriteria().andDataIdEqualTo(wmsHandoverOrder.getDataId());
                            List<WmsHandoverOrderExcp> wmsHandoverOrderExcps = wmsHandoverOrderExcpMapper.selectByExample(excpExample);
                            if (CollectionUtils.isNotEmpty(wmsHandoverOrderExcps)) {
                                //增加调用次数
                                WmsHandoverOrderExcp wmsHandoverOrderExcp = wmsHandoverOrderExcps.get(0);
                                if (wmsHandoverOrderExcp != null) {
                                    if (wmsHandoverOrderExcp.getSendCountSap() <= TOTAL_COUNT) {
                                        wmsHandoverOrderExcp.setGmtUpdate(new Date());
                                        if (wmsHandoverOrderExcp.getSendCountSap() == TOTAL_COUNT) {
                                            wmsHandoverOrderExcp.setFinalSendStatusSap(SendStatusEnum.SEND_SUCCESS.getValue());
                                        } else {
                                            wmsHandoverOrderExcp.setSendCountSap(wmsHandoverOrderExcp.getSendCountSap() + 1);
                                        }
                                        wmsHandoverOrderExcp.setFinalSendStatusSap(SendStatusEnum.SEND_SUCCESS.getValue());
                                        wmsHandoverOrderExcp.setSendStatusSap(SendStatusEnum.SEND_SUCCESS.getValue());
                                        wmsHandoverOrderExcp.setResultMsgSap(de_str);
                                        wmsHandoverOrderExcpMapper.updateByPrimaryKeySelective(wmsHandoverOrderExcp);
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }

    /**
     * 经销商退车接口(client)  地址:http://58.144.142.90:50000/dir/wsdl?p=sa/0164db7b0c013470bec5c0be73250999
     *
     * @param wmsDealerCarBackList
     * @return
     * @throws Exception
     */
    @Override
    public void updateWmsDealerCarBack(List<WmsDealerCarBack> wmsDealerCarBackList) throws Exception {
        LOGGER.info("WmsJmSapServiceImpl.updateWmsDealerCarBack param{}", wmsDealerCarBackList);
        //加入spring的上下文
        //SpringBeanAutowiringSupport.processInjectionBasedOnCurrentContext(this);
        StringBuilder soapXmlBuf = new StringBuilder();
        //xml头
        soapXmlBuf.append("<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:ecc=\"http://zjunma.com/TRAUM/WMS/ECC\">")
                .append("<soapenv:Header/>\n")
                .append("<soapenv:Body>\n")
                .append("<ecc:MT_PGR>\n")
                .append("<TP_HEADER>\n")
                .append("<MSGID>" + wmsDealerCarBackList.get(0).getCbId() + "</MSGID>\n")//消息id
                .append("<BUSID>" + wmsDealerCarBackList.get(0).getCbId() + "</BUSID>")//业务id
                .append("<TLGID>" + WmsWebserviceInterfaceEnum.DEALERCAR_SAP.getValue() + "</TLGID>\n")//接口id
                .append("<TLGNAME>" + WmsWebserviceInterfaceEnum.DEALERCAR_SAP.getText() + "</TLGNAME>\n")//接口名称
                .append("<DTSEND>" + sdf.format(new Date()) + "</DTSEND>\n")//发送时间
                .append("<SENDER>" + WmsWebserviceReceiverAndSenderEnum.SENDER.getText() + "</SENDER>\n")//发送方
                .append("<RECEIVER>" + WmsWebserviceReceiverAndSenderEnum.RECEIVER_SAP.getText() + "</RECEIVER>")//接收方
                .append("<FREEUSE>" + "" + "</FREEUSE>")//备用 (选填)
                .append("</TP_HEADER>\n");

        for (WmsDealerCarBack wmsDealerCarBack : wmsDealerCarBackList) {
            if (Objects.equals(wmsDealerCarBack.getCbId(), null)) {
                throw new BusinessException(wmsDealerCarBack.getCbId() + "数据唯一标识为空!");
            }
            if (StringUtils.isBlank(wmsDealerCarBack.getCbApplyNo())) {
                throw new BusinessException(wmsDealerCarBack.getCbId() + "退车申请单号为空");
            }
            if (Objects.equals(wmsDealerCarBack.getCbLineId(), null)) {
                throw new BusinessException(wmsDealerCarBack.getCbId() + "行id为空");
            }
            if (StringUtils.isBlank(wmsDealerCarBack.getCbDealerCode())) {
                throw new BusinessException(wmsDealerCarBack.getCbId() + "经销商代码为空");
            }
            if (StringUtils.isBlank(wmsDealerCarBack.getCbVin())) {
                throw new BusinessException(wmsDealerCarBack.getCbId() + "车架号为空");
            }
            if (StringUtils.isBlank(wmsDealerCarBack.getCbLgort())) {
                throw new BusinessException(wmsDealerCarBack.getCbId() + "入库仓库code为空");
            }
            //参数拼接
            soapXmlBuf.append("<TP_BODY>")//备注1的字段必填
                    .append("<DATA_ID>" + wmsDealerCarBack.getCbId() + "</DATA_ID>")//数据唯一标识1
                    .append("<VBELN>" + wmsDealerCarBack.getCbApplyNo() + "</VBELN>")//推车申请单号1
                    .append("<POSNR>" + wmsDealerCarBack.getCbLineId() + "</POSNR>")//行id1
                    .append("<KUNNR>" + wmsDealerCarBack.getCbDealerCode() + "</KUNNR>")//经销商代码1
                    .append("<NAME1>" + wmsDealerCarBack.getCbDealerName() + "</NAME1>")//经销商名称
                    .append("<MATNR>" + wmsDealerCarBack.getCbMaterialCode() + "</MATNR>")//物料代码
                    .append("<MAKTX>" + wmsDealerCarBack.getCbMaterialName() + "</MAKTX>")//物料名称
                    .append("<SERNR>" + wmsDealerCarBack.getCbVin() + "</SERNR>")//车架号1
                    //入库仓库code1  在退车申请的dcs接口存入的是CS01 XY01
                    .append("<LGORT>" + wmsDealerCarBack.getCbLgort() + "</LGORT>")
                    .append("<MBDAT>" + sdf.format(wmsDealerCarBack.getCbReceiveTime()) + "</MBDAT>")//接收时间
                    .append("<FREEUSE1>" + "" + "</FREEUSE1>")//备用字段
                    .append("<FREEUSE2>" + "" + "</FREEUSE2>")//备用字段
                    .append("<FREEUSE3>" + "" + "</FREEUSE3>")//备用字段
                    .append("<FREEUSE4>" + "" + "</FREEUSE4>")//备用字段
                    .append("</TP_BODY>");
        }
        soapXmlBuf.append("</ecc:MT_PGR>\n")
                .append("</soapenv:Body>")
                .append("</soapenv:Envelope>");

        //String soapUrl = "http://" + sapHost + "/XISOAPAdapter/MessageServlet?senderParty=&senderService=BC_WMS&receiverParty=&receiverService=&interface=SI_WMS2SAP_PGR_OUT&interfaceNamespace=http://zjunma.com/TRAUM/WMS/ECC";
        String soapUrl = "http://" + sapHost + "/XISOAPAdapter/MessageServlet?senderParty=&senderService=BC_WMS&receiverParty=&receiverService=&interface=SI_WMS2SAP_PGR_OUT&interfaceNamespace=http://zjunma.com/TRAUM/WMS/ECC";
        String soapHost = sapHost;
        String soapAction = "http://client.com/xi/WebService/soap1.1";
        System.err.println(soapXmlBuf.toString());
        LOGGER.info("经销商退车调用sap接口参数:", soapXmlBuf.toString());
        String result = WebServiceSoapUtils.postSoapToPi(soapUrl, soapHost, soapXmlBuf.toString(), soapAction, username, password);
        System.out.println(result);

    }


    /**
     * 处理退车申请接口返回值
     *
     * @param wmsResultDealerCarbackSapBO 参数封装
     * @param s_body                      体
     * @param s_head                      头
     * @return 返回值
     * @throws Exception 异常
     */
    @Override
    public Map<String, Object> updateDealercarbackResult(WmsResultDealerCarbackSapBO
                                                                 wmsResultDealerCarbackSapBO, String s_body, String s_head) throws Exception {
        LOGGER.info("WmsJmSapServiceImpl.updateDealercarbackResult param{}", wmsResultDealerCarbackSapBO);
        //记录日志
        Runnable runnable = () -> {
            WmsSapLogWithBLOBs log = new WmsSapLogWithBLOBs();
            log.setMsgBody(s_body);
            log.setMsgHead(s_head);
            log.setSendType("IN");
            log.setGmtCreate(new Date());
            log.setGmtUpdate(new Date());
            log.setUrl("WmsJmSapServiceImpl.updateDealercarbackResult");
            wmsSapLogMapper.insertSelective(log);
            LOGGER.info("WmsJmSapServiceImpl.updateDealercarbackResult  保存sap退车异步结果成功");
        };
        new Thread(runnable).start();
        Map<String, Object> resultInfo = Maps.newHashMap();
        String checkresult = checkResultDealercarbackResult(wmsResultDealerCarbackSapBO);
        if (StringUtils.isBlank(checkresult)) {
            //校验通过更新对应状态
            WmsDealerCarBack wmsDealerCarBack = null;
            if (StringUtils.isNotBlank(wmsResultDealerCarbackSapBO.getDataId())) {
                try {
                    if (StringUtils.isNotBlank(wmsResultDealerCarbackSapBO.getDataId())) {
                        wmsDealerCarBack = wmsDealerCarBackMapper.selectByPrimaryKey(Long.valueOf(wmsResultDealerCarbackSapBO.getDataId()));
                    }
                } catch (NumberFormatException e) {
                    throw new BusinessException("dataId输入应该为数字格式");
                }
            }
            try {
                if ("N".equals(wmsResultDealerCarbackSapBO.getZstatus())) {
                    //更新状态为异常
                    if (wmsDealerCarBack != null) {
                        wmsDealerCarBack.setCbZstatus(HandoverSendStatusEnum.SEND_FAIL.getStatusCode());
                        wmsDealerCarBack.setGmtUpdate(new Date());
                        wmsDealerCarBack.setCbZmsg(wmsResultDealerCarbackSapBO.getZmsg());
                        wmsDealerCarBackMapper.updateByPrimaryKeySelective(wmsDealerCarBack);
                        WmsDealerBackExcp wmsDealerBackExcp = updateFindDealerBackExcps(wmsResultDealerCarbackSapBO.getDataId());
                        if (Objects.equals(wmsDealerBackExcp, null)) {
                            //需要插入到异常表
                            wmsDealerBackExcp = new WmsDealerBackExcp();
                            BeanUtils.copyProperties(wmsDealerCarBack, wmsDealerBackExcp);
                            wmsDealerBackExcp.setGmtCreate(new Date());
                            wmsDealerBackExcp.setGmtUpdate(new Date());
                            wmsDealerBackExcp.setCbZmsg(wmsDealerCarBack.getCbZmsg());
                            wmsDealerBackExcp.setSendCount(1);
                            wmsDealerBackExcp.setCbZstatus(SendStatusEnum.SEND_FAILED.getValue());
                            if (StringUtils.isNotBlank(wmsResultDealerCarbackSapBO.getDataId())) {
                                //关联退车接口表数据
                                wmsDealerBackExcp.setDataId(Long.valueOf(wmsResultDealerCarbackSapBO.getDataId()));
                            }
                            wmsDealerBackExcpMapper.insertSelective(wmsDealerBackExcp);
                        } else {
                            //把查询接口数+1
                            if (wmsDealerBackExcp != null && wmsDealerBackExcp.getSendCount() <= TOTAL_COUNT) {
                                wmsDealerBackExcp.setGmtUpdate(new Date());
                                if (wmsDealerBackExcp.getSendCount() == TOTAL_COUNT) {
                                    wmsDealerBackExcp.setFinalStatus(SendStatusEnum.SEND_FAILED.getValue());
                                } else {
                                    wmsDealerBackExcp.setSendCount(wmsDealerBackExcp.getSendCount() + 1);
                                }
                                wmsDealerBackExcp.setCbZstatus(SendStatusEnum.SEND_FAILED.getValue());
                                wmsDealerBackExcp.setCbZmsg(wmsResultDealerCarbackSapBO.getZmsg());
                            }
                        }
                    }
                }
                if ("Y".equals(wmsResultDealerCarbackSapBO.getZstatus())) {
                    if (wmsDealerCarBack != null) {
                        //更新状态为成功
                        wmsDealerCarBack.setCbZstatus(HandoverSendStatusEnum.SEND_SUCCESS.getStatusCode());
                        wmsDealerCarBack.setGmtUpdate(new Date());
                        wmsDealerCarBack.setCbZmsg(wmsResultDealerCarbackSapBO.getZmsg());
                        wmsDealerCarBackMapper.updateByPrimaryKeySelective(wmsDealerCarBack);
                    }
                    WmsDealerBackExcp wmsDealerBackExcp = updateFindDealerBackExcps(wmsResultDealerCarbackSapBO.getDataId());
                    if (wmsDealerBackExcp != null) {
                        wmsDealerBackExcp.setFinalStatus(SendStatusEnum.SEND_SUCCESS.getValue());
                        wmsDealerBackExcp.setCbZstatus(SendStatusEnum.SEND_SUCCESS.getValue());
                        wmsDealerBackExcp.setCbZmsg(wmsResultDealerCarbackSapBO.getZmsg());
                        wmsDealerBackExcp.setGmtUpdate(new Date());
                        wmsDealerBackExcpMapper.updateByPrimaryKeySelective(wmsDealerBackExcp);
                    }
                }
                resultInfo.put("returnCode", "S");
                resultInfo.put("returnMsg", "更新数据成功");
                resultInfo.put("dataId", wmsResultDealerCarbackSapBO.getDataId());
            } catch (Exception e) {
                throw new BusinessException("系统修改数据异常!");
            }
        } else {
            //resultInfo.put("returnCode", "E");
            //resultInfo.put("returnMsg", checkresult);
            //resultInfo.put("dataId", wmsResultDealerCarbackSapBO.getDataId());
            throw new BusinessException(checkresult);
        }
        return resultInfo;
    }


    /**
     * 保存返回结果日志
     *
     * @param s_body 返回体
     * @param s_head 返回头
     * @throws Exception 异常
     */
    @Override
    public void saveResultLog(String s_body, String s_head) throws Exception {
        //记录日志
        Runnable runnable = () -> {
            WmsSapLogWithBLOBs log = new WmsSapLogWithBLOBs();
            log.setMsgBody(s_body);
            log.setMsgHead(s_head);
            log.setSendType("IN");
            log.setGmtCreate(new Date());
            log.setGmtUpdate(new Date());
            log.setUrl("WmsJmSapServiceImpl.updateOutofStorageResult");
            wmsSapLogMapper.insertSelective(log);
            LOGGER.info("WmsJmSapServiceImpl.updateOutofStorageResult  保存sap出入库异步结果成功");
        };
        new Thread(runnable).start();
    }

    /**
     * 查询退车异常列表
     *
     * @param dataId
     */

    private WmsDealerBackExcp updateFindDealerBackExcps(String dataId) {
        if (StringUtils.isNotBlank(dataId)) {
            WmsDealerBackExcpExample excpExample = new WmsDealerBackExcpExample();
            excpExample.createCriteria().andDataIdEqualTo(Long.valueOf(dataId));
            List<WmsDealerBackExcp> wmsDealerBackExcps = wmsDealerBackExcpMapper.selectByExample(excpExample);
            if (CollectionUtils.isNotEmpty(wmsDealerBackExcps)) {
                return wmsDealerBackExcps.get(0);
            }
        }
        return null;
    }

    private String checkResultDealercarbackResult(WmsResultDealerCarbackSapBO wmsResultDealerCarbackSapBO) {
        StringBuilder stringBuilder = new StringBuilder();
        if (StringUtils.isBlank(wmsResultDealerCarbackSapBO.getDataId())) {
            stringBuilder.append("dataId:" + wmsResultDealerCarbackSapBO.getDataId() + "返回dataId为空!");
        }
        if (StringUtils.isBlank(wmsResultDealerCarbackSapBO.getZstatus())) {
            stringBuilder.append("dataId:" + wmsResultDealerCarbackSapBO.getDataId() + "返回zstatus为空!");
        }
        //if (StringUtils.isBlank(wmsResultDealerCarbackSapBO.getVbeln())) {
        //    stringBuilder.append("dataId:" + wmsResultDealerCarbackSapBO.getDataId() + "SAP交货单号为空!");
        //}
        //if (StringUtils.isBlank(wmsResultDealerCarbackSapBO.getPosnr())) {
        //    stringBuilder.append("dataId:" + wmsResultDealerCarbackSapBO.getDataId() + "SAP交货单行项目为空!");
        //}
        //if (StringUtils.isBlank(wmsResultDealerCarbackSapBO.getZvbeln())) {
        //    stringBuilder.append("dataId:" + wmsResultDealerCarbackSapBO.getDataId() + "退车申请单号为空!");
        //}
        //if (StringUtils.isBlank(wmsResultDealerCarbackSapBO.getZposnr())) {
        //    stringBuilder.append("dataId:" + wmsResultDealerCarbackSapBO.getDataId() + "行ID为空!");
        //}
        if (StringUtils.isBlank(wmsResultDealerCarbackSapBO.getZmsg())) {
            stringBuilder.append("dataId:" + wmsResultDealerCarbackSapBO.getDataId() + "处理日志为空!");
        }
        return stringBuilder.toString();
    }
}
