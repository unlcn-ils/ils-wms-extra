package com.unlcn.ils.wms.backend.bo.pickup;

import java.util.Date;

/**
 * @Author ：Ligl
 * @Date : 2017/7/22.
 */
public class TmsWaybillBO {
    private Long id;
    private Long infoId;
    private String waybillCode;
    private String vin;
    private String vehicle;
    private String destProvince;
    private String warehouseId;
    private String warehouseName;
    private String motorNo;
    private String status;
    private Date gmtCreate;
    private Integer pickupStatus;
    private Integer checkStatus;
    private Date pickupTime;
    private String pickupUserId;
    private Date generateTime;
    private String checkStatusText;
    private String destCity;
    private String cacheWarehouse;
    private String insepectUserId;
    private String insepectTime;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getWaybillCode() {
        return waybillCode;
    }

    public void setWaybillCode(String waybillCode) {
        this.waybillCode = waybillCode;
    }

    public String getVin() {
        return vin;
    }

    public void setVin(String vin) {
        this.vin = vin;
    }

    public String getVehicle() {
        return vehicle;
    }

    public void setVehicle(String vehicle) {
        this.vehicle = vehicle;
    }

    public String getDestProvince() {
        return destProvince;
    }

    public void setDestProvince(String destProvince) {
        this.destProvince = destProvince;
    }

    public String getWarehouseId() {
        return warehouseId;
    }

    public void setWarehouseId(String warehouseId) {
        this.warehouseId = warehouseId;
    }

    public String getWarehouseName() {
        return warehouseName;
    }

    public void setWarehouseName(String warehouseName) {
        this.warehouseName = warehouseName;
    }

    public String getMotorNo() {
        return motorNo;
    }

    public void setMotorNo(String motorNo) {
        this.motorNo = motorNo;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Long getInfoId() {
        return infoId;
    }

    public void setInfoId(Long infoId) {
        this.infoId = infoId;
    }

    public Date getGmtCreate() {
        return gmtCreate;
    }

    public void setGmtCreate(Date gmtCreate) {
        this.gmtCreate = gmtCreate;
    }

    public Integer getPickupStatus() {
        return pickupStatus;
    }

    public void setPickupStatus(Integer pickupStatus) {
        this.pickupStatus = pickupStatus;
    }

    public Integer getCheckStatus() {
        return checkStatus;
    }

    public void setCheckStatus(Integer checkStatus) {
        this.checkStatus = checkStatus;
    }

    public Date getPickupTime() {
        return pickupTime;
    }

    public void setPickupTime(Date pickupTime) {
        this.pickupTime = pickupTime;
    }


    public String getCheckStatusText() {
        return checkStatusText;
    }

    public void setCheckStatusText(String checkStatusText) {
        this.checkStatusText = checkStatusText;
    }

    public Date getGenerateTime() {
        return generateTime;
    }

    public void setGenerateTime(Date generateTime) {
        this.generateTime = generateTime;
    }

    public String getPickupUserId() {
        return pickupUserId;
    }

    public void setPickupUserId(String pickupUserId) {
        this.pickupUserId = pickupUserId;
    }

    public String getDestCity() {
        return destCity;
    }

    public void setDestCity(String destCity) {
        this.destCity = destCity;
    }

    public String getCacheWarehouse() {
        return cacheWarehouse;
    }

    public void setCacheWarehouse(String cacheWarehouse) {
        this.cacheWarehouse = cacheWarehouse;
    }

    public String getInsepectUserId() {
        return insepectUserId;
    }

    public void setInsepectUserId(String insepectUserId) {
        this.insepectUserId = insepectUserId;
    }

    public String getInsepectTime() {
        return insepectTime;
    }

    public void setInsepectTime(String insepectTime) {
        this.insepectTime = insepectTime;
    }

    @Override
    public String toString() {
        return "TmsWaybillBO{" +
                "id=" + id +
                ", infoId=" + infoId +
                ", waybillCode='" + waybillCode + '\'' +
                ", vin='" + vin + '\'' +
                ", vehicle='" + vehicle + '\'' +
                ", destProvince='" + destProvince + '\'' +
                ", warehouseId='" + warehouseId + '\'' +
                ", warehouseName='" + warehouseName + '\'' +
                ", motorNo='" + motorNo + '\'' +
                ", status='" + status + '\'' +
                ", gmtCreate=" + gmtCreate +
                ", pickupStatus=" + pickupStatus +
                ", checkStatus=" + checkStatus +
                ", pickupTime=" + pickupTime +
                ", pickupUserId='" + pickupUserId + '\'' +
                ", generateTime=" + generateTime +
                ", checkStatusText='" + checkStatusText + '\'' +
                ", destCity='" + destCity + '\'' +
                ", cacheWarehouse='" + cacheWarehouse + '\'' +
                ", insepectUserId='" + insepectUserId + '\'' +
                ", insepectTime='" + insepectTime + '\'' +
                '}';
    }
}
