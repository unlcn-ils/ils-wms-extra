package com.unlcn.ils.wms.backend.enums;

public enum WmsInboundAllocationStatusEnum {
    HAS_ALLOCAED("30", "已分配"),
    ALLOCATED_CONFIRM("40", "确认分配"),
    INBOUND_STATUS("50", "已入库");
    private final String value;
    private final String text;

    WmsInboundAllocationStatusEnum(String value,String text) {
        this.value = value;
        this.text = text;
    }

    public String getValue() {
        return value;
    }

    public String getText() {
        return text;
    }


}
