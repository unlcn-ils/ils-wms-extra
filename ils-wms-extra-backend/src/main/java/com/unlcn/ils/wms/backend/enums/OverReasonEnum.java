package com.unlcn.ils.wms.backend.enums;

/**
 * The type Tms url constant.
 */
public enum OverReasonEnum {

    LOSSES("MASS_LOSS", "质损"),
    MAN_CTRLS("MAN_CTRLS", "管控"),
    SUSPENDS("STOP", "暂运");

    private final String value;
    private final String text;

    public String getValue() {
        return value;
    }

    public String getText() {
        return text;
    }


    OverReasonEnum(String value, String text) {
        this.value = value;
        this.text = text;
    }

    public static OverReasonEnum getByValue(String value) {
        for (OverReasonEnum temp : OverReasonEnum.values()) {
            if (temp.getValue().equals(value)) {
                return temp;
            }
        }
        return null;
    }


}
