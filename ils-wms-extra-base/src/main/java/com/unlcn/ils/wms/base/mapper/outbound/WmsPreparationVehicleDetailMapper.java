package com.unlcn.ils.wms.base.mapper.outbound;

import com.unlcn.ils.wms.base.model.outbound.WmsPreparationVehicleDetail;
import com.unlcn.ils.wms.base.model.outbound.WmsPreparationVehicleDetailExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface WmsPreparationVehicleDetailMapper {
    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table wms_preparation_vehicle_detail
     *
     * @mbggenerated Mon Oct 23 11:04:18 CST 2017
     */
    int countByExample(WmsPreparationVehicleDetailExample example);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table wms_preparation_vehicle_detail
     *
     * @mbggenerated Mon Oct 23 11:04:18 CST 2017
     */
    int deleteByExample(WmsPreparationVehicleDetailExample example);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table wms_preparation_vehicle_detail
     *
     * @mbggenerated Mon Oct 23 11:04:18 CST 2017
     */
    int deleteByPrimaryKey(Long vdId);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table wms_preparation_vehicle_detail
     *
     * @mbggenerated Mon Oct 23 11:04:18 CST 2017
     */
    int insert(WmsPreparationVehicleDetail record);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table wms_preparation_vehicle_detail
     *
     * @mbggenerated Mon Oct 23 11:04:18 CST 2017
     */
    int insertSelective(WmsPreparationVehicleDetail record);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table wms_preparation_vehicle_detail
     *
     * @mbggenerated Mon Oct 23 11:04:18 CST 2017
     */
    List<WmsPreparationVehicleDetail> selectByExample(WmsPreparationVehicleDetailExample example);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table wms_preparation_vehicle_detail
     *
     * @mbggenerated Mon Oct 23 11:04:18 CST 2017
     */
    WmsPreparationVehicleDetail selectByPrimaryKey(Long vdId);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table wms_preparation_vehicle_detail
     *
     * @mbggenerated Mon Oct 23 11:04:18 CST 2017
     */
    int updateByExampleSelective(@Param("record") WmsPreparationVehicleDetail record, @Param("example") WmsPreparationVehicleDetailExample example);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table wms_preparation_vehicle_detail
     *
     * @mbggenerated Mon Oct 23 11:04:18 CST 2017
     */
    int updateByExample(@Param("record") WmsPreparationVehicleDetail record, @Param("example") WmsPreparationVehicleDetailExample example);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table wms_preparation_vehicle_detail
     *
     * @mbggenerated Mon Oct 23 11:04:18 CST 2017
     */
    int updateByPrimaryKeySelective(WmsPreparationVehicleDetail record);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table wms_preparation_vehicle_detail
     *
     * @mbggenerated Mon Oct 23 11:04:18 CST 2017
     */
    int updateByPrimaryKey(WmsPreparationVehicleDetail record);
}