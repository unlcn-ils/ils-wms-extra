package com.unlcn.ils.wms.app.vo.wms;

/**
 * Created by houjianhui on 2017/5/8.
 */
public class OutboundExcpVO {

    private Long entryId;

    private String waybillCode;

    private Integer excpType;

    private String details;

    public Long getEntryId() {
        return entryId;
    }

    public void setEntryId(Long entryId) {
        this.entryId = entryId;
    }

    public String getWaybillCode() {
        return waybillCode;
    }

    public void setWaybillCode(String waybillCode) {
        this.waybillCode = waybillCode;
    }

    public Integer getExcpType() {
        return excpType;
    }

    public void setExcpType(Integer excpType) {
        this.excpType = excpType;
    }

    public String getDetails() {
        return details;
    }

    public void setDetails(String details) {
        this.details = details;
    }

    @Override
    public String toString() {
        return "OutboundExcpVO{" +
                "entryId=" + entryId +
                ", waybillCode='" + waybillCode + '\'' +
                ", excpType=" + excpType +
                ", details='" + details + '\'' +
                '}';
    }
}
