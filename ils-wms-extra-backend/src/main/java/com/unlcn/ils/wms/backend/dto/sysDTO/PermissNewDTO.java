package com.unlcn.ils.wms.backend.dto.sysDTO;

import java.util.List;

/**
 * @Auther linbao
 * @Date 2017-11-09
 */
public class PermissNewDTO {

    //id
    private Integer id;
    //权限名
    private String name;
    //父级id
    private Integer parentId;
    //级别
    private String level;

    private List<PermissNewDTO> childPermissionsBOList;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getParentId() {
        return parentId;
    }

    public void setParentId(Integer parentId) {
        this.parentId = parentId;
    }

    public String getLevel() {
        return level;
    }

    public void setLevel(String level) {
        this.level = level;
    }

    public List<PermissNewDTO> getChildPermissionsBOList() {
        return childPermissionsBOList;
    }

    public void setChildPermissionsBOList(List<PermissNewDTO> childPermissionsBOList) {
        this.childPermissionsBOList = childPermissionsBOList;
    }
}
