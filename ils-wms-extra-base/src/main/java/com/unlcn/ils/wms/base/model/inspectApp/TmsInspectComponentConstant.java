package com.unlcn.ils.wms.base.model.inspectApp;

import java.io.Serializable;
import java.util.Date;

public class TmsInspectComponentConstant implements Serializable {
    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column tms_inspect_component_constant.id
     *
     * @mbggenerated Wed May 09 10:08:28 CST 2018
     */
    private Long id;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column tms_inspect_component_constant.component_code
     *
     * @mbggenerated Wed May 09 10:08:28 CST 2018
     */
    private String componentCode;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column tms_inspect_component_constant.component_name
     *
     * @mbggenerated Wed May 09 10:08:28 CST 2018
     */
    private String componentName;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column tms_inspect_component_constant.gmt_create
     *
     * @mbggenerated Wed May 09 10:08:28 CST 2018
     */
    private Date gmtCreate;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column tms_inspect_component_constant.gmt_update
     *
     * @mbggenerated Wed May 09 10:08:28 CST 2018
     */
    private Date gmtUpdate;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column tms_inspect_component_constant.del_Flag
     *
     * @mbggenerated Wed May 09 10:08:28 CST 2018
     */
    private Boolean delFlag;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column tms_inspect_component_constant.wh_code
     *
     * @mbggenerated Wed May 09 10:08:28 CST 2018
     */
    private String whCode;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database table tms_inspect_component_constant
     *
     * @mbggenerated Wed May 09 10:08:28 CST 2018
     */
    private static final long serialVersionUID = 1L;

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column tms_inspect_component_constant.id
     *
     * @return the value of tms_inspect_component_constant.id
     *
     * @mbggenerated Wed May 09 10:08:28 CST 2018
     */
    public Long getId() {
        return id;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column tms_inspect_component_constant.id
     *
     * @param id the value for tms_inspect_component_constant.id
     *
     * @mbggenerated Wed May 09 10:08:28 CST 2018
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column tms_inspect_component_constant.component_code
     *
     * @return the value of tms_inspect_component_constant.component_code
     *
     * @mbggenerated Wed May 09 10:08:28 CST 2018
     */
    public String getComponentCode() {
        return componentCode;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column tms_inspect_component_constant.component_code
     *
     * @param componentCode the value for tms_inspect_component_constant.component_code
     *
     * @mbggenerated Wed May 09 10:08:28 CST 2018
     */
    public void setComponentCode(String componentCode) {
        this.componentCode = componentCode == null ? null : componentCode.trim();
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column tms_inspect_component_constant.component_name
     *
     * @return the value of tms_inspect_component_constant.component_name
     *
     * @mbggenerated Wed May 09 10:08:28 CST 2018
     */
    public String getComponentName() {
        return componentName;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column tms_inspect_component_constant.component_name
     *
     * @param componentName the value for tms_inspect_component_constant.component_name
     *
     * @mbggenerated Wed May 09 10:08:28 CST 2018
     */
    public void setComponentName(String componentName) {
        this.componentName = componentName == null ? null : componentName.trim();
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column tms_inspect_component_constant.gmt_create
     *
     * @return the value of tms_inspect_component_constant.gmt_create
     *
     * @mbggenerated Wed May 09 10:08:28 CST 2018
     */
    public Date getGmtCreate() {
        return gmtCreate;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column tms_inspect_component_constant.gmt_create
     *
     * @param gmtCreate the value for tms_inspect_component_constant.gmt_create
     *
     * @mbggenerated Wed May 09 10:08:28 CST 2018
     */
    public void setGmtCreate(Date gmtCreate) {
        this.gmtCreate = gmtCreate;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column tms_inspect_component_constant.gmt_update
     *
     * @return the value of tms_inspect_component_constant.gmt_update
     *
     * @mbggenerated Wed May 09 10:08:28 CST 2018
     */
    public Date getGmtUpdate() {
        return gmtUpdate;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column tms_inspect_component_constant.gmt_update
     *
     * @param gmtUpdate the value for tms_inspect_component_constant.gmt_update
     *
     * @mbggenerated Wed May 09 10:08:28 CST 2018
     */
    public void setGmtUpdate(Date gmtUpdate) {
        this.gmtUpdate = gmtUpdate;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column tms_inspect_component_constant.del_Flag
     *
     * @return the value of tms_inspect_component_constant.del_Flag
     *
     * @mbggenerated Wed May 09 10:08:28 CST 2018
     */
    public Boolean getDelFlag() {
        return delFlag;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column tms_inspect_component_constant.del_Flag
     *
     * @param delFlag the value for tms_inspect_component_constant.del_Flag
     *
     * @mbggenerated Wed May 09 10:08:28 CST 2018
     */
    public void setDelFlag(Boolean delFlag) {
        this.delFlag = delFlag;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column tms_inspect_component_constant.wh_code
     *
     * @return the value of tms_inspect_component_constant.wh_code
     *
     * @mbggenerated Wed May 09 10:08:28 CST 2018
     */
    public String getWhCode() {
        return whCode;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column tms_inspect_component_constant.wh_code
     *
     * @param whCode the value for tms_inspect_component_constant.wh_code
     *
     * @mbggenerated Wed May 09 10:08:28 CST 2018
     */
    public void setWhCode(String whCode) {
        this.whCode = whCode == null ? null : whCode.trim();
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table tms_inspect_component_constant
     *
     * @mbggenerated Wed May 09 10:08:28 CST 2018
     */
    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", id=").append(id);
        sb.append(", componentCode=").append(componentCode);
        sb.append(", componentName=").append(componentName);
        sb.append(", gmtCreate=").append(gmtCreate);
        sb.append(", gmtUpdate=").append(gmtUpdate);
        sb.append(", delFlag=").append(delFlag);
        sb.append(", whCode=").append(whCode);
        sb.append("]");
        return sb.toString();
    }
}