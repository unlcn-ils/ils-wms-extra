package com.unlcn.ils.wms.base.businessDTO.outbound;

import java.io.Serializable;
import java.util.Date;

/**
 * 用于出库记录列表查询参数封装-hs
 * <p>
 * 2018-1-23 君马出库记录查询页面增加“物料代码”、“配置”、“颜色代码”、“颜色”字段显示，导出的文件中也需要包含这四个字段
 * </p>
 */
public class WmsOutboundTaskDTOForQuery extends WmsOutboundTaskDTO implements Serializable {
    private Date vehicleOutTime;//板车登记时间--重庆库
    private String materialCode;
    private String configuration;
    private String colourCode;
    private String colourName;
    private String spCarrier;
    private String spDealer;

    public String getSpDealer() {
        return spDealer;
    }

    public void setSpDealer(String spDealer) {
        this.spDealer = spDealer;
    }

    public String getSpCarrier() {
        return spCarrier;
    }

    public void setSpCarrier(String spCarrier) {
        this.spCarrier = spCarrier;
    }

    public String getMaterialCode() {
        return materialCode;
    }

    public void setMaterialCode(String materialCode) {
        this.materialCode = materialCode;
    }

    public String getConfiguration() {
        return configuration;
    }

    public void setConfiguration(String configuration) {
        this.configuration = configuration;
    }

    public String getColourCode() {
        return colourCode;
    }

    public void setColourCode(String colourCode) {
        this.colourCode = colourCode;
    }

    public String getColourName() {
        return colourName;
    }

    public void setColourName(String colourName) {
        this.colourName = colourName;
    }

    public Date getVehicleOutTime() {
        return vehicleOutTime;
    }

    public void setVehicleOutTime(Date vehicleOutTime) {
        this.vehicleOutTime = vehicleOutTime;
    }
}
