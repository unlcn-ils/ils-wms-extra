package com.unlcn.ils.wms.base.dto;

import java.util.Date;

/**
 * Created by lenovo on 2017/10/21.
 */
public class ReturnCarLineDTO {

    private Long rcId;

    private String rcBorrowNo;

    private String rcReturnNo;

    private String rcReturnName;

    private String rcReturnDepartment;

    private String rcReturnTel;

    private Date rcReturnDate;

    private String rcWhcode;

    private String bcBorrowNo;
    
    private String uploadKey;
    
    private String uploadPath;

    public Long getRcId() {
        return rcId;
    }

    public void setRcId(Long rcId) {
        this.rcId = rcId;
    }

    public String getRcBorrowNo() {
        return rcBorrowNo;
    }

    public void setRcBorrowNo(String rcBorrowNo) {
        this.rcBorrowNo = rcBorrowNo;
    }

    public String getRcReturnNo() {
        return rcReturnNo;
    }

    public void setRcReturnNo(String rcReturnNo) {
        this.rcReturnNo = rcReturnNo;
    }

    public String getRcReturnName() {
        return rcReturnName;
    }

    public void setRcReturnName(String rcReturnName) {
        this.rcReturnName = rcReturnName;
    }

    public String getRcReturnDepartment() {
        return rcReturnDepartment;
    }

    public void setRcReturnDepartment(String rcReturnDepartment) {
        this.rcReturnDepartment = rcReturnDepartment;
    }

    public String getRcReturnTel() {
        return rcReturnTel;
    }

    public void setRcReturnTel(String rcReturnTel) {
        this.rcReturnTel = rcReturnTel;
    }

    public Date getRcReturnDate() {
        return rcReturnDate;
    }

    public void setRcReturnDate(Date rcReturnDate) {
        this.rcReturnDate = rcReturnDate;
    }

    public String getRcWhcode() {
        return rcWhcode;
    }

    public void setRcWhcode(String rcWhcode) {
        this.rcWhcode = rcWhcode;
    }

    public String getBcBorrowNo() {
        return bcBorrowNo;
    }

    public void setBcBorrowNo(String bcBorrowNo) {
        this.bcBorrowNo = bcBorrowNo;
    }

	public String getUploadKey() {
		return uploadKey;
	}

	public void setUploadKey(String uploadKey) {
		this.uploadKey = uploadKey;
	}
	
	public String getUploadPath() {
		return uploadPath;
	}

	public void setUploadPath(String uploadPath) {
		this.uploadPath = uploadPath;
	}

	@Override
    public String toString() {
        return "ReturnCarLineDTO{" +
                "rcId=" + rcId +
                ", rcBorrowNo='" + rcBorrowNo + '\'' +
                ", rcReturnNo='" + rcReturnNo + '\'' +
                ", rcReturnName='" + rcReturnName + '\'' +
                ", rcReturnDepartment='" + rcReturnDepartment + '\'' +
                ", rcReturnTel='" + rcReturnTel + '\'' +
                ", rcReturnDate=" + rcReturnDate +
                ", rcWhcode='" + rcWhcode + '\'' +
                ", bcBorrowNo='" + bcBorrowNo + '\'' +
                '}';
    }
}
