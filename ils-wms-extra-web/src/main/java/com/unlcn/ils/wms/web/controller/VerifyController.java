package com.unlcn.ils.wms.web.controller;

import cn.huiyunche.commons.domain.ResultDTO;
import com.unlcn.ils.wms.backend.bo.VerifyBO;
import com.unlcn.ils.wms.backend.service.verify.RedisService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.Objects;

/**
 * @Auther linbao
 * @Date 2017-10-18
 */
@RestController
@RequestMapping("/verify")
public class VerifyController {

    private Logger LOGGER = LoggerFactory.getLogger(VerifyController.class);

    @Autowired
    private RedisService redisService;


    /**
     * 验证接口
     *
     * @param token
     * @return
     */
    @RequestMapping(value = "/verifyUser/{token}", method = RequestMethod.GET)
    public ResultDTO<Object> verify(@PathVariable String token){
        ResultDTO<Object> resultDTO = new ResultDTO<>(true, null, "验证成功");
        try {
            VerifyBO verifyBO = redisService.verifyUser(token);
            if (Objects.equals(verifyBO, null)){
                resultDTO.setSuccess(false);
                resultDTO.setMessage("账户认证已失效, 请重新登录");
            }else {
                resultDTO.setData(verifyBO);
            }
        } catch (Exception e) {
            LOGGER.error("VerifyController.verify error: {}", e);
            resultDTO.setSuccess(false);
            resultDTO.setMessage("账户验证失败, 请重新登录");
        }
        return resultDTO;
    }
}
