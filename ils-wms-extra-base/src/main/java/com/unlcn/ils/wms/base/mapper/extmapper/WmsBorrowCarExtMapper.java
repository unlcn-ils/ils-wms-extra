package com.unlcn.ils.wms.base.mapper.extmapper;

import com.unlcn.ils.wms.base.dto.BorrowDetailDTO;
import com.unlcn.ils.wms.base.dto.BorrowHistoryDTO;
import com.unlcn.ils.wms.base.dto.BorrowPrintDetailDTO;
import com.unlcn.ils.wms.base.model.stock.WmsBorrowCar;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

public interface WmsBorrowCarExtMapper {
    /**
     * 借车等级列表统计
     * @param paramsMap
     * @return
     */
    Integer borrowLineCount(@Param("paramsMap") Map<String, Object> paramsMap);

    /**
     * 借车等级列表
     * @param paramsMap
     * @return
     */
    List<WmsBorrowCar> borrowLine(@Param("paramsMap") Map<String, Object> paramsMap);

    /**
     * 借还记录列表统计
     * @param paramsMap
     * @return
     */
    Integer borrowReturnHistoryCount(@Param("paramsMap") Map<String, Object> paramsMap);

    /**
     * 借还记录列表
     * @param paramsMap
     * @return
     */
    List<BorrowHistoryDTO> borrowReturnHistory(@Param("paramsMap") Map<String, Object> paramsMap);

    /**
     * 查询借车单详情
     * @param bcId
     * @return
     */
    List<BorrowDetailDTO> queryBorrowDetail(@Param("bcId") Long bcId);

    /**
     * 根据id批量查询
     * @param ids
     * @return
     */
    List<WmsBorrowCar> batchSelectByIds(@Param("ids") List<Long> ids);

    /**
     * 批量删除借车单
     * @param bcIds
     */
    void deleteBorrow(@Param("bcIds") List<Long> bcIds);

    /**
     * 查询打印借车单详细信息
     * @param bcId
     * @return
     */
    BorrowPrintDetailDTO queryPrintInfo(@Param("bcId") Long bcId);

    /**
     * 查询最大主键值
     * @return
     */
    Integer selectMaxId();
}