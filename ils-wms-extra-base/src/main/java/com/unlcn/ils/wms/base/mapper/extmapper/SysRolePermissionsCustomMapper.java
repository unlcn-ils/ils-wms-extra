package com.unlcn.ils.wms.base.mapper.extmapper;

import java.util.List;

import com.unlcn.ils.wms.base.model.sys.SysRolePermissions;

public interface SysRolePermissionsCustomMapper {
    
	/**
	 * 
	 * @Title: insertBatch 
	 * @Description: 批量新增
	 * @param @param list
	 * @param @return     
	 * @return Integer    返回类型 
	 * @throws 
	 *
	 */
    Integer insertBatch(List<SysRolePermissions> list);
	
}