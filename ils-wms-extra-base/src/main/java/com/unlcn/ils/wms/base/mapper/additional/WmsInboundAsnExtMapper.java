package com.unlcn.ils.wms.base.mapper.additional;

import com.unlcn.ils.wms.base.model.inbound.WmsAsnTemp;
import com.unlcn.ils.wms.base.model.inbound.WmsInboundAsn;

import java.util.List;
import java.util.Map;

/**
 * Created by DELL on 2017/9/22.
 */
public interface WmsInboundAsnExtMapper {
    /**
     * 查询ASN分页
     * @param paramMap
     * @return
     */
    List<Map<String,Object>> queryForList(Map<String, Object> paramMap);

    /**
     * 查询ASN分页
     * @param paramMap
     * @return
     */
    List<Map<String,Object>> queryNewForList(Map<String, Object> paramMap);

    /**
     * 查询ASN - 不分页
     * @param paramMap
     * @return
     */
    List<Map<String,Object>> queryNewForListByTime(Map<String, Object> paramMap);

    /**
     * 查询ASN记录数
     * @param paramMap
     * @return
     */
    Integer queryForCount(Map<String, Object> paramMap);

    /**
     * 查询ASN记录数
     * @param paramMap
     * @return
     */
    Integer queryNewForCount(Map<String, Object> paramMap);
}
