package com.unlcn.ils.wms.backend.bo.pickup;

/**
 * Created by Lie on 2017/7/22.
 */
public class TmsDamageDeliverBO {
    private Long pickId;        //验车ID
    private String vin;         //车架号
    private String picKey;         //图片KEY

    public Long getPickId() {
        return pickId;
    }

    public void setPickId(Long pickId) {
        this.pickId = pickId;
    }

    public String getVin() {
        return vin;
    }

    public void setVin(String vin) {
        this.vin = vin;
    }

    public String getPicKey() {
        return picKey;
    }

    public void setPicKey(String picKey) {
        this.picKey = picKey;
    }

    @Override
    public String toString() {
        return "TmsDamageDeliverBO{" +
                "pickId=" + pickId +
                ", vin='" + vin + '\'' +
                ", picKey='" + picKey + '\'' +
                '}';
    }
}
