package com.unlcn.ils.wms.base.dto;

public class WmsInventoryLocationChangePrepareByVinDTO {
    private String invlocVin;
    private String invlocStatus;
    private String invlocZoneCode;
    private String invlocZoneName;
    private String invlocLocCode;
    private String invlocLocName;
    private String invlocWhCode;
    private Long invlocId;
    private Long invlocInvId;
    private String invMaterialCode;
    private String invVehicleSpecName;
    private String invColorCode;

    public String getInvlocLocName() {
        return invlocLocName;
    }

    public void setInvlocLocName(String invlocLocName) {
        this.invlocLocName = invlocLocName;
    }

    public String getInvlocZoneName() {
        return invlocZoneName;
    }

    public void setInvlocZoneName(String invlocZoneName) {
        this.invlocZoneName = invlocZoneName;
    }

    public String getInvlocVin() {
        return invlocVin;
    }

    public void setInvlocVin(String invlocVin) {
        this.invlocVin = invlocVin;
    }

    public String getInvlocStatus() {
        return invlocStatus;
    }

    public void setInvlocStatus(String invlocStatus) {
        this.invlocStatus = invlocStatus;
    }

    public String getInvlocZoneCode() {
        return invlocZoneCode;
    }

    public void setInvlocZoneCode(String invlocZoneCode) {
        this.invlocZoneCode = invlocZoneCode;
    }

    public String getInvlocLocCode() {
        return invlocLocCode;
    }

    public void setInvlocLocCode(String invlocLocCode) {
        this.invlocLocCode = invlocLocCode;
    }

    public String getInvlocWhCode() {
        return invlocWhCode;
    }

    public void setInvlocWhCode(String invlocWhCode) {
        this.invlocWhCode = invlocWhCode;
    }

    public Long getInvlocId() {
        return invlocId;
    }

    public void setInvlocId(Long invlocId) {
        this.invlocId = invlocId;
    }

    public Long getInvlocInvId() {
        return invlocInvId;
    }

    public void setInvlocInvId(Long invlocInvId) {
        this.invlocInvId = invlocInvId;
    }

    public String getInvMaterialCode() {
        return invMaterialCode;
    }

    public void setInvMaterialCode(String invMaterialCode) {
        this.invMaterialCode = invMaterialCode;
    }

    public String getInvVehicleSpecName() {
        return invVehicleSpecName;
    }

    public void setInvVehicleSpecName(String invVehicleSpecName) {
        this.invVehicleSpecName = invVehicleSpecName;
    }

    public String getInvColorCode() {
        return invColorCode;
    }

    public void setInvColorCode(String invColorCode) {
        this.invColorCode = invColorCode;
    }
}
