package com.unlcn.ils.wms.base.mapper.inbound;

import com.unlcn.ils.wms.base.model.inbound.WmsAsnTemp;
import org.apache.ibatis.annotations.Param;

import java.util.ArrayList;
import java.util.List;

public interface WmsAsnTempExpandMapper {


    int insertBatch(@Param("asnTempList") ArrayList<WmsAsnTemp> asnTempList);

    /**
     * 颜色列表
     *
     * @param whCode
     * @return
     */
    List<String> queryColorList(@Param("whCode") String whCode);
}