/**
 * Created by lsj on 2017/8/17.
 */
'use strict';
angular.module('wms.constant',[])
    .constant('constant',{
        billsType : {   //单据类型
            systemSchedul : '10', //系统调度
            manualEntry : '20'    //手工录入
        },
        inspectionResults : { //检验结果
            asnOrderPass : '10',    //合格
            asnOrderNotpassInRepair : '20', //不合格-库位返修
            asnOrderNotpassOutRepair : '30',    //不合格-委外返工
            asnOrderNotpass : '40'  //不合格-退厂
        },
        orderStatus : { //订单状态
            wmsInboundCreate : '10',    //未收货
            wmsInboundWaitAllocation : '20',    //已收货
            wmsInboundFinishAllocation : '30',  //已分配
            wmsInboundConfirmAllocation : '40', //待入库
            wmsInboundFinish : '50' //已入库
        },
        billsTypeName : {   //单据类型名称
            systemSchedul : '系统调度',
            manualEntry : '手工录入'
        },
        inspectionResultsName : {   //检验结果名称
            asnOrderPass : '合格',
            asnOrderNotpassInRepair : '不合格-库内返修',
            asnOrderNotpassOutRepair : '不合格-委外返工',
            asnOrderNotpass : '不合格-退厂'
        },
        orderStatusName : { //订单状态名称
            wmsInboundCreate : '未收货',
            wmsInboundWaitAllocation : '已收货',
            wmsInboundFinishAllocation : '已分配',
            wmsInboundConfirmAllocation : '待入库',
            wmsInboundFinish : '已入库'
        },
        status : {  //状态
            enable : 10,   //可用
            disable : 20    //禁用
        },
        statusName : {  //状态名称
            enable : '可用',
            disable : '禁用'
        },
        page : {    //分页
            pageSize : 10,
            pageNo : 1
        },
        moreText : {    //更多展开收起文本
            packUp : '收起',
            spread : '更多条件'
        },
        distributionType : {    //分配类型
            manual : 10,
            auto : 20
        },
        stockUnitStatus : { //库位的状态
            start : 'Y',
            stop : 'N'
        },stockUnitStatusName : { //库位的状态 name
            start : '启用',
            stop : '禁用'
        },
        repairType : {  //维修类型
            inner : 10, //委内维修
            outer : 20  //委外维修
        },
        repairTypeName : {  //维修类型 Name
            inner : '委内维修',
            outer : '委外维修'
        },
        repairStatus : {    //维修状态
            unprocessed : 10,   //未处理
            maintenance : 20,   //维修中
            finish : 30 //已完成
        },
        repairStatusName : {    //维修状态 Name
            unprocessed : '未处理',
            maintenance : '维修中',
            finish : '已完成'
        },
        repairResult : {    //维修结果
            qualified : 10,   //合格
            disqualification : 20,   //不合格
            cancellinStocks : 30 //退库
        },
        repairResultName : {    //维修结果 name
            qualified : '合格',   //合格
            disqualification : '不合格',   //不合格
            cancellinStocks : '退库' //退库
        },
        warehouse : {    //出库状态
            unprocessed : 10,   //未处理
            material : 20,   //已备料
            outbound : 30, //待出库
            haveOutbound : 40   //已出库
        },
        warehouseName : {    //出库状态 name
            unprocessed : '未处理',
            material : '已备料',
            outbound : '待出库',
            haveOutbound : '已出库'
        },
        defaultDropDownNullData  : {    //空数据
            name : '',
            value : ''
        },
        strategyConditionType : {    //策略类型
            shipper : 10,   //货主
            vehicle : 20    //车型
        },
        strategyConditionTypeName : {    //策略类型
            shipper : '货主',
            vehicle : '车型'
        },
        strategyOperator : {    //运算符
            xd : '==',
            bdy : '!=',
            xy : '<',
            dy : '>',
            xydy : '<=',
            dydy : '>=',
            bh : '包含',
            bbh : '不包含',
            s : '是',
            bs : '不是'
        }
    })