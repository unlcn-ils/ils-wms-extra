package com.unlcn.ils.wms.backend.enums;

/**
 * Created by Nicky on 17/11/8.
 */
public enum  TmsUrlTypeEnum {

    BI_PICK_DATE(10,"提车推移图"),
    BI_FRONT_SHIPMENT(20,"前端发运推移图"),
    BI_OVERREASON(30,"超期原因分析"),
    BI_TRANSMODE(40,"发运模式推移"),
    BI_FOREPROGRESS(50,"前端在途"),
    BI_INOUTDRAYCHART(60,"板车进出场图"),
    BI_INOUTDRAYLIST(70,"板车进出场列表"),
    BI_DISTRIBUTIONCHART(80,"配送推移图"),
    BI_DISTRIBUTIONLINE(90,"配送列表");

    private final int value;
    private final String text;

    public int getValue() {
        return value;
    }

    public String getText() {
        return text;
    }


    TmsUrlTypeEnum(int value, String text){
        this.value = value;
        this.text = text;
    }

    public static TmsUrlTypeEnum getByValue(int value){
        for (TmsUrlTypeEnum temp : TmsUrlTypeEnum.values()) {
            if(temp.getValue() == value){
                return temp;
            }
        }
        return null;
    }

}
