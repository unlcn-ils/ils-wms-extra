package com.unlcn.ils.wms.backend.service.inspectApp;

import com.unlcn.ils.wms.backend.bo.inspectAppBO.TmsInspectExcpBO;
import com.unlcn.ils.wms.backend.bo.inspectAppBO.TmsInspectExcpDetailBO;
import com.unlcn.ils.wms.base.dto.WmsInspectForAppDTO;
import com.unlcn.ils.wms.base.dto.WmsOutboundInspectExcpDTO;

import java.util.List;

public interface WmsOutboundInspectExcpService {

    List<WmsOutboundInspectExcpDTO> getPositionExcpCountByInspectId(Long id, String whcode) throws Exception;

    void saveNoteExcp(TmsInspectExcpBO bo) throws Exception;

    List<TmsInspectExcpDetailBO> getInspectExcpListByPositionId(TmsInspectExcpBO bo) throws Exception;

    void deleteAllException(WmsInspectForAppDTO dto);
}
