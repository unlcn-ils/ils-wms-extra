/**
 * Created by houjianhui on 2017/7/19.
 */
'use strict';
//入库管理 ctrl
wmsCtrl.controller('inStorageBillCtrl',['$scope','inStorageBillService','dropDownService',function(scope,inStorageBillService,dropDownService){

    //仓库
    var shck = [
        {
            name:'我只是仓库',
            value:'1'
        },
        {
            name:'我只是仓库1',
            value:'2'
        },
        {
            name:'我只是仓库1',
            value:'3'
        }
    ]

    //品牌
    var pp = [
        {
            name:'我只是品牌',
            value:'1'
        },
        {
            name:'我只是品牌1',
            value:'2'
        },
        {
            name:'我只是品牌2',
            value:'3'
        }
    ]

    //新增入库单 click
    scope.addReceivingNoteClick = function(){
        //获取货主
        dropDownService.getCustomerInfo()
            .then(function(customerResult){
                if(customerResult.success){
                    //获取车型
                    dropDownService.getVehicleSpec()
                        .then(function(vehicleSpecResult){
                            if(vehicleSpecResult.success){
                                scope.Tools.dialog({
                                    controller:'inStorageBillCtrl.dialog',
                                    template : '../public/template/inStorageManagement/inStorageBill/in-storage-bill-form.html',
                                    closeByDocument : false,
                                    closeByEscape : false,
                                    data:{
                                        billsType : scope.Tools.dropDown.billsTypeDropDown, //单据类型
                                        shipper:customerResult.data, //货主
                                        warehouse : shck,   //仓库
                                        vehicleSpec : vehicleSpecResult.data, //车型
                                        brand : pp, //品牌
                                        dropDownDefault : {    //下拉框默认值
                                            billsType : scope.Tools.dropDown.billsTypeDropDown[0],//设置默认值
                                            shipper : customerResult.data[0], //货主默认值
                                            warehouse : shck[0], //仓库默认值
                                            vehicleSpec : vehicleSpecResult.data[0],   //车型默认值
                                            brand : pp[0]   //品牌默认值
                                        },
                                        receipt : {}
                                    },
                                    width : 770,
                                    title : '新增入库单',
                                    scope : scope
                                })
                            }else{
                                scope.Tools.alert(vehicleSpecResult.message);
                            }
                        })

                }else{
                    scope.Tools.alert(customerResult.message);
                }
            })
    }

    //更新入库单 click
    scope.updateReceivingNoteClick = function(){

        var indexArr = scope.Tools.getTableSelectItemIndex('.inStorageTable');  //获取table所选的下标

        if(indexArr.length == 0){
            scope.Tools.alert('请选择需要更新的数据');
            return;
        }

        if(indexArr.length > 1){
            scope.Tools.alert('只能选择一条数据进行更新');
            return;
        }
        //根据id获取入库单
        inStorageBillService.getReceiptById(scope.inStorageList[indexArr[0]].odId)
            .then(function(result){
                if(result.success){
                    //设置仓库默认值
                    var defaultCk = {};
                    for(var i = 0; i < shck.length;i++){
                        if(shck[i].value == result.data.odWhId){
                            defaultCk = shck[i];
                            break;
                        }
                    }

                    //获取货主
                    dropDownService.getCustomerInfo()
                        .then(function(customerResult){
                            if(customerResult.success){
                                //获取车型
                                dropDownService.getVehicleSpec()
                                    .then(function(vehicleSpecResult){
                                        if(vehicleSpecResult.success){
                                            //设置货主默认值
                                            var defaultShipper = {};
                                            for(var i = 0;i < customerResult.data.length;i++){
                                                if(customerResult.data[i].customerId){  //判断是否存在货主
                                                    if(result.data.odCustomerCode == customerResult.data[i].customerId.code){
                                                        defaultShipper = customerResult.data[i];
                                                        break;
                                                    }
                                                }
                                            }
                                            //设置车型默认值
                                            var defaultVehicleSpec = {};
                                            for(var i = 0;i < vehicleSpecResult.data.length;i++){
                                                if(result.data.oddVehicleSpecCode == vehicleSpecResult.data[i].code){
                                                    defaultVehicleSpec = vehicleSpecResult.data[i];
                                                    break;
                                                }
                                            }

                                            scope.Tools.dialog({
                                                controller:'inStorageBillCtrl.dialog',
                                                template : '../public/template/inStorageManagement/inStorageBill/in-storage-bill-form.html',
                                                closeByDocument : false,
                                                closeByEscape : false,
                                                data:{
                                                    billsType : scope.Tools.dropDown.billsTypeDropDown, //单据类型
                                                    shipper:customerResult.data, //货主
                                                    warehouse : shck,   //仓库
                                                    vehicleSpec : vehicleSpecResult.data, //车型
                                                    brand : pp, //品牌
                                                    dropDownDefault : {    //下拉框默认值
                                                        billsType : scope.Tools.getBillsTypeDropDownByKey(result.data.asnBillType),//设置默认值
                                                        shipper : defaultShipper, //货主默认值
                                                        warehouse : defaultCk, //仓库默认值
                                                        vehicleSpec : defaultVehicleSpec/*,   //车型默认值
                                                        brand : pp[0]   //品牌默认值*/
                                                    },
                                                    receipt : result.data
                                                },
                                                width : 770,
                                                title : '更新入库单',
                                                scope : scope
                                            })
                                        }else{
                                            scope.Tools.alert(vehicleSpecResult.message);
                                        }
                                    })
                            }else{
                                scope.Tools.alert(customerResult.message);
                            }
                        })
                }else{
                    scope.Tools.alert(result.message);
                }
            })
    }

    //删除入库单 click
    scope.deleteReceivingNoteClick = function(){

        var indexArr = scope.Tools.getTableSelectItemIndex('.inStorageTable');  //获取table所选的下标

        var inStorageList = scope.Tools.getDataTOIndex(indexArr,scope.inStorageList);

        if(inStorageList.length == 0){
            scope.Tools.alert('请选择需要删除的数据');
            return;
        }

        scope.Tools.dialogConfirm('确定要删除选中的数据吗？')
            .then(function(res){
                if(res){
                    inStorageBillService.deleteAsnOrder(inStorageList)
                        .then(function(result){
                            if(result.success){
                                scope.fristSelect = true;   //设置为第一次查询
                                scope.list(scope.listParam());
                                scope.Tools.alert('删除数据成功');
                            }else{
                                scope.Tools.alert(result.message);
                            }
                        })
                }
            })
    }

    //更多条件默认值
    scope.moreText = scope.constant.moreText.spread;
    //更多条件
    scope.moreClick = function(){
        scope.otherForm = !scope.otherForm;
        if(scope.otherForm){
            scope.moreText = scope.constant.moreText.packUp;
        }else{
            scope.moreText = scope.constant.moreText.spread;
        }
    }

    /*状态数据*/
    scope.dropDownList = {
        status : scope.Tools.dropDown.orderStatusDropDown.concat()    //订单状态
    }
    scope.dropDownList.status.unshift(scope.constant.defaultDropDownNullData);  //向前插入空值

    //设置默认值
    scope.dropDownDefault = {
        status :  scope.dropDownList.status[0], //状态
    }

    //获取货主
    dropDownService.getCustomerInfo()
        .then(function(result){
            if(result.success){
                scope.dropDownList.shipper = result.data;
                scope.dropDownList.shipper.unshift(scope.constant.defaultDropDownNullData); //插入空值
                scope.dropDownDefault.shipper = scope.dropDownList.shipper[0];  //设置默认值
            }else{
                scope.Tools.alert(result.message);
            }
        })

    //获取车型
    dropDownService.getVehicleSpec()
        .then(function(result){
            if(result.success){
                scope.dropDownList.vehicleSpec = result.data;
                scope.dropDownList.vehicleSpec.unshift(scope.constant.defaultDropDownNullData); //插入空值
                scope.dropDownDefault.vehicleSpec = scope.dropDownList.vehicleSpec[0];  //设置默认值
            }else{
                scope.Tools.alert(result.message);
            }
        })

    //列表查询接口参数构造
    scope.listParam = function(){

        if(!scope.search){
            scope.search = {};
        }

        scope.search.pageSize = scope.pageSize;    //设置最大页
        if(!scope.fristSelect){ //不是第一次查询 则设置当前页
            scope.search.pageNo = scope.pageNo;
        }

        if(scope.otherForm){    //展开了更多
            scope.search.oddVehicleSpecName = scope.dropDownDefault.vehicleSpec.name;   //车型
            scope.search.odCustomerName = scope.dropDownDefault.shipper.customerId ? scope.dropDownDefault.shipper.customerId.name : '';   //货主
            scope.search.odStatus = scope.dropDownDefault.status.value;    //订单状态
            return scope.search;
        }else{  //没有展开
            return {
                odWaybillNo : scope.search.odWaybillNo, //单据号
                pageSize : scope.search.pageSize,   //最大号
                pageNo : scope.search.pageNo    //当前页
            }
        }
    }

    //列表
    scope.fristSelect = true;   //判断是否是第一次查询
    scope.list = function(params){
        //设置分页参数
        if(!params){
            params = {};
        }
        params.pageSize = params.pageSize ? params.pageSize : scope.constant.page.pageSize;
        params.pageNo = params.pageNo ? params.pageNo : scope.constant.page.pageNo

        inStorageBillService.inStorageList(params)
            .then(function(result){
                if(result.success){
                    scope.inStorageList = result.data.asnOrderList;
                    if(scope.fristSelect){
                        scope.Tools.page(document.getElementById('pages'),{
                            pages : result.data.inboundAsnOrderBO.totalPage,
                            curr : result.data.inboundAsnOrderBO.pageNo,
                            count : result.data.inboundAsnOrderBO.totalRecord
                        },function(obj, first){
                            scope.fristSelect = first;  //设置状态
                            //初始化多选框
                            scope.Tools.initCheckBox();
                            if(!first){ //判断不是第一次加载
                                var param = scope.listParam();
                                param.pageNo = obj.curr;    //当前页
                                param.pageSize = obj.limit; //最大页
                                scope.pageSize = obj.limit; //设置最大页
                                scope.pageNo = obj.curr; //设置当前页
                                scope.list(param);
                            }
                        })
                    }else{
                        //初始化多选框
                        scope.Tools.initCheckBox();
                    }

                }else{
                    scope.Tools.alert(result.message);
                }
            })
    }

    //列表
    scope.list();

    //回车按下查询
    scope.searchListKeyDown = function(e){
        var keycode = window.event ? e.keyCode:e.which;
        if(keycode == 13){  //回车键
            scope.fristSelect = true;   //设置为第一次查询
            scope.list(scope.listParam());
            e.preventDefault();
        }
    }

    //查询 click
    scope.searchClick = function(){
        scope.fristSelect = true;   //设置为第一次查询
        scope.list(scope.listParam());
    }

    //重置 click
    scope.resetClick = function(){
        scope.search = {};
    }

    //刷新 click
    scope.refreshClick = function(){
        scope.searchClick();
    }

}]).controller('inStorageBillCtrl.dialog',['$scope','inStorageBillService',function(scope,inStorageBillService){   //入库管理ctrl dialog

    //新增或者更新入库单
    scope.saveOrUpdateReceiptClick = function(){
        //货主
        if(scope.ngDialogData.dropDownDefault.shipper){
            if(scope.ngDialogData.dropDownDefault.shipper.customerId){  //判断有没有货主
                scope.ngDialogData.receipt.odCustomerCode = scope.ngDialogData.dropDownDefault.shipper.customerId.code;
                scope.ngDialogData.receipt.odCustomerName = scope.ngDialogData.dropDownDefault.shipper.customerId.name;
            }
        }
        //单据类型
        scope.ngDialogData.receipt.odWaybillType = scope.constant.billsType.manualEntry;  //默认写死传递数据 手工录入

        //入库仓库
        if(scope.ngDialogData.dropDownDefault.warehouse){
            scope.ngDialogData.receipt.odWhId = scope.ngDialogData.dropDownDefault.warehouse.value;
            scope.ngDialogData.receipt.odWhCode = scope.ngDialogData.dropDownDefault.warehouse.name;
        }

        //车型
        if(scope.ngDialogData.dropDownDefault.vehicleSpec){
            scope.ngDialogData.receipt.oddVehicleSpecCode = scope.ngDialogData.dropDownDefault.vehicleSpec.code;
            scope.ngDialogData.receipt.oddVehicleSpecName = scope.ngDialogData.dropDownDefault.vehicleSpec.name;
        }
        /*

        //品牌
        if(scope.ngDialogData.dropDownDefault.brand){
            scope.ngDialogData.receipt.asndtBrandCode = scope.ngDialogData.dropDownDefault.brand.value;
            scope.ngDialogData.receipt.asndtBrandName = scope.ngDialogData.dropDownDefault.brand.name;
        }*/

        //判断是否有id 如果没有id则为新增、有则为修改
        if(scope.ngDialogData.receipt.odId){   //修改
            //更新入库单
            inStorageBillService.updateAsnOrder(scope.ngDialogData.receipt)
                .then(function(result){
                    if(result.success){
                        scope.Tools.alert('更新入库单成功');
                        scope.list(scope.listParam());
                        scope.closeThisDialog();
                    }else{
                        scope.Tools.alert(result.message);
                    }
                })
        }else{  //新增
            //新增入库单
            inStorageBillService.addReceipt(scope.ngDialogData.receipt)
                .then(function(result){
                    if(result.success){
                        scope.Tools.alert('新增入库单成功');
                        scope.$parent.fristSelect = true;   //设置为第一次查询
                        scope.list(scope.listParam());
                        scope.closeThisDialog();
                    }else{
                        scope.Tools.alert(result.message);
                    }
                })
        }
    }
}])
    .controller('inputWorkCtrl',['$scope','inStorageBillService','stockUnitStrategyService','dropDownService',function(scope,inStorageBillService,stockUnitStrategyService,dropDownService){  //入库作业

        //收货质检 click
        scope.receivingQualityTestingClick = function(){

            var indexArr = scope.Tools.getTableSelectItemIndex('.inputWorkTable');  //获取table所选的下标

            var inputWorkList = scope.Tools.getDataTOIndex(indexArr,scope.inputWorkList);

            if(inputWorkList.length == 0){
                scope.Tools.alert('请选择需要质检的数据');
                return;
            }

            //判断数据是否是未收货的数据
            for(var i = 0; i < inputWorkList.length;i++){
                if(inputWorkList[i].odStatus != scope.constant.orderStatus.wmsInboundCreate){
                    scope.Tools.alert('只能选择未收货数据');
                    return;
                }
            }

            scope.Tools.dialog({
                controller : 'inputWorkCtrl.dialog',
                template : '../public/template/inStorageManagement/inputWork/receiving-quality-testing-form.html',
                closeByDocument : false,
                closeByEscape : false,
                data : {
                    qualityList : inputWorkList,  //质检列表
                    inspectionResults : scope.Tools.dropDown.inspectionResultsDropDown //检验结果
                },
                width : 800,
                title : '收货质检',
                scope : scope
            })
        }

        //手工分配
        scope.manuallyAssignClick = function(){

            var indexArr = scope.Tools.getTableSelectItemIndex('.inputWorkTable');  //获取table所选的下标

            var manuallyAssignList = scope.Tools.getDataTOIndex(indexArr,scope.inputWorkList);

            if(manuallyAssignList.length == 0){
                scope.Tools.alert('请选择需要手工分配的数据');
                return;
            }

            //判断数据是否是已收货的数据
            for(var i = 0; i < manuallyAssignList.length;i++){

                if(manuallyAssignList[i].odStatus != scope.constant.orderStatus.wmsInboundWaitAllocation || manuallyAssignList[i].odCheckResult != scope.constant.inspectionResults.asnOrderPass){
                    scope.Tools.alert('只能选择已收货且合格的数据');
                    return;
                }
            }

            //库区下拉集合
            var reservoirDropArr = new Array();
            //库位下拉集合
            var storageLocationDropArr = new Array();
            angular.forEach(manuallyAssignList,function(value,key){
                //TODO 001为自定义数据 暂时没有动态获取的功能
                var whCode = '001';
                //查询库区
                inStorageBillService.reservoirList(whCode)
                    .then(function(result){
                        if(result.success){
                            reservoirDropArr[key] = result.data;
                            //库位
                            inStorageBillService.storageLocationList(whCode,result.data[0].locZoneId)
                                .then(function(res){
                                    if(res.success){
                                        storageLocationDropArr[key] = res.data;
                                        if(manuallyAssignList.length - 1 == key){   //说明已经取到最后一个了

                                            scope.Tools.dialog({
                                                controller : 'inputWorkCtrl.dialog',
                                                template : '../public/template/inStorageManagement/inputWork/manually-assign-form.html',
                                                closeByDocument : false,
                                                closeByEscape : false,
                                                data : {
                                                    manuallyAssignList : manuallyAssignList,
                                                    storageLocationDropArr : storageLocationDropArr,
                                                    reservoirDropArr : reservoirDropArr
                                                },
                                                width : 550,
                                                title : '手工分配',
                                                scope : scope
                                            })

                                        }

                                    }else{
                                        scope.Tools.alert(res.message);
                                    }
                                })

                        }else{
                            scope.Tools.alert(result.message);
                        }
                    })
            })
        }

        //自动分配
        scope.autoAssignClick = function(){

            var indexArr = scope.Tools.getTableSelectItemIndex('.inputWorkTable');  //获取table所选的下标

            var autoAssignList = scope.Tools.getDataTOIndex(indexArr,scope.inputWorkList);

            if(autoAssignList.length == 0){
                scope.Tools.alert('请选择需要手工分配的数据');
                return;
            }

            //判断数据是否是已收货的数据
            for(var i = 0; i < autoAssignList.length;i++){

                if(autoAssignList[i].odStatus != scope.constant.orderStatus.wmsInboundWaitAllocation || autoAssignList[i].odCheckResult != scope.constant.inspectionResults.asnOrderPass){
                    scope.Tools.alert('只能选择已收货且合格的数据');
                    return;
                }
            }

            //库区下拉集合
            var reservoirDropArr = new Array();
            //库位下拉集合
            var storageLocationDropArr = new Array();
            angular.forEach(autoAssignList,function(value,key){
                //TODO 001为自定义数据 暂时没有动态获取的功能
                var whCode = '001';
                //查询库区
                inStorageBillService.reservoirList(whCode)
                    .then(function(result){
                        if(result.success){
                            reservoirDropArr[key] = result.data;

                            if(autoAssignList.length - 1 == key){   //说明已经取到最后一个了
                                //库位规则
                                stockUnitStrategyService.list(whCode)
                                    .then(function(strategyResult){
                                        if(strategyResult.success){
                                            scope.Tools.dialog({
                                                controller:'inputWorkCtrl.dialog',
                                                template : '../public/template/inStorageManagement/inputWork/auto-assign-form.html',
                                                closeByDocument : false,
                                                closeByEscape : false,
                                                data : {
                                                    autoAssignList : autoAssignList,    //入库单数据
                                                    stockUnitStrategyList : strategyResult.data,    //库位规则
                                                    stockUnitStrategy : strategyResult.data[0], //规则默认值
                                                    storageLocationDropArr : storageLocationDropArr,    //库位
                                                    reservoirDropArr : reservoirDropArr //库区
                                                },
                                                width : 550,
                                                title : '自动分配',
                                                scope : scope
                                            })
                                        }else{
                                            scope.Tools.alert(result.message);
                                        }
                                    })
                            }
                        }else{
                            scope.Tools.alert(result.message);
                        }
                    })
            })
        }

        //打印二维码
        scope.printQrCodeClick = function () {

            var indexArr = scope.Tools.getTableSelectItemIndex('.inputWorkTable');  //获取table所选的下标

            var printQrCodeList = scope.Tools.getDataTOIndex(indexArr,scope.inputWorkList);

            if(printQrCodeList.length == 0){
                scope.Tools.alert('请选择需要打印的数据');
                return;
            }

            //判断数据是否是已分配的数据
            for(var i = 0; i < printQrCodeList.length;i++){
                if(printQrCodeList[i].odStatus != scope.constant.orderStatus.wmsInboundConfirmAllocation && printQrCodeList[i].odStatus != scope.constant.orderStatus.wmsInboundFinish){
                    scope.Tools.alert('只能选择待入库、已入库的数据');
                    return;
                }
            }

            inStorageBillService.printBarcode(printQrCodeList)
                .then(function(result){
                    if(result.success){
                        scope.Tools.dialog({
                            controller : 'inputWorkCtrl.dialog',
                            template : '../public/template/inStorageManagement/inputWork/print-qr-code.html',
                            closeByDocument : false,
                            closeByEscape : false,
                            data : {
                                printList : result.data
                            },
                            width : 500,
                            title : '自动分配',
                            scope : scope
                        })
                    }else{
                        scope.Tools.alert(result.message);
                    }
                })
        }

        //更多条件默认值
        scope.moreText = scope.constant.moreText.spread;
        //更多条件
        scope.moreClick = function(){
            scope.otherForm = !scope.otherForm;
            if(scope.otherForm){
                scope.moreText = scope.constant.moreText.packUp;
            }else{
                scope.moreText = scope.constant.moreText.spread;
            }
        }

        /*状态数据*/
        scope.dropDownList = {
            status : scope.Tools.dropDown.orderStatusDropDown.concat(),    //订单状态
            billsType : scope.Tools.dropDown.billsTypeDropDown.concat(),    //单据类型
            inspectionResults : scope.Tools.dropDown.inspectionResultsDropDown.concat() //检验结果
        }
        scope.dropDownList.status.unshift(scope.constant.defaultDropDownNullData) //插入订单空数据
        scope.dropDownList.billsType.unshift(scope.constant.defaultDropDownNullData) //插入订单空数据
        scope.dropDownList.inspectionResults.unshift(scope.constant.defaultDropDownNullData) //插入订单空数据

        //设置默认值
        scope.dropDownDefault = {
            billsType : scope.dropDownList.status[0], //单据类型
            status :  scope.dropDownList.billsType[0], //状态
            inspectionResults : scope.dropDownList.inspectionResults[0] //检验结果
        }

        //获取货主
        dropDownService.getCustomerInfo()
            .then(function(result){
                if(result.success){
                    scope.dropDownList.shipper = result.data;
                    scope.dropDownList.shipper.unshift(scope.constant.defaultDropDownNullData); //插入空值
                    scope.dropDownDefault.shipper = scope.dropDownList.shipper[0];  //设置默认值
                }else{
                    scope.Tools.alert(result.message);
                }
            })

        //获取车型
        dropDownService.getVehicleSpec()
            .then(function(result){
                if(result.success){
                    scope.dropDownList.vehicleSpec = result.data;
                    scope.dropDownList.vehicleSpec.unshift(scope.constant.defaultDropDownNullData); //插入空值
                    scope.dropDownDefault.vehicleSpec = scope.dropDownList.vehicleSpec[0];  //设置默认值
                }else{
                    scope.Tools.alert(result.message);
                }
            })

        //列表查询接口参数构造
        scope.listParam = function(){

            if(!scope.search){
                scope.search = {};
            }

            scope.search.pageSize = scope.pageSize;    //设置最大页
            if(!scope.fristSelect){ //不是第一次查询 则设置当前页
                scope.search.pageNo = scope.pageNo;
            }

            if(scope.otherForm){    //展开了更多
                scope.search.oddVehicleSpecName = scope.dropDownDefault.vehicleSpec.name;   //车型
                scope.search.odCustomerName = scope.dropDownDefault.shipper.customerId ? scope.dropDownDefault.shipper.customerId.name : '';   //货主
                scope.search.odWaybillType = scope.dropDownDefault.billsType.value;   //单据类型
                scope.search.odStatus = scope.dropDownDefault.status.value;    //订单状态
                scope.search.odCheckResult = scope.dropDownDefault.inspectionResults.value;//检验结果
                return scope.search;
            }else{  //没有展开
                return {
                    odWaybillNo : scope.search.odWaybillNo, //单据号
                    pageSize : scope.search.pageSize,   //最大号
                    pageNo : scope.search.pageNo    //当前页
                }
            }
        }

        //列表
        scope.fristSelect = true;   //判断是否是第一次查询
        scope.list = function(params){
            //设置分页参数
            if(!params){
                params = {};
            }
            params.pageSize = params.pageSize ? params.pageSize : scope.constant.page.pageSize;
            params.pageNo = params.pageNo ? params.pageNo : scope.constant.page.pageNo

            inStorageBillService.inStorageList(params)
                .then(function(result){
                    if(result.success){
                        scope.inputWorkList = result.data.asnOrderList;
                        if(scope.fristSelect){
                            scope.Tools.page(document.getElementById('pages'),{
                                pages : result.data.inboundAsnOrderBO.totalPage,
                                curr : result.data.inboundAsnOrderBO.pageNo,
                                count : result.data.inboundAsnOrderBO.totalRecord
                            },function(obj, first){
                                scope.fristSelect = first;  //设置状态
                                //初始化多选框
                                scope.Tools.initCheckBox();
                                if(!first){ //判断不是第一次加载
                                    var param = scope.listParam();
                                    param.pageNo = obj.curr;    //当前页
                                    param.pageSize = obj.limit; //最大页
                                    scope.pageSize = obj.limit; //设置最大页
                                    scope.pageNo = obj.curr; //设置当前页
                                    scope.list(param);
                                }
                            })
                        }else{
                            //初始化多选框
                            scope.Tools.initCheckBox();
                        }

                    }else{
                        scope.Tools.alert(result.message);
                    }
                })
        }

        //列表
        scope.list();

        //回车按下查询
        scope.searchListKeyDown = function(e){
            var keycode = window.event ? e.keyCode:e.which;
            if(keycode == 13){  //回车键
                scope.fristSelect = true;   //设置为第一次查询
                scope.list(scope.listParam());
                e.preventDefault();
            }
        }

        //查询 click
        scope.searchClick = function(){
            scope.fristSelect = true;   //设置为第一次查询
            scope.list(scope.listParam());
        }

        //重置 click
        scope.resetClick = function(){
            scope.search = {};
        }

        //刷新 click
        scope.refreshClick = function(){
            scope.searchClick();
        }

        //确认分配 click
        scope.confirmAllocationClick = function(){
            var indexArr = scope.Tools.getTableSelectItemIndex('.inputWorkTable');  //获取table所选的下标

            var confirmAllocationList = scope.Tools.getDataTOIndex(indexArr,scope.inputWorkList);

            if(confirmAllocationList.length == 0){
                scope.Tools.alert('请选择需要确认分配的数据');
                return;
            }

            //判断数据是否是已分配的数据
            for(var i = 0; i < confirmAllocationList.length;i++){
                if(confirmAllocationList[i].odStatus != scope.constant.orderStatus.wmsInboundFinishAllocation){
                    scope.Tools.alert('只能选择已分配数据');
                    return;
                }
            }

            //确认分配
            inStorageBillService.confirmAllocation(confirmAllocationList)
                .then(function(result){
                    if(result.success){
                        scope.Tools.alert('确认分配成功');
                        scope.Tools.dialogConfirm('分配已完成，是否打印二维码？')
                            .then(function(r){
                                if(r){
                                    scope.Tools.alert('打印中.............');
                                }
                            })
                        scope.list({
                            pageNo : scope.pageNo
                        });
                    }else{
                        scope.Tools.alert(result.message);
                    }
                })
        }

        //确认入库
        scope.warehousingConfirmClick = function(){
            var indexArr = scope.Tools.getTableSelectItemIndex('.inputWorkTable');  //获取table所选的下标

            var warehousingConfirmList = scope.Tools.getDataTOIndex(indexArr,scope.inputWorkList);

            if(warehousingConfirmList.length == 0){
                scope.Tools.alert('请选择需要确认入库的数据');
                return;
            }

            //判断数据是否是待入库的数据
            for(var i = 0; i < warehousingConfirmList.length;i++){
                if(warehousingConfirmList[i].odStatus != scope.constant.orderStatus.wmsInboundConfirmAllocation){
                    scope.Tools.alert('只能选择待入库的数据');
                    return;
                }
            }

            //确认入库
            inStorageBillService.confirmInbound(warehousingConfirmList)
                .then(function(result){
                    if(result.success){
                        scope.Tools.alert('确认入库成功');

                        scope.list({
                            pageNo : scope.pageNo
                        });
                    }else{
                        scope.Tools.alert(result.message);
                    }
                })
        }
    }])
    .controller('inputWorkCtrl.dialog',['$scope','inStorageBillService',function(scope,inStorageBillService){   //入库作业 dialog

        //判断质检是否有数据
        if(scope.ngDialogData.qualityList){

            //选择默认值
            scope.ngDialogData.qualityResult = new Array();
            angular.forEach(scope.ngDialogData.qualityList,function(value,key){
                //默认选中合格
                scope.ngDialogData.qualityResult[key] = scope.ngDialogData.inspectionResults[0];
            })

            //收货确认click
            scope.receivingConfirmaClick = function(){

                angular.forEach(scope.ngDialogData.qualityList,function(value,key){
                    value.odCheckResult = scope.ngDialogData.qualityResult[key].value;
                })

                inStorageBillService.receivingInspection(scope.ngDialogData.qualityList)
                    .then(function(result){
                        if(result.success){
                            scope.Tools.alert('收货质检成功');
                            //在当前页刷新
                            scope.list(scope.listParam());
                            scope.closeThisDialog();
                        }else{
                            scope.Tools.alert(result.message);
                        }
                    })
            }
        }

        //判断人工分配是否有数据
        if(scope.ngDialogData.manuallyAssignList){

            //设置库区库位默认值
            scope.ngDialogData.dropDownDefault = {
                reservoirDrop : new Array(),    //库区
                storageLocationDrop : new Array()   //库位
            }
            angular.forEach(scope.ngDialogData.manuallyAssignList,function(value,key){
                scope.ngDialogData.dropDownDefault.reservoirDrop[key] = scope.ngDialogData.reservoirDropArr[0][0];
                scope.ngDialogData.dropDownDefault.storageLocationDrop[key] = scope.ngDialogData.storageLocationDropArr[0][0];
            })

            //选择库位
            scope.selectReservoirDropChang = function(item,manuallyAssign,index){
                //TODO 默认写死数据
                var whCode = '001';
                inStorageBillService.storageLocationList(whCode,item.locZoneId)
                    .then(function(result){
                        if(result.success){
                            scope.ngDialogData.storageLocationDropArr[index] = result.data;
                            scope.ngDialogData.dropDownDefault.storageLocationDrop[index] = scope.ngDialogData.storageLocationDropArr[index][0];
                        }else{
                            scope.Tools.alert(result.message);
                        }
                    })
            }

            //人工分配
            scope.manuallAssignClick = function(){

                var paramArr = new Array();
                angular.forEach(scope.ngDialogData.manuallyAssignList,function(value,key){  //分配
                    var obj = {
                        alOdId : value.odId,   //订单id
                        alCustomerId : value.odCustomerId,   //货主id
                        alCustomerCode : value.odCustomerCode,  //货主code
                        alWaybillNo : value.odWaybillNo,   //单据号
                        alVehicleSpecCode : value.oddVehicleSpecCode,   //车型 code
                        alVehicleSpecName : value.oddVehicleSpecName,   //车型 name
                        alVin : value.oddVin,   //底盘号
                        alTargetZoneId : scope.ngDialogData.dropDownDefault.reservoirDrop[key].locZoneId, //库区id
                        alTargetLocId : scope.ngDialogData.dropDownDefault.storageLocationDrop[key].locId,    //库位id
                        alTargetZoneCode : scope.ngDialogData.dropDownDefault.reservoirDrop[key].locZoneCode,//库区code
                        alTargetLocCode : scope.ngDialogData.dropDownDefault.storageLocationDrop[key].locCode    //库位code
                    }
                    paramArr.push(obj);
                })

                inStorageBillService.createAlloc(scope.constant.distributionType.manual,paramArr)
                    .then(function(result){
                        if(result.success){
                            scope.Tools.alert('手动分配成功');
                            //在当前页刷新
                            scope.list(scope.listParam());
                            scope.closeThisDialog();
                        }else{
                            scope.Tools.alert(result.message);
                        }
                    })
            }
        }

        //判断是否是自动分配
        if(scope.ngDialogData.autoAssignList){

            scope.ngDialogData.dropDownDefault = {
                reservoirDrop : new Array(),    //库区
                storageLocationDrop : new Array()   //库位
            }

            scope.ngDialogData.firstAssign = true;  //设置为第一次分配

            var paramArr = new Array();
            angular.forEach(scope.ngDialogData.autoAssignList,function(value,key){  //分配
                var obj = {
                    alOdId : value.odId,   //订单id
                    alCustomerId : value.odCustomerId,   //货主id
                    alCustomerCode : value.odCustomerCode,  //货主code
                    alWaybillNo : value.odWaybillNo,   //单据号
                    alVehicleSpecCode : value.oddVehicleSpecCode,   //车型 code
                    alVehicleSpecName : value.oddVehicleSpecName,   //车型 name
                    alVin : value.oddVin   //底盘号
                }
                paramArr.push(obj);
            })

            //选择库位
            scope.selectReservoirDropChang = function(item,manuallyAssign,index){
                //TODO 默认写死数据
                var whCode = '001';
                inStorageBillService.storageLocationList(whCode,item.locZoneId)
                    .then(function(result){
                        if(result.success){
                            scope.ngDialogData.storageLocationDropArr[index] = result.data;
                            scope.ngDialogData.dropDownDefault.storageLocationDrop[index] = scope.ngDialogData.storageLocationDropArr[index][0];
                        }else{
                            scope.Tools.alert(result.message);
                        }
                    })
            }

            //分配 submit
            scope.autoAssignSubmitClick = function(){
                scope.ngDialogData.firstAssign = false; //已经分配了一次
                //设置库区库位默认值
                scope.ngDialogData.dropDownDefault = {
                    reservoirDrop : new Array(),    //库区
                    storageLocationDrop : new Array()   //库位
                }
                inStorageBillService.autoAssign(scope.ngDialogData.stockUnitStrategy.pyCode,paramArr)
                    .then(function(result){
                        if(result.success){
                            if(result.data){
                                var resDataList = new Array();
                                angular.forEach(result.data,function(value,key){
                                    var obj = {
                                        odId : value.alOdId,    //订单id
                                        odCustomerId : value.alCustomerId,  //货主id
                                        odCustomerCode : value.alCustomerCode,  //货主code
                                        odWaybillNo : value.alWaybillNo,    //单据号
                                        oddVehicleSpecCode : value.alVehicleSpecCode,   //车型 code
                                        oddVehicleSpecName : value.alVehicleSpecName,   //车型 name
                                        oddVin : value.alVin,    //底盘号
                                        oddWhLocCode : value.alTargetLocCode, //库位code
                                        oddWhLocName : value.alTargetLocName, //库位name
                                        oddWhZoneCode : value.alTargetZoneCode, //库区code
                                        oddWhZoneName : value.alTargetZoneName  //库区 name
                                    }
                                    resDataList.push(obj);
                                })
                                scope.ngDialogData.autoAssignList = resDataList;
                            }
                        }else{
                            scope.Tools.alert(result.message);
                        }
                    })
            }

            //自动分配 commit click
            scope.autoAssignCommitClick = function(){
                var paramArr = new Array();
                angular.forEach(scope.ngDialogData.autoAssignList,function(value,key){  //分配
                    var obj = {
                        alOdId : value.odId,   //订单id
                        alCustomerId : value.odCustomerId,   //货主id
                        alCustomerCode : value.odCustomerCode,  //货主code
                        alWaybillNo : value.odWaybillNo,   //单据号
                        alVehicleSpecCode : value.oddVehicleSpecCode,   //车型 code
                        alVehicleSpecName : value.oddVehicleSpecName,   //车型 name
                        alVin : value.oddVin   //底盘号
                    }

                    //判断有没有库区
                    if(!value.oddWhZoneCode){
                        obj.alTargetZoneId = scope.ngDialogData.dropDownDefault.reservoirDrop[key].locZoneId; //库区id
                        obj.alTargetZoneCode = scope.ngDialogData.dropDownDefault.reservoirDrop[key].locZoneCode;//库区code
                    }
                    //判断有没有库位
                    if(!value.oddWhLocCode){
                        obj.alTargetLocId = scope.ngDialogData.dropDownDefault.storageLocationDrop[key].locId,    //库位id
                        obj.alTargetLocCode = scope.ngDialogData.dropDownDefault.storageLocationDrop[key].locCode    //库位code
                    }
                    paramArr.push(obj);
                })

                inStorageBillService.createAlloc(scope.constant.distributionType.auto,paramArr)
                    .then(function(result){
                        if(result.success){
                            scope.Tools.alert('自动分配成功');
                            //在当前页刷新
                            scope.list(scope.listParam());
                            scope.closeThisDialog();
                        }else{
                            scope.Tools.alert(result.message);
                        }
                    })
            }
        }
    }])
    .controller('inventorySelectCtrl',['$scope','inventorySelectService',function(scope,inventorySelectService){ //入库查询 controller

        //更多条件
        scope.moreText = scope.constant.moreText.spread;
        scope.moreClick = function(){
            scope.otherForm = !scope.otherForm;
            if(scope.otherForm){
                scope.moreText = scope.constant.moreText.packUp;
            }else{
                scope.moreText = scope.constant.moreText.spread;
            }
        }

        //仓库
        var shck = [
            {
                name:'我只是仓库',
                value:'1'
            },
            {
                name:'我只是仓库1',
                value:'2'
            },
            {
                name:'我只是仓库1',
                value:'3'
            }
        ]

        //车系
        var cx = [
            {
                name:'我只是车系',
                value:'1'
            },
            {
                name:'我只是车系1',
                value:'2'
            },
            {
                name:'我只是车系2',
                value:'3'
            }
        ]
        //品牌
        var pp = [
            {
                name:'我只是品牌',
                value:'1'
            },
            {
                name:'我只是品牌1',
                value:'2'
            },
            {
                name:'我只是品牌2',
                value:'3'
            }
        ]

        //车型
        var cxing = [
            {
                name:'凯锐800H',
                value:'1'
            },
            {
                name:'凯锐800L',
                value:'2'
            },
            {
                name:'凯运升级版',
                value:'3'
            },
            {
                name:'顺达宽',
                value:'4'
            },
            {
                name:'顺达窄(普通款)',
                value:'5'
            },
            {
                name:'顺达窄(豪华款)',
                value:'6'
            }
        ]

        //下拉
        scope.dropDown = {  //下拉
            warehouse : shck.concat(),   //仓库
            vehicleSeries : cx.concat(), //车系
            brand : pp.concat(), //品牌
            motorcycleType : cxing.concat() //车型
        }

        scope.dropDown.warehouse.unshift(scope.constant.defaultDropDownNullData);   //插入仓库空数据
        scope.dropDown.vehicleSeries.unshift(scope.constant.defaultDropDownNullData);   //插入车系空数据
        scope.dropDown.brand.unshift(scope.constant.defaultDropDownNullData);   //插入品牌空数据
        scope.dropDown.motorcycleType.unshift(scope.constant.defaultDropDownNullData);   //插入品牌空数据

        scope.dropDownDefault = {   //下拉默认值
            warehouse : scope.dropDown.warehouse[0], //仓库默认值
            vehicleSeries : scope.dropDown.vehicleSeries[0],   //车系默认值
            brand : scope.dropDown.brand[0],   //品牌默认值
            motorcycleType : scope.dropDown.motorcycleType[0]   //车型默认值
        }

        //列表条件参数
        scope.listParam = function(){
            if(!scope.search){
                scope.search = {};
            }
            scope.search.pageSize = scope.pageSize;    //设置最大页
            if(scope.otherForm){    //展开了更多
                scope.search.invWhName = scope.dropDownDefault.warehouse.name;   //仓库
                scope.search.invBrand = scope.dropDownDefault.brand.name;    //品牌
                scope.search.invVehicleSeries = scope.dropDownDefault.vehicleSeries.name;//车系
                scope.search.invVehicleSpec = scope.dropDownDefault.motorcycleType.name;  //车型
                return scope.search;
            }else{
                return scope.search;
            }
        }

        //列表
        scope.fristSelect = true;   //判断是否是第一次查询
        scope.list = function(params){
            //设置分页参数
            if(!params){
                params = {};
            }
            params.pageSize = params.pageSize ? params.pageSize : scope.constant.page.pageSize;
            params.pageNo = params.pageNo ? params.pageNo : scope.constant.page.pageNo

            inventorySelectService.list(params)
                .then(function(result){
                    if(result.success){
                        scope.inventorySelectList = result.data.wmsInventoryList;
                        if(scope.fristSelect){
                            scope.Tools.page(document.getElementById('pages'),{
                                pages : result.data.wmsInventoryBO.totalPage,
                                curr : result.data.wmsInventoryBO.pageNo,
                                count : result.data.wmsInventoryBO.totalRecord
                            },function(obj, first){
                                scope.fristSelect = first;  //设置状态
                                //初始化多选框
                                scope.Tools.initCheckBox();
                                if(!first){ //判断不是第一次加载
                                    var param = scope.listParam();
                                    param.pageNo = obj.curr;    //当前页
                                    param.pageSize = obj.limit; //最大页
                                    scope.pageSize = obj.limit; //设置最大页
                                    scope.pageNo = obj.curr; //设置当前页
                                    scope.list(param);
                                }
                            })
                        }else{
                            //初始化多选框
                            scope.Tools.initCheckBox();
                        }
                    }else{
                        scope.Tools.alert(result.message);
                    }
                })
        }

        //列表
        scope.list();

        //查询 click
        scope.searchClick = function(){
            scope.fristSelect = true;   //设置为第一次查询
            scope.list(scope.listParam());
        }

        //重置 click
        scope.resetClick = function(){
            scope.search = {};
        }

        //刷新 click
        scope.refreshClick = function(){
            scope.searchClick();
        }

        //详情 click
        scope.inventorySelectDetailClick = function(item){
            inventorySelectService.detail(item.invId)
                .then(function(result){
                    if(result.success){
                        scope.Tools.dialog({
                            controller : 'inventorySelectCtrl.dialog',
                            template : '../public/template/inventoryManagement/inventorySelect/inventory-select-detail.html',
                            closeByDocument : false,
                            closeByEscape : false,
                            data : {
                                inventorySelectDetailList : result.data,
                                invVehicleName : item.invVehicleSpecName
                            },
                            width : 800,
                            title : '车辆存放详情',
                            scope : scope
                        })
                    }else{
                        scope.Tools.alert(result.message);
                    }
                })
        }
    }])
    .controller('inventorySelectCtrl.dialog',['$scope',function(scope){
    }])