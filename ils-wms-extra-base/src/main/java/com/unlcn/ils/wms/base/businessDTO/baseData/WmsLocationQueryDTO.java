package com.unlcn.ils.wms.base.businessDTO.baseData;

import cn.huiyunche.commons.domain.PageVo;

import java.util.Date;

/**
 * Created by DELL on 2017/8/21.
 */
public class WmsLocationQueryDTO extends PageVo{

    private Long locId;

    private String locWhId;

    private String locWhCode;

    private String locWhName;

    private String locZoneId;

    private String locZoneCode;

    private String locZoneName;

    private String locCode;

    private String locName;

    private Long locRow;

    private Long locColumn;

    private Long locLevel;

    private Long locLength;

    private Long locWidth;

    private String locEnableFlag;

    private String createUserId;

    private String createUserName;

    private String modifyUserId;

    private String modifyUserName;

    private Date gmtCreate;

    private Date gmtUpdate;

    private Byte isDeleted;


    private Long versions;

    private int limitStart = -1;

    private int limitEnd = -1;

    public Long getLocId() {
        return locId;
    }

    public void setLocId(Long locId) {
        this.locId = locId;
    }

    public String getLocWhId() {
        return locWhId;
    }

    public void setLocWhId(String locWhId) {
        this.locWhId = locWhId;
    }

    public String getLocWhCode() {
        return locWhCode;
    }

    public void setLocWhCode(String locWhCode) {
        this.locWhCode = locWhCode;
    }

    public String getLocWhName() {
        return locWhName;
    }

    public void setLocWhName(String locWhName) {
        this.locWhName = locWhName;
    }

    public String getLocZoneId() {
        return locZoneId;
    }

    public void setLocZoneId(String locZoneId) {
        this.locZoneId = locZoneId;
    }

    public String getLocZoneCode() {
        return locZoneCode;
    }

    public void setLocZoneCode(String locZoneCode) {
        this.locZoneCode = locZoneCode;
    }

    public String getLocZoneName() {
        return locZoneName;
    }

    public void setLocZoneName(String locZoneName) {
        this.locZoneName = locZoneName;
    }

    public String getLocCode() {
        return locCode;
    }

    public void setLocCode(String locCode) {
        this.locCode = locCode;
    }

    public String getLocName() {
        return locName;
    }

    public void setLocName(String locName) {
        this.locName = locName;
    }

    public Long getLocRow() {
        return locRow;
    }

    public void setLocRow(Long locRow) {
        this.locRow = locRow;
    }

    public Long getLocColumn() {
        return locColumn;
    }

    public void setLocColumn(Long locColumn) {
        this.locColumn = locColumn;
    }

    public Long getLocLevel() {
        return locLevel;
    }

    public void setLocLevel(Long locLevel) {
        this.locLevel = locLevel;
    }

    public Long getLocLength() {
        return locLength;
    }

    public void setLocLength(Long locLength) {
        this.locLength = locLength;
    }

    public Long getLocWidth() {
        return locWidth;
    }

    public void setLocWidth(Long locWidth) {
        this.locWidth = locWidth;
    }

    public String getLocEnableFlag() {
        return locEnableFlag;
    }

    public void setLocEnableFlag(String locEnableFlag) {
        this.locEnableFlag = locEnableFlag;
    }

    public String getCreateUserId() {
        return createUserId;
    }

    public void setCreateUserId(String createUserId) {
        this.createUserId = createUserId;
    }

    public String getCreateUserName() {
        return createUserName;
    }

    public void setCreateUserName(String createUserName) {
        this.createUserName = createUserName;
    }

    public String getModifyUserId() {
        return modifyUserId;
    }

    public void setModifyUserId(String modifyUserId) {
        this.modifyUserId = modifyUserId;
    }

    public String getModifyUserName() {
        return modifyUserName;
    }

    public void setModifyUserName(String modifyUserName) {
        this.modifyUserName = modifyUserName;
    }

    public Date getGmtCreate() {
        return gmtCreate;
    }

    public void setGmtCreate(Date gmtCreate) {
        this.gmtCreate = gmtCreate;
    }

    public Date getGmtUpdate() {
        return gmtUpdate;
    }

    public void setGmtUpdate(Date gmtUpdate) {
        this.gmtUpdate = gmtUpdate;
    }

    public Byte getIsDeleted() {
        return isDeleted;
    }

    public void setIsDeleted(Byte isDeleted) {
        this.isDeleted = isDeleted;
    }

    public Long getVersions() {
        return versions;
    }

    public void setVersions(Long versions) {
        this.versions = versions;
    }

    public int getLimitStart() {
        return limitStart;
    }

    public void setLimitStart(int limitStart) {
        this.limitStart = limitStart;
    }

    public int getLimitEnd() {
        return limitEnd;
    }

    public void setLimitEnd(int limitEnd) {
        this.limitEnd = limitEnd;
    }

    @Override
    public String toString() {
        return "WmsLocationQueryDTO{" +
                "locId=" + locId +
                ", locWhId='" + locWhId + '\'' +
                ", locWhCode='" + locWhCode + '\'' +
                ", locWhName='" + locWhName + '\'' +
                ", locZoneId='" + locZoneId + '\'' +
                ", locZoneCode='" + locZoneCode + '\'' +
                ", locZoneName='" + locZoneName + '\'' +
                ", locCode='" + locCode + '\'' +
                ", locName='" + locName + '\'' +
                ", locRow=" + locRow +
                ", locColumn=" + locColumn +
                ", locLevel=" + locLevel +
                ", locLength=" + locLength +
                ", locWidth=" + locWidth +
                ", locEnableFlag='" + locEnableFlag + '\'' +
                ", createUserId='" + createUserId + '\'' +
                ", createUserName='" + createUserName + '\'' +
                ", modifyUserId='" + modifyUserId + '\'' +
                ", modifyUserName='" + modifyUserName + '\'' +
                ", gmtCreate=" + gmtCreate +
                ", gmtUpdate=" + gmtUpdate +
                ", isDeleted=" + isDeleted +
                ", versions=" + versions +
                ", limitStart=" + limitStart +
                ", limitEnd=" + limitEnd +
                '}';
    }
}
