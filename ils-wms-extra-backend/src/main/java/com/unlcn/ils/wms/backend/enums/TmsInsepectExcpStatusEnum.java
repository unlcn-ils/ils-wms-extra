package com.unlcn.ils.wms.backend.enums;

/**
 * @Author ：Ligl
 * @Date : 2017/9/14.
 */
public enum TmsInsepectExcpStatusEnum {

    BILL_UNTREATED(10,"未处理"),
    BILL_CLOSEERROR(20,"关闭"),
    BILL_CONCESSION(30,"让步");

    private final int value;
    private final String text;
    TmsInsepectExcpStatusEnum(int value,String text){
        this.value = value;
        this.text = text;
    }

    public int getValue() {
        return value;
    }

    public String getText() {
        return text;
    }

    public static WmsExcpStatusEnum getByValue(int value){
        for (WmsExcpStatusEnum temp : WmsExcpStatusEnum.values()) {
            if(temp.getValue() == value){
                return temp;
            }
        }
        return null;
    }
}
