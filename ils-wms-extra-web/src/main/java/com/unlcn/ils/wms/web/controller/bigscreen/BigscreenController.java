package com.unlcn.ils.wms.web.controller.bigscreen;

import cn.huiyunche.commons.domain.ResultDTO;
import cn.huiyunche.commons.domain.ResultDTOWithPagination;
import cn.huiyunche.commons.exception.BusinessException;
import com.unlcn.ils.wms.backend.service.bigscreen.*;
import com.unlcn.ils.wms.backend.service.inbound.WmsInventoryService;
import com.unlcn.ils.wms.base.dto.WmsOnWayLineDTO;
import com.unlcn.ils.wms.base.model.bigscreen.BiDistribution;
import com.unlcn.ils.wms.base.model.bigscreen.BiFrontOnWay;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/bigscreen")
@ResponseBody
public class BigscreenController {

    private Logger LOGGER = LoggerFactory.getLogger(BigscreenController.class);

    @Autowired
    private FrontOnWayService frontOnWayService;          // 前端在途数据service

    @Autowired
    private OverReasonService overReasonService;         // 超期原因分析service

    @Autowired
    private PickDataService pickDataService;          // 提货推移统计service

    @Autowired
    private FrontShipmentService frontShipmentService;    // 前端发运推移图service

    @Autowired
    private TransModelService transModelService;      // 发运模式推移图service

    @Autowired
    private WmsInventoryService wmsInventoryService;   // 库存service

    @Autowired
    private InOutDrayService inOutDrayService;      // 板车出入计划主表service

    @Autowired
    private DistributionService distributionService;  // 配送推移统计service

    /**
     * 获取前端数据列表
     *
     * @return
     */
    @RequestMapping(value = "/onWayLineOld", method = RequestMethod.GET)
    public ResultDTO<Object> onWayLine() {
        LOGGER.info("BigscreenController.onWayLine");
        ResultDTO<Object> resultDTO = null;

        try {
            resultDTO = new ResultDTO<>(true, frontOnWayService.onWayLine(), "成功获取前端数据列表");
        } catch (BusinessException e) {
            LOGGER.error("BigscreenController.onWayLine businessException: {}", e);
            resultDTO = new ResultDTO<>(false, e.getMessage());
        } catch (Exception e) {
            LOGGER.error("BigscreenController.onWayLine error: {}", "系统异常...");
            resultDTO = new ResultDTO<>(false, "系统异常...");
        }

        return resultDTO;
    }

    /**
     * 获取前段数据列表--2017-12-19 <p>新需求:百分比完成的不用显示，
     * 设置一个查询条件（状态查询），超期水铁运输10天以内数据，陆运4天以内，
     * 一页显示10条，数据10秒内自动滚动，运输模式更改为图标。进度表未完成百分百全部显示，异常未完成都显示。</p>
     * 前置库进销存
     *
     * @return 返回值
     */
    @RequestMapping(value = "/onWayLine", method = RequestMethod.POST)
    public ResultDTOWithPagination<Object> onWayLineWithPagination(@RequestBody WmsOnWayLineDTO dto) {
        LOGGER.info("BigscreenController.onWayLineWithPagination param {}", dto);
        ResultDTOWithPagination<Object> resultDTO = new ResultDTOWithPagination<>(true, null, "成功获取前端数据列表");
        try {
            ResultDTOWithPagination<List<BiFrontOnWay>> data = frontOnWayService.onWayLineWithPagiation(dto);
            resultDTO.setData(data.getData());
            resultDTO.setPageVo(data.getPageVo());
        } catch (BusinessException e) {
            LOGGER.error("BigscreenController.onWayLineWithPagination businessException: {}", e);
            resultDTO.setSuccess(false);
            resultDTO.setMessage(e.getMessage());
        } catch (Exception e) {
            LOGGER.error("BigscreenController.onWayLineWithPagination error: {}", "系统异常");
            resultDTO.setSuccess(false);
            resultDTO.setMessage("系统异常");
        }
        return resultDTO;
    }

    /**
     * 获取超期原因分析信息
     *
     * @return
     */
    @RequestMapping(value = "/reasonChartCount", method = RequestMethod.GET)
    public ResultDTO<Object> reasonChartCount() {
        LOGGER.error("BigscreenController.reasonChartCount");
        ResultDTO<Object> resultDTO = null;

        try {
            resultDTO = new ResultDTO<Object>(true, overReasonService.reasonChartCount(), "成功获取超期原因分析信息");
        } catch (BusinessException e) {
            LOGGER.error("BigscreenController.reasonChartCount businessException: {}", e);
            resultDTO = new ResultDTO<>(false, e.getMessage());
        } catch (Exception e) {
            LOGGER.error("BigscreenController.reasonChartCount error: {}", "系统异常...");
            resultDTO = new ResultDTO<>(false, "系统异常...");
        }

        return resultDTO;
    }

    /**
     * 获取提货推移信息统计信息
     *
     * @return
     */
    @RequestMapping(value = "/pickChartCount", method = RequestMethod.GET)
    public ResultDTO<Object> pickChartCount() {
        LOGGER.error("BigscreenController.pickChartCount");
        ResultDTO<Object> resultDTO = null;

        try {
            resultDTO = new ResultDTO<Object>(true, pickDataService.pickDataCount(), "成功获取提货推移信息统计信息");
        } catch (BusinessException e) {
            LOGGER.error("BigscreenController.pickDataCount businessException: {}", e);
            resultDTO = new ResultDTO<>(false, e.getMessage());
        } catch (Exception e) {
            LOGGER.error("BigscreenController.pickDataCount error: {}", e);
            resultDTO = new ResultDTO<>(false, "系统异常...");
        }

        return resultDTO;
    }

    /**
     * 获取前端发运推移图信息
     *
     * @return
     */
    @RequestMapping(value = "/shipmentChartCount", method = RequestMethod.GET)
    public ResultDTO<Object> shipmentChartCount() {
        LOGGER.error("BigscreenController.shipmentChartCount");
        ResultDTO<Object> resultDTO = null;

        try {
            resultDTO = new ResultDTO<Object>(true, frontShipmentService.shipmentChartCount(), "成功获取前端发运推移信息统计信息");
        } catch (BusinessException e) {
            LOGGER.error("BigscreenController.shipmentChartCount businessException: {}", e);
            resultDTO = new ResultDTO<>(false, e.getMessage());
        } catch (Exception e) {
            LOGGER.error("BigscreenController.shipmentChartCount error: {}", "系统异常...");
            resultDTO = new ResultDTO<>(false, "系统异常...");
        }

        return resultDTO;
    }

    /**
     * 获取发运模式推移图统计信息
     *
     * @return
     */
    @RequestMapping(value = "/transModelChartCount", method = RequestMethod.GET)
    public ResultDTO<Object> transModelChartCount() {
        LOGGER.error("BigscreenController.transModelChartCount");
        ResultDTO<Object> resultDTO = null;

        try {
            resultDTO = new ResultDTO<Object>(true, transModelService.transModelChartCount(), "成功获取前端发运模式推移图统计信息");
        } catch (BusinessException e) {
            LOGGER.error("BigscreenController.transModelChartCount businessException: {}", e);
            resultDTO = new ResultDTO<>(false, e.getMessage());
        } catch (Exception e) {
            LOGGER.error("BigscreenController.transModelChartCount error: {}", e);
            resultDTO = new ResultDTO<>(false, "系统异常...");
        }

        return resultDTO;
    }

    /**
     * 进销库存图统计
     *
     * @return
     */
    @RequestMapping(value = "/invoicingStatisticsChartCount", method = RequestMethod.GET)
    public ResultDTO<Object> invoicingStatisticsCount() {
        LOGGER.error("BigscreenController.invoicingStatisticsChartCount");
        ResultDTO<Object> resultDTO = null;

        try {
            resultDTO = new ResultDTO<Object>(true, wmsInventoryService.invoicingStatisticsChartCount(), "成功获取进销库存统计信息");
        } catch (BusinessException e) {
            LOGGER.error("BigscreenController.invoicingStatisticsChartCount businessException: {}", e);
            resultDTO = new ResultDTO<>(false, e.getMessage());
        } catch (Exception e) {
            LOGGER.error("BigscreenController.invoicingStatisticsChartCount error: {}", e);
            resultDTO = new ResultDTO<>(false, "系统异常...");
        }

        return resultDTO;
    }

    /**
     * 进销库存列表统计信息
     * <p>
     * 2018-3-19 调整大屏进销存页面展示数据,已库存数据值展示
     * </p>
     *
     * @return
     */
    @RequestMapping(value = "/invoicingStatisticsLineCount", method = RequestMethod.GET)
    public ResultDTO<Object> invoicingStatistics() {
        LOGGER.error("BigscreenController.invoicingStatisticsLineCount");
        ResultDTO<Object> resultDTO = null;

        try {
            resultDTO = new ResultDTO<Object>(true, wmsInventoryService.invoicingStatisticsLineCount(), "成功获取进销库存统计信息");
        } catch (BusinessException e) {
            LOGGER.error("BigscreenController.invoicingStatisticsLineCount businessException: {}", e);
            resultDTO = new ResultDTO<>(false, e.getMessage());
        } catch (Exception e) {
            LOGGER.error("BigscreenController.invoicingStatisticsLineCount error: {}", e);
            resultDTO = new ResultDTO<>(false, "系统异常...");
        }
        return resultDTO;
    }

    /**
     * 板车出入计划主表统计
     *
     * @return
     */
    @RequestMapping(value = "/inOutDrayChartCount", method = RequestMethod.GET)
    public ResultDTO<Object> outDrayChartCount() {
        LOGGER.error("BigscreenController.inOutDrayChartCount");
        ResultDTO<Object> resultDTO = null;

        try {
            resultDTO = new ResultDTO<>(true, inOutDrayService.inOutDrayChartCount(), "成功获取板车出入计划统计图信息");
        } catch (BusinessException e) {
            LOGGER.error("BigscreenController.inOutDrayChartCount businessException: {}", e);
            resultDTO = new ResultDTO<>(false, e.getMessage());
        } catch (Exception e) {
            LOGGER.error("BigscreenController.inOutDrayChartCount error: {}", e);
            resultDTO = new ResultDTO<>(false, "系统异常...");
        }

        return resultDTO;
    }

    /**
     * 板车出入计划列表统计
     *
     * @return
     */
    @RequestMapping(value = "/inOutDrayLineCountOld", method = RequestMethod.GET)
    public ResultDTO<Object> outDrayLineInfo() {
        LOGGER.error("BigscreenController.inOutDrayLineCount");
        ResultDTO<Object> resultDTO = null;

        try {
            resultDTO = new ResultDTO<>(true, inOutDrayService.inOutDrayLineCount(), "成功获取板车出入计划统计图信息");
        } catch (BusinessException e) {
            LOGGER.error("BigscreenController.inOutDrayLineCount businessException: {}", e);
            resultDTO = new ResultDTO<>(false, e.getMessage());
        } catch (Exception e) {
            LOGGER.error("BigscreenController.inOutDrayLineCount error: {}", e);
            resultDTO = new ResultDTO<>(false, "系统异常...");
        }

        return resultDTO;
    }

    /**
     * @return 返回值
     * @author hs
     * 板车出入计划列表统计
     * <p>2017-12-19 新需求:指令号D开头不显示，显示前10条数据，表格里加上实际进场时间，实际出场时间</p>
     */
    @RequestMapping(value = "/inOutDrayLineCount", method = RequestMethod.GET)
    public ResultDTO<Object> inOutDrayLineCountNew() {
        LOGGER.info("BigscreenController.inOutDrayLineCountNew");
        ResultDTO<Object> resultDTO = new ResultDTO<>(true, null, "获取成功");
        try {
            resultDTO.setData(inOutDrayService.inOutDrayLineCountNew());
        } catch (BusinessException e) {
            LOGGER.error("BigscreenController.inOutDrayLineCountNew error: {}", e);
            resultDTO.setSuccess(false);
            resultDTO.setMessage(e.getMessage());
        } catch (Exception e) {
            LOGGER.error("BigscreenController.inOutDrayLineCountNew error: {}", e);
            resultDTO.setSuccess(false);
            resultDTO.setMessage("系统异常");
        }
        return resultDTO;
    }

    /**
     * 配送推移图统计
     *
     * @return
     */
    @RequestMapping(value = "/distributionChartCount", method = RequestMethod.GET)
    public ResultDTO<Object> distributionChartCount() {
        LOGGER.error("BigscreenController.distributionChartCount");
        ResultDTO<Object> resultDTO = null;

        try {
            resultDTO = new ResultDTO<Object>(true, distributionService.distributionChartCount(), "成功获取板车出入计划统计图信息");
        } catch (BusinessException e) {
            LOGGER.error("BigscreenController.distributionChartCount businessException: {}", e);
            resultDTO = new ResultDTO<>(false, e.getMessage());
        } catch (Exception e) {
            LOGGER.error("BigscreenController.distributionChartCount error: {}", e);
            resultDTO = new ResultDTO<>(false, "系统异常...");
        }

        return resultDTO;
    }

    /**
     * 配送推移列表统计
     *
     * @return
     */
    @RequestMapping(value = "/distributionLineCount", method = RequestMethod.GET)
    public ResultDTO<Object> distributionLineCount() {
        LOGGER.error("BigscreenController.distributionLineCount");
        ResultDTO<Object> resultDTO = new ResultDTO<>(true, null, "成功获取板车出入计划统计图信息");
        try {
            List<BiDistribution> biDistributions = distributionService.getDisLineAndGateTime();
            resultDTO.setData(biDistributions);
        } catch (BusinessException e) {
            LOGGER.error("BigscreenController.distributionLineCount businessException: {}", e);
            resultDTO.setSuccess(false);
            resultDTO.setMessage(e.getMessage());
        } catch (Exception e) {
            LOGGER.error("BigscreenController.distributionLineCount error: {}", e);
            resultDTO.setSuccess(false);
            resultDTO.setMessage("系统异常...");
        }

        return resultDTO;
    }
}
