package com.unlcn.ils.wms.backend.bo.baseDataBO;

import java.util.Date;

/**
 * Created by DELL on 2017/8/18.
 */
public class WmsZoneBO {

    private Long zeId;

    private String zeWhCode;

    private String zeWhName;

    private String zeZoneCode;

    private String zeZoneName;

    private String zeRemark;

    private String createPerson;

    private String updatePerson;

    private Date gmtCreate;

    private Date gmtUpdate;

    private Byte isDeleted;

    private Long versions;

    public Long getZeId() {
        return zeId;
    }

    public void setZeId(Long zeId) {
        this.zeId = zeId;
    }

    public String getZeWhCode() {
        return zeWhCode;
    }

    public void setZeWhCode(String zeWhCode) {
        this.zeWhCode = zeWhCode;
    }

    public String getZeWhName() {
        return zeWhName;
    }

    public void setZeWhName(String zeWhName) {
        this.zeWhName = zeWhName;
    }

    public String getZeZoneCode() {
        return zeZoneCode;
    }

    public void setZeZoneCode(String zeZoneCode) {
        this.zeZoneCode = zeZoneCode;
    }

    public String getZeZoneName() {
        return zeZoneName;
    }

    public void setZeZoneName(String zeZoneName) {
        this.zeZoneName = zeZoneName;
    }

    public String getZeRemark() {
        return zeRemark;
    }

    public void setZeRemark(String zeRemark) {
        this.zeRemark = zeRemark;
    }

    public String getCreatePerson() {
        return createPerson;
    }

    public void setCreatePerson(String createPerson) {
        this.createPerson = createPerson;
    }

    public String getUpdatePerson() {
        return updatePerson;
    }

    public void setUpdatePerson(String updatePerson) {
        this.updatePerson = updatePerson;
    }

    public Date getGmtCreate() {
        return gmtCreate;
    }

    public void setGmtCreate(Date gmtCreate) {
        this.gmtCreate = gmtCreate;
    }

    public Date getGmtUpdate() {
        return gmtUpdate;
    }

    public void setGmtUpdate(Date gmtUpdate) {
        this.gmtUpdate = gmtUpdate;
    }

    public Byte getIsDeleted() {
        return isDeleted;
    }

    public void setIsDeleted(Byte isDeleted) {
        this.isDeleted = isDeleted;
    }

    public Long getVersions() {
        return versions;
    }

    public void setVersions(Long versions) {
        this.versions = versions;
    }

    @Override
    public String toString() {
        return "WmsZoneBO{" +
                "zeId=" + zeId +
                ", zeWhCode='" + zeWhCode + '\'' +
                ", zeWhName='" + zeWhName + '\'' +
                ", zeZoneCode='" + zeZoneCode + '\'' +
                ", zeZoneName='" + zeZoneName + '\'' +
                ", zeRemark='" + zeRemark + '\'' +
                ", createPerson='" + createPerson + '\'' +
                ", updatePerson='" + updatePerson + '\'' +
                ", gmtCreate=" + gmtCreate +
                ", gmtUpdate=" + gmtUpdate +
                ", isDeleted=" + isDeleted +
                ", versions=" + versions +
                '}';
    }
}
