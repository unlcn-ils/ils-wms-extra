package com.unlcn.ils.wms.base.dto;

import java.io.Serializable;
import java.util.Date;

public class WmsPreparationPlanResultDTO implements Serializable {

    private Long oddId;

    private Long oddOdId;

    private String oddOdCode;

    private String oddVehicleBrandCode;

    private String oddVehicleBrandName;

    private String oddVehicleSeriesCode;

    private String oddVehicleSeriesName;

    private String oddVehicleSpecCode;

    private String oddVehicleSpecName;

    private String oddVehicleSpecDesc;

    private String oddVehiclePlate;

    private String oddVin;

    private String oddEngine;

    private String oddWhCode;

    private String oddWhName;

    private String oddWhZoneCode;

    private String oddWhZoneName;

    private String oddWhLocCode;

    private String oddWhLocName;

    private Long oddVehicleLength;

    private Long oddVehicleWidth;

    private Long oddVehicleHigh;

    private String createUserId;

    private String createUserName;

    private String modifyUserId;

    private String modifyUserName;

    private Date gmtCreate;

    private Date gmtUpdate;

    private Byte isDeleted;

    private Long versions;

    private String oddMaterialCode;

    private String oddMaterialName;

    private String oddCarColourCode;

    private String oddCarColour;

    private String oddGearboxNumber;

    private String oddCertification;

    private Date oddProductionDate;

    private Date oddOfflineDate;

    private String factoryWhno;

    private String oddStockTransfer;

    private String oddStockTransferStatus;

    private String odOrderno;

    private String odCustomerName;

    public Long getOddId() {
        return oddId;
    }

    public void setOddId(Long oddId) {
        this.oddId = oddId;
    }

    public Long getOddOdId() {
        return oddOdId;
    }

    public void setOddOdId(Long oddOdId) {
        this.oddOdId = oddOdId;
    }

    public String getOddOdCode() {
        return oddOdCode;
    }

    public void setOddOdCode(String oddOdCode) {
        this.oddOdCode = oddOdCode;
    }

    public String getOddVehicleBrandCode() {
        return oddVehicleBrandCode;
    }

    public void setOddVehicleBrandCode(String oddVehicleBrandCode) {
        this.oddVehicleBrandCode = oddVehicleBrandCode;
    }

    public String getOddVehicleBrandName() {
        return oddVehicleBrandName;
    }

    public void setOddVehicleBrandName(String oddVehicleBrandName) {
        this.oddVehicleBrandName = oddVehicleBrandName;
    }

    public String getOddVehicleSeriesCode() {
        return oddVehicleSeriesCode;
    }

    public void setOddVehicleSeriesCode(String oddVehicleSeriesCode) {
        this.oddVehicleSeriesCode = oddVehicleSeriesCode;
    }

    public String getOddVehicleSeriesName() {
        return oddVehicleSeriesName;
    }

    public void setOddVehicleSeriesName(String oddVehicleSeriesName) {
        this.oddVehicleSeriesName = oddVehicleSeriesName;
    }

    public String getOddVehicleSpecCode() {
        return oddVehicleSpecCode;
    }

    public void setOddVehicleSpecCode(String oddVehicleSpecCode) {
        this.oddVehicleSpecCode = oddVehicleSpecCode;
    }

    public String getOddVehicleSpecName() {
        return oddVehicleSpecName;
    }

    public void setOddVehicleSpecName(String oddVehicleSpecName) {
        this.oddVehicleSpecName = oddVehicleSpecName;
    }

    public String getOddVehicleSpecDesc() {
        return oddVehicleSpecDesc;
    }

    public void setOddVehicleSpecDesc(String oddVehicleSpecDesc) {
        this.oddVehicleSpecDesc = oddVehicleSpecDesc;
    }

    public String getOddVehiclePlate() {
        return oddVehiclePlate;
    }

    public void setOddVehiclePlate(String oddVehiclePlate) {
        this.oddVehiclePlate = oddVehiclePlate;
    }

    public String getOddVin() {
        return oddVin;
    }

    public void setOddVin(String oddVin) {
        this.oddVin = oddVin;
    }

    public String getOddEngine() {
        return oddEngine;
    }

    public void setOddEngine(String oddEngine) {
        this.oddEngine = oddEngine;
    }

    public String getOddWhCode() {
        return oddWhCode;
    }

    public void setOddWhCode(String oddWhCode) {
        this.oddWhCode = oddWhCode;
    }

    public String getOddWhName() {
        return oddWhName;
    }

    public void setOddWhName(String oddWhName) {
        this.oddWhName = oddWhName;
    }

    public String getOddWhZoneCode() {
        return oddWhZoneCode;
    }

    public void setOddWhZoneCode(String oddWhZoneCode) {
        this.oddWhZoneCode = oddWhZoneCode;
    }

    public String getOddWhZoneName() {
        return oddWhZoneName;
    }

    public void setOddWhZoneName(String oddWhZoneName) {
        this.oddWhZoneName = oddWhZoneName;
    }

    public String getOddWhLocCode() {
        return oddWhLocCode;
    }

    public void setOddWhLocCode(String oddWhLocCode) {
        this.oddWhLocCode = oddWhLocCode;
    }

    public String getOddWhLocName() {
        return oddWhLocName;
    }

    public void setOddWhLocName(String oddWhLocName) {
        this.oddWhLocName = oddWhLocName;
    }

    public Long getOddVehicleLength() {
        return oddVehicleLength;
    }

    public void setOddVehicleLength(Long oddVehicleLength) {
        this.oddVehicleLength = oddVehicleLength;
    }

    public Long getOddVehicleWidth() {
        return oddVehicleWidth;
    }

    public void setOddVehicleWidth(Long oddVehicleWidth) {
        this.oddVehicleWidth = oddVehicleWidth;
    }

    public Long getOddVehicleHigh() {
        return oddVehicleHigh;
    }

    public void setOddVehicleHigh(Long oddVehicleHigh) {
        this.oddVehicleHigh = oddVehicleHigh;
    }

    public String getCreateUserId() {
        return createUserId;
    }

    public void setCreateUserId(String createUserId) {
        this.createUserId = createUserId;
    }

    public String getCreateUserName() {
        return createUserName;
    }

    public void setCreateUserName(String createUserName) {
        this.createUserName = createUserName;
    }

    public String getModifyUserId() {
        return modifyUserId;
    }

    public void setModifyUserId(String modifyUserId) {
        this.modifyUserId = modifyUserId;
    }

    public String getModifyUserName() {
        return modifyUserName;
    }

    public void setModifyUserName(String modifyUserName) {
        this.modifyUserName = modifyUserName;
    }

    public Date getGmtCreate() {
        return gmtCreate;
    }

    public void setGmtCreate(Date gmtCreate) {
        this.gmtCreate = gmtCreate;
    }

    public Date getGmtUpdate() {
        return gmtUpdate;
    }

    public void setGmtUpdate(Date gmtUpdate) {
        this.gmtUpdate = gmtUpdate;
    }

    public Byte getIsDeleted() {
        return isDeleted;
    }

    public void setIsDeleted(Byte isDeleted) {
        this.isDeleted = isDeleted;
    }

    public Long getVersions() {
        return versions;
    }

    public void setVersions(Long versions) {
        this.versions = versions;
    }

    public String getOddMaterialCode() {
        return oddMaterialCode;
    }

    public void setOddMaterialCode(String oddMaterialCode) {
        this.oddMaterialCode = oddMaterialCode;
    }

    public String getOddMaterialName() {
        return oddMaterialName;
    }

    public void setOddMaterialName(String oddMaterialName) {
        this.oddMaterialName = oddMaterialName;
    }

    public String getOddCarColourCode() {
        return oddCarColourCode;
    }

    public void setOddCarColourCode(String oddCarColourCode) {
        this.oddCarColourCode = oddCarColourCode;
    }

    public String getOddCarColour() {
        return oddCarColour;
    }

    public void setOddCarColour(String oddCarColour) {
        this.oddCarColour = oddCarColour;
    }

    public String getOddGearboxNumber() {
        return oddGearboxNumber;
    }

    public void setOddGearboxNumber(String oddGearboxNumber) {
        this.oddGearboxNumber = oddGearboxNumber;
    }

    public String getOddCertification() {
        return oddCertification;
    }

    public void setOddCertification(String oddCertification) {
        this.oddCertification = oddCertification;
    }

    public Date getOddProductionDate() {
        return oddProductionDate;
    }

    public void setOddProductionDate(Date oddProductionDate) {
        this.oddProductionDate = oddProductionDate;
    }

    public Date getOddOfflineDate() {
        return oddOfflineDate;
    }

    public void setOddOfflineDate(Date oddOfflineDate) {
        this.oddOfflineDate = oddOfflineDate;
    }

    public String getFactoryWhno() {
        return factoryWhno;
    }

    public void setFactoryWhno(String factoryWhno) {
        this.factoryWhno = factoryWhno;
    }

    public String getOddStockTransfer() {
        return oddStockTransfer;
    }

    public void setOddStockTransfer(String oddStockTransfer) {
        this.oddStockTransfer = oddStockTransfer;
    }

    public String getOddStockTransferStatus() {
        return oddStockTransferStatus;
    }

    public void setOddStockTransferStatus(String oddStockTransferStatus) {
        this.oddStockTransferStatus = oddStockTransferStatus;
    }

    public String getOdOrderno() {
        return odOrderno;
    }

    public void setOdOrderno(String odOrderno) {
        this.odOrderno = odOrderno;
    }

    public String getOdCustomerName() {
        return odCustomerName;
    }

    public void setOdCustomerName(String odCustomerName) {
        this.odCustomerName = odCustomerName;
    }
}
