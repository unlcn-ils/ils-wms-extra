package com.unlcn.ils.wms.base.mapper.extmapper;

import cn.huiyunche.commons.domain.PageVo;
import com.unlcn.ils.wms.base.dto.SysUserResultDTO;
import com.unlcn.ils.wms.base.model.sys.SysUser;
import org.apache.ibatis.annotations.Param;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public interface SysUserCustomMapper {

    /**
     * @param @param  ids
     * @param @param  status
     * @param @param  modifyDate
     * @param @return
     * @return Integer    返回类型
     * @throws
     * @Title: deleteByIds
     * @Description: 根据ids删除 数据
     */
    Integer deleteByIds(@Param("ids") List<String> ids, @Param("status") String status, @Param("modifyDate") Date modifyDate);

    /**
     * @param @param  user
     * @param @param  pageVo
     * @param @return
     * @return List<SysUser>    返回类型
     * @throws
     * @Title: list
     * @Description: 列表
     */
    List<SysUser> list(@Param("user") SysUser user, @Param("page") PageVo pageVo);

    /**
     * @param @param  username
     * @param @param  status
     * @param @return
     * @return SysUser    返回类型
     * @throws
     * @Title: selectByUsername
     * @Description: 根据用户名查询用户
     */
    SysUser selectByUsername(@Param("username") String username, @Param("status") String status);

    /**
     * 列表查询
     * @param paramMap
     * @return
     * @throws Exception
     */
    List<SysUserResultDTO> selectUserList(Map<String, Object> paramMap) throws Exception;

    /**
     * 数据条数
     * @param paramMap
     * @return
     * @throws Exception
     */
    Integer countUserList(Map<String, Object> paramMap) throws Exception;

    /**
     * 查询某仓库的司机
     * @param type
     * @param whCode
     * @return
     */
    List<SysUser> getUserByWarehouse(@Param("type") Integer type, @Param("whCode") String whCode);

    List<SysUser> selectOtDriverByWhCode(HashMap<String, Object> params);
}