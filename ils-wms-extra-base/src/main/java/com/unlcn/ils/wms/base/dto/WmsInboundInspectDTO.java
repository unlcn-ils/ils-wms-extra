package com.unlcn.ils.wms.base.dto;

import java.io.Serializable;
import java.util.Date;

public class WmsInboundInspectDTO implements Serializable {
    private Long id;
    private String vin;
    private String vehicle;
    private String waybillCode;
    private Date generateTime;
    private int inspectStatus;
    private String inspectStatusText;
    private String inspectUserId;
    private Date inspectTime;
    private String vehicleDesc;
    private String engine;
    private String whCode;
    private Integer excpType;

    public Integer getExcpType() {
        return excpType;
    }

    public void setExcpType(Integer excpType) {
        this.excpType = excpType;
    }

    public String getWhCode() {
        return whCode;
    }

    public void setWhCode(String whCode) {
        this.whCode = whCode;
    }

    public String getEngine() {
        return engine;
    }

    public void setEngine(String engine) {
        this.engine = engine;
    }

    public String getVehicleDesc() {
        return vehicleDesc;
    }

    public void setVehicleDesc(String vehicleDesc) {
        this.vehicleDesc = vehicleDesc;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getVin() {
        return vin;
    }

    public void setVin(String vin) {
        this.vin = vin;
    }

    public String getVehicle() {
        return vehicle;
    }

    public void setVehicle(String vehicle) {
        this.vehicle = vehicle;
    }

    public String getWaybillCode() {
        return waybillCode;
    }

    public void setWaybillCode(String waybillCode) {
        this.waybillCode = waybillCode;
    }

    public Date getGenerateTime() {
        return generateTime;
    }

    public void setGenerateTime(Date generateTime) {
        this.generateTime = generateTime;
    }

    public int getInspectStatus() {
        return inspectStatus;
    }

    public void setInspectStatus(int inspectStatus) {
        this.inspectStatus = inspectStatus;
    }

    public String getInspectStatusText() {
        return inspectStatusText;
    }

    public void setInspectStatusText(String inspectStatusText) {
        this.inspectStatusText = inspectStatusText;
    }

    public String getInspectUserId() {
        return inspectUserId;
    }

    public void setInspectUserId(String inspectUserId) {
        this.inspectUserId = inspectUserId;
    }

    public Date getInspectTime() {
        return inspectTime;
    }

    public void setInspectTime(Date inspectTime) {
        this.inspectTime = inspectTime;
    }
}
