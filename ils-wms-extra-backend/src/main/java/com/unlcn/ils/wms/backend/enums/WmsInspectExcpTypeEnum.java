package com.unlcn.ils.wms.backend.enums;

public enum WmsInspectExcpTypeEnum {
    TYPE_MISS("MISS", "缺件异常"),
    TYPE_EXCP("EXCP", "质损异常"),
    TYPE_LOSS("LOSS", "非质损异常"),
    TYPE_MISS_AND_EXCP("MISS_AND_EXCP", "缺件,质损异常"),
    TYPE_LOSS_AND_EXCP("LOSS_AND_EXCP", "非质损,质损异常"),
    TYPE_LOSS_AND_MISS("LOSS_AND_MISS", "非质损,缺件异常"),
    TYPE_LOSS_EXCP_MISS("LOSS_EXCP_MISS", "非质损,质损,缺件异常"),
    EXCP_INBOUND("10", "入库异常"),
    EXCP_OUTBOUND("20", "出库异常"),
    EXCP_PREPARE("30", "备料异常");


    private final String code;
    private final String name;

    WmsInspectExcpTypeEnum(String code, String name) {
        this.code = code;
        this.name = name;
    }

    public String getCode() {
        return code;
    }

    public String getName() {
        return name;
    }

    public static WmsInspectExcpTypeEnum getByCode(String code) {
        for (WmsInspectExcpTypeEnum temp : WmsInspectExcpTypeEnum.values()) {
            if (code.equals(temp.getCode())) {
                return temp;
            }
        }
        return null;
    }

}
