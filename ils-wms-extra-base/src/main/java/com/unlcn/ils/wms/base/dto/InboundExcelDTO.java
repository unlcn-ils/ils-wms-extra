package com.unlcn.ils.wms.base.dto;

import java.util.Date;

/**
 * Created by lenovo on 2017/11/6.
 */
public class InboundExcelDTO {
    // 编号
    private Integer no;

    // 日期
    private String date;

    // 入库时间
    private String inboundDate;

    // 库存创建时间
    private Date gmtCreate;

    // VIN号
    private String vin;

    // 工厂提车库位
    private String factoryWhno;

    // 入库仓库
    private String whName;

    private Long odId; //入库订单id

    public Long getOdId() {
        return odId;
    }

    public void setOdId(Long odId) {
        this.odId = odId;
    }

    public Integer getNo() {
        return no;
    }

    public void setNo(Integer no) {
        this.no = no;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getInboundDate() {
        return inboundDate;
    }

    public void setInboundDate(String inboundDate) {
        this.inboundDate = inboundDate;
    }

    public Date getGmtCreate() {
        return gmtCreate;
    }

    public void setGmtCreate(Date gmtCreate) {
        this.gmtCreate = gmtCreate;
    }

    public String getVin() {
        return vin;
    }

    public void setVin(String vin) {
        this.vin = vin;
    }

    public String getFactoryWhno() {
        return factoryWhno;
    }

    public void setFactoryWhno(String factoryWhno) {
        this.factoryWhno = factoryWhno;
    }

    public String getWhName() {
        return whName;
    }

    public void setWhName(String whName) {
        this.whName = whName;
    }

    @Override
    public String toString() {
        return "InboundExcelDTO{" +
                "no=" + no +
                ", date='" + date + '\'' +
                ", inboundDate='" + inboundDate + '\'' +
                ", gmtCreate=" + gmtCreate +
                ", vin='" + vin + '\'' +
                ", factoryWhno='" + factoryWhno + '\'' +
                ", whName='" + whName + '\'' +
                '}';
    }
}
