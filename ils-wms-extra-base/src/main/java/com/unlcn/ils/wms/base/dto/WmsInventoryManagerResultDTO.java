package com.unlcn.ils.wms.base.dto;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

public class WmsInventoryManagerResultDTO implements Serializable {
    private Long invlocId;
    private String materialCode;
    private String materialName;
    private String colourCode;
    private String colourName;
    /**
     * 底盘号
     */
    private String invlocVin;

    /**
     * 库区
     */
    private String invlocZoneName;

    /**
     * 库位
     */
    private String invlocLocCode;

    /**
     * 生产日期
     */
    private Date atProductionDate;

    /**
     * 下线日期
     */
    private Date atOfflineDate;

    /**
     * 入库日期
     */
    private Date gmtCreate;

    /**
     * 库存状态
     */
    private String status;

    /**
     * 库龄
     */
    private int warehouseAge;

    private Long invId;

    private String invWhCode;

    private String invWhName;

    private String invCustomerCode;

    private String invCustomerName;

    private String invVehicleBrandCode;

    private String invVehicleBrandName;

    private String invVehicleSeriesCode;

    private String invVehicleSeriesName;

    private String invVehicleSpecCode;

    private String invVehicleSpecName;

    private String invVehicleSpecDesc;

    private Long invNum;

    private String invVin;

    private List<WmsInventoryLocationManageDTO> wmsInventoryLocationDTOList;

    public Long getInvlocId() {
        return invlocId;
    }

    public void setInvlocId(Long invlocId) {
        this.invlocId = invlocId;
    }

    public String getMaterialCode() {
        return materialCode;
    }

    public void setMaterialCode(String materialCode) {
        this.materialCode = materialCode;
    }

    public String getMaterialName() {
        return materialName;
    }

    public void setMaterialName(String materialName) {
        this.materialName = materialName;
    }

    public String getColourCode() {
        return colourCode;
    }

    public void setColourCode(String colourCode) {
        this.colourCode = colourCode;
    }

    public String getColourName() {
        return colourName;
    }

    public void setColourName(String colourName) {
        this.colourName = colourName;
    }

    public String getInvlocVin() {
        return invlocVin;
    }

    public void setInvlocVin(String invlocVin) {
        this.invlocVin = invlocVin;
    }

    public String getInvlocZoneName() {
        return invlocZoneName;
    }

    public void setInvlocZoneName(String invlocZoneName) {
        this.invlocZoneName = invlocZoneName;
    }

    public String getInvlocLocCode() {
        return invlocLocCode;
    }

    public void setInvlocLocCode(String invlocLocCode) {
        this.invlocLocCode = invlocLocCode;
    }

    public Date getAtProductionDate() {
        return atProductionDate;
    }

    public void setAtProductionDate(Date atProductionDate) {
        this.atProductionDate = atProductionDate;
    }

    public Date getAtOfflineDate() {
        return atOfflineDate;
    }

    public void setAtOfflineDate(Date atOfflineDate) {
        this.atOfflineDate = atOfflineDate;
    }

    public Date getGmtCreate() {
        return gmtCreate;
    }

    public void setGmtCreate(Date gmtCreate) {
        this.gmtCreate = gmtCreate;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public int getWarehouseAge() {
        return warehouseAge;
    }

    public void setWarehouseAge(int warehouseAge) {
        this.warehouseAge = warehouseAge;
    }

    public Long getInvId() {
        return invId;
    }

    public void setInvId(Long invId) {
        this.invId = invId;
    }

    public String getInvWhCode() {
        return invWhCode;
    }

    public void setInvWhCode(String invWhCode) {
        this.invWhCode = invWhCode;
    }

    public String getInvWhName() {
        return invWhName;
    }

    public void setInvWhName(String invWhName) {
        this.invWhName = invWhName;
    }

    public String getInvCustomerCode() {
        return invCustomerCode;
    }

    public void setInvCustomerCode(String invCustomerCode) {
        this.invCustomerCode = invCustomerCode;
    }

    public String getInvCustomerName() {
        return invCustomerName;
    }

    public void setInvCustomerName(String invCustomerName) {
        this.invCustomerName = invCustomerName;
    }

    public String getInvVehicleBrandCode() {
        return invVehicleBrandCode;
    }

    public void setInvVehicleBrandCode(String invVehicleBrandCode) {
        this.invVehicleBrandCode = invVehicleBrandCode;
    }

    public String getInvVehicleBrandName() {
        return invVehicleBrandName;
    }

    public void setInvVehicleBrandName(String invVehicleBrandName) {
        this.invVehicleBrandName = invVehicleBrandName;
    }

    public String getInvVehicleSeriesCode() {
        return invVehicleSeriesCode;
    }

    public void setInvVehicleSeriesCode(String invVehicleSeriesCode) {
        this.invVehicleSeriesCode = invVehicleSeriesCode;
    }

    public String getInvVehicleSeriesName() {
        return invVehicleSeriesName;
    }

    public void setInvVehicleSeriesName(String invVehicleSeriesName) {
        this.invVehicleSeriesName = invVehicleSeriesName;
    }

    public String getInvVehicleSpecCode() {
        return invVehicleSpecCode;
    }

    public void setInvVehicleSpecCode(String invVehicleSpecCode) {
        this.invVehicleSpecCode = invVehicleSpecCode;
    }

    public String getInvVehicleSpecName() {
        return invVehicleSpecName;
    }

    public void setInvVehicleSpecName(String invVehicleSpecName) {
        this.invVehicleSpecName = invVehicleSpecName;
    }

    public String getInvVehicleSpecDesc() {
        return invVehicleSpecDesc;
    }

    public void setInvVehicleSpecDesc(String invVehicleSpecDesc) {
        this.invVehicleSpecDesc = invVehicleSpecDesc;
    }

    public Long getInvNum() {
        return invNum;
    }

    public void setInvNum(Long invNum) {
        this.invNum = invNum;
    }

    public String getInvVin() {
        return invVin;
    }

    public void setInvVin(String invVin) {
        this.invVin = invVin;
    }

    public List<WmsInventoryLocationManageDTO> getWmsInventoryLocationDTOList() {
        return wmsInventoryLocationDTOList;
    }

    public void setWmsInventoryLocationDTOList(List<WmsInventoryLocationManageDTO> wmsInventoryLocationDTOList) {
        this.wmsInventoryLocationDTOList = wmsInventoryLocationDTOList;
    }
}
