package com.unlcn.ils.wms.web.controller.outbound;

import cn.huiyunche.commons.domain.ResultDTO;
import cn.huiyunche.commons.domain.ResultDTOWithPagination;
import cn.huiyunche.commons.exception.BusinessException;
import com.unlcn.ils.wms.backend.bo.outboundBO.CreatePreparationPlanBO;
import com.unlcn.ils.wms.backend.bo.outboundBO.GetShipmentPLanListBO;
import com.unlcn.ils.wms.backend.enums.WarehouseEnum;
import com.unlcn.ils.wms.backend.service.outbound.WmsShipmentPlanService;
import com.unlcn.ils.wms.base.businessDTO.outbound.WmsShipmentPlanVehicleDTO;
import com.unlcn.ils.wms.web.vo.outbound.CreatePreparationPlanVO;
import com.unlcn.ils.wms.web.vo.outbound.GetShipmentPLanListVO;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.List;

@Controller
@RequestMapping("/wmsShipmentPlan")
@ResponseBody
public class WmsShipmentPlanController {

    private final static Logger LOGGER = LoggerFactory.getLogger(WmsShipmentPlanController.class);

    @Autowired
    WmsShipmentPlanService wmsShipmentPlanService;

    /**
     * 发运计划列表
     * <p>
     * 2018-3-8 调整默认不展示未处理已驳回数据
     * </p>
     *
     * @param vo 参数封装
     * @return 返回值
     */
    @RequestMapping(value = "/getShipmentPLanList", method = RequestMethod.POST)
    public ResultDTOWithPagination<Object> getShipmentPLanList(@RequestBody GetShipmentPLanListVO vo,
                                                               HttpServletRequest request) {
        LOGGER.info("WmsShipmentPlanController.getShipmentPLanList param : {} ", vo);
        ResultDTOWithPagination<Object> result = new ResultDTOWithPagination<>(true, null, "查询成功", vo);
        try {
            GetShipmentPLanListBO bo = new GetShipmentPLanListBO();
            BeanUtils.copyProperties(vo, bo);
            String whCode = request.getHeader("whCode");
            if (StringUtils.isBlank(whCode)) {
                return result;
            }
            bo.setSpWhCode(whCode);
            if (WarehouseEnum.JM_CS.getWhCode().equals(whCode)
                    || WarehouseEnum.JM_XY.getWhCode().equals(whCode)) {
                result = wmsShipmentPlanService.getShipmentPLanListForNew(bo);
            } else if (WarehouseEnum.UNLCN_XN_CQ.getWhCode().equals(whCode)) {
                result = wmsShipmentPlanService.getShipmentPLanList(bo);
            }
        } catch (Exception ex) {
            result.setSuccess(false);
            result.setMessage(ex.getMessage());
            LOGGER.error("WmsShipmentPlanController.getShipmentPLanList error : ", ex);
        }
        return result;
    }

    /**
     * 发运计划对应车型列表
     *
     * @param spId
     * @return
     */
    @RequestMapping(value = "/getShipmentPLanVehicleList/{spId}", method = RequestMethod.GET)
    public ResultDTO<Object> getShipmentPLanVehicleList(@PathVariable("spId") String spId) {
        LOGGER.info("WmsShipmentPlanController.getShipmentPLanVehicleList param : {} ", spId);
        ResultDTO<Object> result = new ResultDTO<>(true, null, "查询成功");
        try {
            List<WmsShipmentPlanVehicleDTO> list = wmsShipmentPlanService.getShipmentPLanVehicleList(spId);
            result.setData(list);
        } catch (Exception ex) {
            result.setSuccess(false);
            result.setMessage(ex.getMessage());
            LOGGER.error("WmsShipmentPlanController.getShipmentPLanVehicleList error : ", ex);
        }
        return result;
    }

    /**
     * 生成备料计划
     * <p>
     * 2017-12-28 新需求:创建备料计划时，弹出对话框计划进场时间系统后台自动＋3小时形式
     * 2018-1-23 君马仓库改成以code的方式去生成计划
     * 2018-3-8 君马库生成备料计划之前可能发运计划已经进行过驳回,调整生成备料计划的逻辑
     * </p>
     *
     * @param voList 参数封装对象
     * @return 返回值
     */
    @RequestMapping(value = "/createPreparationPlan", method = RequestMethod.POST)
    public ResultDTO<Object> createPreparationPlan(@RequestBody List<CreatePreparationPlanVO> voList,
                                                   HttpServletRequest request) {
        LOGGER.info("WmsShipmentPlanController.createPreparationPlan param : {} ", voList);
        ResultDTO<Object> result = new ResultDTO<>(true, null, "创建成功");
        try {
            String whCode = request.getHeader("whCode");
            String userId = request.getHeader("userId");
            if (StringUtils.isBlank(whCode)) {
                result.setSuccess(false);
                result.setMessage("当前登录用户未绑定仓库");
                return result;
            }
            if (WarehouseEnum.UNLCN_XN_CQ.getWhCode().equals(whCode)) {
                //重庆库逻辑
                if (CollectionUtils.isNotEmpty(voList)) {
                    List<CreatePreparationPlanBO> boList = new ArrayList<>();
                    for (CreatePreparationPlanVO vo : voList) {
                        CreatePreparationPlanBO createPreparationPlanBO = new CreatePreparationPlanBO();
                        BeanUtils.copyProperties(vo, createPreparationPlanBO);
                        boList.add(createPreparationPlanBO);
                    }
                    wmsShipmentPlanService.createPreparationPlan(boList, userId);
                }
            } else if (WarehouseEnum.JM_CS.getWhCode().equals(whCode) || WarehouseEnum.JM_XY.getWhCode().equals(whCode)) {
                //君马库逻辑
                List<CreatePreparationPlanBO> boList = new ArrayList<>();
                if (CollectionUtils.isNotEmpty(voList)) {
                    for (CreatePreparationPlanVO vo : voList) {
                        CreatePreparationPlanBO createPreparationPlanBO = new CreatePreparationPlanBO();
                        BeanUtils.copyProperties(vo, createPreparationPlanBO);
                        boList.add(createPreparationPlanBO);
                    }
                    if (CollectionUtils.isNotEmpty(boList)) {
                        wmsShipmentPlanService.createPreparationPlanForJunMa(boList, whCode, userId);
                    }
                }
            }
        } catch (BusinessException ex) {
            result.setSuccess(false);
            result.setMessage(ex.getMessage());
            LOGGER.error("WmsShipmentPlanController.saveCreatePreparationPlan BusinessException : ", ex);
        } catch (Exception ex) {
            result.setSuccess(false);
            result.setMessage("创建备料计划异常");
            LOGGER.error("WmsShipmentPlanController.saveCreatePreparationPlan error : ", ex);
        }
        return result;
    }
}
