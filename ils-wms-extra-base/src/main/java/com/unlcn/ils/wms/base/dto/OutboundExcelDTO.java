package com.unlcn.ils.wms.base.dto;

import java.util.Date;

/**
 * Created by lenovo on 2017/11/6.
 */
public class OutboundExcelDTO {
    // 编号
    private Integer no;

    // 日期
    private String date;

    // 出库时间
    private String outboundDate;

    // 出库任务完成时间
    private Date taskEndTime;

    // DN号
    private String waybillNo;

    // VIN号
    private String vin;

    // 工厂提车库位
    private String factoryWhno;

    // 出库仓库
    private String whName;

    //新增订单标识
    private String orderFlag;

    public String getOrderFlag() {
        return orderFlag;
    }

    public void setOrderFlag(String orderFlag) {
        this.orderFlag = orderFlag;
    }

    public Integer getNo() {
        return no;
    }

    public void setNo(Integer no) {
        this.no = no;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getOutboundDate() {
        return outboundDate;
    }

    public void setOutboundDate(String outboundDate) {
        this.outboundDate = outboundDate;
    }

    public Date getTaskEndTime() {
        return taskEndTime;
    }

    public void setTaskEndTime(Date taskEndTime) {
        this.taskEndTime = taskEndTime;
    }

    public String getWaybillNo() {
        return waybillNo;
    }

    public void setWaybillNo(String waybillNo) {
        this.waybillNo = waybillNo;
    }

    public String getVin() {
        return vin;
    }

    public void setVin(String vin) {
        this.vin = vin;
    }

    public String getFactoryWhno() {
        return factoryWhno;
    }

    public void setFactoryWhno(String factoryWhno) {
        this.factoryWhno = factoryWhno;
    }

    public String getWhName() {
        return whName;
    }

    public void setWhName(String whName) {
        this.whName = whName;
    }

    @Override
    public String toString() {
        return "OutboundExcelDTO{" +
                "no=" + no +
                ", date='" + date + '\'' +
                ", outboundDate='" + outboundDate + '\'' +
                ", taskEndTime=" + taskEndTime +
                ", waybillNo='" + waybillNo + '\'' +
                ", vin='" + vin + '\'' +
                ", factoryWhno='" + factoryWhno + '\'' +
                ", whName='" + whName + '\'' +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof OutboundExcelDTO)) return false;

        OutboundExcelDTO that = (OutboundExcelDTO) o;

        if (getTaskEndTime() != null ? !getTaskEndTime().equals(that.getTaskEndTime()) : that.getTaskEndTime() != null)
            return false;
        if (getWaybillNo() != null ? !getWaybillNo().equals(that.getWaybillNo()) : that.getWaybillNo() != null)
            return false;
        if (getVin() != null ? !getVin().equals(that.getVin()) : that.getVin() != null) return false;
        if (getFactoryWhno() != null ? !getFactoryWhno().equals(that.getFactoryWhno()) : that.getFactoryWhno() != null)
            return false;
        return getWhName() != null ? getWhName().equals(that.getWhName()) : that.getWhName() == null;
    }

    @Override
    public int hashCode() {
        int result = getTaskEndTime() != null ? getTaskEndTime().hashCode() : 0;
        result = 31 * result + (getWaybillNo() != null ? getWaybillNo().hashCode() : 0);
        result = 31 * result + (getVin() != null ? getVin().hashCode() : 0);
        result = 31 * result + (getFactoryWhno() != null ? getFactoryWhno().hashCode() : 0);
        result = 31 * result + (getWhName() != null ? getWhName().hashCode() : 0);
        return result;
    }
}
