package com.unlcn.ils.wms.web.vo.outbound;

/**
 * Created by DELL on 2017/9/26.
 */
public class WmsPreparationVehicleDetailVO {

    /**
     * 主键
     */
    private Long vdId;

    /**
     * 备料计划表ID
     */
    private Long vdPpId;

    /**
     * 车架号(VIN码)
     */
    private String vdVin;

    /**
     * 车型名称
     */
    private String vdVehicleName;

    /**
     * 车型描述
     */
    private String vdVehicleDesc;

    /**
     * 客户运单号
     */
    private String vdWaybillNo;

    /**
     * 出库状态
     */
    private String vdOutstockStatus;

    public Long getVdId() {
        return vdId;
    }

    public void setVdId(Long vdId) {
        this.vdId = vdId;
    }

    public Long getVdPpId() {
        return vdPpId;
    }

    public void setVdPpId(Long vdPpId) {
        this.vdPpId = vdPpId;
    }

    public String getVdVin() {
        return vdVin;
    }

    public void setVdVin(String vdVin) {
        this.vdVin = vdVin;
    }

    public String getVdVehicleName() {
        return vdVehicleName;
    }

    public void setVdVehicleName(String vdVehicleName) {
        this.vdVehicleName = vdVehicleName;
    }

    public String getVdVehicleDesc() {
        return vdVehicleDesc;
    }

    public void setVdVehicleDesc(String vdVehicleDesc) {
        this.vdVehicleDesc = vdVehicleDesc;
    }

    public String getVdWaybillNo() {
        return vdWaybillNo;
    }

    public void setVdWaybillNo(String vdWaybillNo) {
        this.vdWaybillNo = vdWaybillNo;
    }

    public String getVdOutstockStatus() {
        return vdOutstockStatus;
    }

    public void setVdOutstockStatus(String vdOutstockStatus) {
        this.vdOutstockStatus = vdOutstockStatus;
    }

    @Override
    public String toString() {
        return "WmsPreparationVehicleDetailVO{" +
                "vdId=" + vdId +
                ", vdPpId=" + vdPpId +
                ", vdVin='" + vdVin + '\'' +
                ", vdVehicleName='" + vdVehicleName + '\'' +
                ", vdVehicleDesc='" + vdVehicleDesc + '\'' +
                ", vdWaybillNo='" + vdWaybillNo + '\'' +
                ", vdOutstockStatus='" + vdOutstockStatus + '\'' +
                '}';
    }
}
