package com.unlcn.ils.wms.base.mapper.extmapper;

import com.unlcn.ils.wms.base.model.bigscreen.BiFrontOnWay;

import java.util.HashMap;
import java.util.List;

public interface BiFrontOnWayExtMapper {
    /**
     * 前端在途信息列表
     *
     * @return
     */
    List<BiFrontOnWay> onWayLine();

    List<BiFrontOnWay> selectOnWayLineByParam(HashMap<String, Object> paramMap);

    int selectCountOnWayLineByParam(HashMap<String, Object> paramMap);
}