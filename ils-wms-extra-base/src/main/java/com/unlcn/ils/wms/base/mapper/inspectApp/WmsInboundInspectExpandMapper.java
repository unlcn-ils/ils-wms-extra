package com.unlcn.ils.wms.base.mapper.inspectApp;

import cn.huiyunche.commons.domain.PageVo;
import com.unlcn.ils.wms.base.dto.WmsInboundInspectDTO;
import com.unlcn.ils.wms.base.dto.WmsInboundNotMoveCarDTO;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

public interface WmsInboundInspectExpandMapper {
    List<WmsInboundInspectDTO> selectInboundInspectByVin(Map<String, Object> paramMap);

    List<WmsInboundInspectDTO> selectInboundInspectByWaybillNo(@Param("waybillNo") String waybillNo, @Param("del") byte del);

    List<WmsInboundInspectDTO> selectJMInboundInspectByVin(@Param("vin") String vin, @Param("del") byte del, @Param("createValue") int createValue);

    List<WmsInboundInspectDTO> selectJMInboundInspectByWaybillNo(@Param("waybillNo") String vin, @Param("del") byte del, @Param("createValue") int createValue);

    List<WmsInboundNotMoveCarDTO> selectNotMoveCarListForJM(@Param("whCode") String whcode, @Param("excp") int confirmExcp, @Param("qulify") int qualifyValue, @Param("del") byte del, @Param("pageVo") PageVo vo, @Param("createValue") int createValue, @Param("missCode") String missCode);

    List<WmsInboundInspectDTO> selectNotMoveCarInfoByVinParam(Map<String, Object> paramMap);

    List<WmsInboundInspectDTO> selectNotMoveCarInfoByWaybillParam(Map<Object, Object> paramMap);

    List<WmsInboundNotMoveCarDTO> selectNotMoveCarListForCQ(Map<String, Object> paramMap);

    List<WmsInboundInspectDTO> selectNotMoveByVinLikeParamForCQ(Map<String, Object> paramMap);


    //List<TmsInspectExcpDTO> selectExcpListByPositionId(@Param("inspectId") Long inspectId, @Param("positionId") Integer positionId, @Param("closeValue") Integer closeExcp, @Param("del") int del_flag);
    //
    //List<WmsOutboundInspectExcpDTO> selectPositionsByInspectId(@Param("del") int del_flag);
    //
    //int selectExcpCountByPositionIdAndInspectId(@Param("positionId") Integer positionId, @Param("inspectId") Long inspectId, @Param("closeExcp") int closeExcp, @Param("del") int del_flag);
    //
    //int selectExcpCountByInspectId(@Param("inspectId") Long id, @Param("excpValue") int excpValue, @Param("del") int del_value);
    //
    //int selectExcpTotalCount(@Param("excpValue") int excpValue, @Param("damageValue") int damageValue, @Param("compromiseValue") int compromiseValue, @Param("hasNotPickup") int hasNotPickup, @Param("closedExcp") int closedExcp, @Param("del") int del_value);
}