package com.unlcn.ils.wms.base.mapper.inspectApp;

import com.unlcn.ils.wms.base.dto.TmsInspectComponentMissDTO;
import org.apache.ibatis.annotations.Param;

import java.util.HashMap;
import java.util.List;

public interface WmsOutboundComponentMissExpandMapper {


    List<TmsInspectComponentMissDTO> selectMissComponent(HashMap<String, Object> params);

    List<TmsInspectComponentMissDTO> selectNotMissComponent(HashMap<String, Object> params);

    List<TmsInspectComponentMissDTO> selectMissComponentNew(HashMap<String, Object> params);
}
