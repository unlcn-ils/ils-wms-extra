package com.unlcn.ils.wms.backend.service.sys;

import java.util.List;

import com.unlcn.ils.wms.backend.bo.sys.MenuBO;

/**
 * @author laishijian
 * @ClassName: MenuService
 * @Description: 菜单
 * @date 2017年8月9日 上午10:00:58
 */
public interface MenuService {

    /**
     * @param @param  menuBO
     * @param @return
     * @return Integer    返回类型
     * @throws
     * @Title: add
     * @Description: 新增
     */
    Integer add(MenuBO menuBO) throws Exception;

    /**
     * @param @return
     * @return List<MenuBO>    返回类型
     * @throws
     * @Title: findMenuAndChildMenu
     * @Description: 获取菜单和子菜单
     */
    List<MenuBO> findMenuAndChildMenu() throws Exception;

    /**
     * @param @param  parentId
     * @param @return
     * @param @throws Exception
     * @return List<MenuBO>    返回类型
     * @throws
     * @Title: findByParentId
     * @Description: 根据父id获取
     */
    List<MenuBO> findByParentId(Integer parentId) throws Exception;

    /**
     * @param @param  parentId
     * @param @return
     * @param @throws Exception
     * @return List<MenuBO>    返回类型
     * @throws
     * @Title: findMenuAndChildByParentId
     * @Description: 查询菜单及子菜单根据父部门id
     */
    List<MenuBO> findMenuAndChildByParentId(Integer parentId) throws Exception;

}
