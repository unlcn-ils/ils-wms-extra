package com.unlcn.ils.wms.base.dto;

import java.io.Serializable;
import java.util.List;

public class WmsOutboundResultDTO implements Serializable {
    private List notOutbound;
    private List hasOutbound;
    private String orderNo;
    private String outboundType;
    private Integer notOutboundNum;
    private Integer hasOutboundNum;

    public Integer getNotOutboundNum() {
        return notOutboundNum;
    }

    public void setNotOutboundNum(Integer notOutboundNum) {
        this.notOutboundNum = notOutboundNum;
    }

    public Integer getHasOutboundNum() {
        return hasOutboundNum;
    }

    public void setHasOutboundNum(Integer hasOutboundNum) {
        this.hasOutboundNum = hasOutboundNum;
    }

    public List getNotOutbound() {
        return notOutbound;
    }

    public void setNotOutbound(List notOutbound) {
        this.notOutbound = notOutbound;
    }

    public List getHasOutbound() {
        return hasOutbound;
    }

    public void setHasOutbound(List hasOutbound) {
        this.hasOutbound = hasOutbound;
    }

    public String getOrderNo() {
        return orderNo;
    }

    public void setOrderNo(String orderNo) {
        this.orderNo = orderNo;
    }

    public String getOutboundType() {
        return outboundType;
    }

    public void setOutboundType(String outboundType) {
        this.outboundType = outboundType;
    }
}
