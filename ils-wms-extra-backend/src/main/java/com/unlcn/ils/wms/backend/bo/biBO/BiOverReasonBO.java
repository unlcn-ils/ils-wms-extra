package com.unlcn.ils.wms.backend.bo.biBO;

import java.io.Serializable;

/**
 * Created by Nicky on 17/11/9.
 */
public class BiOverReasonBO implements Serializable {
    /**
     * 质损
     */
    private String losses;

    /**
     * 管控
     */
    private String man_ctrls;

    /**
     *暂运
     */
    private String suspends;

    public String getLosses() {
        return losses;
    }

    public void setLosses(String losses) {
        this.losses = losses;
    }

    public String getMan_ctrls() {
        return man_ctrls;
    }

    public void setMan_ctrls(String man_ctrls) {
        this.man_ctrls = man_ctrls;
    }

    public String getSuspends() {
        return suspends;
    }

    public void setSuspends(String suspends) {
        this.suspends = suspends;
    }

    @Override
    public String toString() {
        return "BiOverReasonBO{" +
                "losses='" + losses + '\'' +
                ", man_ctrls='" + man_ctrls + '\'' +
                ", suspends='" + suspends + '\'' +
                '}';
    }
}
