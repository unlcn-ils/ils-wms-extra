package com.unlcn.ils.wms.base.mapper.extmapper;

import com.unlcn.ils.wms.base.model.outbound.WmsPreparationVehicleDetail;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * @Auther linbao
 * @Date 2017-10-24
 */
public interface WmsPreparationVehicleDetailCustomMapper {

    /**
     * 批量插入
     *
     * @param detailList
     */
    void insertForBatch(@Param("detailList") List<WmsPreparationVehicleDetail> detailList);

    /**
     * 根据车架号批量删除
     * @param vdVins
     */
    void batchDeleteDetailByVin(@Param("vdVins") List<String> vdVins);
    //void selectPreparationByVin(@Param("vin") String vin,@Param("del") byte value);
}
