package com.unlcn.ils.wms.api.wms2cs;


import com.unlcn.ils.wms.api.rest.gate.GateController;
import com.unlcn.ils.wms.api.vo.GateVO;
import io.swagger.annotations.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping(value = "/api/wms2Cs")
@Api(value = "/api/wms2Cs", description = "wms对接车商系统接口")
@ResponseBody
public class Wms2CSController {

    private final static Logger LOGGER = LoggerFactory.getLogger(GateController.class);


    @ApiOperation(value = "查询对应库存", notes = "根据时间查询对应天数之前的库存")
    @ApiResponses(value = {@ApiResponse(code = 400, message = "Invalid id supplied"),
            @ApiResponse(code = 404, message = "inboundOpenToPlateNum not found")})
    @RequestMapping(value = "/inboundOpenToPlateNum", method = RequestMethod.POST)
    public String inboundOpenToPlateNum(@ApiParam(required = false, value = "查询参数") GateVO gateVO) {
        LOGGER.info("查询入库的库存数。。对接车商系统");
        return null;
    }
}
