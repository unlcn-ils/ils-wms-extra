package com.unlcn.ils.wms.backend.enums;

/**
 * 维修单状态
 * Created by DELL on 2017/8/24.
 */
public enum WmsRepairStatusEnum {
    WMS_REPAIR_STATUS_NOT_HANDLE(10,"未处理"),
    WMS_REPAIR_STATUS_ING(20,"维修中"),
    WMS_REPAIR_STATUS_COMPLETE(30,"已完成");

    private final int value;
    private final String text;
    WmsRepairStatusEnum(int value, String text){
        this.value = value;
        this.text = text;
    }

    public int getValue() {
        return value;
    }

    public String getText() {
        return text;
    }

    public static WmsRepairStatusEnum getByValue(int value){
        for (WmsRepairStatusEnum temp : WmsRepairStatusEnum.values()) {
            if(temp.getValue() == value){
                return temp;
            }
        }
        return null;
    }
}
