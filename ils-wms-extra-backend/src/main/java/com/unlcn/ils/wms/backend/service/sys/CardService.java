package com.unlcn.ils.wms.backend.service.sys;

import com.unlcn.ils.wms.base.model.sys.SysUser;
import com.unlcn.ils.wms.base.model.sys.WmsCard;

import java.util.List;
import java.util.Map;

/**
 * Created by lenovo on 2017/11/2.
 */
public interface CardService {

    /**
     * 门禁卡列表
     * @param paramsMap
     * @return
     */
    List<WmsCard> selectCardLine(Map<String, Object> paramsMap);

    /**
     * 门禁卡列表统计
     * @param paramsMap
     * @return
     */
    Integer countCardLineCount(Map<String, Object> paramsMap);

    /**
     * 新增门禁卡
     * @param wmsCard
     */
    Integer add(WmsCard wmsCard) throws Exception;

    /**
     * 编辑门禁卡
     * @param wmsCard
     * @return
     */
    Integer modify(WmsCard wmsCard) throws Exception;

    /**
     * 获取司机账户信息
     * @return
     */
    List<SysUser> queryDriverUser();

    /**
     * 判断当前卡号是否有可用状态下的绑定
     *
     * @param carNum
     * @param id 更新需要判断
     * @return true-未绑定, false-已绑定
     * @throws Exception
     */
    boolean queryCheckCarNum(String carNum, Long id) throws Exception;

    /**
     * 判断当前司机下的是否有可用的绑定
     *
     * @param driverId
     * @param id 更新需要判断
     * @return true-未绑定, false-已绑定
     * @throws Exception
     */
    boolean queryCheckDriverId(Long driverId, Long id) throws Exception;
}
