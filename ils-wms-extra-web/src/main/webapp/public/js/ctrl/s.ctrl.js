/**
 * Created by houjianhui on 2017/7/19.
 */
'use strict';
wmsCtrl.controller('stockUnitCtrl',['$scope','stockUnitService',function(scope,stockUnitService){   //库位查询

    //更多条件
    scope.moreText = scope.constant.moreText.spread;
    scope.moreClick = function(){
        scope.otherForm = !scope.otherForm;
        if(scope.otherForm){
            scope.moreText = scope.constant.moreText.packUp;
        }else{
            scope.moreText = scope.constant.moreText.spread;
        }
    }

    //状态下拉
    scope.dropDownList = {
        status : scope.Tools.dropDown.statusDropDown
    }

    //状态下拉默认值
    scope.dropDownDefault = {
        status : scope.Tools.dropDown.statusDropDown[0]
    }

    //仓库
    var shck = [
        {
            name:'我只是仓库',
            value:'1'
        },
        {
            name:'我只是仓库1',
            value:'2'
        },
        {
            name:'我只是仓库2',
            value:'3'
        }
    ]

    /*状态数据*/
    scope.dropDownList = {
        status : scope.Tools.dropDown.stockUnitStatusDropDown.concat(),    //库位状态
        warehouse : shck    //仓库
    }
    scope.dropDownList.status.unshift(scope.constant.defaultDropDownNullData) //插入状态空数据
    scope.dropDownList.warehouse.unshift(scope.constant.defaultDropDownNullData) //插入仓库空数据


    //设置默认值
    scope.dropDownDefault = {
        status :  scope.dropDownList.status[0], //库位状态
        warehouse : scope.dropDownList.warehouse[0] //仓库
    }

    //TODO 仓库code默认写死
    scope.dropDownService.getZoneList('001')
        .then(function(result){
            if(result.success){
                scope.dropDownList.zone = result.data;
                scope.dropDownList.zone.unshift(scope.constant.defaultDropDownNullData)
                scope.dropDownDefault.zone = scope.dropDownList.zone[0];
            }else{
                scope.Tools.alert(result.message);
            }
        })

    //新增库位 click
    scope.addStockUnitClick = function(){
        //TODO 仓库code默认写死
        scope.dropDownService.getZoneList('001')
            .then(function(result){
                if(result.success){
                    scope.Tools.dialog({
                        controller : 'stockUnitCtrl.dialog',
                        template : '../public/template/locationManagement/stockUnitManagement/stock-unit-form.html',
                        closeByDocument : false,
                        closeByEscape : false,
                        data : {
                            warehouse : shck,   //仓库
                            zone : result.data,
                            dropDownDefault : {    //下拉框默认值
                                warehouse : shck[0], //仓库默认值
                                zone : result.data[0]   //库区默认值
                            }
                        },
                        width : 530,
                        title : '新增库位',
                        scope : scope
                    })
                }else{
                    scope.Tools.alert(result.message);
                }
            })
    }

    //更新库位 click
    scope.modifyStockUnitClick = function(){

        var indexArr = scope.Tools.getTableSelectItemIndex('.stockUnitTable');  //获取table所选的下标

        if(indexArr.length == 0){
            scope.Tools.alert('请选择需要更新的数据');
            return;
        }

        if(indexArr.length > 1){
            scope.Tools.alert('只能选择一条数据进行更新');
            return;
        }
        //查询库位数据
        stockUnitService.getById(scope.stockUnitList[indexArr[0]].locId)
            .then(function(result){
                if(result.success){
                    scope.dropDownService.getZoneList('001')
                        .then(function(res){
                            //库区默认值
                            var defaultZone = null;
                            for(var i = 0; i < res.data.length; i++){
                                if(res.data[i].zeZoneCode == result.data.locZoneCode){
                                    defaultZone = res.data[i];
                                }
                            }

                            //设置仓库默认值
                            var defaultCk = {};
                            for(var i = 0; i < shck.length;i++){
                                if(shck[i].value == result.data.locWhId){
                                    defaultCk = shck[i];
                                    break;
                                }
                            }

                            if(res.success){
                                scope.Tools.dialog({
                                    controller : 'stockUnitCtrl.dialog',
                                    template : '../public/template/locationManagement/stockUnitManagement/stock-unit-form.html',
                                    closeByDocument : false,
                                    closeByEscape : false,
                                    data : {
                                        warehouse : shck,   //仓库
                                        zone : res.data,
                                        dropDownDefault : {    //下拉框默认值
                                            warehouse : defaultCk, //仓库默认值
                                            zone : defaultZone   //库区默认值
                                        },
                                        stockUnit : result.data
                                    },
                                    width : 530,
                                    title : '更新库位',
                                    scope : scope
                                })
                            }else{
                                scope.Tools.alert(res.message);
                            }
                        })
                }else{
                    scope.Tools.alert(result.message);
                }
            })


    }

    //批量新增库位 click
    scope.addBatchStockUnitClick = function(){
        scope.Tools.dialog({
            controller : 'stockUnitCtrl.dialog',
            template : '../public/template/locationManagement/stockUnitManagement/stock-unit-batch-add.html',
            closeByDocument : false,
            closeByEscape : false,
            width : 530,
            title : '批量新增库位',
            scope : scope
        })
    }

    //列表条件参数
    scope.listParam = function(){

        if(!scope.search){
            scope.search = {};
        }

        scope.search.pageSize = scope.pageSize;    //设置最大页
        if(scope.otherForm){    //展开了更多
            scope.search.locEnableFlag = scope.dropDownDefault.status.value;   //状态
            scope.search.locWhId = scope.dropDownDefault.warehouse.value;   //仓库
            scope.search.locZoneId = scope.dropDownDefault.zone.zeId;   //库区
            return scope.search;
        }else{
            return scope.search;
        }
    }

    //列表
    scope.fristSelect = true;   //判断是否是第一次查询
    scope.list = function(params){
        //设置分页参数
        if(!params){
            params = {};
        }
        params.pageSize = params.pageSize ? params.pageSize : scope.constant.page.pageSize;
        params.pageNo = params.pageNo ? params.pageNo : scope.constant.page.pageNo;

        stockUnitService.list(params)
            .then(function(result){
                if(result.success){
                    scope.stockUnitList = result.data.wmsLocationList;
                    if(scope.fristSelect){
                        scope.Tools.page(document.getElementById('pages'),{
                            pages : result.data.wmsLocationBO.totalPage,
                            curr : result.data.wmsLocationBO.pageNo,
                            count : result.data.wmsLocationBO.totalRecord
                        },function(obj, first){
                            scope.fristSelect = first;  //设置状态
                            //初始化多选框
                            scope.Tools.initCheckBox();
                            if(!first){ //判断不是第一次加载
                                var param = scope.listParam();
                                param.pageNo = obj.curr;    //当前页
                                param.pageSize = obj.limit; //最大页
                                scope.pageSize = obj.limit; //设置最大页
                                scope.pageNo = obj.curr; //设置当前页
                                scope.list(param);
                            }
                        })
                    }else{
                        //初始化多选框
                        scope.Tools.initCheckBox();
                    }

                }else{
                    scope.Tools.alert(result.message);
                }
            })
    }

    //列表
    scope.list();

    //查询 click
    scope.searchClick = function(){
        scope.fristSelect = true;   //设置为第一次查询
        scope.list(scope.listParam());
    }

    //重置 click
    scope.resetClick = function(){
        scope.search = {};
    }

    //刷新 click
    scope.refreshClick = function(){
        scope.searchClick();
    }

    //启用 click
    scope.startStockUnitClick = function(){
        var indexArr = scope.Tools.getTableSelectItemIndex('.stockUnitTable');  //获取table所选的下标

        if(indexArr.length == 0){
            scope.Tools.alert('请选择需要启用的数据');
            return;
        }

        //选中的库位数据
        var stockUnitList = scope.Tools.getDataTOIndex(indexArr,scope.stockUnitList);

        scope.Tools.dialogConfirm('确定要启用选中的数据吗？')
            .then(function(res){
                if(res){
                    stockUnitService.enableLocation(stockUnitList,scope.constant.stockUnitStatus.start)
                        .then(function(result){
                            if(result.success){
                                scope.Tools.alert('启用数据成功');
                                scope.list(scope.listParam());
                            }else{
                                scope.Tools.alert(result.message);
                            }
                        })
                }
            })
    }

    //禁用 click
    scope.stopStockUnitClick = function(){
        var indexArr = scope.Tools.getTableSelectItemIndex('.stockUnitTable');  //获取table所选的下标

        if(indexArr.length == 0){
            scope.Tools.alert('请选择需要启用的数据');
            return;
        }

        //选中的库位数据
        var stockUnitList = scope.Tools.getDataTOIndex(indexArr,scope.stockUnitList);

        scope.Tools.dialogConfirm('确定要禁用选中的数据吗？')
            .then(function(res){
                if(res){
                    stockUnitService.enableLocation(stockUnitList,scope.constant.stockUnitStatus.stop)
                        .then(function(result){
                            if(result.success){
                                scope.Tools.alert('禁用数据成功');
                                scope.list(scope.listParam());
                            }else{
                                scope.Tools.alert(result.message);
                            }
                        })
                }
            })
    }
}])
    .controller('stockUnitCtrl.dialog',['$scope','stockUnitService',function(scope,stockUnitService){   //库位dialog

        //新增或更新
        scope.addOrUpdateClick = function(){
            //有库位id则为更新 没有则为 新增
            if(scope.ngDialogData.stockUnit.locId){
                stockUnitService.update(scope.ngDialogData.stockUnit)
                    .then(function(result){
                        if(result.success){
                            scope.Tools.alert('更新库位成功');
                            scope.$parent.fristSelect = true;   //设置为第一次查询
                            scope.list();
                            scope.closeThisDialog();
                        }else{
                            scope.Tools.alert(result.message);
                        }
                    })

            }else{
                stockUnitService.add(scope.ngDialogData.stockUnit)
                    .then(function(result){
                        if(result.success){
                            scope.Tools.alert('新增库位成功');
                            scope.$parent.fristSelect = true;   //设置为第一次查询
                            scope.list();
                            scope.closeThisDialog();
                        }else{
                            scope.Tools.alert(result.message);
                        }
                    })
            }
        }

        //仓库修改 change
        scope.changeWarehouseChange = function(item){
            if(!scope.ngDialogData.stockUnit){
                scope.ngDialogData.stockUnit = {};
            }

            scope.ngDialogData.stockUnit.locWhName = item.name;
            scope.ngDialogData.stockUnit.locWhCode = item.name;
            scope.ngDialogData.stockUnit.locWhId = item.value;
        }

        //默认选择仓库名称
        scope.changeWarehouseChange(scope.ngDialogData.dropDownDefault.warehouse);

        //库区修改 change
        scope.changeZoneChange = function(item){
            if(!scope.ngDialogData.stockUnit){
                scope.ngDialogData.stockUnit = {};
            }

            if(item){
                scope.ngDialogData.stockUnit.locZoneCode = item.zeZoneCode;
                scope.ngDialogData.stockUnit.locZoneName = item.zeZoneName;
            }
        }
        //默认选择库区名
        scope.changeZoneChange(scope.ngDialogData.dropDownDefault.zone);
    }])
    .controller('stockUnitStrategyCtrl',['$scope','stockUnitStrategyService','dropDownService',function(scope,stockUnitStrategyService,dropDownService){  //库位策略 controller
        //列表条件参数
        scope.listParam = function(){

            if(!scope.search){
                scope.search = {};
            }

            if(scope.otherForm){    //展开了更多
                return scope.search;
            }else{
                return scope.search;
            }
        }

        //列表
        /*scope.fristSelect = true;   //判断是否是第一次查询*/
        scope.list = function(params){
            //设置分页参数
            if(!params){
                params = {};
            }
            params.pageSize = params.pageSize ? params.pageSize : scope.constant.page.pageSize;
            params.pageNo = params.pageNo ? params.pageNo : scope.constant.page.pageNo

            stockUnitStrategyService.list('001')
                .then(function(result){
                    if(result.success){
                        scope.stockUnitStrategyList = result.data;
                        scope.Tools.initCheckBox();
                    }else{
                        scope.Tools.alert(result.message);
                    }
                })
        }

        //列表
        scope.list();

        //查询 click
        scope.searchClick = function(){
            scope.fristSelect = true;   //设置为第一次查询
            scope.list(scope.listParam());
        }

        //刷新 click
        scope.refreshClick = function(){
            scope.searchClick();
        }

        //回车按下查询
        scope.searchListKeyDown = function(e){
            var keycode = window.event ? e.keyCode:e.which;
            if(keycode == 13){  //回车键
                /*scope.fristSelect = true;   //设置为第一次查询*/
                scope.list(scope.listParam());
                e.preventDefault();
            }
        }

        //新增策略 click
        scope.addStockUnitStrategyClick = function(){
            scope.Tools.dialog({
                controller:'stockUnitStrategyCtrl.dialog',
                template : '../public/template/strategyManagement/stockUnitStrategy/stock-unit-strategy-form.html',
                closeByDocument : false,
                closeByEscape : false,
                width : 400,
                title : '新增策略',
                scope : scope
            })
        }

        //更新策略 click
        scope.updateStockUnitStrategyClick = function(){
            var indexArr = scope.Tools.getTableSelectItemIndex('.stockUnitStrategy');  //获取table所选的下标

            if(indexArr.length == 0){
                scope.Tools.alert('请选择需要更新的数据');
                return;
            }

            if(indexArr.length > 1){
                scope.Tools.alert('只能选择一条数据进行更新');
                return;
            }

            scope.Tools.dialog({
                controller:'stockUnitStrategyCtrl.dialog',
                template : '../public/template/strategyManagement/stockUnitStrategy/stock-unit-strategy-form.html',
                closeByDocument : false,
                closeByEscape : false,
                width : 400,
                title : '更新策略',
                scope : scope
            })
        }

        //删除 策略 click
        scope.deleteStockUnitStrategyClick = function(){
            var indexArr = scope.Tools.getTableSelectItemIndex('.stockUnitStrategy');  //获取table所选的下标

            var stockUnitStrategyList = scope.Tools.getDataTOIndex(indexArr,scope.stockUnitStrategyList);

            if(stockUnitStrategyList.length == 0){
                scope.Tools.alert('请选择需要删除的数据');
                return;
            }
        }

        //新增策略条件 click
        scope.addConditionClick = function(){
            if(!scope.selectPyCode){
                scope.Tools.alert('请选择一条策略');
                return;
            }
            //货主信息获取
            dropDownService.getCustomerInfo()
                .then(function(result){
                    scope.Tools.dialog({
                        controller:'stockUnitStrategyCtrl.dialog',
                        template : '../public/template/strategyManagement/stockUnitStrategy/strategy-condition-form.html',
                        closeByDocument : false,
                        closeByEscape : false,
                        data : {
                            conditionTypeDropDown : scope.Tools.dropDown.strategyConditionTypeDropDown, //条件类型下拉
                            strategyOperatorDropDown : scope.Tools.dropDown.strategyOperatorDropDown,   //条件运算符
                            conditionValueDropDown : result.data,    //货主条件值
                            dropDownDefault : {
                                conditionType: scope.Tools.dropDown.strategyConditionTypeDropDown[0],  //条件类型
                                operator : scope.Tools.dropDown.strategyOperatorDropDown[0], //条件运算符
                                conditionValue : result.data ? result.data[0] : {} //货主条件值
                            }
                        },
                        width : 400,
                        title : '新增策略条件',
                        scope : scope
                    })
                })
        }

        //新增流向 click
        scope.addFlowtoClick = function(){
            if(!scope.selectPyCode){
                scope.Tools.alert('请选择一条策略');
                return;
            }
            //获取区域信息
            dropDownService.getArea()
                .then(function(result){
                    if(result.success){
                        scope.Tools.dialog({
                            controller:'stockUnitStrategyCtrl.dialog',
                            template : '../public/template/strategyManagement/stockUnitStrategy/strategy-flowto-form.html',
                            closeByDocument : false,
                            closeByEscape : false,
                            data : {
                                originAreaList : result.data,   //起始区域
                                destAreaList : result.data, //目的区域
                            },
                            width : 700,
                            title : '新增流向维护',
                            scope : scope
                        })
                    }else{
                        scope.Tools.alert(result.message);
                    }
                })
        }

        //新增库位策略 click
        scope.addLocationClick = function(){
            if(!scope.selectPyCode){
                scope.Tools.alert('请选择一条策略');
                return;
            }
            //默认写死仓库code 001
            dropDownService.getZoneList('001')
                .then(function(result){
                    if(result.success){
                        scope.Tools.dialog({
                            controller:'stockUnitStrategyCtrl.dialog',
                            template : '../public/template/strategyManagement/stockUnitStrategy/strategy-location-form.html',
                            closeByDocument : false,
                            closeByEscape : false,
                            data : {
                                reservoirDropList : result.data,    //库区下拉
                                dropDownDefault : {
                                    reservoir : result.data[0]  //库区默认值
                                }
                            },
                            width : 550,
                            title : '新增库位策略',
                            scope : scope
                        })
                    }else{
                        scope.Tools.alert(result.message);
                    }
                })
        }

        //库位策略 条件列表
        scope.ployConditionList = function(pyCode){
            stockUnitStrategyService.getPloyConditionList(pyCode)
                .then(function(result){
                    if(result.success){
                        scope.strategyConditionList = result.data;
                    }else{
                        scope.Tools.alert(result.message);
                    }
                })
        }

        //库位策略 流向设置 列表
        scope.ployFlowtoList = function(pyCode){
            stockUnitStrategyService.getPloyFlowtoList(pyCode)
                .then(function(result){
                    if(result.success){
                        scope.strategyFlowtoList = result.data;
                    }else{
                        scope.Tools.alert(result.message);
                    }
                })
        }

        //库位策略 库位分配设置 列表
        scope.ployLocationList = function(pyCode){
            stockUnitStrategyService.getPloyLocationList(pyCode)
                .then(function(result){
                    if(result.success){
                        scope.strategyLocationList = result.data;
                    }else{
                        scope.Tools.alert(result.message);
                    }
                })
        }

        //选择一行策略 click
        scope.selectItemClick = function(item){
            //设置选择的 策略code
            scope.selectPyCode = item.pyCode;
            //库位策略 条件列表
            scope.ployConditionList(scope.selectPyCode);
            //库位策略 流向设置
            scope.ployFlowtoList(scope.selectPyCode);
            //库位策略 库位分配设置
            scope.ployLocationList(scope.selectPyCode);
        }
    }])
    .controller('stockUnitStrategyCtrl.dialog',['$scope','stockUnitStrategyService','dropDownService',function(scope,stockUnitStrategyService,dropDownService){   //库位策略 dialog

        //新增和更新策略
        scope.saveOrUpdateStrateyClick = function(){
            //判断是否有id
            if(scope.ngDialogData.stockUnitStrategy.pyId){  //更新

            }else{  //更新
                stockUnitStrategyService.addPloy(scope.ngDialogData.stockUnitStrategy)
                    .then(function(result){
                        if(result.success){
                            //刷新列表
                            scope.list(scope.listParam());
                            scope.closeThisDialog();
                        }else{
                            scope.Tools.alert(result.message);
                        }
                    })
            }
        }

        //新增条件click
        scope.saveConditionClick = function(){

            var param = {};
            param.pycPyCode = scope.selectPyCode;   //选择的策略code
            param.pycType = scope.ngDialogData.dropDownDefault.conditionType.value;   //条件类型
            param.pycOperatorCode = scope.ngDialogData.dropDownDefault.operator.value;  //条件运算符
            //货主
            if(scope.ngDialogData.dropDownDefault.conditionType.value == scope.constant.strategyConditionType.shipper){
                if(scope.ngDialogData.dropDownDefault.conditionValue.customerId){
                    param.pycValue = scope.ngDialogData.dropDownDefault.conditionValue.customerId.code;
                    param.pycName = scope.ngDialogData.dropDownDefault.conditionValue.customerId.name;
                }
            }else{  //车型
                param.pycValue = scope.ngDialogData.dropDownDefault.conditionValue.code;
                param.pycName = scope.ngDialogData.dropDownDefault.conditionValue.name;
            }
             stockUnitStrategyService.addPloyCondition(new Array(param))
                 .then(function(result){
                     if(result.success){
                         //刷新列表
                         scope.ployConditionList(scope.selectPyCode);
                         scope.closeThisDialog();
                     }else{
                         scope.Tools.alert(result.message);
                     }
                 })
        }

        //条件
        scope.pycTypeDropChang = function(item){
            if(item.value == scope.constant.strategyConditionType.shipper){ //货主
                dropDownService.getCustomerInfo()
                    .then(function(result){
                        scope.ngDialogData.conditionValueDropDown = result.data;    //条件值
                        scope.ngDialogData.dropDownDefault.conditionValue = result.data[0];//条件值
                    })
            }else{  //车型
                dropDownService.getVehicleSpec()
                    .then(function(result){
                        scope.ngDialogData.dropDownDefault.conditionValue = result.data[0]; //条件值
                        scope.ngDialogData.conditionValueDropDown = result.data;    //条件值
                    })
            }
        }

        //保存流向
        scope.saveFlowtoClick = function(){
            /*stockUnitStrategyService.addPloyFlowto(scope.selectPyCode)*/
        }
    }])
