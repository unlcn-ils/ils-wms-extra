package com.unlcn.ils.wms.backend.bo.baseDataBO;

/**
 * Created by DELL on 2017/8/14.
 */
public class EmptyLocationBO {
    /**
     * 所属仓库ID
     */
    private String locWhId;
    /**
     * 所属仓库CODE
     */
    private String locWhCode;
    /**
     * 所属库区ID
     */
    private String zoneId;
    /**
     * 所属库区CODE
     */
    private String zoneCode;
    /**
     * 所属库位ID
     */
    private String locId;
    /**
     * 所属库位CODE
     */
    private String locCode;
    /**
     * 库位所在排
     */
    private String locRow;
    /**
     * 库位所在列
     */
    private String locColumn;
    /**
     * 库位所属层
     */
    private String locLevel;
    /**
     * 库位长
     */
    private String locLength;
    /**
     * 库位宽
     */
    private String locWidth;

    public String getLocWhId() {
        return locWhId;
    }

    public void setLocWhId(String locWhId) {
        this.locWhId = locWhId;
    }

    public String getLocWhCode() {
        return locWhCode;
    }

    public void setLocWhCode(String locWhCode) {
        this.locWhCode = locWhCode;
    }

    public String getZoneId() {
        return zoneId;
    }

    public void setZoneId(String zoneId) {
        this.zoneId = zoneId;
    }

    public String getZoneCode() {
        return zoneCode;
    }

    public void setZoneCode(String zoneCode) {
        this.zoneCode = zoneCode;
    }

    public String getLocId() {
        return locId;
    }

    public void setLocId(String locId) {
        this.locId = locId;
    }

    public String getLocCode() {
        return locCode;
    }

    public void setLocCode(String locCode) {
        this.locCode = locCode;
    }

    public String getLocRow() {
        return locRow;
    }

    public void setLocRow(String locRow) {
        this.locRow = locRow;
    }

    public String getLocColumn() {
        return locColumn;
    }

    public void setLocColumn(String locColumn) {
        this.locColumn = locColumn;
    }

    public String getLocLevel() {
        return locLevel;
    }

    public void setLocLevel(String locLevel) {
        this.locLevel = locLevel;
    }

    public String getLocLength() {
        return locLength;
    }

    public void setLocLength(String locLength) {
        this.locLength = locLength;
    }

    public String getLocWidth() {
        return locWidth;
    }

    public void setLocWidth(String locWidth) {
        this.locWidth = locWidth;
    }

    @Override
    public String toString() {
        return "EmptyLocationBO{" +
                "locWhId='" + locWhId + '\'' +
                ", locWhCode='" + locWhCode + '\'' +
                ", zoneId='" + zoneId + '\'' +
                ", zoneCode='" + zoneCode + '\'' +
                ", locId='" + locId + '\'' +
                ", locCode='" + locCode + '\'' +
                ", locRow='" + locRow + '\'' +
                ", locColumn='" + locColumn + '\'' +
                ", locLevel='" + locLevel + '\'' +
                ", locLength='" + locLength + '\'' +
                ", locWidth='" + locWidth + '\'' +
                '}';
    }
}
