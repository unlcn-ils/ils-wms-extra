package com.unlcn.ils.wms.base.mapper.extmapper;

import com.unlcn.ils.wms.base.dto.WmsJMOutOfStorageOperationResultListDTO;
import com.unlcn.ils.wms.base.model.junmadcs.WmsOutOfStorage;
import com.unlcn.ils.wms.base.model.junmadcs.WmsOutOfStorageExcp;
import org.apache.ibatis.annotations.Param;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public interface WmsOutOfStorageExtMapper {
    /**
     * 根据参数查询
     *
     * @param paramsMap
     * @return
     */
    List<WmsOutOfStorage> findByParams(@Param("paramsMap") Map<String, Object> paramsMap);

    List<WmsJMOutOfStorageOperationResultListDTO> listOutOfStorageExcps(HashMap<String, Object> params);

    int countOutOfStorageExcps(HashMap<String, Object> params);

    List<WmsOutOfStorageExcp> selectExcpListByIds(List<String> ids);

    void updateSendDcsAgainBatch(@Param("param") HashMap<String, Object> param);

    void updateSendSapAgainBatch(@Param("param") HashMap<String, Object> params);

    List<WmsOutOfStorage> selectStorageListByIds(List<String> ids);

    void updateSendSapResultSuccess(@Param("param") HashMap<String, Object> params);

    void updateSendDcsResultToSuccess(@Param("param")HashMap<String, Object> params);
}