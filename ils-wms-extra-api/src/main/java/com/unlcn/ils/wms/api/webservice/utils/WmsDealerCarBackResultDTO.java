package com.unlcn.ils.wms.api.webservice.utils;

/**
 * Created by DELL on 2017/9/27.
 */
public class WmsDealerCarBackResultDTO {

    /**
     * vin
     */
    private String cbVin;

    /**
     * 错误信息
     */
    private String eMsg;

    public String getCbVin() {
        return cbVin;
    }

    public void setCbVin(String cbVin) {
        this.cbVin = cbVin;
    }

    public String geteMsg() {
        return eMsg;
    }

    public void seteMsg(String eMsg) {
        this.eMsg = eMsg;
    }

    @Override
    public String toString() {
        return "WmsDealerCarBackResultDTO{" +
                "cbVin='" + cbVin + '\'' +
                ", eMsg='" + eMsg + '\'' +
                '}';
    }
}
