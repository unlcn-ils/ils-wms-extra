package com.unlcn.ils.wms.base.businessDTO.baseData;

/**
 * created by linbao on 2017-10-09
 */
public class WmsWarehouseDTO {

    /**
     * 仓库ID
     */
    private Long whId;

    /**
     * 仓库代码
     */
    private String whCode;

    /**
     * 仓库名称
     */
    private String whName;

    /**
     * 仓库类型
     */
    private String whType;

    public Long getWhId() {
        return whId;
    }

    public void setWhId(Long whId) {
        this.whId = whId;
    }

    public String getWhCode() {
        return whCode;
    }

    public void setWhCode(String whCode) {
        this.whCode = whCode;
    }

    public String getWhName() {
        return whName;
    }

    public void setWhName(String whName) {
        this.whName = whName;
    }

    public String getWhType() {
        return whType;
    }

    public void setWhType(String whType) {
        this.whType = whType;
    }
}
