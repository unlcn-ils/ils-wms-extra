package com.unlcn.ils.wms.backend.enums;

/**
 * 维修后验车地点
 */
public enum WmsRepairInspectPlaceEnum {
    PREPAIR_PLACE(10, "进备料区验车"),
    INBOUND_PLACE(20, "入库区再验车");

    private final int value;
    private final String text;

    WmsRepairInspectPlaceEnum(int value, String text) {
        this.value = value;
        this.text = text;
    }

    public int getValue() {
        return value;
    }

    public String getText() {
        return text;
    }

    public static WmsRepairInspectPlaceEnum getByValue(int value) {
        for (WmsRepairInspectPlaceEnum temp : WmsRepairInspectPlaceEnum.values()) {
            if (temp.getValue() == value) {
                return temp;
            }
        }
        return null;
    }
}
