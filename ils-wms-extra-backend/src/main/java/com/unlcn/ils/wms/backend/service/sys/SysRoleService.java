package com.unlcn.ils.wms.backend.service.sys;

import com.unlcn.ils.wms.backend.bo.sys.PermissionsBO;
import com.unlcn.ils.wms.backend.bo.sys.SysRoleBO;
import com.unlcn.ils.wms.base.model.sys.SysRole;

import java.util.List;
import java.util.Map;

/**
 * @Auther linbao
 * @Date 2017-10-12
 */
public interface SysRoleService {

    /**
     * 分页列表查询
     *
     * @param sysRoleBO
     * @return
     * @throws Exception
     */
    Map<String, Object> listRole(SysRoleBO sysRoleBO) throws Exception;

    /**
     * 新增
     *
     * @param sysRoleBO
     * @throws Exception
     */
    void addSysRole(SysRoleBO sysRoleBO) throws Exception;

    /**
     * 修改
     *
     * @param sysRoleBO
     * @throws Exception
     */
    void updateSysRole(SysRoleBO sysRoleBO) throws Exception;

    /**
     * 删除
     *
     * @param sysRoleBO
     * @throws Exception
     */
    void deleteSysRole(SysRoleBO sysRoleBO) throws Exception;

    /**
     * 根据用户ID查询对应的角色
     * @return
     * @throws Exception
     */
    List<SysRole> listRoleByUserId(SysRoleBO sysRoleBO) throws Exception;

    /**
     * 校验策略代码是否唯一
     *
     * @param sysRoleBO
     * @return
     * @throws Exception
     */
    boolean queryCheckRoleCodeUniqueue(SysRoleBO sysRoleBO) throws Exception;

    /**
     * 根据角色获取权限
     *
     * @param sysRoleBO
     * @return
     * @throws Exception
     */
    List<PermissionsBO> listPermissByRoleId(SysRoleBO sysRoleBO) throws Exception;

    /**
     * 根据userId获取对应的角色
     *
     * @param userId
     * @return
     * @throws Exception
     */
    List<SysRole> getRoleListByUserId(Integer userId) throws Exception;
}
