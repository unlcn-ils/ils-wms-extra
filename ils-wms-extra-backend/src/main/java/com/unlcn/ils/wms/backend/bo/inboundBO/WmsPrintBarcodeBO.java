package com.unlcn.ils.wms.backend.bo.inboundBO;

import java.io.Serializable;

public class WmsPrintBarcodeBO implements Serializable {
    /**
     * 二维码
     */
    private String barcode;

    /**
     * 底盘号
     */
    private String vin;


    public String getBarcode() {
        return barcode;
    }

    public void setBarcode(String barcode) {
        this.barcode = barcode;
    }

    public String getVin() {
        return vin;
    }

    public void setVin(String vin) {
        this.vin = vin;
    }
}
