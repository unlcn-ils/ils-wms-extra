package com.unlcn.ils.wms.api.webservice.server;

import com.alibaba.fastjson.JSONObject;
import com.google.common.collect.Maps;
import com.unlcn.ils.wms.backend.service.inbound.Wms2MesInboundNoticeService;
import com.unlcn.ils.wms.base.dto.Wms2MesInboundServiceParamDTO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.context.support.SpringBeanAutowiringSupport;

import javax.jws.WebMethod;
import javax.jws.WebService;
import java.util.HashMap;

/**
 * wms 提供服务端,接收襄阳MES系统入库数据,进行入库
 */
@WebService
public class Wms2MesInboundNoticeInterface {

	private Logger logger = LoggerFactory.getLogger(getClass());

	@Autowired
	private Wms2MesInboundNoticeService inboundNoticeService;

	@WebMethod
	public String updateInboundDataFromMes(Wms2MesInboundServiceParamDTO[] paramDTOS) {
		if (logger.isInfoEnabled()) {
			logger.info("webService--server--Wms2MesInboundNoticeInterface.updateInboundDataFromMes params:",
					JSONObject.toJSONString(paramDTOS));
		}
		// 加入spring的上下文
		SpringBeanAutowiringSupport.processInjectionBasedOnCurrentContext(this);
		HashMap<String, Object> result = Maps.newHashMap();
		try {
			result = inboundNoticeService.updateInboundDataFromMes(paramDTOS);
		} catch (Exception e) {
			logger.error("webService--server--Wms2MesInboundNoticeInterface.updateInboundDataFromMes error:", e);
			result.put("success", "E");
			result.put("message", "系统异常!");
		}
		return JSONObject.toJSONString(result);
	}

}
