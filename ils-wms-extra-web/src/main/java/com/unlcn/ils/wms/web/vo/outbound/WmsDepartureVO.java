package com.unlcn.ils.wms.web.vo.outbound;

import cn.huiyunche.commons.domain.PageVo;

import java.io.Serializable;

/**
 * @Auther linbao
 * @Date 2017-11-14
 */
public class WmsDepartureVO extends PageVo implements Serializable {

    /**
     * 主键
     */
    private Long drId;

    /**
     * 车牌
     */
    private String drVehiclePlate;

    /**
     * 开闸状态
     */
    private String drOpenStatus;

    /**
     * 离场类型
     */
    private String drDepartureType;

    /**
     * 离场开始时间
     */
    private String startDeparturTime;

    /**
     * 离场结束时间
     */
    private String endDepartureTime;

    /**
     * 离场原因
     */
    private String drDepartureReason;

    private String userId;

    private String whCode;

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getWhCode() {
        return whCode;
    }

    public void setWhCode(String whCode) {
        this.whCode = whCode;
    }

    public Long getDrId() {
        return drId;
    }

    public void setDrId(Long drId) {
        this.drId = drId;
    }

    public String getDrDepartureReason() {
        return drDepartureReason;
    }

    public void setDrDepartureReason(String drDepartureReason) {
        this.drDepartureReason = drDepartureReason;
    }

    public String getDrVehiclePlate() {
        return drVehiclePlate;
    }

    public void setDrVehiclePlate(String drVehiclePlate) {
        this.drVehiclePlate = drVehiclePlate;
    }

    public String getDrOpenStatus() {
        return drOpenStatus;
    }

    public void setDrOpenStatus(String drOpenStatus) {
        this.drOpenStatus = drOpenStatus;
    }

    public String getDrDepartureType() {
        return drDepartureType;
    }

    public void setDrDepartureType(String drDepartureType) {
        this.drDepartureType = drDepartureType;
    }

    public String getStartDeparturTime() {
        return startDeparturTime;
    }

    public void setStartDeparturTime(String startDeparturTime) {
        this.startDeparturTime = startDeparturTime;
    }

    public String getEndDepartureTime() {
        return endDepartureTime;
    }

    public void setEndDepartureTime(String endDepartureTime) {
        this.endDepartureTime = endDepartureTime;
    }

    @Override
    public String toString() {
        return "WmsDepartureVO{" +
                "drId=" + drId +
                ", drVehiclePlate='" + drVehiclePlate + '\'' +
                ", drOpenStatus=" + drOpenStatus +
                ", drDepartureType=" + drDepartureType +
                ", startDeparturTime='" + startDeparturTime + '\'' +
                ", endDepartureTime='" + endDepartureTime + '\'' +
                ", drDepartureReason='" + drDepartureReason + '\'' +
                '}';
    }
}
