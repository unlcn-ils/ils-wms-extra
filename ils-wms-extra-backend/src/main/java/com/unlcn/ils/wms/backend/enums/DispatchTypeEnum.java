package com.unlcn.ils.wms.backend.enums;

/**
 * @Auther linbao
 * @Date 2017-10-23
 * 发运类型
 */
public enum DispatchTypeEnum {

    A1("A1", "正常发运"),
    A2("A2", "调拨发运");

    private final String code;
    private final String value;

    public String getCode() {
        return code;
    }

    public String getValue() {
        return value;
    }

    DispatchTypeEnum(String code, String value) {
        this.code = code;
        this.value = value;
    }
}
