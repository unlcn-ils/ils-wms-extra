package com.unlcn.ils.wms.app.controller.tms;


import cn.huiyunche.commons.domain.PageVo;
import cn.huiyunche.commons.domain.ResultDTO;
import cn.huiyunche.commons.exception.BusinessException;
import com.unlcn.ils.wms.app.vo.tms.WmsInboundConfirmVO;
import com.unlcn.ils.wms.backend.bo.inspectAppBO.TmsInboundMoveCarBO;
import com.unlcn.ils.wms.backend.bo.inspectAppBO.WmsInboundConfirmBO;
import com.unlcn.ils.wms.backend.service.inspectApp.TmsInboundService;
import com.unlcn.ils.wms.base.dto.WmsInboundNotMoveCarDTO;
import com.unlcn.ils.wms.base.dto.WmsWaybillInfoParamDTO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;
import java.util.Map;

/**
 * 此包下接口都是为app提供
 **/
@Controller
@RequestMapping("/warehousing")
public class WmsInboundController {
    private Logger logger = LoggerFactory.getLogger(WmsInboundController.class);

    @Autowired
    private TmsInboundService tmsInboundService;

    /**
     * 扫码查询运单
     * <p>
     * 2018-1-30 更新增加新需求:重庆库车架号模糊匹配
     * 说明:因君马库所有车辆都有二维码,故不需要这个模糊匹配
     * </p>
     *
     * @param dto 参数封装
     * @return 返回值
     */

    @RequestMapping(value = "/getWaybillInfo", method = RequestMethod.POST)
    @ResponseBody
    public ResultDTO<Object> getWaybillInfo(WmsWaybillInfoParamDTO dto) {
        logger.info("TmsInboundController.getWaybillInfo param: {}.", dto);
        ResultDTO<Object> result = new ResultDTO<>(true, null, "查询运单数据成功");
        try {
            Map<String, Object> resultData = tmsInboundService.getInboundInspectInfo(dto);
            result.setData(resultData);
        } catch (BusinessException e) {
            logger.error("TmsInboundController.getWaybillInfo error: {}", e);
            result.setMessage(e.getMessage());
            result.setSuccess(false);
            result.setData(null);
            //throw new BusinessException(e.getMessage());
        } catch (Exception e) {
            logger.error("TmsInboundController.getWaybillInfo error: {}", e);
            result.setMessage("查询异常");
            result.setSuccess(false);
            result.setData(null);
            //throw new BusinessException("查询异常");
        }
        return result;
    }

    /**
     * 查询未移车的列表--君马库功能
     * <p>
     * 2018-1-30 新增重庆库未移车列表
     * </p>
     *
     * @param whcode 仓库code
     * @param vo     参数封装
     * @return 返回值
     */
    @RequestMapping(value = "/getNotMoveCarList", method = RequestMethod.POST)
    @ResponseBody
    public ResultDTO<Object> getNotMoveCarList(String whcode, PageVo vo) {
        logger.info("TmsInboundController.getNotMoveCarList param: {},{}.", whcode, vo);
        ResultDTO<Object> result = new ResultDTO<>(true, null, "查询未移车列表成功");
        try {
            List<WmsInboundNotMoveCarDTO> notMoveCarDTOS = tmsInboundService.getNotMoveCarList(whcode, vo);
            result.setData(notMoveCarDTOS);
        } catch (BusinessException e) {
            logger.error("TmsInboundController.getNotMoveCarList error {}", e);
            result.setMessage(e.getMessage());
            result.setSuccess(false);
            result.setData(null);
        } catch (Exception e) {
            logger.error("TmsInboundController.getNotMoveCarList error {}", e);
            result.setMessage("查询异常");
            result.setSuccess(false);
            result.setData(null);
        }
        return result;
    }


    //移车申请
    @RequestMapping(value = "/moveCar", method = RequestMethod.GET)
    @ResponseBody
    public ResultDTO<Object> moveCar(String whCode, Long inspectId, String userId) {
        logger.info("TmsInboundController.moveCar param vin: {},{},{}.", whCode, inspectId, userId);
        ResultDTO<Object> result = new ResultDTO<>(true, null, "移库申请成功");
        try {
            result.setData(tmsInboundService.updateMoveCar(whCode, inspectId, userId));
        } catch (BusinessException e) {
            logger.error("TmsInboundController.moveCar error: {},{}.", e);
            result.setMessage(e.getMessage());
            result.setSuccess(false);
            result.setData(null);
        } catch (Exception e) {
            logger.error("TmsInboundController.moveCar error: {},{}.", e);
            result.setMessage("操作异常");
            result.setSuccess(false);
            result.setData(null);
        }
        return result;
    }

    /**
     * bugfix 2018-2-27 调整库存的批次号以入库的时间为依据,同一天入库的为一批;
     * app发起入库确认(君马库 入库业务)
     *
     * @param vo 参数封装
     * @return 返回值
     */
    @RequestMapping(value = "/warehousingConfirm", method = RequestMethod.POST)
    @ResponseBody
    public ResultDTO<Object> warehousingConfirm(WmsInboundConfirmVO vo) {
        logger.info("TmsInboundController.warehousingConfirm param vo {}.", vo);
        ResultDTO<Object> result = new ResultDTO<>(true, null, "入库确认成功");
        try {
            WmsInboundConfirmBO bo = new WmsInboundConfirmBO();
            BeanUtils.copyProperties(vo, bo);
            TmsInboundMoveCarBO moveCarBO = tmsInboundService.saveWareHousingConfirm(bo);
            result.setData(moveCarBO);
        } catch (BusinessException e) {
            logger.error("TmsInboundController.inboundConfirm error: {},{}.", e);
            result.setMessage(e.getMessage());
            result.setSuccess(false);
            result.setData(null);
        } catch (Exception e) {
            logger.error("TmsInboundController.inboundConfirm error: {},{}.", e);
            result.setMessage("系统异常");
            result.setSuccess(false);
            result.setData(null);
        }
        return result;
    }
}
