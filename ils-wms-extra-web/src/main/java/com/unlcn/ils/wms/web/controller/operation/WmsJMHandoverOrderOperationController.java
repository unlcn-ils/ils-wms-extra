package com.unlcn.ils.wms.web.controller.operation;

import cn.huiyunche.commons.domain.ResultDTO;
import cn.huiyunche.commons.domain.ResultDTOWithPagination;
import cn.huiyunche.commons.exception.BusinessException;
import com.unlcn.ils.wms.backend.service.operation.WmsJmHandoverOrderOperationService;
import com.unlcn.ils.wms.base.dto.WmsHandoverOrderOperationListResultDTO;
import com.unlcn.ils.wms.base.dto.WmsJmHandoverOrderOperationParamDTO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import java.util.List;


/**
 * <p>
 * 交接单接口数据维护的表现层
 * </p>
 */
@Controller
@ResponseBody
@RequestMapping("/handoverOrderOperation")
public class WmsJMHandoverOrderOperationController {
    private Logger logger = LoggerFactory.getLogger(getClass());

    private WmsJmHandoverOrderOperationService operationService;

    @Autowired
    public void setOperationService(WmsJmHandoverOrderOperationService operationService) {
        this.operationService = operationService;
    }

    /**
     * 获取出入库的失败数据列表
     *
     * @param paramDTO 参数封装
     */
    @RequestMapping(value = "/queryList", method = RequestMethod.POST)
    public ResultDTOWithPagination<Object> queryExcpList(@RequestBody WmsJmHandoverOrderOperationParamDTO paramDTO, HttpServletRequest request) {
        logger.info("WmsJMHandoverOrderOperationController.queryExcpList params:{}", paramDTO);
        ResultDTOWithPagination<Object> result = new ResultDTOWithPagination<>(true, null, "查询成功!");
        try {
            String whCode = request.getHeader("whCode");
            String userId = request.getHeader("userId");
            paramDTO.setUserId(userId);
            paramDTO.setWhCode(whCode);
            ResultDTOWithPagination<List<WmsHandoverOrderOperationListResultDTO>> data = operationService.listHandoverOrderExcp(paramDTO);
            result.setData(data.getData());
            result.setPageVo(data.getPageVo());
        } catch (BusinessException e) {
            logger.error("WmsJMHandoverOrderOperationController.queryExcpList BusinessException:", e);
            result.setMessage(e.getMessage());
        } catch (Exception e) {
            logger.error("WmsJMHandoverOrderOperationController.queryExcpList error:", e);
            result.setMessage("查询异常");
        }
        return result;
    }


    /**
     * 实现重发到DCS系统功能(dcs如果失败以整个组板单为单位发送数据)
     *
     * @return 返回值
     */
    @RequestMapping("/updateSendToDCSAgain")
    public ResultDTO<Object> updateSendToDCSAgain(@RequestBody WmsJmHandoverOrderOperationParamDTO dto, HttpServletRequest request) {
        logger.info("WmsJMHandoverOrderOperationController.updateSendToDCSAgain params:{}", dto);
        ResultDTO<Object> result = new ResultDTO<>(true, null, "操作成功!");
        try {
            String whCode = request.getHeader("whCode");
            String userId = request.getHeader("userId");
            dto.setUserId(userId);
            dto.setWhCode(whCode);
            operationService.updateSendToDCSAgain(dto);
        } catch (BusinessException e) {
            logger.error("WmsJMHandoverOrderOperationController.updateSendToDCSAgain BusinessException:", e);
            result.setMessage(e.getMessage());
        } catch (Exception e) {
            logger.error("WmsJMHandoverOrderOperationController.updateSendToDCSAgain error:", e);
            result.setMessage("操作异常");
        }
        return result;
    }


    /**
     * 实现重发到SAP系统功能(SAP如果失败支持部分发送数据)
     *
     * @return 返回值
     */
    @RequestMapping("/updateSendToSAPAgain")
    public ResultDTO<Object> updateSendToSAPAgain(@RequestBody WmsJmHandoverOrderOperationParamDTO dto, HttpServletRequest request) {
        logger.info("WmsJMHandoverOrderOperationController.updateSendToSAPAgain params:{}", dto);
        ResultDTO<Object> result = new ResultDTO<>(true, null, "操作成功!");
        try {
            String whCode = request.getHeader("whCode");
            String userId = request.getHeader("userId");
            dto.setUserId(userId);
            dto.setWhCode(whCode);
            operationService.updateSendToSAPAgain(dto);
        } catch (BusinessException e) {
            logger.error("WmsJMHandoverOrderOperationController.updateSendToSAPAgain BusinessException:", e);
            result.setMessage(e.getMessage());
        } catch (Exception e) {
            logger.error("WmsJMHandoverOrderOperationController.updateSendToSAPAgain error:", e);
            result.setMessage("操作异常");
        }
        return result;
    }

    /**
     * 实现重发到CRM系统功能(CRM如果失败只支持单条发送数据)
     *
     * @return 返回值
     */
    @RequestMapping("/updateSendToCRMAgain")
    public ResultDTO<Object> updateSendToCRMAgain(@RequestBody WmsJmHandoverOrderOperationParamDTO dto, HttpServletRequest request) {
        logger.info("WmsJMHandoverOrderOperationController.updateSendToCRMAgain params:{}", dto);
        ResultDTO<Object> result = new ResultDTO<>(true, null, "操作成功!");
        try {
            String whCode = request.getHeader("whCode");
            String userId = request.getHeader("userId");
            dto.setUserId(userId);
            dto.setWhCode(whCode);
            operationService.updateSendToCRMAgain(dto);
        } catch (BusinessException e) {
            logger.error("WmsJMHandoverOrderOperationController.updateSendToCRMAgain BusinessException:", e);
            result.setMessage(e.getMessage());
        } catch (Exception e) {
            logger.error("WmsJMHandoverOrderOperationController.updateSendToCRMAgain error:", e);
            result.setMessage("操作异常");
        }
        return result;
    }

    /**
     * 实现DCS的手动成功
     *
     * @return 返回值
     */
    @RequestMapping("/updateSendDCSToSuccess")
    public ResultDTO<Object> updateSendDCSToSuccess(@RequestBody WmsJmHandoverOrderOperationParamDTO dto, HttpServletRequest request) {
        logger.info("WmsJMHandoverOrderOperationController.updateSendDCSToSuccess params:{}", dto);
        ResultDTO<Object> result = new ResultDTO<>(true, null, "操作成功!");
        try {
            String whCode = request.getHeader("whCode");
            String userId = request.getHeader("userId");
            dto.setUserId(userId);
            dto.setWhCode(whCode);
            operationService.updateSendDCSToSuccess(dto);
        } catch (BusinessException e) {
            logger.error("WmsJMHandoverOrderOperationController.updateSendDCSToSuccess BusinessException:", e);
            result.setMessage(e.getMessage());
        } catch (Exception e) {
            logger.error("WmsJMHandoverOrderOperationController.updateSendDCSToSuccess error:", e);
            result.setMessage("操作异常");
        }
        return result;
    }


    /**
     * 实现SAP的手动成功
     *
     * @return 返回值
     */
    @RequestMapping("/updateSendSAPToSuccess")
    public ResultDTO<Object> updateSendSAPToSuccess(@RequestBody WmsJmHandoverOrderOperationParamDTO dto, HttpServletRequest request) {
        logger.info("WmsJMHandoverOrderOperationController.updateSendSAPToSuccess params:{}", dto);
        ResultDTO<Object> result = new ResultDTO<>(true, null, "操作成功!");
        try {
            String whCode = request.getHeader("whCode");
            String userId = request.getHeader("userId");
            dto.setUserId(userId);
            dto.setWhCode(whCode);
            operationService.updateSendSAPToSuccess(dto);
        } catch (BusinessException e) {
            logger.error("WmsJMHandoverOrderOperationController.updateSendSAPToSuccess BusinessException:", e);
            result.setMessage(e.getMessage());
        } catch (Exception e) {
            logger.error("WmsJMHandoverOrderOperationController.updateSendSAPToSuccess error:", e);
            result.setMessage("操作异常");
        }
        return result;
    }


    /**
     * 实现CRM系统手动成功
     *
     * @return 返回值
     */
    @RequestMapping("/updateSendCRMToSuccess")
    public ResultDTO<Object> updateSendCRMToSuccess(@RequestBody WmsJmHandoverOrderOperationParamDTO dto, HttpServletRequest request) {
        logger.info("WmsJMHandoverOrderOperationController.updateSendCRMToSuccess params:{}", dto);
        ResultDTO<Object> result = new ResultDTO<>(true, null, "操作成功!");
        try {
            String whCode = request.getHeader("whCode");
            String userId = request.getHeader("userId");
            dto.setUserId(userId);
            dto.setWhCode(whCode);
            operationService.updateSendCRMToSuccess(dto);
        } catch (BusinessException e) {
            logger.error("WmsJMHandoverOrderOperationController.updateSendCRMToSuccess BusinessException:", e);
            result.setMessage(e.getMessage());
        } catch (Exception e) {
            logger.error("WmsJMHandoverOrderOperationController.updateSendCRMToSuccess error:", e);
            result.setMessage("操作异常");
        }
        return result;
    }
}
