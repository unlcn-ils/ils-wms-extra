package com.unlcn.ils.wms.base.businessDTO.stock;

import cn.huiyunche.commons.domain.PageVo;

/**
 * Created by DELL on 2017/8/21.
 */
public class WmsInventoryQueryDTO extends PageVo {

    /**
     * 君马-条件“物料名称”、“颜色”、“库存状态”
     */
    private String materialName;
    private String materialCode;
    private String colourCode;
    private String colourName;
    private String invStatus;

    private Long invId;

    private String invWhCode;

    private String invWhName;

    private String invCustomerCode;

    private String invCustomerName;

    private String invVehicleBrandCode;

    private String invVehicleBrandName;

    private String invVehicleSeriesCode;

    private String invVehicleSeriesName;

    private String invVehicleSpecCode;

    private String invVehicleSpecName;

    private String invVehicleSpecDesc;

    private String invlocVin;

    private Long invNum;

    private int limitStart = 0;

    private int limitEnd = 10;

    private String inventoryAge;

    public String getInventoryAge() {
        return inventoryAge;
    }

    public void setInventoryAge(String inventoryAge) {
        this.inventoryAge = inventoryAge;
    }

    public String getMaterialName() {
        return materialName;
    }

    public void setMaterialName(String materialName) {
        this.materialName = materialName;
    }

    public String getMaterialCode() {
        return materialCode;
    }

    public void setMaterialCode(String materialCode) {
        this.materialCode = materialCode;
    }

    public String getColourCode() {
        return colourCode;
    }

    public void setColourCode(String colourCode) {
        this.colourCode = colourCode;
    }

    public String getColourName() {
        return colourName;
    }

    public void setColourName(String colourName) {
        this.colourName = colourName;
    }

    public String getInvStatus() {
        return invStatus;
    }

    public void setInvStatus(String invStatus) {
        this.invStatus = invStatus;
    }

    public Long getInvId() {
        return invId;
    }

    public void setInvId(Long invId) {
        this.invId = invId;
    }

    public String getInvWhCode() {
        return invWhCode;
    }

    public void setInvWhCode(String invWhCode) {
        this.invWhCode = invWhCode;
    }

    public String getInvWhName() {
        return invWhName;
    }

    public void setInvWhName(String invWhName) {
        this.invWhName = invWhName;
    }

    public String getInvCustomerCode() {
        return invCustomerCode;
    }

    public void setInvCustomerCode(String invCustomerCode) {
        this.invCustomerCode = invCustomerCode;
    }

    public String getInvCustomerName() {
        return invCustomerName;
    }

    public void setInvCustomerName(String invCustomerName) {
        this.invCustomerName = invCustomerName;
    }

    public String getInvVehicleBrandCode() {
        return invVehicleBrandCode;
    }

    public void setInvVehicleBrandCode(String invVehicleBrandCode) {
        this.invVehicleBrandCode = invVehicleBrandCode;
    }

    public String getInvVehicleBrandName() {
        return invVehicleBrandName;
    }

    public void setInvVehicleBrandName(String invVehicleBrandName) {
        this.invVehicleBrandName = invVehicleBrandName;
    }

    public String getInvVehicleSeriesCode() {
        return invVehicleSeriesCode;
    }

    public void setInvVehicleSeriesCode(String invVehicleSeriesCode) {
        this.invVehicleSeriesCode = invVehicleSeriesCode;
    }

    public String getInvVehicleSeriesName() {
        return invVehicleSeriesName;
    }

    public void setInvVehicleSeriesName(String invVehicleSeriesName) {
        this.invVehicleSeriesName = invVehicleSeriesName;
    }

    public String getInvVehicleSpecCode() {
        return invVehicleSpecCode;
    }

    public void setInvVehicleSpecCode(String invVehicleSpecCode) {
        this.invVehicleSpecCode = invVehicleSpecCode;
    }

    public String getInvVehicleSpecName() {
        return invVehicleSpecName;
    }

    public void setInvVehicleSpecName(String invVehicleSpecName) {
        this.invVehicleSpecName = invVehicleSpecName;
    }

    public String getInvVehicleSpecDesc() {
        return invVehicleSpecDesc;
    }

    public void setInvVehicleSpecDesc(String invVehicleSpecDesc) {
        this.invVehicleSpecDesc = invVehicleSpecDesc;
    }

    public Long getInvNum() {
        return invNum;
    }

    public void setInvNum(Long invNum) {
        this.invNum = invNum;
    }

    public int getLimitStart() {
        return limitStart;
    }

    public void setLimitStart(int limitStart) {
        this.limitStart = limitStart;
    }

    public int getLimitEnd() {
        return limitEnd;
    }

    public void setLimitEnd(int limitEnd) {
        this.limitEnd = limitEnd;
    }

    public String getInvlocVin() {
        return invlocVin;
    }

    public void setInvlocVin(String invlocVin) {
        this.invlocVin = invlocVin;
    }

    @Override
    public String toString() {
        return "WmsInventoryQueryDTO{" +
                "invId=" + invId +
                ", invWhCode='" + invWhCode + '\'' +
                ", invWhName='" + invWhName + '\'' +
                ", invCustomerCode='" + invCustomerCode + '\'' +
                ", invCustomerName='" + invCustomerName + '\'' +
                ", invVehicleBrandCode='" + invVehicleBrandCode + '\'' +
                ", invVehicleBrandName='" + invVehicleBrandName + '\'' +
                ", invVehicleSeriesCode='" + invVehicleSeriesCode + '\'' +
                ", invVehicleSeriesName='" + invVehicleSeriesName + '\'' +
                ", invVehicleSpecCode='" + invVehicleSpecCode + '\'' +
                ", invVehicleSpecName='" + invVehicleSpecName + '\'' +
                ", invVehicleSpecDesc='" + invVehicleSpecDesc + '\'' +
                ", invNum=" + invNum +
                ", limitStart=" + limitStart +
                ", limitEnd=" + limitEnd +
                '}';
    }
}
