package com.unlcn.ils.wms.backend.service.bigscreen;

import com.unlcn.ils.wms.backend.dto.bigscreenDTO.PickDataDTO;

import java.util.List;

/**
 * Created by lenovo on 2017/11/8.
 */
public interface PickDataService {

    /**
     * 提货推移图统计
     * @return
     */
    List<PickDataDTO> pickDataCount();

    void addPickData();
}
