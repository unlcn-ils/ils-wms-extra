package com.unlcn.ils.wms.backend.enums;

/**
 * Created by DELL on 2017/9/13.
 */
public enum WmsInboundStatusEnum {
    WMS_INBOUND_CREATE(10,"未收货"),
    WMS_INBOUND_WAIT_INBOUND(20,"已收货待入库"),
    WMS_INBOUND_FINISH(30,"已入库"),
    WMS_INBOUND_CANCLE(40,"取消入库");

    private final int value;
    private final String text;
    WmsInboundStatusEnum(int value, String text){
        this.value = value;
        this.text = text;
    }

    public int getValue() {
        return value;
    }

    public String getText() {
        return text;
    }

    public static WmsInboundStatusEnum getByValue(int value){
        for (WmsInboundStatusEnum temp : WmsInboundStatusEnum.values()) {
            if(temp.getValue() == value){
                return temp;
            }
        }
        return null;
    }
}
