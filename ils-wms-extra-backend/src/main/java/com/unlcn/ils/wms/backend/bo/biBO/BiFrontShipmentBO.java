package com.unlcn.ils.wms.backend.bo.biBO;

import java.io.Serializable;

/**
 * Created by Nicky on 17/11/9.
 */
public class BiFrontShipmentBO implements Serializable {

    /**
     * 统计日期
     */
    private String rpt_date;

    /**
     * 入库数
     */
    private String in_qty;

    /**
     *发运数
     */
    private String out_qty;

    /**
     *库存数
     */
    private String oncache_qty;

    /**
     *及时发运率
     */
    private String intime_out_qty;

    /**
     * 发运及时率
     */
    private String intime_radio;

    public String getRpt_date() {
        return rpt_date;
    }

    public void setRpt_date(String rpt_date) {
        this.rpt_date = rpt_date;
    }

    public String getIn_qty() {
        return in_qty;
    }

    public void setIn_qty(String in_qty) {
        this.in_qty = in_qty;
    }

    public String getOut_qty() {
        return out_qty;
    }

    public void setOut_qty(String out_qty) {
        this.out_qty = out_qty;
    }

    public String getOncache_qty() {
        return oncache_qty;
    }

    public void setOncache_qty(String oncache_qty) {
        this.oncache_qty = oncache_qty;
    }

    public String getIntime_out_qty() {
        return intime_out_qty;
    }

    public void setIntime_out_qty(String intime_out_qty) {
        this.intime_out_qty = intime_out_qty;
    }

    public String getIntime_radio() {
        return intime_radio;
    }

    public void setIntime_radio(String intime_radio) {
        this.intime_radio = intime_radio;
    }

    @Override
    public String toString() {
        return "BiFrontShipmentBO{" +
                "rpt_date='" + rpt_date + '\'' +
                ", in_qty='" + in_qty + '\'' +
                ", out_qty='" + out_qty + '\'' +
                ", oncache_qty='" + oncache_qty + '\'' +
                ", intime_out_qty='" + intime_out_qty + '\'' +
                ", intime_radio='" + intime_radio + '\'' +
                '}';
    }
}
