package com.unlcn.ils.wms.backend.bo.inspectAppBO;

public class WmsOutboundDamageDispatchBO {
    private Long inspectId;
    private String attachs;
    private String userId;

    public Long getInspectId() {
        return inspectId;
    }

    public void setInspectId(Long inspectId) {
        this.inspectId = inspectId;
    }

    public String getAttachs() {
        return attachs;
    }

    public void setAttachs(String attachs) {
        this.attachs = attachs;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

}
