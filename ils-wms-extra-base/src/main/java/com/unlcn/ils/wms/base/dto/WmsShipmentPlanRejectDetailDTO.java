package com.unlcn.ils.wms.base.dto;

import java.util.Date;

/**
 * @Auther linbao
 * @Date 2017-10-27
 */
public class WmsShipmentPlanRejectDetailDTO {

    /**
     * 任务表ID
     */
    private Long taskId;

    /**
     * 备料计划id
     */
    private Long planId;

    /**
     * 备料详情id
     */
    private Long planDetailId;



    /**
     * 车架号
     */
    private String vin;

    /**
     * 车型
     */
    private String vehicleName;

    /**
     * 状态
     */
    private String status;

    /**
     * 检验结果
     */
    private String checkResult;

    /**
     * 检验描述
     */
    private String checkDest;

    /**
     * 是否需要换车
     */
    private String needChange;

    /**
     * 换车车架号
     */
    private String changeVin;

    /**
     * 取消原因
     */
    private String cancleReason;

    /**
     * 备料任务号
     */
    private String taskNo;

    /**
     * 备料单号
     */
    private String materOrderNo;

    /**
     * 交接单号
     */
    private String handoverNo;

    /**
     * 创建时间
     */
    private Date createDate;

    public String getVin() {
        return vin;
    }

    public void setVin(String vin) {
        this.vin = vin;
    }

    public String getVehicleName() {
        return vehicleName;
    }

    public void setVehicleName(String vehicleName) {
        this.vehicleName = vehicleName;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getCheckResult() {
        return checkResult;
    }

    public void setCheckResult(String checkResult) {
        this.checkResult = checkResult;
    }

    public String getCheckDest() {
        return checkDest;
    }

    public void setCheckDest(String checkDest) {
        this.checkDest = checkDest;
    }

    public String getNeedChange() {
        return needChange;
    }

    public void setNeedChange(String needChange) {
        this.needChange = needChange;
    }

    public String getChangeVin() {
        return changeVin;
    }

    public void setChangeVin(String changeVin) {
        this.changeVin = changeVin;
    }

    public String getCancleReason() {
        return cancleReason;
    }

    public void setCancleReason(String cancleReason) {
        this.cancleReason = cancleReason;
    }

    public String getTaskNo() {
        return taskNo;
    }

    public void setTaskNo(String taskNo) {
        this.taskNo = taskNo;
    }

    public String getMaterOrderNo() {
        return materOrderNo;
    }

    public void setMaterOrderNo(String materOrderNo) {
        this.materOrderNo = materOrderNo;
    }

    public String getHandoverNo() {
        return handoverNo;
    }

    public void setHandoverNo(String handoverNo) {
        this.handoverNo = handoverNo;
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    public Long getTaskId() {
        return taskId;
    }

    public void setTaskId(Long taskId) {
        this.taskId = taskId;
    }

    public Long getPlanId() {
        return planId;
    }

    public void setPlanId(Long planId) {
        this.planId = planId;
    }

    public Long getPlanDetailId() {
        return planDetailId;
    }

    public void setPlanDetailId(Long planDetailId) {
        this.planDetailId = planDetailId;
    }
}
