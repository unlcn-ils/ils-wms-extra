package com.unlcn.ils.wms.backend.bo.inspectAppBO;

/**
 * @Author ：Ligl
 * @Date : 2017/9/12.
 */
public class TmsInspectExcpAttachBO {

    private Long id;

    private Long excpDetailsId;

    private String picKey;

    private Boolean delFlag;

    private String picUrl;

    public String getPicUrl() {
        return picUrl;
    }

    public void setPicUrl(String picUrl) {
        this.picUrl = picUrl;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getExcpDetailsId() {
        return excpDetailsId;
    }

    public void setExcpDetailsId(Long excpDetailsId) {
        this.excpDetailsId = excpDetailsId;
    }

    public String getPicKey() {
        return picKey;
    }

    public void setPicKey(String picKey) {
        this.picKey = picKey;
    }

    public Boolean getDelFlag() {
        return delFlag;
    }

    public void setDelFlag(Boolean delFlag) {
        this.delFlag = delFlag;
    }

    @Override
    public String
    toString() {
        return "TmsInspectExcpAttachBO{" +
                "id=" + id +
                ", excpDetailsId=" + excpDetailsId +
                ", picKey='" + picKey + '\'' +
                ", delFlag=" + delFlag +
                '}';
    }
}
