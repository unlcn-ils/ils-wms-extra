package com.unlcn.ils.wms.web.utils;

import com.alibaba.fastjson.JSONException;
import com.unlcn.ils.wms.web.controller.baseData.WmsBaseDataController;
import org.apache.http.HttpEntity;
import org.apache.http.ParseException;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;

/**
 * Created by DELL on 2017/9/6.
 */
public class HttpClientUtils {

    private static Logger logger = LoggerFactory.getLogger(WmsBaseDataController.class);

    /**
     * http Post请求
     */
    private static String postStr(String url, String jsonInput) throws ParseException, JSONException {
        // 创建默认的httpClient实例.
        CloseableHttpClient httpClient = HttpClients.createDefault();

        // 创建httppost
        HttpPost httpPost = new HttpPost(url);
        String jsonOutput = null;
        try {
            StringEntity postingString = new StringEntity(jsonInput, "UTF-8");// json传递
            httpPost.setEntity(postingString);

            logger.info("executing request " + httpPost.getURI());
            logger.info("json data:" + jsonInput);

            try (CloseableHttpResponse response = httpClient.execute(httpPost)) {
                HttpEntity entity = response.getEntity();
                if (entity != null) {
                    jsonOutput = EntityUtils.toString(entity, "UTF-8");
                    logger.info("Response content: " + jsonOutput);
                }
            }
        } catch (IOException e) {
            logger.error("运行HTTP 失败", e);
            e.printStackTrace();
        } finally {
            // 关闭连接,释放资源
            try {
                httpClient.close();
            } catch (IOException e) {
                logger.error("运行HTTP 失败", e);
                e.printStackTrace();
            }
        }
        return jsonOutput;
    }

    /**
     * http get请求
     */
    public static String getJson(String url) throws ParseException, JSONException {
        // 创建默认的httpClient实例.
        CloseableHttpClient httpClient = HttpClients.createDefault();

        // 创建httppost
        HttpGet httpPost = new HttpGet(url);
        String jsonOutput = null;
        try {
            logger.info("GET executing request " + httpPost.getURI());
            logger.info("json URL:" + url);
            try (CloseableHttpResponse response = httpClient.execute(httpPost)) {
                HttpEntity entity = response.getEntity();
                if (entity != null) {
                    jsonOutput = EntityUtils.toString(entity, "UTF-8");
                    logger.info("Response content: " + jsonOutput);
                }
            }
        } catch (IOException e) {
            logger.error("HTTP请求失败", e);
            e.printStackTrace();
        } finally {
            // 关闭连接,释放资源
            try {
                httpClient.close();
            } catch (IOException e) {
                logger.error("推送WMS 失败", e);
                e.printStackTrace();
            }
        }
        return jsonOutput;
    }
}
