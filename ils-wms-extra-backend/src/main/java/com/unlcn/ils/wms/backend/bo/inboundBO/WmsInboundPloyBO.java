package com.unlcn.ils.wms.backend.bo.inboundBO;

import com.unlcn.ils.wms.backend.bo.baseDataBO.WmsCommonBO;

/**
 * 策略主表
 * Created by DELL on 2017/9/1.
 */
public class WmsInboundPloyBO extends WmsCommonBO {
    /**
     * 策略ID
     */
    private Long pyId;

    /**
     * 策略仓库code
     */
    private String pyWhCode;

    /**
     * 策略仓库名
     */
    private String pyWhName;

    /**
     * 策略code
     */
    private String pyCode;

    /**
     * 策略名称
     */
    private String pyName;

    /**
     * 策略类型
     */
    private String pyType;

    /**
     * 策略状态
     */
    private String pyStatus;

    /**
     * 策略说明
     */
    private String pyRemark;

    public Long getPyId() {
        return pyId;
    }

    public void setPyId(Long pyId) {
        this.pyId = pyId;
    }

    public String getPyWhCode() {
        return pyWhCode;
    }

    public void setPyWhCode(String pyWhCode) {
        this.pyWhCode = pyWhCode;
    }

    public String getPyWhName() {
        return pyWhName;
    }

    public void setPyWhName(String pyWhName) {
        this.pyWhName = pyWhName;
    }

    public String getPyCode() {
        return pyCode;
    }

    public void setPyCode(String pyCode) {
        this.pyCode = pyCode;
    }

    public String getPyName() {
        return pyName;
    }

    public void setPyName(String pyName) {
        this.pyName = pyName;
    }

    public String getPyType() {
        return pyType;
    }

    public void setPyType(String pyType) {
        this.pyType = pyType;
    }

    public String getPyStatus() {
        return pyStatus;
    }

    public void setPyStatus(String pyStatus) {
        this.pyStatus = pyStatus;
    }

    public String getPyRemark() {
        return pyRemark;
    }

    public void setPyRemark(String pyRemark) {
        this.pyRemark = pyRemark;
    }

    @Override
    public String toString() {
        return "WmsInboundPloyBO{" +
                "pyId=" + pyId +
                ", pyWhCode='" + pyWhCode + '\'' +
                ", pyWhName='" + pyWhName + '\'' +
                ", pyCode='" + pyCode + '\'' +
                ", pyName='" + pyName + '\'' +
                ", pyType='" + pyType + '\'' +
                ", pyStatus='" + pyStatus + '\'' +
                ", pyRemark='" + pyRemark + '\'' +
                '}';
    }
}
