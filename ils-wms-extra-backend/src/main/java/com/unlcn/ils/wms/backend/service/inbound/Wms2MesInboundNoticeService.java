package com.unlcn.ils.wms.backend.service.inbound;

import com.unlcn.ils.wms.base.dto.Wms2MesInboundServiceParamDTO;

import java.util.HashMap;

public interface Wms2MesInboundNoticeService {
    HashMap<String, Object> updateInboundDataFromMes(Wms2MesInboundServiceParamDTO[] paramDTOS);
}
