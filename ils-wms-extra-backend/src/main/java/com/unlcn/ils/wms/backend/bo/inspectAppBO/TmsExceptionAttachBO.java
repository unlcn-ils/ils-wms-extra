package com.unlcn.ils.wms.backend.bo.inspectAppBO;

/**
 * Created by houjianhui on 2017/7/26.
 */
public class TmsExceptionAttachBO {
    private String pic_uri;

    public String getPic_uri() {
        return pic_uri;
    }

    public void setPic_uri(String pic_uri) {
        this.pic_uri = pic_uri;
    }

    @Override
    public String toString() {
        return "{" +
                "pic_uri='" + pic_uri + '\'' +
                '}';
    }
}
