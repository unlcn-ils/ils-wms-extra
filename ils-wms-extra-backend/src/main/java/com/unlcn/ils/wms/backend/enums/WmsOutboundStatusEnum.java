package com.unlcn.ils.wms.backend.enums;

/**
 * 备料计划出库状态
 * Created by DELL on 2017/9/27.
 */
public enum WmsOutboundStatusEnum {
    WMS_OUTBOUND_NEW(10,"未出库"),
    WMS_OUTBOUND_FINISH(20,"已出库"),
    WMS_OUTBOUND_QUIT(30, "已退库");

    private final int value;
    private final String text;
    WmsOutboundStatusEnum(int value, String text){
        this.value = value;
        this.text = text;
    }

    public int getValue() {
        return value;
    }

    public String getText() {
        return text;
    }

    public static WmsOutboundStatusEnum getByValue(int value){
        for (WmsOutboundStatusEnum temp : WmsOutboundStatusEnum.values()) {
            if(temp.getValue() == value){
                return temp;
            }
        }
        return null;
    }
}
