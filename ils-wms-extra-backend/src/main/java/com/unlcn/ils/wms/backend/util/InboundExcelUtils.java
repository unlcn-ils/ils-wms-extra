package com.unlcn.ils.wms.backend.util;

import java.io.FileOutputStream;
import java.util.List;

import com.unlcn.ils.wms.base.dto.InboundExcelDTO;
import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;

public class InboundExcelUtils {


    public static void export(List<InboundExcelDTO> inboundExcelDTOList, String filePath) throws Exception {
        // 第一步，创建一个webbook，对应一个Excel文件
        HSSFWorkbook wb = new HSSFWorkbook();
        // 第二步，在webbook中添加一个sheet,对应Excel文件中的sheet
        HSSFSheet sheet = wb.createSheet("入库表");
        // 第三步，在sheet中添加表头第0行,注意老版本poi对Excel的行数列数有限制short
        HSSFRow row = sheet.createRow((int) 0);
        // 第四步，创建单元格，并设置值表头 设置表头居中
        HSSFCellStyle style = wb.createCellStyle();
        style.setAlignment(HSSFCellStyle.ALIGN_CENTER); // 创建一个居中格式

        HSSFCell cell = row.createCell((short) 0);
        cell.setCellValue("编号");
        cell.setCellStyle(style);
        cell = row.createCell((short) 1);
        cell.setCellValue("日期");
        cell.setCellStyle(style);
        cell = row.createCell((short) 2);
        cell.setCellValue("入库时间");
        cell.setCellStyle(style);
        cell = row.createCell((short) 3);
        cell.setCellValue("VIN号");
        cell.setCellStyle(style);
        cell = row.createCell((short) 4);
        cell.setCellValue("工厂提车库位");
        cell.setCellStyle(style);
        cell = row.createCell((short) 5);
        cell.setCellValue("入库仓库");
        cell.setCellStyle(style);

        for (int i = 0; i < inboundExcelDTOList.size(); i++) {
            if (i >= 2) {
                sheet.setColumnWidth(i + 1, 200*30);
            } else {
                sheet.setColumnWidth(i + 1, 200*20);
            }
            row = sheet.createRow((int) i + 1);
            InboundExcelDTO inboundExcelDTO = (InboundExcelDTO) inboundExcelDTOList.get(i);
            // 第四步，创建单元格，并设置值
            row.createCell((short) 0).setCellValue((inboundExcelDTO.getNo()==null)?"":String.valueOf(inboundExcelDTO.getNo()));
            row.createCell((short) 1).setCellValue(inboundExcelDTO.getDate());
            row.createCell((short) 2).setCellValue(inboundExcelDTO.getInboundDate());
            row.createCell((short) 3).setCellValue(inboundExcelDTO.getVin());
            row.createCell((short) 4).setCellValue(inboundExcelDTO.getFactoryWhno());
            cell = row.createCell((short) 5);
            cell.setCellValue(inboundExcelDTO.getWhName());
        }

        // 第六步，将文件存到指定位置
        try {
            FileOutputStream fout = new FileOutputStream(filePath);
            wb.write(fout);
            fout.close();
        }
        catch (Exception e) {
            e.printStackTrace();
        }
    }

//    public static void main(String[] args) throws Exception {
//        // 第一步，创建一个webbook，对应一个Excel文件
//        HSSFWorkbook wb = new HSSFWorkbook();
//        // 第二步，在webbook中添加一个sheet,对应Excel文件中的sheet
//        HSSFSheet sheet = wb.createSheet("学生表一");
//        // 第三步，在sheet中添加表头第0行,注意老版本poi对Excel的行数列数有限制short
//        HSSFRow row = sheet.createRow((int) 0);
//        // 第四步，创建单元格，并设置值表头 设置表头居中
//        HSSFCellStyle style = wb.createCellStyle();
//        style.setAlignment(HSSFCellStyle.ALIGN_CENTER); // 创建一个居中格式
//
//        HSSFCell cell = row.createCell((short) 0);
//        cell.setCellValue("学号");
//        cell.setCellStyle(style);
//        cell = row.createCell((short) 1);
//        cell.setCellValue("姓名");
//        cell.setCellStyle(style);
//        cell = row.createCell((short) 2);
//        cell.setCellValue("年龄");
//        cell.setCellStyle(style);
//        cell = row.createCell((short) 3);
//        cell.setCellValue("生日");
//        cell.setCellStyle(style);
//
//        // 第五步，写入实体数据 实际应用中这些数据从数据库得到，
//        List list = InboundExcelUtils.getStudent();
//
//        for (int i = 0; i < list.size(); i++) {
//            row = sheet.createRow((int) i + 1);
//            InboundExcelUtils stu = (InboundExcelUtils) list.get(i);
//            // 第四步，创建单元格，并设置值
//            row.createCell((short) 0).setCellValue((double) stu.getId());
//            row.createCell((short) 1).setCellValue(stu.getName());
//            row.createCell((short) 2).setCellValue((double) stu.getAge());
//            cell = row.createCell((short) 3);
//            cell.setCellValue(new SimpleDateFormat("yyyy-mm-dd").format(stu
//                    .getBirth()));
//        }
//
//        // 第六步，将文件存到指定位置
//        try {
//            FileOutputStream fout = new FileOutputStream("d:/students.xls");
//            wb.write(fout);
//            fout.close();
//        }
//        catch (Exception e) {
//            e.printStackTrace();
//        }
//    }
}


