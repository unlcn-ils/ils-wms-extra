package com.unlcn.ils.wms.backend.service.sys;

import cn.huiyunche.commons.domain.PageVo;
import com.unlcn.ils.wms.backend.bo.sys.RoleBO;
import com.unlcn.ils.wms.backend.bo.sys.UserBO;
import com.unlcn.ils.wms.backend.dto.sysDTO.UserNewDTO;

import java.util.List;

/**
 * Created by houjianhui on 2017/5/11.
 */
public interface UserService {

    /**
     * 判断用户是否存在
     *
     * @param userName
     * @return
     * @throws Exception
     */
    UserBO isUserExist(String userName) throws Exception;

    /**
     * 查询用户信息
     *
     * @param userId 用户ID
     * @return
     * @throws Exception
     */
    UserBO getErpUser(String userId) throws Exception;
    
    /**
     * 
     * @Title: addUser 
     * @Description: 新建用户
     * @param @param userBO
     * @param @return
     * @param @throws Exception     
     * @return Integer    返回类型 
     * @throws 
     *
     */
    Integer addUser(UserBO userBO)throws Exception;
    
    /**
     * 
     * @Title: findById 
     * @Description: 根据id查询
     * @param @param id
     * @param @return
     * @param @throws Exception     
     * @return UserBO    返回类型 
     * @throws 
     *
     */
    UserBO findById(Integer id) throws Exception;
    
    /**
     * 
     * @Title: updateUser 
     * @Description: 更新用户
     * @param @param userBO
     * @param @return
     * @param @throws Exception     
     * @return UserBO    返回类型 
     * @throws 
     *
     */
    UserBO updateUser(UserBO userBO) throws Exception;
    
    /**
     * 
     * @Title: deleteUserByIds 
     * @Description: 删除用户
     * @param @param ids
     * @param @throws Exception     
     * @return void    返回类型 
     * @throws 
     *
     */
    void deleteUserByIds(String ids) throws Exception;
    
    /**
     * 
     * @Title: list 
     * @Description: 用户列表
     * @param @param userBO
     * @param @param pageVo
     * @param @return
     * @param @throws Exception     
     * @return List<UserBO>    返回类型 
     * @throws 
     *
     */
    List<UserBO> list(UserBO userBO,PageVo pageVo) throws Exception;
    
    /**
     * 
     * @Title: login 
     * @Description: 登陆
     * @param @param userBO
     * @param @return
     * @param @throws Exception     
     * @return UserBO    返回类型 
     * @throws 
     *
     */
    UserBO getLoginInfo(UserBO userBO) throws Exception;

    /**
     * app端登录获取权限
     * linbao 2017-09-26
     *
     * @param userBO
     * @return
     * @throws Exception
     */
    RoleBO getPermissForAppLogin(UserBO userBO) throws Exception;

    UserNewDTO getPermissListForAppLogin(UserBO userBO) throws Exception;
}
