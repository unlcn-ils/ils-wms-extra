package com.unlcn.ils.wms.backend.dto.outboundDTO;

import java.io.Serializable;
import java.util.Date;

public class AppWmsOutboundTaskListDTO implements Serializable {

	private Long otId;

	private String otCode;

	private String otVin;

	private String otStatus;

	private Date gmtCreate;

	private Date gmtModify;

	public Long getOtId() {
		return otId;
	}

	public void setOtId(Long otId) {
		this.otId = otId;
	}

	public String getOtCode() {
		return otCode;
	}

	public void setOtCode(String otCode) {
		this.otCode = otCode;
	}

	public String getOtVin() {
		return otVin;
	}

	public void setOtVin(String otVin) {
		this.otVin = otVin;
	}

	public String getOtStatus() {
		return otStatus;
	}

	public void setOtStatus(String otStatus) {
		this.otStatus = otStatus;
	}

	public Date getGmtCreate() {
		return gmtCreate;
	}

	public void setGmtCreate(Date gmtCreate) {
		this.gmtCreate = gmtCreate;
	}

	public Date getGmtModify() {
		return gmtModify;
	}

	public void setGmtModify(Date gmtModify) {
		this.gmtModify = gmtModify;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("AppWmsOutboundTaskListDTO [otId=");
		builder.append(otId);
		builder.append(", otCode=");
		builder.append(otCode);
		builder.append(", otVin=");
		builder.append(otVin);
		builder.append(", otStatus=");
		builder.append(otStatus);
		builder.append(", gmtCreate=");
		builder.append(gmtCreate);
		builder.append(", gmtModify=");
		builder.append(gmtModify);
		builder.append("]");
		return builder.toString();
	}

}