package com.unlcn.ils.wms.backend.bo.pickup;

import java.util.Date;

/**
 * Created by houjianhui on 2017/7/24.
 */
public class TmsPickUpBO {
    private Long id;
    private Date pickupTime;
    private String vehicle;
    private String warehouseName;
    private String vin;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Date getPickupTime() {
        return pickupTime;
    }

    public void setPickupTime(Date pickupTime) {
        this.pickupTime = pickupTime;
    }

    public String getVehicle() {
        return vehicle;
    }

    public void setVehicle(String vehicle) {
        this.vehicle = vehicle;
    }

    public String getWarehouseName() {
        return warehouseName;
    }

    public void setWarehouseName(String warehouseName) {
        this.warehouseName = warehouseName;
    }

    public String getVin() {
        return vin;
    }

    public void setVin(String vin) {
        this.vin = vin;
    }

    @Override
    public String toString() {
        return "TmsPickUpBO{" +
                "id=" + id +
                ", pickupTime=" + pickupTime +
                ", vehicle='" + vehicle + '\'' +
                ", warehouseName='" + warehouseName + '\'' +
                ", vin='" + vin + '\'' +
                '}';
    }
}
