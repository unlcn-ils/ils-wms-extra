package com.unlcn.ils.wms.backend.service.inspectApp;

import com.unlcn.ils.wms.backend.bo.inspectAppBO.TmsInspectExcpConstantBO;
import com.unlcn.ils.wms.base.dto.WmsInspectForAppDTO;
import com.unlcn.ils.wms.base.dto.WmsOutboundInspectDTO;

import java.util.List;

public interface WmsOutboundInspectService {

    WmsOutboundInspectDTO selectFetchOutboundBill(String vin, String whcode) throws Exception;

    List<TmsInspectExcpConstantBO> getInspectExcpList(WmsInspectForAppDTO dto) throws Exception;
}
