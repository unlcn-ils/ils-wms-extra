package com.unlcn.ils.wms.backend.bo.pickup;

import java.util.Date;
import java.util.List;

/**
 * @Author ：Ligl
 * @Date : 2017/7/22.
 */
public class TmsWaybillDetailBO {
    private Long id;
    private Long infoId;
    private String waybillCode;
    private String vin;
    private String vehicle;
    private String destProvince;
    private String warehouseId;
    private String warehouseName;
    private String motorNo;
    private String status;
    private Date gmtCreate;
    private List<TmsPickupExcpBO> excpList;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getWaybillCode() {
        return waybillCode;
    }

    public void setWaybillCode(String waybillCode) {
        this.waybillCode = waybillCode;
    }

    public String getVin() {
        return vin;
    }

    public void setVin(String vin) {
        this.vin = vin;
    }

    public String getVehicle() {
        return vehicle;
    }

    public void setVehicle(String vehicle) {
        this.vehicle = vehicle;
    }

    public String getDestProvince() {
        return destProvince;
    }

    public void setDestProvince(String destProvince) {
        this.destProvince = destProvince;
    }

    public String getWarehouseId() {
        return warehouseId;
    }

    public void setWarehouseId(String warehouseId) {
        this.warehouseId = warehouseId;
    }

    public String getWarehouseName() {
        return warehouseName;
    }

    public void setWarehouseName(String warehouseName) {
        this.warehouseName = warehouseName;
    }

    public String getMotorNo() {
        return motorNo;
    }

    public void setMotorNo(String motorNo) {
        this.motorNo = motorNo;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Long getInfoId() {
        return infoId;
    }

    public void setInfoId(Long infoId) {
        this.infoId = infoId;
    }

    public Date getGmtCreate() {
        return gmtCreate;
    }

    public void setGmtCreate(Date gmtCreate) {
        this.gmtCreate = gmtCreate;
    }

    public List<TmsPickupExcpBO> getExcpList() {
        return excpList;
    }

    public void setExcpList(List<TmsPickupExcpBO> excpList) {
        this.excpList = excpList;
    }

    @Override
    public String toString() {
        return "TmsWaybillDetailBO{" +
                "id=" + id +
                ", infoId=" + infoId +
                ", waybillCode='" + waybillCode + '\'' +
                ", vin='" + vin + '\'' +
                ", vehicle='" + vehicle + '\'' +
                ", destProvince='" + destProvince + '\'' +
                ", warehouseId='" + warehouseId + '\'' +
                ", warehouseName='" + warehouseName + '\'' +
                ", motorNo='" + motorNo + '\'' +
                ", status='" + status + '\'' +
                ", gmtCreate=" + gmtCreate +
                ", excpList=" + excpList +
                '}';
    }
}
