package com.unlcn.ils.wms.web.controller;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

//@Controller("/")
public class IndexController {
	
	
	@RequestMapping(value="/index",method=RequestMethod.GET)
	public String index(){
        return "index";
    }
	
	@RequestMapping(value="/customIndex",method=RequestMethod.GET)
	public String customIndex(){
        return "customIndex";
    }
}
