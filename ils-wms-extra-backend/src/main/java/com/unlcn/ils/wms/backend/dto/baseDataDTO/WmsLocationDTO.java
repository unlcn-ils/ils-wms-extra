package com.unlcn.ils.wms.backend.dto.baseDataDTO;

import java.util.Date;

/**
 * Created by DELL on 2017/8/18.
 */
public class WmsLocationDTO {

    private Long locId;

    private String locWhId;

    private String locWhCode;

    private String locWhName;

    private String locZoneId;

    private String locZoneCode;

    private String locCode;

    private Long locRow;

    private Long locColumn;

    private Long locLevel;

    private Long locLength;

    private Long locWidth;

    private String createPerson;

    private String updatePerson;

    private Date gmtCreate;

    private Date gmtUpdate;

    private Byte isDeleted;

    private Long versions;

    public Long getLocId() {
        return locId;
    }

    public void setLocId(Long locId) {
        this.locId = locId;
    }

    public String getLocWhId() {
        return locWhId;
    }

    public void setLocWhId(String locWhId) {
        this.locWhId = locWhId;
    }

    public String getLocWhCode() {
        return locWhCode;
    }

    public void setLocWhCode(String locWhCode) {
        this.locWhCode = locWhCode;
    }

    public String getLocWhName() {
        return locWhName;
    }

    public void setLocWhName(String locWhName) {
        this.locWhName = locWhName;
    }

    public String getLocZoneId() {
        return locZoneId;
    }

    public void setLocZoneId(String locZoneId) {
        this.locZoneId = locZoneId;
    }

    public String getLocZoneCode() {
        return locZoneCode;
    }

    public void setLocZoneCode(String locZoneCode) {
        this.locZoneCode = locZoneCode;
    }

    public String getLocCode() {
        return locCode;
    }

    public void setLocCode(String locCode) {
        this.locCode = locCode;
    }

    public Long getLocRow() {
        return locRow;
    }

    public void setLocRow(Long locRow) {
        this.locRow = locRow;
    }

    public Long getLocColumn() {
        return locColumn;
    }

    public void setLocColumn(Long locColumn) {
        this.locColumn = locColumn;
    }

    public Long getLocLevel() {
        return locLevel;
    }

    public void setLocLevel(Long locLevel) {
        this.locLevel = locLevel;
    }

    public Long getLocLength() {
        return locLength;
    }

    public void setLocLength(Long locLength) {
        this.locLength = locLength;
    }

    public Long getLocWidth() {
        return locWidth;
    }

    public void setLocWidth(Long locWidth) {
        this.locWidth = locWidth;
    }

    public String getCreatePerson() {
        return createPerson;
    }

    public void setCreatePerson(String createPerson) {
        this.createPerson = createPerson;
    }

    public String getUpdatePerson() {
        return updatePerson;
    }

    public void setUpdatePerson(String updatePerson) {
        this.updatePerson = updatePerson;
    }

    public Date getGmtCreate() {
        return gmtCreate;
    }

    public void setGmtCreate(Date gmtCreate) {
        this.gmtCreate = gmtCreate;
    }

    public Date getGmtUpdate() {
        return gmtUpdate;
    }

    public void setGmtUpdate(Date gmtUpdate) {
        this.gmtUpdate = gmtUpdate;
    }

    public Byte getIsDeleted() {
        return isDeleted;
    }

    public void setIsDeleted(Byte isDeleted) {
        this.isDeleted = isDeleted;
    }

    public Long getVersions() {
        return versions;
    }

    public void setVersions(Long versions) {
        this.versions = versions;
    }

    @Override
    public String toString() {
        return "WmsLocationDTO{" +
                "locId=" + locId +
                ", locWhId='" + locWhId + '\'' +
                ", locWhCode='" + locWhCode + '\'' +
                ", locWhName='" + locWhName + '\'' +
                ", locZoneId='" + locZoneId + '\'' +
                ", locZoneCode='" + locZoneCode + '\'' +
                ", locCode='" + locCode + '\'' +
                ", locRow=" + locRow +
                ", locColumn=" + locColumn +
                ", locLevel=" + locLevel +
                ", locLength=" + locLength +
                ", locWidth=" + locWidth +
                ", createPerson='" + createPerson + '\'' +
                ", updatePerson='" + updatePerson + '\'' +
                ", gmtCreate=" + gmtCreate +
                ", gmtUpdate=" + gmtUpdate +
                ", isDeleted=" + isDeleted +
                ", versions=" + versions +
                '}';
    }
}
