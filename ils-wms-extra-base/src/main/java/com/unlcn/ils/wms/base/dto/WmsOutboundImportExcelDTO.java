package com.unlcn.ils.wms.base.dto;

import java.io.Serializable;

public class WmsOutboundImportExcelDTO implements Serializable {
    private String whCode;
    private String userId;
    private String outConfirmStartTime;
    private String outConfirmEndTime;
    private String shipStartTime;
    private String shipEndTime;
    private String spCarrier;
    private String spDealer;

    public String getSpDealer() {
        return spDealer;
    }

    public void setSpDealer(String spDealer) {
        this.spDealer = spDealer;
    }

    public String getSpCarrier() {
        return spCarrier;
    }

    public void setSpCarrier(String spCarrier) {
        this.spCarrier = spCarrier;
    }

    public String getWhCode() {
        return whCode;
    }

    public void setWhCode(String whCode) {
        this.whCode = whCode;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getOutConfirmStartTime() {
        return outConfirmStartTime;
    }

    public void setOutConfirmStartTime(String outConfirmStartTime) {
        this.outConfirmStartTime = outConfirmStartTime;
    }

    public String getOutConfirmEndTime() {
        return outConfirmEndTime;
    }

    public void setOutConfirmEndTime(String outConfirmEndTime) {
        this.outConfirmEndTime = outConfirmEndTime;
    }

    public String getShipStartTime() {
        return shipStartTime;
    }

    public void setShipStartTime(String shipStartTime) {
        this.shipStartTime = shipStartTime;
    }

    public String getShipEndTime() {
        return shipEndTime;
    }

    public void setShipEndTime(String shipEndTime) {
        this.shipEndTime = shipEndTime;
    }

    @Override
    public String toString() {
        return "WmsOutboundImportExcelDTO{" +
                "whCode='" + whCode + '\'' +
                ", userId='" + userId + '\'' +
                ", outConfirmStartTime='" + outConfirmStartTime + '\'' +
                ", outConfirmEndTime='" + outConfirmEndTime + '\'' +
                ", shipStartTime='" + shipStartTime + '\'' +
                ", shipEndTime='" + shipEndTime + '\'' +
                '}';
    }
}
