package com.unlcn.ils.wms.backend.enums;

/**
 * @Auther linbao
 * @Date 2017-11-09
 */
public enum OutBoundTaskTypeEnum {
    DIS_OUT("10", "调拨出库"),
    NORMAL_OUT("20", "发运出库"),
    REPAIR_OUT("30", "维修出库"),
    BORROW_OUT("40", "借用出库");

    private final String code;

    private final String value;


    public String getCode() {
        return code;
    }

    public String getValue() {
        return value;
    }

    OutBoundTaskTypeEnum(String code, String value) {
        this.code = code;
        this.value = value;
    }

    public static OutBoundTaskTypeEnum getByValue(String value) {
        for (OutBoundTaskTypeEnum temp : OutBoundTaskTypeEnum.values()) {
            if (temp.getCode().equals(value)) {
                return temp;
            }
        }
        return null;
    }

}
