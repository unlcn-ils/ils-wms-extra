package com.unlcn.ils.wms.backend.bo.inspectAppBO;

import java.io.Serializable;

public class TmsInboundMoveCarBO implements Serializable {

    private String picUrl;
    private String vin;
    private String warhousingDriverId;
    private Integer inboundStatusValue;
    private String inboundStatusText;
    private String warehousingLocation;

    public String getWarehousingLocation() {
        return warehousingLocation;
    }

    public void setWarehousingLocation(String warehousingLocation) {
        this.warehousingLocation = warehousingLocation;
    }

    public String getPicUrl() {
        return picUrl;
    }

    public void setPicUrl(String picUrl) {
        this.picUrl = picUrl;
    }

    public String getVin() {
        return vin;
    }

    public void setVin(String vin) {
        this.vin = vin;
    }

    public String getWarhousingDriverId() {
        return warhousingDriverId;
    }

    public void setWarhousingDriverId(String warhousingDriverId) {
        this.warhousingDriverId = warhousingDriverId;
    }

    public Integer getInboundStatusValue() {
        return inboundStatusValue;
    }

    public void setInboundStatusValue(Integer inboundStatusValue) {
        this.inboundStatusValue = inboundStatusValue;
    }

    public String getInboundStatusText() {
        return inboundStatusText;
    }

    public void setInboundStatusText(String inboundStatusText) {
        this.inboundStatusText = inboundStatusText;
    }
}
