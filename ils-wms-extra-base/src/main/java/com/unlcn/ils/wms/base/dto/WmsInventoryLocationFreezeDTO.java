package com.unlcn.ils.wms.base.dto;

import java.io.Serializable;

public class WmsInventoryLocationFreezeDTO implements Serializable {

    private String invlocVin;
    private String preparationVin;
    private String invlocStatus;
    private String invlocId;

    public String getInvlocVin() {
        return invlocVin;
    }

    public void setInvlocVin(String invlocVin) {
        this.invlocVin = invlocVin;
    }

    public String getPreparationVin() {
        return preparationVin;
    }

    public void setPreparationVin(String preparationVin) {
        this.preparationVin = preparationVin;
    }

    public String getInvlocStatus() {
        return invlocStatus;
    }

    public void setInvlocStatus(String invlocStatus) {
        this.invlocStatus = invlocStatus;
    }

    public String getInvlocId() {
        return invlocId;
    }

    public void setInvlocId(String invlocId) {
        this.invlocId = invlocId;
    }
}
