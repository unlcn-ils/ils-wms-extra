package com.unlcn.ils.wms.base.mapper.extmapper;

import com.unlcn.ils.wms.base.dto.WmsShipmentPlanRejectDTO;
import com.unlcn.ils.wms.base.model.stock.WmsShipmentPlan;
import org.apache.ibatis.annotations.Param;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @Auther linbao
 * @Date 2017-10-27
 */
public interface WmsShipmentPlanRejectExtMapper {

    /**
     * 分页列表查询
     *
     * @param paramMap
     * @return
     */
    List<WmsShipmentPlanRejectDTO> queryRejectPlanList(Map<String, Object> paramMap);

    /**
     * 查询数量
     *
     * @param paramMap
     * @return
     */
    Integer countRejectPlanList(Map<String, Object> paramMap);


    /**
     * 根据组版单号根据获取id集合
     *
     * @return
     */
    List<Long> getSpIdListGroupByBoardNo();

    /**
     *
     * @param id
     * @return
     */
    Long getQtyById(Long id);

    List<WmsShipmentPlan> selectGroupListBySpId(HashMap<String, Object> params);

    List<WmsShipmentPlan> selectRejectListByIds(List<String> ids);

    void updateShipmentRejectStatusBatch(@Param("params") HashMap<String, Object> params);
}
