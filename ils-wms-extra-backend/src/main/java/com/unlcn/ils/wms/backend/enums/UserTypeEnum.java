package com.unlcn.ils.wms.backend.enums;

/**
 * @Auther linbao
 * @Date 2017-10-13
 */
public enum UserTypeEnum {

    DELETED((byte)0, "APP"),
    NORMAL((byte)1, "PC"),
    DRIVER((byte)2, "APP司机");

    private final byte value;
    private final String text;

    public byte getValue() {
        return value;
    }

    public String getText() {
        return text;
    }

    UserTypeEnum(byte value, String text) {
        this.value = value;
        this.text = text;
    }
}
