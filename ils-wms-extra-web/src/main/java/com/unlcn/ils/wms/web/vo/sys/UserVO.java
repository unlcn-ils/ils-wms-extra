package com.unlcn.ils.wms.web.vo.sys;

import cn.huiyunche.commons.domain.PageVo;

import java.util.Date;
import java.util.List;

/**
 * 
 * @ClassName: UserVO 
 * @Description: 用户VO
 * @author laishijian
 * @date 2017年8月7日 下午4:26:58 
 *
 */
public class UserVO {
	
	//id
	private Integer id;
	//用户名
	private String username;
	//密码
	private String password;
	//创建人
	private String createPerson;
	//修改人
	private String updatePersion;
	//创建时间
	private Date gmtCreate;
	//修改时间
	private Date gmtModified;
	//是否可用
	private String enable;
	//分页
	private PageVo pageVO;
	//角色 volist
	private List<RoleVO> roleVOList;
	
	/**
	 * @return the id
	 */
	public Integer getId() {
		return id;
	}
	/**
	 * @param id the id to set
	 */
	public void setId(Integer id) {
		this.id = id;
	}
	/**
	 * @return the username
	 */
	public String getUsername() {
		return username;
	}
	/**
	 * @param username the username to set
	 */
	public void setUsername(String username) {
		this.username = username;
	}
	/**
	 * @return the password
	 */
	public String getPassword() {
		return password;
	}
	/**
	 * @param password the password to set
	 */
	public void setPassword(String password) {
		this.password = password;
	}
	/**
	 * @return the createPerson
	 */
	public String getCreatePerson() {
		return createPerson;
	}
	/**
	 * @param createPerson the createPerson to set
	 */
	public void setCreatePerson(String createPerson) {
		this.createPerson = createPerson;
	}
	/**
	 * @return the updatePersion
	 */
	public String getUpdatePersion() {
		return updatePersion;
	}
	/**
	 * @param updatePersion the updatePersion to set
	 */
	public void setUpdatePersion(String updatePersion) {
		this.updatePersion = updatePersion;
	}
	/**
	 * @return the gmtCreate
	 */
	public Date getGmtCreate() {
		return gmtCreate;
	}
	/**
	 * @param gmtCreate the gmtCreate to set
	 */
	public void setGmtCreate(Date gmtCreate) {
		this.gmtCreate = gmtCreate;
	}
	/**
	 * @return the gmtModified
	 */
	public Date getGmtModified() {
		return gmtModified;
	}
	/**
	 * @param gmtModified the gmtModified to set
	 */
	public void setGmtModified(Date gmtModified) {
		this.gmtModified = gmtModified;
	}
	/**
	 * @return the enable
	 */
	public String getEnable() {
		return enable;
	}
	/**
	 * @param enable the enable to set
	 */
	public void setEnable(String enable) {
		this.enable = enable;
	}
	/**
	 * @return the pageVO
	 */
	public PageVo getPageVO() {
		return pageVO;
	}
	/**
	 * @param pageVO the pageVO to set
	 */
	public void setPageVO(PageVo pageVO) {
		this.pageVO = pageVO;
	}
	/**
	 * @return the roleVOList
	 */
	public List<RoleVO> getRoleVOList() {
		return roleVOList;
	}
	/**
	 * @param roleVOList the roleVOList to set
	 */
	public void setRoleVOList(List<RoleVO> roleVOList) {
		this.roleVOList = roleVOList;
	}
	
	/*
	 * Title: toString
	 * Description: toString
	 * @return
	 * @see java.lang.Object#toString()
	 *
	 */
	@Override
	public String toString() {
		return "UserVO [id=" + id + ", username=" + username + ", password=" + password + ", createPerson="
				+ createPerson + ", updatePersion=" + updatePersion + ", gmtCreate=" + gmtCreate + ", gmtModified="
				+ gmtModified + ", enable=" + enable + "]";
	}
}
