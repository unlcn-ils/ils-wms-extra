package com.unlcn.ils.wms.base.mapper.additional.wmsInboundPloy;

import com.unlcn.ils.wms.base.model.inbound.WmsInboundPloyOperator;

import java.util.List;

/**
 * Created by DELL on 2017/9/1.
 */
public interface WmsInboundPloyOperatorExtMapper {
    List<WmsInboundPloyOperator> getOperatorByCode(String pyoCode);
}
