/**
 * Created by houjianhui on 2017/7/18.
 */

'use strict';
kasCtrl.controller('WayBillCtrl', ['$rootScope', '$scope', 'Settings', 'WaybillService', '$ionicLoading', '$filter', function ($rootScope, $scope, Settings, WaybillService, $ionicLoading, $filter) {
    $scope.waybillInfo = null;
    $scope.isQuery = false;
    $scope.changeInp = function () {
        if ($rootScope.WeiUI.search.content != null) {
            $scope.isQuery = true;
        } else {
            $scope.isQuery = false;
            $scope.waybillInfo = null;
        }
    };
    $scope.getWaybillInfo = function () {
        $ionicLoading.show({
            template: '加载中...'
        });
        WaybillService.getWaybillCode($rootScope.WeiUI.search.content)
            .success(function (result) {
                if (result.success) {
                    $scope.waybillInfo = result.data;
                }
            })
            .finally(function () {
                $ionicLoading.hide();
            });
    };
    $scope.includeSearchBar = Settings.Context.path + '/public/template/wx/components/weui-searchbar-component.html';
}]);
