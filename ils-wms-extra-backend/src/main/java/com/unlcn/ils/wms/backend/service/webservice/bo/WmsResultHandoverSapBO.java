package com.unlcn.ils.wms.backend.service.webservice.bo;

import java.io.Serializable;

public class WmsResultHandoverSapBO implements Serializable {
    //pi参数:
    private String dataId; //唯一标识
    private String vbeln; //SAP交货单号
    private String posnr; //SAP交货单行项目
    private String zvbeln; //订单号
    private String zposnr;//订单行ID
    private String zstatus;//处理状态
    private String zmsg;//处理日志

    public String getDataId() {
        return dataId;
    }

    public void setDataId(String dataId) {
        this.dataId = dataId;
    }

    public String getVbeln() {
        return vbeln;
    }

    public void setVbeln(String vbeln) {
        this.vbeln = vbeln;
    }

    public String getPosnr() {
        return posnr;
    }

    public void setPosnr(String posnr) {
        this.posnr = posnr;
    }

    public String getZvbeln() {
        return zvbeln;
    }

    public void setZvbeln(String zvbeln) {
        this.zvbeln = zvbeln;
    }

    public String getZposnr() {
        return zposnr;
    }

    public void setZposnr(String zposnr) {
        this.zposnr = zposnr;
    }

    public String getZstatus() {
        return zstatus;
    }

    public void setZstatus(String zstatus) {
        this.zstatus = zstatus;
    }

    public String getZmsg() {
        return zmsg;
    }

    public void setZmsg(String zmsg) {
        this.zmsg = zmsg;
    }
}
