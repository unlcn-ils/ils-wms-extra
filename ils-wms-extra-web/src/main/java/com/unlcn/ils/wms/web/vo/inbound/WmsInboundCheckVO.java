package com.unlcn.ils.wms.web.vo.inbound;

import java.util.List;

/**
 * 新需求收货质检
 * Created by DELL on 2017/9/13.
 */
public class WmsInboundCheckVO {

    /**
     * 入库订单主ID
     */
    private Long odId;

    /**
     * 订单CODE,WMS专用
     */
    private String odCode;

    /**
     * 货主ID
     */
    private Long odCustomerId;

    /**
     * 货主CODE
     */
    private String odCustomerCode;

    /**
     * 货主名称
     */
    private String odCustomerName;

    /**
     * 调度单号
     */
    private String odDispatchNo;

    /**
     * 运单号
     */
    private String odWaybillNo;

    /**
     * 运单类型
     */
    private String odWaybillType;

    /**
     * 订单状态
     */
    private String odStatus;

    /**
     * 提货地址
     */
    private String odPickUpAddr;

    /**
     * 仓库ID
     */
    private Long odWhId;

    /**
     * 仓库CODE
     */
    private String odWhCode;

    /**
     * 订单商品数量
     */
    private Long odInboundNum;

    /**
     * 交货方
     */
    private String odSupplierParty;

    /**
     * 交货方CODE
     */
    private String odSupplierCode;

    /**
     * 交货方名称
     */
    private String odSupplierName;

    /**
     * 交货方联系人手机号
     */
    private String odSupplierPhone;

    /**
     * 收货人ID
     */
    private String odConsigneeId;

    /**
     * 收货人CODE
     */
    private String odConsigneeCode;

    /**
     * 收货人名称
     */
    private String odConsigneeName;

    /**
     * 收货时间
     */
    private String odConsigneeDate;

    /**
     * 检验结果
     */
    private String odCheckResult;

    /**
     * 检验描述
     */
    private String odCheckDesc;

    /**
     * 起始地code
     */
    private String odOriginCode;

    /**
     * 起始地
     */
    private String odOrigin;

    /**
     * 目的地code
     */
    private String odDestCode;

    /**
     * 目的地
     */
    private String odDest;

    /**
     * 备注
     */
    private String odRemark;

    /**
     * 逻辑删除
     */
    private Byte isDeleted;

    /**
     * 创建开始日期
     */
    private String createStartDate;

    /**
     * 创建结束日期
     */
    private String createEndDate;

    /**
     * 收货开始日期
     */
    private String consigneeStartDate;

    /**
     * 收货结束日期
     */
    private String consigneeEndDate;

    /**
     * 维修方
     */
    private String odRepairParty;

    /**
     * 客户运单号
     */
    private String odCustomerOrderno;

    /**
     * 任务单
     */
    private String odTaskNo;

    /**
     * 创建时间
     */
    private String gmtCreate;

    /**
     * 修改时间
     */
    private String gmtUpdate;

    /***********************订单详情***************************/
    private List<WmsInboundOrderDetailVO> wmsInboundOrderDetailVOList = null;

    /***********************分配***************************/
    private List<WmsInboundAllocationVO> wmsInboundAllocationVOList = null;

    /***********************质检***************************/
    //private List<WmsInboundCheckDetailVO> wmsInboundCheckDetailVOList = null;
    /***********************维修单***************************/
    //private List<WmsInboundRepairVO> wmsInboundRepairVOList = null;

    public Long getOdId() {
        return odId;
    }

    public void setOdId(Long odId) {
        this.odId = odId;
    }

    public String getOdCode() {
        return odCode;
    }

    public void setOdCode(String odCode) {
        this.odCode = odCode;
    }

    public Long getOdCustomerId() {
        return odCustomerId;
    }

    public void setOdCustomerId(Long odCustomerId) {
        this.odCustomerId = odCustomerId;
    }

    public String getOdCustomerCode() {
        return odCustomerCode;
    }

    public void setOdCustomerCode(String odCustomerCode) {
        this.odCustomerCode = odCustomerCode;
    }

    public String getOdCustomerName() {
        return odCustomerName;
    }

    public void setOdCustomerName(String odCustomerName) {
        this.odCustomerName = odCustomerName;
    }

    public String getOdDispatchNo() {
        return odDispatchNo;
    }

    public void setOdDispatchNo(String odDispatchNo) {
        this.odDispatchNo = odDispatchNo;
    }

    public String getOdWaybillNo() {
        return odWaybillNo;
    }

    public void setOdWaybillNo(String odWaybillNo) {
        this.odWaybillNo = odWaybillNo;
    }

    public String getOdWaybillType() {
        return odWaybillType;
    }

    public void setOdWaybillType(String odWaybillType) {
        this.odWaybillType = odWaybillType;
    }

    public String getOdStatus() {
        return odStatus;
    }

    public void setOdStatus(String odStatus) {
        this.odStatus = odStatus;
    }

    public String getOdPickUpAddr() {
        return odPickUpAddr;
    }

    public void setOdPickUpAddr(String odPickUpAddr) {
        this.odPickUpAddr = odPickUpAddr;
    }

    public Long getOdWhId() {
        return odWhId;
    }

    public void setOdWhId(Long odWhId) {
        this.odWhId = odWhId;
    }

    public String getOdWhCode() {
        return odWhCode;
    }

    public void setOdWhCode(String odWhCode) {
        this.odWhCode = odWhCode;
    }

    public Long getOdInboundNum() {
        return odInboundNum;
    }

    public void setOdInboundNum(Long odInboundNum) {
        this.odInboundNum = odInboundNum;
    }

    public String getOdSupplierParty() {
        return odSupplierParty;
    }

    public void setOdSupplierParty(String odSupplierParty) {
        this.odSupplierParty = odSupplierParty;
    }

    public String getOdSupplierCode() {
        return odSupplierCode;
    }

    public void setOdSupplierCode(String odSupplierCode) {
        this.odSupplierCode = odSupplierCode;
    }

    public String getOdSupplierName() {
        return odSupplierName;
    }

    public void setOdSupplierName(String odSupplierName) {
        this.odSupplierName = odSupplierName;
    }

    public String getOdSupplierPhone() {
        return odSupplierPhone;
    }

    public void setOdSupplierPhone(String odSupplierPhone) {
        this.odSupplierPhone = odSupplierPhone;
    }

    public String getOdConsigneeId() {
        return odConsigneeId;
    }

    public void setOdConsigneeId(String odConsigneeId) {
        this.odConsigneeId = odConsigneeId;
    }

    public String getOdConsigneeCode() {
        return odConsigneeCode;
    }

    public void setOdConsigneeCode(String odConsigneeCode) {
        this.odConsigneeCode = odConsigneeCode;
    }

    public String getOdConsigneeName() {
        return odConsigneeName;
    }

    public void setOdConsigneeName(String odConsigneeName) {
        this.odConsigneeName = odConsigneeName;
    }

    public String getOdConsigneeDate() {
        return odConsigneeDate;
    }

    public void setOdConsigneeDate(String odConsigneeDate) {
        this.odConsigneeDate = odConsigneeDate;
    }

    public String getOdCheckResult() {
        return odCheckResult;
    }

    public void setOdCheckResult(String odCheckResult) {
        this.odCheckResult = odCheckResult;
    }

    public String getOdCheckDesc() {
        return odCheckDesc;
    }

    public void setOdCheckDesc(String odCheckDesc) {
        this.odCheckDesc = odCheckDesc;
    }

    public String getOdOriginCode() {
        return odOriginCode;
    }

    public void setOdOriginCode(String odOriginCode) {
        this.odOriginCode = odOriginCode;
    }

    public String getOdOrigin() {
        return odOrigin;
    }

    public void setOdOrigin(String odOrigin) {
        this.odOrigin = odOrigin;
    }

    public String getOdDestCode() {
        return odDestCode;
    }

    public void setOdDestCode(String odDestCode) {
        this.odDestCode = odDestCode;
    }

    public String getOdDest() {
        return odDest;
    }

    public void setOdDest(String odDest) {
        this.odDest = odDest;
    }

    public String getOdRemark() {
        return odRemark;
    }

    public void setOdRemark(String odRemark) {
        this.odRemark = odRemark;
    }

    public Byte getIsDeleted() {
        return isDeleted;
    }

    public void setIsDeleted(Byte isDeleted) {
        this.isDeleted = isDeleted;
    }

    public String getCreateStartDate() {
        return createStartDate;
    }

    public void setCreateStartDate(String createStartDate) {
        this.createStartDate = createStartDate;
    }

    public String getCreateEndDate() {
        return createEndDate;
    }

    public void setCreateEndDate(String createEndDate) {
        this.createEndDate = createEndDate;
    }

    public String getConsigneeStartDate() {
        return consigneeStartDate;
    }

    public void setConsigneeStartDate(String consigneeStartDate) {
        this.consigneeStartDate = consigneeStartDate;
    }

    public String getConsigneeEndDate() {
        return consigneeEndDate;
    }

    public void setConsigneeEndDate(String consigneeEndDate) {
        this.consigneeEndDate = consigneeEndDate;
    }

    public String getOdRepairParty() {
        return odRepairParty;
    }

    public void setOdRepairParty(String odRepairParty) {
        this.odRepairParty = odRepairParty;
    }

    public String getOdCustomerOrderno() {
        return odCustomerOrderno;
    }

    public void setOdCustomerOrderno(String odCustomerOrderno) {
        this.odCustomerOrderno = odCustomerOrderno;
    }

    public String getOdTaskNo() {
        return odTaskNo;
    }

    public void setOdTaskNo(String odTaskNo) {
        this.odTaskNo = odTaskNo;
    }

    public String getGmtCreate() {
        return gmtCreate;
    }

    public void setGmtCreate(String gmtCreate) {
        this.gmtCreate = gmtCreate;
    }

    public String getGmtUpdate() {
        return gmtUpdate;
    }

    public void setGmtUpdate(String gmtUpdate) {
        this.gmtUpdate = gmtUpdate;
    }

    public List<WmsInboundOrderDetailVO> getWmsInboundOrderDetailVOList() {
        return wmsInboundOrderDetailVOList;
    }

    public void setWmsInboundOrderDetailVOList(List<WmsInboundOrderDetailVO> wmsInboundOrderDetailVOList) {
        this.wmsInboundOrderDetailVOList = wmsInboundOrderDetailVOList;
    }

    /*public List<WmsInboundCheckDetailVO> getWmsInboundCheckDetailVOList() {
        return wmsInboundCheckDetailVOList;
    }

    public void setWmsInboundCheckDetailVOList(List<WmsInboundCheckDetailVO> wmsInboundCheckDetailVOList) {
        this.wmsInboundCheckDetailVOList = wmsInboundCheckDetailVOList;
    }

    public List<WmsInboundRepairVO> getWmsInboundRepairVOList() {
        return wmsInboundRepairVOList;
    }

    public void setWmsInboundRepairVOList(List<WmsInboundRepairVO> wmsInboundRepairVOList) {
        this.wmsInboundRepairVOList = wmsInboundRepairVOList;
    }*/

    public List<WmsInboundAllocationVO> getWmsInboundAllocationVOList() {
        return wmsInboundAllocationVOList;
    }

    public void setWmsInboundAllocationVOList(List<WmsInboundAllocationVO> wmsInboundAllocationVOList) {
        this.wmsInboundAllocationVOList = wmsInboundAllocationVOList;
    }

    @Override
    public String toString() {
        return "WmsInboundCheckVO{" +
                "odId=" + odId +
                ", odCode='" + odCode + '\'' +
                ", odCustomerId=" + odCustomerId +
                ", odCustomerCode='" + odCustomerCode + '\'' +
                ", odCustomerName='" + odCustomerName + '\'' +
                ", odDispatchNo='" + odDispatchNo + '\'' +
                ", odWaybillNo='" + odWaybillNo + '\'' +
                ", odWaybillType='" + odWaybillType + '\'' +
                ", odStatus='" + odStatus + '\'' +
                ", odPickUpAddr='" + odPickUpAddr + '\'' +
                ", odWhId=" + odWhId +
                ", odWhCode='" + odWhCode + '\'' +
                ", odInboundNum=" + odInboundNum +
                ", odSupplierParty='" + odSupplierParty + '\'' +
                ", odSupplierCode='" + odSupplierCode + '\'' +
                ", odSupplierName='" + odSupplierName + '\'' +
                ", odSupplierPhone='" + odSupplierPhone + '\'' +
                ", odConsigneeId='" + odConsigneeId + '\'' +
                ", odConsigneeCode='" + odConsigneeCode + '\'' +
                ", odConsigneeName='" + odConsigneeName + '\'' +
                ", odConsigneeDate='" + odConsigneeDate + '\'' +
                ", odCheckResult='" + odCheckResult + '\'' +
                ", odCheckDesc='" + odCheckDesc + '\'' +
                ", odOriginCode='" + odOriginCode + '\'' +
                ", odOrigin='" + odOrigin + '\'' +
                ", odDestCode='" + odDestCode + '\'' +
                ", odDest='" + odDest + '\'' +
                ", odRemark='" + odRemark + '\'' +
                ", isDeleted=" + isDeleted +
                ", createStartDate='" + createStartDate + '\'' +
                ", createEndDate='" + createEndDate + '\'' +
                ", consigneeStartDate='" + consigneeStartDate + '\'' +
                ", consigneeEndDate='" + consigneeEndDate + '\'' +
                ", odRepairParty='" + odRepairParty + '\'' +
                ", odCustomerOrderno='" + odCustomerOrderno + '\'' +
                ", odTaskNo='" + odTaskNo + '\'' +
                ", gmtCreate='" + gmtCreate + '\'' +
                ", gmtUpdate='" + gmtUpdate + '\'' +
                ", wmsInboundOrderDetailVOList=" + wmsInboundOrderDetailVOList +
                ", wmsInboundAllocationVOList=" + wmsInboundAllocationVOList +
                '}';
    }
}
