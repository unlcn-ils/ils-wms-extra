package com.unlcn.ils.wms.backend.service.bigscreen;

import com.unlcn.ils.wms.backend.dto.bigscreenDTO.TransModelDTO;

import java.util.List;

/**
 * Created by lenovo on 2017/11/9.
 */
public interface TransModelService {

    /**
     * 发运模式推移图统计信息
     * @return
     */
    List<TransModelDTO> transModelChartCount();

    /**
     * 添加发运模式推移图
     */
    void addTransMode();
}
