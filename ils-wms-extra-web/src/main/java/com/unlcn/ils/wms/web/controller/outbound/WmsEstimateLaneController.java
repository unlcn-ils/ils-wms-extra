package com.unlcn.ils.wms.web.controller.outbound;

import cn.huiyunche.commons.domain.ResultDTO;
import cn.huiyunche.commons.exception.BusinessException;
import com.unlcn.ils.wms.backend.bo.outboundBO.WmsEstimateLaneBO;
import com.unlcn.ils.wms.backend.service.outbound.WmsEstimateLaneService;
import com.unlcn.ils.wms.web.utils.Vo2Bo;
import com.unlcn.ils.wms.web.vo.outbound.WmsEstimateLaneVO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;

/**
 * Created by DELL on 2017/10/13.
 */
@Controller
@RequestMapping("/estimateLane")
@ResponseBody
public class WmsEstimateLaneController {
    private Logger LOGGER = LoggerFactory.getLogger(WmsEstimateLaneController.class);
    @Autowired
    WmsEstimateLaneService wmsEstimateLaneService;
    /**
     * 查询数据分页
     * @param whCode
     * @return
     * @throws BusinessException
     */
    @RequestMapping(value = "/getEstimateLaneByWhCode", method = RequestMethod.POST)
    public ResultDTO getEstimateLaneByWhName(@RequestBody String whCode) throws BusinessException {
        ResultDTO<Object> resultDTO = new ResultDTO<Object>(true, null, "查询成功");
        try {
            resultDTO.setData(wmsEstimateLaneService.getEstimateLaneByWhCode(whCode));
            resultDTO.setMessage("查询成功");
            resultDTO.setMessageCode("200");
            resultDTO.setSuccess(true);
        } catch (Exception e) {
            LOGGER.error("WmsEstimateLaneController.getEstimateLaneByWhName error: {}", e);
            resultDTO.setMessage("查询异常");
            resultDTO.setMessageCode("500");
            resultDTO.setSuccess(false);
        }
        return resultDTO;
    }

    @RequestMapping(value = "/addEstimateLane", method = RequestMethod.POST)
    public ResultDTO addEstimateLane(@RequestBody WmsEstimateLaneVO wmsEstimateLaneVO){
        ResultDTO<Object> resultDTO = new ResultDTO<Object>(true, null, "新增成功");
        try {
            WmsEstimateLaneBO wmsEstimateLaneBO = new WmsEstimateLaneBO();
            BeanUtils.copyProperties(wmsEstimateLaneVO,wmsEstimateLaneBO);
            wmsEstimateLaneService.addEstimateLane(wmsEstimateLaneBO);
            resultDTO.setMessage("新增成功");
            resultDTO.setMessageCode("200");
            resultDTO.setSuccess(true);
        } catch (Exception e) {
            LOGGER.error("WmsEstimateLaneController.addEstimateLane error: {}", e);
            resultDTO.setMessage("新增异常");
            resultDTO.setMessageCode("500");
            resultDTO.setSuccess(false);
        }
        return resultDTO;
    }

    @RequestMapping(value = "/deleteEstimateLane", method = RequestMethod.POST)
    public ResultDTO deleteEstimateLane(@RequestBody List<WmsEstimateLaneVO> wmsEstimateLaneVOList) {
        ResultDTO resultDTO = new ResultDTO();
        try {
            Vo2Bo converter = new Vo2Bo();
            List<WmsEstimateLaneBO> wmsEstimateLaneBOList = converter.convert(wmsEstimateLaneVOList, WmsEstimateLaneBO.class);
            wmsEstimateLaneService.deleteEstimateLane(wmsEstimateLaneBOList);
            resultDTO.setMessage("删除装车道成功!");
            resultDTO.setSuccess(true);
            resultDTO.setMessageCode("200");
            return resultDTO;
        } catch (BeansException ex) {
            LOGGER.error("WmsEstimateLaneController.deleteEstimateLane error: {}", ex);
            resultDTO.setMessage("删除装车道失败!");
            resultDTO.setSuccess(false);
            resultDTO.setMessageCode("500");
            return resultDTO;
        }
    }
}
