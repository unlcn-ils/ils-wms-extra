package com.unlcn.ils.wms.base.mapper.stock;

import com.unlcn.ils.wms.base.model.stock.WmsVehicleTracking;
import com.unlcn.ils.wms.base.model.stock.WmsVehicleTrackingExample;
import com.unlcn.ils.wms.base.model.stock.WmsVehicleTrackingWithBLOBs;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface WmsVehicleTrackingMapper {
    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table wms_vehicle_tracking
     *
     * @mbggenerated Thu Dec 28 17:19:09 CST 2017
     */
    int countByExample(WmsVehicleTrackingExample example);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table wms_vehicle_tracking
     *
     * @mbggenerated Thu Dec 28 17:19:09 CST 2017
     */
    int deleteByExample(WmsVehicleTrackingExample example);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table wms_vehicle_tracking
     *
     * @mbggenerated Thu Dec 28 17:19:09 CST 2017
     */
    int deleteByPrimaryKey(Long id);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table wms_vehicle_tracking
     *
     * @mbggenerated Thu Dec 28 17:19:09 CST 2017
     */
    int insert(WmsVehicleTrackingWithBLOBs record);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table wms_vehicle_tracking
     *
     * @mbggenerated Thu Dec 28 17:19:09 CST 2017
     */
    int insertSelective(WmsVehicleTrackingWithBLOBs record);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table wms_vehicle_tracking
     *
     * @mbggenerated Thu Dec 28 17:19:09 CST 2017
     */
    List<WmsVehicleTrackingWithBLOBs> selectByExampleWithBLOBs(WmsVehicleTrackingExample example);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table wms_vehicle_tracking
     *
     * @mbggenerated Thu Dec 28 17:19:09 CST 2017
     */
    List<WmsVehicleTracking> selectByExample(WmsVehicleTrackingExample example);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table wms_vehicle_tracking
     *
     * @mbggenerated Thu Dec 28 17:19:09 CST 2017
     */
    WmsVehicleTrackingWithBLOBs selectByPrimaryKey(Long id);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table wms_vehicle_tracking
     *
     * @mbggenerated Thu Dec 28 17:19:09 CST 2017
     */
    int updateByExampleSelective(@Param("record") WmsVehicleTrackingWithBLOBs record, @Param("example") WmsVehicleTrackingExample example);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table wms_vehicle_tracking
     *
     * @mbggenerated Thu Dec 28 17:19:09 CST 2017
     */
    int updateByExampleWithBLOBs(@Param("record") WmsVehicleTrackingWithBLOBs record, @Param("example") WmsVehicleTrackingExample example);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table wms_vehicle_tracking
     *
     * @mbggenerated Thu Dec 28 17:19:09 CST 2017
     */
    int updateByExample(@Param("record") WmsVehicleTracking record, @Param("example") WmsVehicleTrackingExample example);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table wms_vehicle_tracking
     *
     * @mbggenerated Thu Dec 28 17:19:09 CST 2017
     */
    int updateByPrimaryKeySelective(WmsVehicleTrackingWithBLOBs record);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table wms_vehicle_tracking
     *
     * @mbggenerated Thu Dec 28 17:19:09 CST 2017
     */
    int updateByPrimaryKeyWithBLOBs(WmsVehicleTrackingWithBLOBs record);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table wms_vehicle_tracking
     *
     * @mbggenerated Thu Dec 28 17:19:09 CST 2017
     */
    int updateByPrimaryKey(WmsVehicleTracking record);
}