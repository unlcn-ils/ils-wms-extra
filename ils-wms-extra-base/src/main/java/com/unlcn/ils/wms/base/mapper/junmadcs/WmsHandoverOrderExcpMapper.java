package com.unlcn.ils.wms.base.mapper.junmadcs;

import com.unlcn.ils.wms.base.model.junmadcs.WmsHandoverOrderExcp;
import com.unlcn.ils.wms.base.model.junmadcs.WmsHandoverOrderExcpExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface WmsHandoverOrderExcpMapper {
    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table wms_handover_order_excp
     *
     * @mbggenerated Tue Jun 12 12:00:15 CST 2018
     */
    int countByExample(WmsHandoverOrderExcpExample example);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table wms_handover_order_excp
     *
     * @mbggenerated Tue Jun 12 12:00:15 CST 2018
     */
    int deleteByExample(WmsHandoverOrderExcpExample example);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table wms_handover_order_excp
     *
     * @mbggenerated Tue Jun 12 12:00:15 CST 2018
     */
    int deleteByPrimaryKey(Long id);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table wms_handover_order_excp
     *
     * @mbggenerated Tue Jun 12 12:00:15 CST 2018
     */
    int insert(WmsHandoverOrderExcp record);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table wms_handover_order_excp
     *
     * @mbggenerated Tue Jun 12 12:00:15 CST 2018
     */
    int insertSelective(WmsHandoverOrderExcp record);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table wms_handover_order_excp
     *
     * @mbggenerated Tue Jun 12 12:00:15 CST 2018
     */
    List<WmsHandoverOrderExcp> selectByExample(WmsHandoverOrderExcpExample example);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table wms_handover_order_excp
     *
     * @mbggenerated Tue Jun 12 12:00:15 CST 2018
     */
    WmsHandoverOrderExcp selectByPrimaryKey(Long id);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table wms_handover_order_excp
     *
     * @mbggenerated Tue Jun 12 12:00:15 CST 2018
     */
    int updateByExampleSelective(@Param("record") WmsHandoverOrderExcp record, @Param("example") WmsHandoverOrderExcpExample example);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table wms_handover_order_excp
     *
     * @mbggenerated Tue Jun 12 12:00:15 CST 2018
     */
    int updateByExample(@Param("record") WmsHandoverOrderExcp record, @Param("example") WmsHandoverOrderExcpExample example);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table wms_handover_order_excp
     *
     * @mbggenerated Tue Jun 12 12:00:15 CST 2018
     */
    int updateByPrimaryKeySelective(WmsHandoverOrderExcp record);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table wms_handover_order_excp
     *
     * @mbggenerated Tue Jun 12 12:00:15 CST 2018
     */
    int updateByPrimaryKey(WmsHandoverOrderExcp record);
}