package com.unlcn.ils.wms.web.vo.borrow;

import cn.huiyunche.commons.domain.PageVo;
import com.unlcn.ils.wms.base.model.stock.WmsBorrowCarDetail;

import java.util.Date;
import java.util.List;

/**
 * Created by lenovo on 2017/10/20.
 */
public class WmsBorrowCarVo extends PageVo {
    // 借车单id
    private Long bcId;

    // 借用单号
    private String bcBorrowNo;

    // 借用人姓名
    private String bcBorrowName;

    // 借用部门
    private String bcBorrowDepartment;

    // 联系电话
    private String bcBorrowTel;

    // 借用日期
    private String bcBorrowDate;

    // 借用开始时间
    private String borrowStartTime;

    // 借用截至时间
    private String borrowEndTime;

    // 预计还车日期
    private String bcEstimatedReturnDate;

    // 所属仓库
    private String bcWhcode;

    // 借用原因
    private String bcBorrowReason;

    // 是否删除
    private Integer deleteFlag;

    // 上传凭证key
    private String uploadKey;

    // 上传凭证路径
    private String uploadPath;

    List<WmsBorrowCarDetail> wmsBorrowCarDetailList;

    public Long getBcId() {
        return bcId;
    }

    public void setBcId(Long bcId) {
        this.bcId = bcId;
    }

    public String getBcBorrowNo() {
        return bcBorrowNo;
    }

    public void setBcBorrowNo(String bcBorrowNo) {
        this.bcBorrowNo = bcBorrowNo;
    }

    public String getBcBorrowName() {
        return bcBorrowName;
    }

    public void setBcBorrowName(String bcBorrowName) {
        this.bcBorrowName = bcBorrowName;
    }

    public String getBcBorrowDepartment() {
        return bcBorrowDepartment;
    }

    public void setBcBorrowDepartment(String bcBorrowDepartment) {
        this.bcBorrowDepartment = bcBorrowDepartment;
    }

    public String getBcBorrowTel() {
        return bcBorrowTel;
    }

    public void setBcBorrowTel(String bcBorrowTel) {
        this.bcBorrowTel = bcBorrowTel;
    }

    public String getBcBorrowDate() {
        return bcBorrowDate;
    }

    public void setBcBorrowDate(String bcBorrowDate) {
        this.bcBorrowDate = bcBorrowDate;
    }

    public String getBorrowStartTime() {
        return borrowStartTime;
    }

    public void setBorrowStartTime(String borrowStartTime) {
        this.borrowStartTime = borrowStartTime;
    }

    public String getBorrowEndTime() {
        return borrowEndTime;
    }

    public void setBorrowEndTime(String borrowEndTime) {
        this.borrowEndTime = borrowEndTime;
    }

    public String getBcEstimatedReturnDate() {
        return bcEstimatedReturnDate;
    }

    public void setBcEstimatedReturnDate(String bcEstimatedReturnDate) {
        this.bcEstimatedReturnDate = bcEstimatedReturnDate;
    }

    public String getBcWhcode() {
        return bcWhcode;
    }

    public void setBcWhcode(String bcWhcode) {
        this.bcWhcode = bcWhcode;
    }

    public String getBcBorrowReason() {
        return bcBorrowReason;
    }

    public void setBcBorrowReason(String bcBorrowReason) {
        this.bcBorrowReason = bcBorrowReason;
    }

    public Integer getDeleteFlag() {
        return deleteFlag;
    }

    public void setDeleteFlag(Integer deleteFlag) {
        this.deleteFlag = deleteFlag;
    }

    public String getUploadKey() {
        return uploadKey;
    }

    public void setUploadKey(String uploadKey) {
        this.uploadKey = uploadKey;
    }

    public String getUploadPath() {
        return uploadPath;
    }

    public void setUploadPath(String uploadPath) {
        this.uploadPath = uploadPath;
    }

    public List<WmsBorrowCarDetail> getWmsBorrowCarDetailList() {
        return wmsBorrowCarDetailList;
    }

    public void setWmsBorrowCarDetailList(List<WmsBorrowCarDetail> wmsBorrowCarDetailList) {
        this.wmsBorrowCarDetailList = wmsBorrowCarDetailList;
    }

    @Override
    public String toString() {
        return "WmsBorrowCarVo{" +
                "bcId=" + bcId +
                ", bcBorrowNo='" + bcBorrowNo + '\'' +
                ", bcBorrowName='" + bcBorrowName + '\'' +
                ", bcBorrowDepartment='" + bcBorrowDepartment + '\'' +
                ", bcBorrowTel='" + bcBorrowTel + '\'' +
                ", bcBorrowDate='" + bcBorrowDate + '\'' +
                ", borrowStartTime='" + borrowStartTime + '\'' +
                ", borrowEndTime='" + borrowEndTime + '\'' +
                ", bcEstimatedReturnDate='" + bcEstimatedReturnDate + '\'' +
                ", bcWhcode='" + bcWhcode + '\'' +
                ", bcBorrowReason='" + bcBorrowReason + '\'' +
                ", deleteFlag=" + deleteFlag +
                ", uploadKey='" + uploadKey + '\'' +
                ", uploadPath='" + uploadPath + '\'' +
                ", wmsBorrowCarDetailList=" + wmsBorrowCarDetailList +
                '}';
    }
}
