package com.unlcn.ils.wms.app.controller.wms;

import cn.huiyunche.commons.domain.PageVo;
import cn.huiyunche.commons.domain.ResultDTO;
import cn.huiyunche.commons.domain.ResultDTOWithPagination;
import cn.huiyunche.commons.exception.BusinessException;
import com.unlcn.ils.wms.app.vo.wms.AppOutboundTaskVO;
import com.unlcn.ils.wms.app.vo.wms.OutboundConfirmVO;
import com.unlcn.ils.wms.app.vo.wms.OutboundTaskStartVO;
import com.unlcn.ils.wms.backend.bo.outboundBO.OutboundConfirmBO;
import com.unlcn.ils.wms.backend.bo.outboundBO.OutboundTaskStartBO;
import com.unlcn.ils.wms.backend.bo.outboundBO.WmsOutboundTaskBO;
import com.unlcn.ils.wms.backend.dto.outboundDTO.CurrentWmsOutboundTaskListDTO;
import com.unlcn.ils.wms.backend.dto.outboundDTO.WmsPreparationPlanDTO;
import com.unlcn.ils.wms.backend.service.outbound.OutBoundTaskService;
import com.unlcn.ils.wms.base.dto.WmsOutboundResultDTO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;

/**
 * 备料任务处理类
 */
@Controller
@ResponseBody
@RequestMapping("/appOutboundTask")
public class AppOutboundTaskController {

    private Logger logger = LoggerFactory.getLogger(AppOutboundTaskController.class);

    @Autowired
    private OutBoundTaskService outBoundTaskService;

    /**
     * 查询出库任务(备料任务)司机抢单
     */
    @RequestMapping(value = "/getCompeteTaskList", method = RequestMethod.POST)
    public ResultDTOWithPagination<Object> getCompeteTaskList(PageVo pageVo, AppOutboundTaskVO appOutboundTaskVO) {
        logger.info("AppOutboundTaskController.getCompeteTaskList param : {}, {}", pageVo, appOutboundTaskVO);
        ResultDTOWithPagination<Object> result = new ResultDTOWithPagination<>(true, null, "查询成功", pageVo);
        try {
            WmsOutboundTaskBO wmsOutboundTaskBO = new WmsOutboundTaskBO();
            BeanUtils.copyProperties(appOutboundTaskVO, wmsOutboundTaskBO);
            List<WmsPreparationPlanDTO> list = outBoundTaskService.getOutboundTaskList(pageVo, wmsOutboundTaskBO);
            result.setData(list);
        } catch (Exception ex) {
            result.setSuccess(false);
            result.setMessage(ex.getMessage());
            logger.error("AppOutboundTaskController.getCompeteTaskList error : ", ex);
        }
        return result;
    }

    /**
     * 司机抢备料任务单
     */
    @RequestMapping(value = "/competeTask", method = RequestMethod.POST)
    public ResultDTO<Object> competeTask(Long taskId, String userId) {
        logger.info("AppOutboundTaskController.updateCompeteTask param : {}, {}", taskId, userId);
        ResultDTO<Object> result = new ResultDTO<>(true, null, "领取成功");
        try {
            WmsOutboundTaskBO wmsOutboundTaskBO = new WmsOutboundTaskBO();
            wmsOutboundTaskBO.setOtDriver(userId);
            wmsOutboundTaskBO.setOtId(taskId);
            outBoundTaskService.updateCompeteTask(wmsOutboundTaskBO);
        } catch (Exception ex) {
            result.setSuccess(false);
            result.setMessage(ex.getMessage());
            logger.error("AppOutboundTaskController.updateCompeteTask error : ", ex);
        }

        return result;
    }

    /**
     * 当前任务
     */
    @RequestMapping(value = "/getCurrentTask", method = RequestMethod.POST)
    public ResultDTO<Object> getCurrentTask(String whCode, String userId) {
        logger.info("AppOutboundTaskController.getCurrentTask param : {}", userId);
        ResultDTO<Object> result = new ResultDTO<>(true, null, "获取成功");
        try {
            WmsOutboundTaskBO wmsOutboundTaskBO = new WmsOutboundTaskBO();
            wmsOutboundTaskBO.setOtDriver(userId);
            wmsOutboundTaskBO.setOtWhCode(whCode);
            CurrentWmsOutboundTaskListDTO currentTask = outBoundTaskService.updateCurrentTask(wmsOutboundTaskBO);
            result.setData(currentTask);
        } catch (Exception ex) {
            result.setSuccess(false);
            result.setMessage(ex.getMessage());
            logger.error("AppOutboundTaskController.getCurrentTask error : ", ex);
        }

        return result;
    }

    /**
     * 历史任务
     */
    @RequestMapping(value = "/getFinishedTaskList", method = RequestMethod.POST)
    public ResultDTOWithPagination<Object> getFinishedTaskList(PageVo pageVo, AppOutboundTaskVO appOutboundTaskVO) {
        logger.info("AppOutboundTaskController.getFinishedTaskList param : {}, {}", pageVo, appOutboundTaskVO);
        ResultDTOWithPagination<Object> result = new ResultDTOWithPagination<>(true, null, "查询成功", pageVo);
        try {
            WmsOutboundTaskBO wmsOutboundTaskBO = new WmsOutboundTaskBO();
            BeanUtils.copyProperties(appOutboundTaskVO, wmsOutboundTaskBO);
            List<CurrentWmsOutboundTaskListDTO> list = outBoundTaskService.getFinishedTaskList(pageVo, wmsOutboundTaskBO);
            result.setData(list);
        } catch (Exception ex) {
            result.setSuccess(false);
            result.setMessage(ex.getMessage());
            logger.error("AppOutboundTaskController.getFinishedTaskList error : ", ex);
        }
        return result;
    }

    /**
     * 开始任务/完成任务--君马
     * <p>
     * 2018-1-18  bugFix状态校验:最后一个任务的完成时候 写入整个交接单下的任务备料确认时间
     * </p>
     *
     * @param outboundTaskStartVO 参数封装
     * @return 返回值
     */
    @RequestMapping(value = "/startOrFinishTask", method = RequestMethod.POST)
    public ResultDTO<Object> startTask(OutboundTaskStartVO outboundTaskStartVO) {
        logger.info("AppOutboundTaskController.startTask param : {}", outboundTaskStartVO);
        ResultDTO<Object> result = new ResultDTO<>(true, null, "操作成功");
        try {
            OutboundTaskStartBO outboundTaskStartBO = new OutboundTaskStartBO();
            BeanUtils.copyProperties(outboundTaskStartVO, outboundTaskStartBO);
            outBoundTaskService.saveStartOrFinishTask(outboundTaskStartBO);
        } catch (BusinessException ex) {
            result.setSuccess(false);
            result.setMessage(ex.getMessage());
            logger.error("AppOutboundTaskController.startTask error : ", ex);
        } catch (Exception ex) {
            result.setSuccess(false);
            result.setMessage("系统异常, 操作失败");
            logger.error("AppOutboundTaskController.startTask error : ", ex);
        }
        return result;
    }

    /**
     * 出库确认1.扫描纸质单据号 - 君马
     */
    @RequestMapping(value = "/fetchOrderByNo", method = RequestMethod.POST)
    public ResultDTO<Object> fetchOrderByNo(OutboundConfirmVO vo) {
        logger.info("AppOutboundTaskController.fetchOrderByNo param : {}", vo);
        ResultDTO<Object> result = new ResultDTO<>(true, null, "操作成功");
        try {
            OutboundConfirmBO bo = new OutboundConfirmBO();
            BeanUtils.copyProperties(vo, bo);
            WmsOutboundResultDTO orderMsg = outBoundTaskService.getOrderMsg(bo);
            result.setData(orderMsg);
        } catch (BusinessException ex) {
            result.setSuccess(false);
            result.setMessage(ex.getMessage());
            logger.error("AppOutboundTaskController.fetchOrderByNo error : ", ex);
        } catch (Exception ex) {
            result.setSuccess(false);
            result.setMessage("系统异常, 操作失败");
            logger.error("AppOutboundTaskController.fetchOrderByNo error : ", ex);
        }
        return result;
    }

    /**
     * 出库确认1.扫描车架号出库 - 君马
     * 君马出库闸门
     * <p>
     * 2018-1-9  新需求:扫码出库后调用君马出库闸门开启
     * </p>
     *
     * @param vo 参数封装
     * @return 返回值
     */
    @RequestMapping(value = "/updateOutboundByVin", method = RequestMethod.POST)
    public ResultDTO<Object> updateOutboundByVin(OutboundConfirmVO vo) {
        logger.info("AppOutboundTaskController.updateOutboundByVin param : {}", vo);
        ResultDTO<Object> result = new ResultDTO<>(true, null, "操作成功");
        try {
            OutboundConfirmBO bo = new OutboundConfirmBO();
            BeanUtils.copyProperties(vo, bo);
            outBoundTaskService.updateOutboundByVin(bo);
        } catch (BusinessException ex) {
            result.setSuccess(false);
            result.setMessage(ex.getMessage());
            logger.error("AppOutboundTaskController.updateOutboundByVin error : ", ex);
        } catch (Exception ex) {
            result.setSuccess(false);
            result.setMessage("系统异常, 操作失败");
            logger.error("AppOutboundTaskController.updateOutboundByVin error : ", ex);
        }
        return result;
    }
}
