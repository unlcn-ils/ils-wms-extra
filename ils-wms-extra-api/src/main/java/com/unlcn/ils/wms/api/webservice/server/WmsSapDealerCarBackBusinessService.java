package com.unlcn.ils.wms.api.webservice.server;

import com.unlcn.ils.wms.api.webservice.dto.ResultDealerCarbackSapDTO;

/**
 * 作为sap退车供sap接口调用
 */
public interface WmsSapDealerCarBackBusinessService {

    String getDealerCarbackSapMessage(ResultDealerCarbackSapDTO resultDealerCarbackSapDTO) throws Exception;
}
