package com.unlcn.ils.wms.backend.enums;

public enum WmsOutBoundTypeEnum {
    ZTYPE_Z1("Z1", "正常出库"),
    ZTYPE_Z2("Z2", "维修出库"),
    ZTYPE_Z3("Z3", "借用出库");

    private final String value;
    private final String text;

    WmsOutBoundTypeEnum(String value, String text) {
        this.value = value;
        this.text = text;
    }

    public String getValue() {
        return value;
    }

    public String getText() {
        return text;
    }

    public static WmsOutBoundTypeEnum getByValue(String value) {
        for (WmsOutBoundTypeEnum temp : WmsOutBoundTypeEnum.values()) {
            if (temp.getValue().equals(value)) {
                return temp;
            }
        }
        return null;
    }

}
