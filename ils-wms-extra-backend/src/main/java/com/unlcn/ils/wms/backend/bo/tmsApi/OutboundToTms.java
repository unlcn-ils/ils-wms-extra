package com.unlcn.ils.wms.backend.bo.tmsApi;

/**
 * Created by DELL on 2017/9/7.
 */
public class OutboundToTms {
    /**
     * TMS任务单号
     */
    private String taskNo;

    /**
     * 单据类型
     * 10：入库单
     * 20：出库单
     */
    private String billType;

    /**
     * 状态
     * 10：已入库
     * 20：已出库
     * 30：入库异常
     * 40：出库异常
     */
    private String status;

    /**
     * 备注信息
     */
    private String remark;

    public String getTaskNo() {
        return taskNo;
    }

    public void setTaskNo(String taskNo) {
        this.taskNo = taskNo;
    }

    public String getBillType() {
        return billType;
    }

    public void setBillType(String billType) {
        this.billType = billType;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    @Override
    public String toString() {
        return "OutboundToTms{" +
                "taskNo='" + taskNo + '\'' +
                ", billType='" + billType + '\'' +
                ", status='" + status + '\'' +
                ", remark='" + remark + '\'' +
                '}';
    }
}
