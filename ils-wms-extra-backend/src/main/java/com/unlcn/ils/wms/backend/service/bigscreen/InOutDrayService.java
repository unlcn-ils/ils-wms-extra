package com.unlcn.ils.wms.backend.service.bigscreen;

import com.unlcn.ils.wms.backend.dto.bigscreenDTO.OutDrayChartDataDTO;
import com.unlcn.ils.wms.base.dto.BiInOutDrayDTO;
import com.unlcn.ils.wms.base.model.bigscreen.BiInOutDray;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by lenovo on 2017/11/8.
 */
public interface InOutDrayService {

    /**
     * 板车出入计划图统计
     * @return
     */
    List<OutDrayChartDataDTO> inOutDrayChartCount();

    /**
     * 板车出入计划列表统计
     * @return
     */
    List<BiInOutDray> inOutDrayLineCount();

    /**
     * 板车出入统计图信息
     */
    void addDrayChart();


    /**
     * 板车出入统计列表s
     */
    void addDrayLineInfo();

    ArrayList<BiInOutDrayDTO> inOutDrayLineCountNew() throws Exception;

    void addDrayLineInfoNew() throws Exception;
}
