package com.unlcn.ils.wms.backend.enums;

/**
 * 维修单类型
 * Created by DELL on 2017/8/24.
 */
public enum WmsRepairInAndOutTypeEnum {
    INBOUND_REPAIR(10,"入库异常维修"),
    OUTBOUND_REPAIR(20,"出库异常维修");

    private final int value;
    private final String text;
    WmsRepairInAndOutTypeEnum(int value, String text){
        this.value = value;
        this.text = text;
    }

    public int getValue() {
        return value;
    }

    public String getText() {
        return text;
    }

    public static WmsRepairInAndOutTypeEnum getByValue(int value){
        for (WmsRepairInAndOutTypeEnum temp : WmsRepairInAndOutTypeEnum.values()) {
            if(temp.getValue() == value){
                return temp;
            }
        }
        return null;
    }
}
