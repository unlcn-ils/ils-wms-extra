package com.unlcn.ils.wms.backend.dto.outboundDTO;

/**
 * @Auther linbao
 * @Date 2017-10-23
 */
public class HandoverDetailDTO {

    /**
     * 品牌
     */
    private String brand;

    /**
     * 车架号
     */
    private String vin;

    /**
     * 发动机编码
     */
    private String engineNo;

    /**
     * 车型代码
     */
    private String styleCode;

    /**
     * 车型描述
     */
    private String styleDesc;

    /**
     * 颜色
     */
    private String color;

    /**
     * 签收
     */
    private String sign;

    public String getBrand() {
        return brand;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    public String getVin() {
        return vin;
    }

    public void setVin(String vin) {
        this.vin = vin;
    }

    public String getEngineNo() {
        return engineNo;
    }

    public void setEngineNo(String engineNo) {
        this.engineNo = engineNo;
    }

    public String getStyleCode() {
        return styleCode;
    }

    public void setStyleCode(String styleCode) {
        this.styleCode = styleCode;
    }

    public String getStyleDesc() {
        return styleDesc;
    }

    public void setStyleDesc(String styleDesc) {
        this.styleDesc = styleDesc;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public String getSign() {
        return sign;
    }

    public void setSign(String sign) {
        this.sign = sign;
    }
}
