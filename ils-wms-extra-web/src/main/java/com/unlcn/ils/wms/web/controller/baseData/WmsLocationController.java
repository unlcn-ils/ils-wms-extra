package com.unlcn.ils.wms.web.controller.baseData;

import cn.huiyunche.commons.domain.ResultDTO;
import cn.huiyunche.commons.exception.BusinessException;
import com.unlcn.ils.wms.backend.bo.baseDataBO.WmsLocationBO;
import com.unlcn.ils.wms.backend.service.baseData.WmsLocationService;
import com.unlcn.ils.wms.base.businessDTO.baseData.WmsLocationQueryDTO;
import com.unlcn.ils.wms.web.utils.Vo2Bo;
import com.unlcn.ils.wms.web.vo.baseData.WmsLocationVO;
import jodd.util.StringUtil;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.List;
import java.util.Objects;

/**
 * wms库位管理
 * Created by DELL on 2017/8/7.
 */
@Controller
@RequestMapping("/wmsLocation")
@ResponseBody
public class WmsLocationController {
    private Logger LOGGER = LoggerFactory.getLogger(WmsLocationController.class);
    @Autowired
    private WmsLocationService wmsLocationService;

    @RequestMapping(value = "/list", method = RequestMethod.POST)
    public ResultDTO list(@RequestBody WmsLocationVO wmsLocationVO, HttpServletRequest request) throws BusinessException {
        LOGGER.info("WmsLocationController.getLocationList param:{}.", wmsLocationVO);
        ResultDTO<Object> resultDTO = new ResultDTO<Object>(true, null, "查询成功");
        try {
            WmsLocationQueryDTO wmsLocationQueryDTO = new WmsLocationQueryDTO();
            BeanUtils.copyProperties(wmsLocationVO, wmsLocationQueryDTO);
            resultDTO.setMessage("查询成功");
            resultDTO.setMessageCode("200");
            resultDTO.setSuccess(true);
            String whCode = request.getHeader("whCode");
            if (StringUtils.isBlank(whCode)){
                return resultDTO;
            }
            wmsLocationQueryDTO.setLocWhCode(whCode);
            resultDTO.setData(wmsLocationService.listLocation(wmsLocationQueryDTO));
        } catch (Exception ex) {
            LOGGER.error("WmsLocationController.list error: {}", ex);
            resultDTO.setMessage("查询异常");
            resultDTO.setMessageCode("500");
            resultDTO.setSuccess(false);
        }
        return resultDTO;
    }

    /**
     * 新增库区
     * @param wmsLocationVO
     * @return
     */
    @RequestMapping(value="/addLocation",method = RequestMethod.POST)
    public ResultDTO addLocation(@RequestBody WmsLocationVO wmsLocationVO){
        ResultDTO resultDTO = new ResultDTO();
        try {
            wmsLocationVO.setIsDeleted((byte)0);
            WmsLocationBO wmsLocationBO = new WmsLocationBO();
            BeanUtils.copyProperties(wmsLocationVO, wmsLocationBO);
            boolean result = wmsLocationService.addLocation(wmsLocationBO);
            if(!result){
                resultDTO.setMessage("库位代码重复");
                resultDTO.setSuccess(false);
                resultDTO.setMessageCode("500");
            }else{
                resultDTO.setMessage("新增库区成功!");
                resultDTO.setSuccess(true);
                resultDTO.setMessageCode("200");
            }
            return resultDTO;
        } catch (BeansException ex) {
            LOGGER.error("WmsLocationController.addLocation error: {}", ex);
            resultDTO.setMessage("新增库区失败！");
            resultDTO.setSuccess(false);
            resultDTO.setMessageCode("500");
            return resultDTO;
        }
    }

    /**
     * 根据ID查询库位数据
     * @param locId
     * @return
     */
    @RequestMapping(value="/getLocationById/{locId}")
    public ResultDTO getLocationById(@PathVariable String locId){
        ResultDTO resultDTO = new ResultDTO();
        if(StringUtil.isEmpty(locId)){
            LOGGER.error("WmsLocationController.getLocationById 请求参数为空");
        }
        try{
            WmsLocationBO wmsLocationBO = wmsLocationService.getLocationById(locId);
            resultDTO.setData(wmsLocationBO);
            resultDTO.setMessage("库位数据成功!");
            resultDTO.setSuccess(true);
            resultDTO.setMessageCode("200");
            return resultDTO;
        }catch(BusinessException ex){
            LOGGER.error("WmsLocationController.getLocationById error: {}", ex);
            resultDTO.setMessage("库位数据成功!");
            resultDTO.setSuccess(false);
            resultDTO.setMessageCode("500");
            return resultDTO;
        }
    }

    /**
     * 修改库区
     * todo 修改需要判断此库位是否为在用
     * @param wmsLocationVO
     * @return
     */
    @RequestMapping(value="/updateLocation",method = RequestMethod.POST)
    public ResultDTO updateLocation(@RequestBody WmsLocationVO wmsLocationVO){
        ResultDTO resultDTO = new ResultDTO();
        if(Objects.isNull(wmsLocationVO)){
            LOGGER.error("WmsLocationController.updateLocation 请求参数为空");
        }
        try {
            WmsLocationBO wmsLocationBO = new WmsLocationBO();
            BeanUtils.copyProperties(wmsLocationVO, wmsLocationBO);
            wmsLocationService.updateLocation(wmsLocationBO);
            resultDTO.setMessage("新增库区成功!  ");
            resultDTO.setSuccess(true);
            resultDTO.setMessageCode("200");
            return resultDTO;
        } catch (BeansException ex) {
            LOGGER.error("WmsLocationController.updateLocation error: {}", ex);
            resultDTO.setMessage("新增库区失败！");
            resultDTO.setSuccess(false);
            resultDTO.setMessageCode("500");
            return resultDTO;
        }
    }

    /**
     * 删除库区
     * todo 删除需要判断是否此库位在用
     * @param wmsLocationVOList
     * @return
     */
    @RequestMapping(value="/enableLocation/{locEnableFlag}",method = RequestMethod.POST)
    public ResultDTO enableLocation(@RequestBody List<WmsLocationVO> wmsLocationVOList,
                                    @PathVariable("locEnableFlag") String locEnableFlag){
        ResultDTO resultDTO = new ResultDTO();
        if(CollectionUtils.isEmpty(wmsLocationVOList)){
            LOGGER.error("WmsLocationController.deleteLocation 请求参数为空");
        }
        try {
            Vo2Bo converter = new Vo2Bo();
            List<WmsLocationBO> wmsLocationBOList = converter.convert(wmsLocationVOList, WmsLocationBO.class);
            wmsLocationService.enableLocation(wmsLocationBOList,locEnableFlag);
            resultDTO.setMessage("Y".equals(wmsLocationVOList.get(0).getLocEnableFlag())?"启用库位成功!":"禁用库位成功!");
            resultDTO.setSuccess(true);
            resultDTO.setMessageCode("200");
            return resultDTO;
        } catch (BeansException ex) {
            LOGGER.error("WmsLocationController.enableLocation error: {}", ex);
            resultDTO.setMessage("Y".equals(wmsLocationVOList.get(0).getLocEnableFlag())?"启用库位失败!":"禁用库位失败!");;
            resultDTO.setSuccess(false);
            resultDTO.setMessageCode("500");
            return resultDTO;
        }
    }

    /**
     * 删除库区
     * todo 删除需要判断是否此库位在用
     * @param wmsLocationVOList
     * @return
     */
    @RequestMapping(value="/deleteLocation",method = RequestMethod.POST)
    public ResultDTO deleteLocation(@RequestBody List<WmsLocationVO> wmsLocationVOList){
        ResultDTO resultDTO = new ResultDTO();
        if(CollectionUtils.isEmpty(wmsLocationVOList)){
            LOGGER.error("WmsLocationController.deleteLocation 请求参数为空");
        }
        try {
            Vo2Bo converter = new Vo2Bo();
            List<WmsLocationBO> wmsLocationBOList = converter.convert(wmsLocationVOList, WmsLocationBO.class);
            wmsLocationService.deleteLocation(wmsLocationBOList);
            resultDTO.setMessage("删除库区成功!");
            resultDTO.setSuccess(true);
            resultDTO.setMessageCode("200");
            return resultDTO;
        } catch (BeansException ex) {
            LOGGER.error("WmsLocationController.deleteLocation error: {}", ex);
            resultDTO.setMessage("删除库区失败！");
            resultDTO.setSuccess(false);
            resultDTO.setMessageCode("500");
            return resultDTO;
        }
    }
}
