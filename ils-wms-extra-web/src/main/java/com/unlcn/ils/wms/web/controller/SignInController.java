package com.unlcn.ils.wms.web.controller;

import cn.huiyunche.commons.domain.ResultDTO;
import cn.huiyunche.commons.security.JwtAuthenicationFilter;
import cn.huiyunche.commons.security.JwtUtils;
import cn.huiyunche.commons.security.domains.UserSecurityVO;
import cn.huiyunche.commons.utils.ListUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;
import java.util.List;

/**
 * @author qichao
 */
@Controller
@RequestMapping("/signin")
public class SignInController {
    private Logger LOGGER = LoggerFactory.getLogger(SignInController.class);

    @Value("#{configProperties['secure.key']}")
    private String secKey;

    public String getSecKey() {
        return secKey;
    }



    @RequestMapping
    public String exectue(){
        return "signin";
    }


    /**
     * @FIXME 临时解决方案
     * web页表单（用户名+密码）方式登入
     * @param request
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/form", method = RequestMethod.POST)
    @ResponseBody
    public ResultDTO<Object> UsernamePasswordWebFormLogin(HttpServletRequest request, String username, String password) throws Exception {
        LOGGER.info("SignInController.form param : {}, {}", username,password);
        ResultDTO<Object> signin_result = new ResultDTO<>(true, "登录成功!");
        HashMap<String, Object> map = new HashMap<>();
        /*List<KasAccount> accountList = this.accountService.getAccountByLoginNameAndPassword(username,password);
        if(ListUtil.isNotEmpty(accountList)){
            UserSecurityVO usv = new UserSecurityVO(username,password);
            String token = JwtUtils.generateToken(usv, this.getSecKey());
            map.put(JwtAuthenicationFilter.HEADER_AUTHORIZATION, JwtAuthenicationFilter.PREFIX_AUTHORIZATION + token);
            map.put("userName", usv.getUsername());
            signin_result.setData(map);
        }else{
            signin_result.setSuccess(false);
            signin_result.setMessage("登录失败！");
        }*/
        return signin_result;
    }

}
