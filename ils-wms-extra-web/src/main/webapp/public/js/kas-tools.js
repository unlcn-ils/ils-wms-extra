/**
 * Created by houjianhui on 2017/7/19.
 */
'use strict';
wmsService.service('Tools',['ngDialog','constant','$timeout',function(ngDialog,constant,timeout){

    var self = this;

    return {
        dialog : function(settings){    //弹出层
            return ngDialog.open(settings);
        },
        dialogConfirm : function(msg){ //确认弹出层
            return ngDialog.openConfirm({
                closeByDocument : false,
                closeByEscape : false,
                template :  '../public/template/dialog/dialog-confirm.html',
                width : 500,
                title : '确认提示',
                data : {
                    msg : msg
                }
            });
        },
        dropDown : {    //下拉数据
            billsTypeDropDown : [   //单据类型下拉
                {
                    name:constant.billsTypeName.systemSchedul,  //系统调度
                    value:constant.billsType.systemSchedul
                },
                {
                    name:constant.billsTypeName.manualEntry,  //手工录入
                    value:constant.billsType.manualEntry
                }
            ],
            orderStatusDropDown : [ //订单状态下拉
                {
                    name : constant.orderStatusName.wmsInboundCreate,   //未收货
                    value : constant.orderStatus.wmsInboundCreate
                },
                {
                    name : constant.orderStatusName.wmsInboundWaitAllocation,   //已收货
                    value : constant.orderStatus.wmsInboundWaitAllocation
                },
                {
                    name : constant.orderStatusName.wmsInboundFinishAllocation, //已分配
                    value : constant.orderStatus.wmsInboundFinishAllocation
                },
                {
                    name : constant.orderStatusName.wmsInboundConfirmAllocation,    //待入库
                    value : constant.orderStatus.wmsInboundConfirmAllocation
                },
                {
                    name : constant.orderStatusName.wmsInboundFinish,   //已入库
                    value : constant.orderStatus.wmsInboundFinish
                }
            ],
            inspectionResultsDropDown : [   //检验结果下拉
                {
                    name : constant.inspectionResultsName.asnOrderPass, //合格
                    value : constant.inspectionResults.asnOrderPass
                },
                {
                    name : constant.inspectionResultsName.asnOrderNotpassInRepair,  //不合格-库内返修
                    value : constant.inspectionResults.asnOrderNotpassInRepair
                },
                {
                    name : constant.inspectionResultsName.asnOrderNotpassOutRepair, //不合格-委外返工
                    value : constant.inspectionResults.asnOrderNotpassOutRepair
                },
                {
                    name :  constant.inspectionResultsName.asnOrderNotpass, //不合格-退厂
                    value : constant.inspectionResults.asnOrderNotpass
                }
            ],
            statusDropDown : [  //状态下拉
                {
                    name : constant.statusName.enable,  //可用
                    value : constant.status.enable
                },
                {
                    name : constant.statusName.disable, //不可用
                    value : constant.status.disable
                }
            ],
            repairTypeDropDown : [  //维修类型状态
                {
                    name : constant.repairTypeName.inner,   //委内维修
                    value : constant.repairType.inner
                },
                {
                    name : constant.repairTypeName.outer,   //委外维修
                    value : constant.repairType.outer
                }
            ],
            repairStatusDropDown : [    //维修状态
                {
                    name : constant.repairStatusName.unprocessed,   //未处理
                    value : constant.repairStatus.unprocessed
                },
                {
                    name : constant.repairStatusName.maintenance,   //维修中
                    value : constant.repairStatus.maintenance
                },
                {
                    name : constant.repairStatusName.finish,   //已完成
                    value : constant.repairStatus.finish
                }
            ],
            repairResultDropDown : [    //维修结果
                {
                    name : constant.repairResultName.qualified,   //合格
                    value : constant.repairResult.qualified
                },
                {
                    name : constant.repairResultName.disqualification,   //不合格
                    value : constant.repairResult.disqualification
                },
                {
                    name : constant.repairResultName.cancellinStocks,   //退库
                    value : constant.repairResult.cancellinStocks
                }
            ],
            warehouseDropDown : [    //出库状态
                {
                    name : constant.warehouseName.unprocessed,   //未处理
                    value : constant.warehouse.unprocessed
                },
                {
                    name : constant.warehouseName.material,   //已备料
                    value : constant.warehouse.material
                },
                {
                    name : constant.warehouseName.outbound,   //待出库
                    value : constant.warehouse.outbound
                },
                {
                    name : constant.warehouseName.haveOutbound,   //已出库
                    value : constant.warehouse.haveOutbound
                }
            ],
            stockUnitStatusDropDown : [   //库位状态下拉
                {
                    name : constant.stockUnitStatusName.start,   //启用
                    value : constant.stockUnitStatus.start
                },
                {
                    name : constant.stockUnitStatusName.stop,    //停用
                    value : constant.stockUnitStatus.stop
                }
            ],
            strategyConditionTypeDropDown : [    //策略条件类型
                {
                    name : constant.strategyConditionTypeName.shipper,   //货主
                    value : constant.strategyConditionType.shipper
                },
                {
                    name : constant.strategyConditionTypeName.vehicle,    //车型
                    value : constant.strategyConditionType.vehicle
                }
            ],
            strategyOperatorDropDown : [    //策略运算符
                {
                    name : constant.strategyOperator.xd,    //相等
                    value : constant.strategyOperator.xd
                },
                {
                    name : constant.strategyOperator.bdy,    //不相等
                    value : constant.strategyOperator.bdy
                },
                {
                    name : constant.strategyOperator.xy,    //小于
                    value : constant.strategyOperator.xy
                },
                {
                    name : constant.strategyOperator.dy,    //大于
                    value : constant.strategyOperator.dy
                },
                {
                    name : constant.strategyOperator.xydy,    //小于等于
                    value : constant.strategyOperator.xydy
                },
                {
                    name : constant.strategyOperator.dydy,    //大于等于
                    value : constant.strategyOperator.dydy
                },
                {
                    name : constant.strategyOperator.bh,    //包含
                    value : constant.strategyOperator.bh
                },
                {
                    name : constant.strategyOperator.bbh,    //不包含
                    value : constant.strategyOperator.bbh
                },
                {
                    name : constant.strategyOperator.s,    //是
                    value : constant.strategyOperator.s
                },
                {
                    name : constant.strategyOperator.bs,    //不是
                    value : constant.strategyOperator.bs
                }
            ]
        },
        modelAssignment : function(valueObj,scope){  //给model赋值
            var ngModel = null;
            var value = valueObj;
            if(!(valueObj instanceof Object)){ //不是对象
                value = valueObj;
                ngModel = this.elem.attributes['ng-model'].value;
                scope = this.scope;
            }else{
                ngModel = valueObj.currentTarget.attributes['ng-model'].value
                value = valueObj.currentTarget.value;
            }

            if(ngModel){
                var arrObj = ngModel.split('.');
                if(arrObj.length == 1){
                    scope[arrObj[0]] = value;
                }else if(arrObj.length == 2){
                    if(!scope[arrObj[0]]){
                        scope[arrObj[0]] = {};
                    }
                    scope[arrObj[0]][arrObj[1]] = value;
                }else if(arrObj.length == 3){
                    scope[arrObj[0]][arrObj[1]][arrObj[2]] = value;
                }else if(arrObj.length == 4){
                    scope[arrObj[0]][arrObj[1]][arrObj[2]][arrObj[3]] = value;
                }else if(arrObj.length == 5){
                    scope[arrObj[0]][arrObj[1]][arrObj[2]][arrObj[3]][arrObj[4]] = value;
                }
            }
        },
        alert : function(msg){  //普通提示框
            if(msg){
                layer.msg(msg);
            }
        },
        page : function(element,settings,callback){ //分页
            if(!settings){
                settings = {};
            }
            settings.elem = element ? $(element) : $(settings.elem);
            settings.jump = settings.jump ? settings.jump : callback;
            settings.theme = '#de531a';
            settings.last = settings.last ? settings.last : 10;
            settings.curr = settings.curr ? settings.curr : 0;
            settings.prev = '<';
            settings.next = '>';
            settings.skip = true;
            settings.layout = ['prev', 'page', 'next','count', 'limit','skip'];
            layui.use(['laypage'],function(){
                var laypage = layui.laypage;
                laypage.render(settings);
            })
        },
        initCheckBox : function(){  //初始化多选
        	if(layui.form){
        		var form = layui.form();
                timeout(function(){
                    form.render();
                    form.on('checkbox(allChoose)', function(data){
                        var child = $(data.elem).parents('table').find('tbody input[type="checkbox"]');
                        child.each(function(index, item){
                            item.checked = data.elem.checked;
                        });
                        form.render('checkbox');
                    });
                },500);
        	}else{
        		layui.use(['form'], function () {
            		var form = layui.form();
                    timeout(function(){
                        form.render();
                        form.on('checkbox(allChoose)', function(data){
                            var child = $(data.elem).parents('table').find('tbody input[type="checkbox"]');
                            child.each(function(index, item){
                                item.checked = data.elem.checked;
                            });
                            form.render('checkbox');
                        });
                    },500);
                });
        	}
        },
        getTableSelectItemIndex : function(selector){    //获取表格选中的checkbox index
            var child = $(selector).find('tbody input[type="checkbox"][name=ckValue]');
            var indexArray = new Array();
            child.each(function(index, item){
                if(item.checked){   //判断是否选中
                    indexArray.push(item.value | index);    //获取index
                }
            });
            return indexArray;
        },
        getDataTOIndex : function(array,data){  //根据下标取出相对应的数据
            var dataArry = new Array();
            for(var i = 0;i < array.length;i++){
                dataArry.push(data[array[i]]);
            }
            return dataArry;
        },
        getBillsTypeDropDownByKey : function(key){ //根据key获取单据类型值
            for(var i = 0; i < this.dropDown.billsTypeDropDown.length;i++){
                if(this.dropDown.billsTypeDropDown[i].value == key){
                    return this.dropDown.billsTypeDropDown[i];
                }
            }
        },
        getRepairTypeDropDownByKey : function(key){    //根据key获取维修类型值
            for(var i = 0; i < this.dropDown.repairTypeDropDown.length;i++){
                if(this.dropDown.repairTypeDropDown[i].value == key){
                    return this.dropDown.repairTypeDropDown[i];
                }
            }
        },
        getInspectionResultsDropDownByKey : function(key){  //根据key获取检验结果
            for(var i = 0; i < this.dropDown.inspectionResultsDropDown.length;i++){
                if(this.dropDown.inspectionResultsDropDown[i].value == key){
                    return this.dropDown.inspectionResultsDropDown[i];
                }
            }
        },
        getRepairResultDropDownByKey : function(key){   //根据key获取维修结果
            for(var i = 0; i < this.dropDown.repairResultDropDown.length;i++){
                if(this.dropDown.repairResultDropDown[i].value == key){
                    return this.dropDown.repairResultDropDown[i];
                }
            }
        }
    };
}])