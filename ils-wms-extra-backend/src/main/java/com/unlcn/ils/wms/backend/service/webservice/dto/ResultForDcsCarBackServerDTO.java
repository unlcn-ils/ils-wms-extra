package com.unlcn.ils.wms.backend.service.webservice.dto;

import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;

@XmlRootElement
public class ResultForDcsCarBackServerDTO implements Serializable {
    private ResultForDcsCarBackServerBodyDTO bodyDTO;
    private ResultSapHeaderDTO headerDTO;

    public ResultForDcsCarBackServerDTO(ResultForDcsCarBackServerBodyDTO bodyDTO, ResultSapHeaderDTO headerDTO) {
        this.bodyDTO = bodyDTO;
        this.headerDTO = headerDTO;
    }

    public ResultForDcsCarBackServerDTO() {
        super();
    }

    public ResultForDcsCarBackServerBodyDTO getBodyDTO() {
        return bodyDTO;
    }

    public void setBodyDTO(ResultForDcsCarBackServerBodyDTO bodyDTO) {
        this.bodyDTO = bodyDTO;
    }

    public ResultSapHeaderDTO getHeaderDTO() {
        return headerDTO;
    }

    public void setHeaderDTO(ResultSapHeaderDTO headerDTO) {
        this.headerDTO = headerDTO;
    }
}
