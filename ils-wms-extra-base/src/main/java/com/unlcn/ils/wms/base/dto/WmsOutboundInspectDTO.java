package com.unlcn.ils.wms.base.dto;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

public class WmsOutboundInspectDTO implements Serializable {
    private Long id;
    private String vin;
    private String vehicle;
    private String waybillCode;
    private Date generateTime;
    private int inspectStatusValue;
    private String inspectStatusText;
    private String inspectUserId;
    private Date inspectTime;
    private Integer preparationStatus;
    private Integer outboundStatus;
    private String outboundStatusText;
    private String whcode;
    private List<WmsOutboundInspectExcpDTO> positionAndExcpCount;
    private String missComponentNames;
    private String otType;

    public String getOtType() {
        return otType;
    }

    public void setOtType(String otType) {
        this.otType = otType;
    }

    public String getWhcode() {
        return whcode;
    }

    public void setWhcode(String whcode) {
        this.whcode = whcode;
    }

    public String getMissComponentNames() {
        return missComponentNames;
    }

    public void setMissComponentNames(String missComponentNames) {
        this.missComponentNames = missComponentNames;
    }

    public String getOutboundStatusText() {
        return outboundStatusText;
    }

    public void setOutboundStatusText(String outboundStatusText) {
        this.outboundStatusText = outboundStatusText;
    }

    public List<WmsOutboundInspectExcpDTO> getPositionAndExcpCount() {
        return positionAndExcpCount;
    }

    public void setPositionAndExcpCount(List<WmsOutboundInspectExcpDTO> positionAndExcpCount) {
        this.positionAndExcpCount = positionAndExcpCount;
    }

    public Integer getOutboundStatus() {
        return outboundStatus;
    }

    public void setOutboundStatus(Integer outboundStatus) {
        this.outboundStatus = outboundStatus;
    }

    public Integer getPreparationStatus() {
        return preparationStatus;
    }

    public void setPreparationStatus(Integer preparationStatus) {
        this.preparationStatus = preparationStatus;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getVin() {
        return vin;
    }

    public void setVin(String vin) {
        this.vin = vin;
    }

    public String getVehicle() {
        return vehicle;
    }

    public void setVehicle(String vehicle) {
        this.vehicle = vehicle;
    }

    public String getWaybillCode() {
        return waybillCode;
    }

    public void setWaybillCode(String waybillCode) {
        this.waybillCode = waybillCode;
    }

    public Date getGenerateTime() {
        return generateTime;
    }

    public void setGenerateTime(Date generateTime) {
        this.generateTime = generateTime;
    }

    public int getInspectStatusValue() {
        return inspectStatusValue;
    }

    public void setInspectStatusValue(int inspectStatusValue) {
        this.inspectStatusValue = inspectStatusValue;
    }

    public String getInspectStatusText() {
        return inspectStatusText;
    }

    public void setInspectStatusText(String inspectStatusText) {
        this.inspectStatusText = inspectStatusText;
    }

    public String getInspectUserId() {
        return inspectUserId;
    }

    public void setInspectUserId(String inspectUserId) {
        this.inspectUserId = inspectUserId;
    }

    public Date getInspectTime() {
        return inspectTime;
    }

    public void setInspectTime(Date inspectTime) {
        this.inspectTime = inspectTime;
    }
}
