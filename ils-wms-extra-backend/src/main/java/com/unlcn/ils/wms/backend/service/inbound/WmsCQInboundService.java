package com.unlcn.ils.wms.backend.service.inbound;

import com.unlcn.ils.wms.base.dto.WmsCqInboundTmsDTO;
import com.unlcn.ils.wms.base.model.outbound.WmsDepartureRegister;
import com.unlcn.ils.wms.base.model.sys.SysUser;

import java.util.HashMap;

public interface WmsCQInboundService {

    //调用tms接口查询入库指令,对接道闸接口及业务
    HashMap<String, Object> saveInboundDetailByVehicleFromTms(String vehicle, String oldVehicle, String username);

    void addInboundOpenByHm(WmsDepartureRegister departureRegister, WmsCqInboundTmsDTO dto, SysUser sysUser, String whCode) throws Exception;

}
