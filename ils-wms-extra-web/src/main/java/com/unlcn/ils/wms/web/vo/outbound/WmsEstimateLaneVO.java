package com.unlcn.ils.wms.web.vo.outbound;

/**
 * 装车道
 * Created by DELL on 2017/10/13.
 */
public class WmsEstimateLaneVO {
    /**
     *
     */
    private Long elId;

    /**
     * 仓库CODE
     */
    private String elWhCode;

    /**
     * 仓库NAME
     */
    private String elWhName;

    /**
     * 装车道CODE
     */
    private String elCode;

    /**
     * 装车道NAME
     */
    private String elName;

    /**
     * 数量
     */
    private Long elNumber;

    /**
     * 逻辑删除
     */
    private Byte isDeleted;

    public Long getElId() {
        return elId;
    }

    public void setElId(Long elId) {
        this.elId = elId;
    }

    public String getElWhCode() {
        return elWhCode;
    }

    public void setElWhCode(String elWhCode) {
        this.elWhCode = elWhCode;
    }

    public String getElWhName() {
        return elWhName;
    }

    public void setElWhName(String elWhName) {
        this.elWhName = elWhName;
    }

    public String getElCode() {
        return elCode;
    }

    public void setElCode(String elCode) {
        this.elCode = elCode;
    }

    public String getElName() {
        return elName;
    }

    public void setElName(String elName) {
        this.elName = elName;
    }

    public Long getElNumber() {
        return elNumber;
    }

    public void setElNumber(Long elNumber) {
        this.elNumber = elNumber;
    }

    public Byte getIsDeleted() {
        return isDeleted;
    }

    public void setIsDeleted(Byte isDeleted) {
        this.isDeleted = isDeleted;
    }

    @Override
    public String toString() {
        return "WmsEstimateLaneVO{" +
                "elId=" + elId +
                ", elWhCode='" + elWhCode + '\'' +
                ", elWhName='" + elWhName + '\'' +
                ", elCode='" + elCode + '\'' +
                ", elName='" + elName + '\'' +
                ", elNumber=" + elNumber +
                ", isDeleted=" + isDeleted +
                '}';
    }
}
