package com.unlcn.ils.wms.backend.service.webservice.bo;

import java.io.Serializable;

public class XmlInboundReturnMsgBO implements Serializable {
    private String VIN;
    private String eMsg;

    public String getVIN() {
        return VIN;
    }

    public void setVIN(String VIN) {
        this.VIN = VIN;
    }

    public String geteMsg() {
        return eMsg;
    }

    public void seteMsg(String eMsg) {
        this.eMsg = eMsg;
    }
}
