package com.unlcn.ils.wms.backend.enums;

public enum StatusEnum {

    RIGHT("10", "可用"),
    DELETE("20", "删除");

    private final String value;
    private final String text;

    StatusEnum(String value, String text) {
        this.value = value;
        this.text = text;
    }

    /**
     * @return the value
     */
    public String getValue() {
        return value;
    }

    /**
     * @return the text
     */
    public String getText() {
        return text;
    }

    /**
     * @param @param  value
     * @param @return
     * @return StatusEnum    返回类型
     * @throws
     * @Title: getByValue
     * @Description: 获取值
     */
    public static StatusEnum getByValue(String value) {
        for (StatusEnum temp : StatusEnum.values()) {
            if (temp.getValue().equals(value)) {
                return temp;
            }
        }
        return null;
    }
}
