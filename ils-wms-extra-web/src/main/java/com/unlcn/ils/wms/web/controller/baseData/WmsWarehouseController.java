package com.unlcn.ils.wms.web.controller.baseData;

import cn.huiyunche.commons.domain.ResultDTO;
import com.unlcn.ils.wms.backend.bo.baseDataBO.WmsWarehouseBO;
import com.unlcn.ils.wms.backend.service.baseData.WmsWarehouseService;
import com.unlcn.ils.wms.base.businessDTO.baseData.WmsWarehouseQueryDTO;
import com.unlcn.ils.wms.web.utils.Vo2Bo;
import com.unlcn.ils.wms.web.vo.baseData.WmsWarehouseVO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * 仓库增删改
 * Created by DELL on 2017/9/20.
 */
@Controller
@RequestMapping("/wh")
@ResponseBody
public class WmsWarehouseController {
    private Logger LOGGER = LoggerFactory.getLogger(WmsWarehouseController.class);

    @Autowired
    WmsWarehouseService wmsWarehouseService;

    /**
     * 查询仓库分页
     * @param wmsWarehouseVO
     * @return
     */
    @RequestMapping(value = "/getWarehouseList",method = RequestMethod.POST)
    public ResultDTO getWarehouseList(@RequestBody WmsWarehouseVO wmsWarehouseVO){
        ResultDTO resultDTO = new ResultDTO();
        try{
            WmsWarehouseQueryDTO wmsWarehouseQueryDTO = new WmsWarehouseQueryDTO();
            BeanUtils.copyProperties(wmsWarehouseVO, wmsWarehouseQueryDTO);
            resultDTO.setMessage("查询成功");
            resultDTO.setMessageCode("200");
            resultDTO.setSuccess(true);
            resultDTO.setData(wmsWarehouseService.getWarehouseList(wmsWarehouseQueryDTO));
            return resultDTO;
        }catch(Exception ex){
            resultDTO.setMessage("查询异常");
            resultDTO.setMessageCode("500");
            resultDTO.setSuccess(false);
            return resultDTO;
        }
    }

    /**
     * 新增仓库
     * @param wmsWarehouseVO
     * @return
     */
    @RequestMapping(value = "/addWarehouse",method = RequestMethod.POST)
    public ResultDTO addWarehouse(@RequestBody WmsWarehouseVO wmsWarehouseVO){
        ResultDTO resultDTO = new ResultDTO();
        try{
            WmsWarehouseBO wmsWarehouseBO = new WmsWarehouseBO();
            BeanUtils.copyProperties(wmsWarehouseVO,wmsWarehouseBO);
            wmsWarehouseService.addWarehouse(wmsWarehouseBO);
            resultDTO.setSuccess(true);
            resultDTO.setMessage("新增成功");
            return resultDTO;
        }catch(Exception ex){
            resultDTO.setSuccess(false);
            resultDTO.setMessage("新增异常");
            return resultDTO;
        }
    }

    /**
     * 修改仓库
     * @param wmsWarehouseVO
     * @return
     */
    @RequestMapping(value = "/updateWarehouse",method = RequestMethod.POST)
    public ResultDTO updateWarehouse(@RequestBody WmsWarehouseVO wmsWarehouseVO){
        ResultDTO resultDTO = new ResultDTO();
        try{
            WmsWarehouseBO wmsWarehouseBO = new WmsWarehouseBO();
            BeanUtils.copyProperties(wmsWarehouseVO,wmsWarehouseBO);
            wmsWarehouseService.updateWarehouse(wmsWarehouseBO);
            resultDTO.setSuccess(true);
            resultDTO.setMessage("更新成功");
            return resultDTO;
        }catch(Exception ex){
            resultDTO.setSuccess(false);
            resultDTO.setMessage("更新异常");
            return resultDTO;
        }
    }

    /**
     * 删除仓库
     * @param wmsWarehouseVO
     * @return
     */
    @RequestMapping(value = "/deleteWarehouse",method = RequestMethod.POST)
    public ResultDTO deleteWarehouse(@RequestBody List<WmsWarehouseVO> wmsWarehouseVO){
        ResultDTO resultDTO = new ResultDTO();
        try{
            Vo2Bo vo2Bo = new Vo2Bo();
            List<WmsWarehouseBO> wmsWarehouseBOList = vo2Bo.convert(wmsWarehouseVO,WmsWarehouseBO.class);
            WmsWarehouseBO wmsWarehouseBO = new WmsWarehouseBO();
            BeanUtils.copyProperties(wmsWarehouseVO,wmsWarehouseBO);
            wmsWarehouseService.deleteWarehouse(wmsWarehouseBOList);
            resultDTO.setSuccess(true);
            resultDTO.setMessage("删除成功");
            return resultDTO;
        }catch(Exception ex){
            resultDTO.setSuccess(false);
            resultDTO.setMessage("删除异常");
            return resultDTO;
        }
    }

    @RequestMapping(value = "/getWarehouse")
    public ResultDTO getWarehouse(){
        ResultDTO resultDTO = new ResultDTO();
        try{
            resultDTO.setMessage("查询成功");
            resultDTO.setMessageCode("200");
            resultDTO.setSuccess(true);
            resultDTO.setData(wmsWarehouseService.getWarehouse());
            return resultDTO;
        }catch(Exception ex){
            resultDTO.setMessage("查询异常");
            resultDTO.setMessageCode("500");
            resultDTO.setSuccess(false);
            return resultDTO;
        }
    }

}
