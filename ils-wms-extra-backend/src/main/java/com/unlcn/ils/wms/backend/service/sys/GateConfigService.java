package com.unlcn.ils.wms.backend.service.sys;
/**
 * 
 * @ClassName: GateConfigService 
 * @Description: 闸门配置 service
 * @author laishijian
 * @date 2017年11月15日 下午3:26:02 
 *
 */

import com.unlcn.ils.wms.backend.bo.sys.GateConfigBO;
import com.unlcn.ils.wms.base.dto.SysGateConfigDTO;

public interface GateConfigService {
	/**
	 * 
	 * @Title: getByType 
	 * @Description: 根据类型查询
	 * @param @param whCode
	 * @param @param type
	 * @param @return
	 * @param whCode
     * @return GateConfigBO    返回类型
	 * @throws 
	 *
	 */
	public GateConfigBO getByType(String whCode, SysGateConfigDTO dto);
}
