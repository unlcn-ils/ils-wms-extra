package com.unlcn.ils.wms.backend.enums;

/**
 * 板车出入计划状态枚举
 */
public enum InOutDrayStatusEnum {
    NOT_APPROACH("NOT_APPROACH", "未进场"),
    EXTENDED_NOT_APPROACH("EXTENDED_NOT_APPROACH", "超期未进场"),
    NORMAL_APPROACH("NORMAL_APPROACH", "正常进场"),
    EXTENDED_APPROACH("EXTENDED_APPROACH", "超期进场"),
    NORMAL_DEPARTURE("NORMAL_DEPARTURE", "正常离场"),
    DISPATCH_DELAY("DISPATCH_DELAY", "超期离场"),
    EXTENDE_NOT_DEPARTURE("EXTENDE_NOT_DEPARTURE", "超期未离场");

    final String value;
    private final String text;

    InOutDrayStatusEnum(String value, String text) {
        this.value = value;
        this.text = text;
    }

    public String getValue() {
        return value;
    }

    public String getText() {
        return text;
    }

    public static InOutDrayStatusEnum getByValue(String value) {
        for (InOutDrayStatusEnum temp : InOutDrayStatusEnum.values()) {
            if (temp.getValue().equals(value)) {
                return temp;
            }
        }
        return null;
    }
}
