package com.unlcn.ils.wms.backend.enums.excp;

/**
 * @Auther linbao
 * @Date 2017-11-23
 */
public enum ExcpDealTypeEnum {
    MISS_FOLLOW((byte) 10, "缺件跟进"),
    OUT_WAREHOUSE_REPAIR((byte) 20, "出库维修"),
    IN_WAREHOUSE_REPAIR((byte) 30, "库内维修"),
    MISS_FOLLOW_AND_OUT_WAREHOUSE((byte) 40, "缺件跟进、出库维修"),
    MISS_FOLLOW_AND_IN_WAREHOUSE((byte) 50, "缺件跟进、库内维修");

    private final byte code;
    private final String text;

    ExcpDealTypeEnum(byte code, String text) {
        this.code = code;
        this.text = text;
    }

    public byte getCode() {
        return code;
    }

    public String getText() {
        return text;
    }

    public static ExcpDealTypeEnum getByValue(byte value) {
        for (ExcpDealTypeEnum temp : ExcpDealTypeEnum.values()) {
            if (value == temp.getCode()) {
                return temp;
            }
        }
        return null;
    }

}
