package com.unlcn.ils.wms.backend.dto.baseDataDTO;

import java.util.Date;

/**
 * Created by DELL on 2017/8/18.
 */
public class WmsWarehouseDTO {

    private Long whId;

    private String whCode;

    private String whName;

    private String whType;

    private String whAddress;

    private String createPerson;

    private String updatePerson;

    private Date gmtCreate;

    private Date gmtUpdate;

    private Byte isDeleted;

    private Long versions;

    public Long getWhId() {
        return whId;
    }

    public void setWhId(Long whId) {
        this.whId = whId;
    }

    public String getWhCode() {
        return whCode;
    }

    public void setWhCode(String whCode) {
        this.whCode = whCode;
    }

    public String getWhName() {
        return whName;
    }

    public void setWhName(String whName) {
        this.whName = whName;
    }

    public String getWhType() {
        return whType;
    }

    public void setWhType(String whType) {
        this.whType = whType;
    }

    public String getWhAddress() {
        return whAddress;
    }

    public void setWhAddress(String whAddress) {
        this.whAddress = whAddress;
    }

    public String getCreatePerson() {
        return createPerson;
    }

    public void setCreatePerson(String createPerson) {
        this.createPerson = createPerson;
    }

    public String getUpdatePerson() {
        return updatePerson;
    }

    public void setUpdatePerson(String updatePerson) {
        this.updatePerson = updatePerson;
    }

    public Date getGmtCreate() {
        return gmtCreate;
    }

    public void setGmtCreate(Date gmtCreate) {
        this.gmtCreate = gmtCreate;
    }

    public Date getGmtUpdate() {
        return gmtUpdate;
    }

    public void setGmtUpdate(Date gmtUpdate) {
        this.gmtUpdate = gmtUpdate;
    }

    public Byte getIsDeleted() {
        return isDeleted;
    }

    public void setIsDeleted(Byte isDeleted) {
        this.isDeleted = isDeleted;
    }

    public Long getVersions() {
        return versions;
    }

    public void setVersions(Long versions) {
        this.versions = versions;
    }

    @Override
    public String toString() {
        return "WmsWarehouseDTO{" +
                "whId=" + whId +
                ", whCode='" + whCode + '\'' +
                ", whName='" + whName + '\'' +
                ", whType='" + whType + '\'' +
                ", whAddress='" + whAddress + '\'' +
                ", createPerson='" + createPerson + '\'' +
                ", updatePerson='" + updatePerson + '\'' +
                ", gmtCreate=" + gmtCreate +
                ", gmtUpdate=" + gmtUpdate +
                ", isDeleted=" + isDeleted +
                ", versions=" + versions +
                '}';
    }
}
