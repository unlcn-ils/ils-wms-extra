package com.unlcn.ils.wms.base.dto;

import java.io.Serializable;
import java.util.Date;

public class WmsJMOutOfStorageOperationResultListDTO implements Serializable {
    private Long dataId;
    private String mblnr;
    private String sernr;
    private String matnr;
    private String zaction;
    private String ztype;

    private Date bldat;

    private String werks;

    private String lgort;

    private String umwrk;

    private String umlgo;

    private String ztflag;

    private String zdemo;

    private String zmsg;

    private String sendStatus;

    private String createUserName;

    private Date gmtCreate;

    private String modifyUserName;

    private Date gmtUpdate;

    private String sendStatusSap;

    private String zmsgSap;

    private String finalDcsStatus;
    private String finalSapStatus;

    public String getFinalDcsStatus() {
        return finalDcsStatus;
    }

    public void setFinalDcsStatus(String finalDcsStatus) {
        this.finalDcsStatus = finalDcsStatus;
    }

    public String getFinalSapStatus() {
        return finalSapStatus;
    }

    public void setFinalSapStatus(String finalSapStatus) {
        this.finalSapStatus = finalSapStatus;
    }

    public Long getDataId() {
        return dataId;
    }

    public void setDataId(Long dataId) {
        this.dataId = dataId;
    }

    public String getMblnr() {
        return mblnr;
    }

    public void setMblnr(String mblnr) {
        this.mblnr = mblnr;
    }

    public String getSernr() {
        return sernr;
    }

    public void setSernr(String sernr) {
        this.sernr = sernr;
    }

    public String getMatnr() {
        return matnr;
    }

    public void setMatnr(String matnr) {
        this.matnr = matnr;
    }

    public String getZaction() {
        return zaction;
    }

    public void setZaction(String zaction) {
        this.zaction = zaction;
    }

    public String getZtype() {
        return ztype;
    }

    public void setZtype(String ztype) {
        this.ztype = ztype;
    }

    public Date getBldat() {
        return bldat;
    }

    public void setBldat(Date bldat) {
        this.bldat = bldat;
    }

    public String getWerks() {
        return werks;
    }

    public void setWerks(String werks) {
        this.werks = werks;
    }

    public String getLgort() {
        return lgort;
    }

    public void setLgort(String lgort) {
        this.lgort = lgort;
    }

    public String getUmwrk() {
        return umwrk;
    }

    public void setUmwrk(String umwrk) {
        this.umwrk = umwrk;
    }

    public String getUmlgo() {
        return umlgo;
    }

    public void setUmlgo(String umlgo) {
        this.umlgo = umlgo;
    }

    public String getZtflag() {
        return ztflag;
    }

    public void setZtflag(String ztflag) {
        this.ztflag = ztflag;
    }

    public String getZdemo() {
        return zdemo;
    }

    public void setZdemo(String zdemo) {
        this.zdemo = zdemo;
    }

    public String getZmsg() {
        return zmsg;
    }

    public void setZmsg(String zmsg) {
        this.zmsg = zmsg;
    }

    public String getSendStatus() {
        return sendStatus;
    }

    public void setSendStatus(String sendStatus) {
        this.sendStatus = sendStatus;
    }

    public String getCreateUserName() {
        return createUserName;
    }

    public void setCreateUserName(String createUserName) {
        this.createUserName = createUserName;
    }

    public Date getGmtCreate() {
        return gmtCreate;
    }

    public void setGmtCreate(Date gmtCreate) {
        this.gmtCreate = gmtCreate;
    }

    public String getModifyUserName() {
        return modifyUserName;
    }

    public void setModifyUserName(String modifyUserName) {
        this.modifyUserName = modifyUserName;
    }

    public Date getGmtUpdate() {
        return gmtUpdate;
    }

    public void setGmtUpdate(Date gmtUpdate) {
        this.gmtUpdate = gmtUpdate;
    }

    public String getSendStatusSap() {
        return sendStatusSap;
    }

    public void setSendStatusSap(String sendStatusSap) {
        this.sendStatusSap = sendStatusSap;
    }

    public String getZmsgSap() {
        return zmsgSap;
    }

    public void setZmsgSap(String zmsgSap) {
        this.zmsgSap = zmsgSap;
    }

    @Override
    public String toString() {
        return "WmsJMOutOfStorageOperationResultListDTO{" +
                "dataId=" + dataId +
                ", mblnr='" + mblnr + '\'' +
                ", sernr='" + sernr + '\'' +
                ", matnr='" + matnr + '\'' +
                ", zaction='" + zaction + '\'' +
                ", ztype='" + ztype + '\'' +
                ", bldat=" + bldat +
                ", werks='" + werks + '\'' +
                ", lgort='" + lgort + '\'' +
                ", umwrk='" + umwrk + '\'' +
                ", umlgo='" + umlgo + '\'' +
                ", ztflag='" + ztflag + '\'' +
                ", zdemo='" + zdemo + '\'' +
                ", zmsg='" + zmsg + '\'' +
                ", sendStatus='" + sendStatus + '\'' +
                ", createUserName='" + createUserName + '\'' +
                ", gmtCreate=" + gmtCreate +
                ", modifyUserName='" + modifyUserName + '\'' +
                ", gmtUpdate=" + gmtUpdate +
                ", sendStatusSap='" + sendStatusSap + '\'' +
                ", zmsgSap='" + zmsgSap + '\'' +
                '}';
    }

}
