package com.unlcn.ils.wms.backend.service.outbound.impl;

import com.google.common.collect.Lists;
import com.unlcn.ils.wms.backend.bo.outboundBO.WmsEstimateLaneBO;
import com.unlcn.ils.wms.backend.service.outbound.WmsEstimateLaneService;
import com.unlcn.ils.wms.base.mapper.outbound.WmsEstimateLaneMapper;
import com.unlcn.ils.wms.base.model.outbound.WmsEstimateLane;
import com.unlcn.ils.wms.base.model.outbound.WmsEstimateLaneExample;
import org.apache.commons.collections4.CollectionUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by DELL on 2017/10/13.
 */
@Service
public class WmsEstimateLaneServiceImpl implements WmsEstimateLaneService {
    @Autowired
    WmsEstimateLaneMapper wmsEstimateLaneMapper;

    @Override
    public List<WmsEstimateLaneBO> getEstimateLaneByWhCode(String whCode) {
        WmsEstimateLaneExample wmsEstimateLaneExample = new WmsEstimateLaneExample();
        wmsEstimateLaneExample.createCriteria().andElWhCodeEqualTo(whCode);
        List<WmsEstimateLane> wmsEstimateLaneList =  wmsEstimateLaneMapper.selectByExample(wmsEstimateLaneExample);
        List<WmsEstimateLaneBO> wmsEstimateLaneBOList = Lists.newArrayList();
        if (CollectionUtils.isNotEmpty(wmsEstimateLaneList)) {
            wmsEstimateLaneList.stream().forEach(v -> {
                WmsEstimateLaneBO bo = new WmsEstimateLaneBO();
                BeanUtils.copyProperties(v, bo);
                wmsEstimateLaneBOList.add(bo);
            });
        }
        return wmsEstimateLaneBOList;
    }

    @Override
    public void addEstimateLane(WmsEstimateLaneBO wmsEstimateLaneBO) {
        WmsEstimateLane wmsEstimateLane = new WmsEstimateLane();
        BeanUtils.copyProperties(wmsEstimateLaneBO,wmsEstimateLane);
        wmsEstimateLaneMapper.insert(wmsEstimateLane);
    }

    @Override
    public void deleteEstimateLane(List<WmsEstimateLaneBO> wmsEstimateLaneBOList) {
        List<String> ids = Lists.newArrayList();
        if (CollectionUtils.isNotEmpty(wmsEstimateLaneBOList)) {
            wmsEstimateLaneBOList.stream().forEach(v -> {
                wmsEstimateLaneMapper.deleteByPrimaryKey(v.getElId());
            });
        }
    }
}
