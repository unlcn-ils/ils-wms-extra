package com.unlcn.ils.wms.backend.constant;

public class WmsConstant {

    private WmsConstant() {

    }

    public static final String HN_PROVINCE_NAME = "湖南省";
    public static final String HN_PROVINCE_CODE = "430000";
    public static final String HB_PROVINCE_NAME = "湖北省";
    public static final String HB_PROVINCE_CODE = "420000";
    public static final String CS_CITY_NAME = "长沙市";
    public static final String CS_CITY_CODE = "430100";
    public static final String XY_CITY_NAME = "襄阳市";
    public static final String XY_CITY_CODE = "420600";
    public static final String CS_ADDR = "湖南省长沙市";
    public static final String XY_ADDR = "湖北省襄阳市";
    public static final String CREATOR_CS = "君马-长沙";
    public static final String CREATOR_XY = "君马-襄阳";

    public static final String CS_CUSTOMER_NAME = "君马长沙";
    public static final int CS_CUSTOMER_ID = 1685;
    public static final String XY_CUSTOMER_NAME = "君马襄阳";
    public static final int XY_CUSTOMER_ID = 1686;
    public static final int NOT_BURGENT = 0;
    public static final int IS_BURGENT = 1;

}
