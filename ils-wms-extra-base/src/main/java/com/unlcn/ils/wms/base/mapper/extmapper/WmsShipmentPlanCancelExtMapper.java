package com.unlcn.ils.wms.base.mapper.extmapper;

import com.unlcn.ils.wms.base.model.junmadcs.WmsShipmentPlanCancel;

import java.util.ArrayList;

public interface WmsShipmentPlanCancelExtMapper {

    void insertCancelBatch(ArrayList<WmsShipmentPlanCancel> cancelList);
}
