package com.unlcn.ils.wms.base.dto;

import java.util.Date;

public class WmsWarehouseNoticeHeadForASNListResultDTO extends WmsWarehouseNoticeHeadDTO {
    private Date inspectTime;
    private String odRmark;
    private String odCheckResult;
    private String odCheckDesc;
    private String inspectStatus;

    public String getInspectStatus() {
        return inspectStatus;
    }

    public void setInspectStatus(String inspectStatus) {
        this.inspectStatus = inspectStatus;
    }

    public Date getInspectTime() {
        return inspectTime;
    }

    public void setInspectTime(Date inspectTime) {
        this.inspectTime = inspectTime;
    }

    public String getOdRmark() {
        return odRmark;
    }

    public void setOdRmark(String odRmark) {
        this.odRmark = odRmark;
    }

    public String getOdCheckResult() {
        return odCheckResult;
    }

    public void setOdCheckResult(String odCheckResult) {
        this.odCheckResult = odCheckResult;
    }

    public String getOdCheckDesc() {
        return odCheckDesc;
    }

    public void setOdCheckDesc(String odCheckDesc) {
        this.odCheckDesc = odCheckDesc;
    }
}
