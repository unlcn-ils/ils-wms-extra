package com.unlcn.ils.wms.base.model.sys;

import java.io.Serializable;

public class SysRolePermissions implements Serializable {
    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column sys_role_permissions.id
     *
     * @mbggenerated Wed Sep 27 16:38:09 CST 2017
     */
    private Integer id;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column sys_role_permissions.role_id
     *
     * @mbggenerated Wed Sep 27 16:38:09 CST 2017
     */
    private Integer roleId;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column sys_role_permissions.permissions_id
     *
     * @mbggenerated Wed Sep 27 16:38:09 CST 2017
     */
    private Integer permissionsId;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database table sys_role_permissions
     *
     * @mbggenerated Wed Sep 27 16:38:09 CST 2017
     */
    private static final long serialVersionUID = 1L;

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column sys_role_permissions.id
     *
     * @return the value of sys_role_permissions.id
     *
     * @mbggenerated Wed Sep 27 16:38:09 CST 2017
     */
    public Integer getId() {
        return id;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column sys_role_permissions.id
     *
     * @param id the value for sys_role_permissions.id
     *
     * @mbggenerated Wed Sep 27 16:38:09 CST 2017
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column sys_role_permissions.role_id
     *
     * @return the value of sys_role_permissions.role_id
     *
     * @mbggenerated Wed Sep 27 16:38:09 CST 2017
     */
    public Integer getRoleId() {
        return roleId;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column sys_role_permissions.role_id
     *
     * @param roleId the value for sys_role_permissions.role_id
     *
     * @mbggenerated Wed Sep 27 16:38:09 CST 2017
     */
    public void setRoleId(Integer roleId) {
        this.roleId = roleId;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column sys_role_permissions.permissions_id
     *
     * @return the value of sys_role_permissions.permissions_id
     *
     * @mbggenerated Wed Sep 27 16:38:09 CST 2017
     */
    public Integer getPermissionsId() {
        return permissionsId;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column sys_role_permissions.permissions_id
     *
     * @param permissionsId the value for sys_role_permissions.permissions_id
     *
     * @mbggenerated Wed Sep 27 16:38:09 CST 2017
     */
    public void setPermissionsId(Integer permissionsId) {
        this.permissionsId = permissionsId;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table sys_role_permissions
     *
     * @mbggenerated Wed Sep 27 16:38:09 CST 2017
     */
    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", id=").append(id);
        sb.append(", roleId=").append(roleId);
        sb.append(", permissionsId=").append(permissionsId);
        sb.append("]");
        return sb.toString();
    }
}