package com.unlcn.ils.wms.api.outbound.quartz;

import cn.huiyunche.commons.exception.BusinessException;
import com.unlcn.ils.wms.backend.service.outbound.OutBoundTaskService;
import org.quartz.JobExecutionException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

@Component("outboundEmailNew")
public class QuartzOutboundConfirmNew {

    private Logger logger = LoggerFactory.getLogger(QuartzOutboundConfirmNew.class);

    private static final SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");


    @Autowired
    private OutBoundTaskService outBoundTaskService;

    //四个时间点发送邮件-10:30
    public void sendEamil1030() throws JobExecutionException {
        logger.info("QuartzOutboundConfirm.sendEamil1030 定时任务start========");
        //设置邮件数据起始时间
        try {
            Calendar calendar = Calendar.getInstance();
            calendar.setTime(new Date());
            calendar.set(Calendar.HOUR_OF_DAY, 0);
            calendar.set(Calendar.MINUTE, 0);
            calendar.set(Calendar.SECOND, 0);
            String starttime = sdf.format(calendar.getTime());

            calendar.set(Calendar.HOUR_OF_DAY, 10);
            calendar.set(Calendar.MINUTE, 30);
            calendar.set(Calendar.SECOND, 0);
            String endtime = sdf.format(calendar.getTime());
            outBoundTaskService.getOutboundSendMailDataNew(starttime, endtime);
        } catch (BusinessException e) {
            logger.error("QuartzOutboundConfirm.sendEamil1030 param{}", e);
            e.printStackTrace();
        } catch (Exception ex) {
            logger.error("QuartzOutboundConfirm.sendEamil1030 param{}", ex);
            ex.printStackTrace();
        }
    }

    //14:30
    public void sendEamil1430() throws JobExecutionException {
        logger.info("QuartzOutboundConfirm.sendEamil1430 定时任务start========");
        //设置邮件数据起始时间
        try {
            Calendar calendar = Calendar.getInstance();
            calendar.setTime(new Date());
            calendar.set(Calendar.HOUR_OF_DAY, 10);
            calendar.set(Calendar.MINUTE, 30);
            calendar.set(Calendar.SECOND, 0);
            String starttime = sdf.format(calendar.getTime());

            calendar.set(Calendar.HOUR_OF_DAY, 14);
            calendar.set(Calendar.MINUTE, 30);
            calendar.set(Calendar.SECOND, 0);
            String endtime = sdf.format(calendar.getTime());
            outBoundTaskService.getOutboundSendMailDataNew(starttime, endtime);
        } catch (BusinessException e) {
            logger.error("QuartzOutboundConfirm.sendEamil1430 param{}", e);
            e.printStackTrace();
        } catch (Exception ex) {
            logger.error("QuartzOutboundConfirm.sendEamil1430 param{}", ex);
            ex.printStackTrace();
        }
    }

    //16:30
    public void sendEamil1630() throws JobExecutionException {
        logger.info("QuartzOutboundConfirm.sendEamil1630 定时任务start========");
        //设置邮件数据起始时间
        try {
            Calendar calendar = Calendar.getInstance();
            calendar.setTime(new Date());
            calendar.set(Calendar.HOUR_OF_DAY, 14);
            calendar.set(Calendar.MINUTE, 30);
            calendar.set(Calendar.SECOND, 0);
            String starttime = sdf.format(calendar.getTime());

            calendar.set(Calendar.HOUR_OF_DAY, 16);
            calendar.set(Calendar.MINUTE, 30);
            calendar.set(Calendar.SECOND, 0);
            String endtime = sdf.format(calendar.getTime());
            outBoundTaskService.getOutboundSendMailDataNew(starttime, endtime);
        } catch (BusinessException e) {
            logger.error("QuartzOutboundConfirm.sendEamil1630 param{}", e);
            e.printStackTrace();
        } catch (Exception ex) {
            logger.error("QuartzOutboundConfirm.sendEamil1630 param{}", ex);
            ex.printStackTrace();
        }
    }

    //23:50
    public void sendEamil2350() throws JobExecutionException {
        logger.info("QuartzOutboundConfirm.sendEamil2350 定时任务start========");
        //设置邮件数据起始时间
        try {
            Calendar calendar = Calendar.getInstance();
            calendar.setTime(new Date());
            calendar.set(Calendar.HOUR_OF_DAY, 16);
            calendar.set(Calendar.MINUTE, 30);
            calendar.set(Calendar.SECOND, 0);
            String starttime = sdf.format(calendar.getTime());

            calendar.set(Calendar.HOUR_OF_DAY, 23);
            calendar.set(Calendar.MINUTE, 50);
            calendar.set(Calendar.SECOND, 0);
            String endtime = sdf.format(calendar.getTime());
            outBoundTaskService.getOutboundSendMailDataNew(starttime, endtime);
        } catch (BusinessException e) {
            logger.error("QuartzOutboundConfirm.sendEamil2350 param{}", e);
            e.printStackTrace();
        } catch (Exception ex) {
            logger.error("QuartzOutboundConfirm.sendEamil2350 param{}", ex);
            ex.printStackTrace();
        }
    }
}
