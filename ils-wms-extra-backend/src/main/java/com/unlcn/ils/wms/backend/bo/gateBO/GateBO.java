package com.unlcn.ils.wms.backend.bo.gateBO;

/**
 * 
 * @ClassName: GateBO 
 * @Description: 门 BO
 * @author laishijian
 * @date 2017年11月9日 上午10:09:36 
 *
 */
public class GateBO {
	/**
	 * 通道编号
	 */
	private String channel;
	/**
	 * 车牌号
	 */
	private String platenum;
	/**
	 * 出发时间
	 */
	private String ymdhms;
	/**
	 * 卡种类
	 */
	private String kind;
	
	/**
	 * @return the channel
	 */
	public String getChannel() {
		return channel;
	}
	/**
	 * @param channel the channel to set
	 */
	public void setChannel(String channel) {
		this.channel = channel;
	}
	/**
	 * @return the platenum
	 */
	public String getPlatenum() {
		return platenum;
	}
	/**
	 * @param platenum the platenum to set
	 */
	public void setPlatenum(String platenum) {
		this.platenum = platenum;
	}
	/**
	 * @return the ymdhms
	 */
	public String getYmdhms() {
		return ymdhms;
	}
	/**
	 * @param ymdhms the ymdhms to set
	 */
	public void setYmdhms(String ymdhms) {
		this.ymdhms = ymdhms;
	}
	/**
	 * @return the kind
	 */
	public String getKind() {
		return kind;
	}
	/**
	 * @param kind the kind to set
	 */
	public void setKind(String kind) {
		this.kind = kind;
	}
}
