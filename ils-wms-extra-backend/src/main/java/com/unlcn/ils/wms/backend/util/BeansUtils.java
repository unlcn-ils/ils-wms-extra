package com.unlcn.ils.wms.backend.util;

import com.google.common.base.CaseFormat;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * 获取到的List<Map>结果集转化为JavaBean工具类
 * Created by DELL on 2017/9/27.
 */
public class BeansUtils<T> {
    /**
     * 根据List<Map<String, Object>>数据转换为JavaBean数据
     * @param datas
     * @param beanClass
     * @return
     */

    public List<T> listMap2JavaBean(List<Map<String, Object>> datas, Class<T> beanClass) throws CommonException {
        DateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        // 返回数据集合
        List<T> list = null;
        // 对象字段名称
        String fieldname = "";
        // 对象方法名称
        String methodname = "";
        // 对象方法需要赋的值
        Object methodsetvalue = "";
        try {
            list = new ArrayList<T>();
            // 得到对象所有字段
            Field fields[] = beanClass.getDeclaredFields();
            // 遍历数据
            for (Map<String, Object> mapdata : datas) {
                // 创建一个泛型类型实例
                T t = beanClass.newInstance();
                // 遍历所有字段，对应配置好的字段并赋值
                for (Field field : fields) {
                    if(null != field) {
                        // 全部转化为大写
                        String dbfieldname = field.getName().toUpperCase();
                        // 获取字段名称
                        fieldname = field.getName();
                        // 拼接set方法
                        methodname = "set" + capitalize(fieldname);
                        // 获取data里的对应值
                        methodsetvalue = mapdata.get(CaseFormat.LOWER_CAMEL.to(CaseFormat.LOWER_UNDERSCORE, fieldname));
                        // 赋值给字段

                        Method m = beanClass.getDeclaredMethod(methodname, field.getType());
                        if(methodsetvalue instanceof Timestamp || methodsetvalue instanceof Date){
                            methodsetvalue = sdf.format(methodsetvalue);
                        }
                        m.invoke(t, methodsetvalue);
                    }
                }
                // 存入返回列表
                list.add(t);
            }
        } catch (InstantiationException | IllegalAccessException e) {
            throw new CommonException(e, "创建beanClass实例异常");
        } catch (SecurityException | NoSuchMethodException e) {
            throw new CommonException(e, "获取[" + fieldname + "] getter setter 方法异常");
        } catch (IllegalArgumentException | InvocationTargetException e) {
            throw new CommonException(e, "[" + methodname + "] 方法赋值异常");
        }
        // 返回
        return list;
    }

    private static String capitalize(String str) {
        if (str == null || str.length() == 0) {
            return str;
        }
        return String.valueOf(Character.toTitleCase(str.charAt(0))) +
                str.substring(1);
    }
}
