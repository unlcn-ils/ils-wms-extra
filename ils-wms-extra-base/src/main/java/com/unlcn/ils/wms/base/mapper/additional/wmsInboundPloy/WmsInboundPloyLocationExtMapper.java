package com.unlcn.ils.wms.base.mapper.additional.wmsInboundPloy;

import com.unlcn.ils.wms.base.model.inbound.WmsInboundPloyLocation;

import java.util.List;

/**
 * Created by DELL on 2017/9/1.
 */
public interface WmsInboundPloyLocationExtMapper {
    /**
     * 根据主策略CODE查询策略库区库位信息
     * @param ployCode
     * @return
     */
    List<WmsInboundPloyLocation> getPloyLocationList(String ployCode);
}
