package com.unlcn.ils.wms.backend.service.inbound;

/**
 * Created by lenovo on 2017/10/27.
 */
public interface WmsBatchNoService {
    /**
     * 生成流水号
     * @param serialCode
     * @param bnPrefix
     * @return
     */
    String addSerialNum(String serialCode, String bnPrefix);

    //String getnumber(String headCode, String lastCode, String decimalFormat);
}
