package com.unlcn.ils.wms.api.webservice.server;

import com.unlcn.ils.wms.api.webservice.dto.ResultHandoverSapDTO;
import com.unlcn.ils.wms.api.webservice.dto.ResultInboundAndOutBoundSapDTO;

/**
 * 作为服务端供sap接口调用
 */
public interface WmsSapInorOutBusinessService {

    //String getInboundOutboundMessage(ResultInboundAndOutBoundSapDTO inboundAndOutBoundSapDTO) throws Exception;

    String getHandoverSapMessage(ResultHandoverSapDTO resultHandoverSapDTO) throws Exception;

}
