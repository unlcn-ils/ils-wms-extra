package com.unlcn.ils.wms.backend.bo;

/**
 * Created by houjianhui on 2017/5/7.
 */
public class WarehouseBO {
    private Integer id;
    private String warehouseName;
    private Boolean isCheck;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getWarehouseName() {
        return warehouseName;
    }

    public void setWarehouseName(String warehouseName) {
        this.warehouseName = warehouseName;
    }

    public Boolean getCheck() {
        return isCheck;
    }

    public void setCheck(Boolean check) {
        isCheck = check;
    }

    @Override
    public String toString() {
        return "WarehouseBO{" +
                "id=" + id +
                ", warehouseName='" + warehouseName + '\'' +
                ", isCheck=" + isCheck +
                '}';
    }
}
