package com.unlcn.ils.wms.base.model.inspectApp;

import java.io.Serializable;
import java.util.Date;

public class TmsInspectExcp implements Serializable {
    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column tms_inspect_excp.id
     *
     * @mbggenerated Wed May 09 10:08:28 CST 2018
     */
    private Long id;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column tms_inspect_excp.inspect_id
     *
     * @mbggenerated Wed May 09 10:08:28 CST 2018
     */
    private Long inspectId;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column tms_inspect_excp.position_id
     *
     * @mbggenerated Wed May 09 10:08:28 CST 2018
     */
    private Integer positionId;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column tms_inspect_excp.constant_body_code
     *
     * @mbggenerated Wed May 09 10:08:28 CST 2018
     */
    private Integer constantBodyCode;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column tms_inspect_excp.constant_hurt_code
     *
     * @mbggenerated Wed May 09 10:08:28 CST 2018
     */
    private Integer constantHurtCode;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column tms_inspect_excp.deal_status
     *
     * @mbggenerated Wed May 09 10:08:28 CST 2018
     */
    private Integer dealStatus;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column tms_inspect_excp.gmt_create
     *
     * @mbggenerated Wed May 09 10:08:28 CST 2018
     */
    private Date gmtCreate;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column tms_inspect_excp.gmt_update
     *
     * @mbggenerated Wed May 09 10:08:28 CST 2018
     */
    private Date gmtUpdate;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column tms_inspect_excp.del_Flag
     *
     * @mbggenerated Wed May 09 10:08:28 CST 2018
     */
    private Boolean delFlag;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column tms_inspect_excp.excp_status
     *
     * @mbggenerated Wed May 09 10:08:28 CST 2018
     */
    private Byte excpStatus;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column tms_inspect_excp.involved_party
     *
     * @mbggenerated Wed May 09 10:08:28 CST 2018
     */
    private String involvedParty;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column tms_inspect_excp.deal_type
     *
     * @mbggenerated Wed May 09 10:08:28 CST 2018
     */
    private Byte dealType;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column tms_inspect_excp.deal_result_desc
     *
     * @mbggenerated Wed May 09 10:08:28 CST 2018
     */
    private String dealResultDesc;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column tms_inspect_excp.deal_end_time
     *
     * @mbggenerated Wed May 09 10:08:28 CST 2018
     */
    private Date dealEndTime;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column tms_inspect_excp.excp_type_code
     *
     * @mbggenerated Wed May 09 10:08:28 CST 2018
     */
    private String excpTypeCode;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column tms_inspect_excp.excp_repair_id
     *
     * @mbggenerated Wed May 09 10:08:28 CST 2018
     */
    private Long excpRepairId;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column tms_inspect_excp.excp_timestamp
     *
     * @mbggenerated Wed May 09 10:08:28 CST 2018
     */
    private String excpTimestamp;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column tms_inspect_excp.wh_code
     *
     * @mbggenerated Wed May 09 10:08:28 CST 2018
     */
    private String whCode;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database table tms_inspect_excp
     *
     * @mbggenerated Wed May 09 10:08:28 CST 2018
     */
    private static final long serialVersionUID = 1L;

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column tms_inspect_excp.id
     *
     * @return the value of tms_inspect_excp.id
     *
     * @mbggenerated Wed May 09 10:08:28 CST 2018
     */
    public Long getId() {
        return id;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column tms_inspect_excp.id
     *
     * @param id the value for tms_inspect_excp.id
     *
     * @mbggenerated Wed May 09 10:08:28 CST 2018
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column tms_inspect_excp.inspect_id
     *
     * @return the value of tms_inspect_excp.inspect_id
     *
     * @mbggenerated Wed May 09 10:08:28 CST 2018
     */
    public Long getInspectId() {
        return inspectId;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column tms_inspect_excp.inspect_id
     *
     * @param inspectId the value for tms_inspect_excp.inspect_id
     *
     * @mbggenerated Wed May 09 10:08:28 CST 2018
     */
    public void setInspectId(Long inspectId) {
        this.inspectId = inspectId;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column tms_inspect_excp.position_id
     *
     * @return the value of tms_inspect_excp.position_id
     *
     * @mbggenerated Wed May 09 10:08:28 CST 2018
     */
    public Integer getPositionId() {
        return positionId;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column tms_inspect_excp.position_id
     *
     * @param positionId the value for tms_inspect_excp.position_id
     *
     * @mbggenerated Wed May 09 10:08:28 CST 2018
     */
    public void setPositionId(Integer positionId) {
        this.positionId = positionId;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column tms_inspect_excp.constant_body_code
     *
     * @return the value of tms_inspect_excp.constant_body_code
     *
     * @mbggenerated Wed May 09 10:08:28 CST 2018
     */
    public Integer getConstantBodyCode() {
        return constantBodyCode;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column tms_inspect_excp.constant_body_code
     *
     * @param constantBodyCode the value for tms_inspect_excp.constant_body_code
     *
     * @mbggenerated Wed May 09 10:08:28 CST 2018
     */
    public void setConstantBodyCode(Integer constantBodyCode) {
        this.constantBodyCode = constantBodyCode;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column tms_inspect_excp.constant_hurt_code
     *
     * @return the value of tms_inspect_excp.constant_hurt_code
     *
     * @mbggenerated Wed May 09 10:08:28 CST 2018
     */
    public Integer getConstantHurtCode() {
        return constantHurtCode;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column tms_inspect_excp.constant_hurt_code
     *
     * @param constantHurtCode the value for tms_inspect_excp.constant_hurt_code
     *
     * @mbggenerated Wed May 09 10:08:28 CST 2018
     */
    public void setConstantHurtCode(Integer constantHurtCode) {
        this.constantHurtCode = constantHurtCode;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column tms_inspect_excp.deal_status
     *
     * @return the value of tms_inspect_excp.deal_status
     *
     * @mbggenerated Wed May 09 10:08:28 CST 2018
     */
    public Integer getDealStatus() {
        return dealStatus;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column tms_inspect_excp.deal_status
     *
     * @param dealStatus the value for tms_inspect_excp.deal_status
     *
     * @mbggenerated Wed May 09 10:08:28 CST 2018
     */
    public void setDealStatus(Integer dealStatus) {
        this.dealStatus = dealStatus;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column tms_inspect_excp.gmt_create
     *
     * @return the value of tms_inspect_excp.gmt_create
     *
     * @mbggenerated Wed May 09 10:08:28 CST 2018
     */
    public Date getGmtCreate() {
        return gmtCreate;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column tms_inspect_excp.gmt_create
     *
     * @param gmtCreate the value for tms_inspect_excp.gmt_create
     *
     * @mbggenerated Wed May 09 10:08:28 CST 2018
     */
    public void setGmtCreate(Date gmtCreate) {
        this.gmtCreate = gmtCreate;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column tms_inspect_excp.gmt_update
     *
     * @return the value of tms_inspect_excp.gmt_update
     *
     * @mbggenerated Wed May 09 10:08:28 CST 2018
     */
    public Date getGmtUpdate() {
        return gmtUpdate;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column tms_inspect_excp.gmt_update
     *
     * @param gmtUpdate the value for tms_inspect_excp.gmt_update
     *
     * @mbggenerated Wed May 09 10:08:28 CST 2018
     */
    public void setGmtUpdate(Date gmtUpdate) {
        this.gmtUpdate = gmtUpdate;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column tms_inspect_excp.del_Flag
     *
     * @return the value of tms_inspect_excp.del_Flag
     *
     * @mbggenerated Wed May 09 10:08:28 CST 2018
     */
    public Boolean getDelFlag() {
        return delFlag;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column tms_inspect_excp.del_Flag
     *
     * @param delFlag the value for tms_inspect_excp.del_Flag
     *
     * @mbggenerated Wed May 09 10:08:28 CST 2018
     */
    public void setDelFlag(Boolean delFlag) {
        this.delFlag = delFlag;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column tms_inspect_excp.excp_status
     *
     * @return the value of tms_inspect_excp.excp_status
     *
     * @mbggenerated Wed May 09 10:08:28 CST 2018
     */
    public Byte getExcpStatus() {
        return excpStatus;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column tms_inspect_excp.excp_status
     *
     * @param excpStatus the value for tms_inspect_excp.excp_status
     *
     * @mbggenerated Wed May 09 10:08:28 CST 2018
     */
    public void setExcpStatus(Byte excpStatus) {
        this.excpStatus = excpStatus;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column tms_inspect_excp.involved_party
     *
     * @return the value of tms_inspect_excp.involved_party
     *
     * @mbggenerated Wed May 09 10:08:28 CST 2018
     */
    public String getInvolvedParty() {
        return involvedParty;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column tms_inspect_excp.involved_party
     *
     * @param involvedParty the value for tms_inspect_excp.involved_party
     *
     * @mbggenerated Wed May 09 10:08:28 CST 2018
     */
    public void setInvolvedParty(String involvedParty) {
        this.involvedParty = involvedParty == null ? null : involvedParty.trim();
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column tms_inspect_excp.deal_type
     *
     * @return the value of tms_inspect_excp.deal_type
     *
     * @mbggenerated Wed May 09 10:08:28 CST 2018
     */
    public Byte getDealType() {
        return dealType;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column tms_inspect_excp.deal_type
     *
     * @param dealType the value for tms_inspect_excp.deal_type
     *
     * @mbggenerated Wed May 09 10:08:28 CST 2018
     */
    public void setDealType(Byte dealType) {
        this.dealType = dealType;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column tms_inspect_excp.deal_result_desc
     *
     * @return the value of tms_inspect_excp.deal_result_desc
     *
     * @mbggenerated Wed May 09 10:08:28 CST 2018
     */
    public String getDealResultDesc() {
        return dealResultDesc;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column tms_inspect_excp.deal_result_desc
     *
     * @param dealResultDesc the value for tms_inspect_excp.deal_result_desc
     *
     * @mbggenerated Wed May 09 10:08:28 CST 2018
     */
    public void setDealResultDesc(String dealResultDesc) {
        this.dealResultDesc = dealResultDesc == null ? null : dealResultDesc.trim();
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column tms_inspect_excp.deal_end_time
     *
     * @return the value of tms_inspect_excp.deal_end_time
     *
     * @mbggenerated Wed May 09 10:08:28 CST 2018
     */
    public Date getDealEndTime() {
        return dealEndTime;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column tms_inspect_excp.deal_end_time
     *
     * @param dealEndTime the value for tms_inspect_excp.deal_end_time
     *
     * @mbggenerated Wed May 09 10:08:28 CST 2018
     */
    public void setDealEndTime(Date dealEndTime) {
        this.dealEndTime = dealEndTime;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column tms_inspect_excp.excp_type_code
     *
     * @return the value of tms_inspect_excp.excp_type_code
     *
     * @mbggenerated Wed May 09 10:08:28 CST 2018
     */
    public String getExcpTypeCode() {
        return excpTypeCode;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column tms_inspect_excp.excp_type_code
     *
     * @param excpTypeCode the value for tms_inspect_excp.excp_type_code
     *
     * @mbggenerated Wed May 09 10:08:28 CST 2018
     */
    public void setExcpTypeCode(String excpTypeCode) {
        this.excpTypeCode = excpTypeCode == null ? null : excpTypeCode.trim();
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column tms_inspect_excp.excp_repair_id
     *
     * @return the value of tms_inspect_excp.excp_repair_id
     *
     * @mbggenerated Wed May 09 10:08:28 CST 2018
     */
    public Long getExcpRepairId() {
        return excpRepairId;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column tms_inspect_excp.excp_repair_id
     *
     * @param excpRepairId the value for tms_inspect_excp.excp_repair_id
     *
     * @mbggenerated Wed May 09 10:08:28 CST 2018
     */
    public void setExcpRepairId(Long excpRepairId) {
        this.excpRepairId = excpRepairId;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column tms_inspect_excp.excp_timestamp
     *
     * @return the value of tms_inspect_excp.excp_timestamp
     *
     * @mbggenerated Wed May 09 10:08:28 CST 2018
     */
    public String getExcpTimestamp() {
        return excpTimestamp;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column tms_inspect_excp.excp_timestamp
     *
     * @param excpTimestamp the value for tms_inspect_excp.excp_timestamp
     *
     * @mbggenerated Wed May 09 10:08:28 CST 2018
     */
    public void setExcpTimestamp(String excpTimestamp) {
        this.excpTimestamp = excpTimestamp == null ? null : excpTimestamp.trim();
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column tms_inspect_excp.wh_code
     *
     * @return the value of tms_inspect_excp.wh_code
     *
     * @mbggenerated Wed May 09 10:08:28 CST 2018
     */
    public String getWhCode() {
        return whCode;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column tms_inspect_excp.wh_code
     *
     * @param whCode the value for tms_inspect_excp.wh_code
     *
     * @mbggenerated Wed May 09 10:08:28 CST 2018
     */
    public void setWhCode(String whCode) {
        this.whCode = whCode == null ? null : whCode.trim();
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table tms_inspect_excp
     *
     * @mbggenerated Wed May 09 10:08:28 CST 2018
     */
    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", id=").append(id);
        sb.append(", inspectId=").append(inspectId);
        sb.append(", positionId=").append(positionId);
        sb.append(", constantBodyCode=").append(constantBodyCode);
        sb.append(", constantHurtCode=").append(constantHurtCode);
        sb.append(", dealStatus=").append(dealStatus);
        sb.append(", gmtCreate=").append(gmtCreate);
        sb.append(", gmtUpdate=").append(gmtUpdate);
        sb.append(", delFlag=").append(delFlag);
        sb.append(", excpStatus=").append(excpStatus);
        sb.append(", involvedParty=").append(involvedParty);
        sb.append(", dealType=").append(dealType);
        sb.append(", dealResultDesc=").append(dealResultDesc);
        sb.append(", dealEndTime=").append(dealEndTime);
        sb.append(", excpTypeCode=").append(excpTypeCode);
        sb.append(", excpRepairId=").append(excpRepairId);
        sb.append(", excpTimestamp=").append(excpTimestamp);
        sb.append(", whCode=").append(whCode);
        sb.append("]");
        return sb.toString();
    }
}