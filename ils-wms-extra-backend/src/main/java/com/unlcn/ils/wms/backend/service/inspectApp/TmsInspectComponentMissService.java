package com.unlcn.ils.wms.backend.service.inspectApp;

import com.unlcn.ils.wms.base.dto.TmsInspectComponentMissDTO;
import com.unlcn.ils.wms.base.dto.WmsInspectForAppDTO;

import java.util.List;

public interface TmsInspectComponentMissService {


    List<TmsInspectComponentMissDTO> getComponentMissList(String vin) throws Exception;


    List<TmsInspectComponentMissDTO> getComponentMissListNew(String vin,String whCode) throws Exception;

    void saveComponentMiss(WmsInspectForAppDTO dto) throws Exception;

    String getMissComponentNames(String vin) throws Exception;

    String getMissComponentNamesNew(Long id, String whCode) throws Exception;
}
