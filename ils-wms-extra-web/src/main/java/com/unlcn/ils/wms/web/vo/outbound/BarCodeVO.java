package com.unlcn.ils.wms.web.vo.outbound;

/**
 * @Auther linbao
 * @Date 2017-10-24
 */
public class BarCodeVO {

    /**
     * 类型
     */
    private String orderNo;

    /**
     * 类型
     */
    private String type;

    public String getOrderNo() {
        return orderNo;
    }

    public void setOrderNo(String orderNo) {
        this.orderNo = orderNo;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    @Override
    public String toString() {
        return "BarCodeVO{" +
                "orderNo='" + orderNo + '\'' +
                ", type='" + type + '\'' +
                '}';
    }
}
