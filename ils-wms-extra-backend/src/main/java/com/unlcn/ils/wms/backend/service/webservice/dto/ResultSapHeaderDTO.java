package com.unlcn.ils.wms.backend.service.webservice.dto;

import java.io.Serializable;

public class ResultSapHeaderDTO implements Serializable {
    // pi头信息
    private String msgid; //消息id
    private String busid;//业务id
    private String tlgid;//接口id
    private String tlgname;//接口名称
    private String dtsend;//发送时间
    private String sender;//发送方
    private String receiver;//接收方
    private String freeuse;//备用 (选填)

    public String getMsgid() {
        return msgid;
    }

    public void setMsgid(String msgid) {
        this.msgid = msgid;
    }

    public String getBusid() {
        return busid;
    }

    public void setBusid(String busid) {
        this.busid = busid;
    }

    public String getTlgid() {
        return tlgid;
    }

    public void setTlgid(String tlgid) {
        this.tlgid = tlgid;
    }

    public String getTlgname() {
        return tlgname;
    }

    public void setTlgname(String tlgname) {
        this.tlgname = tlgname;
    }

    public String getDtsend() {
        return dtsend;
    }

    public void setDtsend(String dtsend) {
        this.dtsend = dtsend;
    }

    public String getSender() {
        return sender;
    }

    public void setSender(String sender) {
        this.sender = sender;
    }

    public String getReceiver() {
        return receiver;
    }

    public void setReceiver(String receiver) {
        this.receiver = receiver;
    }

    public String getFreeuse() {
        return freeuse;
    }

    public void setFreeuse(String freeuse) {
        this.freeuse = freeuse;
    }
}
