package com.unlcn.ils.wms.base.mapper.extmapper;

import com.unlcn.ils.wms.base.model.bigscreen.BiDistribution;
import com.unlcn.ils.wms.base.model.bigscreen.BiDistributionExample;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface BiDistributionExtMapper {

    /**
     * 配送推移列表统计
     * @return
     */
    List<BiDistribution> distributionLineCount();
}