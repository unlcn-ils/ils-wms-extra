package com.unlcn.ils.wms.base.mapper.extmapper;

import com.unlcn.ils.wms.base.businessDTO.inbound.AsnOrderDTO;
import com.unlcn.ils.wms.base.dto.InboundExcelDTO;
import com.unlcn.ils.wms.base.dto.InboundInfoDTO;
import com.unlcn.ils.wms.base.dto.WmsInboundOrderDetailDTO;
import com.unlcn.ils.wms.base.dto.WmsInboundOrderInsertProcedureDTO;
import com.unlcn.ils.wms.base.dto.WmsPreparationPlanResultDTO;
import com.unlcn.ils.wms.base.model.inbound.WmsInboundOrder;
import com.unlcn.ils.wms.base.model.inbound.WmsInboundOrderDetail;
import com.unlcn.ils.wms.base.model.stock.WmsInventoryLocation;
import org.apache.ibatis.annotations.Param;

import java.util.HashMap;
import java.util.List;

public interface WmsInboundOrderDetailExtMapper {

    /**
     * 根据车架号查询入库订单信息
     *
     * @param vin
     * @return
     */
    List<InboundInfoDTO> inboundInfo(@Param("vin") String vin);

    List<WmsInventoryLocation> selectByOrderNO(@Param("orderno") String orderno, @Param("del") byte del, @Param("ob") String ob, @Param("oe") String oe);

    List<WmsInventoryLocation> selectByParam(@Param("param") HashMap<String, Object> param, @Param("del") byte del);

    void updateWmsOrderDeatailBatch(List<WmsPreparationPlanResultDTO> details);

    List<WmsInboundOrderDetailDTO> selectDetailsByParam(HashMap<String, Object> paramMap);

    List<AsnOrderDTO> selectOrderByParam(HashMap<String, Object> paramMap);

    void updateOrderSendFlagBatch(List<InboundExcelDTO> inboundExcelList);

    void insertInboundOrderDetailByProcedure(WmsInboundOrderDetail orderDetail);

    void insertInboundOrderByProcedure(WmsInboundOrderInsertProcedureDTO procedureDTO);

}