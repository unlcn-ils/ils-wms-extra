package com.unlcn.ils.wms.web.controller.annotation;

import java.lang.annotation.*;

/**
 * Created by DELL on 2017/9/12.
 */
@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.PARAMETER, ElementType.METHOD})
@Documented
public @interface LogAnnotation {
    //模块名
    String modules() default "";

    String methods() default "";
}
