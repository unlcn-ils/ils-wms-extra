package com.unlcn.ils.wms.base.dto;

import java.io.Serializable;
import java.util.Date;

public class WmsHandoverOrderOperationListResultDTO implements Serializable {

    private Long dataId;

    private String gbno;

    private String vbeln;

    private Long posnr;

    private String zaction;

    private String lgort;

    private String zlgort;

    private String matnr;

    private String zmode;

    private String zset;

    private String zcol;

    private String werks;

    private String vstel;

    private String sernr;

    private String zsernr;

    private String zdoc;

    private Date mbdat;

    private String sendStatus;

    private String createUserName;

    private Date gmtCreate;

    private String modifyUserName;

    private Date gmtUpdate;

    private String sendStatusSap;

    private String resultMsgDcs;

    private String resultMsgSap;

    private String preparationStatus;

    private String sendStatusCrm;

    private String resultMsgCrm;

    private String finalDcsStatus;

    private String finalCrmStatus;

    private String finalSapStatus;

    public Long getDataId() {
        return dataId;
    }

    public void setDataId(Long dataId) {
        this.dataId = dataId;
    }

    public String getGbno() {
        return gbno;
    }

    public void setGbno(String gbno) {
        this.gbno = gbno;
    }

    public String getVbeln() {
        return vbeln;
    }

    public void setVbeln(String vbeln) {
        this.vbeln = vbeln;
    }

    public Long getPosnr() {
        return posnr;
    }

    public void setPosnr(Long posnr) {
        this.posnr = posnr;
    }

    public String getZaction() {
        return zaction;
    }

    public void setZaction(String zaction) {
        this.zaction = zaction;
    }

    public String getLgort() {
        return lgort;
    }

    public void setLgort(String lgort) {
        this.lgort = lgort;
    }

    public String getZlgort() {
        return zlgort;
    }

    public void setZlgort(String zlgort) {
        this.zlgort = zlgort;
    }

    public String getMatnr() {
        return matnr;
    }

    public void setMatnr(String matnr) {
        this.matnr = matnr;
    }

    public String getZmode() {
        return zmode;
    }

    public void setZmode(String zmode) {
        this.zmode = zmode;
    }

    public String getZset() {
        return zset;
    }

    public void setZset(String zset) {
        this.zset = zset;
    }

    public String getZcol() {
        return zcol;
    }

    public void setZcol(String zcol) {
        this.zcol = zcol;
    }

    public String getWerks() {
        return werks;
    }

    public void setWerks(String werks) {
        this.werks = werks;
    }

    public String getVstel() {
        return vstel;
    }

    public void setVstel(String vstel) {
        this.vstel = vstel;
    }

    public String getSernr() {
        return sernr;
    }

    public void setSernr(String sernr) {
        this.sernr = sernr;
    }

    public String getZsernr() {
        return zsernr;
    }

    public void setZsernr(String zsernr) {
        this.zsernr = zsernr;
    }

    public String getZdoc() {
        return zdoc;
    }

    public void setZdoc(String zdoc) {
        this.zdoc = zdoc;
    }

    public Date getMbdat() {
        return mbdat;
    }

    public void setMbdat(Date mbdat) {
        this.mbdat = mbdat;
    }

    public String getSendStatus() {
        return sendStatus;
    }

    public void setSendStatus(String sendStatus) {
        this.sendStatus = sendStatus;
    }

    public String getCreateUserName() {
        return createUserName;
    }

    public void setCreateUserName(String createUserName) {
        this.createUserName = createUserName;
    }

    public Date getGmtCreate() {
        return gmtCreate;
    }

    public void setGmtCreate(Date gmtCreate) {
        this.gmtCreate = gmtCreate;
    }

    public String getModifyUserName() {
        return modifyUserName;
    }

    public void setModifyUserName(String modifyUserName) {
        this.modifyUserName = modifyUserName;
    }

    public Date getGmtUpdate() {
        return gmtUpdate;
    }

    public void setGmtUpdate(Date gmtUpdate) {
        this.gmtUpdate = gmtUpdate;
    }

    public String getSendStatusSap() {
        return sendStatusSap;
    }

    public void setSendStatusSap(String sendStatusSap) {
        this.sendStatusSap = sendStatusSap;
    }

    public String getResultMsgDcs() {
        return resultMsgDcs;
    }

    public void setResultMsgDcs(String resultMsgDcs) {
        this.resultMsgDcs = resultMsgDcs;
    }

    public String getResultMsgSap() {
        return resultMsgSap;
    }

    public void setResultMsgSap(String resultMsgSap) {
        this.resultMsgSap = resultMsgSap;
    }

    public String getPreparationStatus() {
        return preparationStatus;
    }

    public void setPreparationStatus(String preparationStatus) {
        this.preparationStatus = preparationStatus;
    }

    public String getSendStatusCrm() {
        return sendStatusCrm;
    }

    public void setSendStatusCrm(String sendStatusCrm) {
        this.sendStatusCrm = sendStatusCrm;
    }

    public String getResultMsgCrm() {
        return resultMsgCrm;
    }

    public void setResultMsgCrm(String resultMsgCrm) {
        this.resultMsgCrm = resultMsgCrm;
    }

    public String getFinalDcsStatus() {
        return finalDcsStatus;
    }

    public void setFinalDcsStatus(String finalDcsStatus) {
        this.finalDcsStatus = finalDcsStatus;
    }

    public String getFinalCrmStatus() {
        return finalCrmStatus;
    }

    public void setFinalCrmStatus(String finalCrmStatus) {
        this.finalCrmStatus = finalCrmStatus;
    }

    public String getFinalSapStatus() {
        return finalSapStatus;
    }

    public void setFinalSapStatus(String finalSapStatus) {
        this.finalSapStatus = finalSapStatus;
    }
}
