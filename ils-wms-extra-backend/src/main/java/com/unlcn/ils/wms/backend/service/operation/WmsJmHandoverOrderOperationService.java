package com.unlcn.ils.wms.backend.service.operation;

import cn.huiyunche.commons.domain.ResultDTOWithPagination;
import com.unlcn.ils.wms.base.dto.WmsHandoverOrderOperationListResultDTO;
import com.unlcn.ils.wms.base.dto.WmsJmHandoverOrderOperationParamDTO;

import java.util.List;

public interface WmsJmHandoverOrderOperationService {
    ResultDTOWithPagination<List<WmsHandoverOrderOperationListResultDTO>> listHandoverOrderExcp(WmsJmHandoverOrderOperationParamDTO paramDTO) throws Exception;

    void updateSendToDCSAgain(WmsJmHandoverOrderOperationParamDTO dto) throws Exception;

    void updateSendToSAPAgain(WmsJmHandoverOrderOperationParamDTO dto) throws Exception;

    void updateSendToCRMAgain(WmsJmHandoverOrderOperationParamDTO dto) throws Exception;

    void updateSendDCSToSuccess(WmsJmHandoverOrderOperationParamDTO dto) throws Exception;

    void updateSendSAPToSuccess(WmsJmHandoverOrderOperationParamDTO dto) throws  Exception;

    void updateSendCRMToSuccess(WmsJmHandoverOrderOperationParamDTO dto)throws Exception;
}
