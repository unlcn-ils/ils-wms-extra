package com.unlcn.ils.wms.base.businessDTO.inbound;

import java.io.Serializable;
import java.util.Date;

/**
 * Created by DELL on 2017/8/11.
 */
public class AsnOrderDetailDTO implements Serializable {
    private String factoryWhno;//工厂移车库
    private String oddStockTransfer;//是否中转标识
    private String oddStockTransferStatus; //中转处理状态
    /**
     * 详情主键ID
     */
    private Long oddId;

    /**
     * 关联主表ID
     */
    private Long oddOdId;

    /**
     * 关联主表CODE
     */
    private String oddOdCode;

    /**
     * 车品牌CODE
     */
    private String oddVehicleBrandCode;

    /**
     * 车品牌名称
     */
    private String oddVehicleBrandName;

    /**
     * 车系CODE
     */
    private String oddVehicleSeriesCode;

    /**
     * 车系名称
     */
    private String oddVehicleSeriesName;

    /**
     * 车型CODE
     */
    private String oddVehicleSpecCode;

    /**
     * 车型名称
     */
    private String oddVehicleSpecName;

    /**
     * 车型描述
     */
    private String oddVehicleSpecDesc;

    /**
     * 车牌号
     */
    private String oddVehiclePlate;

    /**
     * 底盘号
     */
    private String oddVin;

    /**
     * 发动机号
     */
    private String oddEngine;

    /**
     * 仓库CODE
     */
    private String oddWhCode;

    /**
     * 仓库名称
     */
    private String oddWhName;

    /**
     * 库区CODE
     */
    private String oddWhZoneCode;

    /**
     * 库区名称
     */
    private String oddWhZoneName;

    /**
     * 库位CODE
     */
    private String oddWhLocCode;

    /**
     * 库位名称
     */
    private String oddWhLocName;

    /**
     * 车长
     */
    private Long oddVehicleLength;

    /**
     * 车宽
     */
    private Long oddVehicleWidth;

    /**
     * 车高
     */
    private Long oddVehicleHigh;

    /**
     * 物料代码
     */
    private String oddMaterialCode;

    /**
     * 物料名称
     */
    private String oddMaterialName;

    /**
     * 颜色代码
     */
    private String oddCarColourCode;

    /**
     * 颜色名称
     */
    private String oddCarColour;

    /**
     * 变速箱号
     */
    private String oddGearboxNumber;

    /**
     * 合格证
     */
    private String oddCertification;

    /**
     * 生产日期
     */
    private Date oddProductionDate;

    /**
     * 下线日期
     */
    private Date oddOfflineDate;


    public String getFactoryWhno() {
        return factoryWhno;
    }

    public void setFactoryWhno(String factoryWhno) {
        this.factoryWhno = factoryWhno;
    }

    public String getOddStockTransfer() {
        return oddStockTransfer;
    }

    public void setOddStockTransfer(String oddStockTransfer) {
        this.oddStockTransfer = oddStockTransfer;
    }

    public String getOddStockTransferStatus() {
        return oddStockTransferStatus;
    }

    public void setOddStockTransferStatus(String oddStockTransferStatus) {
        this.oddStockTransferStatus = oddStockTransferStatus;
    }

    public Long getOddId() {
        return oddId;
    }

    public void setOddId(Long oddId) {
        this.oddId = oddId;
    }

    public Long getOddOdId() {
        return oddOdId;
    }

    public void setOddOdId(Long oddOdId) {
        this.oddOdId = oddOdId;
    }

    public String getOddOdCode() {
        return oddOdCode;
    }

    public void setOddOdCode(String oddOdCode) {
        this.oddOdCode = oddOdCode;
    }

    public String getOddVehicleBrandCode() {
        return oddVehicleBrandCode;
    }

    public void setOddVehicleBrandCode(String oddVehicleBrandCode) {
        this.oddVehicleBrandCode = oddVehicleBrandCode;
    }

    public String getOddVehicleBrandName() {
        return oddVehicleBrandName;
    }

    public void setOddVehicleBrandName(String oddVehicleBrandName) {
        this.oddVehicleBrandName = oddVehicleBrandName;
    }

    public String getOddVehicleSeriesCode() {
        return oddVehicleSeriesCode;
    }

    public void setOddVehicleSeriesCode(String oddVehicleSeriesCode) {
        this.oddVehicleSeriesCode = oddVehicleSeriesCode;
    }

    public String getOddVehicleSeriesName() {
        return oddVehicleSeriesName;
    }

    public void setOddVehicleSeriesName(String oddVehicleSeriesName) {
        this.oddVehicleSeriesName = oddVehicleSeriesName;
    }

    public String getOddVehicleSpecCode() {
        return oddVehicleSpecCode;
    }

    public void setOddVehicleSpecCode(String oddVehicleSpecCode) {
        this.oddVehicleSpecCode = oddVehicleSpecCode;
    }

    public String getOddVehicleSpecName() {
        return oddVehicleSpecName;
    }

    public void setOddVehicleSpecName(String oddVehicleSpecName) {
        this.oddVehicleSpecName = oddVehicleSpecName;
    }

    public String getOddVehicleSpecDesc() {
        return oddVehicleSpecDesc;
    }

    public void setOddVehicleSpecDesc(String oddVehicleSpecDesc) {
        this.oddVehicleSpecDesc = oddVehicleSpecDesc;
    }

    public String getOddVehiclePlate() {
        return oddVehiclePlate;
    }

    public void setOddVehiclePlate(String oddVehiclePlate) {
        this.oddVehiclePlate = oddVehiclePlate;
    }

    public String getOddVin() {
        return oddVin;
    }

    public void setOddVin(String oddVin) {
        this.oddVin = oddVin;
    }

    public String getOddEngine() {
        return oddEngine;
    }

    public void setOddEngine(String oddEngine) {
        this.oddEngine = oddEngine;
    }

    public String getOddWhCode() {
        return oddWhCode;
    }

    public void setOddWhCode(String oddWhCode) {
        this.oddWhCode = oddWhCode;
    }

    public String getOddWhName() {
        return oddWhName;
    }

    public void setOddWhName(String oddWhName) {
        this.oddWhName = oddWhName;
    }

    public String getOddWhZoneCode() {
        return oddWhZoneCode;
    }

    public void setOddWhZoneCode(String oddWhZoneCode) {
        this.oddWhZoneCode = oddWhZoneCode;
    }

    public String getOddWhZoneName() {
        return oddWhZoneName;
    }

    public void setOddWhZoneName(String oddWhZoneName) {
        this.oddWhZoneName = oddWhZoneName;
    }

    public String getOddWhLocCode() {
        return oddWhLocCode;
    }

    public void setOddWhLocCode(String oddWhLocCode) {
        this.oddWhLocCode = oddWhLocCode;
    }

    public String getOddWhLocName() {
        return oddWhLocName;
    }

    public void setOddWhLocName(String oddWhLocName) {
        this.oddWhLocName = oddWhLocName;
    }

    public Long getOddVehicleLength() {
        return oddVehicleLength;
    }

    public void setOddVehicleLength(Long oddVehicleLength) {
        this.oddVehicleLength = oddVehicleLength;
    }

    public Long getOddVehicleWidth() {
        return oddVehicleWidth;
    }

    public void setOddVehicleWidth(Long oddVehicleWidth) {
        this.oddVehicleWidth = oddVehicleWidth;
    }

    public Long getOddVehicleHigh() {
        return oddVehicleHigh;
    }

    public void setOddVehicleHigh(Long oddVehicleHigh) {
        this.oddVehicleHigh = oddVehicleHigh;
    }

    public String getOddMaterialCode() {
        return oddMaterialCode;
    }

    public void setOddMaterialCode(String oddMaterialCode) {
        this.oddMaterialCode = oddMaterialCode;
    }

    public String getOddMaterialName() {
        return oddMaterialName;
    }

    public void setOddMaterialName(String oddMaterialName) {
        this.oddMaterialName = oddMaterialName;
    }

    public String getOddCarColourCode() {
        return oddCarColourCode;
    }

    public void setOddCarColourCode(String oddCarColourCode) {
        this.oddCarColourCode = oddCarColourCode;
    }

    public String getOddCarColour() {
        return oddCarColour;
    }

    public void setOddCarColour(String oddCarColour) {
        this.oddCarColour = oddCarColour;
    }

    public String getOddGearboxNumber() {
        return oddGearboxNumber;
    }

    public void setOddGearboxNumber(String oddGearboxNumber) {
        this.oddGearboxNumber = oddGearboxNumber;
    }

    public String getOddCertification() {
        return oddCertification;
    }

    public void setOddCertification(String oddCertification) {
        this.oddCertification = oddCertification;
    }

    public Date getOddProductionDate() {
        return oddProductionDate;
    }

    public void setOddProductionDate(Date oddProductionDate) {
        this.oddProductionDate = oddProductionDate;
    }

    public Date getOddOfflineDate() {
        return oddOfflineDate;
    }

    public void setOddOfflineDate(Date oddOfflineDate) {
        this.oddOfflineDate = oddOfflineDate;
    }
}
