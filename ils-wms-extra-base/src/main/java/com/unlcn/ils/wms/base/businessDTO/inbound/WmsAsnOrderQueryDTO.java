package com.unlcn.ils.wms.base.businessDTO.inbound;

import cn.huiyunche.commons.domain.PageVo;

import java.util.Date;

/**
 * Created by DELL on 2017/8/17.
 */
public class WmsAsnOrderQueryDTO extends PageVo{
    /**
     * 入库订单主ID
     */
    private Long odId;

    /**
     * 订单CODE,WMS专用
     */
    private String odCode;

    /**
     * 货主ID
     */
    private Long odCustomerId;

    /**
     * 货主CODE
     */
    private String odCustomerCode;

    /**
     * 货主名称
     */
    private String odCustomerName;

    /**
     * 调度单号
     */
    private String odDispatchNo;

    /**
     * 运单号
     */
    private String odWaybillNo;

    /**
     * 运单类型
     */
    private String odWaybillType;

    /**
     * 订单状态
     */
    private String odStatus;

    /**
     * 提货地址
     */
    private String odPickUpAddr;

    /**
     * 仓库ID
     */
    private Long odWhId;

    /**
     * 仓库CODE
     */
    private String odWhCode;

    /**
     * 订单商品数量
     */
    private Long odInboundNum;

    /**
     * 交货方
     */
    private String odSupplierParty;

    /**
     * 交货方CODE
     */
    private String odSupplierCode;

    /**
     * 交货方名称
     */
    private String odSupplierName;

    /**
     * 交货方联系人手机号
     */
    private String odSupplierPhone;

    /**
     * 收货人ID
     */
    private String odConsigneeId;

    /**
     * 收货人CODE
     */
    private String odConsigneeCode;

    /**
     * 收货人名称
     */
    private String odConsigneeName;

    /**
     * 收货时间
     */
    private String odConsigneeDate;

    /**
     * 检验结果
     */
    private String odCheckResult;

    /**
     * 检验描述
     */
    private String odCheckDesc;

    /**
     * 起始地code
     */
    private String odOriginCode;

    /**
     * 起始地
     */
    private String odOrigin;

    /**
     * 目的地code
     */
    private String odDestCode;

    /**
     * 目的地
     */
    private String odDest;

    /**
     * 备注
     */
    private String odRemark;

    /**
     * 创建开始日期
     */
    private String createStartDate;

    /**
     * 创建结束日期
     */
    private String createEndDate;

    /**
     * 收货开始日期
     */
    private String consigneeStartDate;

    /**
     * 收货结束日期
     */
    private String consigneeEndDate;

    private String createUserId;

    private String createUserName;

    private String modifyUserId;

    private String modifyUserName;

    private Date gmtCreate;

    private Date gmtUpdate;

    private Byte isDeleted;

    private Long versions;

    /**
     * 维修方
     */
    private String odRepairParty;


    private String oddVehicleSpecCode;

    private String oddVehicleSpecName;

    private String oddVin;


    private int limitStart = -1;

    private int limitEnd = -1;

    public Long getOdId() {
        return odId;
    }

    public void setOdId(Long odId) {
        this.odId = odId;
    }

    public String getOdCode() {
        return odCode;
    }

    public void setOdCode(String odCode) {
        this.odCode = odCode;
    }

    public Long getOdCustomerId() {
        return odCustomerId;
    }

    public void setOdCustomerId(Long odCustomerId) {
        this.odCustomerId = odCustomerId;
    }

    public String getOdCustomerCode() {
        return odCustomerCode;
    }

    public void setOdCustomerCode(String odCustomerCode) {
        this.odCustomerCode = odCustomerCode;
    }

    public String getOdCustomerName() {
        return odCustomerName;
    }

    public void setOdCustomerName(String odCustomerName) {
        this.odCustomerName = odCustomerName;
    }

    public String getOdDispatchNo() {
        return odDispatchNo;
    }

    public void setOdDispatchNo(String odDispatchNo) {
        this.odDispatchNo = odDispatchNo;
    }

    public String getOdWaybillNo() {
        return odWaybillNo;
    }

    public void setOdWaybillNo(String odWaybillNo) {
        this.odWaybillNo = odWaybillNo;
    }

    public String getOdWaybillType() {
        return odWaybillType;
    }

    public void setOdWaybillType(String odWaybillType) {
        this.odWaybillType = odWaybillType;
    }

    public String getOdStatus() {
        return odStatus;
    }

    public void setOdStatus(String odStatus) {
        this.odStatus = odStatus;
    }

    public String getOdPickUpAddr() {
        return odPickUpAddr;
    }

    public void setOdPickUpAddr(String odPickUpAddr) {
        this.odPickUpAddr = odPickUpAddr;
    }

    public Long getOdWhId() {
        return odWhId;
    }

    public void setOdWhId(Long odWhId) {
        this.odWhId = odWhId;
    }

    public String getOdWhCode() {
        return odWhCode;
    }

    public void setOdWhCode(String odWhCode) {
        this.odWhCode = odWhCode;
    }

    public Long getOdInboundNum() {
        return odInboundNum;
    }

    public void setOdInboundNum(Long odInboundNum) {
        this.odInboundNum = odInboundNum;
    }

    public String getOdSupplierParty() {
        return odSupplierParty;
    }

    public void setOdSupplierParty(String odSupplierParty) {
        this.odSupplierParty = odSupplierParty;
    }

    public String getOdSupplierCode() {
        return odSupplierCode;
    }

    public void setOdSupplierCode(String odSupplierCode) {
        this.odSupplierCode = odSupplierCode;
    }

    public String getOdSupplierName() {
        return odSupplierName;
    }

    public void setOdSupplierName(String odSupplierName) {
        this.odSupplierName = odSupplierName;
    }

    public String getOdSupplierPhone() {
        return odSupplierPhone;
    }

    public void setOdSupplierPhone(String odSupplierPhone) {
        this.odSupplierPhone = odSupplierPhone;
    }

    public String getOdConsigneeId() {
        return odConsigneeId;
    }

    public void setOdConsigneeId(String odConsigneeId) {
        this.odConsigneeId = odConsigneeId;
    }

    public String getOdConsigneeCode() {
        return odConsigneeCode;
    }

    public void setOdConsigneeCode(String odConsigneeCode) {
        this.odConsigneeCode = odConsigneeCode;
    }

    public String getOdConsigneeName() {
        return odConsigneeName;
    }

    public void setOdConsigneeName(String odConsigneeName) {
        this.odConsigneeName = odConsigneeName;
    }

    public String getOdConsigneeDate() {
        return odConsigneeDate;
    }

    public void setOdConsigneeDate(String odConsigneeDate) {
        this.odConsigneeDate = odConsigneeDate;
    }

    public String getOdCheckResult() {
        return odCheckResult;
    }

    public void setOdCheckResult(String odCheckResult) {
        this.odCheckResult = odCheckResult;
    }

    public String getOdCheckDesc() {
        return odCheckDesc;
    }

    public void setOdCheckDesc(String odCheckDesc) {
        this.odCheckDesc = odCheckDesc;
    }

    public String getOdOriginCode() {
        return odOriginCode;
    }

    public void setOdOriginCode(String odOriginCode) {
        this.odOriginCode = odOriginCode;
    }

    public String getOdDestCode() {
        return odDestCode;
    }

    public void setOdDestCode(String odDestCode) {
        this.odDestCode = odDestCode;
    }

    public String getOdDest() {
        return odDest;
    }

    public void setOdDest(String odDest) {
        this.odDest = odDest;
    }

    public String getOdOrigin() {
        return odOrigin;
    }

    public void setOdOrigin(String odOrigin) {
        this.odOrigin = odOrigin;
    }

    public String getOdRemark() {
        return odRemark;
    }

    public void setOdRemark(String odRemark) {
        this.odRemark = odRemark;
    }

    public String getCreateStartDate() {
        return createStartDate;
    }

    public void setCreateStartDate(String createStartDate) {
        this.createStartDate = createStartDate;
    }

    public String getCreateEndDate() {
        return createEndDate;
    }

    public void setCreateEndDate(String createEndDate) {
        this.createEndDate = createEndDate;
    }

    public String getConsigneeStartDate() {
        return consigneeStartDate;
    }

    public void setConsigneeStartDate(String consigneeStartDate) {
        this.consigneeStartDate = consigneeStartDate;
    }

    public String getConsigneeEndDate() {
        return consigneeEndDate;
    }

    public void setConsigneeEndDate(String consigneeEndDate) {
        this.consigneeEndDate = consigneeEndDate;
    }

    public String getCreateUserId() {
        return createUserId;
    }

    public void setCreateUserId(String createUserId) {
        this.createUserId = createUserId;
    }

    public String getCreateUserName() {
        return createUserName;
    }

    public void setCreateUserName(String createUserName) {
        this.createUserName = createUserName;
    }

    public String getModifyUserId() {
        return modifyUserId;
    }

    public void setModifyUserId(String modifyUserId) {
        this.modifyUserId = modifyUserId;
    }

    public String getModifyUserName() {
        return modifyUserName;
    }

    public void setModifyUserName(String modifyUserName) {
        this.modifyUserName = modifyUserName;
    }

    public Date getGmtCreate() {
        return gmtCreate;
    }

    public void setGmtCreate(Date gmtCreate) {
        this.gmtCreate = gmtCreate;
    }

    public Date getGmtUpdate() {
        return gmtUpdate;
    }

    public void setGmtUpdate(Date gmtUpdate) {
        this.gmtUpdate = gmtUpdate;
    }

    public Byte getIsDeleted() {
        return isDeleted;
    }

    public void setIsDeleted(Byte isDeleted) {
        this.isDeleted = isDeleted;
    }

    public Long getVersions() {
        return versions;
    }

    public void setVersions(Long versions) {
        this.versions = versions;
    }

    public String getOdRepairParty() {
        return odRepairParty;
    }

    public void setOdRepairParty(String odRepairParty) {
        this.odRepairParty = odRepairParty;
    }

    public String getOddVehicleSpecCode() {
        return oddVehicleSpecCode;
    }

    public void setOddVehicleSpecCode(String oddVehicleSpecCode) {
        this.oddVehicleSpecCode = oddVehicleSpecCode;
    }

    public String getOddVehicleSpecName() {
        return oddVehicleSpecName;
    }

    public void setOddVehicleSpecName(String oddVehicleSpecName) {
        this.oddVehicleSpecName = oddVehicleSpecName;
    }

    public String getOddVin() {
        return oddVin;
    }

    public void setOddVin(String oddVin) {
        this.oddVin = oddVin;
    }

    public int getLimitStart() {
        return limitStart;
    }

    public void setLimitStart(int limitStart) {
        this.limitStart = limitStart;
    }

    public int getLimitEnd() {
        return limitEnd;
    }

    public void setLimitEnd(int limitEnd) {
        this.limitEnd = limitEnd;
    }

    @Override
    public String toString() {
        return "WmsAsnOrderQueryDTO{" +
                "odId=" + odId +
                ", odCode='" + odCode + '\'' +
                ", odCustomerId=" + odCustomerId +
                ", odCustomerCode='" + odCustomerCode + '\'' +
                ", odCustomerName='" + odCustomerName + '\'' +
                ", odDispatchNo='" + odDispatchNo + '\'' +
                ", odWaybillNo='" + odWaybillNo + '\'' +
                ", odWaybillType='" + odWaybillType + '\'' +
                ", odStatus='" + odStatus + '\'' +
                ", odPickUpAddr='" + odPickUpAddr + '\'' +
                ", odWhId=" + odWhId +
                ", odWhCode='" + odWhCode + '\'' +
                ", odInboundNum=" + odInboundNum +
                ", odSupplierParty='" + odSupplierParty + '\'' +
                ", odSupplierCode='" + odSupplierCode + '\'' +
                ", odSupplierName='" + odSupplierName + '\'' +
                ", odSupplierPhone='" + odSupplierPhone + '\'' +
                ", odConsigneeId='" + odConsigneeId + '\'' +
                ", odConsigneeCode='" + odConsigneeCode + '\'' +
                ", odConsigneeName='" + odConsigneeName + '\'' +
                ", odConsigneeDate='" + odConsigneeDate + '\'' +
                ", odCheckResult='" + odCheckResult + '\'' +
                ", odCheckDesc='" + odCheckDesc + '\'' +
                ", odOriginCode='" + odOriginCode + '\'' +
                ", odOrigin='" + odOrigin + '\'' +
                ", odDestCode='" + odDestCode + '\'' +
                ", odDest='" + odDest + '\'' +
                ", odRemark='" + odRemark + '\'' +
                ", createStartDate='" + createStartDate + '\'' +
                ", createEndDate='" + createEndDate + '\'' +
                ", consigneeStartDate='" + consigneeStartDate + '\'' +
                ", consigneeEndDate='" + consigneeEndDate + '\'' +
                ", createUserId='" + createUserId + '\'' +
                ", createUserName='" + createUserName + '\'' +
                ", modifyUserId='" + modifyUserId + '\'' +
                ", modifyUserName='" + modifyUserName + '\'' +
                ", gmtCreate=" + gmtCreate +
                ", gmtUpdate=" + gmtUpdate +
                ", isDeleted=" + isDeleted +
                ", versions=" + versions +
                ", odRepairParty='" + odRepairParty + '\'' +
                ", oddVehicleSpecCode='" + oddVehicleSpecCode + '\'' +
                ", oddVehicleSpecName='" + oddVehicleSpecName + '\'' +
                ", oddVin='" + oddVin + '\'' +
                ", limitStart=" + limitStart +
                ", limitEnd=" + limitEnd +
                '}';
    }
}
