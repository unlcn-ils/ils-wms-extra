package com.unlcn.ils.wms.web.vo.outbound;

import java.util.List;

/**
 * @Auther linbao
 * @Date 2017-10-25
 */
public class OutboundTaskConfirmFormVO{

    /**
     * 条形码内容
     */
    private String barCode;

    private List<String> vinList;//已有的车架号集合

    private String vin;//需要出库的车架号

    public List<String> getVinList() {
        return vinList;
    }

    public void setVinList(List<String> vinList) {
        this.vinList = vinList;
    }

    public String getVin() {
        return vin;
    }

    public void setVin(String vin) {
        this.vin = vin;
    }

    public String getBarCode() {
        return barCode;
    }

    public void setBarCode(String barCode) {
        this.barCode = barCode;
    }

    @Override
    public String toString() {
        return "OutboundTaskConfirmFormVO{" +
                "barCode='" + barCode + '\'' +
                ", vinList=" + vinList +
                ", vin='" + vin + '\'' +
                '}';
    }
}
