package com.unlcn.ils.wms.backend.enums;

/**
 * Created by DELL on 2017/8/26.
 */
public enum WmsOutboundTaskStatusEnum {
    WMS_OUTBOUND_TASK_STATUS_NEW(10,"未领取"),
    WMS_OUTBOUND_TASK_STATUS_READY(20,"待开始"),
    WMS_OUTBOUND_TASK_STATUS_GOING(30,"进行中"),
    WMS_OUTBOUND_TASK_STATUS_FINISHED(40,"已完成"),
    WMS_OUTBOUND_TASK_CANCLE(50, "已取消");

    private final int value;
    private final String text;
    WmsOutboundTaskStatusEnum(int value, String text){
        this.value = value;
        this.text = text;
    }

    public int getValue() {
        return value;
    }

    public String getText() {
        return text;
    }

    public static WmsOutboundTaskStatusEnum getByValue(int value){
        for (WmsOutboundTaskStatusEnum temp : WmsOutboundTaskStatusEnum.values()) {
            if(temp.getValue() == value){
                return temp;
            }
        }
        return null;
    }
}
