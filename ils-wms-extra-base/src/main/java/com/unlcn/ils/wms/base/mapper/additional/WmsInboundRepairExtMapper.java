package com.unlcn.ils.wms.base.mapper.additional;

import com.unlcn.ils.wms.base.model.inbound.WmsInboundRepair;

import java.util.List;
import java.util.Map;

/**
 * Created by DELL on 2017/8/18.
 */
public interface WmsInboundRepairExtMapper {

    List<WmsInboundRepair> queryForList(Map<String,Object> paramMap);

    Integer queryForCount(Map<String,Object> paramMap);

    /**
     * 根据维修单ID修改维修单状态为维修中20
     * @param rpId
     */
    void updateRepairStatusById(Long rpId);
}
