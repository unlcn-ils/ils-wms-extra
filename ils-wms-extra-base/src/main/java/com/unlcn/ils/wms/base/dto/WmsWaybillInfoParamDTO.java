package com.unlcn.ils.wms.base.dto;

import java.io.Serializable;

/**
 * app 移车申请页面 获取车辆信息的参数封装
 * <p>
 * 2018-1-30 新需求:根据车架号模糊匹配功能
 * </p>
 */
public class WmsWaybillInfoParamDTO implements Serializable {
    private String vin;
    private String whCode;

    public String getVin() {
        return vin;
    }

    public void setVin(String vin) {
        this.vin = vin;
    }

    public String getWhCode() {
        return whCode;
    }

    public void setWhCode(String whCode) {
        this.whCode = whCode;
    }
}
