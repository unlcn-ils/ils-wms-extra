package com.unlcn.ils.wms.backend.enums;

/**
 * @Auther linbao
 * @Date 2017-10-25
 */
public enum RepairInOutStatusEnum {
    NO_SEND("10", "未出库"),
    HAVE_SEND("20", "已出库");

    private final String code;

    private final String value;

    public String getCode() {
        return code;
    }

    public String getValue() {
        return value;
    }

    RepairInOutStatusEnum(String code, String value) {
        this.code = code;
        this.value = value;
    }
}
