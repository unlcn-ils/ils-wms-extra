package com.unlcn.ils.wms.base.mapper.extmapper;

import com.unlcn.ils.wms.base.model.inbound.WmsTmsOrder;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public interface WmsTmsOrderExtMapper {
    List<WmsTmsOrder> selectLastHTTPTimeStampByParam(Map<String, Object> paramMap);

    List<WmsTmsOrder> selectLastRESTInfoByParam(Map<String, Object> paramMap);

    List<WmsTmsOrder> selectLastHTTPInfoByParam(Map<String, Object> paramMap);

    List<WmsTmsOrder> selectVinListLikeByParam(Map<String, Object> paramMap);

    void insertDataBatch(ArrayList<WmsTmsOrder> list);

    void updateSetSqlMode();

}
