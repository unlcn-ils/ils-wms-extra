package com.unlcn.ils.wms.backend.enums;

public enum WmsCustomerEnum {
    JM("JM客户", "君马销售有限公司"),
    CQ_JL("CQ-JL", "江铃股份"),
    WMS_ALL("WMS_ALL", "WMS所有客户");

    private final String code;
    private final String name;

    WmsCustomerEnum(String code, String name) {
        this.code = code;
        this.name = name;
    }

    public String getCode() {
        return code;
    }

    public String getName() {
        return name;
    }

    public static WmsCustomerEnum getByCode(String code) {
        for (WmsCustomerEnum temp : WmsCustomerEnum.values()) {
            if (temp.getCode().equals(code)) {
                return temp;
            }
        }
        return null;
    }
}
