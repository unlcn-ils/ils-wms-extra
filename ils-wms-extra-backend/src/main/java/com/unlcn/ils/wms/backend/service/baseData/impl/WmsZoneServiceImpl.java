package com.unlcn.ils.wms.backend.service.baseData.impl;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.unlcn.ils.wms.backend.bo.baseDataBO.WmsZoneBO;
import com.unlcn.ils.wms.backend.service.baseData.WmsZoneService;
import com.unlcn.ils.wms.backend.service.inbound.impl.AsnOrderServiceImpl;
import com.unlcn.ils.wms.backend.util.ObjectToMapUtils;
import com.unlcn.ils.wms.base.businessDTO.baseData.WmsZoneQueryDTO;
import com.unlcn.ils.wms.base.mapper.additional.WmsZoneExtMapper;
import com.unlcn.ils.wms.base.mapper.stock.WmsZoneMapper;
import com.unlcn.ils.wms.base.model.stock.WmsZone;
import com.unlcn.ils.wms.base.model.stock.WmsZoneExample;
import org.apache.commons.collections4.CollectionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;
import java.util.Objects;

/**
 * 库区增删改查
 * Created by DELL on 2017/8/18.
 */
@Service
public class WmsZoneServiceImpl implements WmsZoneService {
    private Logger LOGGER = LoggerFactory.getLogger(AsnOrderServiceImpl.class);
    @Autowired
    private WmsZoneExtMapper wmsZoneExtMapper;

    @Autowired
    private WmsZoneMapper wmsZoneMapper;

    @Override
    public Map<String, Object> listZone(WmsZoneQueryDTO wmsZoneQueryDTO) throws IllegalAccessException {

        if (Objects.equals(wmsZoneQueryDTO, null)) {
            LOGGER.info("WmsZoneServiceImpl.wmsZoneQueryDTO must not be null");
            throw new IllegalArgumentException("分页参数不能为空");
        }
        Map<String,Object> resultMap = Maps.newHashMap();

        wmsZoneQueryDTO.setLimitStart(wmsZoneQueryDTO.getStartIndex());
        wmsZoneQueryDTO.setLimitEnd(wmsZoneQueryDTO.getPageSize());

        Map<String,Object> paramMap = ObjectToMapUtils.objectToMap(wmsZoneQueryDTO);
        List<WmsZone> infos = wmsZoneExtMapper.queryForList(paramMap);
        List<WmsZoneBO> bos = Lists.newArrayList();
        if (CollectionUtils.isNotEmpty(infos)) {
            infos.stream().forEach(v -> {
                WmsZoneBO bo = new WmsZoneBO();
                BeanUtils.copyProperties(v, bo);
                bos.add(bo);
            });
            resultMap.put("wmsZoneList", bos);
        }
        wmsZoneQueryDTO.setPageNo(wmsZoneQueryDTO.getStartIndex());
        wmsZoneQueryDTO.setPageSize(wmsZoneQueryDTO.getPageSize());
        wmsZoneQueryDTO.setTotalRecord(wmsZoneExtMapper.queryForCount(paramMap));
        resultMap.put("wmsZoneBO", wmsZoneQueryDTO);
        return resultMap;
    }

    @Override
    public boolean addZone(WmsZoneBO wmsZoneBO){
        WmsZone wmsZone = new WmsZone();
        BeanUtils.copyProperties(wmsZoneBO,wmsZone);
        wmsZone.setIsDeleted((byte)1);
        WmsZoneExample wmsZoneExample = new WmsZoneExample();
        wmsZoneExample.createCriteria().andZeZoneCodeEqualTo(wmsZoneBO.getZeZoneCode()).andZeZoneNameEqualTo(wmsZoneBO.getZeZoneName());
        List<WmsZone> wmsZoneList = wmsZoneMapper.selectByExample(wmsZoneExample);
        if(wmsZoneList.size()>0){
            return false;
        }
        wmsZoneMapper.insert(wmsZone);
        return true;
    }

    @Override
    public void updateZone(WmsZoneBO wmsZoneBo) {
        WmsZone wmsZone = new WmsZone();
        BeanUtils.copyProperties(wmsZoneBo,wmsZone);
        wmsZoneMapper.updateByPrimaryKeySelective(wmsZone);
    }

    @Override
    public void deleteZone(List<WmsZoneBO> wmsZoneBOList) {
        wmsZoneBOList.stream().forEach(v -> {
            WmsZoneExample wmsZoneExample = new WmsZoneExample();
            BeanUtils.copyProperties(v, wmsZoneExample);
            wmsZoneMapper.deleteByExample(wmsZoneExample);
        });
    }

    /**
     * 新删除
     *
     * @param zoneIdList
     */
    @Override
    public void deleteZoneNew(List<Long> zoneIdList) {
        WmsZoneExample wmsZoneExample = new WmsZoneExample();
        wmsZoneExample.createCriteria().andZeIdIn(zoneIdList);
        wmsZoneMapper.deleteByExample(wmsZoneExample);
    }

    public WmsZoneBO getZoneById(String zeId) throws NumberFormatException{
        Map<String,Object> paramMap = Maps.newHashMap();
        paramMap.put("zeId",zeId);
        WmsZone wmsZone = wmsZoneMapper.selectByPrimaryKey(Long.valueOf(zeId));
        WmsZoneBO wmsZoneBO = new WmsZoneBO();
        BeanUtils.copyProperties(wmsZone,wmsZoneBO);
        return wmsZoneBO;
    }

}
