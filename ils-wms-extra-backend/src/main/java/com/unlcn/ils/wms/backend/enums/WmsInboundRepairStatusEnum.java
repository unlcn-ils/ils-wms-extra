package com.unlcn.ils.wms.backend.enums;

/**
 * @Auther linbao
 * @Date 2017-11-24
 */
public enum WmsInboundRepairStatusEnum {
    NOT_DEAL("10", "未处理"),
    DEALING("20", "处理中"),
    DEALED("30", "已完成");

    private final String code;

    private final String text;

    WmsInboundRepairStatusEnum(String code, String text) {
        this.code = code;
        this.text = text;
    }

    public String getCode() {
        return code;
    }

    public String getText() {
        return text;
    }
}
