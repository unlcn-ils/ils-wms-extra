package com.unlcn.ils.wms.web.controller.inbound;


import cn.huiyunche.commons.domain.ResultDTO;
import cn.huiyunche.commons.exception.BusinessException;
import com.unlcn.ils.wms.backend.service.baseData.WmsLocationService;
import com.unlcn.ils.wms.backend.service.inbound.WmsStrategyService;
import com.unlcn.ils.wms.base.businessDTO.inbound.AsnOrderDTO;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

/**
 * Created by DELL on 2017/9/27.
 */
@Controller
@RequestMapping("/extraInbound")
@ResponseBody
public class WmsExtraInboundController {
    private Logger LOGGER = LoggerFactory.getLogger(WmsAllocationController.class);
    @Autowired
    WmsStrategyService wmsStrategyService;

    @Autowired
    private WmsLocationService wmsLocationService;

    /**
     * 收货质检推送库位到app--重庆库
     * <p>
     * 接口调整说明:
     * 2018-1-14 service 事务控制,取消之前的手动事务
     * 2018-3-13 使用多线程,极光推送库位信息到手机app
     * 2018-3-13 新开线程并切换TMS的入库确认接口到收货确认进行
     * </p>
     *
     * @param vin 车架号
     * @return 返回值
     * @throws Exception 异常
     */
    @RequestMapping(value = "/barcodePrint/{userId}", method = RequestMethod.POST)
    public ResultDTO<Object> barcodePrint(@RequestBody String vin, @PathVariable String userId) throws Exception {
        LOGGER.info("WmsExtraInboundController.barcodePrint params:{}", vin);
        ResultDTO<Object> resultDTO = new ResultDTO<>(true, null, "收获质检成功");
        try {
            if (StringUtils.isBlank(vin)) {
                throw new BusinessException("传入车架号为空!");
            }
            //回车字符替换
            if (vin.endsWith("\n")) {
                vin = vin.replaceAll("\n", "");
            }
            AsnOrderDTO result = wmsStrategyService.saveAllcationByExtra(vin, userId);
            resultDTO.setData(result);
        } catch (BusinessException e) {
            LOGGER.error("WmsExtraInboundController.barcodePrint businessException:", e);
            resultDTO.setSuccess(false);
            resultDTO.setMessage(e.getMessage());
        } catch (Exception ex) {
            LOGGER.error("WmsExtraInboundController.barcodePrint error: ", ex);
            resultDTO.setSuccess(false);
            resultDTO.setMessage("收货失败!");
        }
        return resultDTO;
    }
}
