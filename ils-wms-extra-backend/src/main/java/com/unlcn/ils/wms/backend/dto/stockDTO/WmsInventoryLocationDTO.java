package com.unlcn.ils.wms.backend.dto.stockDTO;

import java.util.Date;

/**
 * Created by DELL on 2017/8/18.
 */
public class WmsInventoryLocationDTO {

    private Long invlocId;

    private Long invlocInvId;

    private Long invlocWhId;

    private String invlocWhCode;

    private String invlocWhName;

    private String invlocWhType;

    private Long invlocZoneId;

    private String invlocZoneCode;

    private String invlocZoneName;

    private Long invlocLocId;

    private String invlocLocCode;

    private String invlocLocName;

    private String invlocAsnorderId;

    private String invlocStorerId;

    private String invlocStorerName;

    private String invlocBillNo;

    private String invlocLot;

    private String invlocVin;

    private String createPerson;

    private String updatePerson;

    private Date gmtCreate;

    private Date gmtUpdate;

    private Byte isDeleted;

    private Long versions;

    public Long getInvlocId() {
        return invlocId;
    }

    public void setInvlocId(Long invlocId) {
        this.invlocId = invlocId;
    }

    public Long getInvlocInvId() {
        return invlocInvId;
    }

    public void setInvlocInvId(Long invlocInvId) {
        this.invlocInvId = invlocInvId;
    }

    public Long getInvlocWhId() {
        return invlocWhId;
    }

    public void setInvlocWhId(Long invlocWhId) {
        this.invlocWhId = invlocWhId;
    }

    public String getInvlocWhCode() {
        return invlocWhCode;
    }

    public void setInvlocWhCode(String invlocWhCode) {
        this.invlocWhCode = invlocWhCode;
    }

    public String getInvlocWhName() {
        return invlocWhName;
    }

    public void setInvlocWhName(String invlocWhName) {
        this.invlocWhName = invlocWhName;
    }

    public String getInvlocWhType() {
        return invlocWhType;
    }

    public void setInvlocWhType(String invlocWhType) {
        this.invlocWhType = invlocWhType;
    }

    public Long getInvlocZoneId() {
        return invlocZoneId;
    }

    public void setInvlocZoneId(Long invlocZoneId) {
        this.invlocZoneId = invlocZoneId;
    }

    public String getInvlocZoneCode() {
        return invlocZoneCode;
    }

    public void setInvlocZoneCode(String invlocZoneCode) {
        this.invlocZoneCode = invlocZoneCode;
    }

    public String getInvlocZoneName() {
        return invlocZoneName;
    }

    public void setInvlocZoneName(String invlocZoneName) {
        this.invlocZoneName = invlocZoneName;
    }

    public Long getInvlocLocId() {
        return invlocLocId;
    }

    public void setInvlocLocId(Long invlocLocId) {
        this.invlocLocId = invlocLocId;
    }

    public String getInvlocLocCode() {
        return invlocLocCode;
    }

    public void setInvlocLocCode(String invlocLocCode) {
        this.invlocLocCode = invlocLocCode;
    }

    public String getInvlocLocName() {
        return invlocLocName;
    }

    public void setInvlocLocName(String invlocLocName) {
        this.invlocLocName = invlocLocName;
    }

    public String getInvlocAsnorderId() {
        return invlocAsnorderId;
    }

    public void setInvlocAsnorderId(String invlocAsnorderId) {
        this.invlocAsnorderId = invlocAsnorderId;
    }

    public String getInvlocStorerId() {
        return invlocStorerId;
    }

    public void setInvlocStorerId(String invlocStorerId) {
        this.invlocStorerId = invlocStorerId;
    }

    public String getInvlocStorerName() {
        return invlocStorerName;
    }

    public void setInvlocStorerName(String invlocStorerName) {
        this.invlocStorerName = invlocStorerName;
    }

    public String getInvlocBillNo() {
        return invlocBillNo;
    }

    public void setInvlocBillNo(String invlocBillNo) {
        this.invlocBillNo = invlocBillNo;
    }

    public String getInvlocLot() {
        return invlocLot;
    }

    public void setInvlocLot(String invlocLot) {
        this.invlocLot = invlocLot;
    }

    public String getCreatePerson() {
        return createPerson;
    }

    public void setCreatePerson(String createPerson) {
        this.createPerson = createPerson;
    }

    public String getUpdatePerson() {
        return updatePerson;
    }

    public void setUpdatePerson(String updatePerson) {
        this.updatePerson = updatePerson;
    }

    public Date getGmtCreate() {
        return gmtCreate;
    }

    public void setGmtCreate(Date gmtCreate) {
        this.gmtCreate = gmtCreate;
    }

    public Date getGmtUpdate() {
        return gmtUpdate;
    }

    public void setGmtUpdate(Date gmtUpdate) {
        this.gmtUpdate = gmtUpdate;
    }

    public Byte getIsDeleted() {
        return isDeleted;
    }

    public void setIsDeleted(Byte isDeleted) {
        this.isDeleted = isDeleted;
    }

    public Long getVersions() {
        return versions;
    }

    public void setVersions(Long versions) {
        this.versions = versions;
    }

    public String getInvlocVin() {
        return invlocVin;
    }

    public void setInvlocVin(String invlocVin) {
        this.invlocVin = invlocVin;
    }

    @Override
    public String toString() {
        return "WmsInventoryLocationDTO{" +
                "invlocId=" + invlocId +
                ", invlocInvId=" + invlocInvId +
                ", invlocWhId=" + invlocWhId +
                ", invlocWhCode='" + invlocWhCode + '\'' +
                ", invlocWhName='" + invlocWhName + '\'' +
                ", invlocWhType='" + invlocWhType + '\'' +
                ", invlocZoneId=" + invlocZoneId +
                ", invlocZoneCode='" + invlocZoneCode + '\'' +
                ", invlocZoneName='" + invlocZoneName + '\'' +
                ", invlocLocId=" + invlocLocId +
                ", invlocLocCode='" + invlocLocCode + '\'' +
                ", invlocLocName='" + invlocLocName + '\'' +
                ", invlocAsnorderId='" + invlocAsnorderId + '\'' +
                ", invlocStorerId='" + invlocStorerId + '\'' +
                ", invlocStorerName='" + invlocStorerName + '\'' +
                ", invlocBillNo='" + invlocBillNo + '\'' +
                ", invlocLot='" + invlocLot + '\'' +
                ", invlocVin='" + invlocVin + '\'' +
                ", createPerson='" + createPerson + '\'' +
                ", updatePerson='" + updatePerson + '\'' +
                ", gmtCreate=" + gmtCreate +
                ", gmtUpdate=" + gmtUpdate +
                ", isDeleted=" + isDeleted +
                ", versions=" + versions +
                '}';
    }
}
