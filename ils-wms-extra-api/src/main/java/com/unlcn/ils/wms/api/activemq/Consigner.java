package com.unlcn.ils.wms.api.activemq;

import java.io.Serializable;

/**
 * 货主表
 * @author 
 * @generated
 */
public class Consigner implements Serializable{
	
	private static final long serialVersionUID = 1688951382441984L;
	/**
	 *
	 */
	private String id;

	/**
	 *客户ID
	 */
	private CustomerInfo customerId;
	
	/**
	 * 等级
	 */
	private CustomerLevel levelId;


	/**
	 *名称
	 */
	private String name;

	/**
	 *简称
	 */
	private String shortName;


	/**
	 *业务性质,1.OEM主机厂,2.其它三方物流公司，3.个人,4.二手车
	 */
	private String businessType;

	/**
	 *注册地
	 */
	private String regAddress;

	/**
	 *注册资金
	 */
	private Integer capital;

	/**
	 *备注
	 */
	private String desc;

	/**
	 *状态1：正常
	 */
	private String status;

	/**
	 *是否VIP，1：是，0：不是
	 */
	private String isVip;

	/**
	 *添加人
	 */
	private String createUser;

	/**
	 *添加时间
	 */
	private java.sql.Timestamp createTime;

	/**
	 *最后修改人
	 */
	private String updateUser;

	/**
	 *最后修改时间
	 */
	private java.sql.Timestamp updateTime;

	/**
	 *省份
	 */
	private String province;

	/**
	 *城市
	 */
	private String city;

	/**
	 *标签
	 */
	private String tag;
	
	

	/**
	 * 设置
	 */
	public void setId(String id) {
		this.id = id;
	}

	/**
	 * 获取
	 */
	public String getId() {
		return id;
	}

	/**
	 * 设置客户ID
	 */
	public void setCustomerId(CustomerInfo customerId) {
		this.customerId = customerId;
	}

	/**
	 * 获取客户ID
	 */
	public CustomerInfo getCustomerId() {
		return customerId;
	}
	/**
	 * 设置等级
	 */
	public void setLevelId(CustomerLevel levelId) {
		this.levelId = levelId;
	}

	/**
	 * 获取等级
	 */
	public CustomerLevel getLevelId() {
		return levelId;
	}

	/**
	 * 设置名称
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * 获取名称
	 */
	public String getName() {
		return name;
	}

	/**
	 * 设置简称
	 */
	public void setShortName(String shortName) {
		this.shortName = shortName;
	}

	/**
	 * 获取简称
	 */
	public String getShortName() {
		return shortName;
	}


	/**
	 * 设置业务性质,1.OEM主机厂,2.其它三方物流公司，3.个人,4.二手车
	 */
	public void setBusinessType(String businessType) {
		this.businessType = businessType;
	}

	/**
	 * 获取业务性质,1.OEM主机厂,2.其它三方物流公司，3.个人,4.二手车
	 */
	public String getBusinessType() {
		return businessType;
	}

	/**
	 * 设置注册地
	 */
	public void setRegAddress(String regAddress) {
		this.regAddress = regAddress;
	}

	/**
	 * 获取注册地
	 */
	public String getRegAddress() {
		return regAddress;
	}

	/**
	 * 设置注册资金
	 */
	public void setCapital(Integer capital) {
		this.capital = capital;
	}

	/**
	 * 获取注册资金
	 */
	public Integer getCapital() {
		return capital;
	}

	/**
	 * 设置备注
	 */
	public void setDesc(String desc) {
		this.desc = desc;
	}

	/**
	 * 获取备注
	 */
	public String getDesc() {
		return desc;
	}

	/**
	 * 设置状态1：正常
	 */
	public void setStatus(String status) {
		this.status = status;
	}

	/**
	 * 获取状态1：正常
	 */
	public String getStatus() {
		return status;
	}

	/**
	 * 设置是否VIP，1：是，0：不是
	 */
	public void setIsVip(String isVip) {
		this.isVip = isVip;
	}

	/**
	 * 获取是否VIP，1：是，0：不是
	 */
	public String getIsVip() {
		return isVip;
	}

	/**
	 * 设置添加人
	 */
	public void setCreateUser(String createUser) {
		this.createUser = createUser;
	}

	/**
	 * 获取添加人
	 */
	public String getCreateUser() {
		return createUser;
	}

	/**
	 * 设置添加时间
	 */
	public void setCreateTime(java.sql.Timestamp createTime) {
		this.createTime = createTime;
	}

	/**
	 * 获取添加时间
	 */
	public java.sql.Timestamp getCreateTime() {
		return createTime;
	}

	/**
	 * 设置最后修改人
	 */
	public void setUpdateUser(String updateUser) {
		this.updateUser = updateUser;
	}

	/**
	 * 获取最后修改人
	 */
	public String getUpdateUser() {
		return updateUser;
	}

	/**
	 * 设置最后修改时间
	 */
	public void setUpdateTime(java.sql.Timestamp updateTime) {
		this.updateTime = updateTime;
	}

	/**
	 * 获取最后修改时间
	 */
	public java.sql.Timestamp getUpdateTime() {
		return updateTime;
	}

	/**
	 * 设置省份
	 */
	public void setProvince(String province) {
		this.province = province;
	}

	/**
	 * 获取省份
	 */
	public String getProvince() {
		return province;
	}

	/**
	 * 设置城市
	 */
	public void setCity(String city) {
		this.city = city;
	}

	/**
	 * 获取城市
	 */
	public String getCity() {
		return city;
	}

	/**
	 * 设置标签
	 */
	public void setTag(String tag) {
		this.tag = tag;
	}

	/**
	 * 获取标签
	 */
	public String getTag() {
		return tag;
	}

	@Override
	public String toString() {
		return "Consigner{" +
				"id='" + id + '\'' +
				", customerId=" + customerId +
				", levelId=" + levelId +
				", name='" + name + '\'' +
				", shortName='" + shortName + '\'' +
				", businessType='" + businessType + '\'' +
				", regAddress='" + regAddress + '\'' +
				", capital=" + capital +
				", desc='" + desc + '\'' +
				", status='" + status + '\'' +
				", isVip='" + isVip + '\'' +
				", createUser='" + createUser + '\'' +
				", createTime=" + createTime +
				", updateUser='" + updateUser + '\'' +
				", updateTime=" + updateTime +
				", province='" + province + '\'' +
				", city='" + city + '\'' +
				", tag='" + tag + '\'' +
				'}';
	}
}
