package com.unlcn.ils.wms.backend.bo.inboundBO;

import com.unlcn.ils.wms.backend.bo.baseDataBO.WmsCommonBO;

import java.util.Date;

/**
 * 策略条件表
 * Created by DELL on 2017/9/1.
 */
public class WmsInboundPloyConditionBO extends WmsCommonBO {
    /**
     * 策略条件主id
     */
    private Long pycId;

    /**
     * 策略主id
     */
    private String pycPyId;

    /**
     * 策略主code
     */
    private String pycPyCode;

    /**
     * 策略主名称
     */
    private String pycPyName;

    /**
     * 策略条件code
     */
    private String pycCode;

    /**
     * 策略条件名
     */
    private String pycName;

    /**
     * 策略条件类型10：货主；20：车型
     */
    private String pycType;

    /**
     * 策略条件值
     */
    private String pycValue;

    /**
     * 策略运算
     */
    private String pycOperatorCode;

    public Long getPycId() {
        return pycId;
    }

    public void setPycId(Long pycId) {
        this.pycId = pycId;
    }

    public String getPycPyId() {
        return pycPyId;
    }

    public void setPycPyId(String pycPyId) {
        this.pycPyId = pycPyId;
    }

    public String getPycPyCode() {
        return pycPyCode;
    }

    public void setPycPyCode(String pycPyCode) {
        this.pycPyCode = pycPyCode;
    }

    public String getPycPyName() {
        return pycPyName;
    }

    public void setPycPyName(String pycPyName) {
        this.pycPyName = pycPyName;
    }

    public String getPycCode() {
        return pycCode;
    }

    public void setPycCode(String pycCode) {
        this.pycCode = pycCode;
    }

    public String getPycName() {
        return pycName;
    }

    public void setPycName(String pycName) {
        this.pycName = pycName;
    }

    public String getPycType() {
        return pycType;
    }

    public void setPycType(String pycType) {
        this.pycType = pycType;
    }

    public String getPycValue() {
        return pycValue;
    }

    public void setPycValue(String pycValue) {
        this.pycValue = pycValue;
    }

    public String getPycOperatorCode() {
        return pycOperatorCode;
    }

    public void setPycOperatorCode(String pycOperatorCode) {
        this.pycOperatorCode = pycOperatorCode;
    }

    @Override
    public String toString() {
        return "WmsInboundPloyConditionBO{" +
                "pycId=" + pycId +
                ", pycPyId='" + pycPyId + '\'' +
                ", pycPyCode='" + pycPyCode + '\'' +
                ", pycPyName='" + pycPyName + '\'' +
                ", pycCode='" + pycCode + '\'' +
                ", pycName='" + pycName + '\'' +
                ", pycType='" + pycType + '\'' +
                ", pycValue='" + pycValue + '\'' +
                ", pycOperatorCode='" + pycOperatorCode + '\'' +
                '}';
    }
}
