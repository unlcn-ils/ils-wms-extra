package com.unlcn.ils.wms.backend.bo.inboundBO;

import java.util.Date;

/**
 * 订单BO
 * Created by DELL on 2017/8/4.
 */
public class WmsInboundOrderBO {
    private String userId;

    /**
     * 入库订单主ID
     */
    private Long odId;

    /**
     * 订单CODE,WMS专用
     */
    private String odCode;

    /**
     * 货主ID
     */
    private Long odCustomerId;

    /**
     * 货主CODE
     */
    private String odCustomerCode;

    /**
     * 货主名称
     */
    private String odCustomerName;

    /**
     * 调度单号
     */
    private String odDispatchNo;

    /**
     * 运单号
     */
    private String odWaybillNo;

    /**
     * 运单类型
     */
    private String odWaybillType;

    /**
     * 订单状态
     */
    private String odStatus;

    /**
     * 提货地址
     */
    private String odPickUpAddr;

    /**
     * 仓库ID
     */
    private Long odWhId;

    /**
     * 仓库CODE
     */
    private String odWhCode;

    /**
     * 订单商品数量
     */
    private Long odInboundNum;

    /**
     * 交货方
     */
    private String odSupplierParty;

    /**
     * 交货方CODE
     */
    private String odSupplierCode;

    /**
     * 交货方名称
     */
    private String odSupplierName;

    /**
     * 交货方联系人手机号
     */
    private String odSupplierPhone;

    /**
     * 收货人ID
     */
    private String odConsigneeId;

    /**
     * 收货人CODE
     */
    private String odConsigneeCode;

    /**
     * 收货人名称
     */
    private String odConsigneeName;

    /**
     * 收货时间
     */
    private String odConsigneeDate;

    private String whCode;//

    /**
     * 检验结果
     */
    private String odCheckResult;

    /**
     * 检验描述
     */
    private String odCheckDesc;

    /**
     * 起始地code
     */
    private String odOriginCode;

    /**
     * 起始地
     */
    private String odOrigin;

    /**
     * 目的地code
     */
    private String odDestCode;

    /**
     * 目的地
     */
    private String odDest;

    /**
     * 备注
     */
    private String odRemark;

    /**
     * 逻辑删除
     */
    private Byte isDeleted;

    /**
     * 创建开始日期
     */
    private String createStartDate;

    /**
     * 创建结束日期
     */
    private String createEndDate;

    /**
     * 收货开始日期
     */
    private String consigneeStartDate;

    /**
     * 收货结束日期
     */
    private String consigneeEndDate;

    /**
     * 维修方
     */
    private String odRepairParty;

    /**
     * 任务单
     */
    private String odTaskNo;

    /**
     * 创建时间
     */
    private String gmtCreate;

    /**
     * 修改时间
     */
    private String gmtUpdate;

    /**
     * 客户订单号
     */
    private String odCustomerOrderNo;

    /**
     * 是否异常标识 0-否 1-是
     */
    private String odIsExcpSign;

    /**
     * 仓库NAME
     */
    private String odWhName;
    //详情表

    /**
     * 详情主键ID
     */
    private Long oddId;

    /**
     * 关联主表ID
     */
    private Long oddOdId;

    /**
     * 关联主表CODE
     */
    private String oddOdCode;

    /**
     * 车品牌CODE
     */
    private String oddVehicleBrandCode;

    /**
     * 车品牌名称
     */
    private String oddVehicleBrandName;

    /**
     * 车系CODE
     */
    private String oddVehicleSeriesCode;

    /**
     * 车系名称
     */
    private String oddVehicleSeriesName;

    /**
     * 车型CODE
     */
    private String oddVehicleSpecCode;

    /**
     * 车型名称
     */
    private String oddVehicleSpecName;

    /**
     * 车型描述
     */
    private String oddVehicleSpecDesc;

    /**
     * 车牌号
     */
    private String oddVehiclePlate;

    /**
     * 底盘号
     */
    private String oddVin;

    /**
     * 发动机号
     */
    private String oddEngine;

    /**
     * 仓库CODE
     */
    private String oddWhCode;

    /**
     * 仓库名称
     */
    private String oddWhName;

    /**
     * 库区CODE
     */
    private String oddWhZoneCode;

    /**
     * 库区名称
     */
    private String oddWhZoneName;

    /**
     * 库位CODE
     */
    private String oddWhLocCode;

    /**
     * 库位名称
     */
    private String oddWhLocName;

    /**
     * 车长
     */
    private Long oddVehicleLength;

    /**
     * 车宽
     */
    private Long oddVehicleWidth;

    /**
     * 车高
     */
    private Long oddVehicleHigh;

    /**
     * 物料代码
     */
    private String oddMaterialCode;

    /**
     * 物料名称
     */
    private String oddMaterialName;

    /**
     * 颜色代码
     */
    private String oddCarColourCode;

    /**
     * 颜色名称
     */
    private String oddCarColour;

    /**
     * 变速箱号
     */
    private String oddGearboxNumber;

    /**
     * 合格证
     */
    private String oddCertification;

    /**
     * 生产日期
     */
    private Date oddProductionDate;

    /**
     * 下线日期
     */
    private Date oddOfflineDate;

    public String getWhCode() {
        return whCode;
    }

    public void setWhCode(String whCode) {
        this.whCode = whCode;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public Long getOdId() {
        return odId;
    }

    public void setOdId(Long odId) {
        this.odId = odId;
    }

    public String getOdCode() {
        return odCode;
    }

    public void setOdCode(String odCode) {
        this.odCode = odCode;
    }

    public Long getOdCustomerId() {
        return odCustomerId;
    }

    public void setOdCustomerId(Long odCustomerId) {
        this.odCustomerId = odCustomerId;
    }

    public String getOdCustomerCode() {
        return odCustomerCode;
    }

    public void setOdCustomerCode(String odCustomerCode) {
        this.odCustomerCode = odCustomerCode;
    }

    public String getOdCustomerName() {
        return odCustomerName;
    }

    public void setOdCustomerName(String odCustomerName) {
        this.odCustomerName = odCustomerName;
    }

    public String getOdDispatchNo() {
        return odDispatchNo;
    }

    public void setOdDispatchNo(String odDispatchNo) {
        this.odDispatchNo = odDispatchNo;
    }

    public String getOdWaybillNo() {
        return odWaybillNo;
    }

    public void setOdWaybillNo(String odWaybillNo) {
        this.odWaybillNo = odWaybillNo;
    }

    public String getOdWaybillType() {
        return odWaybillType;
    }

    public void setOdWaybillType(String odWaybillType) {
        this.odWaybillType = odWaybillType;
    }

    public String getOdStatus() {
        return odStatus;
    }

    public void setOdStatus(String odStatus) {
        this.odStatus = odStatus;
    }

    public String getOdPickUpAddr() {
        return odPickUpAddr;
    }

    public void setOdPickUpAddr(String odPickUpAddr) {
        this.odPickUpAddr = odPickUpAddr;
    }

    public Long getOdWhId() {
        return odWhId;
    }

    public void setOdWhId(Long odWhId) {
        this.odWhId = odWhId;
    }

    public String getOdWhCode() {
        return odWhCode;
    }

    public void setOdWhCode(String odWhCode) {
        this.odWhCode = odWhCode;
    }

    public Long getOdInboundNum() {
        return odInboundNum;
    }

    public void setOdInboundNum(Long odInboundNum) {
        this.odInboundNum = odInboundNum;
    }

    public String getOdSupplierParty() {
        return odSupplierParty;
    }

    public void setOdSupplierParty(String odSupplierParty) {
        this.odSupplierParty = odSupplierParty;
    }

    public String getOdSupplierCode() {
        return odSupplierCode;
    }

    public void setOdSupplierCode(String odSupplierCode) {
        this.odSupplierCode = odSupplierCode;
    }

    public String getOdSupplierName() {
        return odSupplierName;
    }

    public void setOdSupplierName(String odSupplierName) {
        this.odSupplierName = odSupplierName;
    }

    public String getOdSupplierPhone() {
        return odSupplierPhone;
    }

    public void setOdSupplierPhone(String odSupplierPhone) {
        this.odSupplierPhone = odSupplierPhone;
    }

    public String getOdConsigneeId() {
        return odConsigneeId;
    }

    public void setOdConsigneeId(String odConsigneeId) {
        this.odConsigneeId = odConsigneeId;
    }

    public String getOdConsigneeCode() {
        return odConsigneeCode;
    }

    public void setOdConsigneeCode(String odConsigneeCode) {
        this.odConsigneeCode = odConsigneeCode;
    }

    public String getOdConsigneeName() {
        return odConsigneeName;
    }

    public void setOdConsigneeName(String odConsigneeName) {
        this.odConsigneeName = odConsigneeName;
    }

    public String getOdConsigneeDate() {
        return odConsigneeDate;
    }

    public void setOdConsigneeDate(String odConsigneeDate) {
        this.odConsigneeDate = odConsigneeDate;
    }

    public String getOdCheckResult() {
        return odCheckResult;
    }

    public void setOdCheckResult(String odCheckResult) {
        this.odCheckResult = odCheckResult;
    }

    public String getOdCheckDesc() {
        return odCheckDesc;
    }

    public void setOdCheckDesc(String odCheckDesc) {
        this.odCheckDesc = odCheckDesc;
    }

    public String getOdOriginCode() {
        return odOriginCode;
    }

    public void setOdOriginCode(String odOriginCode) {
        this.odOriginCode = odOriginCode;
    }

    public String getOdOrigin() {
        return odOrigin;
    }

    public void setOdOrigin(String odOrigin) {
        this.odOrigin = odOrigin;
    }

    public String getOdDestCode() {
        return odDestCode;
    }

    public void setOdDestCode(String odDestCode) {
        this.odDestCode = odDestCode;
    }

    public String getOdDest() {
        return odDest;
    }

    public void setOdDest(String odDest) {
        this.odDest = odDest;
    }

    public String getOdRemark() {
        return odRemark;
    }

    public void setOdRemark(String odRemark) {
        this.odRemark = odRemark;
    }

    public Byte getIsDeleted() {
        return isDeleted;
    }

    public void setIsDeleted(Byte isDeleted) {
        this.isDeleted = isDeleted;
    }

    public String getCreateStartDate() {
        return createStartDate;
    }

    public void setCreateStartDate(String createStartDate) {
        this.createStartDate = createStartDate;
    }

    public String getCreateEndDate() {
        return createEndDate;
    }

    public void setCreateEndDate(String createEndDate) {
        this.createEndDate = createEndDate;
    }

    public String getConsigneeStartDate() {
        return consigneeStartDate;
    }

    public void setConsigneeStartDate(String consigneeStartDate) {
        this.consigneeStartDate = consigneeStartDate;
    }

    public String getConsigneeEndDate() {
        return consigneeEndDate;
    }

    public void setConsigneeEndDate(String consigneeEndDate) {
        this.consigneeEndDate = consigneeEndDate;
    }

    public String getOdRepairParty() {
        return odRepairParty;
    }

    public void setOdRepairParty(String odRepairParty) {
        this.odRepairParty = odRepairParty;
    }

    public String getOdTaskNo() {
        return odTaskNo;
    }

    public void setOdTaskNo(String odTaskNo) {
        this.odTaskNo = odTaskNo;
    }

    public String getGmtCreate() {
        return gmtCreate;
    }

    public void setGmtCreate(String gmtCreate) {
        this.gmtCreate = gmtCreate;
    }

    public String getGmtUpdate() {
        return gmtUpdate;
    }

    public void setGmtUpdate(String gmtUpdate) {
        this.gmtUpdate = gmtUpdate;
    }

    public String getOdCustomerOrderNo() {
        return odCustomerOrderNo;
    }

    public void setOdCustomerOrderNo(String odCustomerOrderNo) {
        this.odCustomerOrderNo = odCustomerOrderNo;
    }

    public String getOdIsExcpSign() {
        return odIsExcpSign;
    }

    public void setOdIsExcpSign(String odIsExcpSign) {
        this.odIsExcpSign = odIsExcpSign;
    }

    public Long getOddId() {
        return oddId;
    }

    public void setOddId(Long oddId) {
        this.oddId = oddId;
    }

    public Long getOddOdId() {
        return oddOdId;
    }

    public void setOddOdId(Long oddOdId) {
        this.oddOdId = oddOdId;
    }

    public String getOddOdCode() {
        return oddOdCode;
    }

    public void setOddOdCode(String oddOdCode) {
        this.oddOdCode = oddOdCode;
    }

    public String getOddVehicleBrandCode() {
        return oddVehicleBrandCode;
    }

    public void setOddVehicleBrandCode(String oddVehicleBrandCode) {
        this.oddVehicleBrandCode = oddVehicleBrandCode;
    }

    public String getOddVehicleBrandName() {
        return oddVehicleBrandName;
    }

    public void setOddVehicleBrandName(String oddVehicleBrandName) {
        this.oddVehicleBrandName = oddVehicleBrandName;
    }

    public String getOddVehicleSeriesCode() {
        return oddVehicleSeriesCode;
    }

    public void setOddVehicleSeriesCode(String oddVehicleSeriesCode) {
        this.oddVehicleSeriesCode = oddVehicleSeriesCode;
    }

    public String getOddVehicleSeriesName() {
        return oddVehicleSeriesName;
    }

    public void setOddVehicleSeriesName(String oddVehicleSeriesName) {
        this.oddVehicleSeriesName = oddVehicleSeriesName;
    }

    public String getOddVehicleSpecCode() {
        return oddVehicleSpecCode;
    }

    public void setOddVehicleSpecCode(String oddVehicleSpecCode) {
        this.oddVehicleSpecCode = oddVehicleSpecCode;
    }

    public String getOddVehicleSpecName() {
        return oddVehicleSpecName;
    }

    public void setOddVehicleSpecName(String oddVehicleSpecName) {
        this.oddVehicleSpecName = oddVehicleSpecName;
    }

    public String getOddVehicleSpecDesc() {
        return oddVehicleSpecDesc;
    }

    public void setOddVehicleSpecDesc(String oddVehicleSpecDesc) {
        this.oddVehicleSpecDesc = oddVehicleSpecDesc;
    }

    public String getOddVehiclePlate() {
        return oddVehiclePlate;
    }

    public void setOddVehiclePlate(String oddVehiclePlate) {
        this.oddVehiclePlate = oddVehiclePlate;
    }

    public String getOddVin() {
        return oddVin;
    }

    public void setOddVin(String oddVin) {
        this.oddVin = oddVin;
    }

    public String getOddEngine() {
        return oddEngine;
    }

    public void setOddEngine(String oddEngine) {
        this.oddEngine = oddEngine;
    }

    public String getOddWhCode() {
        return oddWhCode;
    }

    public void setOddWhCode(String oddWhCode) {
        this.oddWhCode = oddWhCode;
    }

    public String getOddWhName() {
        return oddWhName;
    }

    public void setOddWhName(String oddWhName) {
        this.oddWhName = oddWhName;
    }

    public String getOddWhZoneCode() {
        return oddWhZoneCode;
    }

    public void setOddWhZoneCode(String oddWhZoneCode) {
        this.oddWhZoneCode = oddWhZoneCode;
    }

    public String getOddWhZoneName() {
        return oddWhZoneName;
    }

    public void setOddWhZoneName(String oddWhZoneName) {
        this.oddWhZoneName = oddWhZoneName;
    }

    public String getOddWhLocCode() {
        return oddWhLocCode;
    }

    public void setOddWhLocCode(String oddWhLocCode) {
        this.oddWhLocCode = oddWhLocCode;
    }

    public String getOddWhLocName() {
        return oddWhLocName;
    }

    public void setOddWhLocName(String oddWhLocName) {
        this.oddWhLocName = oddWhLocName;
    }

    public Long getOddVehicleLength() {
        return oddVehicleLength;
    }

    public void setOddVehicleLength(Long oddVehicleLength) {
        this.oddVehicleLength = oddVehicleLength;
    }

    public Long getOddVehicleWidth() {
        return oddVehicleWidth;
    }

    public void setOddVehicleWidth(Long oddVehicleWidth) {
        this.oddVehicleWidth = oddVehicleWidth;
    }

    public Long getOddVehicleHigh() {
        return oddVehicleHigh;
    }

    public void setOddVehicleHigh(Long oddVehicleHigh) {
        this.oddVehicleHigh = oddVehicleHigh;
    }

    public String getOddMaterialCode() {
        return oddMaterialCode;
    }

    public void setOddMaterialCode(String oddMaterialCode) {
        this.oddMaterialCode = oddMaterialCode;
    }

    public String getOddMaterialName() {
        return oddMaterialName;
    }

    public void setOddMaterialName(String oddMaterialName) {
        this.oddMaterialName = oddMaterialName;
    }

    public String getOddCarColourCode() {
        return oddCarColourCode;
    }

    public void setOddCarColourCode(String oddCarColourCode) {
        this.oddCarColourCode = oddCarColourCode;
    }

    public String getOddCarColour() {
        return oddCarColour;
    }

    public void setOddCarColour(String oddCarColour) {
        this.oddCarColour = oddCarColour;
    }

    public String getOddGearboxNumber() {
        return oddGearboxNumber;
    }

    public void setOddGearboxNumber(String oddGearboxNumber) {
        this.oddGearboxNumber = oddGearboxNumber;
    }

    public String getOddCertification() {
        return oddCertification;
    }

    public void setOddCertification(String oddCertification) {
        this.oddCertification = oddCertification;
    }

    public Date getOddProductionDate() {
        return oddProductionDate;
    }

    public void setOddProductionDate(Date oddProductionDate) {
        this.oddProductionDate = oddProductionDate;
    }

    public Date getOddOfflineDate() {
        return oddOfflineDate;
    }

    public void setOddOfflineDate(Date oddOfflineDate) {
        this.oddOfflineDate = oddOfflineDate;
    }

    public String getOdWhName() {
        return odWhName;
    }

    public void setOdWhName(String odWhName) {
        this.odWhName = odWhName;
    }

    @Override
    public String toString() {
        return "WmsInboundOrderBO{" +
                "odId=" + odId +
                ", odCode='" + odCode + '\'' +
                ", odCustomerId=" + odCustomerId +
                ", odCustomerCode='" + odCustomerCode + '\'' +
                ", odCustomerName='" + odCustomerName + '\'' +
                ", odDispatchNo='" + odDispatchNo + '\'' +
                ", odWaybillNo='" + odWaybillNo + '\'' +
                ", odWaybillType='" + odWaybillType + '\'' +
                ", odStatus='" + odStatus + '\'' +
                ", odPickUpAddr='" + odPickUpAddr + '\'' +
                ", odWhId=" + odWhId +
                ", odWhCode='" + odWhCode + '\'' +
                ", odInboundNum=" + odInboundNum +
                ", odSupplierParty='" + odSupplierParty + '\'' +
                ", odSupplierCode='" + odSupplierCode + '\'' +
                ", odSupplierName='" + odSupplierName + '\'' +
                ", odSupplierPhone='" + odSupplierPhone + '\'' +
                ", odConsigneeId='" + odConsigneeId + '\'' +
                ", odConsigneeCode='" + odConsigneeCode + '\'' +
                ", odConsigneeName='" + odConsigneeName + '\'' +
                ", odConsigneeDate='" + odConsigneeDate + '\'' +
                ", odCheckResult='" + odCheckResult + '\'' +
                ", odCheckDesc='" + odCheckDesc + '\'' +
                ", odOriginCode='" + odOriginCode + '\'' +
                ", odOrigin='" + odOrigin + '\'' +
                ", odDestCode='" + odDestCode + '\'' +
                ", odDest='" + odDest + '\'' +
                ", odRemark='" + odRemark + '\'' +
                ", isDeleted=" + isDeleted +
                ", createStartDate='" + createStartDate + '\'' +
                ", createEndDate='" + createEndDate + '\'' +
                ", consigneeStartDate='" + consigneeStartDate + '\'' +
                ", consigneeEndDate='" + consigneeEndDate + '\'' +
                ", odRepairParty='" + odRepairParty + '\'' +
                ", odTaskNo='" + odTaskNo + '\'' +
                ", gmtCreate='" + gmtCreate + '\'' +
                ", gmtUpdate='" + gmtUpdate + '\'' +
                ", odCustomerOrderNo='" + odCustomerOrderNo + '\'' +
                ", odIsExcpSign='" + odIsExcpSign + '\'' +
                ", odWhName='" + odWhName + '\'' +
                ", oddId=" + oddId +
                ", oddOdId=" + oddOdId +
                ", oddOdCode='" + oddOdCode + '\'' +
                ", oddVehicleBrandCode='" + oddVehicleBrandCode + '\'' +
                ", oddVehicleBrandName='" + oddVehicleBrandName + '\'' +
                ", oddVehicleSeriesCode='" + oddVehicleSeriesCode + '\'' +
                ", oddVehicleSeriesName='" + oddVehicleSeriesName + '\'' +
                ", oddVehicleSpecCode='" + oddVehicleSpecCode + '\'' +
                ", oddVehicleSpecName='" + oddVehicleSpecName + '\'' +
                ", oddVehicleSpecDesc='" + oddVehicleSpecDesc + '\'' +
                ", oddVehiclePlate='" + oddVehiclePlate + '\'' +
                ", oddVin='" + oddVin + '\'' +
                ", oddEngine='" + oddEngine + '\'' +
                ", oddWhCode='" + oddWhCode + '\'' +
                ", oddWhName='" + oddWhName + '\'' +
                ", oddWhZoneCode='" + oddWhZoneCode + '\'' +
                ", oddWhZoneName='" + oddWhZoneName + '\'' +
                ", oddWhLocCode='" + oddWhLocCode + '\'' +
                ", oddWhLocName='" + oddWhLocName + '\'' +
                ", oddVehicleLength=" + oddVehicleLength +
                ", oddVehicleWidth=" + oddVehicleWidth +
                ", oddVehicleHigh=" + oddVehicleHigh +
                ", oddMaterialCode='" + oddMaterialCode + '\'' +
                ", oddMaterialName='" + oddMaterialName + '\'' +
                ", oddCarColourCode='" + oddCarColourCode + '\'' +
                ", oddCarColour='" + oddCarColour + '\'' +
                ", oddGearboxNumber='" + oddGearboxNumber + '\'' +
                ", oddCertification='" + oddCertification + '\'' +
                ", oddProductionDate=" + oddProductionDate +
                ", oddOfflineDate=" + oddOfflineDate +
                '}';
    }
}
