package com.unlcn.ils.wms.web.vo.stock;

import cn.huiyunche.commons.domain.PageVo;

import java.util.Date;

/**
 * 库存VO
 * Created by DELL on 2017/8/8.
 */
public class WmsInventoryVO extends PageVo {

    /**
     * 君马-条件“物料名称”、“颜色”、“库存状态”
     */
    private String materialName;
    private String materialCode;
    private String colourCode;
    private String colourName;
    private String invStatus;

    /**
     * 库存ID
     */
    private Long invId;

    /**
     * 仓库CODE
     */
    private String invWhCode;

    /**
     * 仓库名称
     */
    private String invWhName;

    /**
     * 货主ID
     */
    private String invCustomerCode;

    /**
     * 货主名称
     */
    private String invCustomerName;

    /**
     * 品牌CODE
     */
    private String invVehicleBrandCode;

    /**
     * 品牌名称
     */
    private String invVehicleBrandName;

    /**
     * 车系CODE
     */
    private String invVehicleSeriesCode;

    /**
     * 车系名称
     */
    private String invVehicleSeriesName;

    /**
     * 车型CODE
     */
    private String invVehicleSpecCode;

    /**
     * 车型名称
     */
    private String invVehicleSpecName;

    /**
     * 车型描述
     */
    private String invVehicleSpecDesc;

    /**
     * 库存数量
     */
    private Long invNum;

    /**
     * 底盘号
     */
    private String invVin;

    /********************详情****************************/
    private Long invlocId;

    private Long invlocInvId;

    private Long invlocWhId;

    private String invlocWhCode;

    private String invlocWhName;

    private String invlocWhType;

    private Long invlocZoneId;

    private String invlocZoneCode;

    private String invlocZoneName;

    private Long invlocLocId;

    private String invlocLocCode;

    private String invlocLocName;

    private String invlocOdId;

    private String invlocCustomerCode;

    private String invlocCustomerName;

    private String invlocWaybillNo;

    private String invlocLot;

    private String invlocVin;

    private String createUserId;

    private String createUserName;

    private String modifyUserId;

    private String modifyUserName;

    private Date gmtCreate;

    private Date gmtUpdate;

    private Byte isDeleted;

    private Long versions;

    public String getInvStatus() {
        return invStatus;
    }

    public void setInvStatus(String invStatus) {
        this.invStatus = invStatus;
    }

    public String getMaterialName() {
        return materialName;
    }

    public void setMaterialName(String materialName) {
        this.materialName = materialName;
    }

    public String getMaterialCode() {
        return materialCode;
    }

    public void setMaterialCode(String materialCode) {
        this.materialCode = materialCode;
    }

    public String getColourCode() {
        return colourCode;
    }

    public void setColourCode(String colourCode) {
        this.colourCode = colourCode;
    }

    public String getColourName() {
        return colourName;
    }

    public void setColourName(String colourName) {
        this.colourName = colourName;
    }

    public Long getInvId() {
        return invId;
    }

    public void setInvId(Long invId) {
        this.invId = invId;
    }

    public String getInvWhCode() {
        return invWhCode;
    }

    public void setInvWhCode(String invWhCode) {
        this.invWhCode = invWhCode;
    }

    public String getInvWhName() {
        return invWhName;
    }

    public void setInvWhName(String invWhName) {
        this.invWhName = invWhName;
    }

    public String getInvCustomerCode() {
        return invCustomerCode;
    }

    public void setInvCustomerCode(String invCustomerCode) {
        this.invCustomerCode = invCustomerCode;
    }

    public String getInvCustomerName() {
        return invCustomerName;
    }

    public void setInvCustomerName(String invCustomerName) {
        this.invCustomerName = invCustomerName;
    }

    public String getInvVehicleBrandCode() {
        return invVehicleBrandCode;
    }

    public void setInvVehicleBrandCode(String invVehicleBrandCode) {
        this.invVehicleBrandCode = invVehicleBrandCode;
    }

    public String getInvVehicleBrandName() {
        return invVehicleBrandName;
    }

    public void setInvVehicleBrandName(String invVehicleBrandName) {
        this.invVehicleBrandName = invVehicleBrandName;
    }

    public String getInvVehicleSeriesCode() {
        return invVehicleSeriesCode;
    }

    public void setInvVehicleSeriesCode(String invVehicleSeriesCode) {
        this.invVehicleSeriesCode = invVehicleSeriesCode;
    }

    public String getInvVehicleSeriesName() {
        return invVehicleSeriesName;
    }

    public void setInvVehicleSeriesName(String invVehicleSeriesName) {
        this.invVehicleSeriesName = invVehicleSeriesName;
    }

    public String getInvVehicleSpecCode() {
        return invVehicleSpecCode;
    }

    public void setInvVehicleSpecCode(String invVehicleSpecCode) {
        this.invVehicleSpecCode = invVehicleSpecCode;
    }

    public String getInvVehicleSpecName() {
        return invVehicleSpecName;
    }

    public void setInvVehicleSpecName(String invVehicleSpecName) {
        this.invVehicleSpecName = invVehicleSpecName;
    }

    public String getInvVehicleSpecDesc() {
        return invVehicleSpecDesc;
    }

    public void setInvVehicleSpecDesc(String invVehicleSpecDesc) {
        this.invVehicleSpecDesc = invVehicleSpecDesc;
    }

    public Long getInvNum() {
        return invNum;
    }

    public void setInvNum(Long invNum) {
        this.invNum = invNum;
    }

    public Long getInvlocId() {
        return invlocId;
    }

    public void setInvlocId(Long invlocId) {
        this.invlocId = invlocId;
    }

    public Long getInvlocInvId() {
        return invlocInvId;
    }

    public void setInvlocInvId(Long invlocInvId) {
        this.invlocInvId = invlocInvId;
    }

    public Long getInvlocWhId() {
        return invlocWhId;
    }

    public void setInvlocWhId(Long invlocWhId) {
        this.invlocWhId = invlocWhId;
    }

    public String getInvlocWhCode() {
        return invlocWhCode;
    }

    public void setInvlocWhCode(String invlocWhCode) {
        this.invlocWhCode = invlocWhCode;
    }

    public String getInvlocWhName() {
        return invlocWhName;
    }

    public void setInvlocWhName(String invlocWhName) {
        this.invlocWhName = invlocWhName;
    }

    public String getInvlocWhType() {
        return invlocWhType;
    }

    public void setInvlocWhType(String invlocWhType) {
        this.invlocWhType = invlocWhType;
    }

    public Long getInvlocZoneId() {
        return invlocZoneId;
    }

    public void setInvlocZoneId(Long invlocZoneId) {
        this.invlocZoneId = invlocZoneId;
    }

    public String getInvlocZoneCode() {
        return invlocZoneCode;
    }

    public void setInvlocZoneCode(String invlocZoneCode) {
        this.invlocZoneCode = invlocZoneCode;
    }

    public String getInvlocZoneName() {
        return invlocZoneName;
    }

    public void setInvlocZoneName(String invlocZoneName) {
        this.invlocZoneName = invlocZoneName;
    }

    public Long getInvlocLocId() {
        return invlocLocId;
    }

    public void setInvlocLocId(Long invlocLocId) {
        this.invlocLocId = invlocLocId;
    }

    public String getInvlocLocCode() {
        return invlocLocCode;
    }

    public void setInvlocLocCode(String invlocLocCode) {
        this.invlocLocCode = invlocLocCode;
    }

    public String getInvlocLocName() {
        return invlocLocName;
    }

    public void setInvlocLocName(String invlocLocName) {
        this.invlocLocName = invlocLocName;
    }

    public String getInvlocOdId() {
        return invlocOdId;
    }

    public void setInvlocOdId(String invlocOdId) {
        this.invlocOdId = invlocOdId;
    }

    public String getInvlocCustomerCode() {
        return invlocCustomerCode;
    }

    public void setInvlocCustomerCode(String invlocCustomerCode) {
        this.invlocCustomerCode = invlocCustomerCode;
    }

    public String getInvlocCustomerName() {
        return invlocCustomerName;
    }

    public void setInvlocCustomerName(String invlocCustomerName) {
        this.invlocCustomerName = invlocCustomerName;
    }

    public String getInvlocWaybillNo() {
        return invlocWaybillNo;
    }

    public void setInvlocWaybillNo(String invlocWaybillNo) {
        this.invlocWaybillNo = invlocWaybillNo;
    }

    public String getInvlocLot() {
        return invlocLot;
    }

    public void setInvlocLot(String invlocLot) {
        this.invlocLot = invlocLot;
    }

    public String getInvlocVin() {
        return invlocVin;
    }

    public void setInvlocVin(String invlocVin) {
        this.invlocVin = invlocVin;
    }

    public String getCreateUserId() {
        return createUserId;
    }

    public void setCreateUserId(String createUserId) {
        this.createUserId = createUserId;
    }

    public String getCreateUserName() {
        return createUserName;
    }

    public void setCreateUserName(String createUserName) {
        this.createUserName = createUserName;
    }

    public String getModifyUserId() {
        return modifyUserId;
    }

    public void setModifyUserId(String modifyUserId) {
        this.modifyUserId = modifyUserId;
    }

    public String getModifyUserName() {
        return modifyUserName;
    }

    public void setModifyUserName(String modifyUserName) {
        this.modifyUserName = modifyUserName;
    }

    public Date getGmtCreate() {
        return gmtCreate;
    }

    public void setGmtCreate(Date gmtCreate) {
        this.gmtCreate = gmtCreate;
    }

    public Date getGmtUpdate() {
        return gmtUpdate;
    }

    public void setGmtUpdate(Date gmtUpdate) {
        this.gmtUpdate = gmtUpdate;
    }

    public Byte getIsDeleted() {
        return isDeleted;
    }

    public void setIsDeleted(Byte isDeleted) {
        this.isDeleted = isDeleted;
    }

    public Long getVersions() {
        return versions;
    }

    public void setVersions(Long versions) {
        this.versions = versions;
    }

    public String getInvVin() {
        return invVin;
    }

    public void setInvVin(String invVin) {
        this.invVin = invVin;
    }

    @Override
    public String toString() {
        return "WmsInventoryVO{" +
                "invId=" + invId +
                ", invWhCode='" + invWhCode + '\'' +
                ", invWhName='" + invWhName + '\'' +
                ", invCustomerCode='" + invCustomerCode + '\'' +
                ", invCustomerName='" + invCustomerName + '\'' +
                ", invVehicleBrandCode='" + invVehicleBrandCode + '\'' +
                ", invVehicleBrandName='" + invVehicleBrandName + '\'' +
                ", invVehicleSeriesCode='" + invVehicleSeriesCode + '\'' +
                ", invVehicleSeriesName='" + invVehicleSeriesName + '\'' +
                ", invVehicleSpecCode='" + invVehicleSpecCode + '\'' +
                ", invVehicleSpecName='" + invVehicleSpecName + '\'' +
                ", invVehicleSpecDesc='" + invVehicleSpecDesc + '\'' +
                ", invNum=" + invNum +
                ", invVin='" + invVin + '\'' +
                ", invlocId=" + invlocId +
                ", invlocInvId=" + invlocInvId +
                ", invlocWhId=" + invlocWhId +
                ", invlocWhCode='" + invlocWhCode + '\'' +
                ", invlocWhName='" + invlocWhName + '\'' +
                ", invlocWhType='" + invlocWhType + '\'' +
                ", invlocZoneId=" + invlocZoneId +
                ", invlocZoneCode='" + invlocZoneCode + '\'' +
                ", invlocZoneName='" + invlocZoneName + '\'' +
                ", invlocLocId=" + invlocLocId +
                ", invlocLocCode='" + invlocLocCode + '\'' +
                ", invlocLocName='" + invlocLocName + '\'' +
                ", invlocOdId='" + invlocOdId + '\'' +
                ", invlocCustomerCode='" + invlocCustomerCode + '\'' +
                ", invlocCustomerName='" + invlocCustomerName + '\'' +
                ", invlocWaybillNo='" + invlocWaybillNo + '\'' +
                ", invlocLot='" + invlocLot + '\'' +
                ", invlocVin='" + invlocVin + '\'' +
                ", createUserId='" + createUserId + '\'' +
                ", createUserName='" + createUserName + '\'' +
                ", modifyUserId='" + modifyUserId + '\'' +
                ", modifyUserName='" + modifyUserName + '\'' +
                ", gmtCreate=" + gmtCreate +
                ", gmtUpdate=" + gmtUpdate +
                ", isDeleted=" + isDeleted +
                ", versions=" + versions +
                '}';
    }
}
