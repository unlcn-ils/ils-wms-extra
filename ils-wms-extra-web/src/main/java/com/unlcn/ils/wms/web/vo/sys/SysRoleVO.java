package com.unlcn.ils.wms.web.vo.sys;

import cn.huiyunche.commons.domain.PageVo;

import java.util.List;

/**
 * @Auther linbao
 * @Date 2017-10-12
 */
public class SysRoleVO extends PageVo {

    //id
    private Integer id;

    //编码
    private String code;

    //角色名
    private String name;

    //是否可以用状态
    private String enable;

    //创建人
    private String currentUserName;

    private Integer userId;

    /**
     * 权限
     */
    private List<Integer> permissionsIdList;

    public List<Integer> getPermissionsIdList() {
        return permissionsIdList;
    }

    public void setPermissionsIdList(List<Integer> permissionsIdList) {
        this.permissionsIdList = permissionsIdList;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    private List<Integer> roleIdList;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEnable() {
        return enable;
    }

    public void setEnable(String enable) {
        this.enable = enable;
    }

    public String getCurrentUserName() {
        return currentUserName;
    }

    public void setCurrentUserName(String currentUserName) {
        this.currentUserName = currentUserName;
    }

    public List<Integer> getRoleIdList() {
        return roleIdList;
    }

    public void setRoleIdList(List<Integer> roleIdList) {
        this.roleIdList = roleIdList;
    }

    @Override
    public String toString() {
        return "SysRoleVO{" +
                "id=" + id +
                ", code='" + code + '\'' +
                ", name='" + name + '\'' +
                ", enable='" + enable + '\'' +
                ", currentUserName='" + currentUserName + '\'' +
                ", roleIdList=" + roleIdList +
                '}';
    }
}
