package com.unlcn.ils.wms.backend.dto.outboundDTO;

import com.unlcn.ils.wms.base.model.stock.WmsInventoryLocation;
import com.unlcn.ils.wms.base.model.stock.WmsShipmentPlan;

import java.util.List;

/**
 * @Auther linbao
 * @Date 2017-11-07
 */
public class ShipmentPlanEmptyDTO {

    private WmsShipmentPlan shipmentPlan;

    private List<WmsInventoryLocation> locationList;

    public WmsShipmentPlan getShipmentPlan() {
        return shipmentPlan;
    }

    public void setShipmentPlan(WmsShipmentPlan shipmentPlan) {
        this.shipmentPlan = shipmentPlan;
    }

    public List<WmsInventoryLocation> getLocationList() {
        return locationList;
    }

    public void setLocationList(List<WmsInventoryLocation> locationList) {
        this.locationList = locationList;
    }
}


