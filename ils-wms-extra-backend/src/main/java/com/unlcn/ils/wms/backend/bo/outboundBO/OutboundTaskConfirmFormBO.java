package com.unlcn.ils.wms.backend.bo.outboundBO;

import java.util.List;

/**
 * @Auther linbao
 * @Date 2017-10-25
 */
public class OutboundTaskConfirmFormBO extends BarCodeBO {

    /**
     * 已有的车架号集合
     */
    private List<String> vinList;

    /**
     * 需要出库的车架号
     */
    private String vin;

    public List<String> getVinList() {
        return vinList;
    }

    public void setVinList(List<String> vinList) {
        this.vinList = vinList;
    }

    public String getVin() {
        return vin;
    }

    public void setVin(String vin) {
        this.vin = vin;
    }

    @Override
    public String toString() {
        super.toString();
        return "OutboundTaskConfirmFormBO{" +
                "vinList=" + vinList +
                ", vin='" + vin + '\'' +
                '}';
    }
}
