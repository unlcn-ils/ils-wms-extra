package com.unlcn.ils.wms.web.vo.borrow;

import cn.huiyunche.commons.domain.PageVo;

import java.util.Date;

/**
 * Created by lenovo on 2017/10/20.
 */
public class BorrowHistoryVo extends PageVo {

    // 车架号
    private String vin;

    // 是否还车
    private Integer isReturn;

    // 借用人
    private String borrowName;

    // 还车人
    private String returnName;

    // 还车起始时间
    private String returnStartTime;

    // 还车截止时间
    private String returnEndTime;

    // 所属部门
    private String bcWhcode;

    public String getVin() {
        return vin;
    }

    public void setVin(String vin) {
        this.vin = vin;
    }

    public Integer getIsReturn() {
        return isReturn;
    }

    public void setIsReturn(Integer isReturn) {
        this.isReturn = isReturn;
    }

    public String getBorrowName() {
        return borrowName;
    }

    public void setBorrowName(String borrowName) {
        this.borrowName = borrowName;
    }

    public String getReturnName() {
        return returnName;
    }

    public void setReturnName(String returnName) {
        this.returnName = returnName;
    }

    public String getReturnStartTime() {
        return returnStartTime;
    }

    public void setReturnStartTime(String returnStartTime) {
        this.returnStartTime = returnStartTime;
    }

    public String getReturnEndTime() {
        return returnEndTime;
    }

    public void setReturnEndTime(String returnEndTime) {
        this.returnEndTime = returnEndTime;
    }

    public String getBcWhcode() {
        return bcWhcode;
    }

    public void setBcWhcode(String bcWhcode) {
        this.bcWhcode = bcWhcode;
    }

    @Override
    public String toString() {
        return "BorrowHistoryVo{" +
                "vin='" + vin + '\'' +
                ", isReturn=" + isReturn +
                ", borrowName='" + borrowName + '\'' +
                ", returnName='" + returnName + '\'' +
                ", returnStartTime='" + returnStartTime + '\'' +
                ", returnEndTime='" + returnEndTime + '\'' +
                ", bcWhcode='" + bcWhcode + '\'' +
                '}';
    }
}
