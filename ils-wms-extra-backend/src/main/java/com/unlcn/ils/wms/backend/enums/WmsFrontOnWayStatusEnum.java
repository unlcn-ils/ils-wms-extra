package com.unlcn.ils.wms.backend.enums;

public enum WmsFrontOnWayStatusEnum {

    NOT_FINISHED("10", "未完成"),
    FINISHED("20", "已完成"),
    DELAYED("30", "超期");

    private final String code;
    private final String text;

    WmsFrontOnWayStatusEnum(String value, String text) {
        this.code = value;
        this.text = text;
    }

    /**
     * @return the value
     */
    public String getCode() {
        return code;
    }

    /**
     * @return the text
     */
    public String getText() {
        return text;
    }

    public static WmsFrontOnWayStatusEnum getByValue(String value) {
        for (WmsFrontOnWayStatusEnum temp : WmsFrontOnWayStatusEnum.values()) {
            if (temp.getCode().equals(value)) {
                return temp;
            }
        }
        return null;
    }
}
