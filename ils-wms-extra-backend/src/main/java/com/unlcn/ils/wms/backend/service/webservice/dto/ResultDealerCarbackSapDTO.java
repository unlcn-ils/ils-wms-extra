package com.unlcn.ils.wms.backend.service.webservice.dto;

import java.io.Serializable;

public class ResultDealerCarbackSapDTO implements Serializable {
	// pi参数:
	private String dataId;
	private String vbeln;
	private String posnr;
	private String zvbeln;
	private String zposnr;
	private String zstatus;
	private String zmsg;
	private String sernr;//多余参数(本来是vin)

	public String getSernr() {
		return sernr;
	}

	public void setSernr(String sernr) {
		this.sernr = sernr;
	}

	public String getDataId() {
		return dataId;
	}

	public void setDataId(String dataId) {
		this.dataId = dataId;
	}

	public String getVbeln() {
		return vbeln;
	}

	public void setVbeln(String vbeln) {
		this.vbeln = vbeln;
	}

	public String getPosnr() {
		return posnr;
	}

	public void setPosnr(String posnr) {
		this.posnr = posnr;
	}

	public String getZvbeln() {
		return zvbeln;
	}

	public void setZvbeln(String zvbeln) {
		this.zvbeln = zvbeln;
	}

	public String getZposnr() {
		return zposnr;
	}

	public void setZposnr(String zposnr) {
		this.zposnr = zposnr;
	}

	public String getZstatus() {
		return zstatus;
	}

	public void setZstatus(String zstatus) {
		this.zstatus = zstatus;
	}

	public String getZmsg() {
		return zmsg;
	}

	public void setZmsg(String zmsg) {
		this.zmsg = zmsg;
	}
}
