package com.unlcn.ils.wms.base.mapper.extmapper;

import com.unlcn.ils.wms.base.model.bigscreen.BiInOutDray;
import com.unlcn.ils.wms.base.model.bigscreen.BiInOutDrayHead;

import java.util.HashMap;
import java.util.List;

public interface BiInOutDrayExtMapper {
    /**
     * 板车出入计划列表统计
     * @return
     */
    List<BiInOutDray> inOutDrayLineCount();

    List<BiInOutDray> selectInOutDrayLineByParam(HashMap<String, Object> paramMap);
}