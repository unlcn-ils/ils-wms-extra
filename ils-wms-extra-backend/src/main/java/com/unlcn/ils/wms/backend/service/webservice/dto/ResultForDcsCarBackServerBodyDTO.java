package com.unlcn.ils.wms.backend.service.webservice.dto;

import java.io.Serializable;

public class ResultForDcsCarBackServerBodyDTO implements Serializable {
    private String returnCode;
    private String returnMsg;

    private ResultForDcsDealerCarbackDetailsDTO[] msgDetails;

    public String getReturnCode() {
        return returnCode;
    }

    public void setReturnCode(String returnCode) {
        this.returnCode = returnCode;
    }

    public String getReturnMsg() {
        return returnMsg;
    }

    public void setReturnMsg(String returnMsg) {
        this.returnMsg = returnMsg;
    }

    public ResultForDcsDealerCarbackDetailsDTO[] getMsgDetails() {
        return msgDetails;
    }

    public void setMsgDetails(ResultForDcsDealerCarbackDetailsDTO[] msgDetails) {
        this.msgDetails = msgDetails;
    }
}
