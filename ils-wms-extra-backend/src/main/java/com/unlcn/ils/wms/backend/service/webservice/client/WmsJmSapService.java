package com.unlcn.ils.wms.backend.service.webservice.client;


import com.unlcn.ils.wms.backend.service.webservice.bo.WmsResultDealerCarbackSapBO;
import com.unlcn.ils.wms.backend.service.webservice.bo.WmsResultInorOutFromSapBO;
import com.unlcn.ils.wms.base.model.junmadcs.WmsHandoverOrder;
import com.unlcn.ils.wms.base.model.junmadcs.WmsOutOfStorage;
import com.unlcn.ils.wms.base.model.outbound.WmsDealerCarBack;

import java.util.List;
import java.util.Map;

public interface WmsJmSapService {

    void updateSendInOrOutbounRequest(List<WmsOutOfStorage> wmsOutOfStorageList) throws Exception;

    void saveHandoverOrderToSap(List<WmsHandoverOrder> handoverOrderList) throws Exception;

    Map<String, Object> updateOutofStorageResult(WmsResultInorOutFromSapBO wmsResultInorOutFromSapBO, String s_body, String s_head) throws Exception;

    void updateWmsDealerCarBack(List<WmsDealerCarBack> wmsDealerCarBackList) throws Exception;

    Map<String, Object> updateDealercarbackResult(WmsResultDealerCarbackSapBO wmsResultDealerCarbackSapBO, String s_body, String s_head) throws Exception;

    void saveResultLog(String s_body, String s_head) throws Exception;
}
