package com.unlcn.ils.wms.web.controller.sys;

import cn.huiyunche.commons.domain.ResultDTO;
import cn.huiyunche.commons.exception.BusinessException;
import com.unlcn.ils.wms.backend.bo.sys.GateConfigBO;
import com.unlcn.ils.wms.backend.service.sys.GateConfigService;
import com.unlcn.ils.wms.base.dto.SysGateConfigDTO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;

/**
 * @author laishijian
 * @ClassName: SysGateConfigController
 * @Description: 闸门配置 controller
 * @date 2017年11月15日 下午3:26:36
 */

@Controller
@RequestMapping("/gateConfig")
@ResponseBody
public class SysGateConfigController {

    private final static Logger LOGGER = LoggerFactory.getLogger(SysGateConfigController.class);

    @Autowired
    private GateConfigService gateConfigService;

    /**
     * 根据类型获取闸门配置信息
     *
     * @param dto     参数封装
     * @param request 请求
     * @return 返回值
     */
    @RequestMapping(value = "/getByType", method = RequestMethod.POST)
    public ResultDTO<GateConfigBO> getByType(@RequestBody SysGateConfigDTO dto, HttpServletRequest request) {

        LOGGER.info("OutboundTaskController.getOutboundTaskList param: {},{}", dto);
        ResultDTO<GateConfigBO> result = new ResultDTO<>(true, null, "查询闸门数据成功");
        try {
            String whCode = request.getHeader("whCode");
            GateConfigBO gateCofigBO = gateConfigService.getByType(whCode, dto);
            result.setData(gateCofigBO);
        } catch (BusinessException be) {
            LOGGER.error("OutboundTaskController.getOutboundTaskList error: {}", be);
            result.setSuccess(false);
            result.setMessage(be.getMessage());
        } catch (Exception e) {
            LOGGER.error("OutboundTaskController.getOutboundTaskList error: {}", e);
            result.setSuccess(false);
            result.setMessage(e.getMessage());
        }
        return result;
    }
}
