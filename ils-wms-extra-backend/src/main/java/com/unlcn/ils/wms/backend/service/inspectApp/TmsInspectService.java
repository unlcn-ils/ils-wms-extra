package com.unlcn.ils.wms.backend.service.inspectApp;


import com.unlcn.ils.wms.backend.bo.inspectAppBO.TmsInboundInspectBO;
import com.unlcn.ils.wms.base.dto.WmsInspectForAppDTO;

import java.util.Map;

public interface TmsInspectService {

    Map<String, Object> updateFetchInspectBillOld(String vin, String whCode, String whName) throws Exception;

    TmsInboundInspectBO getBoFromRepair(String vin, String whCode) throws Exception;

    void updateInspectStatusById(Long id, String userId);

    void updateToInspectCancel(Long inspectId, String userId) throws Exception;

    TmsInboundInspectBO getInspectBoForJM(String vin, String whCode, String whName);

    TmsInboundInspectBO getInspectBoForJMOld(String vin, String whCode, String whName) throws Exception;

    void saveTmsLog(String vin, String msg) throws Exception;

    Map<String, Object> updateFetchInspectBill(WmsInspectForAppDTO appDTO) throws Exception;


}
