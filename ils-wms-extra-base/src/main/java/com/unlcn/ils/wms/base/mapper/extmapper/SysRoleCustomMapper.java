package com.unlcn.ils.wms.base.mapper.extmapper;

import java.util.Date;
import java.util.List;

import com.unlcn.ils.wms.base.model.sys.SysRole;
import org.apache.ibatis.annotations.Param;

import cn.huiyunche.commons.domain.PageVo;

public interface SysRoleCustomMapper {
	
	/**
	 * 
	 * @Title: deleteByIds 
	 * @Description: 根据ids删除角色
	 * @param @param ids
	 * @param @param status 状态
	 * @param @param modifyDate 修改时间
	 * @param @return     
	 * @return Integer    返回类型 
	 * @throws 
	 *
	 */
	Integer deleteByIds(@Param("ids") List<String> ids,@Param("status") String status,@Param("modifyDate") Date modifyDate);
	
	/**
	 * 
	 * @Title: list 
	 * @Description: 角色列表
	 * @param @param role
	 * @param @param pageVO
	 * @param @return     
	 * @return List<SysRole>    返回类型 
	 * @throws 
	 *
	 */
	List<SysRole> list(@Param("role") SysRole role, @Param("page") PageVo pageVO);
	
	/**
	 * 
	 * @Title: selectByUserId 
	 * @Description: 根据userId查询
	 * @param @param userId
	 * @param @param status
	 * @param @return     
	 * @return List<SysRole>    返回类型 
	 * @throws 
	 *
	 */
	List<SysRole> selectByUserId(@Param("userId") Integer userId,@Param("status") String status);

	/**
	 * 根据用户id和仓库id获取角色
	 * @param userId
	 * @param warehouseId
	 * @param status
	 * @return
	 */
	List<SysRole> selectByUseIdAnsWarehouseId(@Param("userId") Integer userId, @Param("warehouseId") Long warehouseId, @Param("status") String status);
	
	/**
	 * 
	 * @Title: selectByParentId 
	 * @Description: 根据父id获取角色 
	 * @param @param parentId
	 * @param @return     
	 * @return List<SysRole>    返回类型 
	 * @throws 
	 *
	 */
	List<SysRole> selectByParentId(@Param("parentId") Integer parentId);
}