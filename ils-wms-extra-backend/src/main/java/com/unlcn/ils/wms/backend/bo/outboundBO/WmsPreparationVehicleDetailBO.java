package com.unlcn.ils.wms.backend.bo.outboundBO;

import java.io.Serializable;
import java.util.Date;

/**
 * Created by DELL on 2017/9/26.
 */
public class WmsPreparationVehicleDetailBO implements Serializable {

    private String createUserName;
    private Date gmtCreate;
    private String modifyUserName;
    private Date gmtUpdate;
    private Byte isDeleted;
    private String versions;
    private String driverId;
    private String driverName;
    /**
     * 主键
     */
    private Long vdId;

    /**
     * 备料计划表ID
     */
    private Long vdPpId;

    /**
     * 车架号(VIN码)
     */
    private String vdVin;

    /**
     * 车型
     */
    private String vdVehicleName;

    /**
     * 车型描述
     */
    private String vdVehicleDesc;

    /**
     * 客户运单号
     */
    private String vdWaybillNo;

    /**
     * 出库状态
     */
    private String vdOutstockStatus;

    /**
     * 交接单号
     */
    private String hoHandoverNumber;

    /**
     * 是否需要剔除
     */
    private Integer deleteFlag;

    private String wndMaterialCode;

    private String alSourceLocCode;

    private String alSourceZoneCode;

    public String getAlSourceZoneCode() {
        return alSourceZoneCode;
    }

    public void setAlSourceZoneCode(String alSourceZoneCode) {
        this.alSourceZoneCode = alSourceZoneCode;
    }

    public String getWndMaterialCode() {
        return wndMaterialCode;
    }

    public void setWndMaterialCode(String wndMaterialCode) {
        this.wndMaterialCode = wndMaterialCode;
    }

    public String getAlSourceLocCode() {
        return alSourceLocCode;
    }

    public void setAlSourceLocCode(String alSourceLocCode) {
        this.alSourceLocCode = alSourceLocCode;
    }

    public Integer getDeleteFlag() {
        return deleteFlag;
    }

    public void setDeleteFlag(Integer deleteFlag) {
        this.deleteFlag = deleteFlag;
    }

    public String getDriverId() {
        return driverId;
    }

    public void setDriverId(String driverId) {
        this.driverId = driverId;
    }

    public String getDriverName() {
        return driverName;
    }

    public void setDriverName(String driverName) {
        this.driverName = driverName;
    }

    public String getCreateUserName() {
        return createUserName;
    }

    public void setCreateUserName(String createUserName) {
        this.createUserName = createUserName;
    }

    public Date getGmtCreate() {
        return gmtCreate;
    }

    public void setGmtCreate(Date gmtCreate) {
        this.gmtCreate = gmtCreate;
    }

    public String getModifyUserName() {
        return modifyUserName;
    }

    public void setModifyUserName(String modifyUserName) {
        this.modifyUserName = modifyUserName;
    }

    public Date getGmtUpdate() {
        return gmtUpdate;
    }

    public void setGmtUpdate(Date gmtUpdate) {
        this.gmtUpdate = gmtUpdate;
    }

    public Byte getIsDeleted() {
        return isDeleted;
    }

    public void setIsDeleted(Byte isDeleted) {
        this.isDeleted = isDeleted;
    }

    public String getVersions() {
        return versions;
    }

    public void setVersions(String versions) {
        this.versions = versions;
    }

    public Long getVdId() {
        return vdId;
    }

    public void setVdId(Long vdId) {
        this.vdId = vdId;
    }

    public Long getVdPpId() {
        return vdPpId;
    }

    public void setVdPpId(Long vdPpId) {
        this.vdPpId = vdPpId;
    }

    public String getVdVin() {
        return vdVin;
    }

    public void setVdVin(String vdVin) {
        this.vdVin = vdVin;
    }

    public String getVdVehicleName() {
        return vdVehicleName;
    }

    public void setVdVehicleName(String vdVehicleName) {
        this.vdVehicleName = vdVehicleName;
    }

    public String getVdVehicleDesc() {
        return vdVehicleDesc;
    }

    public void setVdVehicleDesc(String vdVehicleDesc) {
        this.vdVehicleDesc = vdVehicleDesc;
    }

    public String getVdWaybillNo() {
        return vdWaybillNo;
    }

    public void setVdWaybillNo(String vdWaybillNo) {
        this.vdWaybillNo = vdWaybillNo;
    }

    public String getVdOutstockStatus() {
        return vdOutstockStatus;
    }

    public void setVdOutstockStatus(String vdOutstockStatus) {
        this.vdOutstockStatus = vdOutstockStatus;
    }

    public String getHoHandoverNumber() {
        return hoHandoverNumber;
    }

    public void setHoHandoverNumber(String hoHandoverNumber) {
        this.hoHandoverNumber = hoHandoverNumber;
    }

    @Override
    public String toString() {
        return "WmsPreparationVehicleDetailBO{" +
                "vdId=" + vdId +
                ", vdPpId=" + vdPpId +
                ", vdVin='" + vdVin + '\'' +
                ", vdVehicleName='" + vdVehicleName + '\'' +
                ", vdVehicleDesc='" + vdVehicleDesc + '\'' +
                ", vdWaybillNo='" + vdWaybillNo + '\'' +
                ", vdOutstockStatus='" + vdOutstockStatus + '\'' +
                '}';
    }
}
