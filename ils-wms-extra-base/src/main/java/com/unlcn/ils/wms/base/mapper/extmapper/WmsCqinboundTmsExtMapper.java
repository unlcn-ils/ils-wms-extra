package com.unlcn.ils.wms.base.mapper.extmapper;

import com.unlcn.ils.wms.base.model.inbound.WmsCqinboundTms;

import java.util.HashMap;
import java.util.List;

public interface WmsCqinboundTmsExtMapper {

    List<WmsCqinboundTms> selectFailedList(HashMap<String, Object> paramMap);

    int selectTotalFailedCount(HashMap<String, Object> paramMap);

    List<WmsCqinboundTms> selectLastGateInFailDataByParam(HashMap<String, Object> paramMap);

    List<WmsCqinboundTms> selectLastGateOutFailDataByParam(HashMap<String, Object> paramMap);
}