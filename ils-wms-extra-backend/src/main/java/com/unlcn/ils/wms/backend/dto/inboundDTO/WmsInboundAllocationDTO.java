package com.unlcn.ils.wms.backend.dto.inboundDTO;

import java.util.Date;

/**
 * Created by DELL on 2017/8/18.
 */
public class WmsInboundAllocationDTO {

    private Long alId;

    private String alCode;

    private String alType;

    private String alStatus;

    private String alWhId;

    private String alOdId;

    private String alCustomerId;

    private String alCustomerCode;

    private String alCustomerName;

    private String alSourceZoneId;

    private String alSourceZoneCode;

    private String alSourceZoneName;

    private String alTargetZoneId;

    private String alTargetZoneCode;

    private String alTargetZoneName;

    private String alSourceLocId;

    private String alSourceLocCode;

    private String alSourceLocName;

    private String alTargetLocId;

    private String alTargetLocCode;

    private String alTargetLocName;

    private String alWaybillNo;

    private String alVin;

    private String alVehicleSpecCode;

    private String alVehicleSpecName;

    private String createUserId;

    private String createUserName;

    private String modifyUserId;

    private String modifyUserName;

    private Date gmtCreate;

    private Date gmtUpdate;

    private Byte isDeleted;


    private Long versions;

    public Long getAlId() {
        return alId;
    }

    public void setAlId(Long alId) {
        this.alId = alId;
    }

    public String getAlCode() {
        return alCode;
    }

    public void setAlCode(String alCode) {
        this.alCode = alCode;
    }

    public String getAlType() {
        return alType;
    }

    public void setAlType(String alType) {
        this.alType = alType;
    }

    public String getAlStatus() {
        return alStatus;
    }

    public void setAlStatus(String alStatus) {
        this.alStatus = alStatus;
    }

    public String getAlWhId() {
        return alWhId;
    }

    public void setAlWhId(String alWhId) {
        this.alWhId = alWhId;
    }

    public String getAlOdId() {
        return alOdId;
    }

    public void setAlOdId(String alOdId) {
        this.alOdId = alOdId;
    }

    public String getAlCustomerId() {
        return alCustomerId;
    }

    public void setAlCustomerId(String alCustomerId) {
        this.alCustomerId = alCustomerId;
    }

    public String getAlCustomerCode() {
        return alCustomerCode;
    }

    public void setAlCustomerCode(String alCustomerCode) {
        this.alCustomerCode = alCustomerCode;
    }

    public String getAlCustomerName() {
        return alCustomerName;
    }

    public void setAlCustomerName(String alCustomerName) {
        this.alCustomerName = alCustomerName;
    }

    public String getAlSourceZoneId() {
        return alSourceZoneId;
    }

    public void setAlSourceZoneId(String alSourceZoneId) {
        this.alSourceZoneId = alSourceZoneId;
    }

    public String getAlSourceZoneCode() {
        return alSourceZoneCode;
    }

    public void setAlSourceZoneCode(String alSourceZoneCode) {
        this.alSourceZoneCode = alSourceZoneCode;
    }

    public String getAlSourceZoneName() {
        return alSourceZoneName;
    }

    public void setAlSourceZoneName(String alSourceZoneName) {
        this.alSourceZoneName = alSourceZoneName;
    }

    public String getAlTargetZoneId() {
        return alTargetZoneId;
    }

    public void setAlTargetZoneId(String alTargetZoneId) {
        this.alTargetZoneId = alTargetZoneId;
    }

    public String getAlTargetZoneCode() {
        return alTargetZoneCode;
    }

    public void setAlTargetZoneCode(String alTargetZoneCode) {
        this.alTargetZoneCode = alTargetZoneCode;
    }

    public String getAlTargetZoneName() {
        return alTargetZoneName;
    }

    public void setAlTargetZoneName(String alTargetZoneName) {
        this.alTargetZoneName = alTargetZoneName;
    }

    public String getAlSourceLocId() {
        return alSourceLocId;
    }

    public void setAlSourceLocId(String alSourceLocId) {
        this.alSourceLocId = alSourceLocId;
    }

    public String getAlSourceLocCode() {
        return alSourceLocCode;
    }

    public void setAlSourceLocCode(String alSourceLocCode) {
        this.alSourceLocCode = alSourceLocCode;
    }

    public String getAlSourceLocName() {
        return alSourceLocName;
    }

    public void setAlSourceLocName(String alSourceLocName) {
        this.alSourceLocName = alSourceLocName;
    }

    public String getAlTargetLocId() {
        return alTargetLocId;
    }

    public void setAlTargetLocId(String alTargetLocId) {
        this.alTargetLocId = alTargetLocId;
    }

    public String getAlTargetLocCode() {
        return alTargetLocCode;
    }

    public void setAlTargetLocCode(String alTargetLocCode) {
        this.alTargetLocCode = alTargetLocCode;
    }

    public String getAlTargetLocName() {
        return alTargetLocName;
    }

    public void setAlTargetLocName(String alTargetLocName) {
        this.alTargetLocName = alTargetLocName;
    }

    public String getAlWaybillNo() {
        return alWaybillNo;
    }

    public void setAlWaybillNo(String alWaybillNo) {
        this.alWaybillNo = alWaybillNo;
    }

    public String getAlVin() {
        return alVin;
    }

    public void setAlVin(String alVin) {
        this.alVin = alVin;
    }

    public String getAlVehicleSpecCode() {
        return alVehicleSpecCode;
    }

    public void setAlVehicleSpecCode(String alVehicleSpecCode) {
        this.alVehicleSpecCode = alVehicleSpecCode;
    }

    public String getAlVehicleSpecName() {
        return alVehicleSpecName;
    }

    public void setAlVehicleSpecName(String alVehicleSpecName) {
        this.alVehicleSpecName = alVehicleSpecName;
    }

    public String getCreateUserId() {
        return createUserId;
    }

    public void setCreateUserId(String createUserId) {
        this.createUserId = createUserId;
    }

    public String getCreateUserName() {
        return createUserName;
    }

    public void setCreateUserName(String createUserName) {
        this.createUserName = createUserName;
    }

    public String getModifyUserId() {
        return modifyUserId;
    }

    public void setModifyUserId(String modifyUserId) {
        this.modifyUserId = modifyUserId;
    }

    public String getModifyUserName() {
        return modifyUserName;
    }

    public void setModifyUserName(String modifyUserName) {
        this.modifyUserName = modifyUserName;
    }

    public Date getGmtCreate() {
        return gmtCreate;
    }

    public void setGmtCreate(Date gmtCreate) {
        this.gmtCreate = gmtCreate;
    }

    public Date getGmtUpdate() {
        return gmtUpdate;
    }

    public void setGmtUpdate(Date gmtUpdate) {
        this.gmtUpdate = gmtUpdate;
    }

    public Byte getIsDeleted() {
        return isDeleted;
    }

    public void setIsDeleted(Byte isDeleted) {
        this.isDeleted = isDeleted;
    }

    public Long getVersions() {
        return versions;
    }

    public void setVersions(Long versions) {
        this.versions = versions;
    }

    @Override
    public String toString() {
        return "WmsInboundAllocationDTO{" +
                "alId=" + alId +
                ", alCode='" + alCode + '\'' +
                ", alType='" + alType + '\'' +
                ", alStatus='" + alStatus + '\'' +
                ", alWhId='" + alWhId + '\'' +
                ", alOdId='" + alOdId + '\'' +
                ", alCustomerId='" + alCustomerId + '\'' +
                ", alCustomerCode='" + alCustomerCode + '\'' +
                ", alCustomerName='" + alCustomerName + '\'' +
                ", alSourceZoneId='" + alSourceZoneId + '\'' +
                ", alSourceZoneCode='" + alSourceZoneCode + '\'' +
                ", alSourceZoneName='" + alSourceZoneName + '\'' +
                ", alTargetZoneId='" + alTargetZoneId + '\'' +
                ", alTargetZoneCode='" + alTargetZoneCode + '\'' +
                ", alTargetZoneName='" + alTargetZoneName + '\'' +
                ", alSourceLocId='" + alSourceLocId + '\'' +
                ", alSourceLocCode='" + alSourceLocCode + '\'' +
                ", alSourceLocName='" + alSourceLocName + '\'' +
                ", alTargetLocId='" + alTargetLocId + '\'' +
                ", alTargetLocCode='" + alTargetLocCode + '\'' +
                ", alTargetLocName='" + alTargetLocName + '\'' +
                ", alWaybillNo='" + alWaybillNo + '\'' +
                ", alVin='" + alVin + '\'' +
                ", alVehicleSpecCode='" + alVehicleSpecCode + '\'' +
                ", alVehicleSpecName='" + alVehicleSpecName + '\'' +
                ", createUserId='" + createUserId + '\'' +
                ", createUserName='" + createUserName + '\'' +
                ", modifyUserId='" + modifyUserId + '\'' +
                ", modifyUserName='" + modifyUserName + '\'' +
                ", gmtCreate=" + gmtCreate +
                ", gmtUpdate=" + gmtUpdate +
                ", isDeleted=" + isDeleted +
                ", versions=" + versions +
                '}';
    }
}
