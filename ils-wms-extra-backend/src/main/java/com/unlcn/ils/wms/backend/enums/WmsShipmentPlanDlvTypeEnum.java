package com.unlcn.ils.wms.backend.enums;

import java.util.Objects;

/**
 * 发运类型
 *
 * @author renml
 */
public enum WmsShipmentPlanDlvTypeEnum {
    A1("A1", "正常发运"),
    A2("A2", "调拨发运");

    private final String value;
    private final String text;

    WmsShipmentPlanDlvTypeEnum(String value, String text) {
        this.value = value;
        this.text = text;
    }

    public String getValue() {
        return value;
    }

    public String getText() {
        return text;
    }

    public static WmsShipmentPlanDlvTypeEnum getByValue(String value) {
        for (WmsShipmentPlanDlvTypeEnum temp : WmsShipmentPlanDlvTypeEnum.values()) {
            if (Objects.equals(temp.getValue(), value)) {
                return temp;
            }
        }
        return null;
    }
}
