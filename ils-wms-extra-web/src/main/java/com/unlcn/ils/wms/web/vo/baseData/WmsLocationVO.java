package com.unlcn.ils.wms.web.vo.baseData;

import cn.huiyunche.commons.domain.PageVo;

import java.util.Date;

/**
 * Created by DELL on 2017/8/8.
 */
public class WmsLocationVO extends PageVo{

    /**
     * 库位ID
     */
    private Long locId;

    /**
     * 仓库ID
     */
    private String locWhId;

    /**
     * 仓库CODE
     */
    private String locWhCode;

    /**
     * 仓库名称
     */
    private String locWhName;

    /**
     * 库区ID
     */
    private String locZoneId;

    /**
     * 库区CODE
     */
    private String locZoneCode;

    /**
     * 库区名称
     */
    private String locZoneName;

    /**
     * 库位CODE
     */
    private String locCode;

    /**
     * 库位名称
     */
    private String locName;

    /**
     * 库位行
     */
    private Long locRow;

    /**
     * 库位列
     */
    private Long locColumn;

    /**
     * 库位层
     */
    private Long locLevel;

    /**
     * 库位长
     */
    private Long locLength;

    /**
     * 库位宽
     */
    private Long locWidth;

    /**
     * 启用禁用标识
     */
    private String locEnableFlag;

    /**
     * 创建人
     */
    private String createPerson;

    /**
     * 修改人
     */
    private String updatePerson;

    /**
     * 创建时间
     */
    private Date gmtCreate;

    /**
     * 修改时间
     */
    private Date gmtUpdate;

    /**
     * 逻辑删除
     */
    private Byte isDeleted;

    /**
     * 版本
     */
    private Long versions;

    private String locType;

    /**
     *
     */
    private String locSize;

    public Long getLocId() {
        return locId;
    }

    public void setLocId(Long locId) {
        this.locId = locId;
    }

    public String getLocWhId() {
        return locWhId;
    }

    public void setLocWhId(String locWhId) {
        this.locWhId = locWhId;
    }

    public String getLocWhCode() {
        return locWhCode;
    }

    public void setLocWhCode(String locWhCode) {
        this.locWhCode = locWhCode;
    }

    public String getLocWhName() {
        return locWhName;
    }

    public void setLocWhName(String locWhName) {
        this.locWhName = locWhName;
    }

    public String getLocZoneId() {
        return locZoneId;
    }

    public void setLocZoneId(String locZoneId) {
        this.locZoneId = locZoneId;
    }

    public String getLocZoneCode() {
        return locZoneCode;
    }

    public void setLocZoneCode(String locZoneCode) {
        this.locZoneCode = locZoneCode;
    }

    public String getLocZoneName() {
        return locZoneName;
    }

    public void setLocZoneName(String locZoneName) {
        this.locZoneName = locZoneName;
    }

    public String getLocCode() {
        return locCode;
    }

    public void setLocCode(String locCode) {
        this.locCode = locCode;
    }

    public String getLocName() {
        return locName;
    }

    public void setLocName(String locName) {
        this.locName = locName;
    }

    public Long getLocRow() {
        return locRow;
    }

    public void setLocRow(Long locRow) {
        this.locRow = locRow;
    }

    public Long getLocColumn() {
        return locColumn;
    }

    public void setLocColumn(Long locColumn) {
        this.locColumn = locColumn;
    }

    public Long getLocLevel() {
        return locLevel;
    }

    public void setLocLevel(Long locLevel) {
        this.locLevel = locLevel;
    }

    public Long getLocLength() {
        return locLength;
    }

    public void setLocLength(Long locLength) {
        this.locLength = locLength;
    }

    public Long getLocWidth() {
        return locWidth;
    }

    public void setLocWidth(Long locWidth) {
        this.locWidth = locWidth;
    }

    public String getLocEnableFlag() {
        return locEnableFlag;
    }

    public void setLocEnableFlag(String locEnableFlag) {
        this.locEnableFlag = locEnableFlag;
    }

    public String getCreatePerson() {
        return createPerson;
    }

    public void setCreatePerson(String createPerson) {
        this.createPerson = createPerson;
    }

    public String getUpdatePerson() {
        return updatePerson;
    }

    public void setUpdatePerson(String updatePerson) {
        this.updatePerson = updatePerson;
    }

    public Date getGmtCreate() {
        return gmtCreate;
    }

    public void setGmtCreate(Date gmtCreate) {
        this.gmtCreate = gmtCreate;
    }

    public Date getGmtUpdate() {
        return gmtUpdate;
    }

    public void setGmtUpdate(Date gmtUpdate) {
        this.gmtUpdate = gmtUpdate;
    }

    public Byte getIsDeleted() {
        return isDeleted;
    }

    public void setIsDeleted(Byte isDeleted) {
        this.isDeleted = isDeleted;
    }

    public Long getVersions() {
        return versions;
    }

    public void setVersions(Long versions) {
        this.versions = versions;
    }

    public String getLocType() {
        return locType;
    }

    public void setLocType(String locType) {
        this.locType = locType;
    }

    public String getLocSize() {
        return locSize;
    }

    public void setLocSize(String locSize) {
        this.locSize = locSize;
    }

    @Override
    public String toString() {
        return "WmsLocationVO{" +
                "locId=" + locId +
                ", locWhId='" + locWhId + '\'' +
                ", locWhCode='" + locWhCode + '\'' +
                ", locWhName='" + locWhName + '\'' +
                ", locZoneId='" + locZoneId + '\'' +
                ", locZoneCode='" + locZoneCode + '\'' +
                ", locZoneName='" + locZoneName + '\'' +
                ", locCode='" + locCode + '\'' +
                ", locName='" + locName + '\'' +
                ", locRow=" + locRow +
                ", locColumn=" + locColumn +
                ", locLevel=" + locLevel +
                ", locLength=" + locLength +
                ", locWidth=" + locWidth +
                ", locEnableFlag=" + locEnableFlag +
                ", createPerson='" + createPerson + '\'' +
                ", updatePerson='" + updatePerson + '\'' +
                ", gmtCreate=" + gmtCreate +
                ", gmtUpdate=" + gmtUpdate +
                ", isDeleted=" + isDeleted +
                ", versions=" + versions +
                '}';
    }
}
