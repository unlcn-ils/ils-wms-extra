package com.unlcn.ils.wms.backend.service.inbound;

import com.unlcn.ils.wms.base.businessDTO.baseData.WmsEmptyLocationDTO;
import com.unlcn.ils.wms.base.model.stock.WmsLocation;
import com.unlcn.ils.wms.base.model.stock.WmsZone;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * 库区库位service
 * Created by DELL on 2017/8/8.
 */
public interface WmsZoneLocationService {

    /**
     * 根据条件获得库区数据
     * @param whCode
     * @return
     */
    List<WmsZone> getZone(@Param(value = "whCode") String whCode);

    /**
     * 获得空库区
     * @param whCode
     * @return
     */
    List<WmsEmptyLocationDTO> getEmptyZone(@Param(value = "whCode") String whCode);

    /**
     * 根据条件获得库位数据
     * @param whCode
     * @return
     */
    List<WmsLocation> getLocation(@Param(value = "whCode") String whCode);

    /**
     * 根据条件获得当前仓库下的所有空库位
     * 同时包括库区数据
     * @param whCode
     * @return
     */
    List<WmsEmptyLocationDTO> getEmptyLocation(@Param(value = "whCode") String whCode,@Param(value = "zoneId") String zoneId);

    /**
     * 根据仓库以及类型查询库区
     * @param whCode
     * @param whType
     * @return
     */
    List<WmsEmptyLocationDTO> queryQualifiedZone(@Param(value = "whCode") String whCode,@Param(value = "whType") String whType);

}
