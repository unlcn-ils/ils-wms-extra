package com.unlcn.ils.wms.backend.service.sys;

import com.unlcn.ils.wms.backend.bo.sys.PermissionsBO;
import com.unlcn.ils.wms.backend.dto.sysDTO.PermissNewDTO;
import com.unlcn.ils.wms.base.dto.SysPermissDTO;

import java.util.List;

/**
 * @author laishijian
 * @ClassName: PermissionsService
 * @Description: 权限
 * @date 2017年8月8日 下午4:01:12
 */
public interface PermissionsService {

    /**
     * @param @param  permissionsBO
     * @param @return
     * @return Integer    返回类型
     * @throws
     * @Title: add
     * @Description: 新增权限
     */
    Integer add(PermissionsBO permissionsBO) throws Exception;

    /**
     * @param @param  id
     * @param @return
     * @param @throws Exception
     * @return PermissionsBO    返回类型
     * @throws
     * @Title: findById
     * @Description: 根据id查询
     */
    PermissionsBO findById(Integer id) throws Exception;

    /**
     * @param @param  roleId 角色id
     * @param @param  permissionsId	父级权限id
     * @param @return
     * @param @throws Exception
     * @return List<PermissionsBO>    返回类型
     * @throws
     * @Title: findByRoleId
     * @Description: 根据角色id获取权限
     */
    List<PermissionsBO> findByRoleId(Integer roleId, Integer permissionsId) throws Exception;

    /**
     * @param @return
     * @return List<PermissionsBO>    返回类型
     * @throws
     * @Title: findPermissionsAndChildAll
     * @Description: 获取所有权限
     */
    List<PermissionsBO> findAll();

    /**
     * @param @return
     * @return List<PermissionsBO>    返回类型
     * @throws
     * @Title: findPermissionsAndChildAll
     * @Description: 获取所有权限
     */
    List<PermissionsBO> findAllForEnabled();

    /**
     * @param @param  parentId
     * @param @return
     * @return List<PermissionsBO>    返回类型
     * @throws
     * @Title: findPermissionsAndChildByParentId
     * @Description: 根据父权限id获取权限和子权限
     */
    List<PermissionsBO> findPermissionsAndChildByParentId(Integer parentId);

    /**
     * 根据角色ID集合获取权限
     *
     * @param roleIdList
     * @return
     * @throws Exception
     */
    List<SysPermissDTO> getPermissListByRoleIdList(List<Integer> roleIdList) throws Exception;

    /**
     * 根据角色ID集合获取权限
     *
     * @param userId
     * @return
     * @throws Exception
     */
    List<SysPermissDTO> getPermissListByUserId(Integer userId) throws Exception;

    /**
     * 根据用户id和节点获取菜单
     *
     * @param parentId
     * @param userId
     * @return
     */
    List<PermissNewDTO> findPermissionsAndChildByParentIdAndUserId(Integer parentId, Integer userId, Long warehouseId);

}
