package com.unlcn.ils.wms.backend.service.inbound;

import com.unlcn.ils.wms.backend.bo.inboundBO.WmsInboundAllocationBO;
import com.unlcn.ils.wms.backend.bo.inboundBO.WmsInboundOrderBO;
import com.unlcn.ils.wms.base.model.inbound.WmsInboundAllocation;

import java.util.List;
import java.util.Map;

/**
 * Created by DELL on 2017/8/8.
 */
public interface WmsAllocationService {
    /**
     * 新增分配任务
     *
     * @param wmsInboundAlloc
     */
    void addAllocation(WmsInboundAllocation wmsInboundAlloc);

    /**
     * 根据订单ID修改分配状态
     *
     * @param list
     * @param status
     */
    void updateAllocationByAsnId(List<WmsInboundOrderBO> list, String status);

    /**
     * 根据订单ID删除分配数据
     *
     * @param asnId
     */
    void deleteAllocationByAsnId(String asnId);

    /**
     * 库位调整
     *
     * @param wmsInboundAllocationBO
     */
    void updateAllocationByOdId(WmsInboundAllocationBO wmsInboundAllocationBO);

    Map<String, Object> selectStockMoveRecord(WmsInboundAllocationBO wmsInboundAllocationBO) throws IllegalAccessException;

}