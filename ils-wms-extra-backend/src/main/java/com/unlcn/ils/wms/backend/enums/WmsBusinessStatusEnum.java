package com.unlcn.ils.wms.backend.enums;

public enum WmsBusinessStatusEnum {
    WNH_NOT_INBOUND("10", "未入库"),
    WNH_INBOUND_PART("20", "部分入库"),
    WNH_INBOUND_ALL("30", "全部入库"),
    WND_NOT_RECEIVE("10", "未收货"),
    WND_CHECKED("20", "已质检"),
    WND_RECEIVED("30", "已收货"),
    WND_INBOUND("40", "已入位");

    private final String code;
    private final String text;

    WmsBusinessStatusEnum(String code, String text) {
        this.code = code;
        this.text = text;
    }

    public String getCode() {
        return code;
    }

    public String getText() {
        return text;
    }

}
