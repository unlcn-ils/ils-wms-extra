package com.unlcn.ils.wms.base.mapper.extmapper;

import com.unlcn.ils.wms.base.model.sys.WmsCard;
import com.unlcn.ils.wms.base.model.sys.WmsCardExample;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

public interface WmsCardExtMapper {

    /**
     * 门禁卡列表
     * @param paramsMap
     * @return
     */
    List<WmsCard> cardLine(@Param("paramsMap") Map<String, Object> paramsMap);

    /**
     * 门禁卡列表统计
     * @param paramsMap
     * @return
     */
    Integer cardLineCount(@Param("paramsMap") Map<String, Object> paramsMap);
}