package com.unlcn.ils.wms.backend.enums;

public enum WmsInboundOrderStockTransferEnum {
    STOCK_NORMAL("1", "正常业务数据"),
    STOCK_NOTCQ("0", "西南库中转数据"),
    ST_STATUS_10("10", "正常业务无需处理"),
    ST_STATUS_NOTDEAL("20", "中转未处理"),
    ST_STATUS_DEALED("30", "中转已处理"),
    OD_DELETED("DELETE", "已删除入库信息"),
    OD_MODIFY("MODIFY", "修改入库信息");

    private final String code;
    private final String text;

    WmsInboundOrderStockTransferEnum(String value, String text) {
        this.code = value;
        this.text = text;
    }

    public String getCode() {
        return code;
    }

    public String getText() {
        return text;
    }

    public static WmsInboundOrderStockTransferEnum getByValue(String value) {
        for (WmsInboundOrderStockTransferEnum temp : WmsInboundOrderStockTransferEnum.values()) {
            if (temp.getCode().equals(value)) {
                return temp;
            }
        }
        return null;
    }
}
