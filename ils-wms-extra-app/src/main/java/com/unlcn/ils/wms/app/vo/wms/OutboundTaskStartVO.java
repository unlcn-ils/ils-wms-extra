package com.unlcn.ils.wms.app.vo.wms;

import java.io.Serializable;

public class OutboundTaskStartVO implements Serializable {
    private static final long serialVersionUID = 1L;

    private Long taskId;

    private String vin;

    private String status;

    public Long getTaskId() {
        return taskId;
    }

    public void setTaskId(Long taskId) {
        this.taskId = taskId;
    }

    public String getVin() {
        return vin;
    }

    public void setVin(String vin) {
        this.vin = vin;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append("OutboundTaskStartVO [taskId=");
        builder.append(taskId);
        builder.append(", vin=");
        builder.append(vin);
        builder.append(", status=");
        builder.append(status);
        builder.append("]");
        return builder.toString();
    }

}
