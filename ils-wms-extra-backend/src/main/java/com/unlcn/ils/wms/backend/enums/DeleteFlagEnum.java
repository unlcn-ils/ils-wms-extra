package com.unlcn.ils.wms.backend.enums;

public enum DeleteFlagEnum {

    DELETED((byte)0, "已删除"),
    NORMAL((byte)1, "正常");

    private final byte value;
    private final String text;

    DeleteFlagEnum(byte value, String text) {
        this.value = value;
        this.text = text;
    }

    public byte getValue() {
        return value;
    }

    public String getText() {
        return text;
    }
}
