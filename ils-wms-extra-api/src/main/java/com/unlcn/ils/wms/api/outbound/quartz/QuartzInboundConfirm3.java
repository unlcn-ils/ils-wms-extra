package com.unlcn.ils.wms.api.outbound.quartz;

import com.unlcn.ils.wms.backend.service.inbound.AsnOrderService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component("inboundConfirmJob3")
public class QuartzInboundConfirm3 {

    private Logger logger = LoggerFactory.getLogger(QuartzInboundConfirm3.class);

    @Autowired
    private AsnOrderService asnOrderService;

    public void inboundConfirm3() throws Exception {
        logger.info("QuartzInboundConfirm1.inboundConfirm3 定时任务start========");
        asnOrderService.getInboundSendMailData();
        logger.info("QuartzInboundConfirm1.inboundConfirm3 定时任务end========");
    }
}
