package com.unlcn.ils.wms.base.mapper.sys;

import com.unlcn.ils.wms.base.model.sys.TmsCallHistory;
import com.unlcn.ils.wms.base.model.sys.TmsCallHistoryExample;
import org.apache.ibatis.annotations.Param;

import java.util.List;


public interface TmsCallHistoryMapper {
    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table tms_order_call_history
     *
     * @mbggenerated Sun Mar 05 03:17:20 CST 2017
     */
    int countByExample(TmsCallHistoryExample example);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table tms_order_call_history
     *
     * @mbggenerated Sun Mar 05 03:17:20 CST 2017
     */
    int deleteByExample(TmsCallHistoryExample example);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table tms_order_call_history
     *
     * @mbggenerated Sun Mar 05 03:17:20 CST 2017
     */
    int deleteByPrimaryKey(Long id);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table tms_order_call_history
     *
     * @mbggenerated Sun Mar 05 03:17:20 CST 2017
     */
    int insert(TmsCallHistory record);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table tms_order_call_history
     *
     * @mbggenerated Sun Mar 05 03:17:20 CST 2017
     */
    int insertSelective(TmsCallHistory record);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table tms_order_call_history
     *
     * @mbggenerated Sun Mar 05 03:17:20 CST 2017
     */
    List<TmsCallHistory> selectByExample(TmsCallHistory example);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table tms_order_call_history
     *
     * @mbggenerated Sun Mar 05 03:17:20 CST 2017
     */
    TmsCallHistory selectByPrimaryKey(Long id);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table tms_order_call_history
     *
     * @mbggenerated Sun Mar 05 03:17:20 CST 2017
     */
    int updateByExampleSelective(@Param("record") TmsCallHistory record, @Param("example") TmsCallHistoryExample example);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table tms_order_call_history
     *
     * @mbggenerated Sun Mar 05 03:17:20 CST 2017
     */
    int updateByExample(@Param("record") TmsCallHistory record, @Param("example") TmsCallHistoryExample example);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table tms_order_call_history
     *
     * @mbggenerated Sun Mar 05 03:17:20 CST 2017
     */
    int updateByPrimaryKeySelective(TmsCallHistory record);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table tms_order_call_history
     *
     * @mbggenerated Sun Mar 05 03:17:20 CST 2017
     */
    int updateByPrimaryKey(TmsCallHistory record);
}