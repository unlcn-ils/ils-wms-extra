package com.unlcn.ils.wms.backend.enums;

/**
 * @Auther linbao
 * @Date 2017-11-14
 * 离场类型
 */
public enum WmsDepartureTypeEnum {

    TMS_SUCCESS((byte) 10, "自动tms调用成功开启"),
    WMS_HAND((byte) 20, "页面手动开启闸门"),
    TMS_FAILED((byte) 30, "系统闸门开启失败");

    private final byte value;
    private final String text;

    public byte getValue() {
        return value;
    }

    public String getText() {
        return text;
    }

    WmsDepartureTypeEnum(byte value, String text) {
        this.value = value;
        this.text = text;
    }

    public static WmsDepartureTypeEnum getByValue(byte value) {
        for (WmsDepartureTypeEnum temp : WmsDepartureTypeEnum.values()) {
            if (temp.getValue() == value) {
                return temp;
            }
        }
        return null;
    }
}
