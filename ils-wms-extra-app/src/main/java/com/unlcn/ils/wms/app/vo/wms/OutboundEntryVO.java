package com.unlcn.ils.wms.app.vo.wms;

/**
 * Created by houjianhui on 2017/5/5.
 */
public class OutboundEntryVO {

    private String waybillCode;
    private String warehouseId;
    private String userNo;
    private String waybillOrigin;
    private String waybillDest;
    private Integer vehicleAmt;

    private String details;
    private String excpDetails;    
    private Integer userId;
    private String userName;   

	public String getWaybillCode() {
        return waybillCode;
    }

    public void setWaybillCode(String waybillCode) {
        this.waybillCode = waybillCode;
    }

    public String getWarehouseId() {
        return warehouseId;
    }

    public void setWarehouseId(String warehouseId) {
        this.warehouseId = warehouseId;
    }

    public String getUserNo() {
        return userNo;
    }

    public void setUserNo(String userNo) {
        this.userNo = userNo;
    }

    public String getWaybillOrigin() {
        return waybillOrigin;
    }

    public void setWaybillOrigin(String waybillOrigin) {
        this.waybillOrigin = waybillOrigin;
    }

    public String getWaybillDest() {
        return waybillDest;
    }

    public void setWaybillDest(String waybillDest) {
        this.waybillDest = waybillDest;
    }

    public Integer getVehicleAmt() {
        return vehicleAmt;
    }

    public void setVehicleAmt(Integer vehicleAmt) {
        this.vehicleAmt = vehicleAmt;
    }

    public String getDetails() {
        return details;
    }

    public void setDetails(String details) {
        this.details = details;
    }

    public String getExcpDetails() {
        return excpDetails;
    }

    public void setExcpDetails(String excpDetails) {
        this.excpDetails = excpDetails;
    }

    public Integer getUserId() {
		return userId;
	}

	public void setUserId(Integer userId) {
		this.userId = userId;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}
	
    @Override
    public String toString() {
        return "OutboundEntryVO{" +
                "waybillCode='" + waybillCode + '\'' +
                ", warehouseId='" + warehouseId + '\'' +
                ", userNo='" + userNo + '\'' +
                ", waybillOrigin='" + waybillOrigin + '\'' +
                ", waybillDest='" + waybillDest + '\'' +
                ", vehicleAmt=" + vehicleAmt +
                ", details='" + details + '\'' +
                ", excpDetails='" + excpDetails + '\'' +
                ", userId='" + userId + '\'' +
                ", userName='" + userName + '\'' +
                '}';
    }
}
