package com.unlcn.ils.wms.backend.enums;

/**
 * 检验结果、维修结果
 * Created by DELL on 2017/8/24.
 */
public enum AsnOrderCheckEnum {
    ASNORDER_PASS(10,"合格"),
    ASNORDER_NOTPASS_IN_REPAIR(20,"不合格-库内返工"),
    ASNORDER_NOTPASS_OUT_REPAIR(30,"不合格-委外返工"),
    ASNORDER_NOTPASS(40,"不合格-退厂");

    private final int value;
    private final String text;
    AsnOrderCheckEnum(int value, String text){
        this.value = value;
        this.text = text;
    }

    public int getValue() {
        return value;
    }

    public String getText() {
        return text;
    }

    public static AsnOrderCheckEnum getByValue(int value){
        for (AsnOrderCheckEnum temp : AsnOrderCheckEnum.values()) {
            if(temp.getValue() == value){
                return temp;
            }
        }
        return null;
    }
}
