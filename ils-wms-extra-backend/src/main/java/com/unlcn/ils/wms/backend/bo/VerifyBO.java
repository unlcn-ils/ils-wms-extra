package com.unlcn.ils.wms.backend.bo;

/**
 * @Auther linbao
 * @Date 2017-10-18
 */
public class VerifyBO {

    private String token;

    private Long time;

    private Integer userId;

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public Long getTime() {
        return time;
    }

    public void setTime(Long time) {
        this.time = time;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    @Override
    public String toString() {
        return "VerifyBO{" +
                "token='" + token + '\'' +
                ", time=" + time +
                ", userId=" + userId +
                '}';
    }
}
