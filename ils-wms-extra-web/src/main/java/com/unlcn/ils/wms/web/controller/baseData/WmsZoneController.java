package com.unlcn.ils.wms.web.controller.baseData;

import cn.huiyunche.commons.domain.ResultDTO;
import cn.huiyunche.commons.exception.BusinessException;
import com.unlcn.ils.wms.backend.bo.baseDataBO.WmsZoneBO;
import com.unlcn.ils.wms.backend.service.baseData.WmsZoneService;
import com.unlcn.ils.wms.base.businessDTO.baseData.WmsZoneQueryDTO;
import com.unlcn.ils.wms.web.utils.Vo2Bo;
import com.unlcn.ils.wms.web.vo.baseData.WmsZoneVO;
import jodd.util.StringUtil;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.List;
import java.util.Objects;

/**
 * wms库区管理
 * Created by DELL on 2017/8/7.
 */
@Controller
@RequestMapping("/wmsZone")
@ResponseBody
public class WmsZoneController {
    private Logger LOGGER = LoggerFactory.getLogger(WmsZoneController.class);

    @Autowired
    private WmsZoneService wmsZoneService;

    /**
     * 查询库区分页
     * @param wmsZoneVO
     * @return
     * @throws BusinessException
     */
    @RequestMapping(value = "/list", method = RequestMethod.POST)
    public ResultDTO list(@RequestBody WmsZoneVO wmsZoneVO, HttpServletRequest request) throws BusinessException {
        LOGGER.info("WmsZoneController.getZoneList param:{}.", wmsZoneVO);
        ResultDTO<Object> resultDTO = new ResultDTO<Object>(true, null, "查询成功");
        try {
            WmsZoneQueryDTO wmsZoneQueryDTO = new WmsZoneQueryDTO();
            BeanUtils.copyProperties(wmsZoneVO, wmsZoneQueryDTO);
            resultDTO.setMessage("查询成功");
            resultDTO.setMessageCode("200");
            resultDTO.setSuccess(true);
            String whCode = request.getHeader("whCode");
            if (StringUtils.isBlank(whCode)){
                return resultDTO;
            }
            wmsZoneQueryDTO.setZeWhCode(whCode);
            resultDTO.setData(wmsZoneService.listZone(wmsZoneQueryDTO));
        } catch (Exception ex) {
            LOGGER.error("WmsZoneController.list error: {}", ex);
            resultDTO.setMessage("查询异常");
            resultDTO.setMessageCode("500");
            resultDTO.setSuccess(false);
        }
        return resultDTO;
    }

    /**
     * 新增库区
     * @param wmsZoneVO
     * @return
     */
    @RequestMapping(value="/addZone",method = RequestMethod.POST)
    public ResultDTO addZone(@RequestBody WmsZoneVO wmsZoneVO){
        ResultDTO resultDTO = new ResultDTO();
        try {
            wmsZoneVO.setIsDeleted((byte)0);
            WmsZoneBO wmsZoneBO = new WmsZoneBO();
            BeanUtils.copyProperties(wmsZoneVO, wmsZoneBO);
            boolean result = wmsZoneService.addZone(wmsZoneBO);
            if(!result){
                resultDTO.setMessage("库区代码重复");
                resultDTO.setSuccess(false);
                resultDTO.setMessageCode("500");
            }else{
                resultDTO.setMessage("新增库区成功!  ");
                resultDTO.setSuccess(true);
                resultDTO.setMessageCode("200");
            }
            return resultDTO;
        } catch (BeansException ex) {
            LOGGER.error("WmsZoneController.addZone error: {}", ex);
            resultDTO.setMessage("新增库区失败！");
            resultDTO.setSuccess(false);
            resultDTO.setMessageCode("500");
            return resultDTO;
        }
    }

    /**
     * 获取库区信息
     *
     * @param zeId
     * @return
     */
    @RequestMapping(value="/getZoneById/{zeId}")
    public ResultDTO getZoneById(@PathVariable String zeId){
        ResultDTO resultDTO = new ResultDTO();
        if(StringUtil.isEmpty(zeId)){
            LOGGER.error("WmsZoneController.getZoneById 请求参数为空");
        }
        try{
            WmsZoneBO wmsZoneBO = wmsZoneService.getZoneById(zeId);
            resultDTO.setData(wmsZoneBO);
            resultDTO.setMessage("库区数据成功!  ");
            resultDTO.setSuccess(true);
            resultDTO.setMessageCode("200");
            return resultDTO;
        }catch(BusinessException ex){
            LOGGER.error("WmsZoneController.getZoneById error: {}", ex);
            resultDTO.setMessage("库区数据成功!  ");
            resultDTO.setSuccess(false);
            resultDTO.setMessageCode("500");
            return resultDTO;
        }

    }

    /**
     * 修改库区
     * @param wmsZoneVO
     * @return
     */
    @RequestMapping(value="/updateZone",method = RequestMethod.POST)
    public ResultDTO updateZone(@RequestBody WmsZoneVO wmsZoneVO){
        ResultDTO resultDTO = new ResultDTO();
        if(Objects.isNull(wmsZoneVO)){
            LOGGER.error("WmsZoneController.updateZone 请求参数为空");
        }
        try {
            WmsZoneBO wmsZoneBO = new WmsZoneBO();
            BeanUtils.copyProperties(wmsZoneVO, wmsZoneBO);
            wmsZoneService.updateZone(wmsZoneBO);
            resultDTO.setMessage("新增库区成功!  ");
            resultDTO.setSuccess(true);
            resultDTO.setMessageCode("200");
            return resultDTO;
        } catch (BeansException ex) {
            LOGGER.error("WmsZoneController.updateZone error: {}", ex);
            resultDTO.setMessage("新增库区失败！");
            resultDTO.setSuccess(false);
            resultDTO.setMessageCode("500");
            return resultDTO;
        }
    }

    /**
     * 删除库区
     * @param wmsZoneList
     * @return
     */
    @RequestMapping(value="/deleteZone",method = RequestMethod.POST)
    public ResultDTO deleteZone(@RequestBody List<WmsZoneVO> wmsZoneList){
        LOGGER.info("WmsZoneController.deleteZone wmsZoneList: {}", wmsZoneList);
        ResultDTO resultDTO = new ResultDTO();
        if(CollectionUtils.isEmpty(wmsZoneList)){
            LOGGER.error("WmsZoneController.deleteZone 请求参数为空");
        }
        try {
            Vo2Bo converter = new Vo2Bo();
            List<WmsZoneBO> wmsZoneBOList = converter.convert(wmsZoneList, WmsZoneBO.class);
            wmsZoneService.deleteZone(wmsZoneBOList);
            resultDTO.setMessage("删除库区成功!  ");
            resultDTO.setSuccess(true);
            resultDTO.setMessageCode("200");
            return resultDTO;
        } catch (BeansException ex) {
            LOGGER.error("WmsZoneController.deleteZone error: {}", ex);
            resultDTO.setMessage("删除库区失败！");
            resultDTO.setSuccess(false);
            resultDTO.setMessageCode("500");
            return resultDTO;
        }
    }

    /**
     * 删除库区
     * @param zoneIdList
     * @return
     */
    @RequestMapping(value="/deleteZoneNew",method = RequestMethod.POST)
    public ResultDTO deleteZoneNew(@RequestBody List<Long> zoneIdList){
        LOGGER.info("WmsZoneController.deleteZoneNew zoneIdList: {}", zoneIdList);
        ResultDTO resultDTO = new ResultDTO();
        if(CollectionUtils.isEmpty(zoneIdList)){
            LOGGER.error("WmsZoneController.deleteZone 请求参数为空");
        }
        try {
            wmsZoneService.deleteZoneNew(zoneIdList);
            resultDTO.setMessage("删除库区成功!  ");
            resultDTO.setSuccess(true);
            resultDTO.setMessageCode("200");
            return resultDTO;
        } catch (BeansException ex) {
            LOGGER.error("WmsZoneController.deleteZone error: {}", ex);
            resultDTO.setMessage("删除库区失败！");
            resultDTO.setSuccess(false);
            resultDTO.setMessageCode("500");
            return resultDTO;
        }
    }
}
