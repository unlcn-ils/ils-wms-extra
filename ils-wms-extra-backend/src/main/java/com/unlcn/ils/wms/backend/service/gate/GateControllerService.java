package com.unlcn.ils.wms.backend.service.gate;


import cn.huiyunche.commons.domain.ResultDTOWithPagination;
import com.unlcn.ils.wms.backend.bo.gateBO.GateBO;
import com.unlcn.ils.wms.backend.bo.outboundBO.OutboundConfirmBO;
import com.unlcn.ils.wms.base.businessDTO.inbound.AsnOrderDTO;
import com.unlcn.ils.wms.base.dto.SysGateConfigDTO;
import com.unlcn.ils.wms.base.dto.WmsCqInboundTmsDTO;
import com.unlcn.ils.wms.base.model.outbound.WmsDepartureRegister;
import com.unlcn.ils.wms.base.model.sys.SysGateConfig;
import com.unlcn.ils.wms.base.model.sys.SysUser;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;
import java.util.Map;

/**
 * @author laishijian
 * @ClassName: GateControllerService
 * @Description: 门控制 service
 * @date 2017年11月9日 上午10:10:59
 */
public interface GateControllerService {
    /**
     * @param @param  gateBO
     * @param @return
     * @return Map<String,String>    返回类型
     * @throws
     * @Title: openToPlateNum
     * @Description: 入库根据车牌号开门
     */

    Map<String, Object> addInboundOpenToPlateNum(GateBO gateBO) ;

    /**
     * @param @param  gateBO
     * @param @return
     * @param @throws Exception
     * @return String    返回类型
     * @throws
     * @Title: outboundOpenToPlateNum
     * @Description: 出库库根据车牌号开门
     */
    Map<String, Object> updateOutboundOpenToPlateNum(GateBO gateBO);

    ResultDTOWithPagination<List<WmsDepartureRegister>> getFailedOpenList(WmsCqInboundTmsDTO dto, String userId, String whCode) throws Exception;

    void addOpenByHm(WmsCqInboundTmsDTO dto, String userId, String whCode) throws Exception;

    void gateRecordsImpotExcel(WmsCqInboundTmsDTO dto, HttpServletRequest request, HttpServletResponse response) throws Exception;

    void updateJmGateIn(AsnOrderDTO result) throws Exception;

    void updateJmGateOut(SysUser sysUser, OutboundConfirmBO bo) throws Exception;

    List<SysGateConfig> getJmGateUrl(String whCode) throws Exception;

    void updateJmGateUrl(SysGateConfigDTO dto) throws Exception;
}
