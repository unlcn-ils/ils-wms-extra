package com.unlcn.ils.wms.backend.enums;

/**
 * Created by DELL on 2017/8/15.
 */
public enum WmsStatusEnum {
    WMS_INBOUND_CREATE(10,"未收货"),
    WMS_INBOUND_WAIT_ALLOCATION(20,"已收货"),
    WMS_INBOUND_FINISH_ALLOCATION(30,"已分配"),
    WMS_INBOUND_WAIT_INBOUND(40,"待入库"),
    WMS_INBOUND_FINISH(50,"已入库");

    private final int value;
    private final String text;
    WmsStatusEnum(int value, String text){
        this.value = value;
        this.text = text;
    }

    public int getValue() {
        return value;
    }

    public String getText() {
        return text;
    }

    public static WmsStatusEnum getByValue(int value){
        for (WmsStatusEnum temp : WmsStatusEnum.values()) {
            if(temp.getValue() == value){
                return temp;
            }
        }
        return null;
    }
}
