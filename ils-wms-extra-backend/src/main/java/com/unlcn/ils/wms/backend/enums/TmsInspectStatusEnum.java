package com.unlcn.ils.wms.backend.enums;

/**
 * @Author ：Ligl
 * @Date : 2017/9/13.
 */
public enum TmsInspectStatusEnum {


    WAYBILL_INIT(10, "待验车"),
    WAYBILL_QUALIFY(20, "合格"),
    WAYBILL_EXCP(30, "异常"),
    WAYBILL_DAMAGE(40, "带伤发运"),
    WAYBILL_REPAIR(50, "返修合格"),
    WAYBILL_COMPROMISE(60, "让步合格"),
    WAYBILL_CONFIRM_EXCP(70, "验车完成"),
    WAYBILL_REPAIRING(80, "维修中"),
    REPAIR_FISHIED(90, "维修完成");


    private final int value;
    private final String text;

    TmsInspectStatusEnum(int value, String text) {
        this.value = value;
        this.text = text;
    }

    public int getValue() {
        return value;
    }

    public String getText() {
        return text;
    }

    public static TmsInspectStatusEnum getByValue(int value) {
        for (TmsInspectStatusEnum temp : TmsInspectStatusEnum.values()) {
            if (temp.getValue() == value) {
                return temp;
            }
        }
        return null;
    }
}
