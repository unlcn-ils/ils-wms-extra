package com.unlcn.ils.wms.web.vo.inbound;

/**
 * Created by DELL on 2017/9/8.
 */
public class AreaTreeVO {
    /**
     * 区域ID
     */
    private Long id;
    /**
     * 区域code
     */
    private String code;
    /**
     * 区域名称
     */
    private String name;

    /**
     *
     */
    private String sname;

    /**
     *
     */
    private Long parentId;

    /**
     *
     */
    private String level;

    /**
     *
     */
    private String status;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSname() {
        return sname;
    }

    public void setSname(String sname) {
        this.sname = sname;
    }

    public Long getParentId() {
        return parentId;
    }

    public void setParentId(Long parentId) {
        this.parentId = parentId;
    }

    public String getLevel() {
        return level;
    }

    public void setLevel(String level) {
        this.level = level;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    @Override
    public String toString() {
        return "AreaTreeVO{" +
                "id=" + id +
                ", code='" + code + '\'' +
                ", name='" + name + '\'' +
                ", sname='" + sname + '\'' +
                ", parentId=" + parentId +
                ", level='" + level + '\'' +
                ", status='" + status + '\'' +
                '}';
    }
}
