package com.unlcn.ils.wms.base.mapper.extmapper;

import com.unlcn.ils.wms.base.businessDTO.outbound.WmsShipmentPlanVehicleDTO;
import com.unlcn.ils.wms.base.model.stock.WmsShipmentPlan;
import org.apache.ibatis.annotations.Param;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public interface WmsShipmentPlanExtMapper {
	/**
	 * 发运计划分页查询 - 西南
	 * @param paramMap
	 * @return
	 */
	List<WmsShipmentPlan> getListByGroup(Map<String, Object> paramMap);

	/**
	 * 发运计划分页查询 - 君马
	 * @param paramMap
	 * @return
	 */
	List<WmsShipmentPlan> getListForNew(Map<String, Object> paramMap);

	/**
	 * 数据条数 - 西南
	 * @param paramMap
	 * @return
	 */
	Integer countListByGroup(Map<String, Object> paramMap);

	/**
	 * 发运计划分页查询 - 君马
	 * @param paramMap
	 * @return
	 */
	Integer countListForNew(Map<String, Object> paramMap);

	List<WmsShipmentPlanVehicleDTO> getShipmentPLanVehicleList(Map<String, Object> paramMap);

	/**
	 * 根据车架号查询发运信息
	 * @param spVin
	 * @return
	 */
	List<WmsShipmentPlan> selectByVin(@Param("spVin") String spVin);

	/**
	 * 查询发运计划
	 * linbao 2017-11-16
	 * @param paramMap
	 * @return
	 */
	List<WmsShipmentPlan> getShipmentList(Map<String, Object> paramMap);

	/**
	 * 获取非删除, 未处理的发运计划数据
	 *
	 * @param paramMap
	 * @return
	 */
	Integer countShipmentForUpdtePlan(Map<String, Object> paramMap);

    List<WmsShipmentPlan> selectGateInDataByVehicle(HashMap<String, Object> paramMap);

	List<WmsShipmentPlan> selectGateOutDataByVehicle(HashMap<String, Object> paramMap);

    void insertShipmentBatch(ArrayList<WmsShipmentPlan> insertShips);
}
