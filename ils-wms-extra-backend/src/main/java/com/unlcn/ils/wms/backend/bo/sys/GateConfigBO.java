package com.unlcn.ils.wms.backend.bo.sys;

public class GateConfigBO {
    /**
     * id
     */
    private Integer id;
    /**
     * 仓库 code
     */
    private String whCode;
    /**
     * 通道号
     */
    private String channel;
    /**
     * 出入库类型 10-入库 20-出库
     */
    private String type;

    private String gateType;

    public String getGateType() {
        return gateType;
    }

    public void setGateType(String gateType) {
        this.gateType = gateType;
    }

    /**
     * @return the id
     */
    public Integer getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * @return the whCode
     */
    public String getWhCode() {
        return whCode;
    }

    /**
     * @param whCode the whCode to set
     */
    public void setWhCode(String whCode) {
        this.whCode = whCode;
    }

    /**
     * @return the channel
     */
    public String getChannel() {
        return channel;
    }

    /**
     * @param channel the channel to set
     */
    public void setChannel(String channel) {
        this.channel = channel;
    }

    /**
     * @return the type
     */
    public String getType() {
        return type;
    }

    /**
     * @param type the type to set
     */
    public void setType(String type) {
        this.type = type;
    }

    @Override
    public String toString() {
        return "GateConfigBO{" +
                "id=" + id +
                ", whCode='" + whCode + '\'' +
                ", channel='" + channel + '\'' +
                ", type='" + type + '\'' +
                '}';
    }
}
