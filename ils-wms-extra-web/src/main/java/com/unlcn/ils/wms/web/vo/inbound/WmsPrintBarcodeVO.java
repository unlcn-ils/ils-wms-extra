package com.unlcn.ils.wms.web.vo.inbound;

/**
 * 打印二维码
 * Created by DELL on 2017/8/19.
 */
public class WmsPrintBarcodeVO {

    /**
     * 二维码
     */
    private String barcode;

    /**
     * 底盘号
     */
    private String vin;

    /**
     * 库位
     */
    private String locNo;

    /**
     * 目的地
     */
    private String destination;

    /**
     * 运单号
     * @return
     */
    private String asnOrderBillNo;

    public String getBarcode() {
        return barcode;
    }

    public void setBarcode(String barcode) {
        this.barcode = barcode;
    }

    public String getVin() {
        return vin;
    }

    public void setVin(String vin) {
        this.vin = vin;
    }

    public String getLocNo() {
        return locNo;
    }

    public void setLocNo(String locNo) {
        this.locNo = locNo;
    }

    public String getDestination() {
        return destination;
    }

    public void setDestination(String destination) {
        this.destination = destination;
    }

    public String getAsnOrderBillNo() {
        return asnOrderBillNo;
    }

    public void setAsnOrderBillNo(String asnOrderBillNo) {
        this.asnOrderBillNo = asnOrderBillNo;
    }

    @Override
    public String toString() {
        return "WmsPrintBarcodeVO{" +
                "barcode='" + barcode + '\'' +
                ", vin='" + vin + '\'' +
                ", locNo='" + locNo + '\'' +
                ", destination='" + destination + '\'' +
                ", asnOrderBillNo='" + asnOrderBillNo + '\'' +
                '}';
    }
}
