package com.unlcn.ils.wms.api.webservice.server;

import cn.huiyunche.commons.exception.BusinessException;
import com.google.common.collect.Maps;
import com.google.gson.Gson;
import com.unlcn.ils.wms.api.webservice.dto.ResultInboundAndOutBoundSapDTO;
import com.unlcn.ils.wms.backend.service.webservice.bo.WmsResultInorOutFromSapBO;
import com.unlcn.ils.wms.backend.service.webservice.client.WmsJmSapService;
import com.unlcn.ils.wms.backend.service.webservice.dto.ResultSapHeaderDTO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.context.support.SpringBeanAutowiringSupport;

import java.util.Map;
import java.util.Objects;

/**
 * 出入库sap接口回调
 */
public class WmsSapInorOutBusinessServiceImpl {

    private Logger logger = LoggerFactory.getLogger(WmsSapInorOutBusinessServiceImpl.class);

    @Autowired
    private WmsJmSapService wmsJmSapService;


    /**
     * <p>
     * 出入库对接君马sap系统的异步返回消息服务端
     * </p>
     *
     * @param inboundAndOutBoundSapDTO 参数封装
     * @return 返回值
     */
    public String getInboundOutboundMessage(ResultInboundAndOutBoundSapDTO[] inboundAndOutBoundSapDTO,
                                            ResultSapHeaderDTO[] resultSapHeaderDTOS) throws Exception {
        logger.info("WmsSapBusinessServiceImpl.getInboundOutboundMessage param{},{}", inboundAndOutBoundSapDTO, resultSapHeaderDTOS);
        //加入spring的上下文
        SpringBeanAutowiringSupport.processInjectionBasedOnCurrentContext(this);
        Map<String, Object> resultInfo = Maps.newHashMap();
        if (Objects.equals(inboundAndOutBoundSapDTO, null)) {
            resultInfo.put("returnCode", "E");
            resultInfo.put("returnMsg", "传入数据为空!");
            resultInfo.put("dataId", null);
            return new Gson().toJson(resultInfo);
        }
        String s_body = new Gson().toJson(inboundAndOutBoundSapDTO);
        String s_head = new Gson().toJson(resultSapHeaderDTOS);
        wmsJmSapService.saveResultLog(s_body, s_head);
        for (ResultInboundAndOutBoundSapDTO boundSapDTO : inboundAndOutBoundSapDTO) {
            WmsResultInorOutFromSapBO wmsResultInorOutFromSapBO = new WmsResultInorOutFromSapBO();
            BeanUtils.copyProperties(boundSapDTO, wmsResultInorOutFromSapBO);
            try {
                resultInfo = wmsJmSapService.updateOutofStorageResult(wmsResultInorOutFromSapBO, s_body, s_head);
            } catch (BusinessException e) {
                logger.error("WmsSapBusinessServiceImpl.getInboundOutboundMessage param{}", e);
                resultInfo.put("returnCode", "E");
                resultInfo.put("returnMsg", e.getMessage());
                resultInfo.put("dataId", wmsResultInorOutFromSapBO.getDataId());
                return new Gson().toJson(resultInfo);
            } catch (Exception e) {
                logger.error("WmsSapBusinessServiceImpl.getInboundOutboundMessage param{}", e);
                resultInfo.put("returnCode", "E");
                resultInfo.put("returnMsg", "系统内部异常!");
                resultInfo.put("dataId", wmsResultInorOutFromSapBO.getDataId());
                return new Gson().toJson(resultInfo);
            }
        }

        return new Gson().toJson(resultInfo);
    }
}
