package com.unlcn.ils.wms.backend.enums;

public enum WmsOutboundInspectStatusEnum {
    WAYBILL_INIT(10, "待验车"),
    WAYBILL_QUALIFY(20, "正常发运"),
    WAYBILL_EXCP(30, "异常"),
    WAYBILL_DAMAGE(40, "带伤发运");

    private final int value;
    private final String text;

    WmsOutboundInspectStatusEnum(int value, String text) {
        this.value = value;
        this.text = text;
    }

    public int getValue() {
        return value;
    }

    public String getText() {
        return text;
    }

    public static WmsOutboundInspectStatusEnum getByValue(int value){
        for (WmsOutboundInspectStatusEnum temp : WmsOutboundInspectStatusEnum.values()) {
            if(temp.getValue() == value){
                return temp;
            }
        }
        return null;
    }
}
