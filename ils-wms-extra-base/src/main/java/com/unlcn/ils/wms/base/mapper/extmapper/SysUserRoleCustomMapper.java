package com.unlcn.ils.wms.base.mapper.extmapper;

import com.unlcn.ils.wms.base.dto.UserWarehouseInfoDTO;
import com.unlcn.ils.wms.base.model.sys.SysUserRole;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface SysUserRoleCustomMapper {
	
	/**
	 * 
	 * @Title: insertBatch 
	 * @Description: 批量新增
	 * @param @param list
	 * @param @return     
	 * @return Integer    返回类型 
	 * @throws 
	 *
	 */
    Integer insertBatch(List<SysUserRole> list);

	/**
	 * 用户所属仓库信息
	 * @param userId
	 * @return
	 */
    List<UserWarehouseInfoDTO> userWarehouseInfo(@Param("userId") Long userId);
}