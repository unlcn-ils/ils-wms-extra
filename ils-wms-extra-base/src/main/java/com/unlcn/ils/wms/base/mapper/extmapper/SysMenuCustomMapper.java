package com.unlcn.ils.wms.base.mapper.extmapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.unlcn.ils.wms.base.model.sys.SysMenu;

public interface SysMenuCustomMapper {
	/**
	 * 
	 * @Title: selectByParentId 
	 * @Description: 根据父id查询
	 * @param @param parentId
	 * @param @return     
	 * @return List<SysMenu>    返回类型 
	 * @throws 
	 *
	 */
	public List<SysMenu> selectByParentId(@Param("parentId") Integer parentId);
	
}