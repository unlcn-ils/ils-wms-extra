package com.unlcn.ils.wms.base.mapper.extmapper;

import org.apache.ibatis.annotations.Param;

public interface WmsOutboundInspectExtMapper {
    int selectExcpCountByInspectId(@Param("inspectId") Long inspectId, @Param("excpValue") int excpValue,@Param("del") int del_flag);

}
