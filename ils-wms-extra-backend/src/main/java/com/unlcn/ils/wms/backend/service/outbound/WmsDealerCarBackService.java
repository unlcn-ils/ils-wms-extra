package com.unlcn.ils.wms.backend.service.outbound;

import com.unlcn.ils.wms.backend.service.webservice.dto.ResultForDcsDealerCarbackDetailsDTO;
import com.unlcn.ils.wms.backend.service.webservice.vo.WmsDealerCarBackVO;
import com.unlcn.ils.wms.base.model.outbound.WmsDealerCarBack;

import java.util.ArrayList;

/**
 * Created by DELL on 2017/9/27.
 */
public interface WmsDealerCarBackService {

    ArrayList<ResultForDcsDealerCarbackDetailsDTO> saveWmsDealerCarBack(WmsDealerCarBackVO[] wmsDealerCarBack, String s_body, String s_head) throws Exception;

    /**
     * 根据车架号获取退车信息
     *
     * @param vin
     * @return
     * @throws Exception
     */
    WmsDealerCarBack getWmsDealerCarBackByVin(String vin) throws Exception;

    void updateDelearCarBack() throws Exception;

    void updateDelearCarBackExcp() throws Exception;
}
