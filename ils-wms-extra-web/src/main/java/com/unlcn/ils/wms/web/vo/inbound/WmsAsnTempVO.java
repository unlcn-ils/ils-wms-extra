package com.unlcn.ils.wms.web.vo.inbound;

/**
 * ASN
 * Created by DELL on 2017/9/28.
 */
public class WmsAsnTempVO {
    /**
     * 主键
     */
    private Long atId;

    /**
     * 调度单号
     */
    private String atDispatchNo;

    /**
     * 客户订单号
     */
    private String atCustomerOrderNo;

    /**
     * 起运地
     */
    private String atOrigin;

    /**
     * 目的地
     */
    private String atDest;

    /**
     * 入库仓库
     */
    private String atWarehouseName;

    /**
     * 货主
     */
    private String atCustomerName;

    /**
     * 分供方
     */
    private String atSupplierName;

    /**
     * 板车车牌号
     */
    private String atSupplierVehiclePlate;

    /**
     * 车架号(VIN)
     */
    private String atVin;

    /**
     * 车型代码
     */
    private String atVehicleCode;

    /**
     * 车型名称
     */
    private String atVehicleName;

    /**
     * 物料代码
     */
    private String atMaterialCode;

    /**
     * 物料名称
     */
    private String atMaterialName;

    /**
     * 颜色代码
     */
    private String atCarColourCode;

    /**
     * 颜色名称
     */
    private String atCarColour;

    /**
     * 生产日期
     */
    private String atProductionDate;

    /**
     * 发动机号
     */
    private String atEngineNumber;

    /**
     * 合格证
     */
    private String atCertification;

    /**
     * 下线日期
     */
    private String atOfflineDate;

    /**
     * 变速箱号
     */
    private String atGearboxNumber;

    /**
     * 系统来源
     */
    private String atSysSource;

    /**
     * 是否已同步到业务表 0-否 1-是
     */
    private String atSendBusinessFlag;

    /**
     * 创建开始时间
     */
    private String startCreateTime;

    /**
     * 创建结束时间
     */
    private String endCreateTime;

    public Long getAtId() {
        return atId;
    }

    public void setAtId(Long atId) {
        this.atId = atId;
    }

    public String getAtDispatchNo() {
        return atDispatchNo;
    }

    public void setAtDispatchNo(String atDispatchNo) {
        this.atDispatchNo = atDispatchNo;
    }

    public String getAtCustomerOrderNo() {
        return atCustomerOrderNo;
    }

    public void setAtCustomerOrderNo(String atCustomerOrderNo) {
        this.atCustomerOrderNo = atCustomerOrderNo;
    }

    public String getAtOrigin() {
        return atOrigin;
    }

    public void setAtOrigin(String atOrigin) {
        this.atOrigin = atOrigin;
    }

    public String getAtDest() {
        return atDest;
    }

    public void setAtDest(String atDest) {
        this.atDest = atDest;
    }

    public String getAtWarehouseName() {
        return atWarehouseName;
    }

    public void setAtWarehouseName(String atWarehouseName) {
        this.atWarehouseName = atWarehouseName;
    }

    public String getAtCustomerName() {
        return atCustomerName;
    }

    public void setAtCustomerName(String atCustomerName) {
        this.atCustomerName = atCustomerName;
    }

    public String getAtSupplierName() {
        return atSupplierName;
    }

    public void setAtSupplierName(String atSupplierName) {
        this.atSupplierName = atSupplierName;
    }

    public String getAtSupplierVehiclePlate() {
        return atSupplierVehiclePlate;
    }

    public void setAtSupplierVehiclePlate(String atSupplierVehiclePlate) {
        this.atSupplierVehiclePlate = atSupplierVehiclePlate;
    }

    public String getAtVin() {
        return atVin;
    }

    public void setAtVin(String atVin) {
        this.atVin = atVin;
    }

    public String getAtVehicleCode() {
        return atVehicleCode;
    }

    public void setAtVehicleCode(String atVehicleCode) {
        this.atVehicleCode = atVehicleCode;
    }

    public String getAtVehicleName() {
        return atVehicleName;
    }

    public void setAtVehicleName(String atVehicleName) {
        this.atVehicleName = atVehicleName;
    }

    public String getAtMaterialCode() {
        return atMaterialCode;
    }

    public void setAtMaterialCode(String atMaterialCode) {
        this.atMaterialCode = atMaterialCode;
    }

    public String getAtMaterialName() {
        return atMaterialName;
    }

    public void setAtMaterialName(String atMaterialName) {
        this.atMaterialName = atMaterialName;
    }

    public String getAtCarColourCode() {
        return atCarColourCode;
    }

    public void setAtCarColourCode(String atCarColourCode) {
        this.atCarColourCode = atCarColourCode;
    }

    public String getAtCarColour() {
        return atCarColour;
    }

    public void setAtCarColour(String atCarColour) {
        this.atCarColour = atCarColour;
    }

    public String getAtProductionDate() {
        return atProductionDate;
    }

    public void setAtProductionDate(String atProductionDate) {
        this.atProductionDate = atProductionDate;
    }

    public String getAtEngineNumber() {
        return atEngineNumber;
    }

    public void setAtEngineNumber(String atEngineNumber) {
        this.atEngineNumber = atEngineNumber;
    }

    public String getAtCertification() {
        return atCertification;
    }

    public void setAtCertification(String atCertification) {
        this.atCertification = atCertification;
    }

    public String getAtOfflineDate() {
        return atOfflineDate;
    }

    public void setAtOfflineDate(String atOfflineDate) {
        this.atOfflineDate = atOfflineDate;
    }

    public String getAtGearboxNumber() {
        return atGearboxNumber;
    }

    public void setAtGearboxNumber(String atGearboxNumber) {
        this.atGearboxNumber = atGearboxNumber;
    }

    public String getAtSysSource() {
        return atSysSource;
    }

    public void setAtSysSource(String atSysSource) {
        this.atSysSource = atSysSource;
    }

    public String getAtSendBusinessFlag() {
        return atSendBusinessFlag;
    }

    public void setAtSendBusinessFlag(String atSendBusinessFlag) {
        this.atSendBusinessFlag = atSendBusinessFlag;
    }

    public String getStartCreateTime() {
        return startCreateTime;
    }

    public void setStartCreateTime(String startCreateTime) {
        this.startCreateTime = startCreateTime;
    }

    public String getEndCreateTime() {
        return endCreateTime;
    }

    public void setEndCreateTime(String endCreateTime) {
        this.endCreateTime = endCreateTime;
    }

    @Override
    public String toString() {
        return "WmsAsnTempVO{" +
                "atId=" + atId +
                ", atDispatchNo='" + atDispatchNo + '\'' +
                ", atCustomerOrderNo='" + atCustomerOrderNo + '\'' +
                ", atOrigin='" + atOrigin + '\'' +
                ", atDest='" + atDest + '\'' +
                ", atWarehouseName='" + atWarehouseName + '\'' +
                ", atCustomerName='" + atCustomerName + '\'' +
                ", atSupplierName='" + atSupplierName + '\'' +
                ", atSupplierVehiclePlate='" + atSupplierVehiclePlate + '\'' +
                ", atVin='" + atVin + '\'' +
                ", atVehicleCode='" + atVehicleCode + '\'' +
                ", atVehicleName='" + atVehicleName + '\'' +
                ", atMaterialCode='" + atMaterialCode + '\'' +
                ", atMaterialName='" + atMaterialName + '\'' +
                ", atCarColourCode='" + atCarColourCode + '\'' +
                ", atCarColour='" + atCarColour + '\'' +
                ", atProductionDate=" + atProductionDate +
                ", atEngineNumber='" + atEngineNumber + '\'' +
                ", atCertification='" + atCertification + '\'' +
                ", atOfflineDate=" + atOfflineDate +
                ", atGearboxNumber='" + atGearboxNumber + '\'' +
                ", atSysSource='" + atSysSource + '\'' +
                ", atSendBusinessFlag='" + atSendBusinessFlag + '\'' +
                '}';
    }
}
