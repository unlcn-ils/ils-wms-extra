package com.unlcn.ils.wms.backend.service.outbound;

import com.unlcn.ils.wms.base.dto.WmsCqInboundTmsDTO;
import com.unlcn.ils.wms.base.model.outbound.WmsDepartureRegister;
import com.unlcn.ils.wms.base.model.sys.SysUser;

import java.util.Map;

public interface WmsCQOutboundService {


    //调用tms接口查询入库指令,对接道闸接口及业务--自动调用tms
    Map<String, Object> saveOutboundDetailByVehicleFromTms(String warehouse, String vehicle, String shipno);

    void addOutboundOpenByHm(WmsCqInboundTmsDTO dto, SysUser sysUser, String whCode, WmsDepartureRegister departureRegister) throws Exception;

    void updateSendOutboundDataToTMS();

    void sendOutboundDataExcpToTMSForJM();

}
