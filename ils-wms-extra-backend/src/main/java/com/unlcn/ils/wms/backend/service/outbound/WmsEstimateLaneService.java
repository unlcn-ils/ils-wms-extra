package com.unlcn.ils.wms.backend.service.outbound;

import com.unlcn.ils.wms.backend.bo.outboundBO.WmsEstimateLaneBO;

import java.util.List;

/**
 * 装车道
 * Created by DELL on 2017/10/13.
 */
public interface WmsEstimateLaneService {
    /**
     * 装车道查询
     * @return
     */
    List<WmsEstimateLaneBO> getEstimateLaneByWhCode(String whName);

    /**
     * 新增装车道
     * @param wmsEstimateLaneBO
     */
    void addEstimateLane(WmsEstimateLaneBO wmsEstimateLaneBO);

    /**
     * 删除装车道
     * @param wmsEstimateLaneBOList
     */
    void deleteEstimateLane(List<WmsEstimateLaneBO> wmsEstimateLaneBOList);

}
