package com.unlcn.ils.wms.backend.service.inbound.impl;

import com.google.common.collect.Maps;
import com.unlcn.ils.wms.backend.bo.inboundBO.*;
import com.unlcn.ils.wms.backend.service.inbound.WmsInboundPloyService;
import com.unlcn.ils.wms.base.mapper.additional.wmsInboundPloy.*;
import com.unlcn.ils.wms.base.mapper.inbound.*;
import com.unlcn.ils.wms.base.model.inbound.*;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

/**
 * Created by DELL on 2017/9/1.
 */
@Service
public class WmsInboundPloyServiceImpl implements WmsInboundPloyService {
    @Autowired
    private WmsInboundPloyMapper wmsInboundPloyMapper;

    @Autowired
    private WmsInboundPloyConditionMapper wmsInboundPloyConditionMapper;

    @Autowired
    private WmsInboundPloyOperatorMapper wmsInboundPloyOperatorMapper;

    @Autowired
    private WmsInboundPloyFlowtoMapper wmsInboundPloyFlowtoMapper;

    @Autowired
    private WmsInboundPloyLocationMapper wmsInboundPloyLocationMapper;

    @Autowired
    private WmsInboundPloyGenconditionMapper wmsInboundPloyGenConditionMapper;

    @Autowired
    private WmsInboundPloyGenresultMapper wmsInboundPloyGenResultMapper;

    @Autowired
    private WmsInboundPloyExtMapper wmsInboundPloyExtMapper;

    @Autowired
    private WmsInboundPloyConditionExtMapper wmsInboundPloyConditionExtMapper;

    @Autowired
    private WmsInboundPloyOperatorExtMapper wmsInboundPloyOperatorExtMapper;

    @Autowired
    private WmsInboundPloyFlowtoExtMapper wmsInboundPloyFlowtoExtMapper;

    @Autowired
    private WmsInboundPloyLocationExtMapper wmsInboundPloyLocationExtMapper;

    @Autowired
    private WmsInboundPloyGenConditionExtMapper wmsInboundPloyGenConditionExtMapper;

    @Autowired
    private WmsInboundPloyGenResultExtMapper wmsInboundPloyGenResultExtMapper;



    @Override
    public List<WmsInboundPloy> getPloyList(String whCode) {
        return wmsInboundPloyExtMapper.getPloyList(whCode);
    }

    @Override
    public void addPloy(WmsInboundPloyBO wmsInboundPloyBO) {
        WmsInboundPloy wmsInboundPloy = new WmsInboundPloy();
        BeanUtils.copyProperties(wmsInboundPloyBO,wmsInboundPloy);
        wmsInboundPloy.setIsDeleted((byte)1);
        wmsInboundPloyMapper.insert(wmsInboundPloy);
    }

    @Override
    public void updatePloy(WmsInboundPloyBO wmsInboundPloyBO) {
        WmsInboundPloy wmsInboundPloy= new WmsInboundPloy();
        BeanUtils.copyProperties(wmsInboundPloyBO,wmsInboundPloy);
        wmsInboundPloyMapper.updateByPrimaryKeySelective(wmsInboundPloy);
    }

    @Override
    public void deletePloy(List<WmsInboundPloyBO> wmsInboundPloyBOList) {
        wmsInboundPloyBOList.stream().forEach(v -> {
            wmsInboundPloyMapper.deleteByPrimaryKey(v.getPyId());
        });
    }

    @Override
    public List<WmsInboundPloyCondition> getPloyConditionList(String ployCode) {
        return wmsInboundPloyConditionExtMapper.getPloyConditionList(ployCode);
    }

    @Override
    public void addPloyCondition(List<WmsInboundPloyConditionBO> wmsInboundPloyConditionBO){
        wmsInboundPloyConditionBO.stream().forEach(v -> {
            WmsInboundPloyCondition wmsInboundPloyCondition = new WmsInboundPloyCondition();
            BeanUtils.copyProperties(v, wmsInboundPloyCondition);
            wmsInboundPloyCondition.setIsDeleted((byte)1);
            wmsInboundPloyConditionMapper.insert(wmsInboundPloyCondition);
        });
    }

    @Override
    public void deletePloyCondition(List<WmsInboundPloyConditionBO> wmsInboundPloyConditionBO) {
        wmsInboundPloyConditionBO.stream().forEach(v -> {
            wmsInboundPloyConditionMapper.deleteByPrimaryKey(v.getPycId());
        });
    }

    @Override
    public List<WmsInboundPloyFlowto> getPloyFlowtoList(String ployCode) {
        return wmsInboundPloyFlowtoExtMapper.getPloyFlowtoList(ployCode);
    }

    @Override
    public void addPloyFlowto(List<WmsInboundPloyFlowtoBO> wmsInboundPloyFlowtoBOList) {
        //新增前先删除
        /*WmsInboundPloyFlowtoExample deleteParam = new WmsInboundPloyFlowtoExample();
        deleteParam.createCriteria().andPyfPyCodeEqualTo(wmsInboundPloyFlowtoBO.get(0).getPyfPyCode());
        wmsInboundPloyFlowtoMapper.deleteByExample(deleteParam);*/
        /**
         * 判断重复的数据并且删除重复数据
         */
        List<WmsInboundPloyFlowto> wmsInboundPloyFlowtos = wmsInboundPloyFlowtoExtMapper.getPloyFlowtoList(wmsInboundPloyFlowtoBOList.get(0).getPyfPyCode());
        Map<String,String> havaMap = Maps.newHashMap();
        for(WmsInboundPloyFlowto wmsInboundPloyFlowto : wmsInboundPloyFlowtos){
            havaMap.put(wmsInboundPloyFlowto.getPyfId().toString(),wmsInboundPloyFlowto.getPyfFromCode()+wmsInboundPloyFlowto.getPyfToCode());
        }
        for(WmsInboundPloyFlowtoBO wmsInboundPloyFlowtoBO : wmsInboundPloyFlowtoBOList){
            String str = wmsInboundPloyFlowtoBO.getPyfFromCode()+wmsInboundPloyFlowtoBO.getPyfToCode();
            if(havaMap.containsValue(str)){
                wmsInboundPloyFlowtoBOList.remove(wmsInboundPloyFlowtoBO);
            }
        }
        /**
         * 保存没有重复数据
         */
        wmsInboundPloyFlowtoBOList.stream().forEach(v -> {
            WmsInboundPloyFlowto wmsInboundPloyFlowto = new WmsInboundPloyFlowto();
            BeanUtils.copyProperties(v, wmsInboundPloyFlowto);
            wmsInboundPloyFlowto.setIsDeleted((byte)1);
            wmsInboundPloyFlowtoMapper.insert(wmsInboundPloyFlowto);
        });
    }

    @Override
    public void deletePloyFlowto(List<WmsInboundPloyFlowtoBO> wmsInboundPloyFlowtoBOList) {
        wmsInboundPloyFlowtoBOList.stream().forEach(v -> {
            wmsInboundPloyFlowtoMapper.deleteByPrimaryKey(v.getPyfId());
        });
    }

    @Override
    public List<WmsInboundPloyLocation> getPloyLocationList(String ployCode) {
        return wmsInboundPloyLocationExtMapper.getPloyLocationList(ployCode);
    }

    @Override
    public void addPloyLocation(List<WmsInboundPloyLocationBO> wmsInboundPloyLocationBO) {
        //新增前先删除
        /*WmsInboundPloyLocationExample deleteParam = new WmsInboundPloyLocationExample();
        deleteParam.createCriteria().andPylPyCodeEqualTo(wmsInboundPloyLocationBO.get(0).getPylPyCode());
        wmsInboundPloyLocationMapper.deleteByExample(deleteParam);*/

        wmsInboundPloyLocationBO.stream().forEach(v -> {
            WmsInboundPloyLocation wmsInboundPloyLocation = new WmsInboundPloyLocation();
            BeanUtils.copyProperties(v, wmsInboundPloyLocation);
            wmsInboundPloyLocation.setIsDeleted((byte)1);
            wmsInboundPloyLocationMapper.insert(wmsInboundPloyLocation);
        });
    }

    @Override
    public void deletePloyLocation(List<WmsInboundPloyLocationBO> wmsInboundPloyLocationBOList) {
        wmsInboundPloyLocationBOList.stream().forEach(v ->{
            wmsInboundPloyLocationMapper.deleteByPrimaryKey(v.getPylId());
        });
    }
}
