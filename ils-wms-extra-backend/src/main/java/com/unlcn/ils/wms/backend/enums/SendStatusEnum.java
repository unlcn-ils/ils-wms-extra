package com.unlcn.ils.wms.backend.enums;

public enum SendStatusEnum {
    SEND_INIT("0", "未处理"),
    SEND_SUCCESS("1", "发送成功"),
    SEND_FAILED("2", "发送失败"),
    FINAL_SUCCESS("10", "最终成功"),
    FINAL_FAIL("20", "最终失败"),
    NOT_FINAL("30", "未确定最终状态"),
    NOT_SEND("40", "未发送");

    private final String value;
    private final String text;

    SendStatusEnum(String value, String text) {
        this.value = value;
        this.text = text;
    }

    public String getValue() {
        return value;
    }

    public String getText() {
        return text;
    }

    public static SendStatusEnum getByValue(String value) {
        for (SendStatusEnum temp : SendStatusEnum.values()) {
            if (temp.getValue().equals(value)) {
                return temp;
            }
        }
        return null;
    }
}
