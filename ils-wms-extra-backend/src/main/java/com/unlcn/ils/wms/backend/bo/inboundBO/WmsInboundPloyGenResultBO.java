package com.unlcn.ils.wms.backend.bo.inboundBO;

import com.unlcn.ils.wms.backend.bo.baseDataBO.WmsCommonBO;

/**
 * Created by DELL on 2017/9/1.
 */
public class WmsInboundPloyGenResultBO extends WmsCommonBO{
    /**
     * 生成结果主id
     */
    private Long pygrrId;

    /**
     * 策略主id
     */
    private String pygrrPyId;

    /**
     * 策略主code
     */
    private String pygrrPyCode;

    /**
     * 策略条件id
     */
    private String pygrrPycId;

    /**
     * 生成结果
     */
    private String pygrrResult;
}
