package com.unlcn.ils.wms.base.dto;

import java.io.Serializable;

public class WmsRepairInspectDTO implements Serializable {
    private String rpId;
    private String whCode;
    private String userId;
    //通过的接口调用地址10:入库维修再验车合格 20:出库维修再验车合格；
    private String passType;
    //维修再验车不合格选择的验车地点
    private String excpInpsectPlace;
    private String vin;
    private String positionId;
    private String whName;

    public String getWhName() {
        return whName;
    }

    public void setWhName(String whName) {
        this.whName = whName;
    }

    public String getVin() {
        return vin;
    }

    public void setVin(String vin) {
        this.vin = vin;
    }

    public String getPositionId() {
        return positionId;
    }

    public void setPositionId(String positionId) {
        this.positionId = positionId;
    }

    public String getExcpInpsectPlace() {
        return excpInpsectPlace;
    }

    public void setExcpInpsectPlace(String excpInpsectPlace) {
        this.excpInpsectPlace = excpInpsectPlace;
    }

    public String getPassType() {
        return passType;
    }

    public void setPassType(String passType) {
        this.passType = passType;
    }

    public String getRpId() {
        return rpId;
    }

    public void setRpId(String rpId) {
        this.rpId = rpId;
    }

    public String getWhCode() {
        return whCode;
    }

    public void setWhCode(String whCode) {
        this.whCode = whCode;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }
}
