package com.unlcn.ils.wms.backend.bo.inboundBO;

import com.unlcn.ils.wms.backend.bo.baseDataBO.WmsCommonBO;

/**
 * Created by DELL on 2017/9/1.
 */
public class WmsInboundPloyFlowtoBO extends WmsCommonBO {

    /**
     * 流向主id
     */
    private Long pyfId;

    /**
     * 策略主id
     */
    private String pyfPyId;

    /**
     * 策略主code
     */
    private String pyfPyCode;

    /**
     * 起始地id
     */
    private String pyfFromId;

    /**
     * 起始地code
     */
    private String pyfFromCode;

    /**
     * 起始地名
     */
    private String pyfFromName;

    /**
     * 目的地id
     */
    private String pyfToId;

    /**
     * 目的地code
     */
    private String pyfToCode;

    /**
     * 目的地名
     */
    private String pyfToName;

    public Long getPyfId() {
        return pyfId;
    }

    public void setPyfId(Long pyfId) {
        this.pyfId = pyfId;
    }

    public String getPyfPyId() {
        return pyfPyId;
    }

    public void setPyfPyId(String pyfPyId) {
        this.pyfPyId = pyfPyId;
    }

    public String getPyfPyCode() {
        return pyfPyCode;
    }

    public void setPyfPyCode(String pyfPyCode) {
        this.pyfPyCode = pyfPyCode;
    }

    public String getPyfFromId() {
        return pyfFromId;
    }

    public void setPyfFromId(String pyfFromId) {
        this.pyfFromId = pyfFromId;
    }

    public String getPyfFromCode() {
        return pyfFromCode;
    }

    public void setPyfFromCode(String pyfFromCode) {
        this.pyfFromCode = pyfFromCode;
    }

    public String getPyfFromName() {
        return pyfFromName;
    }

    public void setPyfFromName(String pyfFromName) {
        this.pyfFromName = pyfFromName;
    }

    public String getPyfToId() {
        return pyfToId;
    }

    public void setPyfToId(String pyfToId) {
        this.pyfToId = pyfToId;
    }

    public String getPyfToCode() {
        return pyfToCode;
    }

    public void setPyfToCode(String pyfToCode) {
        this.pyfToCode = pyfToCode;
    }

    public String getPyfToName() {
        return pyfToName;
    }

    public void setPyfToName(String pyfToName) {
        this.pyfToName = pyfToName;
    }

    @Override
    public String toString() {
        return "WmsInboundPloyFlowtoBO{" +
                "pyfId=" + pyfId +
                ", pyfPyId='" + pyfPyId + '\'' +
                ", pyfPyCode='" + pyfPyCode + '\'' +
                ", pyfFromId='" + pyfFromId + '\'' +
                ", pyfFromCode='" + pyfFromCode + '\'' +
                ", pyfFromName='" + pyfFromName + '\'' +
                ", pyfToId='" + pyfToId + '\'' +
                ", pyfToCode='" + pyfToCode + '\'' +
                ", pyfToName='" + pyfToName + '\'' +
                '}';
    }
}
