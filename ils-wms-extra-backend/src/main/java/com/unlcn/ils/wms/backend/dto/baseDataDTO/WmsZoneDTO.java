package com.unlcn.ils.wms.backend.dto.baseDataDTO;

import java.util.Date;

/**
 * Created by DELL on 2017/8/18.
 */
public class WmsZoneDTO {

    private Long zeId;

    private String zeWhCode;

    private String zeWhName;

    private String zone;

    private String createPerson;

    private String updatePerson;

    private Date gmtCreate;

    private Date gmtUpdate;

    private Byte isDeleted;

    private Long versions;

    public Long getZeId() {
        return zeId;
    }

    public void setZeId(Long zeId) {
        this.zeId = zeId;
    }

    public String getZeWhCode() {
        return zeWhCode;
    }

    public void setZeWhCode(String zeWhCode) {
        this.zeWhCode = zeWhCode;
    }

    public String getZeWhName() {
        return zeWhName;
    }

    public void setZeWhName(String zeWhName) {
        this.zeWhName = zeWhName;
    }

    public String getZone() {
        return zone;
    }

    public void setZone(String zone) {
        this.zone = zone;
    }

    public String getCreatePerson() {
        return createPerson;
    }

    public void setCreatePerson(String createPerson) {
        this.createPerson = createPerson;
    }

    public String getUpdatePerson() {
        return updatePerson;
    }

    public void setUpdatePerson(String updatePerson) {
        this.updatePerson = updatePerson;
    }

    public Date getGmtCreate() {
        return gmtCreate;
    }

    public void setGmtCreate(Date gmtCreate) {
        this.gmtCreate = gmtCreate;
    }

    public Date getGmtUpdate() {
        return gmtUpdate;
    }

    public void setGmtUpdate(Date gmtUpdate) {
        this.gmtUpdate = gmtUpdate;
    }

    public Byte getIsDeleted() {
        return isDeleted;
    }

    public void setIsDeleted(Byte isDeleted) {
        this.isDeleted = isDeleted;
    }

    public Long getVersions() {
        return versions;
    }

    public void setVersions(Long versions) {
        this.versions = versions;
    }

    @Override
    public String toString() {
        return "WmsZoneDTO{" +
                "zeId=" + zeId +
                ", zeWhCode='" + zeWhCode + '\'' +
                ", zeWhName='" + zeWhName + '\'' +
                ", zone='" + zone + '\'' +
                ", createPerson='" + createPerson + '\'' +
                ", updatePerson='" + updatePerson + '\'' +
                ", gmtCreate=" + gmtCreate +
                ", gmtUpdate=" + gmtUpdate +
                ", isDeleted=" + isDeleted +
                ", versions=" + versions +
                '}';
    }
}
