package com.unlcn.ils.wms.backend.service.baseData;

import com.unlcn.ils.wms.backend.bo.baseDataBO.WmsZoneBO;
import com.unlcn.ils.wms.base.businessDTO.baseData.WmsZoneQueryDTO;

import java.util.List;
import java.util.Map;

/**
 * Created by DELL on 2017/8/18.
 */
public interface WmsZoneService {
    /**
     * 查询分页
     * @param wmsZoneQueryDTO
     * @return
     * @throws IllegalAccessException
     */
    Map<String, Object> listZone(WmsZoneQueryDTO wmsZoneQueryDTO) throws IllegalAccessException;

    /**
     * 新增库区
     * @param wmsZoneBo
     */
    boolean addZone(WmsZoneBO wmsZoneBo);

    /**
     * 修改库区
     * @param wmsZoneBo
     */
    void updateZone(WmsZoneBO wmsZoneBo);

    /**
     * 删除库区
     * @param wmsZoneBOList
     */
    void deleteZone(List<WmsZoneBO> wmsZoneBOList);

    /**
     * 删除库区
     * @param zoneIdList
     */
    void deleteZoneNew(List<Long> zoneIdList);

    /**
     * 根据ID查询库区数据
     * @param zeId
     * @return
     */
    WmsZoneBO getZoneById(String zeId);
}
