package com.unlcn.ils.wms.backend.service.webservice.dto;

import java.io.Serializable;

public class ResultForDcsInorOutDetailsDTO implements Serializable {
    private String SpGroupBoardNo;
    private String SpMaterialCode;
    private String SpOrderNo;
    private String eMsg;

    public String getSpGroupBoardNo() {
        return SpGroupBoardNo;
    }

    public void setSpGroupBoardNo(String spGroupBoardNo) {
        SpGroupBoardNo = spGroupBoardNo;
    }

    public String getSpMaterialCode() {
        return SpMaterialCode;
    }

    public void setSpMaterialCode(String spMaterialCode) {
        SpMaterialCode = spMaterialCode;
    }

    public String getSpOrderNo() {
        return SpOrderNo;
    }

    public void setSpOrderNo(String spOrderNo) {
        SpOrderNo = spOrderNo;
    }

    public String geteMsg() {
        return eMsg;
    }

    public void seteMsg(String eMsg) {
        this.eMsg = eMsg;
    }
}
