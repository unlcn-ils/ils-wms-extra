package com.unlcn.ils.wms.backend.service.inbound.impl;

import com.google.common.collect.Maps;
import com.unlcn.ils.wms.backend.bo.inboundBO.WmsInboundCheckBO;
import com.unlcn.ils.wms.backend.enums.AsnOrderCheckEnum;
import com.unlcn.ils.wms.backend.enums.WmsInboundStatusEnum;
import com.unlcn.ils.wms.backend.enums.WmsRepairStatusEnum;
import com.unlcn.ils.wms.backend.service.inbound.AsnOrderCheckService;
import com.unlcn.ils.wms.base.mapper.additional.WmsInboundAsnOrderAddMapper;
import com.unlcn.ils.wms.base.mapper.inbound.*;
import com.unlcn.ils.wms.base.mapper.stock.WmsInventoryLocationMapper;
import com.unlcn.ils.wms.base.mapper.stock.WmsInventoryMapper;
import com.unlcn.ils.wms.base.model.inbound.WmsInboundCheck;
import com.unlcn.ils.wms.base.model.inbound.WmsInboundOrder;
import com.unlcn.ils.wms.base.model.inbound.WmsInboundOrderDetail;
import com.unlcn.ils.wms.base.model.inbound.WmsInboundRepair;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * Created by DELL on 2017/8/7.
 */
@Service
public class AsnOrderCheckServiceImpl implements AsnOrderCheckService {
    SimpleDateFormat sdf=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

    @Autowired
    private WmsInboundOrderMapper wmsInboundOrderMapper;

    @Autowired
    private WmsInboundOrderDetailMapper wmsInboundOrderDetailMapper;

    @Autowired
    private WmsInboundCheckMapper wmsInboundCheckMapper;

    @Autowired
    private WmsInboundRepairMapper wmsInboundRepairMapper;

    @Autowired
    private WmsInventoryMapper wmsInventoryMapper;

    @Autowired
    private WmsInboundAsnOrderAddMapper wmsInboundAsnOrderAddMapper;

    @Autowired
    private WmsInboundAllocationMapper wmsInboundAllocationMapper;

    @Autowired
    private WmsInventoryLocationMapper wmsInventoryLocationMapper;

    @Override
    public boolean submitCheckAsnOrder(WmsInboundCheckBO wmsInboundCheckBO)throws Exception{
        int result = 0;
        /*try {*/
            WmsInboundOrder wmsInboundAsnOrder = wmsInboundOrderMapper.selectByPrimaryKey(wmsInboundCheckBO.getOdId());
            Map<String, Object> paramMap = Maps.newHashMap();
            paramMap.put("oddOdId", wmsInboundCheckBO.getOdId());
            List<WmsInboundOrderDetail> wmsInboundAsnOrderDetailList = wmsInboundAsnOrderAddMapper.selectByAsnOrderId(paramMap);
            for (WmsInboundOrderDetail wmsInboundAsnOrderDetail : wmsInboundAsnOrderDetailList) {
                WmsInboundCheck wmsInboundCheck = new WmsInboundCheck();
                wmsInboundCheck.setCkOdId(String.valueOf(wmsInboundAsnOrder.getOdId()));
                wmsInboundCheck.setCkWaybillNo(wmsInboundAsnOrder.getOdWaybillNo());//运单号
                wmsInboundCheck.setCkVehicleSpecCode(wmsInboundAsnOrderDetail.getOddVehicleSpecCode());//车型
                wmsInboundCheck.setCkVehicleSpecName(wmsInboundAsnOrderDetail.getOddVehicleSpecName());//车型
                wmsInboundCheck.setCkVin(wmsInboundAsnOrderDetail.getOddVin());//底盘号
                wmsInboundCheck.setCkEngine(wmsInboundAsnOrderDetail.getOddEngine());//发动机号
                wmsInboundCheck.setCkCheckResult(String.valueOf(wmsInboundCheckBO.getOdCheckResult()));
                wmsInboundCheck.setCkCheckDesc(wmsInboundCheckBO.getOdCheckDesc());

                if (String.valueOf(AsnOrderCheckEnum.ASNORDER_PASS.getValue()).equals(wmsInboundCheckBO.getOdCheckResult())) {//合格
                    result = wmsInboundCheckMapper.insert(wmsInboundCheck);
                } else if (String.valueOf(AsnOrderCheckEnum.ASNORDER_NOTPASS_IN_REPAIR.getValue()).equals(wmsInboundCheckBO.getOdCheckResult())
                        || String.valueOf(AsnOrderCheckEnum.ASNORDER_NOTPASS_OUT_REPAIR.getValue()).equals(wmsInboundCheckBO.getOdCheckResult())) { //库内维修委外返工需要 生成维修单
                    WmsInboundRepair wmsInboundRepair = new WmsInboundRepair();
                    wmsInboundRepair.setIsDeleted((byte) 0);
                    wmsInboundRepair.setRpStatus(String.valueOf(WmsRepairStatusEnum.WMS_REPAIR_STATUS_NOT_HANDLE.getValue()));
                    wmsInboundRepair.setRpOdId(String.valueOf(wmsInboundCheckBO.getOdId()));
                    wmsInboundRepair.setRpCustomerNo(String.valueOf(wmsInboundCheckBO.getOdCustomerId()));
                    wmsInboundRepair.setRpCustomerName(wmsInboundCheckBO.getOdCustomerName());
                    wmsInboundRepair.setRpWaybillNo(wmsInboundCheckBO.getOdWaybillNo());
                    wmsInboundRepair.setRpWhId(String.valueOf(wmsInboundCheckBO.getOdWhId()));
                    wmsInboundRepair.setRpVehicleSpecCode(wmsInboundCheckBO.getWmsInboundOrderDetailBOList().get(0).getOddVehicleSpecCode());
                    wmsInboundRepair.setRpVehicleSpecName(wmsInboundAsnOrderDetail.getOddVehicleSpecName());
                    wmsInboundRepair.setRpVin(wmsInboundAsnOrderDetail.getOddVin());
                    wmsInboundRepair.setRpEngine(wmsInboundAsnOrderDetail.getOddEngine());
                    wmsInboundRepair.setRpCheckResult(wmsInboundCheckBO.getOdCheckResult().toString());
                    wmsInboundRepair.setRpType(wmsInboundCheckBO.getOdCheckResult().toString());
                    result = wmsInboundRepairMapper.insert(wmsInboundRepair);
                } else if (String.valueOf(AsnOrderCheckEnum.ASNORDER_NOTPASS.getValue()).equals(wmsInboundCheckBO.getOdCheckResult())) {//不合格直接退厂
                    result = wmsInboundCheckMapper.insert(wmsInboundCheck);
                }
            }
            //修改订单表的订单状态
            wmsInboundAsnOrder.setOdCheckResult(wmsInboundCheckBO.getOdCheckResult());
            wmsInboundAsnOrder.setOdCheckDesc(wmsInboundCheckBO.getOdCheckDesc());
            wmsInboundAsnOrder.setOdRepairParty(wmsInboundCheckBO.getOdRepairParty());
            wmsInboundAsnOrder.setOdConsigneeDate(new Date());

            wmsInboundAsnOrder.setOdStatus(String.valueOf(WmsInboundStatusEnum.WMS_INBOUND_WAIT_INBOUND.getValue()));//质检不管是否合格都修改为已收货
            result = wmsInboundOrderMapper.updateByPrimaryKey(wmsInboundAsnOrder);
        /*}catch(Exception ex){
            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
            throw new Exception("数据库异常");
        }*/

        return result > 0 ? true : false;
    }
}
