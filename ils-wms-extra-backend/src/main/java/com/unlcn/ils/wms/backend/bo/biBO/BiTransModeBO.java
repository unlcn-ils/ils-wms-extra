package com.unlcn.ils.wms.backend.bo.biBO;

import java.io.Serializable;

/**
 * Created by Nicky on 17/11/9.
 */
public class BiTransModeBO implements Serializable {
    /**
     * 统计日期
     */
    private String rpt_date;

    /**
     * 水运
     */
    private String dock_qty;

    /**
     *铁运
     */
    private String rail_qty;

    /**
     * 板运
     */
    private String truck_qty;

    /**
     * 人送地跑
     */
    private String road_qty;


    public String getRpt_date() {
        return rpt_date;
    }

    public void setRpt_date(String rpt_date) {
        this.rpt_date = rpt_date;
    }

    public String getDock_qty() {
        return dock_qty;
    }

    public void setDock_qty(String dock_qty) {
        this.dock_qty = dock_qty;
    }

    public String getRail_qty() {
        return rail_qty;
    }

    public void setRail_qty(String rail_qty) {
        this.rail_qty = rail_qty;
    }

    public String getTruck_qty() {
        return truck_qty;
    }

    public void setTruck_qty(String truck_qty) {
        this.truck_qty = truck_qty;
    }

    public String getRoad_qty() {
        return road_qty;
    }

    public void setRoad_qty(String road_qty) {
        this.road_qty = road_qty;
    }

    @Override
    public String toString() {
        return "BiTransModeBO{" +
                "rpt_date='" + rpt_date + '\'' +
                ", dock_qty='" + dock_qty + '\'' +
                ", rail_qty='" + rail_qty + '\'' +
                ", truck_qty='" + truck_qty + '\'' +
                ", road_qty='" + road_qty + '\'' +
                '}';
    }
}
