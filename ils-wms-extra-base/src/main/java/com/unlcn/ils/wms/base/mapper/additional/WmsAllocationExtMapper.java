package com.unlcn.ils.wms.base.mapper.additional;

import com.unlcn.ils.wms.base.model.inbound.WmsInboundAllocation;

import java.util.Map;

/**
 * 分配
 * Created by DELL on 2017/8/14.
 */
public interface WmsAllocationExtMapper {
    void updateStatusByAsnOrderId(Map<String,Object> paramMap);
    void updateAllocationByOdIdOrWaybillNo(WmsInboundAllocation wmsInboundAllocation);

}
