package com.unlcn.ils.wms.backend.dto.excpDTO;

/**
 * @Auther linbao
 * @Date 2017-11-24
 * 异常信息图片照片
 */
public class ExcpImageDTO {

    private String desc;

    private String imageUrl;

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }
}
