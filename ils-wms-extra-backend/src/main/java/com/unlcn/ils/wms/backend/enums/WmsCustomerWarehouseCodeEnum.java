package com.unlcn.ils.wms.backend.enums;

import com.google.common.collect.Lists;

import java.util.ArrayList;
import java.util.List;

/**
 * 客户编码和仓库对应的枚举
 */
public enum WmsCustomerWarehouseCodeEnum {
    JL_QS("JL", "JMC_NC_QS", "江铃-全顺库"),
    JL_CQ("JL", "UNLCN_XN_CQ", "江铃-重庆前置库"),
    JM_CS("JM", "JM_CS", "君马库长沙库"),
    JM_XY("JM", "JM_XY", "君马库襄阳库"),
    ALL_QS("WMS_ALL", "JMC_NC_QS", "君马库襄阳库"),
    ALL_CQ("WMS_ALL", "UNLCN_XN_CQ", "君马库襄阳库"),
    ALL_CS("WMS_ALL", "JM_CS", "君马库襄阳库"),
    ALL_XY("WMS_ALL", "JM_XY", "君马库襄阳库");

    private final String customerCode;
    private final String warehouseCode;
    private final String text;

    public String getCustomerCode() {
        return customerCode;
    }

    public String getWarehouseCode() {
        return warehouseCode;
    }

    public String getText() {
        return text;
    }

    WmsCustomerWarehouseCodeEnum(String customerCode, String warehouseCode, String text) {
        this.customerCode = customerCode;
        this.warehouseCode = warehouseCode;
        this.text = text;
    }

    /**
     * 根据客户code获取所属仓库列表
     *
     * @param customerCode 客户code
     * @return 返回值
     */
    public static List<WmsCustomerWarehouseCodeEnum> getListForByCustomer(String customerCode) {
        ArrayList<WmsCustomerWarehouseCodeEnum> codeList = Lists.newArrayList();
        for (WmsCustomerWarehouseCodeEnum temp : WmsCustomerWarehouseCodeEnum.values()) {
            if (temp.getCustomerCode().startsWith(customerCode)) {
                codeList.add(temp);
            }
        }
        return codeList;
    }
}
