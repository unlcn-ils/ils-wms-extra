package com.unlcn.ils.wms.base.mapper.extmapper;

import com.unlcn.ils.wms.base.model.inbound.WmsBatchNo;
import com.unlcn.ils.wms.base.model.inbound.WmsBatchNoExample;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

public interface WmsBatchNoExtMapper {

    List<WmsBatchNo> findByParams(@Param("paramsMap") Map<String, Object> paramsMap);
}