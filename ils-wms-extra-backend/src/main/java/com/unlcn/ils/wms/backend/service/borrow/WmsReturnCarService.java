package com.unlcn.ils.wms.backend.service.borrow;

import com.unlcn.ils.wms.base.dto.ReturnCarLineDTO;
import com.unlcn.ils.wms.base.dto.ReturnDetailDTO;
import com.unlcn.ils.wms.base.model.stock.WmsBorrowCarDetail;
import com.unlcn.ils.wms.base.model.stock.WmsReturnCar;

import java.util.List;
import java.util.Map;

/**
 * Created by lenovo on 2017/10/20.
 */
public interface WmsReturnCarService {
    /**
     * 还车登记列表统计
     * @param paramsMap
     * @return
     */
    Integer returnLineCount(Map<String, Object> paramsMap);

    /**
     * 还车登记列表
     * @param paramsMap
     * @return
     */
    List<ReturnCarLineDTO> returnLine(Map<String, Object> paramsMap);

    /**
     * 查找借车单明细
     * @param returnId
     * @return
     */
    List<WmsBorrowCarDetail> findReturnDetail(Long returnId);

    /**
     * 新增还车单
     * @param wmsReturnCar
     */
    void add(WmsReturnCar wmsReturnCar, List<WmsBorrowCarDetail> wmsBorrowCarDetailList);

    /**
     * 查询编辑还车单明细
     * @param rcId
     * @return
     */
    List<ReturnDetailDTO> queryModifyDetail(Long rcId);

    /**
     * 新增还车单
     * @param wmsReturnCar
     */
    void modify(WmsReturnCar wmsReturnCar, List<WmsBorrowCarDetail> wmsBorrowCarDetailList);

    /**
     * 删除还车单
     * @param rcId
     */
    void delete(List<Long> rcId);

    /**
     * 查找借车明细
     * @param borrowNo
     * @return
     */
    List<WmsBorrowCarDetail> getDetail(String borrowNo);
}
