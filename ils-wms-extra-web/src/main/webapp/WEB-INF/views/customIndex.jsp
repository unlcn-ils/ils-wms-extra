<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%!
    private String hostAddress;
%><%
	response.setHeader("X-Frame-Options", "SAMEORIGIN");
    String urlPath = request.getContextPath();
    String hostAddress = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort() + urlPath;
%>
<!DOCTYPE html>
<html lang="en">
<head>
    <title>仓储管理系统</title>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <link href="<%=hostAddress%>/public/css/bootstrap.min.css" type="text/css" rel="stylesheet"/>
    <link href="<%=hostAddress%>/public/lib/layui/css/layui.css" type="text/css" rel="stylesheet"/>
    <%--<link href="<%=hostAddress%>/public/lib/font-awesome/css/font-awesome.min.css" type="text/css" rel="stylesheet"/>--%>
    <%--<link href="<%=hostAddress%>/public/css/kas.css" type="text/css" rel="stylesheet"/>--%>
    <link href="<%=hostAddress%>/public/css/wms.css" type="text/css" rel="stylesheet"/>
</head>
<body ng-app="kasApp" ng-cloak>
    <div class="layui-layout layui-layout-admin">
        <div class="layui-body" style="position:initial;">
            <div class="layui-tab" lay-filter="tab_kas" style="margin-top: 2px;">
                <div class="layui-tab-content">
                    <div class="layui-tab-item layui-show">
                        <div ui-view></div>
                    </div>
                </div>
            </div>
        </div>
        <script type="text/javascript">
            var contextPath = "<%=hostAddress%>";
        </script>
        <!-- Lib Begin -->
        <script src="<%=hostAddress%>/public/lib/layui/layui.js" type="text/javascript"></script>
        <script src="<%=hostAddress%>/public/lib/jquery/jquery-1.9.1.min.js" type="text/javascript"></script>
        <script src="<%=hostAddress%>/public/lib/jquery/jquery-form.min.js" type="text/javascript"></script>
        <script src="<%=hostAddress%>/public/lib/layer/2.4/layer.js" type="text/javascript"></script>
        <script src="<%=hostAddress%>/public/lib/angular/angular.min.js" type="text/javascript"></script>
        <script src="<%=hostAddress%>/public/lib/angular-ui-router/angular-ui-router.min.js" type="text/javascript"></script>
        <script src="<%=hostAddress%>/public/lib/ngDialog/ngDialog.min.js" type="text/javascript"></script>
        <script src="<%=hostAddress%>/public/lib/restangular/restangular.min.js" type="text/javascript"></script>
        <script src="<%=hostAddress%>/public/lib/ui-select/select.min.js" type="text/javascript"></script>
        <script src="<%=hostAddress%>/public/lib/underscore/underscore.min.js" type="text/javascript"></script>
        <script src="<%=hostAddress%>/public/lib/ztree/jquery.ztree.all.min.js" type="text/javascript"></script>
        <!-- Lib End -->

        <!-- Ctrl Begin-->
        <script src="<%=hostAddress%>/public/js/kas-config.js" type="text/javascript"></script>
        <script src="<%=hostAddress%>/public/js/kas-service.js" type="text/javascript"></script>
        <script src="<%=hostAddress%>/public/js/kas-controllers.js" type="text/javascript"></script>
        <script src="<%=hostAddress%>/public/js/kas-tools.js" type="text/javascript"></script>
        <script src="<%=hostAddress%>/public/js/wms-filter.js" type="text/javascript"></script>
        <script src="<%=hostAddress%>/public/js/wms-constant.js" type="text/javascript"></script>
        <script src="<%=hostAddress%>/public/js/ctrl/a.ctrl.js" type="text/javascript"></script>
        <script src="<%=hostAddress%>/public/js/ctrl/b.ctrl.js" type="text/javascript"></script>
        <script src="<%=hostAddress%>/public/js/ctrl/c.ctrl.js" type="text/javascript"></script>
        <script src="<%=hostAddress%>/public/js/ctrl/d.ctrl.js" type="text/javascript"></script>
        <script src="<%=hostAddress%>/public/js/ctrl/e.ctrl.js" type="text/javascript"></script>
        <script src="<%=hostAddress%>/public/js/ctrl/f.ctrl.js" type="text/javascript"></script>
        <script src="<%=hostAddress%>/public/js/ctrl/g.ctrl.js" type="text/javascript"></script>
        <script src="<%=hostAddress%>/public/js/ctrl/h.ctrl.js" type="text/javascript"></script>
        <script src="<%=hostAddress%>/public/js/ctrl/i.ctrl.js" type="text/javascript"></script>
        <script src="<%=hostAddress%>/public/js/ctrl/j.ctrl.js" type="text/javascript"></script>
        <script src="<%=hostAddress%>/public/js/ctrl/k.ctrl.js" type="text/javascript"></script>
        <script src="<%=hostAddress%>/public/js/ctrl/l.ctrl.js" type="text/javascript"></script>
        <script src="<%=hostAddress%>/public/js/ctrl/m.ctrl.js" type="text/javascript"></script>
        <script src="<%=hostAddress%>/public/js/ctrl/n.ctrl.js" type="text/javascript"></script>
        <script src="<%=hostAddress%>/public/js/ctrl/o.ctrl.js" type="text/javascript"></script>
        <script src="<%=hostAddress%>/public/js/ctrl/p.ctrl.js" type="text/javascript"></script>
        <script src="<%=hostAddress%>/public/js/ctrl/q.ctrl.js" type="text/javascript"></script>
        <script src="<%=hostAddress%>/public/js/ctrl/r.ctrl.js" type="text/javascript"></script>
        <script src="<%=hostAddress%>/public/js/ctrl/s.ctrl.js" type="text/javascript"></script>
        <script src="<%=hostAddress%>/public/js/ctrl/t.ctrl.js" type="text/javascript"></script>
        <script src="<%=hostAddress%>/public/js/ctrl/u.ctrl.js" type="text/javascript"></script>
        <script src="<%=hostAddress%>/public/js/ctrl/v.ctrl.js" type="text/javascript"></script>
        <script src="<%=hostAddress%>/public/js/ctrl/w.ctrl.js" type="text/javascript"></script>
        <script src="<%=hostAddress%>/public/js/ctrl/x.ctrl.js" type="text/javascript"></script>
        <script src="<%=hostAddress%>/public/js/ctrl/y.ctrl.js" type="text/javascript"></script>
        <script src="<%=hostAddress%>/public/js/ctrl/z.ctrl.js" type="text/javascript"></script>
        <!-- Ctrl End-->
    </div>
</body>
</html>