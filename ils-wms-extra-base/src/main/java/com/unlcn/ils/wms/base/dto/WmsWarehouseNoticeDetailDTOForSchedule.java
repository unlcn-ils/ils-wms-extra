package com.unlcn.ils.wms.base.dto;

import java.io.Serializable;
import java.util.Date;

public class WmsWarehouseNoticeDetailDTOForSchedule implements Serializable {
    private Long wndId;
    private Long wndHeadId;
    private String wndVin;
    private Long wndVehicleId;
    private String wndVehicleCode;
    private String wndVehicleName;
    private String wndVehicleDesc;
    private String wndMaterialCode;
    private String wndMaterialName;
    private String wndColorCode;
    private String wndColorName;
    private String wndEngineNo;
    private String wndCertification;
    private String wndGearBoxNo;
    private Date wndProducteTime;
    private String wndOfflineTime;
    private String wndCustomerWhno;
    private Date wndCreate;
    private Date wndUpdate;
    private Integer wndCreateUserId;
    private String wndCreateUserName;
    private Integer wndUpdateUserId;
    private String wndUpdateUserName;
    private String wndSendBusinessFlag;
    private String wndTransferFlag;
    private String wndModifyType;
    private String wndBusinessStatus;
    private String wndDataStatus;
    private String wndBusinessType;
    private String wndWarehouseCode;
    private String inspectStatus;
    private String odCheckResult;
    private String odCheckDesc;

    public String getOdCheckResult() {
        return odCheckResult;
    }

    public void setOdCheckResult(String odCheckResult) {
        this.odCheckResult = odCheckResult;
    }

    public String getOdCheckDesc() {
        return odCheckDesc;
    }

    public void setOdCheckDesc(String odCheckDesc) {
        this.odCheckDesc = odCheckDesc;
    }

    public String getInspectStatus() {
        return inspectStatus;
    }

    public void setInspectStatus(String inspectStatus) {
        this.inspectStatus = inspectStatus;
    }

    public String getWndBusinessType() {
        return wndBusinessType;
    }

    public void setWndBusinessType(String wndBusinessType) {
        this.wndBusinessType = wndBusinessType;
    }

    public String getWndCustomerWhno() {
        return wndCustomerWhno;
    }

    public void setWndCustomerWhno(String wndCustomerWhno) {
        this.wndCustomerWhno = wndCustomerWhno;
    }

    public String getWndSendBusinessFlag() {
        return wndSendBusinessFlag;
    }

    public void setWndSendBusinessFlag(String wndSendBusinessFlag) {
        this.wndSendBusinessFlag = wndSendBusinessFlag;
    }

    public String getWndTransferFlag() {
        return wndTransferFlag;
    }

    public void setWndTransferFlag(String wndTransferFlag) {
        this.wndTransferFlag = wndTransferFlag;
    }

    public String getWndModifyType() {
        return wndModifyType;
    }

    public void setWndModifyType(String wndModifyType) {
        this.wndModifyType = wndModifyType;
    }

    public Long getWndId() {
        return wndId;
    }

    public void setWndId(Long wndId) {
        this.wndId = wndId;
    }

    public Long getWndHeadId() {
        return wndHeadId;
    }

    public void setWndHeadId(Long wndHeadId) {
        this.wndHeadId = wndHeadId;
    }

    public String getWndVin() {
        return wndVin;
    }

    public void setWndVin(String wndVin) {
        this.wndVin = wndVin;
    }

    public Long getWndVehicleId() {
        return wndVehicleId;
    }

    public void setWndVehicleId(Long wndVehicleId) {
        this.wndVehicleId = wndVehicleId;
    }

    public String getWndVehicleCode() {
        return wndVehicleCode;
    }

    public void setWndVehicleCode(String wndVehicleCode) {
        this.wndVehicleCode = wndVehicleCode;
    }

    public String getWndVehicleName() {
        return wndVehicleName;
    }

    public void setWndVehicleName(String wndVehicleName) {
        this.wndVehicleName = wndVehicleName;
    }

    public String getWndVehicleDesc() {
        return wndVehicleDesc;
    }

    public void setWndVehicleDesc(String wndVehicleDesc) {
        this.wndVehicleDesc = wndVehicleDesc;
    }

    public String getWndMaterialCode() {
        return wndMaterialCode;
    }

    public void setWndMaterialCode(String wndMaterialCode) {
        this.wndMaterialCode = wndMaterialCode;
    }

    public String getWndMaterialName() {
        return wndMaterialName;
    }

    public void setWndMaterialName(String wndMaterialName) {
        this.wndMaterialName = wndMaterialName;
    }

    public String getWndColorCode() {
        return wndColorCode;
    }

    public void setWndColorCode(String wndColorCode) {
        this.wndColorCode = wndColorCode;
    }

    public String getWndColorName() {
        return wndColorName;
    }

    public void setWndColorName(String wndColorName) {
        this.wndColorName = wndColorName;
    }

    public String getWndEngineNo() {
        return wndEngineNo;
    }

    public void setWndEngineNo(String wndEngineNo) {
        this.wndEngineNo = wndEngineNo;
    }

    public String getWndCertification() {
        return wndCertification;
    }

    public void setWndCertification(String wndCertification) {
        this.wndCertification = wndCertification;
    }

    public String getWndGearBoxNo() {
        return wndGearBoxNo;
    }

    public void setWndGearBoxNo(String wndGearBoxNo) {
        this.wndGearBoxNo = wndGearBoxNo;
    }

    public Date getWndProducteTime() {
        return wndProducteTime;
    }

    public void setWndProducteTime(Date wndProducteTime) {
        this.wndProducteTime = wndProducteTime;
    }

    public String getWndOfflineTime() {
        return wndOfflineTime;
    }

    public void setWndOfflineTime(String wndOfflineTime) {
        this.wndOfflineTime = wndOfflineTime;
    }

    public Date getWndCreate() {
        return wndCreate;
    }

    public void setWndCreate(Date wndCreate) {
        this.wndCreate = wndCreate;
    }

    public Date getWndUpdate() {
        return wndUpdate;
    }

    public void setWndUpdate(Date wndUpdate) {
        this.wndUpdate = wndUpdate;
    }

    public Integer getWndCreateUserId() {
        return wndCreateUserId;
    }

    public void setWndCreateUserId(Integer wndCreateUserId) {
        this.wndCreateUserId = wndCreateUserId;
    }

    public String getWndCreateUserName() {
        return wndCreateUserName;
    }

    public void setWndCreateUserName(String wndCreateUserName) {
        this.wndCreateUserName = wndCreateUserName;
    }

    public Integer getWndUpdateUserId() {
        return wndUpdateUserId;
    }

    public void setWndUpdateUserId(Integer wndUpdateUserId) {
        this.wndUpdateUserId = wndUpdateUserId;
    }

    public String getWndUpdateUserName() {
        return wndUpdateUserName;
    }

    public void setWndUpdateUserName(String wndUpdateUserName) {
        this.wndUpdateUserName = wndUpdateUserName;
    }

    public String getWndBusinessStatus() {
        return wndBusinessStatus;
    }

    public void setWndBusinessStatus(String wndBusinessStatus) {
        this.wndBusinessStatus = wndBusinessStatus;
    }

    public String getWndDataStatus() {
        return wndDataStatus;
    }

    public void setWndDataStatus(String wndDataStatus) {
        this.wndDataStatus = wndDataStatus;
    }

    public String getWndWarehouseCode() {
        return wndWarehouseCode;
    }

    public void setWndWarehouseCode(String wndWarehouseCode) {
        this.wndWarehouseCode = wndWarehouseCode;
    }

    @Override
    public String toString() {
        return "WmsWarehouseNoticeDetailDTOForSchedule{" +
                "wndId=" + wndId +
                ", wndHeadId=" + wndHeadId +
                ", wndVin='" + wndVin + '\'' +
                ", wndVehicleId=" + wndVehicleId +
                ", wndVehicleCode='" + wndVehicleCode + '\'' +
                ", wndVehicleName='" + wndVehicleName + '\'' +
                ", wndVehicleDesc='" + wndVehicleDesc + '\'' +
                ", wndMaterialCode='" + wndMaterialCode + '\'' +
                ", wndMaterialName='" + wndMaterialName + '\'' +
                ", wndColorCode='" + wndColorCode + '\'' +
                ", wndColorName='" + wndColorName + '\'' +
                ", wndEngineNo='" + wndEngineNo + '\'' +
                ", wndCertification='" + wndCertification + '\'' +
                ", wndGearBoxNo='" + wndGearBoxNo + '\'' +
                ", wndProducteTime='" + wndProducteTime + '\'' +
                ", wndOfflineTime='" + wndOfflineTime + '\'' +
                ", wndCustomerWhno='" + wndCustomerWhno + '\'' +
                ", wndCreate=" + wndCreate +
                ", wndUpdate=" + wndUpdate +
                ", wndCreateUserId=" + wndCreateUserId +
                ", wndCreateUserName='" + wndCreateUserName + '\'' +
                ", wndUpdateUserId=" + wndUpdateUserId +
                ", wndUpdateUserName='" + wndUpdateUserName + '\'' +
                ", wndBusinessStatus='" + wndBusinessStatus + '\'' +
                ", wndDataStatus='" + wndDataStatus + '\'' +
                ", wndWarehouseCode='" + wndWarehouseCode + '\'' +
                '}';
    }
}