package com.unlcn.ils.wms.backend.enums;

public enum WmsSysSourceEnum {
    DCS_IMPORT("0", "君马数据导入"),
    TMS("1", "TMS数据推送"),
    WMS_BUSINESS("2", "WMS业务产生库内返工、委外返工"),
    WMS_CREATE("3", "WMS新增"),
    WMS_RETURN("4", "WMS还车"),
    DCS_DEALER_BACK("5", "经销商退车"),
    DCS_SYNC("6", "DCS数据推送"),
    WMS_QUIT("7", "备料退库"),
    HTTP("HTTP", "http接口定时同步"),
    REST("REST", "REST方式修改数据"),
    IMPORT("IMPORT", "系统Excel导入"),
    MES_INTERFACE("MES_INTERFACE", "mes系统接口对接"),
    MQ("TMS_MQ", "MQ推送消息方式修改数据"),
    LIKE("LIKE", "app模糊查询"),
    ACCURATE("ACCU", "APP精确查找");

    private final String value;
    private final String text;

    WmsSysSourceEnum(String value, String text) {
        this.value = value;
        this.text = text;
    }

    public String getValue() {
        return value;
    }

    public String getText() {
        return text;
    }

    public static WmsSysSourceEnum getByValue(String value) {
        for (WmsSysSourceEnum temp : WmsSysSourceEnum.values()) {
            if (temp.getValue().equals(value)) {
                return temp;
            }
        }
        return null;
    }
}
