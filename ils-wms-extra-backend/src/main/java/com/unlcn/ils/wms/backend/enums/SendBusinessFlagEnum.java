package com.unlcn.ils.wms.backend.enums;

/**
 * 检验结果、维修结果
 * Created by DELL on 2017/8/24.
 */
public enum SendBusinessFlagEnum {
    SEND_Y(1,"已发送"),
    SEND_N(0,"未发送");

    private final int value;
    private final String text;
    SendBusinessFlagEnum(int value, String text){
        this.value = value;
        this.text = text;
    }

    public int getValue() {
        return value;
    }

    public String getText() {
        return text;
    }

    public static SendBusinessFlagEnum getByValue(int value){
        for (SendBusinessFlagEnum temp : SendBusinessFlagEnum.values()) {
            if(temp.getValue() == value){
                return temp;
            }
        }
        return null;
    }
}
