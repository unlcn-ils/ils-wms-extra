package com.unlcn.ils.wms.backend.bo.outboundBO;

import cn.huiyunche.commons.domain.PageVo;

/**
 * created by linbao on 2017-09-28
 */
public class WmsOutboundTaskFormBO extends PageVo{

    /**
     * 备料单号
     */
    private String otPreparationMaterialNo;

    /**
     * 车型
     */
    private String otVehicleSpecName;

    /**
     * 装车道
     */
    private String otEstimateLane;

    /**
     * 备料司机id
     */
    private String otDriver;

    /**
     * 备料司机名称
     */
    private String otDriverName;

    /**
     * 状态
     */
    private String otStatus;

    /**
     * 创建开始时间
     */
    private String startCreateTime;

    /**
     * 创建结束时间
     */
    private String endCreateTime;

    /**
     * 备料确认开始时间
     */
    private String startBlConfirmTime;

    /**
     * 备料确认结束时间
     */
    private String endBlConfirmTime;

    /**
     * 车架号
     */
    private String otVin;

    /**
     * 仓库code
     */
    private String otWhCode;

    /**
     * 交接单号
     */
    private String hoHandoverNumber;

    /**
     * 打印状态
     */
    private Integer printStatus;

    /**
     * id
     */
    private Long otId;

    /**
     * 取消原因
     */
    private String cancleReason;

    /**
     * 退库状态, 0-正常, 1-已退库
     */
    private Integer otQuitFlag;

    /**
     * 发运状态
     */
    private Integer otOutboundFlag;

    public Integer getOtOutboundFlag() {
        return otOutboundFlag;
    }

    public void setOtOutboundFlag(Integer otOutboundFlag) {
        this.otOutboundFlag = otOutboundFlag;
    }

    public String getOtDriver() {
        return otDriver;
    }

    public void setOtDriver(String otDriver) {
        this.otDriver = otDriver;
    }

    public String getOtDriverName() {
        return otDriverName;
    }

    public void setOtDriverName(String otDriverName) {
        this.otDriverName = otDriverName;
    }

    public String getCancleReason() {
        return cancleReason;
    }

    public void setCancleReason(String cancleReason) {
        this.cancleReason = cancleReason;
    }

    public Long getOtId() {
        return otId;
    }

    public void setOtId(Long otId) {
        this.otId = otId;
    }

    public String getUserId() {
        return userId;
    }

    /**
     * 打印开始时间
     */
    private String printStartTime;

    /**
     * 打印截至时间
     */
    private String printEndTime;

    private String userId;

    public String getOtPreparationMaterialNo() {
        return otPreparationMaterialNo;
    }

    public void setOtPreparationMaterialNo(String otPreparationMaterialNo) {
        this.otPreparationMaterialNo = otPreparationMaterialNo;
    }

    public String getOtVehicleSpecName() {
        return otVehicleSpecName;
    }

    public void setOtVehicleSpecName(String otVehicleSpecName) {
        this.otVehicleSpecName = otVehicleSpecName;
    }

    public String getOtEstimateLane() {
        return otEstimateLane;
    }

    public void setOtEstimateLane(String otEstimateLane) {
        this.otEstimateLane = otEstimateLane;
    }

    public String getOtStatus() {
        return otStatus;
    }

    public void setOtStatus(String otStatus) {
        this.otStatus = otStatus;
    }

    public String getStartCreateTime() {
        return startCreateTime;
    }

    public void setStartCreateTime(String startCreateTime) {
        this.startCreateTime = startCreateTime;
    }

    public String getEndCreateTime() {
        return endCreateTime;
    }

    public void setEndCreateTime(String endCreateTime) {
        this.endCreateTime = endCreateTime;
    }

    public String getStartBlConfirmTime() {
        return startBlConfirmTime;
    }

    public void setStartBlConfirmTime(String startBlConfirmTime) {
        this.startBlConfirmTime = startBlConfirmTime;
    }

    public String getEndBlConfirmTime() {
        return endBlConfirmTime;
    }

    public void setEndBlConfirmTime(String endBlConfirmTime) {
        this.endBlConfirmTime = endBlConfirmTime;
    }

    public String getOtVin() {
        return otVin;
    }

    public void setOtVin(String otVin) {
        this.otVin = otVin;
    }

    public String getOtWhCode() {
        return otWhCode;
    }

    public void setOtWhCode(String otWhCode) {
        this.otWhCode = otWhCode;
    }

    public String getHoHandoverNumber() {
        return hoHandoverNumber;
    }

    public void setHoHandoverNumber(String hoHandoverNumber) {
        this.hoHandoverNumber = hoHandoverNumber;
    }

    public Integer getPrintStatus() {
        return printStatus;
    }

    public void setPrintStatus(Integer printStatus) {
        this.printStatus = printStatus;
    }

    public String getPrintStartTime() {
        return printStartTime;
    }

    public void setPrintStartTime(String printStartTime) {
        this.printStartTime = printStartTime;
    }

    public String getPrintEndTime() {
        return printEndTime;
    }

    public void setPrintEndTime(String printEndTime) {
        this.printEndTime = printEndTime;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public Integer getOtQuitFlag() {
        return otQuitFlag;
    }

    public void setOtQuitFlag(Integer otQuitFlag) {
        this.otQuitFlag = otQuitFlag;
    }

    @Override
    public String toString() {
        return "WmsOutboundTaskFormBO{" +
                "otPreparationMaterialNo='" + otPreparationMaterialNo + '\'' +
                ", otVehicleSpecName='" + otVehicleSpecName + '\'' +
                ", otEstimateLane='" + otEstimateLane + '\'' +
                ", otDriver='" + otDriver + '\'' +
                ", otDriverName='" + otDriverName + '\'' +
                ", otStatus='" + otStatus + '\'' +
                ", startCreateTime='" + startCreateTime + '\'' +
                ", endCreateTime='" + endCreateTime + '\'' +
                ", startBlConfirmTime='" + startBlConfirmTime + '\'' +
                ", endBlConfirmTime='" + endBlConfirmTime + '\'' +
                ", otVin='" + otVin + '\'' +
                ", otWhCode='" + otWhCode + '\'' +
                ", hoHandoverNumber='" + hoHandoverNumber + '\'' +
                ", printStatus=" + printStatus +
                ", otId=" + otId +
                ", cancleReason='" + cancleReason + '\'' +
                ", otQuitFlag=" + otQuitFlag +
                ", otOutboundFlag=" + otOutboundFlag +
                ", printStartTime='" + printStartTime + '\'' +
                ", printEndTime='" + printEndTime + '\'' +
                ", userId='" + userId + '\'' +
                '}';
    }
}
