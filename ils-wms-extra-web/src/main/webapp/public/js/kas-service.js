/**
 * Created by Tuffy on 16/6/7.
 */
'use strict';

var wmsService = angular.module('wms.services', [])
    .service('HttpService', ['Restangular', function (restangular) {
        return {
            customPOST: function (url, data) {
                return restangular.one(url).customPOST(data);
            },
            post: function (url, data) {
                return restangular.one(url).post(data);
            },
            get: function (url, params) {
                return restangular.one(url).get('/',params);
            },
            customGET: function (url, params) {
                return restangular.one(url).customGET('/',params);
            },
            put : function(url,data){

            },
            del : function(url,data){

            }
        };
    }])
    .service('inStorageBillService',['HttpService',function(httpService){   //入库service
        return {
            addReceipt : function(data){  //新增收货单
                return httpService.customPOST('asnOrder/addAsnOrder',data);
            },
            inStorageList : function(data){  //入库单列表
                return httpService.customPOST('asnOrder/getAsnOrderList',data);
            },
            receivingInspection : function(data){   //收货质检
                return httpService.customPOST('asnOrder/asnOrderCheck',data);
            },
            reservoirList : function(whCode){ //库区查询
                return httpService.customGET('baseData/getEmptyZone/'+ whCode);
            },
            storageLocationList : function(whCode,zoneId){   //库位查询
                return httpService.customGET('baseData/getEmptyLocation/' + whCode + '/' + zoneId);
            },
            createAlloc : function(allocType,data){   //手动分配
                return httpService.customPOST('wmsAlloc/createAlloc/' + allocType,data);
            },
            confirmAllocation : function(data){ //确认分配
                return httpService.customPOST('wmsAlloc/confirmAllocation',data);
            },
            confirmInbound : function(data){    //确认入库
                return httpService.customPOST('asnOrder/confirmInbound',data);
            },
            printBarcode : function(data){  //打印
                return httpService.customPOST('asnOrder/printBarcode',data);
            },
            getReceiptById : function(id){ //根据id获取入库单
                return httpService.customGET('asnOrder/getAsnOrderById/' + id);
            },
            updateAsnOrder : function(data){    //更新入库单
                return httpService.customPOST('asnOrder/updateAsnOrder',data);
            },
            deleteAsnOrder : function(data){    //删除入库单
                return httpService.customPOST('asnOrder/deleteAsnOrder',data);
            },
            autoAssign : function(strategyId,data){  //自动分配
                return httpService.customPOST('wmsAlloc/autoAllcation/' + strategyId,data)
            }
        };
    }])
    .service('inventorySelectService',['HttpService',function(httpService){    //库存service
        return {
            list : function(data){  //库存列表
                return httpService.customPOST('wmsInventory/list',data);
            },
            detail : function(id){    //详情
                return httpService.customGET('wmsInventory/listInventoryLocation/' + id);
            }
        }
    }])
    .service('reservoirService',['HttpService',function(httpService){   //库区service
        return {
            list : function(data){  //库区列表
                return httpService.customPOST('wmsZone/list',data);
            },
            add : function(data){   //新增库区
                return httpService.customPOST('wmsZone/addZone',data);
            },
            update : function(data){    //更新库区
                return httpService.customPOST('wmsZone/updateZone',data);
            },
            getById : function(id){ //根据id查库区
                return httpService.customGET('wmsZone/getZoneById/' + id);
            }
        }
    }])
    .service('stockUnitService',['HttpService',function(httpService){  //库位 service
        return {
            list : function(data){  //库位列表
                return httpService.customPOST('wmsLocation/list',data);
            },
            add : function(data){   //新增库位
                return httpService.customPOST('wmsLocation/addLocation',data)
            },
            getById : function(id){ //根据id查询库位
                return httpService.customGET('wmsLocation/getLocationById/' + id);
            },
            update : function(data){    //更新库位
                return httpService.customPOST('wmsLocation/updateLocation',data);
            },
            enableLocation : function(data,flag){   //更新库位状态
                return httpService.customPOST('wmsLocation/enableLocation/' + flag,data);
            }
        }
    }])
    .service('repairBillService',['HttpService',function(httpService){ //维修单 service
        return {
            list : function(data){  //维修单列表
                return httpService.customPOST('wmsRepair/list',data);
            },
            submitRepair : function(data){  //提交维修单
                return httpService.customPOST('wmsRepair/submitRepair',data);
            },
            addWmsRepair : function(data){  //维修单新增
                return httpService.customPOST('wmsRepair/addWmsRepair',data);
            },
            updateWmsRepair : function(){   //维修单更新
                return httpService.customPOST('wmsRepair/updateWmsRepair',data);
            },
            getById : function(id){   //维修单id查询
                return httpService.customGET('wmsRepair/getRepairById/' + id);
            },
            submitAfterCheckAdd : function(data){   //维修单后检验
                return httpService.customPOST('wmsRepair/submitAfterCheckAdd',data);
            },
            submitAfterCheckCopyAndAdd : function(data){    //维修单后检验复制并新增
                return httpService.customPOST('wmsRepair/submitAfterCheckCopyAndAdd',data);
            }
        }
    }])
    .service('warehouseService',['HttpService',function(httpService){  //出库单
        return {
            list : function(data){  //出库单列表
                return httpService.customPOST('outOrder/list',data);
            },
            addBLOrder : function(data){    //新增备料单
                return httpService.customPOST('outOrder/addBLOrder',data);
            },
            removeLocationConfirm : function(data){ //移库确认
                return httpService.customPOST('outOrder/removeLocationConfirm',data);
            },
            outboundCheck : function(data){ //出库质检
                return httpService.customPOST('outOrder/outboundCheck',data);
            },
            outboundConfirm : function(data){   //出库确认
                return httpService.customPOST('outOrder/outboundConfirm',data);
            }
        }
    }])
    .service('outboundOrderService',['HttpService',function(httpService){   //备料单 service
        return {
            list : function(data){  //备料单
                return httpService.customPOST('blOrder/list',data);
            },
            getOutOrderByBLId : function(id){   //根据id查询备料单
                return httpService.customGET('blOrder/getOutOrderByBLId/' + id);
            }
        }
    }])
    .service('stockUnitStrategyService',['HttpService',function(httpService){   //策略service
        return {
            list : function(whCode){  //策略列表    仓库code
                return httpService.customGET('inboundPloy/getPloyList/' + whCode);
            },
            getPloyConditionList : function(pyCode){  //策略条件列表
                return httpService.customGET('inboundPloy/getPloyConditionList/' + pyCode);
            },
            getPloyFlowtoList : function(pyCode){   //策略流向列表
                return httpService.customGET('inboundPloy/getPloyFlowtoList/' + pyCode);
            },
            getPloyLocationList : function(pyCode){ //策略库位分配
                return httpService.customGET('inboundPloy/getPloyLocationList/' + pyCode);
            },
            addPloy : function(data){   //新增策略
                return httpService.customPOST('inboundPloy/addPloy',data);
            },
            addPloyCondition : function(data){  //新增策略条件
                return httpService.customPOST('inboundPloy/addPloyCondition',data);
            },
            addPloyFlowto : function(pyCode,data){ //新增策略流向
                return httpService.customPOST('inboundPloy/addPloyFlowto/' + pyCode,data);
            }
        }
    }])
    .service('dropDownService',['HttpService',function(httpService){    //下拉service
        return {
            getZoneList : function(data){   //库区下拉 仓库code
                return httpService.customGET('baseData/getZone/' + data);
            },
            getCustomerInfo : function(){   //获取货主
                return httpService.customGET('baseData/getCustomerInfo');
            },
            getVehicleSpec : function(){    //获取车型
                return httpService.customGET('baseData/getVehicleSpec');
            },
            getArea : function(){   //获取区域信息
                return httpService.customGET('baseData/getArea');
            }
        }
    }])