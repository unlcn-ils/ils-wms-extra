package com.unlcn.ils.wms.backend.enums;

/**
 * 备料计划备料状态
 * Created by DELL on 2017/9/27.
 */
public enum WmsPreparationPlanEnum {
    WMS_PLAN_NEW(10,"未开始"),
    WMS_PLAN_START(20,"已计划"),
    WMS_PLAN_CONFIRM(30,"已备料"),
    WMS_PLAN_FINISH(40,"已完成");

    private final int value;
    private final String text;
    WmsPreparationPlanEnum(int value, String text){
        this.value = value;
        this.text = text;
    }

    public int getValue() {
        return value;
    }

    public String getText() {
        return text;
    }

    public static WmsPreparationPlanEnum getByValue(int value){
        for (WmsPreparationPlanEnum temp : WmsPreparationPlanEnum.values()) {
            if(temp.getValue() == value){
                return temp;
            }
        }
        return null;
    }
}
