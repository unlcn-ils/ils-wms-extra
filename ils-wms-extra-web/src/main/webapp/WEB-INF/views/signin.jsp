<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%!
    private String hostAddress;
%><%
    String urlPath = request.getContextPath();
    String hostAddress = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort() + urlPath;
%>
<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="keywords" content="江铃，大客户">
    <meta name="Description" content="描述">
    <title>欢迎登录大客户系统</title>
    <link rel="stylesheet" type="text/css" href="<%=hostAddress%>/public/css/login_02.css">
    <link rel="stylesheet" href="<%=hostAddress%>/public/lib/layui/css/layui.css" media="all" />
    <link rel="stylesheet" href="<%=hostAddress%>/public/css/login.css" />
</head>
<body>

<header>
    <ul>
        <li class='head-logo'>
            <img class='head_logo' src='<%=hostAddress%>/public/images/login_logo.png' />
        </li>
        <li class='head-t01'>
            <span>欢迎登录大客户系统</span>
        </li>
        <li class='head-t02'>
            <img src='<%=hostAddress%>/public/images/icon_customer_service.png' />
            <a href="#"><span>中联客服</span></a>
        </li>
        <li class='head-t03'>
            <img src='<%=hostAddress%>/public/images/icon_opinion.png' />
            <a href="#"><span>反馈意见</span></a>
        </li>
    </ul>

</header>
<section>
    <div id='container'>
        <div class="login-b01">
            <form action="" method="post" class="layui-form">
                <p class="login-text01">登录您的系统</p>
                <ul>
                    <li>
                        <div class="layui-form-item" >
                            <input type="text" name="username" lay-verify="userName"  autocomplete="off" placeholder="这里输入登录名" class="layui-input">
                        </div>
                    </li>
                    <li>
                        <div class="layui-form-item">
                            <input type="password" name="password" lay-verify="password" autocomplete="off" placeholder="这里输入密码" class="layui-input">
                        </div>
                    </li>

                    <li class="login-btn">
                        <div class="layui-form-item">
                            <div class="beg-pull-right">
                                <button class="layui-btn layui-btn-primary login-btn" lay-submit lay-filter="login">
                                    <i class="layui-icon">&#xe650;</i> 登录
                                </button>
                            </div>
                            <div class="beg-clear"></div>
                            <p><a href="#">忘记密码</a> | <a href="#">账户申请</a></p>
                        </div>
                    </li>
                </ul>
            </form>
        </div>
        <div class="login-b02">
            <ul>
                <li>&nbsp</li>
                <li>标准的作业流系统</li>
                <li>统一的数据交换系统</li>
                <li>完善的运力跟踪系统</li>
                <li>让我们的物流管理更显专业气质</li>
                <li>&nbsp</li>
                <li><p></p>——中联物流承诺给您更多！</li>
            </ul>

        </div>
    </div>
</section>
<footer>
    <div class="footer_text">
        <p>© 2005 - 2010 China-Union Logistics CO.,LTD. All Rights Reserved</p>
        <p><a href="#">关于中联</a> | <a href="#">服务条款</a></p>
    </div>
</footer>


<script type="text/javascript" src="<%=hostAddress%>/public/lib/layui/layui.js"></script>
<script type="text/javascript" src="<%=hostAddress%>/public/lib/jquery/jquery-1.9.1.min.js"></script>

<script>
    layui.use(['layer', 'form'], function() {
        var layer = layui.layer,
            $ = layui.jquery,
            form = layui.form();
        form.on('submit(login)',function(data){
            $.ajax({
                type: 'POST',
                url:'signin/form',
                data:data.field,
                success:function(data){
                    if(data.success){
                        <%--location.href='index?userName=' + data.data.userName;--%>
                        location.href = 'index'
                    } else {
                        layer.alert("请输入正确的用户名或者密码");
                    }
                }
            });
            return false;
        });
    });
</script>

</body>
</html>