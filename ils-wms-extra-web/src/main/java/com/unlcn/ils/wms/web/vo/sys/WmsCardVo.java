package com.unlcn.ils.wms.web.vo.sys;

import cn.huiyunche.commons.domain.PageVo;

import java.io.Serializable;
import java.util.Date;

public class WmsCardVo extends PageVo {

    private Long id;

    private Long driverId;

    private String cardNum;

    private String driverUserName;

    private String name;

    private Integer status;

    private String startTime;

    private String endTime;

    private String startCreateTime;

    private String endCreateTime;


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getDriverId() {
        return driverId;
    }

    public void setDriverId(Long driverId) {
        this.driverId = driverId;
    }

    public String getCardNum() {
        return cardNum;
    }

    public void setCardNum(String cardNum) {
        this.cardNum = cardNum;
    }

    public String getDriverUserName() {
        return driverUserName;
    }

    public void setDriverUserName(String driverUserName) {
        this.driverUserName = driverUserName;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getStartTime() {
        return startTime;
    }

    public void setStartTime(String startTime) {
        this.startTime = startTime;
    }

    public String getEndTime() {
        return endTime;
    }

    public void setEndTime(String endTime) {
        this.endTime = endTime;
    }

    public String getStartCreateTime() {
        return startCreateTime;
    }

    public void setStartCreateTime(String startCreateTime) {
        this.startCreateTime = startCreateTime;
    }

    public String getEndCreateTime() {
        return endCreateTime;
    }

    public void setEndCreateTime(String endCreateTime) {
        this.endCreateTime = endCreateTime;
    }

    @Override
    public String toString() {
        return "WmsCardVo{" +
                "id=" + id +
                ", driverId=" + driverId +
                ", cardNum='" + cardNum + '\'' +
                ", driverUserName='" + driverUserName + '\'' +
                ", name='" + name + '\'' +
                ", status=" + status +
                ", startTime='" + startTime + '\'' +
                ", endTime='" + endTime + '\'' +
                ", startCreateTime='" + startCreateTime + '\'' +
                ", endCreateTime='" + endCreateTime + '\'' +
                '}';
    }
}