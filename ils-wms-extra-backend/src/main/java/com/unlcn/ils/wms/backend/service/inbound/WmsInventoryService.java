package com.unlcn.ils.wms.backend.service.inbound;

import com.unlcn.ils.wms.backend.bo.inboundBO.WmsInboundCheckBO;
import com.unlcn.ils.wms.backend.bo.inboundBO.WmsInventoryLocationBO;
import com.unlcn.ils.wms.backend.dto.bigscreenDTO.InvoicingStatisticsChartDataDTO;
import com.unlcn.ils.wms.backend.dto.bigscreenDTO.InvoicingStatisticsLineDataDTO;
import com.unlcn.ils.wms.base.businessDTO.stock.WmsInventoryDTO;
import com.unlcn.ils.wms.base.businessDTO.stock.WmsInventoryQueryDTO;
import com.unlcn.ils.wms.base.dto.WmsInventoryChangeLocCodeDTO;
import com.unlcn.ils.wms.base.dto.WmsInventoryFreezeDTO;
import com.unlcn.ils.wms.base.dto.WmsInventoryLocationExtDTO;
import com.unlcn.ils.wms.base.model.stock.WmsInventory;
import com.unlcn.ils.wms.base.model.stock.WmsInventoryLocation;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;

import java.util.List;
import java.util.Map;

/**
 * 库存service
 * Created by DELL on 2017/8/8.
 */
public interface WmsInventoryService {

    Map<String, Object> queryListByInventory(WmsInventoryQueryDTO wmsInventoryQueryDTO) throws IllegalAccessException;

    Map<String, Object> queryInventoryListForJunMaOld(WmsInventoryQueryDTO wmsInventoryQueryDTO) throws Exception;

    List<WmsInventoryDTO> getWmsInventoryInfo(Map<String, Object> paramMap);

    List<WmsInventoryLocationBO> getWmsInventoryLocation(Map<String, Object> paramMap);

    List<WmsInventoryLocation> listInventoryLocation(String invId);

    void addInventory(WmsInboundCheckBO wmsInboundCheckBO) throws Exception;

    List<WmsInventory> selectInvertoryListForPlan(String materialCode, String color, String vehicleName, String whCode) throws Exception;

    List<WmsInventory> selectChangeVinForPlanNew(String materialCode, String colorCode, String vehicleCode, String whCode, String oldVin) throws Exception;

    List<WmsInventory> selectInvertoryListForPlanNew(String materialCode, String colorCode, String vehicleCode, String whCode) throws Exception;

    List<WmsInventoryLocation> listInventoryLocation(List<Long> invIdList, String whCode);

    Integer countInventoryLocation(List<Long> invIdList, String whCode);

    List<WmsInventoryLocation> listInventoryLocationForNormal(List<Long> invIdList, String whCode, int count);

    List<WmsInventoryLocation> listInventoryLocationForNormalBatchNoIsNull(List<Long> invIdList, String whCode, int count);

    Integer countInventoryLocationForNormal(List<Long> invIdList, String whCode, int count);

    void deleteInventoryForBatch(List<Long> idList) throws Exception;

    void deleteLocationForBatch(List<Long> idList) throws Exception;

    void updateStatusLocationForBatch(List<Long> idList, String status) throws Exception;

    void updateStatusLocation(Long id, String status) throws Exception;

    boolean updateStatusLocationByVin(String vin, String status) throws Exception;

    WmsInventoryLocation getInventoryLocationByVin(String vin) throws Exception;

    WmsInventoryLocation getInventoryLocationByParentId(Long id) throws Exception;

    List<WmsInventoryLocation> getLocationByVin(String vin) throws Exception;

    WmsInventory getWmsInventoryByPrimaryKey(Long id) throws Exception;

    List<InvoicingStatisticsChartDataDTO> invoicingStatisticsChartCount();

    InvoicingStatisticsLineDataDTO invoicingStatisticsLineCount();

    void addinvoicingStatisticsChart();

    HSSFWorkbook inventoryRecordImportForCq(WmsInventoryQueryDTO wmsInventoryQueryDTO) throws Exception;

    HSSFWorkbook inventoryRecordImportForJm(WmsInventoryQueryDTO wmsInventoryQueryDTO) throws Exception;

    Integer countInventoryRecord(WmsInventoryQueryDTO wmsInventoryQueryDTO) throws Exception;

    List<WmsInventoryLocationExtDTO> selectColourListForJM(WmsInventoryQueryDTO wmsInventoryQueryDTO) throws Exception;

    List<WmsInventoryLocationExtDTO> selectMaterialListForJM(WmsInventoryQueryDTO wmsInventoryQueryDTO) throws Exception;

    Map<String, Object> queryInventoryListForJunMa(WmsInventoryQueryDTO wmsInventoryQueryDTO) throws Exception;

    void updateStockFreeze(WmsInventoryFreezeDTO freezeDTO) throws Exception;

    void updateStockNormal(WmsInventoryFreezeDTO freezeDTO) throws Exception;

    void updateChangeLocCode(WmsInventoryChangeLocCodeDTO dto) throws Exception;
}
