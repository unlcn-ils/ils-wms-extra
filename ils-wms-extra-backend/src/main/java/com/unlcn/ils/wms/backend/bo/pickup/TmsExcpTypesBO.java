package com.unlcn.ils.wms.backend.bo.pickup;

/**
 * Created by houjianhui on 2017/7/25.
 */
public class TmsExcpTypesBO {
    private Long infoId;
    private String name;
    private String excpTypes;
    private String attachs;
    private String vin;
    private String userId;

    public Long getInfoId() {
        return infoId;
    }

    public void setInfoId(Long infoId) {
        this.infoId = infoId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getExcpTypes() {
        return excpTypes;
    }

    public void setExcpTypes(String excpTypes) {
        this.excpTypes = excpTypes;
    }

    public String getAttachs() {
        return attachs;
    }

    public void setAttachs(String attachs) {
        this.attachs = attachs;
    }

    public String getVin() {
        return vin;
    }

    public void setVin(String vin) {
        this.vin = vin;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    @Override
    public String toString() {
        return "TmsExcpTypesBO{" +
                "infoId=" + infoId +
                ", name='" + name + '\'' +
                ", excpTypes='" + excpTypes + '\'' +
                ", attachs='" + attachs + '\'' +
                ", vin='" + vin + '\'' +
                ", userId='" + userId + '\'' +
                '}';
    }
}
