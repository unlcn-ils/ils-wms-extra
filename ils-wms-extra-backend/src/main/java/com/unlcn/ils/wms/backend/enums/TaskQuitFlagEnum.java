package com.unlcn.ils.wms.backend.enums;

/**
 * @Auther linbao
 * @Date 2017-11-04
 */
public enum TaskQuitFlagEnum {

    NOT_QUIT((byte)0, "正常"),
    HAS_QUIT((byte)1, "已退库");

    private final byte value;
    private final String text;

    public byte getValue() {
        return value;
    }

    public String getText() {
        return text;
    }

    TaskQuitFlagEnum(byte value, String text) {
        this.value = value;
        this.text = text;
    }
}
