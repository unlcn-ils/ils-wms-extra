package com.unlcn.ils.wms.base.dto;

/**
 * Created by lenovo on 2017/10/20.
 */
public class InventoryDetailDTO {
    // 库存id
    private Long invId;

    // 车系
    private String invVehicleSeriesName;

    // 车型code
    private String invVehicleSpecCode;

    // 车型名称
    private String invVehicleSpecName;

    // 车型描述
    private String invVehicleSpecDesc;

    // 物料code
    private String invMaterialCode;

    // 物料名称
    private String invMaterialName;

    // 颜色
    private String invColor;

    // 明细id
    private Long invlocId;

    // 数量
    private Integer invNum;

    // 车架号
    private String invlocVin;

    // 库区code
    private String invlocZoneCode;

    // 库区名称
    private String invlocZoneName;

    // 库位code
    private String invlocLocCode;

    // 库位名称
    private String invlocLocName;

    // 仓库code
    private String invlocWhCode;

    // 仓库名称
    private String invlocWhName;

    // 中联运单号
    private String invlocWaybillNo;

    public Long getInvId() {
        return invId;
    }

    public void setInvId(Long invId) {
        this.invId = invId;
    }

    public String getInvVehicleSeriesName() {
        return invVehicleSeriesName;
    }

    public void setInvVehicleSeriesName(String invVehicleSeriesName) {
        this.invVehicleSeriesName = invVehicleSeriesName;
    }

    public String getInvVehicleSpecCode() {
        return invVehicleSpecCode;
    }

    public void setInvVehicleSpecCode(String invVehicleSpecCode) {
        this.invVehicleSpecCode = invVehicleSpecCode;
    }

    public String getInvVehicleSpecName() {
        return invVehicleSpecName;
    }

    public void setInvVehicleSpecName(String invVehicleSpecName) {
        this.invVehicleSpecName = invVehicleSpecName;
    }

    public String getInvVehicleSpecDesc() {
        return invVehicleSpecDesc;
    }

    public void setInvVehicleSpecDesc(String invVehicleSpecDesc) {
        this.invVehicleSpecDesc = invVehicleSpecDesc;
    }

    public String getInvMaterialCode() {
        return invMaterialCode;
    }

    public void setInvMaterialCode(String invMaterialCode) {
        this.invMaterialCode = invMaterialCode;
    }

    public String getInvMaterialName() {
        return invMaterialName;
    }

    public void setInvMaterialName(String invMaterialName) {
        this.invMaterialName = invMaterialName;
    }

    public String getInvColor() {
        return invColor;
    }

    public void setInvColor(String invColor) {
        this.invColor = invColor;
    }

    public Long getInvlocId() {
        return invlocId;
    }

    public void setInvlocId(Long invlocId) {
        this.invlocId = invlocId;
    }

    public Integer getInvNum() {
        return invNum;
    }

    public void setInvNum(Integer invNum) {
        this.invNum = invNum;
    }

    public String getInvlocVin() {
        return invlocVin;
    }

    public void setInvlocVin(String invlocVin) {
        this.invlocVin = invlocVin;
    }

    public String getInvlocZoneCode() {
        return invlocZoneCode;
    }

    public void setInvlocZoneCode(String invlocZoneCode) {
        this.invlocZoneCode = invlocZoneCode;
    }

    public String getInvlocZoneName() {
        return invlocZoneName;
    }

    public void setInvlocZoneName(String invlocZoneName) {
        this.invlocZoneName = invlocZoneName;
    }

    public String getInvlocLocCode() {
        return invlocLocCode;
    }

    public void setInvlocLocCode(String invlocLocCode) {
        this.invlocLocCode = invlocLocCode;
    }

    public String getInvlocLocName() {
        return invlocLocName;
    }

    public void setInvlocLocName(String invlocLocName) {
        this.invlocLocName = invlocLocName;
    }

    public String getInvlocWhCode() {
        return invlocWhCode;
    }

    public void setInvlocWhCode(String invlocWhCode) {
        this.invlocWhCode = invlocWhCode;
    }

    public String getInvlocWhName() {
        return invlocWhName;
    }

    public void setInvlocWhName(String invlocWhName) {
        this.invlocWhName = invlocWhName;
    }

    public String getInvlocWaybillNo() {
        return invlocWaybillNo;
    }

    public void setInvlocWaybillNo(String invlocWaybillNo) {
        this.invlocWaybillNo = invlocWaybillNo;
    }

    @Override
    public String toString() {
        return "InventoryDetailDTO{" +
                "invId=" + invId +
                ", invVehicleSeriesName='" + invVehicleSeriesName + '\'' +
                ", invVehicleSpecCode='" + invVehicleSpecCode + '\'' +
                ", invVehicleSpecName='" + invVehicleSpecName + '\'' +
                ", invVehicleSpecDesc='" + invVehicleSpecDesc + '\'' +
                ", invMaterialCode='" + invMaterialCode + '\'' +
                ", invMaterialName='" + invMaterialName + '\'' +
                ", invColor='" + invColor + '\'' +
                ", invlocId=" + invlocId +
                ", invNum=" + invNum +
                ", invlocVin='" + invlocVin + '\'' +
                ", invlocZoneCode='" + invlocZoneCode + '\'' +
                ", invlocZoneName='" + invlocZoneName + '\'' +
                ", invlocLocCode='" + invlocLocCode + '\'' +
                ", invlocLocName='" + invlocLocName + '\'' +
                ", invlocWhCode='" + invlocWhCode + '\'' +
                ", invlocWhName='" + invlocWhName + '\'' +
                ", invlocWaybillNo='" + invlocWaybillNo + '\'' +
                '}';
    }
}
