package com.unlcn.ils.wms.base.mapper.extmapper;

import com.unlcn.ils.wms.base.model.stock.WmsBorrowCarDetail;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

public interface WmsBorrowCarDetailExtMapper {

    /**
     * 查询未完成的车型、颜色借车单车辆
     * @param bdCarScname
     * @param bdCarColorname
     * @return
     */
    List<Integer> queryNumBySpecAndColor(@Param("bdCarScname") String bdCarScname, @Param("bdCarColorname") String bdCarColorname);

    /**
     * 批量插入
     * @param wmsBorrowCarDetailList
     */
    void batchInsert(@Param("wmsBorrowCarDetailList") List<WmsBorrowCarDetail> wmsBorrowCarDetailList);

    /**
     * 根据参数查询
     * @param paramsMap
     * @return
     */
    List<WmsBorrowCarDetail> findParams(@Param("paramsMap") Map<String, Object> paramsMap);

    /**
     * 删除明细
     * @param bdBorrowIds
     * @param bdReturnIds
     */
    void deleteDetail(@Param("bdBorrowIds") List<Long> bdBorrowIds, @Param("bdReturnIds") List<Long> bdReturnIds);

    /**
     * 删除
     * @param bdId
     */
    void delete(@Param("bdId") Long bdId);

    /**
     * 清除还车单
     * @param bdReturnIds
     */
    void clearReturn(@Param("bdReturnIds") List<Long> bdReturnIds, @Param("bdVin") String bdVin);

    /**
     * 根据借车单号更新车辆归还状态
     * @param bdReturnId
     * @param borrowNo
     */
    void updateStatusByBorrowNo(@Param("bdReturnId") Long bdReturnId, @Param("borrowNo") String borrowNo);

    /**
     * 根据车架号更新车辆归还状态
     * @param bdReturnId
     * @param bdVins
     */
    void updateStatusByVin(@Param("bdReturnId") Long bdReturnId, @Param("bdVins") List<String> bdVins);
}