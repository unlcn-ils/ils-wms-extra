package com.unlcn.ils.wms.backend.service.bigscreen;

import com.unlcn.ils.wms.backend.dto.bigscreenDTO.ShipmentDataDTO;

import java.util.List;

/**
 * Created by lenovo on 2017/11/8.
 */
public interface FrontShipmentService {
    /**
     * 前端发运推移图统计
     * @return
     */
    List<ShipmentDataDTO> shipmentChartCount();

    void addFrontShipmentData();
}
