package com.unlcn.ils.wms.backend.bo.baseDataBO;

import java.io.Serializable;

public class CustomerNameList implements Serializable {
    private CustomerName customerId;

    public CustomerName getCustomerId() {
        return customerId;
    }

    public void setCustomerId(CustomerName customerId) {
        this.customerId = customerId;
    }
}
