package com.unlcn.ils.wms.backend.bo.pickup;

/**
 * Created by zhiche024 on 2017/7/25.
 */
public class TmsCloseErrorBO {
    private Long infoId;
    private String excpTypes;
    private int status;

    public Long getInfoId() {
        return infoId;
    }

    public void setInfoId(Long infoId) {
        this.infoId = infoId;
    }

    public String getExcpTypes() {
        return excpTypes;
    }

    public void setExcpTypes(String excpTypes) {
        this.excpTypes = excpTypes;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }
}
