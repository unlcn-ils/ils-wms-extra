package com.unlcn.ils.wms.backend.bo.inboundBO;

import java.util.Date;

/**
 * Created by DELL on 2017/9/21.
 */
public class WmsInboundAsnBO {

    /**
     * 主键ID
     */
    private Long asnId;

    /**
     * CODE
     */
    private String asnCode;

    /**
     * 状态
     */
    private String asnStatus;

    /**
     * 车架号
     */
    private String asnVin;

    /**
     * 车型code
     */
    private String asnVehicleSpecCode;

    /**
     * 车型名称
     */
    private String asnVehicleSpecName;

    /**
     * 物料代码
     */
    private String asnMaterialCode;

    /**
     * 物料名称
     */
    private String asnMaterialName;

    /**
     * 颜色代码
     */
    private String asnColourCode;

    /**
     * 颜色名称
     */
    private String asnColourName;

    /**
     * 货主代码
     */
    private String asnCustomerCode;

    /**
     * 货主名称
     */
    private String asnCustomerName;

    /**
     * 生成日期
     */
    private String asnProductionDate;

    /**
     * 发动机号
     */
    private String asnEngine;

    /**
     * 合格证
     */
    private String asnCertificate;

    /**
     * 下线日期
     */
    private String asnOfflineDate;

    /**
     * 变速箱
     */
    private String asnGearbox;

    /**
     * 备注
     */
    private String asnRemark;

    /**
     * 检验结果
     */
    private String asnCheckResult;

    /**
     * 检验描述
     */
    private String asnCheckDesc;

    /**
     * 验车交接时间
     */
    private String asnCheckTransferDate;

    /**
     * 创建人
     */
    private String createUserName;

    /**
     * 创建日期
     */
    private Date gmtCreate;

    /**
     * 修改人
     */
    private String modifyUserName;

    /**
     * 修改日期
     */
    private Date gmtUpdate;

    /**
     * 逻辑删除
     */
    private Byte isDeleted;

    /**
     * 下线开始日期
     */
    private String asnOfflineDateStart;

    /**
     * 下线结束日期
     */
    private String asnOfflineDateEnd;

    /**
     * 验车交接开始时间
     */
    private String asnCheckTransferDateStart;

    /**
     * 验车交接结束时间
     */
    private String asnCheckTransferDateEnd;

    public Long getAsnId() {
        return asnId;
    }

    public void setAsnId(Long asnId) {
        this.asnId = asnId;
    }

    public String getAsnCode() {
        return asnCode;
    }

    public void setAsnCode(String asnCode) {
        this.asnCode = asnCode;
    }

    public String getAsnStatus() {
        return asnStatus;
    }

    public void setAsnStatus(String asnStatus) {
        this.asnStatus = asnStatus;
    }

    public String getAsnVin() {
        return asnVin;
    }

    public void setAsnVin(String asnVin) {
        this.asnVin = asnVin;
    }

    public String getAsnVehicleSpecCode() {
        return asnVehicleSpecCode;
    }

    public void setAsnVehicleSpecCode(String asnVehicleSpecCode) {
        this.asnVehicleSpecCode = asnVehicleSpecCode;
    }

    public String getAsnVehicleSpecName() {
        return asnVehicleSpecName;
    }

    public void setAsnVehicleSpecName(String asnVehicleSpecName) {
        this.asnVehicleSpecName = asnVehicleSpecName;
    }

    public String getAsnMaterialCode() {
        return asnMaterialCode;
    }

    public void setAsnMaterialCode(String asnMaterialCode) {
        this.asnMaterialCode = asnMaterialCode;
    }

    public String getAsnMaterialName() {
        return asnMaterialName;
    }

    public void setAsnMaterialName(String asnMaterialName) {
        this.asnMaterialName = asnMaterialName;
    }

    public String getAsnColourCode() {
        return asnColourCode;
    }

    public void setAsnColourCode(String asnColourCode) {
        this.asnColourCode = asnColourCode;
    }

    public String getAsnColourName() {
        return asnColourName;
    }

    public void setAsnColourName(String asnColourName) {
        this.asnColourName = asnColourName;
    }

    public String getAsnCustomerCode() {
        return asnCustomerCode;
    }

    public void setAsnCustomerCode(String asnCustomerCode) {
        this.asnCustomerCode = asnCustomerCode;
    }

    public String getAsnCustomerName() {
        return asnCustomerName;
    }

    public void setAsnCustomerName(String asnCustomerName) {
        this.asnCustomerName = asnCustomerName;
    }

    public String getAsnProductionDate() {
        return asnProductionDate;
    }

    public void setAsnProductionDate(String asnProductionDate) {
        this.asnProductionDate = asnProductionDate;
    }

    public String getAsnEngine() {
        return asnEngine;
    }

    public void setAsnEngine(String asnEngine) {
        this.asnEngine = asnEngine;
    }

    public String getAsnCertificate() {
        return asnCertificate;
    }

    public void setAsnCertificate(String asnCertificate) {
        this.asnCertificate = asnCertificate;
    }

    public String getAsnOfflineDate() {
        return asnOfflineDate;
    }

    public void setAsnOfflineDate(String asnOfflineDate) {
        this.asnOfflineDate = asnOfflineDate;
    }

    public String getAsnGearbox() {
        return asnGearbox;
    }

    public void setAsnGearbox(String asnGearbox) {
        this.asnGearbox = asnGearbox;
    }

    public String getAsnRemark() {
        return asnRemark;
    }

    public void setAsnRemark(String asnRemark) {
        this.asnRemark = asnRemark;
    }

    public String getAsnCheckResult() {
        return asnCheckResult;
    }

    public void setAsnCheckResult(String asnCheckResult) {
        this.asnCheckResult = asnCheckResult;
    }

    public String getAsnCheckDesc() {
        return asnCheckDesc;
    }

    public void setAsnCheckDesc(String asnCheckDesc) {
        this.asnCheckDesc = asnCheckDesc;
    }

    public String getAsnCheckTransferDate() {
        return asnCheckTransferDate;
    }

    public void setAsnCheckTransferDate(String asnCheckTransferDate) {
        this.asnCheckTransferDate = asnCheckTransferDate;
    }

    public String getCreateUserName() {
        return createUserName;
    }

    public void setCreateUserName(String createUserName) {
        this.createUserName = createUserName;
    }

    public Date getGmtCreate() {
        return gmtCreate;
    }

    public void setGmtCreate(Date gmtCreate) {
        this.gmtCreate = gmtCreate;
    }

    public String getModifyUserName() {
        return modifyUserName;
    }

    public void setModifyUserName(String modifyUserName) {
        this.modifyUserName = modifyUserName;
    }

    public Date getGmtUpdate() {
        return gmtUpdate;
    }

    public void setGmtUpdate(Date gmtUpdate) {
        this.gmtUpdate = gmtUpdate;
    }

    public Byte getIsDeleted() {
        return isDeleted;
    }

    public void setIsDeleted(Byte isDeleted) {
        this.isDeleted = isDeleted;
    }

    public String getAsnOfflineDateStart() {
        return asnOfflineDateStart;
    }

    public void setAsnOfflineDateStart(String asnOfflineDateStart) {
        this.asnOfflineDateStart = asnOfflineDateStart;
    }

    public String getAsnOfflineDateEnd() {
        return asnOfflineDateEnd;
    }

    public void setAsnOfflineDateEnd(String asnOfflineDateEnd) {
        this.asnOfflineDateEnd = asnOfflineDateEnd;
    }

    public String getAsnCheckTransferDateStart() {
        return asnCheckTransferDateStart;
    }

    public void setAsnCheckTransferDateStart(String asnCheckTransferDateStart) {
        this.asnCheckTransferDateStart = asnCheckTransferDateStart;
    }

    public String getAsnCheckTransferDateEnd() {
        return asnCheckTransferDateEnd;
    }

    public void setAsnCheckTransferDateEnd(String asnCheckTransferDateEnd) {
        this.asnCheckTransferDateEnd = asnCheckTransferDateEnd;
    }

    @Override
    public String toString() {
        return "WmsInboundAsnBO{" +
                "asnId=" + asnId +
                ", asnCode='" + asnCode + '\'' +
                ", asnStatus='" + asnStatus + '\'' +
                ", asnVin='" + asnVin + '\'' +
                ", asnVehicleSpecCode='" + asnVehicleSpecCode + '\'' +
                ", asnVehicleSpecName='" + asnVehicleSpecName + '\'' +
                ", asnMaterialCode='" + asnMaterialCode + '\'' +
                ", asnMaterialName='" + asnMaterialName + '\'' +
                ", asnColourCode='" + asnColourCode + '\'' +
                ", asnColourName='" + asnColourName + '\'' +
                ", asnCustomerCode='" + asnCustomerCode + '\'' +
                ", asnCustomerName='" + asnCustomerName + '\'' +
                ", asnProductionDate='" + asnProductionDate + '\'' +
                ", asnEngine='" + asnEngine + '\'' +
                ", asnCertificate='" + asnCertificate + '\'' +
                ", asnOfflineDate='" + asnOfflineDate + '\'' +
                ", asnGearbox='" + asnGearbox + '\'' +
                ", asnRemark='" + asnRemark + '\'' +
                ", asnCheckResult='" + asnCheckResult + '\'' +
                ", asnCheckDesc='" + asnCheckDesc + '\'' +
                ", asnCheckTransferDate='" + asnCheckTransferDate + '\'' +
                ", createUserName='" + createUserName + '\'' +
                ", gmtCreate=" + gmtCreate +
                ", modifyUserName='" + modifyUserName + '\'' +
                ", gmtUpdate=" + gmtUpdate +
                ", isDeleted=" + isDeleted +
                ", asnOfflineDateStart='" + asnOfflineDateStart + '\'' +
                ", asnOfflineDateEnd='" + asnOfflineDateEnd + '\'' +
                ", asnCheckTransferDateStart='" + asnCheckTransferDateStart + '\'' +
                ", asnCheckTransferDateEnd='" + asnCheckTransferDateEnd + '\'' +
                '}';
    }
}
