package com.unlcn.ils.wms.backend.service.inspectApp;


import com.unlcn.ils.wms.base.model.inspectApp.TmsInspectExcp;

import java.util.List;

/**
 *
 *
 * Created by houjianhui on 2017/7/24.
 */
public interface TmsExcpService {


    /**
     * 根据运单id查询异常列表
     * @param inspectId
     * @return
     */
    List<TmsInspectExcp> getInspectExcpListByInspectId(Long inspectId);

}
