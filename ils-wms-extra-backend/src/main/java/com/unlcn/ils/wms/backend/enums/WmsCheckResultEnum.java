package com.unlcn.ils.wms.backend.enums;

public enum WmsCheckResultEnum {
    WAYBILL_QUALIFY(20, "10"),
    WAYBILL_EXCP(30, "20"),
    CHECK_EXCP(70, "20");//异常登记后的状态

    private final int value;
    private final String text;

    WmsCheckResultEnum(int value, String text) {
        this.value = value;
        this.text = text;
    }

    public int getValue() {
        return value;
    }

    public String getText() {
        return text;
    }

    public static WmsCheckResultEnum getByValue(int value) {
        for (WmsCheckResultEnum temp : WmsCheckResultEnum.values()) {
            if (temp.getValue() == value) {
                return temp;
            }
        }
        return null;
    }
}
