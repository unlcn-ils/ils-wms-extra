package com.unlcn.ils.wms.base.mapper.extmapper;

import com.unlcn.ils.wms.base.dto.ReturnCarLineDTO;
import com.unlcn.ils.wms.base.dto.ReturnDetailDTO;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

public interface WmsReturnCarExtMapper {

    /**
     * 还车登记列表统计
     * @param paramsMap
     * @return
     */
    Integer returnLineCount(@Param("paramsMap") Map<String, Object> paramsMap);

    /**
     * 还车登记列表
     * @param paramsMap
     * @return
     */
    List<ReturnCarLineDTO> returnLine(@Param("paramsMap") Map<String, Object> paramsMap);

    /**
     * 查询还车单详细信息
     * @param rcId
     * @return
     */
    List<ReturnDetailDTO> queryReturnDetail(@Param("rcId") Long rcId);

    /**
     * 删除还车单
     * @param rcIds
     */
    void deleteReturn(@Param("rcIds") List<Long> rcIds);

    /**
     * 查询最大主键值
     * @return
     */
    Integer selectMaxId();
}