package com.unlcn.ils.wms.base.dto;

import java.io.Serializable;
import java.util.Date;

public class WmsFetchInboundFromTMSDTO implements Serializable {

    private Long id;
    private String waybillNo;
    private String dispatchNo;
    private String customerOrderNo;
    private String vin;
    private String customerName;
    private String supplierName;
    private String origin;
    private String dest;
    private String supplierVehiclePlate;
    private String supplierDriverName;
    private String supplierDriverMobile;
    private String vehicleStyleDesc;
    private String vehicleStyle;
    private Date dtshipTime;
    private String routeEnd;
    private String whName;
    private String whCode;
    private String createUserName;
    private Date gmtCreate;
    private String modifyUserName;
    private Date gmtUpdate;
    private String atSendBusinessFlag;
    private String atSysSource;
    private Byte isDelete;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getWaybillNo() {
        return waybillNo;
    }

    public void setWaybillNo(String waybillNo) {
        this.waybillNo = waybillNo;
    }

    public String getDispatchNo() {
        return dispatchNo;
    }

    public void setDispatchNo(String dispatchNo) {
        this.dispatchNo = dispatchNo;
    }

    public String getCustomerOrderNo() {
        return customerOrderNo;
    }

    public void setCustomerOrderNo(String customerOrderNo) {
        this.customerOrderNo = customerOrderNo;
    }

    public String getVin() {
        return vin;
    }

    public void setVin(String vin) {
        this.vin = vin;
    }

    public String getCustomerName() {
        return customerName;
    }

    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }

    public String getSupplierName() {
        return supplierName;
    }

    public void setSupplierName(String supplierName) {
        this.supplierName = supplierName;
    }

    public String getOrigin() {
        return origin;
    }

    public void setOrigin(String origin) {
        this.origin = origin;
    }

    public String getDest() {
        return dest;
    }

    public void setDest(String dest) {
        this.dest = dest;
    }

    public String getSupplierVehiclePlate() {
        return supplierVehiclePlate;
    }

    public void setSupplierVehiclePlate(String supplierVehiclePlate) {
        this.supplierVehiclePlate = supplierVehiclePlate;
    }

    public String getSupplierDriverName() {
        return supplierDriverName;
    }

    public void setSupplierDriverName(String supplierDriverName) {
        this.supplierDriverName = supplierDriverName;
    }

    public String getSupplierDriverMobile() {
        return supplierDriverMobile;
    }

    public void setSupplierDriverMobile(String supplierDriverMobile) {
        this.supplierDriverMobile = supplierDriverMobile;
    }

    public String getVehicleStyleDesc() {
        return vehicleStyleDesc;
    }

    public void setVehicleStyleDesc(String vehicleStyleDesc) {
        this.vehicleStyleDesc = vehicleStyleDesc;
    }

    public String getVehicleStyle() {
        return vehicleStyle;
    }

    public void setVehicleStyle(String vehicleStyle) {
        this.vehicleStyle = vehicleStyle;
    }

    public Date getDtshipTime() {
        return dtshipTime;
    }

    public void setDtshipTime(Date dtshipTime) {
        this.dtshipTime = dtshipTime;
    }

    public String getRouteEnd() {
        return routeEnd;
    }

    public void setRouteEnd(String routeEnd) {
        this.routeEnd = routeEnd;
    }

    public String getWhName() {
        return whName;
    }

    public void setWhName(String whName) {
        this.whName = whName;
    }

    public String getWhCode() {
        return whCode;
    }

    public void setWhCode(String whCode) {
        this.whCode = whCode;
    }

    public String getCreateUserName() {
        return createUserName;
    }

    public void setCreateUserName(String createUserName) {
        this.createUserName = createUserName;
    }

    public Date getGmtCreate() {
        return gmtCreate;
    }

    public void setGmtCreate(Date gmtCreate) {
        this.gmtCreate = gmtCreate;
    }

    public String getModifyUserName() {
        return modifyUserName;
    }

    public void setModifyUserName(String modifyUserName) {
        this.modifyUserName = modifyUserName;
    }

    public Date getGmtUpdate() {
        return gmtUpdate;
    }

    public void setGmtUpdate(Date gmtUpdate) {
        this.gmtUpdate = gmtUpdate;
    }

    public String getAtSendBusinessFlag() {
        return atSendBusinessFlag;
    }

    public void setAtSendBusinessFlag(String atSendBusinessFlag) {
        this.atSendBusinessFlag = atSendBusinessFlag;
    }

    public String getAtSysSource() {
        return atSysSource;
    }

    public void setAtSysSource(String atSysSource) {
        this.atSysSource = atSysSource;
    }

    public Byte getIsDelete() {
        return isDelete;
    }

    public void setIsDelete(Byte isDelete) {
        this.isDelete = isDelete;
    }
}
