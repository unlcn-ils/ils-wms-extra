package com.unlcn.ils.wms.app.controller.tms;

import cn.huiyunche.commons.domain.ResultDTO;
import cn.huiyunche.commons.exception.BusinessException;
import com.unlcn.ils.wms.backend.service.inspectApp.WmsRepairInspectService;
import com.unlcn.ils.wms.base.dto.WmsRepairInspectDTO;
import com.unlcn.ils.wms.base.dto.WmsRepairInspectPlaceDTO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.ArrayList;

/**
 * 维修再验车业务表现层
 */
@Controller
@RequestMapping(value = "/warehousing")
@ResponseBody
public class WmsRepairInspectController {

    public Logger logger = LoggerFactory.getLogger(WmsRepairInspectController.class);

    @Autowired
    private WmsRepairInspectService wmsRepairInspectService;

    /**
     * 维修信息抓取
     */
    @RequestMapping(value = "/getRepairInfo", method = RequestMethod.POST)
    public ResultDTO<Object> getRepairInfo(WmsRepairInspectDTO dto) {
        logger.info("WmsRepairInspectController.getRepairInfo params:{}", dto);
        ResultDTO<Object> result = new ResultDTO<>(true, null, "查询成功!");
        try {
            result.setData(wmsRepairInspectService.getRepairInfo(dto));
        } catch (BusinessException e) {
            logger.error("WmsRepairInspectController.getRepairInfo error", e);
            result.setData(null);
            result.setSuccess(false);
            result.setMessage(e.getMessage());
        } catch (Exception ex) {
            logger.error("WmsRepairInspectController.getRepairInfo error", ex);
            result.setData(null);
            result.setSuccess(false);
            result.setMessage("查询异常!");
        }
        return result;
    }


    /**
     * 获取区域的异常信息
     *
     */
    @RequestMapping(value = "/getExcpInfoByPostionId", method = RequestMethod.POST)
    @ResponseBody
    public ResultDTO<Object> getExcpInfoByPositionId(WmsRepairInspectDTO dto) {
        logger.info("WmsRepairInspectController.getExcpInfoByPositionId param:{}.", dto);
        ResultDTO<Object> result = new ResultDTO<>(true, null, "查询成功");
        try {
            result.setData(wmsRepairInspectService.getExcpInfoByPositionId(dto));
        } catch (BusinessException e) {
            logger.error("WmsRepairInspectController.getExcpInfoByPositionId error: {}", e);
            result.setMessage(e.getMessage());
            result.setSuccess(false);
            result.setData(null);
        } catch (Exception ex) {
            logger.error("WmsRepairInspectController.getExcpInfoByPositionId error: {}", ex);
            result.setMessage("查询异常");
            result.setSuccess(false);
            result.setData(null);
        }
        return result;
    }

    /**
     * 获取缺件的异常信息
     *
     */
    @RequestMapping(value = "/getRpComponentMissInfo", method = RequestMethod.POST)
    @ResponseBody
    public ResultDTO<Object> getRpComponentMissInfo(WmsRepairInspectDTO dto) {
        logger.info("WmsRepairInspectController.getRpComponentMissInfo param:{}.", dto);
        ResultDTO<Object> result = new ResultDTO<>(true, null, "查询成功");
        try {
            result.setData(wmsRepairInspectService.getRpComponentMissInfo(dto));
        } catch (BusinessException e) {
            logger.error("WmsRepairInspectController.getRpComponentMissInfo error: {}", e);
            result.setMessage(e.getMessage());
            result.setSuccess(false);
            result.setData(null);
        } catch (Exception ex) {
            logger.error("WmsRepairInspectController.getRpComponentMissInfo error: {}", ex);
            result.setMessage("查询异常");
            result.setSuccess(false);
            result.setData(null);
        }
        return result;
    }


    /**
     * 维修合格接口
     */
    @RequestMapping(value = "/repairedPass", method = RequestMethod.POST)
    public ResultDTO<Object> repairedPass(WmsRepairInspectDTO dto) {
        logger.info("WmsRepairInspectController.repairedPass params:{}", dto);
        ResultDTO<Object> result = new ResultDTO<>(true, null, "操作成功!");
        try {
            wmsRepairInspectService.updateRepairPass(dto);
        } catch (BusinessException e) {
            logger.error("WmsRepairInspectController.repairedPass error", e);
            result.setData(null);
            result.setSuccess(false);
            result.setMessage(e.getMessage());
        } catch (Exception ex) {
            logger.error("WmsRepairInspectController.repairedPass error", ex);
            result.setData(null);
            result.setSuccess(false);
            result.setMessage("操作异常!");
        }
        return result;
    }

    /**
     * 维修不合格接口
     */
    @RequestMapping(value = "/repairedExcp", method = RequestMethod.POST)
    public ResultDTO<Object> repairedExcp(WmsRepairInspectDTO dto) {
        logger.info("WmsRepairInspectController.repairedExcp params:{}", dto);
        ResultDTO<Object> result = new ResultDTO<>(true, null, "操作成功!");
        try {
            wmsRepairInspectService.updateRepairExcp(dto);
        } catch (BusinessException e) {
            logger.error("WmsRepairInspectController.repairedExcp error", e);
            result.setData(null);
            result.setSuccess(false);
            result.setMessage(e.getMessage());
        } catch (Exception ex) {
            logger.error("WmsRepairInspectController.repairedExcp error", ex);
            result.setData(null);
            result.setSuccess(false);
            result.setMessage("操作异常!");
        }
        return result;
    }

    /**
     * 维修不合格提示其再验车列表
     */
    @RequestMapping(value = "/getRepairInspectPlace", method = RequestMethod.GET)
    public ResultDTO<Object> repairInspectPlace(String rpId, String whCode) {
        logger.info("WmsRepairInspectController.repairInspectPlace params:{}");
        ResultDTO<Object> result = new ResultDTO<>(true, null, "查询成功!");
        try {
            ArrayList<WmsRepairInspectPlaceDTO> repairInspectPlace = wmsRepairInspectService.getRepairInspectPlace(rpId);
            result.setData(repairInspectPlace);
        } catch (BusinessException e) {
            logger.error("WmsRepairInspectController.repairInspectPlace error", e);
            result.setData(null);
            result.setSuccess(false);
            result.setMessage(e.getMessage());
        } catch (Exception ex) {
            logger.error("WmsRepairInspectController.repairInspectPlace error", ex);
            result.setData(null);
            result.setSuccess(false);
            result.setMessage("查询异常!");
        }
        return result;
    }

}
