package com.unlcn.ils.wms.web.vo.outbound;

import cn.huiyunche.commons.domain.PageVo;

import java.io.Serializable;

public class GetShipmentPLanListVO extends PageVo implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private String spWhCode;

	/**
	 * 主板单号
	 */
	private String spGroupBoardNo;

	/**
	 * 指令号
	 */
	private String spDispatchNo;

	private String spSendBusinessFlag;

	private String spOrderNo;

	private String spCustomerName;

	private String spCarrier;

	private String spOrigin;

	private String spDest;

	private String gmtCreateBegin;

	private String gmtCreateEnd;

	public String getSpWhCode() {
		return spWhCode;
	}

	public void setSpWhCode(String spWhCode) {
		this.spWhCode = spWhCode;
	}

	public String getSpGroupBoardNo() {
		return spGroupBoardNo;
	}

	public void setSpGroupBoardNo(String spGroupBoardNo) {
		this.spGroupBoardNo = spGroupBoardNo;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public String getSpSendBusinessFlag() {
		return spSendBusinessFlag;
	}

	public void setSpSendBusinessFlag(String spSendBusinessFlag) {
		this.spSendBusinessFlag = spSendBusinessFlag;
	}

	public String getSpOrderNo() {
		return spOrderNo;
	}

	public void setSpOrderNo(String spOrderNo) {
		this.spOrderNo = spOrderNo;
	}

	public String getSpCustomerName() {
		return spCustomerName;
	}

	public void setSpCustomerName(String spCustomerName) {
		this.spCustomerName = spCustomerName;
	}

	public String getSpCarrier() {
		return spCarrier;
	}

	public void setSpCarrier(String spCarrier) {
		this.spCarrier = spCarrier;
	}

	public String getSpOrigin() {
		return spOrigin;
	}

	public void setSpOrigin(String spOrigin) {
		this.spOrigin = spOrigin;
	}

	public String getSpDest() {
		return spDest;
	}

	public void setSpDest(String spDest) {
		this.spDest = spDest;
	}

	public String getGmtCreateBegin() {
		return gmtCreateBegin;
	}

	public void setGmtCreateBegin(String gmtCreateBegin) {
		this.gmtCreateBegin = gmtCreateBegin;
	}

	public String getGmtCreateEnd() {
		return gmtCreateEnd;
	}

	public void setGmtCreateEnd(String gmtCreateEnd) {
		this.gmtCreateEnd = gmtCreateEnd;
	}

	public String getSpDispatchNo() {
		return spDispatchNo;
	}

	public void setSpDispatchNo(String spDispatchNo) {
		this.spDispatchNo = spDispatchNo;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("GetShipmentPLanListVO [spWhCode=");
		builder.append(spWhCode);
		builder.append(", spGroupBoardNo=");
		builder.append(spGroupBoardNo);
		builder.append(", spSendBusinessFlag=");
		builder.append(spSendBusinessFlag);
		builder.append(", spOrderNo=");
		builder.append(spOrderNo);
		builder.append(", spCustomerName=");
		builder.append(spCustomerName);
		builder.append(", spCarrier=");
		builder.append(spCarrier);
		builder.append(", spOrigin=");
		builder.append(spOrigin);
		builder.append(", spDest=");
		builder.append(spDest);
		builder.append(", gmtCreateBegin=");
		builder.append(gmtCreateBegin);
		builder.append(", gmtCreateEnd=");
		builder.append(gmtCreateEnd);
		builder.append("]");
		return builder.toString();
	}

}
