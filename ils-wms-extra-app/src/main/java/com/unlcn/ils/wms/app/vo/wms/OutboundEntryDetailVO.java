package com.unlcn.ils.wms.app.vo.wms;

/**
 * Created by houjianhui on 2017/5/8.
 */
public class OutboundEntryDetailVO {

    private String vehicleTypeCode;
    private String vin;
    private String plateNumber;

    public String getVehicleTypeCode() {
        return vehicleTypeCode;
    }

    public void setVehicleTypeCode(String vehicleTypeCode) {
        this.vehicleTypeCode = vehicleTypeCode;
    }

    public String getVin() {
        return vin;
    }

    public void setVin(String vin) {
        this.vin = vin;
    }

    public String getPlateNumber() {
        return plateNumber;
    }

    public void setPlateNumber(String plateNumber) {
        this.plateNumber = plateNumber;
    }

    @Override
    public String toString() {
        return "OutboundEntryDetailVO{" +
                "vehicleTypeCode='" + vehicleTypeCode + '\'' +
                ", vin='" + vin + '\'' +
                ", plateNumber='" + plateNumber + '\'' +
                '}';
    }
}
