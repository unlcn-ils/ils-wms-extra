package com.unlcn.ils.wms.app.vo.wms;

import java.io.Serializable;

public class AppOutboundTaskVO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private String otWhCode; // 仓库代码

	private String otDriver;

	public String getOtWhCode() {
		return otWhCode;
	}

	public void setOtWhCode(String otWhCode) {
		this.otWhCode = otWhCode;
	}

	public String getOtDriver() {
		return otDriver;
	}

	public void setOtDriver(String otDriver) {
		this.otDriver = otDriver;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("AppOutboundTaskVO [otWhCode=");
		builder.append(otWhCode);
		builder.append(", otDriver=");
		builder.append(otDriver);
		builder.append("]");
		return builder.toString();
	}

}
