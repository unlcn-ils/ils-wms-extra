package com.unlcn.ils.wms.base.mapper.extmapper;

import com.unlcn.ils.wms.base.model.bigscreen.BiInOutDrayHead;

import java.util.List;
import java.util.Map;

public interface BiInOutDrayHeadExtMapper {
    /**
     * 板车出入计划图统计
     * @return
     */
    List<BiInOutDrayHead> inOutDrayChartCount();

    void updateByStatVersion(Map<String,Object> paramMap);
}