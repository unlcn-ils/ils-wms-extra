package com.unlcn.ils.wms.backend.enums;

public enum WmsOutboundTaskOutboundFlagEnum {
    NOT_OUTBOUND((byte) 0, "未发运"),
    HAS_OUTBOUND((byte) 1, "已发运"),
    GATE_OUT((byte) 2, "已出闸");

    private final byte value;
    private final String text;

    WmsOutboundTaskOutboundFlagEnum(byte value, String text) {
        this.value = value;
        this.text = text;
    }

    public byte getValue() {
        return value;
    }

    public String getText() {
        return text;
    }

    public static WmsOutboundTaskOutboundFlagEnum getByValue(byte value) {
        for (WmsOutboundTaskOutboundFlagEnum temp : WmsOutboundTaskOutboundFlagEnum.values()) {
            if (temp.getValue() == value) {
                return temp;
            }
        }
        return null;
    }

}
