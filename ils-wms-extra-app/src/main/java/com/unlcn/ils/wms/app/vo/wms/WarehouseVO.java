package com.unlcn.ils.wms.app.vo.wms;

/**
 * Created by houjianhui on 2017/5/7.
 */
public class WarehouseVO {
    private Integer id;
    private String warehouseNo;
    private String warehouseName;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getWarehouseNo() {
        return warehouseNo;
    }

    public void setWarehouseNo(String warehouseNo) {
        this.warehouseNo = warehouseNo;
    }

    public String getWarehouseName() {
        return warehouseName;
    }

    public void setWarehouseName(String warehouseName) {
        this.warehouseName = warehouseName;
    }

    @Override
    public String toString() {
        return "WarehouseVO{" +
                "id=" + id +
                ", warehouseNo='" + warehouseNo + '\'' +
                ", warehouseName='" + warehouseName + '\'' +
                '}';
    }
}
