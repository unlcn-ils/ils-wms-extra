package com.unlcn.ils.wms.web.controller.outbound;

import cn.huiyunche.commons.domain.ResultDTO;
import cn.huiyunche.commons.exception.BusinessException;
import com.unlcn.ils.wms.backend.bo.outboundBO.WmsShipmentPlanRejectBO;
import com.unlcn.ils.wms.backend.enums.WarehouseEnum;
import com.unlcn.ils.wms.backend.service.outbound.WmsShipmentPlanRejectService;
import com.unlcn.ils.wms.base.dto.WmsChangePreparaByVinDTO;
import com.unlcn.ils.wms.base.dto.WmsRejectUndealDTO;
import com.unlcn.ils.wms.web.vo.outbound.WmsShipmentPlanRejectVO;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;

/**
 * 发运计划驳回 --君马
 */
@RestController
@RequestMapping("/shipmentReject")
public class WmsShipmentPlanRejectController {

    private Logger logger = LoggerFactory.getLogger(WmsShipmentPlanRejectController.class);

    @Autowired
    private WmsShipmentPlanRejectService shipmentPlanRejectService;

    /**
     * 列表查询
     *
     */
    @RequestMapping(value = "/queryShipmentList", method = RequestMethod.POST)
    public ResultDTO<Object> queryShipmentList(@RequestBody WmsShipmentPlanRejectVO shipmentPlanRejectVO, HttpServletRequest request) {
        logger.info("WmsShipmentPlanRejectController.queryShipmentList shipmentPlanRejectVO: {}", shipmentPlanRejectVO);
        ResultDTO<Object> resultDTO = new ResultDTO<>(true, null, "查询成功");
        try {
            String whCode = request.getHeader("whCode");
            if (StringUtils.isBlank(whCode)) {
                return resultDTO;
            }
            if (!WarehouseEnum.JM_XY.getWhCode().equals(whCode) && !WarehouseEnum.JM_CS.getWhCode().equals(whCode)) {
                return resultDTO;
            }
            WmsShipmentPlanRejectBO rejectBO = new WmsShipmentPlanRejectBO();
            BeanUtils.copyProperties(shipmentPlanRejectVO, rejectBO);
            rejectBO.setWhCode(whCode);
            resultDTO.setData(shipmentPlanRejectService.queryRejectList(rejectBO));
        } catch (BusinessException e) {
            logger.error("WmsShipmentPlanRejectController.queryShipmentList businessException: {}", e);
            resultDTO.setSuccess(false);
            resultDTO.setMessage(e.getMessage());
        } catch (Exception e) {
            logger.error("WmsShipmentPlanRejectController.queryShipmentList error: {}", e);
            resultDTO.setSuccess(false);
            resultDTO.setMessage("查询失败");
        }
        return resultDTO;
    }

    /**
     * 发运计划驳回--针对已生成备料计划无车可换的发运驳回
     *
     * @param spId 发运计划驳回id
     * @return 返回值
     */
    @RequestMapping(value = "/shipmentReject/{spId}", method = RequestMethod.GET)
    public ResultDTO<Object> shipmentReject(@PathVariable Long spId) {
        logger.info("WmsShipmentPlanRejectController.shipmentReject spId: {}", spId);
        ResultDTO<Object> resultDTO = new ResultDTO<>(true, null, "发运计划驳回成功");
        try {
            shipmentPlanRejectService.updateToReject(spId);
        } catch (BusinessException e) {
            logger.error("WmsShipmentPlanRejectController.shipmentReject businessException: {}", e);
            resultDTO.setSuccess(false);
            resultDTO.setMessage(e.getMessage());
        } catch (Exception e) {
            logger.error("WmsShipmentPlanRejectController.shipmentReject error: {}", e);
            resultDTO.setSuccess(false);
            resultDTO.setMessage("发运计划驳回失败");
        }
        return resultDTO;
    }

    /**
     * <p>
     * 2018-3-7 新需求:运计划驳回--针对未生成备料计划的驳回
     * </p>
     *
     * @param spId 发运计划驳回id
     * @return 返回值
     */
    @RequestMapping(value = "/rejectUnDealList/{spId}", method = RequestMethod.GET)
    public ResultDTO<Object> unDealRejectList(@PathVariable Long spId, HttpServletRequest request) {
        logger.info("WmsShipmentPlanRejectController.unDealRejectList params: {}", spId);
        ResultDTO<Object> resultDTO = new ResultDTO<>(true, null, "查询成功");
        try {
            String whCode = request.getHeader("whCode");
            resultDTO.setData(shipmentPlanRejectService.getNotHandleRejectList(spId, whCode));
        } catch (BusinessException e) {
            logger.error("WmsShipmentPlanRejectController.unDealRejectList businessException: {}", e);
            resultDTO.setSuccess(false);
            resultDTO.setMessage(e.getMessage());
        } catch (Exception e) {
            logger.error("WmsShipmentPlanRejectController.unDealRejectList error: {}", e);
            resultDTO.setSuccess(false);
            resultDTO.setMessage("发运计划驳回失败");
        }
        return resultDTO;
    }


    /**
     * <p>
     * 2018-3-7 新需求:运计划驳回--针对未生成备料计划的驳回
     * </p>
     *
     * @param dto 发运计划驳回
     * @return 返回值
     */
    @RequestMapping(value = "/rejectUnDeal", method = RequestMethod.POST)
    public ResultDTO<Object> shipmentReject(@RequestBody WmsRejectUndealDTO dto, HttpServletRequest request) {
        logger.info("WmsShipmentPlanRejectController.shipmentReject params: {}", dto);
        ResultDTO<Object> resultDTO = new ResultDTO<>(true, null, "发运计划驳回成功");
        try {
            String userId = request.getHeader("userId");
            String whCode = request.getHeader("whCode");
            dto.setWhCode(whCode);
            dto.setUserId(userId);
            shipmentPlanRejectService.updateToRejectNotHandle(dto);
        } catch (BusinessException e) {
            logger.error("WmsShipmentPlanRejectController.shipmentReject businessException: {}", e);
            resultDTO.setSuccess(false);
            resultDTO.setMessage(e.getMessage());
        } catch (Exception e) {
            logger.error("WmsShipmentPlanRejectController.shipmentReject error: {}", e);
            resultDTO.setSuccess(false);
            resultDTO.setMessage("发运计划驳回失败");
        }
        return resultDTO;
    }

    /**
     * 详情
     *
     */
    @RequestMapping(value = "/queryDetailList/{spId}", method = RequestMethod.GET)
    public ResultDTO<Object> queryDetailList(@PathVariable Long spId) {
        logger.info("WmsShipmentPlanRejectController.queryDetailList spId: {}", spId);
        ResultDTO<Object> resultDTO = new ResultDTO<>(true, null, "获取详情成功");

        try {
            resultDTO.setData(shipmentPlanRejectService.queryDetailList(spId));
        } catch (BusinessException e) {
            logger.error("WmsShipmentPlanRejectController.queryDetailList businessException: {}", e);
            resultDTO.setSuccess(false);
            resultDTO.setMessage(e.getMessage());
        } catch (Exception e) {
            logger.error("WmsShipmentPlanRejectController.queryDetailList error: {}", e);
            resultDTO.setSuccess(false);
            resultDTO.setMessage("获取详情失败");
        }
        return resultDTO;
    }

    /**
     * 任务取消 --君马功能
     * <p>
     * 2018-1-26 修复任务取消时更新旧的车为正常在库状态
     * </p>
     *
     * @param shipmentPlanRejectVO 参数封装
     * @return 返回值
     */
    @RequestMapping(value = "/cancleTask", method = RequestMethod.POST)
    public ResultDTO<Object> cancelTask(@RequestBody WmsShipmentPlanRejectVO shipmentPlanRejectVO, HttpServletRequest request) {
        logger.info("WmsShipmentPlanRejectController.cancelTask shipmentPlanRejectVO: {}", shipmentPlanRejectVO);
        ResultDTO<Object> resultDTO = new ResultDTO<>(true, null, "任务取消成功");
        try {
            WmsShipmentPlanRejectBO wmsShipmentPlanRejectBO = new WmsShipmentPlanRejectBO();
            BeanUtils.copyProperties(shipmentPlanRejectVO, wmsShipmentPlanRejectBO);
            String whCode = request.getHeader("whCode");
            wmsShipmentPlanRejectBO.setWhCode(whCode);
            shipmentPlanRejectService.updateToCancleTask(wmsShipmentPlanRejectBO);
        } catch (BusinessException e) {
            logger.error("WmsShipmentPlanRejectController.cancelTask businessException: {}", e);
            resultDTO.setSuccess(false);
            resultDTO.setMessage(e.getMessage());
        } catch (Exception e) {
            logger.error("WmsShipmentPlanRejectController.cancelTask error: {}", e);
            resultDTO.setSuccess(false);
            resultDTO.setMessage("任务取消失败");
        }
        return resultDTO;
    }

    /**
     * 备料更换--系统自动根据条件更换(自动换车)
     * <p>
     * bugfix 2018-2-5  更新增加接口表中的换车字段
     * 2018-4-27 bugfix 修复调整因入库通知单功能重构,换车匹配车辆的逻辑
     * </p>
     *
     * @param shipmentPlanRejectVO 参数封装
     * @return 返回值
     */
    @RequestMapping(value = "/changePrepare", method = RequestMethod.POST)
    public ResultDTO<Object> changePrepare(@RequestBody WmsShipmentPlanRejectVO shipmentPlanRejectVO) {
        logger.info("WmsShipmentPlanRejectController.changePrepare shipmentPlanRejectVO: {}", shipmentPlanRejectVO);
        ResultDTO<Object> resultDTO = new ResultDTO<>(true, null, "备料替换成功");

        try {
            WmsShipmentPlanRejectBO wmsShipmentPlanRejectBO = new WmsShipmentPlanRejectBO();
            BeanUtils.copyProperties(shipmentPlanRejectVO, wmsShipmentPlanRejectBO);
            resultDTO.setData(shipmentPlanRejectService.updateToReplace(wmsShipmentPlanRejectBO));
        } catch (BusinessException e) {
            logger.error("WmsShipmentPlanRejectController.changePrepare businessException: {}", e);
            resultDTO.setSuccess(false);
            resultDTO.setMessage(e.getMessage());
        } catch (Exception e) {
            logger.error("WmsShipmentPlanRejectController.changePrepare error: {}", e);
            resultDTO.setSuccess(false);
            resultDTO.setMessage("备料替换失败");
        }
        return resultDTO;
    }

    /**
     * 备料更换--指定车辆进行更换(手动换车)
     *
     * <p>
     * 新需求 2018-3-4  更新指定车辆进行换车
     * 2018-3-12 任务完成得车换车后还原库位
     * 2018-4-27 bugfix 修复调整因入库通知单功能重构,换车匹配车辆的逻辑
     * </p>
     *
     * @param byVinDTO 参数封装
     * @return 返回值
     */
    @RequestMapping(value = "/changeByVin", method = RequestMethod.POST)
    public ResultDTO<Object> updateChangeByVin(@RequestBody WmsChangePreparaByVinDTO byVinDTO, HttpServletRequest request) {
        logger.info("WmsShipmentPlanRejectController.updateChangeByVin param: {}", byVinDTO);
        ResultDTO<Object> resultDTO = new ResultDTO<>(true, null, "备料替换成功");
        try {
            String whCode = request.getHeader("whCode");
            byVinDTO.setWhCode(whCode);
            resultDTO.setData(shipmentPlanRejectService.updateChangeDetailByVin(byVinDTO));
        } catch (BusinessException e) {
            logger.error("WmsShipmentPlanRejectController.updateChangeByVin businessException: {}", e);
            resultDTO.setSuccess(false);
            resultDTO.setMessage(e.getMessage());
        } catch (Exception e) {
            logger.error("WmsShipmentPlanRejectController.updateChangeByVin error: {}", e);
            resultDTO.setSuccess(false);
            resultDTO.setMessage("备料替换失败");
        }
        return resultDTO;
    }
}
