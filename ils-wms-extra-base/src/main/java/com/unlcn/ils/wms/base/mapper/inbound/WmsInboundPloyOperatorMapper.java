package com.unlcn.ils.wms.base.mapper.inbound;

import com.unlcn.ils.wms.base.model.inbound.WmsInboundPloyOperator;
import com.unlcn.ils.wms.base.model.inbound.WmsInboundPloyOperatorExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface WmsInboundPloyOperatorMapper {
    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table wms_inbound_ploy_operator
     *
     * @mbggenerated Wed Aug 30 11:06:18 CST 2017
     */
    int countByExample(WmsInboundPloyOperatorExample example);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table wms_inbound_ploy_operator
     *
     * @mbggenerated Wed Aug 30 11:06:18 CST 2017
     */
    int deleteByExample(WmsInboundPloyOperatorExample example);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table wms_inbound_ploy_operator
     *
     * @mbggenerated Wed Aug 30 11:06:18 CST 2017
     */
    int deleteByPrimaryKey(Long pyoId);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table wms_inbound_ploy_operator
     *
     * @mbggenerated Wed Aug 30 11:06:18 CST 2017
     */
    int insert(WmsInboundPloyOperator record);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table wms_inbound_ploy_operator
     *
     * @mbggenerated Wed Aug 30 11:06:18 CST 2017
     */
    int insertSelective(WmsInboundPloyOperator record);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table wms_inbound_ploy_operator
     *
     * @mbggenerated Wed Aug 30 11:06:18 CST 2017
     */
    List<WmsInboundPloyOperator> selectByExample(WmsInboundPloyOperatorExample example);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table wms_inbound_ploy_operator
     *
     * @mbggenerated Wed Aug 30 11:06:18 CST 2017
     */
    WmsInboundPloyOperator selectByPrimaryKey(Long pyoId);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table wms_inbound_ploy_operator
     *
     * @mbggenerated Wed Aug 30 11:06:18 CST 2017
     */
    int updateByExampleSelective(@Param("record") WmsInboundPloyOperator record, @Param("example") WmsInboundPloyOperatorExample example);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table wms_inbound_ploy_operator
     *
     * @mbggenerated Wed Aug 30 11:06:18 CST 2017
     */
    int updateByExample(@Param("record") WmsInboundPloyOperator record, @Param("example") WmsInboundPloyOperatorExample example);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table wms_inbound_ploy_operator
     *
     * @mbggenerated Wed Aug 30 11:06:18 CST 2017
     */
    int updateByPrimaryKeySelective(WmsInboundPloyOperator record);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table wms_inbound_ploy_operator
     *
     * @mbggenerated Wed Aug 30 11:06:18 CST 2017
     */
    int updateByPrimaryKey(WmsInboundPloyOperator record);
}