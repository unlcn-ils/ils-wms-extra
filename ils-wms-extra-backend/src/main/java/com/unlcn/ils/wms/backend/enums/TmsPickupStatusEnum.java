package com.unlcn.ils.wms.backend.enums;

/**
 * @Author ：Ligl
 * @Date : 2017/9/18.
 */
public enum TmsPickupStatusEnum {

    WAYBILL_INIT(10, "未提车"),
    WAYBILL_ALREADY(20, "已提车"),
    WAYBILL_CANCEL(30, "取消提车");

    private final int value;
    private final String text;

    TmsPickupStatusEnum(int value, String text) {
        this.value = value;
        this.text = text;
    }

    public int getValue() {
        return value;
    }

    public String getText() {
        return text;
    }

    public static TmsPickupStatusEnum getByValue(int value){
        for (TmsPickupStatusEnum temp : TmsPickupStatusEnum.values()) {
            if(temp.getValue() == value){
                return temp;
            }
        }
        return null;
    }
}
