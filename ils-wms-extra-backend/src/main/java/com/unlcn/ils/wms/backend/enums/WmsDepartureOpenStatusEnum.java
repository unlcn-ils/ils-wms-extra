package com.unlcn.ils.wms.backend.enums;

/**
 * @Auther linbao
 * @Date 2017-11-14
 */
public enum WmsDepartureOpenStatusEnum {

    NOT_OPEN((byte)10, "未开闸"),
    OPENED((byte)20, "已开闸");

    private final byte value;
    private final String text;

    public byte getValue() {
        return value;
    }

    public String getText() {
        return text;
    }

    WmsDepartureOpenStatusEnum(byte value, String text) {
        this.value = value;
        this.text = text;
    }
}
