package com.unlcn.ils.wms.base.dto;

import cn.huiyunche.commons.domain.PageVo;

import java.io.Serializable;

public class WmsPreparationPlanDTO extends PageVo implements Serializable {
    private String orderno; //订单号
    private String customerName; //客户名称
    private String status;//状态
    private String startTime; //创建开始时间
    private String endTime;//创建时间截止时间
    private String whCode;
    private String userId;
    private String oddIds;//要生成备料计划的复选框勾选id,用逗号分隔
    private String estimateFinishTime;//预计装车时间
    private String estimateLoadingTime;//预计装车完成时间
    private String carrier;//承运商
    private String vehiclePlate;//承运车牌
    private String vin; //车架号
    private String ppOrderNo;//客户运单号
    private String ppEstimateLane;//计划装车道
    private String ppPreparationMaterialNo;//备料编码
    private String gmtCreateStart; //创建时间起始时间
    private String gmtCreateEnd; //创建时间截止时间
    private String ppStatus; //备料状态

    public String getPpOrderNo() {
        return ppOrderNo;
    }

    public void setPpOrderNo(String ppOrderNo) {
        this.ppOrderNo = ppOrderNo;
    }

    public String getPpEstimateLane() {
        return ppEstimateLane;
    }

    public void setPpEstimateLane(String ppEstimateLane) {
        this.ppEstimateLane = ppEstimateLane;
    }

    public String getPpPreparationMaterialNo() {
        return ppPreparationMaterialNo;
    }

    public void setPpPreparationMaterialNo(String ppPreparationMaterialNo) {
        this.ppPreparationMaterialNo = ppPreparationMaterialNo;
    }

    public String getGmtCreateStart() {
        return gmtCreateStart;
    }

    public void setGmtCreateStart(String gmtCreateStart) {
        this.gmtCreateStart = gmtCreateStart;
    }

    public String getGmtCreateEnd() {
        return gmtCreateEnd;
    }

    public void setGmtCreateEnd(String gmtCreateEnd) {
        this.gmtCreateEnd = gmtCreateEnd;
    }

    public String getPpStatus() {
        return ppStatus;
    }

    public void setPpStatus(String ppStatus) {
        this.ppStatus = ppStatus;
    }

    public String getVin() {
        return vin;
    }

    public void setVin(String vin) {
        this.vin = vin;
    }

    public String getCarrier() {
        return carrier;
    }

    public void setCarrier(String carrier) {
        this.carrier = carrier;
    }

    public String getVehiclePlate() {
        return vehiclePlate;
    }

    public void setVehiclePlate(String vehiclePlate) {
        this.vehiclePlate = vehiclePlate;
    }

    public String getEstimateFinishTime() {
        return estimateFinishTime;
    }

    public void setEstimateFinishTime(String estimateFinishTime) {
        this.estimateFinishTime = estimateFinishTime;
    }

    public String getEstimateLoadingTime() {
        return estimateLoadingTime;
    }

    public void setEstimateLoadingTime(String estimateLoadingTime) {
        this.estimateLoadingTime = estimateLoadingTime;
    }

    public String getOddIds() {
        return oddIds;
    }

    public void setOddIds(String oddIds) {
        this.oddIds = oddIds;
    }

    public String getWhCode() {
        return whCode;
    }

    public void setWhCode(String whCode) {
        this.whCode = whCode;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getOrderno() {
        return orderno;
    }

    public void setOrderno(String orderno) {
        this.orderno = orderno;
    }

    public String getCustomerName() {
        return customerName;
    }

    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getStartTime() {
        return startTime;
    }

    public void setStartTime(String startTime) {
        this.startTime = startTime;
    }

    public String getEndTime() {
        return endTime;
    }

    public void setEndTime(String endTime) {
        this.endTime = endTime;
    }
}
