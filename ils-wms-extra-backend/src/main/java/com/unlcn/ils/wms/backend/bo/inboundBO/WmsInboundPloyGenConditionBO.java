package com.unlcn.ils.wms.backend.bo.inboundBO;

import com.unlcn.ils.wms.backend.bo.baseDataBO.WmsCommonBO;

/**
 * Created by DELL on 2017/9/1.
 */
public class WmsInboundPloyGenConditionBO extends WmsCommonBO{

    /**
     * 生成条件主id
     */
    private Long pygrcId;

    /**
     * 生成条件主code
     */
    private String pygrcCode;

    /**
     * 生成条件名
     */
    private String pygrcName;

    /**
     * 条件的序号用于做条件之间的并跟或操作
     */
    private String pygrcNo;

    /**
     * 条件之间的根据序号的连接方式
     */
    private String pygrcJoinStyle;

    /**
     * 策略主id
     */
    private String pygrcPyId;

    /**
     * 策略主code
     */
    private String pygrcPyCode;

    /**
     * 策略主表名
     */
    private String pygrcPyName;

    /**
     * 策略条件id
     */
    private String pygrcPycId;

    /**
     * 策略条件code
     */
    private String pygrcPycCode;

    /**
     * 策略条件名
     */
    private String pygrcPycName;

    /**
     * 策略运算符id
     */
    private String pygrcPyoId;

    /**
     * 策略运算符code
     */
    private String pygrcPyoCode;

    /**
     * 策略运算符name
     */
    private String pygrcPyoName;

    /**
     * 策略流向id
     */
    private String pygrcPyfId;

    /**
     * 策略流向code
     */
    private String pygrcPyfCode;

    /**
     * 策略流向名
     */
    private String pygrcPyfName;

    /**
     * 策略库区id
     */
    private String pygrcPyzId;

    /**
     * 策略库区code
     */
    private String pygrcPyzCode;

    /**
     * 策略库区name
     */
    private String pygrcPyzName;

    /**
     * 策略库位id
     */
    private String pygrcPylId;

    /**
     * 策略库位code
     */
    private String pygrcPylCode;

    /**
     * 策略库位name
     */
    private String pygrcPylName;

    /**
     * 生成规则结果ID
     */
    private String pygrcPyrId;

    /**
     * 生成规则结果CODE
     */
    private String pygrcPyrCode;

    public Long getPygrcId() {
        return pygrcId;
    }

    public void setPygrcId(Long pygrcId) {
        this.pygrcId = pygrcId;
    }

    public String getPygrcCode() {
        return pygrcCode;
    }

    public void setPygrcCode(String pygrcCode) {
        this.pygrcCode = pygrcCode;
    }

    public String getPygrcName() {
        return pygrcName;
    }

    public void setPygrcName(String pygrcName) {
        this.pygrcName = pygrcName;
    }

    public String getPygrcNo() {
        return pygrcNo;
    }

    public void setPygrcNo(String pygrcNo) {
        this.pygrcNo = pygrcNo;
    }

    public String getPygrcJoinStyle() {
        return pygrcJoinStyle;
    }

    public void setPygrcJoinStyle(String pygrcJoinStyle) {
        this.pygrcJoinStyle = pygrcJoinStyle;
    }

    public String getPygrcPyId() {
        return pygrcPyId;
    }

    public void setPygrcPyId(String pygrcPyId) {
        this.pygrcPyId = pygrcPyId;
    }

    public String getPygrcPyCode() {
        return pygrcPyCode;
    }

    public void setPygrcPyCode(String pygrcPyCode) {
        this.pygrcPyCode = pygrcPyCode;
    }

    public String getPygrcPyName() {
        return pygrcPyName;
    }

    public void setPygrcPyName(String pygrcPyName) {
        this.pygrcPyName = pygrcPyName;
    }

    public String getPygrcPycId() {
        return pygrcPycId;
    }

    public void setPygrcPycId(String pygrcPycId) {
        this.pygrcPycId = pygrcPycId;
    }

    public String getPygrcPycCode() {
        return pygrcPycCode;
    }

    public void setPygrcPycCode(String pygrcPycCode) {
        this.pygrcPycCode = pygrcPycCode;
    }

    public String getPygrcPycName() {
        return pygrcPycName;
    }

    public void setPygrcPycName(String pygrcPycName) {
        this.pygrcPycName = pygrcPycName;
    }

    public String getPygrcPyoId() {
        return pygrcPyoId;
    }

    public void setPygrcPyoId(String pygrcPyoId) {
        this.pygrcPyoId = pygrcPyoId;
    }

    public String getPygrcPyoCode() {
        return pygrcPyoCode;
    }

    public void setPygrcPyoCode(String pygrcPyoCode) {
        this.pygrcPyoCode = pygrcPyoCode;
    }

    public String getPygrcPyoName() {
        return pygrcPyoName;
    }

    public void setPygrcPyoName(String pygrcPyoName) {
        this.pygrcPyoName = pygrcPyoName;
    }

    public String getPygrcPyfId() {
        return pygrcPyfId;
    }

    public void setPygrcPyfId(String pygrcPyfId) {
        this.pygrcPyfId = pygrcPyfId;
    }

    public String getPygrcPyfCode() {
        return pygrcPyfCode;
    }

    public void setPygrcPyfCode(String pygrcPyfCode) {
        this.pygrcPyfCode = pygrcPyfCode;
    }

    public String getPygrcPyfName() {
        return pygrcPyfName;
    }

    public void setPygrcPyfName(String pygrcPyfName) {
        this.pygrcPyfName = pygrcPyfName;
    }

    public String getPygrcPyzId() {
        return pygrcPyzId;
    }

    public void setPygrcPyzId(String pygrcPyzId) {
        this.pygrcPyzId = pygrcPyzId;
    }

    public String getPygrcPyzCode() {
        return pygrcPyzCode;
    }

    public void setPygrcPyzCode(String pygrcPyzCode) {
        this.pygrcPyzCode = pygrcPyzCode;
    }

    public String getPygrcPyzName() {
        return pygrcPyzName;
    }

    public void setPygrcPyzName(String pygrcPyzName) {
        this.pygrcPyzName = pygrcPyzName;
    }

    public String getPygrcPylId() {
        return pygrcPylId;
    }

    public void setPygrcPylId(String pygrcPylId) {
        this.pygrcPylId = pygrcPylId;
    }

    public String getPygrcPylCode() {
        return pygrcPylCode;
    }

    public void setPygrcPylCode(String pygrcPylCode) {
        this.pygrcPylCode = pygrcPylCode;
    }

    public String getPygrcPylName() {
        return pygrcPylName;
    }

    public void setPygrcPylName(String pygrcPylName) {
        this.pygrcPylName = pygrcPylName;
    }

    public String getPygrcPyrId() {
        return pygrcPyrId;
    }

    public void setPygrcPyrId(String pygrcPyrId) {
        this.pygrcPyrId = pygrcPyrId;
    }

    public String getPygrcPyrCode() {
        return pygrcPyrCode;
    }

    public void setPygrcPyrCode(String pygrcPyrCode) {
        this.pygrcPyrCode = pygrcPyrCode;
    }

    @Override
    public String toString() {
        return "WmsInboundPloyGenConditionBO{" +
                "pygrcId=" + pygrcId +
                ", pygrcCode='" + pygrcCode + '\'' +
                ", pygrcName='" + pygrcName + '\'' +
                ", pygrcNo='" + pygrcNo + '\'' +
                ", pygrcJoinStyle='" + pygrcJoinStyle + '\'' +
                ", pygrcPyId='" + pygrcPyId + '\'' +
                ", pygrcPyCode='" + pygrcPyCode + '\'' +
                ", pygrcPyName='" + pygrcPyName + '\'' +
                ", pygrcPycId='" + pygrcPycId + '\'' +
                ", pygrcPycCode='" + pygrcPycCode + '\'' +
                ", pygrcPycName='" + pygrcPycName + '\'' +
                ", pygrcPyoId='" + pygrcPyoId + '\'' +
                ", pygrcPyoCode='" + pygrcPyoCode + '\'' +
                ", pygrcPyoName='" + pygrcPyoName + '\'' +
                ", pygrcPyfId='" + pygrcPyfId + '\'' +
                ", pygrcPyfCode='" + pygrcPyfCode + '\'' +
                ", pygrcPyfName='" + pygrcPyfName + '\'' +
                ", pygrcPyzId='" + pygrcPyzId + '\'' +
                ", pygrcPyzCode='" + pygrcPyzCode + '\'' +
                ", pygrcPyzName='" + pygrcPyzName + '\'' +
                ", pygrcPylId='" + pygrcPylId + '\'' +
                ", pygrcPylCode='" + pygrcPylCode + '\'' +
                ", pygrcPylName='" + pygrcPylName + '\'' +
                ", pygrcPyrId='" + pygrcPyrId + '\'' +
                ", pygrcPyrCode='" + pygrcPyrCode + '\'' +
                '}';
    }
}
