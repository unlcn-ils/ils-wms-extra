package com.unlcn.ils.wms.backend.bo.baseDataBO;

/**
 * Created by DELL on 2017/10/19.
 */
public class VehicleSpecBO {
    /**
     * 车型code
     */
    private String code;

    /**
     * 车型name
     */
    private String name;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        VehicleSpecBO vehicleSpecBO = (VehicleSpecBO) o;

        if (!name.equals(vehicleSpecBO.name)) return false;
        return name.equals(vehicleSpecBO.name);

    }

    @Override
    public int hashCode() {
        int result = name.hashCode();
        result = 31 * result + name.hashCode();
        return result;
    }
}
