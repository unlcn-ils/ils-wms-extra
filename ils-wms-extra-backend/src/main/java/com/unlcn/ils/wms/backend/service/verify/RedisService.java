package com.unlcn.ils.wms.backend.service.verify;

import com.unlcn.ils.wms.backend.bo.VerifyBO;

/**
 * @Auther linbao
 * @Date 2017-10-18
 */
public interface RedisService {

    /**
     * 验证
     *
     * @param token
     * @return
     * @throws Exception
     */
    VerifyBO verifyUser(String token) throws Exception;

    /**
     * 加入redis队列总
     * @param value
     * @throws Exception
     */
    void rpush(String value) throws Exception;

    /**
     * 根据key获取数据
     *
     * @param token
     * @return
     * @throws Exception
     */
    String getData(String token) throws Exception;

    /**
     * 移除
     *
     * @throws Exception
     */
    void removeDataByValue(String value) throws Exception;

    /**
     * 移除
     *
     * @throws Exception
     */
    void removeDataByToken(String token) throws Exception;
}
