package com.unlcn.ils.wms.base.dto;

import com.unlcn.ils.wms.base.model.stock.WmsBorrowCarDetail;

import java.util.Date;
import java.util.List;

/**
 * Created by lenovo on 2017/10/20.
 */
public class ReturnDetailDTO {
    // 还车单id
    private Long rcId;

    // 还车人姓名
    private String rcReturnName;

    // 还车部门
    private String rcReturnDepartment;

    // 联系电话
    private String rcReturnTel;

    // 还车日期
    private String rcReturnDate;

    // 关联车借车单号
    private String rcBorrowNo;

    // 上传凭证key
    private String uploadKey;

    // 上传凭证路径
    private String uploadPath;
    
    // 还车单详情
    List<WmsBorrowCarDetail> wmsBorrowCarDetailList;

    public Long getRcId() {
        return rcId;
    }

    public void setRcId(Long rcId) {
        this.rcId = rcId;
    }

    public String getRcReturnName() {
        return rcReturnName;
    }

    public void setRcReturnName(String rcReturnName) {
        this.rcReturnName = rcReturnName;
    }

    public String getRcReturnDepartment() {
        return rcReturnDepartment;
    }

    public void setRcReturnDepartment(String rcReturnDepartment) {
        this.rcReturnDepartment = rcReturnDepartment;
    }

    public String getRcReturnTel() {
        return rcReturnTel;
    }

    public void setRcReturnTel(String rcReturnTel) {
        this.rcReturnTel = rcReturnTel;
    }

    public String getRcReturnDate() {
        return rcReturnDate;
    }

    public void setRcReturnDate(String rcReturnDate) {
        this.rcReturnDate = rcReturnDate;
    }

    public String getRcBorrowNo() {
        return rcBorrowNo;
    }

    public void setRcBorrowNo(String rcBorrowNo) {
        this.rcBorrowNo = rcBorrowNo;
    }

    public List<WmsBorrowCarDetail> getWmsBorrowCarDetailList() {
        return wmsBorrowCarDetailList;
    }

    public void setWmsBorrowCarDetailList(List<WmsBorrowCarDetail> wmsBorrowCarDetailList) {
        this.wmsBorrowCarDetailList = wmsBorrowCarDetailList;
    }

	public String getUploadKey() {
		return uploadKey;
	}

	public void setUploadKey(String uploadKey) {
		this.uploadKey = uploadKey;
	}

	public String getUploadPath() {
		return uploadPath;
	}

	public void setUploadPath(String uploadPath) {
		this.uploadPath = uploadPath;
	}

	@Override
    public String toString() {
        return "ReturnDetailDTO{" +
                "rcId=" + rcId +
                ", rcReturnName='" + rcReturnName + '\'' +
                ", rcReturnDepartment='" + rcReturnDepartment + '\'' +
                ", rcReturnTel='" + rcReturnTel + '\'' +
                ", rcReturnDate='" + rcReturnDate + '\'' +
                ", rcBorrowNo='" + rcBorrowNo + '\'' +
                ", wmsBorrowCarDetailList=" + wmsBorrowCarDetailList +
                '}';
    }
}
