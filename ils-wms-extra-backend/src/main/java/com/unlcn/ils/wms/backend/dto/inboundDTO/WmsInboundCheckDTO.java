package com.unlcn.ils.wms.backend.dto.inboundDTO;

import java.util.Date;

/**
 * Created by DELL on 2017/8/18.
 */
public class WmsInboundCheckDTO {

    private Long ckId;

    private String ckCode;

    private String ckOdId;

    private String ckBillNo;

    private String ckWaybillNo;

    private String ckVehicleSpecCode;

    private String ckVehicleSpecName;

    private String ckVin;

    private String ckEngine;

    private String ckCheckResult;

    private String ckCheckDesc;

    private String createUserId;

    private String createUserName;

    private String modifyUserId;

    private String modifyUserName;

    private Date gmtCreate;

    private Date gmtUpdate;

    private Byte isDeleted;


    private Long versions;

    public Long getCkId() {
        return ckId;
    }

    public void setCkId(Long ckId) {
        this.ckId = ckId;
    }

    public String getCkCode() {
        return ckCode;
    }

    public void setCkCode(String ckCode) {
        this.ckCode = ckCode;
    }

    public String getCkOdId() {
        return ckOdId;
    }

    public void setCkOdId(String ckOdId) {
        this.ckOdId = ckOdId;
    }

    public String getCkBillNo() {
        return ckBillNo;
    }

    public void setCkBillNo(String ckBillNo) {
        this.ckBillNo = ckBillNo;
    }

    public String getCkWaybillNo() {
        return ckWaybillNo;
    }

    public void setCkWaybillNo(String ckWaybillNo) {
        this.ckWaybillNo = ckWaybillNo;
    }

    public String getCkVehicleSpecCode() {
        return ckVehicleSpecCode;
    }

    public void setCkVehicleSpecCode(String ckVehicleSpecCode) {
        this.ckVehicleSpecCode = ckVehicleSpecCode;
    }

    public String getCkVehicleSpecName() {
        return ckVehicleSpecName;
    }

    public void setCkVehicleSpecName(String ckVehicleSpecName) {
        this.ckVehicleSpecName = ckVehicleSpecName;
    }

    public String getCkVin() {
        return ckVin;
    }

    public void setCkVin(String ckVin) {
        this.ckVin = ckVin;
    }

    public String getCkEngine() {
        return ckEngine;
    }

    public void setCkEngine(String ckEngine) {
        this.ckEngine = ckEngine;
    }

    public String getCkCheckResult() {
        return ckCheckResult;
    }

    public void setCkCheckResult(String ckCheckResult) {
        this.ckCheckResult = ckCheckResult;
    }

    public String getCkCheckDesc() {
        return ckCheckDesc;
    }

    public void setCkCheckDesc(String ckCheckDesc) {
        this.ckCheckDesc = ckCheckDesc;
    }

    public String getCreateUserId() {
        return createUserId;
    }

    public void setCreateUserId(String createUserId) {
        this.createUserId = createUserId;
    }

    public String getCreateUserName() {
        return createUserName;
    }

    public void setCreateUserName(String createUserName) {
        this.createUserName = createUserName;
    }

    public String getModifyUserId() {
        return modifyUserId;
    }

    public void setModifyUserId(String modifyUserId) {
        this.modifyUserId = modifyUserId;
    }

    public String getModifyUserName() {
        return modifyUserName;
    }

    public void setModifyUserName(String modifyUserName) {
        this.modifyUserName = modifyUserName;
    }

    public Date getGmtCreate() {
        return gmtCreate;
    }

    public void setGmtCreate(Date gmtCreate) {
        this.gmtCreate = gmtCreate;
    }

    public Date getGmtUpdate() {
        return gmtUpdate;
    }

    public void setGmtUpdate(Date gmtUpdate) {
        this.gmtUpdate = gmtUpdate;
    }

    public Byte getIsDeleted() {
        return isDeleted;
    }

    public void setIsDeleted(Byte isDeleted) {
        this.isDeleted = isDeleted;
    }

    public Long getVersions() {
        return versions;
    }

    public void setVersions(Long versions) {
        this.versions = versions;
    }

    @Override
    public String toString() {
        return "WmsInboundCheckDTO{" +
                "ckId=" + ckId +
                ", ckCode='" + ckCode + '\'' +
                ", ckOdId='" + ckOdId + '\'' +
                ", ckBillNo='" + ckBillNo + '\'' +
                ", ckWaybillNo='" + ckWaybillNo + '\'' +
                ", ckVehicleSpecCode='" + ckVehicleSpecCode + '\'' +
                ", ckVehicleSpecName='" + ckVehicleSpecName + '\'' +
                ", ckVin='" + ckVin + '\'' +
                ", ckEngine='" + ckEngine + '\'' +
                ", ckCheckResult='" + ckCheckResult + '\'' +
                ", ckCheckDesc='" + ckCheckDesc + '\'' +
                ", createUserId='" + createUserId + '\'' +
                ", createUserName='" + createUserName + '\'' +
                ", modifyUserId='" + modifyUserId + '\'' +
                ", modifyUserName='" + modifyUserName + '\'' +
                ", gmtCreate=" + gmtCreate +
                ", gmtUpdate=" + gmtUpdate +
                ", isDeleted=" + isDeleted +
                ", versions=" + versions +
                '}';
    }
}
