package com.unlcn.ils.wms.app.controller.user;

import cn.huiyunche.commons.domain.ResultDTO;
import cn.huiyunche.commons.exception.BusinessException;
import com.unlcn.ils.wms.app.vo.wms.UserVO;
import com.unlcn.ils.wms.backend.bo.sys.RoleBO;
import com.unlcn.ils.wms.backend.bo.sys.UserBO;
import com.unlcn.ils.wms.backend.dto.sysDTO.UserNewDTO;
import com.unlcn.ils.wms.backend.service.baseData.WmsWarehouseService;
import com.unlcn.ils.wms.backend.service.sys.UserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpSession;
import java.util.UUID;

/**
 * Created by houjianhui on 2017/5/11.
 */
@Controller
@RequestMapping("/login")
public class LoginController {

    private Logger LOGGER = LoggerFactory.getLogger(LoginController.class);

    @Autowired
    private UserService userService;

    @Autowired
    private WmsWarehouseService wmsWarehouseService;

    /**
     * 获取用户信息
     *
     * @param userName 用户登录名称
     * @return
     * @throws BusinessException
     */
    @RequestMapping(value = "/userlogin", method = RequestMethod.POST)
    public @ResponseBody
    ResultDTO<Object> login(String userName) throws BusinessException {
        LOGGER.info("LoginController.login params userName: {}", userName);
        ResultDTO<Object> result = new ResultDTO<>(true, null, "登录成功");
        try {
            result.setData(userService.isUserExist(userName));
        } catch (Exception e) {
            LOGGER.error("LoginController.login error: {}", e);
            throw new BusinessException(e.getMessage());
        }
        return result;
    }

    /**
     * app端的登录
     *
     * @param userVO
     * @return
     */
    @RequestMapping(value = "/loginForApp", method = RequestMethod.GET)
    public @ResponseBody
    ResultDTO<Object> loginForApp(UserVO userVO, HttpSession session) {
        LOGGER.info("LoginController.loginForApp params userVO: {}", userVO);
        ResultDTO<Object> resultDTO = new ResultDTO<>(true, null, "登录成功");

        try {
            UserBO userBO = new UserBO();
            BeanUtils.copyProperties(userVO, userBO);
            String token = UUID.randomUUID().toString();
            //RoleBO roleBo = userService.getPermissForAppLogin(userBO);
            //roleBo.setToken(token);
            //session.setAttribute(token, userBO.getUsername());
            //resultDTO.setData(roleBo);
            UserNewDTO userNewDTO = userService.getPermissListForAppLogin(userBO);
            userNewDTO.setToken(token);
            session.setAttribute(token, userBO.getUsername());
            resultDTO.setData(userNewDTO);
        } catch (BusinessException e) {
            LOGGER.error("LoginController.loginForApp businessExcetption: {}", e);
            resultDTO.setSuccess(false);
            resultDTO.setMessage(e.getMessage());
        } catch (Exception e) {
            LOGGER.error("LoginController.loginForApp error: {}", e);
            resultDTO.setSuccess(false);
            resultDTO.setMessage("登录失败");
        }
        return resultDTO;
    }

    /**
     * app首页获取仓库
     *
     * @return
     */
    @RequestMapping(value = "/listWarehouseForApp", method = RequestMethod.GET)
    public @ResponseBody
    ResultDTO<Object> listWarehouseForApp() {
        LOGGER.info("LoginController.listWarehouse 获取仓库");
        ResultDTO<Object> resultDTO = new ResultDTO<>(true, null, "获取仓库成功");

        try {
            resultDTO.setData(wmsWarehouseService.listWarehouseForApp());
        } catch (Exception e) {
            LOGGER.error("LoginController.listWarehouse error: {}", e);
            resultDTO.setSuccess(false);
            resultDTO.setMessage("获取仓库失败");
        }
        return resultDTO;
    }
}
