package com.unlcn.ils.wms.backend.enums;

/**
 * @Auther linbao
 * @Date 2017-11-03
 * 库存库位状态
 */
public enum InventoryLocationStatusEnum {

    NORMAL_10("10", "正常在库"),
    DEST_LOCK_20("20", "发运锁定"),
    BORROW_LOCK_30("30", "借用锁定"),
    REPAIR_LOCK_40("40", "维修锁定"),
    MAINTAIN_LOCK_50("50", "已出库"),
    INVENTORY_FREEZE("60", "冻结"),
    MAIL_SEND("Y", "入库邮件已发送"),
    MAI_NOT_SEND("N", "入库邮件未发送");

    private final String code;

    private final String value;

    public String getCode() {
        return code;
    }

    public String getValue() {
        return value;
    }

    InventoryLocationStatusEnum(String code, String value) {
        this.code = code;
        this.value = value;
    }
}
