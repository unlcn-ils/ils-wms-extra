package com.unlcn.ils.wms.backend.bo.pickup;

/**
 * Created by houjianhui on 2017/7/24.
 */
public class TmsExcpAttachBO {

    private Long id;
    private String picKey;
    private String picUrl;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getPicKey() {
        return picKey;
    }

    public void setPicKey(String picKey) {
        this.picKey = picKey;
    }

    public String getPicUrl() {
        return picUrl;
    }

    public void setPicUrl(String picUrl) {
        this.picUrl = picUrl;
    }

    @Override
    public String toString() {
        return "TmsExcpAttachBO{" +
                "id=" + id +
                ", picKey='" + picKey + '\'' +
                ", picUrl='" + picUrl + '\'' +
                '}';
    }
}
