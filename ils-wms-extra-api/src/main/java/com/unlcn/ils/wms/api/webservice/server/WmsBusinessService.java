package com.unlcn.ils.wms.api.webservice.server;

import com.unlcn.ils.wms.backend.service.webservice.vo.WmsDealerCarBackVO;
import com.unlcn.ils.wms.backend.service.webservice.vo.WmsShipmentPlanVO;

public interface WmsBusinessService {

    /**
     * 新增发运计划
     */

    String insertWmsShipmentPlan(WmsShipmentPlanVO[] json) throws Exception;

    /**
     * 新增经销商退车
     */
    String insertWmsDealerCarBack(WmsDealerCarBackVO[] json) throws Exception;

}

