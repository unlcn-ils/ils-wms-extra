package com.unlcn.ils.wms.backend.service.inspectApp.impl;

import com.unlcn.ils.wms.backend.service.inspectApp.TmsExcpService;
import com.unlcn.ils.wms.base.mapper.inspectApp.TmsInspectExcpMapper;
import com.unlcn.ils.wms.base.model.inspectApp.TmsInspectExcp;
import com.unlcn.ils.wms.base.model.inspectApp.TmsInspectExcpExample;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Objects;

/**
 * Created by houjianhui on 2017/7/24.
 */

@Service
public class TmsExcpServiceImpl implements TmsExcpService {

    private Logger LOGGER = LoggerFactory.getLogger(TmsExcpServiceImpl.class);

    @Autowired
    private TmsInspectExcpMapper tmsInspectExcpMapper;

    @Override
    public List<TmsInspectExcp> getInspectExcpListByInspectId(Long inspectId) {
        LOGGER.info("TmsExcpServiceImpl.getPickupExcpByInfoId inspectId: {}", inspectId);
        if (Objects.equals(inspectId, null)) {
            LOGGER.info("TmsExcpServiceImpl.getPickupExcpByInfoId infoId must not be null");
            throw new IllegalArgumentException("检查ID不能为空");
        }
        TmsInspectExcpExample excpExample = new TmsInspectExcpExample();
        excpExample.createCriteria().andInspectIdEqualTo(inspectId).andDelFlagEqualTo(true);
        return tmsInspectExcpMapper.selectByExample(excpExample);
    }
}
