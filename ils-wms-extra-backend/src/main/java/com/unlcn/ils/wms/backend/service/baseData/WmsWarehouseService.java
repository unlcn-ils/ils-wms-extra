package com.unlcn.ils.wms.backend.service.baseData;

import com.unlcn.ils.wms.backend.bo.baseDataBO.WmsWarehouseBO;
import com.unlcn.ils.wms.base.businessDTO.baseData.WmsWarehouseDTO;
import com.unlcn.ils.wms.base.businessDTO.baseData.WmsWarehouseQueryDTO;
import com.unlcn.ils.wms.base.model.stock.WmsWarehouse;

import java.util.List;
import java.util.Map;

/**
 * Created by DELL on 2017/9/20.
 */
public interface WmsWarehouseService {

    /**
     * 查询仓库list
     * @return
     */
    Map<String,Object> getWarehouseList(WmsWarehouseQueryDTO wmsWarehouseQueryDTO)throws IllegalAccessException ;

    /**
     * 新增仓库
     * @param wmsWarehouseBO
     */
    void addWarehouse(WmsWarehouseBO wmsWarehouseBO);

    /**
     * 修改仓库
     * @param wmsWarehouseBO
     */
    void updateWarehouse(WmsWarehouseBO wmsWarehouseBO);

    /**
     * 删除仓库
     * @param wmsWarehouseBOList
     */
    void deleteWarehouse(List<WmsWarehouseBO> wmsWarehouseBOList);

    /**
     * 根据仓库code查询仓库 当为空那么返回所有
     * @return
     */
    List<WmsWarehouse> getWarehouse();

    /**
     * 根据仓库code查询单个仓库
     * @return
     */
    WmsWarehouse findWarehouseByCode(WmsWarehouseBO wmsWarehouseBO);

    /**
     * 根据仓库ID查询单个仓库
     * @return
     */
    WmsWarehouse findWarehouseById(WmsWarehouseBO wmsWarehouseBO);

    /**
     * APP首页查询仓库
     * linbao 2017-10-09
     *
     * @return
     */
    List<WmsWarehouseDTO> listWarehouseForApp() throws Exception;
}
