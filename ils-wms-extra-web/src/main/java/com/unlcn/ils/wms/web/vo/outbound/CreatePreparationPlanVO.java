package com.unlcn.ils.wms.web.vo.outbound;

import java.io.Serializable;

public class CreatePreparationPlanVO implements Serializable {

	/**
	* 
	*/
	private static final long serialVersionUID = 1L;
	private String spId; // 发运计划ID
	private String estimateLane; // 计划装车道
	private String estimateLoadingTime; // 计划装车时间
	private String estimateFinishTime; // 预计完成时间

	public String getSpId() {
		return spId;
	}

	public void setSpId(String spId) {
		this.spId = spId;
	}

	public String getEstimateLane() {
		return estimateLane;
	}

	public void setEstimateLane(String estimateLane) {
		this.estimateLane = estimateLane;
	}

	public String getEstimateLoadingTime() {
		return estimateLoadingTime;
	}

	public void setEstimateLoadingTime(String estimateLoadingTime) {
		this.estimateLoadingTime = estimateLoadingTime;
	}

	public String getEstimateFinishTime() {
		return estimateFinishTime;
	}

	public void setEstimateFinishTime(String estimateFinishTime) {
		this.estimateFinishTime = estimateFinishTime;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("CreatePreparationPlanBO [spId=");
		builder.append(spId);
		builder.append(", estimateLane=");
		builder.append(estimateLane);
		builder.append(", estimateLoadingTime=");
		builder.append(estimateLoadingTime);
		builder.append(", estimateFinishTime=");
		builder.append(estimateFinishTime);
		builder.append("]");
		return builder.toString();
	}

}
