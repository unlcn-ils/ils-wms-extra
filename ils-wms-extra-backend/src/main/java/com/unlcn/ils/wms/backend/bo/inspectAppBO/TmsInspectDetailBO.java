package com.unlcn.ils.wms.backend.bo.inspectAppBO;

import com.unlcn.ils.wms.base.dto.TmsInspectExcpDTO;

import java.util.Date;
import java.util.List;

/**
 * @Author ：Ligl
 * @Date : 2017-09-11.
 */
public class TmsInspectDetailBO {
    private Date pickupTime;
    private String pickupUserId;
    private String checkStatusText;
    private String pickupStatusText;

    private String waybillCode;

    private String generateTime;

    private Long id;

    private String vehicle;

    private String destProvince;

    private String destCity;

    private String cacheWarehouse;

    private String vin;

    private String warehouseName;

    private String engineNo;

    private Integer inspectStatus;

    private String inspectStatusText;

    private String inspectUserId;

    private Date inspectTime;

    private List<TmsInspectExcpDTO> positionAndExcpCount;

    public Date getPickupTime() {
        return pickupTime;
    }

    public void setPickupTime(Date pickupTime) {
        this.pickupTime = pickupTime;
    }

    public String getPickupUserId() {
        return pickupUserId;
    }

    public void setPickupUserId(String pickupUserId) {
        this.pickupUserId = pickupUserId;
    }

    public String getCheckStatusText() {
        return checkStatusText;
    }

    public void setCheckStatusText(String checkStatusText) {
        this.checkStatusText = checkStatusText;
    }

    public String getPickupStatusText() {
        return pickupStatusText;
    }

    public void setPickupStatusText(String pickupStatusText) {
        this.pickupStatusText = pickupStatusText;
    }

    public List<TmsInspectExcpDTO> getPositionAndExcpCount() {
        return positionAndExcpCount;
    }

    public void setPositionAndExcpCount(List<TmsInspectExcpDTO> positionAndExcpCount) {
        this.positionAndExcpCount = positionAndExcpCount;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getWaybillCode() {
        return waybillCode;
    }

    public void setWaybillCode(String waybillCode) {
        this.waybillCode = waybillCode;
    }

    public String getGenerateTime() {
        return generateTime;
    }

    public void setGenerateTime(String generateTime) {
        this.generateTime = generateTime;
    }

    public String getVehicle() {
        return vehicle;
    }

    public void setVehicle(String vehicle) {
        this.vehicle = vehicle;
    }

    public String getDestProvince() {
        return destProvince;
    }

    public void setDestProvince(String destProvince) {
        this.destProvince = destProvince;
    }

    public String getDestCity() {
        return destCity;
    }

    public void setDestCity(String destCity) {
        this.destCity = destCity;
    }

    public String getCacheWarehouse() {
        return cacheWarehouse;
    }

    public void setCacheWarehouse(String cacheWarehouse) {
        this.cacheWarehouse = cacheWarehouse;
    }

    public String getVin() {
        return vin;
    }

    public void setVin(String vin) {
        this.vin = vin;
    }

    public String getWarehouseName() {
        return warehouseName;
    }

    public void setWarehouseName(String warehouseName) {
        this.warehouseName = warehouseName;
    }


    public String getInspectUserId() {
        return inspectUserId;
    }

    public void setInspectUserId(String inspectUserId) {
        this.inspectUserId = inspectUserId;
    }

    public Date getInspectTime() {
        return inspectTime;
    }

    public void setInspectTime(Date inspectTime) {
        this.inspectTime = inspectTime;
    }


    public String getEngineNo() {
        return engineNo;
    }

    public void setEngineNo(String engineNo) {
        this.engineNo = engineNo;
    }

    public Integer getInspectStatus() {
        return inspectStatus;
    }

    public void setInspectStatus(Integer inspectStatus) {
        this.inspectStatus = inspectStatus;
    }

    public String getInspectStatusText() {
        return inspectStatusText;
    }

    public void setInspectStatusText(String inspectStatusText) {
        this.inspectStatusText = inspectStatusText;
    }
}
