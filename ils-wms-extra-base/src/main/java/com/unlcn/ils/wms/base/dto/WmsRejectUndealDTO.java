package com.unlcn.ils.wms.base.dto;

import java.io.Serializable;

public class WmsRejectUndealDTO implements Serializable {
    private String gbNo;
    private String whCode;
    private String spIds;
    private String userId;

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getGbNo() {
        return gbNo;
    }

    public void setGbNo(String gbNo) {
        this.gbNo = gbNo;
    }

    public String getWhCode() {
        return whCode;
    }

    public void setWhCode(String whCode) {
        this.whCode = whCode;
    }

    public String getSpIds() {
        return spIds;
    }

    public void setSpIds(String spIds) {
        this.spIds = spIds;
    }
}
