package com.unlcn.ils.wms.base.dto;

import java.io.Serializable;

public class WmsRepairInspectPlaceDTO implements Serializable {
    private String code;
    private String name;

    public WmsRepairInspectPlaceDTO() {
    }

    public WmsRepairInspectPlaceDTO(String code, String name) {
        this.code = code;
        this.name = name;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
