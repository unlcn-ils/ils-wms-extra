package com.unlcn.ils.wms.base.dto;

import java.util.Date;

/**
 * @Auther linbao
 * @Date 2017-11-23
 */
public class ExcpManageDTO {

    /**
     * 单ID
     */
    private Long orderId;

    private String orderNo;

    /**
     * 车架号
     */
    private String vin;

    /**
     * 异常分类 10-入库异常, 20-出库异常, 30-备料异常
     */
    private String excpType;

    /**
     * 异常类型代码
     */
    private String excpTypeCode;

    /**
     * 异常类型名称
     */
    private String excpTypeName;

    /**
     * 异常状态
     */
    private String excpStatus;

    /**
     * 异常描述
     */
    private String excpDesc;

    /**
     * 责任方
     */
    private String involvedParty;

    /**
     * 异常发生时间
     */
    private Date gmtCreate;

    /**
     * 处理类型
     */
    private String dealType;

    /**
     * 处理结果描述
     */
    private String dealResultDesc;

    /**
     * 处理结束时间
     */
    private Date dealEndTime;

    /**
     * 时间戳
     */
    private String excpTimestamp;

    private String inspectUserName;


    public String getInspectUserName() {
        return inspectUserName;
    }

    public void setInspectUserName(String inspectUserName) {
        this.inspectUserName = inspectUserName;
    }

    public String getOrderNo() {
        return orderNo;
    }

    public void setOrderNo(String orderNo) {
        this.orderNo = orderNo;
    }

    public String getExcpTimestamp() {
        return excpTimestamp;
    }

    public void setExcpTimestamp(String excpTimestamp) {
        this.excpTimestamp = excpTimestamp;
    }

    public Long getOrderId() {
        return orderId;
    }

    public void setOrderId(Long orderId) {
        this.orderId = orderId;
    }

    public String getVin() {
        return vin;
    }

    public void setVin(String vin) {
        this.vin = vin;
    }

    public String getExcpType() {
        return excpType;
    }

    public void setExcpType(String excpType) {
        this.excpType = excpType;
    }

    public String getExcpTypeCode() {
        return excpTypeCode;
    }

    public void setExcpTypeCode(String excpTypeCode) {
        this.excpTypeCode = excpTypeCode;
    }

    public String getExcpTypeName() {
        return excpTypeName;
    }

    public void setExcpTypeName(String excpTypeName) {
        this.excpTypeName = excpTypeName;
    }

    public String getExcpStatus() {
        return excpStatus;
    }

    public void setExcpStatus(String excpStatus) {
        this.excpStatus = excpStatus;
    }

    public String getExcpDesc() {
        return excpDesc;
    }

    public void setExcpDesc(String excpDesc) {
        this.excpDesc = excpDesc;
    }

    public String getInvolvedParty() {
        return involvedParty;
    }

    public void setInvolvedParty(String involvedParty) {
        this.involvedParty = involvedParty;
    }

    public Date getGmtCreate() {
        return gmtCreate;
    }

    public void setGmtCreate(Date gmtCreate) {
        this.gmtCreate = gmtCreate;
    }

    public String getDealType() {
        return dealType;
    }

    public void setDealType(String dealType) {
        this.dealType = dealType;
    }

    public String getDealResultDesc() {
        return dealResultDesc;
    }

    public void setDealResultDesc(String dealResultDesc) {
        this.dealResultDesc = dealResultDesc;
    }

    public Date getDealEndTime() {
        return dealEndTime;
    }

    public void setDealEndTime(Date dealEndTime) {
        this.dealEndTime = dealEndTime;
    }
}
