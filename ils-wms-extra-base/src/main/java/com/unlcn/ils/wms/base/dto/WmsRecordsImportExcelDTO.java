package com.unlcn.ils.wms.base.dto;

import java.io.Serializable;

/**
 * 用于入库记录导出功能参数封装
 */
public class WmsRecordsImportExcelDTO implements Serializable {
    //入库记录按照收货起止时间进行导出
    private String recieveStartTime;
    private String recieveEndTime;
    private String whCode;
    private String userId;

    public String getWhCode() {
        return whCode;
    }

    public void setWhCode(String whCode) {
        this.whCode = whCode;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getRecieveStartTime() {
        return recieveStartTime;
    }

    public void setRecieveStartTime(String recieveStartTime) {
        this.recieveStartTime = recieveStartTime;
    }

    public String getRecieveEndTime() {
        return recieveEndTime;
    }

    public void setRecieveEndTime(String recieveEndTime) {
        this.recieveEndTime = recieveEndTime;
    }
}
