package com.unlcn.ils.wms.backend.enums.excp;

/**
 * @Auther linbao
 * @Date 2017-11-23
 */
public enum ExcpStatusEnum {
    NOT_DEAL((byte) 10, "未处理"),
    DEALING((byte) 20, "处理中"),
    CLOSED((byte) 30, "已关闭");

    private final Byte code;

    private final String text;

    ExcpStatusEnum(Byte code, String text) {
        this.code = code;
        this.text = text;
    }

    public Byte getCode() {
        return code;
    }

    public String getText() {
        return text;
    }


    public static ExcpStatusEnum getByValue(byte value) {
        for (ExcpStatusEnum temp : ExcpStatusEnum.values()) {
            if (value == temp.getCode()) {
                return temp;
            }
        }
        return null;
    }

}
