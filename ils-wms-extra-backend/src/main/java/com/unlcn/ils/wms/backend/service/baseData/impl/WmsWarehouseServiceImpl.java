package com.unlcn.ils.wms.backend.service.baseData.impl;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.unlcn.ils.wms.backend.bo.baseDataBO.WmsWarehouseBO;
import com.unlcn.ils.wms.backend.service.baseData.WmsWarehouseService;
import com.unlcn.ils.wms.backend.util.ObjectToMapUtils;
import com.unlcn.ils.wms.base.businessDTO.baseData.WmsWarehouseDTO;
import com.unlcn.ils.wms.base.businessDTO.baseData.WmsWarehouseQueryDTO;
import com.unlcn.ils.wms.base.mapper.additional.WmsWarehouseExtMapper;
import com.unlcn.ils.wms.base.mapper.stock.WmsWarehouseMapper;
import com.unlcn.ils.wms.base.model.stock.WmsWarehouse;
import com.unlcn.ils.wms.base.model.stock.WmsWarehouseExample;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.text.SimpleDateFormat;
import java.util.List;
import java.util.Map;
import java.util.Objects;

/**
 * Created by DELL on 2017/9/20.
 */
@Service
public class WmsWarehouseServiceImpl implements WmsWarehouseService {
    private Logger LOGGER = LoggerFactory.getLogger(WmsWarehouseServiceImpl.class);

    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

    @Autowired
    WmsWarehouseMapper wmsWarehouseMapper;

    @Autowired
    WmsWarehouseExtMapper wmsWarehouseExtMapper;

    @Override
    public Map<String, Object> getWarehouseList(WmsWarehouseQueryDTO wmsWarehouseQueryDTO) throws IllegalAccessException {
        Map<String, Object> resultMap = Maps.newHashMap();

        wmsWarehouseQueryDTO.setLimitStart(wmsWarehouseQueryDTO.getStartIndex());
        wmsWarehouseQueryDTO.setLimitEnd(wmsWarehouseQueryDTO.getPageSize());

        Map<String, Object> paramMap = ObjectToMapUtils.objectToMap(wmsWarehouseQueryDTO);
        List<WmsWarehouse> wmsWarehouseList = wmsWarehouseExtMapper.queryForList(paramMap);
        List<WmsWarehouseBO> bos = Lists.newArrayList();
        if (CollectionUtils.isNotEmpty(wmsWarehouseList)) {
            wmsWarehouseList.stream().forEach(v -> {
                WmsWarehouseBO bo = new WmsWarehouseBO();
                BeanUtils.copyProperties(v, bo);
                bos.add(bo);
            });
            resultMap.put("warehouseList", bos);
        }
        wmsWarehouseQueryDTO.setPageNo(wmsWarehouseQueryDTO.getStartIndex());
        wmsWarehouseQueryDTO.setPageSize(wmsWarehouseQueryDTO.getPageSize());
        wmsWarehouseQueryDTO.setTotalRecord(wmsWarehouseExtMapper.queryForCount(paramMap));
        resultMap.put("wmsWarehouseQueryBO", wmsWarehouseQueryDTO);
        return resultMap;
    }

    @Override
    public void addWarehouse(WmsWarehouseBO wmsWarehouseBO) {
        WmsWarehouse wmsWarehouse = new WmsWarehouse();
        BeanUtils.copyProperties(wmsWarehouseBO, wmsWarehouse);
        wmsWarehouse.setIsDeleted((byte) 1);
        wmsWarehouseMapper.insert(wmsWarehouse);
    }

    @Override
    public void updateWarehouse(WmsWarehouseBO wmsWarehouseBO) {
        WmsWarehouse wmsWarehouse = new WmsWarehouse();
        BeanUtils.copyProperties(wmsWarehouseBO, wmsWarehouse);
        wmsWarehouseMapper.updateByPrimaryKeySelective(wmsWarehouse);
    }

    @Override
    public void deleteWarehouse(List<WmsWarehouseBO> wmsWarehouseBOList) {
        if (CollectionUtils.isNotEmpty(wmsWarehouseBOList)) {
            wmsWarehouseBOList.stream().forEach(v -> {
                wmsWarehouseMapper.deleteByPrimaryKey(v.getWhId());
            });
        }
    }

    @Override
    public List<WmsWarehouse> getWarehouse() {
        WmsWarehouseExample example = new WmsWarehouseExample();
        example.setLimitStart(0);
        example.setLimitEnd(Integer.MAX_VALUE);
        return wmsWarehouseMapper.selectByExample(example);
    }

    /**
     * 根据仓库code查询单个仓库
     *
     * @param wmsWarehouseBO
     * @return
     */
    @Override
    public WmsWarehouse findWarehouseByCode(WmsWarehouseBO wmsWarehouseBO) {
        if (!Objects.equals(wmsWarehouseBO, null)
                && StringUtils.isNotBlank(wmsWarehouseBO.getWhCode())) {
            WmsWarehouseExample example = new WmsWarehouseExample();
            example.createCriteria().andWhCodeEqualTo(wmsWarehouseBO.getWhCode());
            List<WmsWarehouse> warehouseList = wmsWarehouseMapper.selectByExample(example);
            if (CollectionUtils.isNotEmpty(warehouseList)
                    && warehouseList.size() == 1) {
                return warehouseList.get(0);
            }
        }
        return null;
    }

    /**
     * 根据仓库ID查询单个仓库
     *
     * @return
     */
    @Override
    public WmsWarehouse findWarehouseById(WmsWarehouseBO wmsWarehouseBO) {
        if (!Objects.equals(wmsWarehouseBO, null)
                && !Objects.equals(wmsWarehouseBO.getWhId(), null)) {
            return wmsWarehouseMapper.selectByPrimaryKey(wmsWarehouseBO.getWhId());
        }
        return null;
    }

    /**
     * APP首页查询仓库
     * linbao 2017-10-09
     *
     * @return
     */
    @Override
    public List<WmsWarehouseDTO> listWarehouseForApp() throws Exception {
        return wmsWarehouseExtMapper.listWarehouseForApp();
    }
}
