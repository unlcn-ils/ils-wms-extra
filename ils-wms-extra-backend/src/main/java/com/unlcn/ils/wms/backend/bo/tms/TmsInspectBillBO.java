package com.unlcn.ils.wms.backend.bo.tms;

/**
 * @Author ：Ligl
 * @Date : 2017/9/12.
 */
public class TmsInspectBillBO {
    private String vin;
    private String motorno;
    private String dn;
    private String orderno;
    private String warehouse;
    private String transMode;
    private String createDate;
    private String destcity;
    private String bplan;
    private String style;
    private String cacheWarehouse;
    private String province;

    public String getVin() {
        return vin;
    }

    public void setVin(String vin) {
        this.vin = vin;
    }

    public String getMotorno() {
        return motorno;
    }

    public void setMotorno(String motorno) {
        this.motorno = motorno;
    }

    public String getDn() {
        return dn;
    }

    public void setDn(String dn) {
        this.dn = dn;
    }

    public String getOrderno() {
        return orderno;
    }

    public void setOrderno(String orderno) {
        this.orderno = orderno;
    }

    public String getWarehouse() {
        return warehouse;
    }

    public void setWarehouse(String warehouse) {
        this.warehouse = warehouse;
    }

    public String getTransMode() {
        return transMode;
    }

    public void setTransMode(String transMode) {
        this.transMode = transMode;
    }

    public String getCreateDate() {
        return createDate;
    }

    public void setCreateDate(String createDate) {
        this.createDate = createDate;
    }

    public String getDestcity() {
        return destcity;
    }

    public void setDestcity(String destcity) {
        this.destcity = destcity;
    }

    public String getBplan() {
        return bplan;
    }

    public void setBplan(String bplan) {
        this.bplan = bplan;
    }

    public String getStyle() {
        return style;
    }

    public void setStyle(String style) {
        this.style = style;
    }

    public String getCacheWarehouse() {
        return cacheWarehouse;
    }

    public void setCacheWarehouse(String cacheWarehouse) {
        this.cacheWarehouse = cacheWarehouse;
    }

    public String getProvince() {
        return province;
    }

    public void setProvince(String province) {
        this.province = province;
    }

    @Override
    public String toString() {
        return "TmsInspectBillBO{" +
                "vin='" + vin + '\'' +
                ", motorno='" + motorno + '\'' +
                ", dn='" + dn + '\'' +
                ", orderno='" + orderno + '\'' +
                ", warehouse='" + warehouse + '\'' +
                ", transMode='" + transMode + '\'' +
                ", createDate='" + createDate + '\'' +
                ", destcity='" + destcity + '\'' +
                ", bplan='" + bplan + '\'' +
                ", style='" + style + '\'' +
                ", cacheWarehouse='" + cacheWarehouse + '\'' +
                ", province='" + province + '\'' +
                '}';
    }
}
