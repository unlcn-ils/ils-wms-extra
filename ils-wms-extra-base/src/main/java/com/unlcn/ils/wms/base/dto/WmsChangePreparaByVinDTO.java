package com.unlcn.ils.wms.base.dto;

import java.io.Serializable;

public class WmsChangePreparaByVinDTO implements Serializable {
    private String changeVin;
    private Long planDetailId;
    private Long planId;
    private Long taskId;
    private String whCode;

    public String getChangeVin() {
        return changeVin;
    }

    public void setChangeVin(String changeVin) {
        this.changeVin = changeVin;
    }

    public Long getPlanDetailId() {
        return planDetailId;
    }

    public void setPlanDetailId(Long planDetailId) {
        this.planDetailId = planDetailId;
    }

    public Long getPlanId() {
        return planId;
    }

    public void setPlanId(Long planId) {
        this.planId = planId;
    }

    public Long getTaskId() {
        return taskId;
    }

    public void setTaskId(Long taskId) {
        this.taskId = taskId;
    }

    public String getWhCode() {
        return whCode;
    }

    public void setWhCode(String whCode) {
        this.whCode = whCode;
    }
}
