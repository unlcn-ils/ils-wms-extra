package com.unlcn.ils.wms.api.activemq;

import java.io.Serializable;

/**
 * 客户信息
 * 
 * @author
 * @generated
 */
public class CustomerInfo implements Serializable {
	private static final long serialVersionUID = 1689158224150528L;
	/**
	 *
	 */
	private String id;

	/**
	 * 客户编号
	 */
	private String code;

	/**
	 * 名称
	 */
	private String name;

	/**
	 * 客户类型，1：个人、2：企业
	 */
	private String type;

	/**
	 * 证件类型，1：身份证，2：营业执照
	 */
	private String idType;

	/**
	 * 证件号码
	 */
	private String idNumber;

	/**
	 * 证件照片
	 */
	private String idFilepath;

	/**
	 * 归属省份
	 */
	private String province;

	/**
	 * 归属城市
	 */
	private String city;

	/**
	 * 详细地址
	 */
	private String address;

	/**
	 * 联系人
	 */
	private String linkman;

	/**
	 * 手机
	 */
	private String phone;

	/**
	 * 联系电话
	 */
	private String tel;

	/**
	 * 添加人
	 */
	private String operUser;

	/**
	 * 添加时间
	 */
	private java.sql.Timestamp operTime;

	/**
	 * 状态：1：生效，0：失效
	 */
	private String status;

	/**
	 * 设置
	 */
	public void setId(String id) {
		this.id = id;
	}

	/**
	 * 获取
	 */
	public String getId() {
		return id;
	}

	/**
	 * 设置客户编号
	 */
	public void setCode(String code) {
		this.code = code;
	}

	/**
	 * 获取客户编号
	 */
	public String getCode() {
		return code;
	}

	/**
	 * 设置名称
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * 获取名称
	 */
	public String getName() {
		return name;
	}

	/**
	 * 设置客户类型，1：个人、2：企业
	 */
	public void setType(String type) {
		this.type = type;
	}

	/**
	 * 获取客户类型，1：个人、2：企业
	 */
	public String getType() {
		return type;
	}

	/**
	 * 设置证件类型，1：身份证，2：营业执照
	 */
	public void setIdType(String idType) {
		this.idType = idType;
	}

	/**
	 * 获取证件类型，1：身份证，2：营业执照
	 */
	public String getIdType() {
		return idType;
	}

	/**
	 * 设置证件号码
	 */
	public void setIdNumber(String idNumber) {
		this.idNumber = idNumber;
	}

	/**
	 * 获取证件号码
	 */
	public String getIdNumber() {
		return idNumber;
	}

	/**
	 * 设置证件照片
	 */
	public void setIdFilepath(String idFilepath) {
		this.idFilepath = idFilepath;
	}

	/**
	 * 获取证件照片
	 */
	public String getIdFilepath() {
		return idFilepath;
	}

	/**
	 * 设置归属省份
	 */
	public void setProvince(String province) {
		this.province = province;
	}

	/**
	 * 获取归属省份
	 */
	public String getProvince() {
		return province;
	}

	/**
	 * 设置归属城市
	 */
	public void setCity(String city) {
		this.city = city;
	}

	/**
	 * 获取归属城市
	 */
	public String getCity() {
		return city;
	}

	/**
	 * 设置详细地址
	 */
	public void setAddress(String address) {
		this.address = address;
	}

	/**
	 * 获取详细地址
	 */
	public String getAddress() {
		return address;
	}

	/**
	 * 设置联系人
	 */
	public void setLinkman(String linkman) {
		this.linkman = linkman;
	}

	/**
	 * 获取联系人
	 */
	public String getLinkman() {
		return linkman;
	}

	/**
	 * 设置手机
	 */
	public void setPhone(String phone) {
		this.phone = phone;
	}

	/**
	 * 获取手机
	 */
	public String getPhone() {
		return phone;
	}

	/**
	 * 设置联系电话
	 */
	public void setTel(String tel) {
		this.tel = tel;
	}

	/**
	 * 获取联系电话
	 */
	public String getTel() {
		return tel;
	}

	/**
	 * 设置添加人
	 */
	public void setOperUser(String operUser) {
		this.operUser = operUser;
	}

	/**
	 * 获取添加人
	 */
	public String getOperUser() {
		return operUser;
	}

	/**
	 * 设置添加时间
	 */
	public void setOperTime(java.sql.Timestamp operTime) {
		this.operTime = operTime;
	}

	/**
	 * 获取添加时间
	 */
	public java.sql.Timestamp getOperTime() {
		return operTime;
	}

	/**
	 * 设置状态：1：生效，0：失效
	 */
	public void setStatus(String status) {
		this.status = status;
	}

	/**
	 * 获取状态：1：生效，0：失效
	 */
	public String getStatus() {
		return status;
	}
}