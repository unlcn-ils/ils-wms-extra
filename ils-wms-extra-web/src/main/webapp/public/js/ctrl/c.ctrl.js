/**
 * Created by houjianhui on 2017/7/19.
 */
'use strict';

/*
kasCtrl.controller('CustomerCtrl', ['$rootScope', '$scope', 'Settings', 'CustomerService','$http', function ($rootScope, $scope, Settings, CustomerService,$http) {

    //分页函数
    $rootScope.getData_clipPage = function (num) {
        $scope.initData(num)
    };

    $scope.queryObj = {
        customerOrderCode: '',
        orderCode: '',
        vehicleVin: ''
    };

    $scope.customerOtdList = [];

    $scope.initData = function (num) {
        CustomerService.list({
            pageNo: num || 1,
            pageSize: $rootScope.pageSize || 10,
            customerOrderCode: $scope.queryObj.customerOrderCode,
            orderCode: $scope.queryObj.orderCode,
            vehicleVin: $scope.queryObj.vehicleVin,
            pickBeginDate: $('#pickBeginDate').val(),
            pickEndDate: $('#pickEndDate').val(),
            transportType: $('#transportType').val(),
            orderStatus: $('#orderStatus').val()
        }).success(function (result) {
            if (result.success) {
                $scope.customerOtdList = result.data;
                $rootScope.totalPage = result.pageVo.totalPage;
                $rootScope.totalRecord = result.pageVo.totalRecord;
                $rootScope.rootNewClip('#clipPage', $rootScope.getData_clipPage);
            }
        })
    };

    $scope.asnOrder = [];
    $scope.queryCustomer = function () {


        $http({
            method: 'GET',
            //url: '/public/js/data/asnOrder.json'
            url : Settings.Context.path + '/asnOrder/getQueryResult'
        }).success(function(data,status,headers,config) {
            $scope.inviteData=[];debugger;
            angular.forEach(data.data.asnOrderList,function (record) {
                $scope.asnOrder.push({
                    asnBillNo:record.asnBillNo,
                    asnBillType:record.asnBillType,
                    asnCheckDesc:record.asnCheckDesc,
                    asnCheckResult:record.asnCheckResult,
                    asnCode:record.asnCode,
                    asnConsigneeDate:record.asnConsigneeDate,
                    asnDest:record.asnDest,
                    asnDispatchNo:record.asnDispatchNo
                });
            });
            $rootScope.$broadcast('asnOrder',$scope.asnOrder);
        }).error(function(data,status,headers,config) {

        });
        //$scope.initData();
    };

    $scope.cleanQuery = function () {

        $scope.queryObj = {
            customerOrderCode: '',
            orderCode: '',
            vehicleVin: ''
        };
        $('#pickBeginDate').val('');
        $('#pickEndDate').val('');
        $('#transportType').val('');
        $('#orderStatus').val('');
        $rootScope.totalPage = 0;
        $rootScope.pageNo = 1;
        $rootScope.curr = 0;
        $scope.initData();
    };

    $rootScope.form.render();

    $scope.pickBeginDate = function (event) {
        Tools.beginDate.elem = event.target;
        $rootScope.laydate(Tools.beginDate);
    };

    $scope.pickEndDate = function (event) {
        Tools.endDate.elem = event.target;
        $rootScope.laydate(Tools.endDate);
    };

   $scope.exportExcel = function () {
        var pickBeginDate = $('#pickBeginDate').val();
        var pickEndDate = $('#pickEndDate').val();
        var transportType = $('#transportType').val();
        var orderStatus = $('#orderStatus').val();
        window.open(Settings.Context.path + "/otd/exportExecl?customerOrderCode="
            + $scope.queryObj.customerOrderCode + "&vehicleVin="
            + $scope.queryObj.vehicleVin + "&pickBeginDate="
            + pickBeginDate + "&pickEndDate="
            + pickEndDate + "&orderCode="
            + $scope.queryObj.orderCode + "&transportType="
            + transportType + "&orderStatus="
            + orderStatus);
    };

    $scope.initData();

}]);*/
