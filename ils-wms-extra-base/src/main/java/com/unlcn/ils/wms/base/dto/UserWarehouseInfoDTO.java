package com.unlcn.ils.wms.base.dto;

/**
 * Created by lenovo on 2017/11/13.
 */
public class UserWarehouseInfoDTO {


    // 仓库code
    private String whCode;

    // 仓库名称
    private String whName;

    // 仓库类型
    private String whType;

    // 仓库地址
    private String whAddress;


    public String getWhCode() {
        return whCode;
    }

    public void setWhCode(String whCode) {
        this.whCode = whCode;
    }

    public String getWhName() {
        return whName;
    }

    public void setWhName(String whName) {
        this.whName = whName;
    }

    public String getWhType() {
        return whType;
    }

    public void setWhType(String whType) {
        this.whType = whType;
    }

    public String getWhAddress() {
        return whAddress;
    }

    public void setWhAddress(String whAddress) {
        this.whAddress = whAddress;
    }

    @Override
    public String toString() {
        return "UserWarehouseInfoDTO{" +
                "whCode='" + whCode + '\'' +
                ", whName='" + whName + '\'' +
                ", whType='" + whType + '\'' +
                ", whAddress='" + whAddress + '\'' +
                '}';
    }
}
