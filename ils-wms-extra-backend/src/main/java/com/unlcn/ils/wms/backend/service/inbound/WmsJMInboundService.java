package com.unlcn.ils.wms.backend.service.inbound;

import com.unlcn.ils.wms.base.businessDTO.inbound.AsnOrderDTO;
import org.springframework.web.multipart.MultipartFile;

import java.util.ArrayList;

public interface WmsJMInboundService {

    void saveImportAsnByExcel(String whCode, String userId, MultipartFile file) throws Exception;

    void saveImportAsnByExcelOld(String whCode, String userId, MultipartFile file) throws Exception;

    ArrayList<Object> getAsnQrCode(String whCode, String[] atId) throws Exception;

    ArrayList<Object> getAsnQrCodeOld(String whCode, String[] atId) throws Exception;

    AsnOrderDTO saveAllcationByExtra(String vin, String userId) throws Exception;

    void updateSendMessageToAPP(AsnOrderDTO result, String userId) throws Exception;

    void updateOutOfStorageDcs() throws Exception;

    void updateSyncTmsOrderToNotice();

    void updateOutOfStorageExcpForDcs() throws Exception;

    void updateOutOfStorageSap() throws Exception;

    void updateSyncWmsAsnTempToNotice();

    void updateOutOfStorageExcpForSap() throws Exception;
}
