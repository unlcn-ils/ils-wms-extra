package com.unlcn.ils.wms.web.getCustomer;

import java.io.Serializable;

/**
 * 客户等级
 * 
 * @author
 * @generated
 */
public class CustomerLevel implements Serializable {
	private static final long serialVersionUID = 1689138730516480L;
	/**
	 *
	 */
	private String id;

	/**
	 * 等级类型
	 */
	private String levelType;

	/**
	 * 等级级别，对应等级的值，如3级积分1000
	 */
	private String levelValue;

	/**
	 * 显示名称
	 */
	private String levelName;

	/**
	 * 等级图标
	 */
	private String levelIcon;

	/**
	 * 等级说明
	 */
	private String levelDesc;

	/**
	 * 等级规则
	 */
	private String levelRule;

	/**
	 * 添加人
	 */
	private String operUser;

	/**
	 * 添加时间
	 */
	private java.sql.Timestamp operTime;

	/**
	 *
	 */
	private String status;

	/**
	 * 设置
	 */
	public void setId(String id) {
		this.id = id;
	}

	/**
	 * 获取
	 */
	public String getId() {
		return id;
	}

	/**
	 * 设置等级类型
	 */
	public void setLevelType(String levelType) {
		this.levelType = levelType;
	}

	/**
	 * 获取等级类型
	 */
	public String getLevelType() {
		return levelType;
	}

	/**
	 * 设置等级级别，对应等级的值，如3级积分1000
	 */
	public void setLevelValue(String levelValue) {
		this.levelValue = levelValue;
	}

	/**
	 * 获取等级级别，对应等级的值，如3级积分1000
	 */
	public String getLevelValue() {
		return levelValue;
	}

	/**
	 * 设置显示名称
	 */
	public void setLevelName(String levelName) {
		this.levelName = levelName;
	}

	/**
	 * 获取显示名称
	 */
	public String getLevelName() {
		return levelName;
	}

	/**
	 * 设置等级图标
	 */
	public void setLevelIcon(String levelIcon) {
		this.levelIcon = levelIcon;
	}

	/**
	 * 获取等级图标
	 */
	public String getLevelIcon() {
		return levelIcon;
	}

	/**
	 * 设置等级说明
	 */
	public void setLevelDesc(String levelDesc) {
		this.levelDesc = levelDesc;
	}

	/**
	 * 获取等级说明
	 */
	public String getLevelDesc() {
		return levelDesc;
	}

	/**
	 * 设置等级规则
	 */
	public void setLevelRule(String levelRule) {
		this.levelRule = levelRule;
	}

	/**
	 * 获取等级规则
	 */
	public String getLevelRule() {
		return levelRule;
	}

	/**
	 * 设置添加人
	 */
	public void setOperUser(String operUser) {
		this.operUser = operUser;
	}

	/**
	 * 获取添加人
	 */
	public String getOperUser() {
		return operUser;
	}

	/**
	 * 设置添加时间
	 */
	public void setOperTime(java.sql.Timestamp operTime) {
		this.operTime = operTime;
	}

	/**
	 * 获取添加时间
	 */
	public java.sql.Timestamp getOperTime() {
		return operTime;
	}

	/**
	 * 设置
	 */
	public void setStatus(String status) {
		this.status = status;
	}

	/**
	 * 获取
	 */
	public String getStatus() {
		return status;
	}
}