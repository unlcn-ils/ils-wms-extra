package com.unlcn.ils.wms.web.controller.inbound;

import cn.huiyunche.commons.domain.ResultDTO;
import com.unlcn.ils.wms.backend.bo.inboundBO.WmsInboundPloyBO;
import com.unlcn.ils.wms.backend.bo.inboundBO.WmsInboundPloyConditionBO;
import com.unlcn.ils.wms.backend.bo.inboundBO.WmsInboundPloyFlowtoBO;
import com.unlcn.ils.wms.backend.bo.inboundBO.WmsInboundPloyLocationBO;
import com.unlcn.ils.wms.backend.service.inbound.WmsInboundPloyService;
import com.unlcn.ils.wms.base.model.inbound.WmsInboundPloy;
import com.unlcn.ils.wms.base.model.inbound.WmsInboundPloyCondition;
import com.unlcn.ils.wms.base.model.inbound.WmsInboundPloyFlowto;
import com.unlcn.ils.wms.base.model.inbound.WmsInboundPloyLocation;
import com.unlcn.ils.wms.web.utils.Vo2Bo;
import com.unlcn.ils.wms.web.vo.inbound.WmsInboundPloyConditionVO;
import com.unlcn.ils.wms.web.vo.inbound.WmsInboundPloyFlowtoVO;
import com.unlcn.ils.wms.web.vo.inbound.WmsInboundPloyLocationVO;
import com.unlcn.ils.wms.web.vo.inbound.WmsInboundPloyVO;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.Comparator;
import java.util.List;

/**
 * 策略管理、新增、修改、删除策略
 * Created by DELL on 2017/8/7.
 */
@Controller
@RequestMapping("/inboundPloy")
@ResponseBody
public class WmsInboundPloyController {

    @Autowired
    private WmsInboundPloyService wmsInboundPloyService;

    /**
     * 策略主信息
     * @return
     */
    @RequestMapping(value="/getPloyList/{whCode}")
    public ResultDTO getPloyList(@PathVariable("whCode")String whCode){
        ResultDTO resultDTO = new ResultDTO();
        try{
            List<WmsInboundPloy> wmsInboundPloyList = wmsInboundPloyService.getPloyList(whCode);
            resultDTO.setData(wmsInboundPloyList);
            resultDTO.setSuccess(true);
            return resultDTO;
        }catch(Exception ex){
            resultDTO.setSuccess(false);
            return resultDTO;
        }
    }

    /**
     * 新增主策略
     * @param wmsInboundPloyVO
     * @return
     */
    @RequestMapping(value="/addPloy",method = RequestMethod.POST)
    public ResultDTO addPloy(@RequestBody WmsInboundPloyVO wmsInboundPloyVO){
        ResultDTO resultDTO = new ResultDTO();
        try{
            WmsInboundPloyBO wmsInboundPloyBO = new WmsInboundPloyBO();
            BeanUtils.copyProperties(wmsInboundPloyVO,wmsInboundPloyBO);
            wmsInboundPloyService.addPloy(wmsInboundPloyBO);
            resultDTO.setMessage("新增策略成功");
            resultDTO.setSuccess(true);
            return resultDTO;
        }catch(Exception ex){
            resultDTO.setMessage("新增策略异常");
            resultDTO.setSuccess(false);
            return resultDTO;
        }
    }

    /**
     * 更新主策略
     * @param wmsInboundPloyVO
     * @return
     */
        @RequestMapping(value="/updatePloy",method = RequestMethod.POST)
    public ResultDTO updatePloy(@RequestBody WmsInboundPloyVO wmsInboundPloyVO){
        ResultDTO resultDTO = new ResultDTO();
        try{
            WmsInboundPloyBO wmsInboundPloyBO = new WmsInboundPloyBO();
            BeanUtils.copyProperties(wmsInboundPloyVO,wmsInboundPloyBO);
            wmsInboundPloyService.updatePloy(wmsInboundPloyBO);
            resultDTO.setMessage("修改策略成功");
            resultDTO.setSuccess(true);
            return resultDTO;
        }catch(Exception ex){
            resultDTO.setMessage("修改策略异常");
            resultDTO.setSuccess(false);
            return resultDTO;
        }
    }

    @RequestMapping(value="/deletePloy",method = RequestMethod.POST)
    public ResultDTO deletePloy(@RequestBody List<WmsInboundPloyVO> wmsInboundPloyVO){
        ResultDTO resultDTO = new ResultDTO();
        try{
            Vo2Bo vo2Bo = new Vo2Bo();
            List<WmsInboundPloyBO> wmsInboundPloyBOList = vo2Bo.convert(wmsInboundPloyVO,WmsInboundPloyBO.class);
            wmsInboundPloyService.deletePloy(wmsInboundPloyBOList);
            resultDTO.setMessage("删除策略成功");
            resultDTO.setSuccess(true);
            return resultDTO;
        }catch(Exception ex){
            resultDTO.setMessage("删除策略异常");
            resultDTO.setSuccess(false);
            return resultDTO;
        }
    }

    /**
     * 策略条件信息
     * @return
     */
    @RequestMapping(value="/getPloyConditionList/{ployCode}")
    public ResultDTO getPloyConditionList(@PathVariable("ployCode") String ployCode){
        ResultDTO resultDTO = new ResultDTO();
        try{
            List<WmsInboundPloyCondition> wmsInboundPloyConditionList = wmsInboundPloyService.getPloyConditionList(ployCode);
            resultDTO.setData(wmsInboundPloyConditionList);
            resultDTO.setSuccess(true);
            return resultDTO;
        }catch(Exception ex){
            resultDTO.setSuccess(false);
            return resultDTO;
        }
    }

    /**
     * 新增策略条件
     * @param wmsInboundPloyConditionVOList
     * @return
     */
    @RequestMapping(value="/addPloyCondition",method = RequestMethod.POST)
    public ResultDTO addPloyCondition(@RequestBody List<WmsInboundPloyConditionVO> wmsInboundPloyConditionVOList){
        ResultDTO resultDTO = new ResultDTO();
        try{
            Vo2Bo vo2Bo = new Vo2Bo();
            List<WmsInboundPloyConditionBO> wmsInboundPloyConditionBOList = vo2Bo.convert(wmsInboundPloyConditionVOList, WmsInboundPloyConditionBO.class);
            wmsInboundPloyService.addPloyCondition(wmsInboundPloyConditionBOList);
            resultDTO.setMessage("新增策略条件成功");
            resultDTO.setSuccess(true);
            return resultDTO;
        }catch(Exception ex){
            resultDTO.setMessage("新增策略条件异常");
            resultDTO.setSuccess(false);
            return resultDTO;
        }
    }

    /**
     * 删除策略条件
     * @param wmsInboundPloyConditionVOList
     * @return
     */
    @RequestMapping(value="/deletePloyCondition",method = RequestMethod.POST)
    public ResultDTO deletePloyCondition(@RequestBody List<WmsInboundPloyConditionVO> wmsInboundPloyConditionVOList){
        ResultDTO resultDTO = new ResultDTO();
        try{
            Vo2Bo vo2Bo = new Vo2Bo();
            List<WmsInboundPloyConditionBO> wmsInboundPloyConditionBOList = vo2Bo.convert(wmsInboundPloyConditionVOList, WmsInboundPloyConditionBO.class);
            wmsInboundPloyService.deletePloyCondition(wmsInboundPloyConditionBOList);
            resultDTO.setMessage("删除策略条件成功");
            resultDTO.setSuccess(true);
            return resultDTO;
        }catch(Exception ex){
            resultDTO.setMessage("删除策略条件异常");
            resultDTO.setSuccess(false);
            return resultDTO;
        }
    }

    /**
     * 策略流向信息
     * @return
     */
    @RequestMapping(value="/getPloyFlowtoList/{ployCode}")
    public ResultDTO getPloyFlowtoList(@PathVariable("ployCode") String ployCode){
        ResultDTO resultDTO = new ResultDTO();
        try{
            List<WmsInboundPloyFlowto> wmsInboundPloyFlowtoList = wmsInboundPloyService.getPloyFlowtoList(ployCode);
            resultDTO.setData(wmsInboundPloyFlowtoList);
            resultDTO.setSuccess(true);
            return resultDTO;
        }catch(Exception ex){
            resultDTO.setSuccess(false);
            return resultDTO;
        }
    }

    /**
     * 新增策略流向
     * @param wmsInboundPloyFlowtoVOList
     * @return
     */
    @RequestMapping(value="/addPloyFlowto",method = RequestMethod.POST)
    public ResultDTO addPloyFlowto(@RequestBody List<WmsInboundPloyFlowtoVO> wmsInboundPloyFlowtoVOList){
        ResultDTO resultDTO = new ResultDTO();
        try{
            Vo2Bo vo2Bo = new Vo2Bo();
            List<WmsInboundPloyFlowtoBO> wmsInboundPloyFlowtoBOList = vo2Bo.convert(wmsInboundPloyFlowtoVOList, WmsInboundPloyFlowtoBO.class);
            wmsInboundPloyService.addPloyFlowto(wmsInboundPloyFlowtoBOList);
            resultDTO.setMessage("新增策略流向成功");
            resultDTO.setSuccess(true);
            return resultDTO;
        }catch(Exception ex){
            resultDTO.setMessage("新增策略流向异常");
            resultDTO.setSuccess(false);
            return resultDTO;
        }
    }

    /**
     * 删除策略条件
     * @param wmsInboundPloyFlowtoVOList
     * @return
     */
    @RequestMapping(value="/deletePloyFlowto",method = RequestMethod.POST)
    public ResultDTO deletePloyFlowto(@RequestBody List<WmsInboundPloyFlowtoVO> wmsInboundPloyFlowtoVOList){
        ResultDTO resultDTO = new ResultDTO();
        try{
            Vo2Bo vo2Bo = new Vo2Bo();
            List<WmsInboundPloyFlowtoBO> WmsInboundPloyFlowtoBOList = vo2Bo.convert(wmsInboundPloyFlowtoVOList, WmsInboundPloyFlowtoBO.class);
            wmsInboundPloyService.deletePloyFlowto(WmsInboundPloyFlowtoBOList);
            resultDTO.setMessage("删除策略流向成功");
            resultDTO.setSuccess(true);
            return resultDTO;
        }catch(Exception ex){
            resultDTO.setMessage("删除策略流向异常");
            resultDTO.setSuccess(false);
            return resultDTO;
        }
    }

    /**
     * 策略库位信息
     * @return
     */
    @RequestMapping(value="/getPloyLocationList/{ployCode}")
    public ResultDTO getPloyLocationList(@PathVariable("ployCode") String ployCode){
        ResultDTO resultDTO = new ResultDTO();
        try{
            List<WmsInboundPloyLocation> wmsInboundPloyLocationList = wmsInboundPloyService.getPloyLocationList(ployCode);
            resultDTO.setData(wmsInboundPloyLocationList);
            resultDTO.setSuccess(true);
            return resultDTO;
        }catch(Exception ex){
            resultDTO.setSuccess(false);
            return resultDTO;
        }
    }

    /**
     * 新增策略库位数据
     * @param wmsInboundPloyLocationVOList
     * @return
     */
    @RequestMapping(value="/addPloyLocation",method = RequestMethod.POST)
    public ResultDTO addPloyLocation(@RequestBody List<WmsInboundPloyLocationVO> wmsInboundPloyLocationVOList){
        ResultDTO resultDTO = new ResultDTO();
        try{
            Vo2Bo vo2Bo = new Vo2Bo();
            List<WmsInboundPloyLocationBO> wmsInboundPloyLocationBOList = vo2Bo.convert(wmsInboundPloyLocationVOList, WmsInboundPloyLocationBO.class);
            wmsInboundPloyService.addPloyLocation(wmsInboundPloyLocationBOList);
            resultDTO.setMessage("新增策略库位成功");
            resultDTO.setSuccess(true);
            return resultDTO;
        }catch(Exception ex){
            resultDTO.setMessage("新增策略库位成功");
            resultDTO.setSuccess(false);
            return resultDTO;
        }
    }

    /**
     * 删除策略条件
     * @param wmsInboundPloyLocationVOList
     * @return
     */
    @RequestMapping(value="/deletePloyLocation",method = RequestMethod.POST)
    public ResultDTO deletePloyLocation(@RequestBody List<WmsInboundPloyLocationVO> wmsInboundPloyLocationVOList){
        ResultDTO resultDTO = new ResultDTO();
        try{
            Vo2Bo vo2Bo = new Vo2Bo();
            List<WmsInboundPloyLocationBO> wmsInboundPloyLocationBOList = vo2Bo.convert(wmsInboundPloyLocationVOList, WmsInboundPloyLocationBO.class);
            wmsInboundPloyService.deletePloyLocation(wmsInboundPloyLocationBOList);
            resultDTO.setMessage("删除策略库位成功");
            resultDTO.setSuccess(true);
            return resultDTO;
        }catch(Exception ex){
            resultDTO.setMessage("删除策略库位异常");
            resultDTO.setSuccess(false);
            return resultDTO;
        }
    }


    /**
     * 生成规则
     * @param ployCode
     * @return
     */
    @RequestMapping(value="/genPloyContent/{ployCode}")
    public ResultDTO addPloyLocation(@PathVariable("ployCode")String ployCode){
        ResultDTO resultDTO = new ResultDTO();
        try {
            List<WmsInboundPloyCondition> wmsInboundPloyConditionList = wmsInboundPloyService.getPloyConditionList(ployCode);
            List<WmsInboundPloyFlowto> wmsInboundPloyFlowtoList = wmsInboundPloyService.getPloyFlowtoList(ployCode);
            List<WmsInboundPloyLocation> wmsInboundPloyLocationList = wmsInboundPloyService.getPloyLocationList(ployCode);
            String condition = genCondition(wmsInboundPloyConditionList);
            String flowto = genFlowto(wmsInboundPloyFlowtoList);
            String location = genLocation(wmsInboundPloyLocationList);
            resultDTO.setData(condition + flowto + location);
            resultDTO.setSuccess(true);
        }catch(Exception ex){
            resultDTO.setSuccess(false);
        }
        return resultDTO;
    }

    private String genCondition(List<WmsInboundPloyCondition> wmsInboundPloyConditionList){
        StringBuffer condition = new StringBuffer();
        condition.append("当");
        new Comparator<WmsInboundPloyCondition>(){
            @Override
            public int compare(WmsInboundPloyCondition o1, WmsInboundPloyCondition o2) {
                return o1.getPycType().compareTo(o2.getPycType());
            }
        };
        for(WmsInboundPloyCondition wmsInboundPloyCondition : wmsInboundPloyConditionList){
            if(wmsInboundPloyCondition.getPycType().equals("10") && !condition.toString().contains("货主")) {
                condition.append("货主");
            }
            if(wmsInboundPloyCondition.getPycType().equals("20") && !condition.toString().contains("车型")){
                condition.append("车型");
            }
            switch (wmsInboundPloyCondition.getPycOperatorCode()) {
                case "10"://等于
                    condition.append("等于");
                    break;
                case "20"://不等于
                    condition.append("不等于");
                    break;
                case "30"://包含
                    condition.append("包含");
                    break;
                case "40"://不包含
                    condition.append("不包含");
                    break;
                case "50"://是
                    condition.append("是");
                    break;
                case "60"://不是
                    condition.append("不是");
                    break;
                case "70"://大于等于
                    condition.append("大于等于");
                    break;
                case "80"://小于等于
                    condition.append("小于等于");
                    break;
            }
            condition.append(wmsInboundPloyCondition.getPycName());
        }
        return condition.append(";").toString();
    }

    private String genFlowto(List<WmsInboundPloyFlowto> wmsInboundPloyFlowtoList){
        StringBuffer genFlowto = new StringBuffer();
        StringBuffer origin = new StringBuffer();
        StringBuffer dest = new StringBuffer();

        for(WmsInboundPloyFlowto wmsInboundPloyFlowto : wmsInboundPloyFlowtoList){
            origin.append(wmsInboundPloyFlowto.getPyfFromName()+";");
            dest.append(wmsInboundPloyFlowto.getPyfToName()+";");
        }
        return genFlowto.append("起始地为：").append(origin.toString()).append("目的地为：").append(dest.toString()).append(";").toString();
    }

    private String genLocation(List<WmsInboundPloyLocation> wmsInboundPloyLocationList){
        StringBuffer location = new StringBuffer();
        location.append("分配的库区为");
        for(WmsInboundPloyLocation wmsInboundPloyLocation : wmsInboundPloyLocationList){
            location.append(wmsInboundPloyLocation.getPylZoneCode());
            location.append("库位从").append(wmsInboundPloyLocation.getPylStartRow())
                    .append("排到").append(wmsInboundPloyLocation.getPylEndRow()).append("排");
        }
        return location.toString();
    }
}