package com.unlcn.ils.wms.backend.enums;

public enum WmsShipmentSourceEnum {
    //10-HTTP定时任务调度  20-mq修改数据推送  30-mq修改新增数据
    HTTP("10", "HTTP定时任务调度"),
    MQ_MODIFY("20", "mq修改数据推送"),
    MQ_ADD("30", "mq修改新增数据");

    private final String value;
    private final String text;

    WmsShipmentSourceEnum(String value, String text) {
        this.value = value;
        this.text = text;
    }

    /**
     * @return the value
     */
    public String getValue() {
        return value;
    }

    /**
     * @return the text
     */
    public String getText() {
        return text;
    }

    public static WmsShipmentSourceEnum getByValue(String value) {
        for (WmsShipmentSourceEnum temp : WmsShipmentSourceEnum.values()) {
            if (temp.getValue().equals(value)) {
                return temp;
            }
        }
        return null;
    }

}
