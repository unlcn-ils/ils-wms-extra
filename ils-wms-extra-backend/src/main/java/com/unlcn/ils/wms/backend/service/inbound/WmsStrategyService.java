package com.unlcn.ils.wms.backend.service.inbound;

import com.unlcn.ils.wms.backend.bo.inboundBO.WmsAllocationBO;
import com.unlcn.ils.wms.backend.bo.inboundBO.WmsInboundAllocationBO;
import com.unlcn.ils.wms.backend.bo.inboundBO.WmsInboundOrderBO;
import com.unlcn.ils.wms.base.businessDTO.inbound.AsnOrderDTO;

import java.util.List;

/**
 * Created by DELL on 2017/8/8.
 */
public interface WmsStrategyService {


    /**
     * 分配操作
     * @param wmsInboundAllocationBO
     * @return
     */
    String addAllocation(WmsInboundAllocationBO wmsInboundAllocationBO) throws Exception;

    /**
     * 自动分配
     * @param wmsAllocationVOList
     * @param strategyId
     */
    List<WmsAllocationBO> updateAutoAllocation(List<WmsAllocationBO> wmsAllocationVOList, String strategyId)throws Exception;

    /**
     * 分配确认操作 确认运单分配库位完成，若修改只能在库存概况菜单下修改
     * @param wmsInboundOrderBOList
     * @return
     */
    boolean updateToConfirmAllocation(List<WmsInboundOrderBO> wmsInboundOrderBOList);

    /**
     * 西南库跟君马库的逻辑
     * @param vin
     * @param userId
     * @return
     * @throws Exception
     */
    AsnOrderDTO saveAllcationByExtra(String vin, String userId) throws Exception;


    /**
     * 西南库跟君马库的逻辑
     * @param vin
     * @param userId
     * @return
     * @throws Exception
     */
    AsnOrderDTO saveAllcationByExtraForJunMa(String vin, String userId) throws Exception;

    void saveBarcodeKey(Long odId,String vin,String key);
}
