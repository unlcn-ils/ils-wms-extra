package com.unlcn.ils.wms.api.outbound.quartz;

import com.unlcn.ils.wms.backend.service.inbound.AsnOrderService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component("inboundConfirmJob1")
public class QuartzInboundConfirm1 {

    private Logger logger = LoggerFactory.getLogger(QuartzInboundConfirm1.class);

    @Autowired
    private AsnOrderService asnOrderService;

    public void inboundConfirm1() throws Exception {
        logger.info("QuartzInboundConfirm1.inboundConfirm1 定时任务start========");
        asnOrderService.getInboundSendMailData();
        logger.info("QuartzInboundConfirm1.inboundConfirm1 定时任务end========");
    }
}
