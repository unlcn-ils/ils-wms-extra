package com.unlcn.ils.wms.backend.enums;

public enum WmsGateControllerTypeEnum {
    GATE_IN("IN", "入场开闸"),
    GATE_OUT("OUT", "出场开闸");

    private final String code;
    private final String text;

    WmsGateControllerTypeEnum(String value, String text) {
        this.code = value;
        this.text = text;
    }

    public String getCode() {
        return code;
    }

    public String getText() {
        return text;
    }

    public static WmsGateControllerTypeEnum getByValue(String value) {
        for (WmsGateControllerTypeEnum temp : WmsGateControllerTypeEnum.values()) {
            if (temp.getCode().equals(value)) {
                return temp;
            }
        }
        return null;
    }
}
