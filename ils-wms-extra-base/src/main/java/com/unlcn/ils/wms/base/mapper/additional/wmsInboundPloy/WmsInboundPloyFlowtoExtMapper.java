package com.unlcn.ils.wms.base.mapper.additional.wmsInboundPloy;

import com.unlcn.ils.wms.base.model.inbound.WmsInboundPloyFlowto;

import java.util.List;

/**
 * Created by DELL on 2017/9/1.
 */
public interface WmsInboundPloyFlowtoExtMapper {
    /**
     * 根据主策略CODE查询策略流向
     * @param ployCode
     * @return
     */
    List<WmsInboundPloyFlowto> getPloyFlowtoList(String ployCode);
}
