package com.unlcn.ils.wms.app.vo.wms;

/**
 * Created by houjianhui on 2017/5/8.
 */
public class OutboundExcpDetailVO {

    private Long excpId;

    private String excpAttachKeys;

    private String excpContent;

    public Long getExcpId() {
        return excpId;
    }

    public void setExcpId(Long excpId) {
        this.excpId = excpId;
    }

    public String getExcpAttachKeys() {
        return excpAttachKeys;
    }

    public void setExcpAttachKeys(String excpAttachKeys) {
        this.excpAttachKeys = excpAttachKeys;
    }

    public String getExcpContent() {
        return excpContent;
    }

    public void setExcpContent(String excpContent) {
        this.excpContent = excpContent;
    }

    @Override
    public String toString() {
        return "OutboundExcpDetailVO{" +
                "excpId=" + excpId +
                ", excpAttachKeys='" + excpAttachKeys + '\'' +
                ", excpContent='" + excpContent + '\'' +
                '}';
    }
}
