package com.unlcn.ils.wms.backend.service.sys;

import com.unlcn.ils.wms.backend.bo.sys.SysUserBO;
import com.unlcn.ils.wms.backend.dto.sysDTO.UserDTO;
import com.unlcn.ils.wms.base.dto.SysUserDTO;
import com.unlcn.ils.wms.base.model.sys.SysUser;

import java.util.List;
import java.util.Map;

/**
 * @Auther linbao
 * @Date 2017-10-12
 */
public interface SysUserService {

    /**
     * pc登录
     *
     * @param sysUserBO
     * @throws Exception
     */
    SysUserDTO loginForWeb(SysUserBO sysUserBO) throws Exception;

    /**
     * 分页列表查询
     *
     * @param sysUserBO
     * @return
     * @throws Exception
     */
    Map<String, Object> listUser(SysUserBO sysUserBO) throws Exception;

    /**
     * 新增
     *
     * @param sysUserBO
     * @throws Exception
     */
    void addSysUser(SysUserBO sysUserBO) throws Exception;

    /**
     * 修改
     *
     * @param sysUserBO
     * @throws Exception
     */
    void updateSysUser(SysUserBO sysUserBO) throws Exception;

    /**
     * 删除
     *
     * @param sysUserBO
     * @throws Exception
     */
    void deleteSysUser(SysUserBO sysUserBO) throws Exception;

    /**
     * 判断用户名是否唯一
     *
     * @param sysUserBO
     * @return
     * @throws Exception
     */
    boolean checkUserNameUniquene(SysUserBO sysUserBO) throws Exception;

    /**
     * 获取用户
     * @param sysUserBO
     * @return
     * @throws Exception
     */
    UserDTO findSysUserById(SysUserBO sysUserBO) throws Exception;

    SysUser selectUserById(String userId) throws Exception;

    List<SysUser> getUserByWarehouseOld(SysUserBO sysUserBO);

    /**
     * 获取仓库中所有司机
     * @param sysUserBO
     * @return
     */
    List<SysUser> getUserByWarehouse(SysUserBO sysUserBO);
}
