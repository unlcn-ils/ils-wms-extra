package com.unlcn.ils.wms.backend.service.inbound;

import com.unlcn.ils.wms.base.model.inbound.WmsInboundRepair;

import java.util.List;

/**
 * @Auther linbao
 * @Date 2017-10-24
 * 维修
 */
public interface WmsInboundRepairService {

    /**
     * 根据车架号获取维修单信息
     *
     * @param vin
     * @return
     * @throws Exception
     */
    WmsInboundRepair getWmsInboundRepairByVin(String vin) throws Exception;

    /**
     * 根据维修单号获取对应的维修单集合
     *
     * @param repairNo
     * @return
     * @throws Exception
     */
    List<WmsInboundRepair> getRepairListByRepairNo(String repairNo) throws Exception;

    /**
     * 根据车架号更新出入库状态
     *
     * @param vin
     * @param status
     * @throws Exception
     */
    void updateInOutStatusByVin(String vin, String status) throws Exception;

    /**
     * 新增维修单
     *
     * @param wmsInboundRepair
     * @throws Exception
     */
    void addInboundRepair(WmsInboundRepair wmsInboundRepair) throws Exception;
}
