package com.unlcn.ils.wms.backend.enums;

/**
 * Created by DELL on 2017/8/26.
 */
public enum WmsOutboundOrderStatusEnum {
    WMS_OUTBOUND_ORDER_STATUS_NOT_GET(10,"未领取"),
    WMS_OUTBOUND_ORDER_STATUS_EXECUTING(20,"执行中"),
    WMS_OUTBOUND_ORDER_STATUS_FINISH(30,"已完成");

    private final int value;
    private final String text;
    WmsOutboundOrderStatusEnum(int value, String text){
        this.value = value;
        this.text = text;
    }

    public int getValue() {
        return value;
    }

    public String getText() {
        return text;
    }

    public static WmsOutboundOrderStatusEnum getByValue(int value){
        for (WmsOutboundOrderStatusEnum temp : WmsOutboundOrderStatusEnum.values()) {
            if(temp.getValue() == value){
                return temp;
            }
        }
        return null;
    }
}
