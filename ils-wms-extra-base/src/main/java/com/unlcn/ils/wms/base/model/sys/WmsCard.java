package com.unlcn.ils.wms.base.model.sys;

import java.io.Serializable;
import java.util.Date;

public class WmsCard implements Serializable {
    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column wms_card.id
     *
     * @mbggenerated Wed Nov 22 11:57:20 CST 2017
     */
    private Long id;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column wms_card.card_num
     *
     * @mbggenerated Wed Nov 22 11:57:20 CST 2017
     */
    private String cardNum;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column wms_card.driver_id
     *
     * @mbggenerated Wed Nov 22 11:57:20 CST 2017
     */
    private Long driverId;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column wms_card.driver_user_name
     *
     * @mbggenerated Wed Nov 22 11:57:20 CST 2017
     */
    private String driverUserName;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column wms_card.name
     *
     * @mbggenerated Wed Nov 22 11:57:20 CST 2017
     */
    private String name;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column wms_card.status
     *
     * @mbggenerated Wed Nov 22 11:57:20 CST 2017
     */
    private Integer status;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column wms_card.start_time
     *
     * @mbggenerated Wed Nov 22 11:57:20 CST 2017
     */
    private Date startTime;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column wms_card.end_time
     *
     * @mbggenerated Wed Nov 22 11:57:20 CST 2017
     */
    private Date endTime;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column wms_card.create_by
     *
     * @mbggenerated Wed Nov 22 11:57:20 CST 2017
     */
    private String createBy;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column wms_card.update_by
     *
     * @mbggenerated Wed Nov 22 11:57:20 CST 2017
     */
    private String updateBy;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column wms_card.gmt_create
     *
     * @mbggenerated Wed Nov 22 11:57:20 CST 2017
     */
    private Date gmtCreate;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column wms_card.gmt_update
     *
     * @mbggenerated Wed Nov 22 11:57:20 CST 2017
     */
    private Date gmtUpdate;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column wms_card.is_delete
     *
     * @mbggenerated Wed Nov 22 11:57:20 CST 2017
     */
    private Integer isDelete;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database table wms_card
     *
     * @mbggenerated Wed Nov 22 11:57:20 CST 2017
     */
    private static final long serialVersionUID = 1L;

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column wms_card.id
     *
     * @return the value of wms_card.id
     *
     * @mbggenerated Wed Nov 22 11:57:20 CST 2017
     */
    public Long getId() {
        return id;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column wms_card.id
     *
     * @param id the value for wms_card.id
     *
     * @mbggenerated Wed Nov 22 11:57:20 CST 2017
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column wms_card.card_num
     *
     * @return the value of wms_card.card_num
     *
     * @mbggenerated Wed Nov 22 11:57:20 CST 2017
     */
    public String getCardNum() {
        return cardNum;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column wms_card.card_num
     *
     * @param cardNum the value for wms_card.card_num
     *
     * @mbggenerated Wed Nov 22 11:57:20 CST 2017
     */
    public void setCardNum(String cardNum) {
        this.cardNum = cardNum == null ? null : cardNum.trim();
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column wms_card.driver_id
     *
     * @return the value of wms_card.driver_id
     *
     * @mbggenerated Wed Nov 22 11:57:20 CST 2017
     */
    public Long getDriverId() {
        return driverId;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column wms_card.driver_id
     *
     * @param driverId the value for wms_card.driver_id
     *
     * @mbggenerated Wed Nov 22 11:57:20 CST 2017
     */
    public void setDriverId(Long driverId) {
        this.driverId = driverId;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column wms_card.driver_user_name
     *
     * @return the value of wms_card.driver_user_name
     *
     * @mbggenerated Wed Nov 22 11:57:20 CST 2017
     */
    public String getDriverUserName() {
        return driverUserName;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column wms_card.driver_user_name
     *
     * @param driverUserName the value for wms_card.driver_user_name
     *
     * @mbggenerated Wed Nov 22 11:57:20 CST 2017
     */
    public void setDriverUserName(String driverUserName) {
        this.driverUserName = driverUserName == null ? null : driverUserName.trim();
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column wms_card.name
     *
     * @return the value of wms_card.name
     *
     * @mbggenerated Wed Nov 22 11:57:20 CST 2017
     */
    public String getName() {
        return name;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column wms_card.name
     *
     * @param name the value for wms_card.name
     *
     * @mbggenerated Wed Nov 22 11:57:20 CST 2017
     */
    public void setName(String name) {
        this.name = name == null ? null : name.trim();
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column wms_card.status
     *
     * @return the value of wms_card.status
     *
     * @mbggenerated Wed Nov 22 11:57:20 CST 2017
     */
    public Integer getStatus() {
        return status;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column wms_card.status
     *
     * @param status the value for wms_card.status
     *
     * @mbggenerated Wed Nov 22 11:57:20 CST 2017
     */
    public void setStatus(Integer status) {
        this.status = status;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column wms_card.start_time
     *
     * @return the value of wms_card.start_time
     *
     * @mbggenerated Wed Nov 22 11:57:20 CST 2017
     */
    public Date getStartTime() {
        return startTime;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column wms_card.start_time
     *
     * @param startTime the value for wms_card.start_time
     *
     * @mbggenerated Wed Nov 22 11:57:20 CST 2017
     */
    public void setStartTime(Date startTime) {
        this.startTime = startTime;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column wms_card.end_time
     *
     * @return the value of wms_card.end_time
     *
     * @mbggenerated Wed Nov 22 11:57:20 CST 2017
     */
    public Date getEndTime() {
        return endTime;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column wms_card.end_time
     *
     * @param endTime the value for wms_card.end_time
     *
     * @mbggenerated Wed Nov 22 11:57:20 CST 2017
     */
    public void setEndTime(Date endTime) {
        this.endTime = endTime;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column wms_card.create_by
     *
     * @return the value of wms_card.create_by
     *
     * @mbggenerated Wed Nov 22 11:57:20 CST 2017
     */
    public String getCreateBy() {
        return createBy;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column wms_card.create_by
     *
     * @param createBy the value for wms_card.create_by
     *
     * @mbggenerated Wed Nov 22 11:57:20 CST 2017
     */
    public void setCreateBy(String createBy) {
        this.createBy = createBy == null ? null : createBy.trim();
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column wms_card.update_by
     *
     * @return the value of wms_card.update_by
     *
     * @mbggenerated Wed Nov 22 11:57:20 CST 2017
     */
    public String getUpdateBy() {
        return updateBy;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column wms_card.update_by
     *
     * @param updateBy the value for wms_card.update_by
     *
     * @mbggenerated Wed Nov 22 11:57:20 CST 2017
     */
    public void setUpdateBy(String updateBy) {
        this.updateBy = updateBy == null ? null : updateBy.trim();
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column wms_card.gmt_create
     *
     * @return the value of wms_card.gmt_create
     *
     * @mbggenerated Wed Nov 22 11:57:20 CST 2017
     */
    public Date getGmtCreate() {
        return gmtCreate;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column wms_card.gmt_create
     *
     * @param gmtCreate the value for wms_card.gmt_create
     *
     * @mbggenerated Wed Nov 22 11:57:20 CST 2017
     */
    public void setGmtCreate(Date gmtCreate) {
        this.gmtCreate = gmtCreate;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column wms_card.gmt_update
     *
     * @return the value of wms_card.gmt_update
     *
     * @mbggenerated Wed Nov 22 11:57:20 CST 2017
     */
    public Date getGmtUpdate() {
        return gmtUpdate;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column wms_card.gmt_update
     *
     * @param gmtUpdate the value for wms_card.gmt_update
     *
     * @mbggenerated Wed Nov 22 11:57:20 CST 2017
     */
    public void setGmtUpdate(Date gmtUpdate) {
        this.gmtUpdate = gmtUpdate;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column wms_card.is_delete
     *
     * @return the value of wms_card.is_delete
     *
     * @mbggenerated Wed Nov 22 11:57:20 CST 2017
     */
    public Integer getIsDelete() {
        return isDelete;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column wms_card.is_delete
     *
     * @param isDelete the value for wms_card.is_delete
     *
     * @mbggenerated Wed Nov 22 11:57:20 CST 2017
     */
    public void setIsDelete(Integer isDelete) {
        this.isDelete = isDelete;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table wms_card
     *
     * @mbggenerated Wed Nov 22 11:57:20 CST 2017
     */
    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", id=").append(id);
        sb.append(", cardNum=").append(cardNum);
        sb.append(", driverId=").append(driverId);
        sb.append(", driverUserName=").append(driverUserName);
        sb.append(", name=").append(name);
        sb.append(", status=").append(status);
        sb.append(", startTime=").append(startTime);
        sb.append(", endTime=").append(endTime);
        sb.append(", createBy=").append(createBy);
        sb.append(", updateBy=").append(updateBy);
        sb.append(", gmtCreate=").append(gmtCreate);
        sb.append(", gmtUpdate=").append(gmtUpdate);
        sb.append(", isDelete=").append(isDelete);
        sb.append("]");
        return sb.toString();
    }
}