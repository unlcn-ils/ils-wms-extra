package com.unlcn.ils.wms.base.businessDTO.stock;

import java.util.Date;
import java.util.List;

/**
 * Created by DELL on 2017/8/15.
 */
public class WmsInventoryDTO {

    private Long invId;

    private String invWhCode;

    private String invWhName;

    private String invCustomerCode;

    private String invCustomerName;

    private String invVehicleBrandCode;

    private String invVehicleBrandName;

    private String invVehicleSeriesCode;

    private String invVehicleSeriesName;

    private String invVehicleSpecCode;

    private String invVehicleSpecName;

    private String invVehicleSpecDesc;

    private Long invNum;

    private String invVin;

    private List<WmsInventoryLocationDTO> wmsInventoryLocationDTOList;

    public Long getInvId() {
        return invId;
    }

    public void setInvId(Long invId) {
        this.invId = invId;
    }

    public String getInvWhCode() {
        return invWhCode;
    }

    public void setInvWhCode(String invWhCode) {
        this.invWhCode = invWhCode;
    }

    public String getInvWhName() {
        return invWhName;
    }

    public void setInvWhName(String invWhName) {
        this.invWhName = invWhName;
    }

    public String getInvCustomerCode() {
        return invCustomerCode;
    }

    public void setInvCustomerCode(String invCustomerCode) {
        this.invCustomerCode = invCustomerCode;
    }

    public String getInvCustomerName() {
        return invCustomerName;
    }

    public void setInvCustomerName(String invCustomerName) {
        this.invCustomerName = invCustomerName;
    }

    public String getInvVehicleBrandCode() {
        return invVehicleBrandCode;
    }

    public void setInvVehicleBrandCode(String invVehicleBrandCode) {
        this.invVehicleBrandCode = invVehicleBrandCode;
    }

    public String getInvVehicleBrandName() {
        return invVehicleBrandName;
    }

    public void setInvVehicleBrandName(String invVehicleBrandName) {
        this.invVehicleBrandName = invVehicleBrandName;
    }

    public String getInvVehicleSeriesCode() {
        return invVehicleSeriesCode;
    }

    public void setInvVehicleSeriesCode(String invVehicleSeriesCode) {
        this.invVehicleSeriesCode = invVehicleSeriesCode;
    }

    public String getInvVehicleSeriesName() {
        return invVehicleSeriesName;
    }

    public void setInvVehicleSeriesName(String invVehicleSeriesName) {
        this.invVehicleSeriesName = invVehicleSeriesName;
    }

    public String getInvVehicleSpecCode() {
        return invVehicleSpecCode;
    }

    public void setInvVehicleSpecCode(String invVehicleSpecCode) {
        this.invVehicleSpecCode = invVehicleSpecCode;
    }

    public String getInvVehicleSpecName() {
        return invVehicleSpecName;
    }

    public void setInvVehicleSpecName(String invVehicleSpecName) {
        this.invVehicleSpecName = invVehicleSpecName;
    }

    public String getInvVehicleSpecDesc() {
        return invVehicleSpecDesc;
    }

    public void setInvVehicleSpecDesc(String invVehicleSpecDesc) {
        this.invVehicleSpecDesc = invVehicleSpecDesc;
    }

    public Long getInvNum() {
        return invNum;
    }

    public void setInvNum(Long invNum) {
        this.invNum = invNum;
    }

    public String getInvVin() {
        return invVin;
    }

    public void setInvVin(String invVin) {
        this.invVin = invVin;
    }

    public List<WmsInventoryLocationDTO> getWmsInventoryLocationDTOList() {
        return wmsInventoryLocationDTOList;
    }

    public void setWmsInventoryLocationDTOList(List<WmsInventoryLocationDTO> wmsInventoryLocationDTOList) {
        this.wmsInventoryLocationDTOList = wmsInventoryLocationDTOList;
    }

    @Override
    public String toString() {
        return "WmsInventoryDTO{" +
                "invId=" + invId +
                ", invWhCode='" + invWhCode + '\'' +
                ", invWhName='" + invWhName + '\'' +
                ", invCustomerCode='" + invCustomerCode + '\'' +
                ", invCustomerName='" + invCustomerName + '\'' +
                ", invVehicleBrandCode='" + invVehicleBrandCode + '\'' +
                ", invVehicleBrandName='" + invVehicleBrandName + '\'' +
                ", invVehicleSeriesCode='" + invVehicleSeriesCode + '\'' +
                ", invVehicleSeriesName='" + invVehicleSeriesName + '\'' +
                ", invVehicleSpecCode='" + invVehicleSpecCode + '\'' +
                ", invVehicleSpecName='" + invVehicleSpecName + '\'' +
                ", invVehicleSpecDesc='" + invVehicleSpecDesc + '\'' +
                ", invNum=" + invNum +
                ", invVin='" + invVin + '\'' +
                ", wmsInventoryLocationDTOList=" + wmsInventoryLocationDTOList +
                '}';
    }
}
