package com.unlcn.ils.wms.backend.bo.outboundBO;

import java.util.ArrayList;
import java.util.List;

/**
 * @Auther linbao
 * @Date 2017-10-24
 */
public class BarCodeBO {

    /**
     * 类型
     */
    private String orderNo;

    /**
     * 类型
     */
    private String type;

    private List<String> vins = new ArrayList<String>();

    public String getOrderNo() {
        return orderNo;
    }

    public void setOrderNo(String orderNo) {
        this.orderNo = orderNo;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public List<String> getVins() {
        return vins;
    }

    public void setVins(List<String> vins) {
        this.vins = vins;
    }


    @Override
    public String toString() {
        return "BarCodeBO{" +
                "orderNo='" + orderNo + '\'' +
                ", type='" + type + '\'' +
                ", vins=" + vins +
                '}';
    }
}
