package com.unlcn.ils.wms.backend.service.webservice.client.enums;

/**
 * @Date : 2017/9/13.
 */
public enum WmsWebserviceReceiverAndSenderEnum {


    RECEIVER_SAP(1, "接收方SAP"),
    RECEIVER_DCS(2, "接收方DCS"),
    RECEIVER_CRM(3, "接收方CRM"),
    SENDER(2, "发送方WMS");

    private final int value;
    private final String text;

    WmsWebserviceReceiverAndSenderEnum(int value, String text) {
        this.value = value;
        this.text = text;
    }

    public int getValue() {
        return value;
    }

    public String getText() {
        return text;
    }

    public static WmsWebserviceReceiverAndSenderEnum getByValue(int value) {
        for (WmsWebserviceReceiverAndSenderEnum temp : WmsWebserviceReceiverAndSenderEnum.values()) {
            if (temp.getValue() == value) {
                return temp;
            }
        }
        return null;
    }
}
