package com.unlcn.ils.wms.backend.bo.biBO;

import java.io.Serializable;

/**
 * Created by Nicky on 17/11/9.
 */
public class BiFrontProgressBO implements Serializable {
    /**
     * 指令号
     */
    private String shipno;

    /**
     * 台数
     */
    private String qty;

    /**
     * 运输模式
     */
    private String transmode;

    /**
     * 指令起运地
     */
    private String route_start;

    /**
     * 指令目的地
     */
    private String route_end;

    /**
     * 指令当前位置
     */
    private String position;

    /**
     * 指令发运时间
     */
    private String out_date;

    /**
     * 计划到达时间
     */
    private String est_arrival_date;

    /**
     * 进度公里数
     */
    private String curr_km;

    /**
     * 标准公里数
     */
    private String standard_km;

    /**
     * 进度占比
     */
    private String progress;

    /**
     * 超期状态
     */
    private String exceed_status;

    /**
     * 指令时间
     */
    private String assign_date;

    public String getAssign_date() {
        return assign_date;
    }

    public void setAssign_date(String assign_date) {
        this.assign_date = assign_date;
    }

    public String getShipno() {
        return shipno;
    }

    public void setShipno(String shipno) {
        this.shipno = shipno;
    }

    public String getQty() {
        return qty;
    }

    public void setQty(String qty) {
        this.qty = qty;
    }

    public String getTransmode() {
        return transmode;
    }

    public void setTransmode(String transmode) {
        this.transmode = transmode;
    }

    public String getRoute_start() {
        return route_start;
    }

    public void setRoute_start(String route_start) {
        this.route_start = route_start;
    }

    public String getRoute_end() {
        return route_end;
    }

    public void setRoute_end(String route_end) {
        this.route_end = route_end;
    }

    public String getPosition() {
        return position;
    }

    public void setPosition(String position) {
        this.position = position;
    }

    public String getOut_date() {
        return out_date;
    }

    public void setOut_date(String out_date) {
        this.out_date = out_date;
    }

    public String getEst_arrival_date() {
        return est_arrival_date;
    }

    public void setEst_arrival_date(String est_arrival_date) {
        this.est_arrival_date = est_arrival_date;
    }

    public String getCurr_km() {
        return curr_km;
    }

    public void setCurr_km(String curr_km) {
        this.curr_km = curr_km;
    }

    public String getStandard_km() {
        return standard_km;
    }

    public void setStandard_km(String standard_km) {
        this.standard_km = standard_km;
    }

    public String getProgress() {
        return progress;
    }

    public void setProgress(String progress) {
        this.progress = progress;
    }

    public String getExceed_status() {
        return exceed_status;
    }

    public void setExceed_status(String exceed_status) {
        this.exceed_status = exceed_status;
    }

    @Override
    public String toString() {
        return "BiFrontProgressBO{" +
                "shipno='" + shipno + '\'' +
                ", qty='" + qty + '\'' +
                ", transmode='" + transmode + '\'' +
                ", route_start='" + route_start + '\'' +
                ", route_end='" + route_end + '\'' +
                ", position='" + position + '\'' +
                ", out_date='" + out_date + '\'' +
                ", est_arrival_date='" + est_arrival_date + '\'' +
                ", curr_km='" + curr_km + '\'' +
                ", standard_km='" + standard_km + '\'' +
                ", progress='" + progress + '\'' +
                ", exceed_status='" + exceed_status + '\'' +
                '}';
    }
}
