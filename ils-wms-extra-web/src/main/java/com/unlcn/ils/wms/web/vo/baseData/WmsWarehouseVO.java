package com.unlcn.ils.wms.web.vo.baseData;

import cn.huiyunche.commons.domain.PageVo;

import java.util.Date;

/**
 * Created by DELL on 2017/9/20.
 */
public class WmsWarehouseVO extends PageVo{

    /**
     * 仓库ID
     */
    private Long whId;

    /**
     * 仓库code
     */
    private String whCode;

    /**
     * 仓库名称
     */
    private String whName;

    /**
     * 仓库类型
     */
    private String whType;

    /**
     * 仓库地址
     */
    private String whAddress;

    /**
     * 仓库属性方式
     */
    private String whStyle;

    /**
     * 是否自动分配
     */
    private String whIsAuto;

    /**
     * 备注
     */
    private String whRemark;

    /**
     * 创建人用户ID
     */
    private String createUserId;

    /**
     * 创建人用户名
     */
    private String createUserName;

    /**
     * 修改人用户id
     */
    private String modifyUserId;

    /**
     * 修改人用户名
     */
    private String modifyUserName;

    /**
     * 创建时间
     */
    private Date gmtCreate;

    /**
     * 修改时间
     */
    private Date gmtUpdate;

    /**
     * 是否删除
     */
    private Byte isDeleted;

    /**
     * 版本号
     */
    private Long versions;

    private int startIndex;

    public Long getWhId() {
        return whId;
    }

    public void setWhId(Long whId) {
        this.whId = whId;
    }

    public String getWhCode() {
        return whCode;
    }

    public void setWhCode(String whCode) {
        this.whCode = whCode;
    }

    public String getWhName() {
        return whName;
    }

    public void setWhName(String whName) {
        this.whName = whName;
    }

    public String getWhType() {
        return whType;
    }

    public void setWhType(String whType) {
        this.whType = whType;
    }

    public String getWhAddress() {
        return whAddress;
    }

    public void setWhAddress(String whAddress) {
        this.whAddress = whAddress;
    }

    public String getWhStyle() {
        return whStyle;
    }

    public void setWhStyle(String whStyle) {
        this.whStyle = whStyle;
    }

    public String getWhIsAuto() {
        return whIsAuto;
    }

    public void setWhIsAuto(String whIsAuto) {
        this.whIsAuto = whIsAuto;
    }

    public String getWhRemark() {
        return whRemark;
    }

    public void setWhRemark(String whRemark) {
        this.whRemark = whRemark;
    }

    public String getCreateUserId() {
        return createUserId;
    }

    public void setCreateUserId(String createUserId) {
        this.createUserId = createUserId;
    }

    public String getCreateUserName() {
        return createUserName;
    }

    public void setCreateUserName(String createUserName) {
        this.createUserName = createUserName;
    }

    public String getModifyUserId() {
        return modifyUserId;
    }

    public void setModifyUserId(String modifyUserId) {
        this.modifyUserId = modifyUserId;
    }

    public String getModifyUserName() {
        return modifyUserName;
    }

    public void setModifyUserName(String modifyUserName) {
        this.modifyUserName = modifyUserName;
    }

    public Date getGmtCreate() {
        return gmtCreate;
    }

    public void setGmtCreate(Date gmtCreate) {
        this.gmtCreate = gmtCreate;
    }

    public Date getGmtUpdate() {
        return gmtUpdate;
    }

    public void setGmtUpdate(Date gmtUpdate) {
        this.gmtUpdate = gmtUpdate;
    }

    public Byte getIsDeleted() {
        return isDeleted;
    }

    public void setIsDeleted(Byte isDeleted) {
        this.isDeleted = isDeleted;
    }

    public Long getVersions() {
        return versions;
    }

    public void setVersions(Long versions) {
        this.versions = versions;
    }

    @Override
    public int getStartIndex() {
        return startIndex;
    }

    public void setStartIndex(int startIndex) {
        this.startIndex = startIndex;
    }

    @Override
    public String toString() {
        return "WmsWarehouseVO{" +
                "whId=" + whId +
                ", whCode='" + whCode + '\'' +
                ", whName='" + whName + '\'' +
                ", whType='" + whType + '\'' +
                ", whAddress='" + whAddress + '\'' +
                ", whStyle='" + whStyle + '\'' +
                ", whIsAuto='" + whIsAuto + '\'' +
                ", whRemark='" + whRemark + '\'' +
                ", createUserId='" + createUserId + '\'' +
                ", createUserName='" + createUserName + '\'' +
                ", modifyUserId='" + modifyUserId + '\'' +
                ", modifyUserName='" + modifyUserName + '\'' +
                ", gmtCreate=" + gmtCreate +
                ", gmtUpdate=" + gmtUpdate +
                ", isDeleted=" + isDeleted +
                ", versions=" + versions +
                ", startIndex=" + startIndex +
                '}';
    }
}
