package com.unlcn.ils.wms.base.mapper.extmapper;

import com.unlcn.ils.wms.base.dto.WmsWarehouseNoticeDetailDTOForSchedule;
import com.unlcn.ils.wms.base.dto.WmsWarehouseNoticeHeadDTO;
import com.unlcn.ils.wms.base.dto.WmsWarehouseNoticeHeadForASNListResultDTO;
import com.unlcn.ils.wms.base.model.inbound.WmsAsnTemp;
import com.unlcn.ils.wms.base.model.inbound.WmsTmsOrder;
import com.unlcn.ils.wms.base.model.inbound.WmsWarehouseNoticeDetail;
import com.unlcn.ils.wms.base.model.inbound.WmsWarehouseNoticeHead;
import org.apache.ibatis.annotations.Param;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public interface WmsWarehouseNoticeExtMapper {

    int updateByPrimaryKey(WmsWarehouseNoticeDetail record);

    List<WmsWarehouseNoticeHeadDTO> selectLastHTTPTimeStampByParam(Map<String, Object> paramMap);

    void insertHeadBatch(ArrayList<WmsWarehouseNoticeHead> heads);

    void insertDetailBatch(ArrayList<WmsWarehouseNoticeDetail> details);

    void updateSetSqlMode();

    List<WmsWarehouseNoticeDetailDTOForSchedule> selectVinListLikeByParam(Map<String, Object> paramMap);

    List<WmsWarehouseNoticeHeadDTO> selectLastRESTInfoByParam(Map<String, Object> paramMap);

    List<WmsWarehouseNoticeHeadDTO> selectLastHTTPInfoByParam(Map<String, Object> paramMap);

    List<WmsWarehouseNoticeHeadDTO> selectNotInboundNoticeByParam(HashMap<String, Object> params);

    List<WmsWarehouseNoticeHeadForASNListResultDTO> selectNoticeListByParamForASN(Map<String, Object> paramMap);

    int countNoticeListByParamForASN(Map<String, Object> paramMap);

    List<WmsWarehouseNoticeDetail> selectNoticeListByIdsForPrint(List<String> ids);

    List<WmsWarehouseNoticeHead> selectAllNoticeCustomerNameList(HashMap<String, Object> params);

    List<WmsWarehouseNoticeDetail> selectAllNoticeVehicleSpecListByParam(HashMap<String, Object> params);

    List<WmsWarehouseNoticeDetail> selectAllNoticeColorListByParam(HashMap<String, Object> params);

    List<WmsAsnTemp> selectWmsAsnTempListForSync(Map<String, Object> params);

    int countWmsAsnTemp();

    void updateSetMaxSQLPacket();

    void insertHeadBatchSelective(ArrayList<WmsWarehouseNoticeHead> insertHeads);

    void insertDetailBatchSelective(ArrayList<WmsWarehouseNoticeDetail> insertDetails);

    int countTmsOrder();

    List<WmsTmsOrder> selectWmsTmsOrderListForSync(HashMap<String, Object> params);

    void updateOdCodeForSync();

    List<WmsWarehouseNoticeHeadDTO> selectBatchNoForSyncPreparationToOrder(HashMap<String, Object> params);

    List<WmsWarehouseNoticeHeadDTO> selectWmsWarehouseNoticeListByVinList(@Param("params") HashMap<String, Object> params);


    //List<WmsWarehouseNoticeDetailDTOForSchedule> selectNoticeDetailListByTimeParam(HashMap<String, Object> params);
}