package com.unlcn.ils.wms.backend.service.webservice.bo;

import java.io.Serializable;

/**
 * 发运计划
 * Created by DELL on 2017/9/23.
 */
public class WmsWebServiceShipmentPlanBO implements Serializable {

    private String spDeliverWarehouseCode;//发运仓库code

    private String spDeliverWarehouseName;//发运仓库name

    /**
     * 组板单号
     */
    private String spGroupBoardNo;

    /**
     * 订单号
     */
    private String spOrderNo;

    /**
     * 车型代码
     */
    private String spVehicleCode;

    /**
     * 车型名称
     */
    private String spVehicleName;

    /**
     * 配置
     */
    private String spConfigure;

    /**
     * 颜色
     */
    private String spCarColour;

    /**
     * 物料代码
     */
    private String spMaterialCode;

    /**
     * 组板数量
     */
    private String spGroupBoardQuantity;

    /**
     * 经销商代码
     */
    private String spDealerCode;

    /**
     * 经销商名称
     */
    private String spDealerName;

    /**
     * 收货省
     */
    private String spProvince;

    /**
     * 收货市
     */
    private String spCity;

    /**
     * 收货区县
     */
    private String spDistrictCounty;

    /**
     * 收货详细地址
     */
    private String spDetailedAddress;

    /**
     * 承运商
     */
    private String spCarrier;

    /**
     * 预计装车时间
     */
    private String spEstimateLoadingTime;

    /**
     * 承运商联系人
     */
    private String spLogiContact;

    /**
     * 承运商座机
     */
    private String spLogiTel;

    /**
     * 承运商手机
     */
    private String spLogiPhone;

    /**
     * 收货人联系人
     */
    private String spReceiveMan;

    /**
     * 收货人联系电话
     */
    private String spReceiveTel;

    /**
     * 资金类型
     */
    private String spFoundType;

    /**
     * 订单备注
     */
    private String spOrderRemark;

    /**
     * 是否保留合格证
     */
    private String spIsRemain;

    /**
     * 分派备注
     */
    private String spDispRemark;

    /**
     * 订单行ID
     */
    private String spLineId;

    /**
     * 发运类型 A1-正常发运；A2-调拨发运
     */
    private String spDlvType;

    public String getSpDeliverWarehouseCode() {
        return spDeliverWarehouseCode;
    }

    public void setSpDeliverWarehouseCode(String spDeliverWarehouseCode) {
        this.spDeliverWarehouseCode = spDeliverWarehouseCode;
    }

    public String getSpDeliverWarehouseName() {
        return spDeliverWarehouseName;
    }

    public void setSpDeliverWarehouseName(String spDeliverWarehouseName) {
        this.spDeliverWarehouseName = spDeliverWarehouseName;
    }

    public String getSpGroupBoardNo() {
        return spGroupBoardNo;
    }

    public void setSpGroupBoardNo(String spGroupBoardNo) {
        this.spGroupBoardNo = spGroupBoardNo;
    }

    public String getSpOrderNo() {
        return spOrderNo;
    }

    public void setSpOrderNo(String spOrderNo) {
        this.spOrderNo = spOrderNo;
    }

    public String getSpVehicleCode() {
        return spVehicleCode;
    }

    public void setSpVehicleCode(String spVehicleCode) {
        this.spVehicleCode = spVehicleCode;
    }

    public String getSpVehicleName() {
        return spVehicleName;
    }

    public void setSpVehicleName(String spVehicleName) {
        this.spVehicleName = spVehicleName;
    }

    public String getSpConfigure() {
        return spConfigure;
    }

    public void setSpConfigure(String spConfigure) {
        this.spConfigure = spConfigure;
    }

    public String getSpCarColour() {
        return spCarColour;
    }

    public void setSpCarColour(String spCarColour) {
        this.spCarColour = spCarColour;
    }

    public String getSpMaterialCode() {
        return spMaterialCode;
    }

    public void setSpMaterialCode(String spMaterialCode) {
        this.spMaterialCode = spMaterialCode;
    }

    public String getSpGroupBoardQuantity() {
        return spGroupBoardQuantity;
    }

    public void setSpGroupBoardQuantity(String spGroupBoardQuantity) {
        this.spGroupBoardQuantity = spGroupBoardQuantity;
    }

    public String getSpDealerCode() {
        return spDealerCode;
    }

    public void setSpDealerCode(String spDealerCode) {
        this.spDealerCode = spDealerCode;
    }

    public String getSpDealerName() {
        return spDealerName;
    }

    public void setSpDealerName(String spDealerName) {
        this.spDealerName = spDealerName;
    }

    public String getSpProvince() {
        return spProvince;
    }

    public void setSpProvince(String spProvince) {
        this.spProvince = spProvince;
    }

    public String getSpCity() {
        return spCity;
    }

    public void setSpCity(String spCity) {
        this.spCity = spCity;
    }

    public String getSpDistrictCounty() {
        return spDistrictCounty;
    }

    public void setSpDistrictCounty(String spDistrictCounty) {
        this.spDistrictCounty = spDistrictCounty;
    }

    public String getSpDetailedAddress() {
        return spDetailedAddress;
    }

    public void setSpDetailedAddress(String spDetailedAddress) {
        this.spDetailedAddress = spDetailedAddress;
    }

    public String getSpCarrier() {
        return spCarrier;
    }

    public void setSpCarrier(String spCarrier) {
        this.spCarrier = spCarrier;
    }

    public String getSpEstimateLoadingTime() {
        return spEstimateLoadingTime;
    }

    public void setSpEstimateLoadingTime(String spEstimateLoadingTime) {
        this.spEstimateLoadingTime = spEstimateLoadingTime;
    }

    public String getSpLogiContact() {
        return spLogiContact;
    }

    public void setSpLogiContact(String spLogiContact) {
        this.spLogiContact = spLogiContact;
    }

    public String getSpLogiTel() {
        return spLogiTel;
    }

    public void setSpLogiTel(String spLogiTel) {
        this.spLogiTel = spLogiTel;
    }

    public String getSpLogiPhone() {
        return spLogiPhone;
    }

    public void setSpLogiPhone(String spLogiPhone) {
        this.spLogiPhone = spLogiPhone;
    }

    public String getSpReceiveMan() {
        return spReceiveMan;
    }

    public void setSpReceiveMan(String spReceiveMan) {
        this.spReceiveMan = spReceiveMan;
    }

    public String getSpReceiveTel() {
        return spReceiveTel;
    }

    public void setSpReceiveTel(String spReceiveTel) {
        this.spReceiveTel = spReceiveTel;
    }

    public String getSpFoundType() {
        return spFoundType;
    }

    public void setSpFoundType(String spFoundType) {
        this.spFoundType = spFoundType;
    }

    public String getSpOrderRemark() {
        return spOrderRemark;
    }

    public void setSpOrderRemark(String spOrderRemark) {
        this.spOrderRemark = spOrderRemark;
    }

    public String getSpIsRemain() {
        return spIsRemain;
    }

    public void setSpIsRemain(String spIsRemain) {
        this.spIsRemain = spIsRemain;
    }

    public String getSpDispRemark() {
        return spDispRemark;
    }

    public void setSpDispRemark(String spDispRemark) {
        this.spDispRemark = spDispRemark;
    }

    public String getSpLineId() {
        return spLineId;
    }

    public void setSpLineId(String spLineId) {
        this.spLineId = spLineId;
    }

    public String getSpDlvType() {
        return spDlvType;
    }

    public void setSpDlvType(String spDlvType) {
        this.spDlvType = spDlvType;
    }

    @Override
    public String toString() {
        return "WmsShipmentPlanVO{" +
                "spGroupBoardNo='" + spGroupBoardNo + '\'' +
                ", spOrderNo='" + spOrderNo + '\'' +
                ", spVehicleCode='" + spVehicleCode + '\'' +
                ", spVehicleName='" + spVehicleName + '\'' +
                ", spConfigure='" + spConfigure + '\'' +
                ", spCarColour='" + spCarColour + '\'' +
                ", spMaterialCode='" + spMaterialCode + '\'' +
                ", spGroupBoardQuantity=" + spGroupBoardQuantity +
                ", spDealerCode='" + spDealerCode + '\'' +
                ", spDealerName='" + spDealerName + '\'' +
                ", spProvince='" + spProvince + '\'' +
                ", spCity='" + spCity + '\'' +
                ", spDistrictCounty='" + spDistrictCounty + '\'' +
                ", spDetailedAddress='" + spDetailedAddress + '\'' +
                ", spCarrier='" + spCarrier + '\'' +
                ", spEstimateLoadingTime='" + spEstimateLoadingTime + '\'' +
                ", spLogiContact='" + spLogiContact + '\'' +
                ", spLogiTel='" + spLogiTel + '\'' +
                ", spLogiPhone='" + spLogiPhone + '\'' +
                ", spReceiveMan='" + spReceiveMan + '\'' +
                ", spReceiveTel='" + spReceiveTel + '\'' +
                ", spFoundType='" + spFoundType + '\'' +
                ", spOrderRemark='" + spOrderRemark + '\'' +
                ", spIsRemain='" + spIsRemain + '\'' +
                ", spDispRemark='" + spDispRemark + '\'' +
                ", spLineId='" + spLineId + '\'' +
                ", spDlvType='" + spDlvType + '\'' +
                '}';
    }
}
