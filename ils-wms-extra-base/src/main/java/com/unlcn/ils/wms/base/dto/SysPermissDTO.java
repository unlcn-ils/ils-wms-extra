package com.unlcn.ils.wms.base.dto;

import java.util.List;

/**
 * @Auther linbao
 * @Date 2017-10-13
 */
public class SysPermissDTO {

    private Integer id;

    private String name;

    private Integer parentId;

    private String level;

    private String url;

    private List<SysPermissDTO> childPermiss;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getParentId() {
        return parentId;
    }

    public void setParentId(Integer parentId) {
        this.parentId = parentId;
    }

    public String getLevel() {
        return level;
    }

    public void setLevel(String level) {
        this.level = level;
    }

    public List<SysPermissDTO> getChildPermiss() {
        return childPermiss;
    }

    public void setChildPermiss(List<SysPermissDTO> childPermiss) {
        this.childPermiss = childPermiss;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }
}
