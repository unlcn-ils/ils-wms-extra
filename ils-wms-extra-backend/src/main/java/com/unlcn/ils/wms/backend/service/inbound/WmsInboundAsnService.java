package com.unlcn.ils.wms.backend.service.inbound;

import cn.huiyunche.commons.domain.ResultDTOWithPagination;
import com.unlcn.ils.wms.backend.bo.baseDataBO.CustomerNameList;
import com.unlcn.ils.wms.backend.bo.inboundBO.WmsAsnTempBO;
import com.unlcn.ils.wms.base.dto.WmsRecordsImportExcelDTO;
import com.unlcn.ils.wms.base.dto.WmsWarehouseNoticeHeadForASNListResultDTO;
import com.unlcn.ils.wms.base.model.inbound.WmsWarehouseNoticeHead;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;
import java.util.Map;

/**
 * Created by DELL on 2017/9/21.
 */
public interface WmsInboundAsnService {

    ResultDTOWithPagination<List<WmsWarehouseNoticeHeadForASNListResultDTO>> getInboundNoticeList(Map<String, Object> paramMap) throws Exception;

    Map<String, Object> getAsnListOld(Map<String, Object> paramMap) throws Exception;

    List<WmsWarehouseNoticeHead> getAllCustomerName(String whCode);

    List<CustomerNameList> getAllCustomerNameOld(String whCode) throws Exception;

    List<WmsWarehouseNoticeHeadForASNListResultDTO> queryListByTime(WmsAsnTempBO wmsAsnTempBO, String whCode);

    List<WmsAsnTempBO> queryListByTimeOld(WmsAsnTempBO wmsAsnTempBO) throws Exception;

    void updateInboundImportExcel(WmsRecordsImportExcelDTO dto, HttpServletRequest request, HttpServletResponse response) throws Exception;

    Integer countInboundRecord(WmsRecordsImportExcelDTO dto) throws Exception;
}
