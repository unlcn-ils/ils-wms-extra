package com.unlcn.ils.wms.web.controller.sys;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.unlcn.ils.wms.backend.bo.sys.PermissionsBO;
import com.unlcn.ils.wms.backend.bo.sys.RoleBO;
import com.unlcn.ils.wms.backend.service.sys.RoleService;
import com.unlcn.ils.wms.web.vo.sys.PermissionsVO;
import com.unlcn.ils.wms.web.vo.sys.RoleVO;

import cn.huiyunche.commons.domain.ResultDTO;
import cn.huiyunche.commons.exception.BusinessException;

/**
 * 
 * @ClassName: RoleController 
 * @Description: 角色
 * @author laishijian
 * @date 2017年8月8日 上午9:58:05 
 *
 */
@Controller
@RequestMapping("/role")
@ResponseBody
public class RoleController {
	
	private Logger LOGGER = LoggerFactory.getLogger(RoleController.class);
	
	@Autowired
	private RoleService roleService;
	
	/**
	 * 
	 * @Title: add 
	 * @Description: 新增角色
	 * @param @param roleVO
	 * @param @return     
	 * @return ResultDTO<Object>    返回类型 
	 * @throws 
	 *
	 */
	@RequestMapping(value = "/add",method=RequestMethod.POST)
	public ResultDTO<Object> add(@RequestBody RoleVO roleVO){
		
		LOGGER.info("RoleController.add param : {}", roleVO);
		
		ResultDTO<Object> result = new ResultDTO<>(true);
		try {
			
			RoleBO roleBO = new RoleBO();
			BeanUtils.copyProperties(roleVO, roleBO);
			
			if(null != roleVO.getPermissionsVOList()) {
				roleBO.setPermissionsBOList(new ArrayList<PermissionsBO>());
				
				for(PermissionsVO permissionsVO : roleVO.getPermissionsVOList()) {
					PermissionsBO permissionsBO = new PermissionsBO();
					BeanUtils.copyProperties(permissionsVO, permissionsBO);
					roleBO.getPermissionsBOList().add(permissionsBO);
				}
				
			}
			
			roleService.addRole(roleBO);
			
			result.setMessage("新增角色成功");
			
		}catch(BusinessException be) {
			result.setSuccess(false);
			result.setMessage(be.getMessage());
			LOGGER.error("RoleController.add error: {}", be);
		}catch(Exception e) {
			result.setSuccess(false);
			result.setMessage("系统异常");
			LOGGER.error("RoleController.add error: {}", e);
		}
		return result;
	}
	
	/**
	 * 
	 * @Title: get 
	 * @Description: 查询
	 * @param @param id
	 * @param @return     
	 * @return ResultDTO<RoleVO>    返回类型 
	 * @throws 
	 *
	 */
	@RequestMapping(value="/get/{id}",method=RequestMethod.GET)
	public ResultDTO<RoleVO> get(@PathVariable("id") Integer id){
		
		LOGGER.info("RoleController.get param : {}", id);
		
		ResultDTO<RoleVO> result = new ResultDTO<>(true);
		try {
			
			RoleBO roleBO = roleService.findById(id);
			
			if(null != roleBO) {
				RoleVO roleVO = new RoleVO();
				BeanUtils.copyProperties(roleBO, roleVO);
				result.setData(roleVO);
			}
			result.setMessage("查询角色成功");
			
		}catch(BusinessException be) {
			result.setSuccess(false);
			result.setMessage(be.getMessage());
			LOGGER.error("RoleController.get error: {}", be);
		}catch(Exception e) {
			result.setSuccess(false);
			result.setMessage("系统异常");
			LOGGER.error("RoleController.get error: {}", e);
		}
		return result;
	}
	
	/**
	 * 
	 * @Title: update 
	 * @Description: 更新角色
	 * @param @param roleVO
	 * @param @return     
	 * @return ResultDTO<Object>    返回类型 
	 * @throws 
	 *
	 */
	@RequestMapping(value = "/update",method = RequestMethod.PUT)
	public ResultDTO<Object> update(@RequestBody RoleVO roleVO){
		
		LOGGER.info("RoleController.update param : {}", roleVO);
		
		ResultDTO<Object> result = new ResultDTO<>(true);
		try {
			RoleBO roleBO = new RoleBO();
			BeanUtils.copyProperties(roleVO, roleBO);
			
			roleService.updateRole(roleBO);
			result.setMessage("更新角色成功");
			
		}catch(BusinessException be) {
			result.setSuccess(false);
			result.setMessage(be.getMessage());
			LOGGER.error("RoleController.update error: {}", be);
		}catch(Exception e) {
			result.setSuccess(false);
			result.setMessage("系统异常");
			LOGGER.error("RoleController.update error: {}", e);
		}
		return result;
	}
	
	/**
	 * 
	 * @Title: delete 
	 * @Description: 删除角色
	 * @param @param ids
	 * @param @return     
	 * @return ResultDTO<Object>    返回类型 
	 * @throws 
	 *
	 */
	@RequestMapping(value="/delete/{ids}",method=RequestMethod.DELETE)
	public ResultDTO<Object> delete(@PathVariable("ids") String ids){
		
		LOGGER.info("RoleController.delete param : {}", ids);
		
		ResultDTO<Object> result = new ResultDTO<>(true);
		try {
			
			roleService.deleteByIds(ids);
			result.setMessage("删除角色成功");
			
		}catch(BusinessException be) {
			result.setSuccess(false);
			result.setMessage(be.getMessage());
			LOGGER.error("RoleController.delete error: {}", be);
		}catch(Exception e) {
			result.setSuccess(false);
			result.setMessage("系统异常");
			LOGGER.error("RoleController.delete error: {}", e);
		}
		return result;
	}
	
	/**
	 * 
	 * @Title: list 
	 * @Description: 列表
	 * @param @param roleVO
	 * @param @return     
	 * @return ResultDTO<List<RoleVO>>    返回类型 
	 * @throws 
	 *
	 */
	@RequestMapping(value = "/list",method=RequestMethod.POST)
	public ResultDTO<List<RoleVO>> list(@RequestBody RoleVO roleVO){
		
		LOGGER.info("RoleController.list param : {}", roleVO);
		
		ResultDTO<List<RoleVO>> result = new ResultDTO<>(true);
		try {
			
			RoleBO roleBO = new RoleBO();
			BeanUtils.copyProperties(roleVO, roleBO);
			
			List<RoleBO> roleBOList = roleService.list(roleBO,roleVO.getPageVO());
			
			List<RoleVO> roleVOList = new ArrayList<>();
			for (RoleBO roleBOTemp : roleBOList) {
				RoleVO newRoleVO = new RoleVO();
				BeanUtils.copyProperties(roleBOTemp, newRoleVO);
				roleVOList.add(newRoleVO);
			}
			
			result.setData(roleVOList);
			result.setMessage("获取角色成功");
			
		}catch(BusinessException be) {
			result.setSuccess(false);
			result.setMessage(be.getMessage());
			LOGGER.error("RoleController.list error: {}", be);
		}catch(Exception e) {
			result.setSuccess(false);
			result.setMessage("系统异常");
			LOGGER.error("RoleController.list error: {}", e);
		}
		return result;
	}
	
}
