package com.unlcn.ils.wms.backend.service.bigscreen;

import com.unlcn.ils.wms.backend.dto.bigscreenDTO.ReasonInfoDTO;

import java.util.List;

/**
 * Created by lenovo on 2017/11/8.
 */
public interface OverReasonService {
    /**
     * 超期原因分析信息
     * @return
     */
    List<ReasonInfoDTO> reasonChartCount();

    /**
     * 添加超期分析原因
     */
    void addReasonInfo();

}
