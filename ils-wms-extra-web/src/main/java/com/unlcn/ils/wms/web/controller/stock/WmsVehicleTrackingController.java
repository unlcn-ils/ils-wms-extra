package com.unlcn.ils.wms.web.controller.stock;

import cn.huiyunche.commons.domain.ResultDTOWithPagination;
import cn.huiyunche.commons.exception.BusinessException;
import com.unlcn.ils.wms.backend.bo.stockBO.WmsVehicleTrackingBO;
import com.unlcn.ils.wms.backend.service.stock.WmsVehicleTrackingService;
import com.unlcn.ils.wms.base.model.stock.WmsVehicleTracking;
import com.unlcn.ils.wms.base.model.stock.WmsVehicleTrackingWithBLOBs;
import com.unlcn.ils.wms.web.vo.stock.WmsVehicleTranckingVO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
 * 出入库的跟踪接口---重庆库功能
 */
@Controller
@RequestMapping("/vehicleTracking")
public class WmsVehicleTrackingController {

    private Logger LOGGER = LoggerFactory.getLogger(WmsVehicleTrackingController.class);

    @Autowired
    private WmsVehicleTrackingService wmsVehicleTrackingService;

    /**
     * 查询跟踪信息
     * <p>2017-12-26 新需求:关联查询异常信息</p>
     *
     * @param vo      参数封装
     * @param request 请求
     * @return 返回值
     * @throws Exception 异常
     */
    @RequestMapping(value = "/getTrackList", method = RequestMethod.POST)
    @ResponseBody
    public ResultDTOWithPagination<List<WmsVehicleTrackingWithBLOBs>> getTrackList(@RequestBody WmsVehicleTranckingVO vo, HttpServletRequest request) throws Exception {
        LOGGER.info("WmsVehicleTrackingController.getTrackList param:{},{}", vo);
        ResultDTOWithPagination<List<WmsVehicleTrackingWithBLOBs>> resultDTOWithPagination = new ResultDTOWithPagination<>(true, null, "查询成功");
        try {
            WmsVehicleTrackingBO trackingBO = new WmsVehicleTrackingBO();
            BeanUtils.copyProperties(vo, trackingBO);
            String whCode = request.getHeader("whCode");
            String userId = request.getHeader("userId");
            resultDTOWithPagination = wmsVehicleTrackingService.updateTrackingList(trackingBO, whCode, userId);
            resultDTOWithPagination.setSuccess(true);
        } catch (BusinessException e) {
            LOGGER.error("WmsVehicleTrackingController.getTrackList param:{}", e);
            resultDTOWithPagination.setSuccess(false);
            resultDTOWithPagination.setMessage(e.getMessage());
            resultDTOWithPagination.setData(null);
        } catch (Exception ex) {
            LOGGER.error("WmsVehicleTrackingController.getTrackList param:{}", ex);
            resultDTOWithPagination.setSuccess(false);
            resultDTOWithPagination.setMessage("查询跟踪列表异常!");
            resultDTOWithPagination.setData(null);
        }
        return resultDTOWithPagination;
    }

    /**
     * 数据导出功能
     *
     * @param vo      参数封装
     * @param request 请求
     * @return 返回值
     * @throws Exception 异常
     */
    @RequestMapping(value = "/importOutExcel", method = RequestMethod.GET)
    @ResponseBody
    public ResultDTOWithPagination<List<WmsVehicleTracking>> getImportOutExcel(WmsVehicleTranckingVO vo, HttpServletRequest request, HttpServletResponse response) throws Exception {
        LOGGER.info("WmsVehicleTrackingController.importOutExcel param:{}", vo);
        ResultDTOWithPagination<List<WmsVehicleTracking>> result = new ResultDTOWithPagination<>(true, null, "查询成功");
        try {
            WmsVehicleTrackingBO trackingBO = new WmsVehicleTrackingBO();
            BeanUtils.copyProperties(vo, trackingBO);
            wmsVehicleTrackingService.getImportOutExcel(trackingBO, request, response);
        } catch (BusinessException e) {
            LOGGER.error("WmsVehicleTrackingController.getTrackList param:{}", e);
            result.setSuccess(false);
            result.setMessage(e.getMessage());
            result.setData(null);
        } catch (Exception ex) {
            LOGGER.error("WmsVehicleTrackingController.getTrackList param:{}", ex);
            result.setSuccess(false);
            result.setMessage("查询跟踪列表异常!");
            result.setData(null);
        }
        return result;
    }
}
