package com.unlcn.ils.wms.base.mapper.extmapper;

import com.unlcn.ils.wms.base.model.sys.SysPermissions;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface SysPermissionsCustomMapper {
	
	/**
	 * 
	 * @Title: selectByRoleId 
	 * @Description: 根据角色id查询权限
	 * @param @param roleId 角色id 
	 * @param @param parentPermissionsId 父级权限id
	 * @param @param status 状态
	 * @param @return     
	 * @return List<SysPermissions>    返回类型 
	 * @throws 
	 *
	 */
	List<SysPermissions> selectByRoleId(@Param("roleId") Integer roleId,@Param("parentPermissionsId") Integer parentPermissionsId,@Param("status") String status);
	
	/**
	 * 
	 * @Title: selectByParentId 
	 * @Description: 根据父id查询权限 
	 * @param @param parentId
	 * @param @return     
	 * @return List<SysPermissions>    返回类型 
	 * @throws 
	 *
	 */
	List<SysPermissions> selectByParentId(@Param("parentId") Integer parentId);

	/**
	 * 查询可用的权限
	 * @param parentId
	 * @return
	 */
	List<SysPermissions> selectByParentIdForEnabled(@Param("parentId") Integer parentId);

	/**
	 * 根据角色获取权限
	 *
	 * @param roleIdList
	 * @param parentId
	 * @return
	 */
	List<SysPermissions> selectPermissListByRoleIdListAndParentId(@Param("roleIdList") List<Integer> roleIdList, @Param("parentId") Integer parentId);

	/**
	 * 根据用户id获取数据
	 *
	 * @param parentId
	 * @param userId
	 * @return
	 */
	List<SysPermissions> selectPermissListByParentAndUserId(@Param("parentId") Integer parentId,
															@Param("userId") Integer userId,
															@Param("warehouseId") Long warehouseId);
}