package com.unlcn.ils.wms.backend.enums;

/**
 * @Auther linbao
 * @Date 2017-10-24
 */
public enum SerialNoTypeEnum {
    BC("BC", "批次号"),
    JC("JC", "借车单号"),
    HC("HC", "还车单号");

    private final String code;

    private final String value;

    public String getCode() {
        return code;
    }

    public String getValue() {
        return value;
    }

    SerialNoTypeEnum(String code, String value) {
        this.code = code;
        this.value = value;
    }
}
