package com.unlcn.ils.wms.backend.bo.inspectAppBO;

/**
 * @Author ：Ligl
 * @Date : 2017/9/16.
 */
public class TmsInspectExcpCloseBO {
    private Long inspectId;
    private Long excpId;
    private int positionId;
    private int status;

    public Long getInspectId() {
        return inspectId;
    }

    public void setInspectId(Long inspectId) {
        this.inspectId = inspectId;
    }

    public Long getExcpId() {
        return excpId;
    }

    public void setExcpId(Long excpId) {
        this.excpId = excpId;
    }

    public int getPositionId() {
        return positionId;
    }

    public void setPositionId(int positionId) {
        this.positionId = positionId;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    @Override
    public String toString() {
        return "TmsInspectExcpCloseBO{" +
                "inspectId=" + inspectId +
                ", excpId=" + excpId +
                ", positionId=" + positionId +
                ", status=" + status +
                '}';
    }
}
