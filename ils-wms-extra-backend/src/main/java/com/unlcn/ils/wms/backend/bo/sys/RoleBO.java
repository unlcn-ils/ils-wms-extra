package com.unlcn.ils.wms.backend.bo.sys;

import java.util.Date;
import java.util.List;

/**
 * @author laishijian
 * @ClassName: RoleBO
 * @Description: 角色BO
 * @date 2017年8月8日 上午10:16:16
 */
public class RoleBO {

    //id
    private Integer id;

    //用户ID
    private Integer userId;

    private String userName;

    //角色名
    private String name;
    //父id
    private Integer parentId;
    //是否可以用状态
    private String enable;
    //创建人
    private String createPerson;
    //修改人
    private String updatePerson;
    //创建时间
    private Date gmtCreate;
    //修改时间
    private Date gmtModified;
    //权限BOList
    private List<PermissionsBO> permissionsBOList;
    //子权限
    private List<RoleBO> childRoleBOList;

    private String token;

    /**
     * 二维码超时时间
     */
    private String timeout;

    public String getTimeout() {
        return timeout;
    }

    public void setTimeout(String timeout) {
        this.timeout = timeout;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    /**
     * @return the id
     */
    public Integer getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * @param name the name to set
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * @return the parentId
     */
    public Integer getParentId() {
        return parentId;
    }

    /**
     * @param parentId the parentId to set
     */
    public void setParentId(Integer parentId) {
        this.parentId = parentId;
    }

    /**
     * @return the enable
     */
    public String getEnable() {
        return enable;
    }

    /**
     * @param enable the enable to set
     */
    public void setEnable(String enable) {
        this.enable = enable;
    }

    /**
     * @return the createPerson
     */
    public String getCreatePerson() {
        return createPerson;
    }

    /**
     * @param createPerson the createPerson to set
     */
    public void setCreatePerson(String createPerson) {
        this.createPerson = createPerson;
    }

    /**
     * @return the updatePerson
     */
    public String getUpdatePerson() {
        return updatePerson;
    }

    /**
     * @param updatePerson the updatePerson to set
     */
    public void setUpdatePerson(String updatePerson) {
        this.updatePerson = updatePerson;
    }

    /**
     * @return the gmtCreate
     */
    public Date getGmtCreate() {
        return gmtCreate;
    }

    /**
     * @param gmtCreate the gmtCreate to set
     */
    public void setGmtCreate(Date gmtCreate) {
        this.gmtCreate = gmtCreate;
    }

    /**
     * @return the gmtModified
     */
    public Date getGmtModified() {
        return gmtModified;
    }

    /**
     * @param gmtModified the gmtModified to set
     */
    public void setGmtModified(Date gmtModified) {
        this.gmtModified = gmtModified;
    }

    /**
     * @return the permissionsBOList
     */
    public List<PermissionsBO> getPermissionsBOList() {
        return permissionsBOList;
    }

    /**
     * @param permissionsBOList the permissionsBOList to set
     */
    public void setPermissionsBOList(List<PermissionsBO> permissionsBOList) {
        this.permissionsBOList = permissionsBOList;
    }

    /**
     * @return the childRoleBOList
     */
    public List<RoleBO> getChildRoleBOList() {
        return childRoleBOList;
    }

    /**
     * @param childRoleBOList the childRoleBOList to set
     */
    public void setChildRoleBOList(List<RoleBO> childRoleBOList) {
        this.childRoleBOList = childRoleBOList;
    }


    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    /*
             * Title: toString
             * Description: toString
             * @return
             * @see java.lang.Object#toString()
             *
             */
    @Override
    public String toString() {
        return "RoleBO [id=" + id + ", name=" + name + ", parentId=" + parentId + ", enable=" + enable
                + ", createPerson=" + createPerson + ", updatePerson=" + updatePerson + ", gmtCreate=" + gmtCreate
                + ", gmtModified=" + gmtModified + ", permissionsBOList=" + permissionsBOList + "]";
    }

}
