package com.unlcn.ils.wms.base.dto;

import java.util.List;

/**
 * @Auther linbao
 * @Date 2017-10-13
 */
public class SysUserDTO {

    private Integer id;

    private String username;

    private String name;

    private Byte gender;

    private String job;

    private String email;

    private String mobile;

    /**
     * 账号类型
     */
    private Byte type;

    /**
     * 所属仓库id
     */
    private Long warehouseId;

    /**
     * 所属仓库编码
     */
    private String whCode;

    private String whName;

    private String token;

    private List<SysPermissDTO> permisssList;

    public String getWhName() {
        return whName;
    }

    public void setWhName(String whName) {
        this.whName = whName;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Byte getGender() {
        return gender;
    }

    public void setGender(Byte gender) {
        this.gender = gender;
    }

    public String getJob() {
        return job;
    }

    public void setJob(String job) {
        this.job = job;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public Byte getType() {
        return type;
    }

    public void setType(Byte type) {
        this.type = type;
    }

    public List<SysPermissDTO> getPermisssList() {
        return permisssList;
    }

    public void setPermisssList(List<SysPermissDTO> permisssList) {
        this.permisssList = permisssList;
    }

    public Long getWarehouseId() {
        return warehouseId;
    }

    public void setWarehouseId(Long warehouseId) {
        this.warehouseId = warehouseId;
    }

    public String getWhCode() {
        return whCode;
    }

    public void setWhCode(String whCode) {
        this.whCode = whCode;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }
}
