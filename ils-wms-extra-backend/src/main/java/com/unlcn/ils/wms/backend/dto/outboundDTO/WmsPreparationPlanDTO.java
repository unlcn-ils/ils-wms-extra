package com.unlcn.ils.wms.backend.dto.outboundDTO;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

public class WmsPreparationPlanDTO implements Serializable {

	private Long ppId;

	private String ppPreparationMaterialNo;

	private String ppEstimateLane;

	private Date gmtCreate;

	List<AppWmsOutboundTaskListDTO> taskList;

	public Long getPpId() {
		return ppId;
	}

	public void setPpId(Long ppId) {
		this.ppId = ppId;
	}

	public String getPpPreparationMaterialNo() {
		return ppPreparationMaterialNo;
	}

	public void setPpPreparationMaterialNo(String ppPreparationMaterialNo) {
		this.ppPreparationMaterialNo = ppPreparationMaterialNo;
	}

	public String getPpEstimateLane() {
		return ppEstimateLane;
	}

	public void setPpEstimateLane(String ppEstimateLane) {
		this.ppEstimateLane = ppEstimateLane;
	}

	public Date getGmtCreate() {
		return gmtCreate;
	}

	public void setGmtCreate(Date gmtCreate) {
		this.gmtCreate = gmtCreate;
	}

	public List<AppWmsOutboundTaskListDTO> getTaskList() {
		return taskList;
	}

	public void setTaskList(List<AppWmsOutboundTaskListDTO> taskList) {
		this.taskList = taskList;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("WmsPreparationPlanDTO [ppId=");
		builder.append(ppId);
		builder.append(", ppPreparationMaterialNo=");
		builder.append(ppPreparationMaterialNo);
		builder.append(", ppEstimateLane=");
		builder.append(ppEstimateLane);
		builder.append(", gmtCreate=");
		builder.append(gmtCreate);
		builder.append(", taskList=");
		builder.append(taskList);
		builder.append("]");
		return builder.toString();
	}

}