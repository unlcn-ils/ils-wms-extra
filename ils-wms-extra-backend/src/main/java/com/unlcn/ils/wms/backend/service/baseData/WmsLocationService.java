package com.unlcn.ils.wms.backend.service.baseData;

import com.unlcn.ils.wms.backend.bo.baseDataBO.WmsLocationBO;
import com.unlcn.ils.wms.base.businessDTO.baseData.WmsLocationQueryDTO;
import com.unlcn.ils.wms.base.businessDTO.inbound.AsnOrderDTO;

import java.util.List;
import java.util.Map;

/**
 * Created by DELL on 2017/8/18.
 */
public interface WmsLocationService {

    /**
     * 查询库位分页
     * @param wmsLocationQueryDTO
     * @return
     * @throws IllegalAccessException
     */
    Map<String, Object> listLocation(WmsLocationQueryDTO wmsLocationQueryDTO) throws IllegalAccessException;

    /**
     * 新增库区
     * @param wmsLocationBO
     */
    boolean addLocation(WmsLocationBO wmsLocationBO);

    /**
     * 修改库区
     * @param wmsLocationBo
     */
    void updateLocation(WmsLocationBO wmsLocationBo);

    /**
     * 删除库区
     * @param wmsLocationBOList
     */
    void deleteLocation(List<WmsLocationBO> wmsLocationBOList);

    /**
     * 根据ID查询库位数据
     * @param locId
     * @return
     */
    WmsLocationBO getLocationById(String locId);

    /**
     * 启用禁用库位
     * @param wmsLocationBOList
     */
    void enableLocation(List<WmsLocationBO> wmsLocationBOList,String locEnableFlag);

    void updateSendToAPPAndPrintCode(AsnOrderDTO result, String userId) throws Exception;

}
