package com.unlcn.ils.wms.backend.enums;

public enum WmsCheckDescEnum {
    WAYBILL_INIT(10, "该车辆还未验车"),
    WAYBILL_QUALIFY(20, "验车通过"),
    WAYBILL_EXCP(30, "验车未通过"),
    DESC_EXCP(70,"验车未通过");
    private final int value;
    private final String text;

    WmsCheckDescEnum(int value, String text) {
        this.value = value;
        this.text = text;
    }

    public int getValue() {
        return value;
    }

    public String getText() {
        return text;
    }

    public static WmsCheckDescEnum getByValue(int value){
        for (WmsCheckDescEnum temp : WmsCheckDescEnum.values()) {
            if(temp.getValue() == value){
                return temp;
            }
        }
        return null;
    }
}
