/**
 * Created by houjianhui on 2017/7/19.
 */
'use strict';
//出库管理
wmsCtrl.controller('warehouseCtrl',['$scope','warehouseService',function(scope,warehouseService){

    //生成备料单
    scope.generatingListClick = function(){

        var truckLoadingList = [
            {
                name : '装车道',
                value : 10
            },
            {
                name : '装车道1',
                value : 20
            },
            {
                name : '装车道2',
                value : 30
            }
        ]

        var indexArr = scope.Tools.getTableSelectItemIndex('.warehouseTable');  //获取table所选的下标

        var warehouseList = scope.Tools.getDataTOIndex(indexArr,scope.warehouseList);

        if(warehouseList.length == 0){
            scope.Tools.alert('请选择需要生成备料单的数据');
            return;
        }

        var dispachNo = warehouseList[0].oodDispatchNo;
        for(var i = 0; i < warehouseList.length;i++){
            if(warehouseList[i].oodDispatchNo != dispachNo){
                scope.Tools.alert('只能选择相同调度单号的数据');
                return;
            }
        }

        scope.Tools.dialog({
            controller : 'warehouseCtrl.dialog',
            template : '../public/template/warehouseManagement/warehouse/generatingList-form.html',
            closeByDocument : false,
            closeByEscape : false,
            data : {
                warehouseList : warehouseList,  //出库单
                truckLoadingList : truckLoadingList,    //装车道
                dropDownDefault : {
                    truckLoading : truckLoadingList[0]  //装车道默认下拉
                }
            },
            width : 760,
            title : '生成备料单',
            scope : scope
        })
    }

    //移库确认
    scope.shiftingParkingClick = function(){
        var indexArr = scope.Tools.getTableSelectItemIndex('.warehouseTable');  //获取table所选的下标

        var warehouseList = scope.Tools.getDataTOIndex(indexArr,scope.warehouseList);

        if(warehouseList.length == 0){
            scope.Tools.alert('请选择需要移库确认的数据');
            return;
        }

        scope.Tools.dialogConfirm('确定要移库确认选中的数据吗？')
            .then(function(res){
                if(res){
                    warehouseService.removeLocationConfirm(warehouseList)
                        .then(function(result){
                            if(result.success){
                                scope.Tools.alert('移库确认成功');
                                scope.list(scope.listParam());
                            }else{
                                scope.Tools.alert(result.message);
                            }
                        })
                }
            })
    }

    //出库质检
    scope.outboundInspectionClick = function(){

        var indexArr = scope.Tools.getTableSelectItemIndex('.warehouseTable');  //获取table所选的下标

        var warehouseCheckList = scope.Tools.getDataTOIndex(indexArr,scope.warehouseList);

        if(warehouseCheckList.length == 0){
            scope.Tools.alert('请选择需要出库质检的数据');
            return;
        }

        scope.Tools.dialog({
            controller : 'warehouseCtrl.dialog',
            template : '../public/template/warehouseManagement/warehouse/outbound-inspection-form.html',
            closeByDocument : false,
            closeByEscape : false,
            data : {
                warehouseCheckList : warehouseCheckList,    //出库单list
                inspectionResults : scope.Tools.dropDown.inspectionResultsDropDown //检验结果
            },
            width : 760,
            title : '出库质检',
            scope : scope
        })
    }

    //出库确认 click
    scope.outboundConfirmClick = function(){

        var indexArr = scope.Tools.getTableSelectItemIndex('.warehouseTable');  //获取table所选的下标

        var outboundConfirmList = scope.Tools.getDataTOIndex(indexArr,scope.warehouseList);

        if(outboundConfirmList.length == 0){
            scope.Tools.alert('请选择需要出库确认的数据');
            return;
        }

        scope.Tools.dialogConfirm('确定要出库确认选中的数据吗？')
            .then(function(res){
                if(res){
                    warehouseService.outboundConfirm(outboundConfirmList)
                        .then(function(result){
                            if(result.success){
                                scope.Tools.alert('出库确认成功');
                                scope.list(scope.listParam());
                            }else{
                                scope.Tools.alert(result.message);
                            }
                        })
                }
            })
    }

    //更多条件默认值
    scope.moreText = scope.constant.moreText.spread;
    //更多条件
    scope.moreClick = function(){
        scope.otherForm = !scope.otherForm;
        if(scope.otherForm){
            scope.moreText = scope.constant.moreText.packUp;
        }else{
            scope.moreText = scope.constant.moreText.spread;
        }
    }

    /*状态数据*/
    scope.dropDownList = {
        status : scope.Tools.dropDown.warehouseDropDown.concat(),    //出库状态
        inspectionResults : scope.Tools.dropDown.inspectionResultsDropDown.concat() //检验结果
    }

    scope.dropDownList.status.unshift(scope.constant.defaultDropDownNullData) //插入出库状态数据
    scope.dropDownList.inspectionResults.unshift(scope.constant.defaultDropDownNullData) //插入检验结果数据

    //设置默认值
    scope.dropDownDefault = {
        status :  scope.dropDownList.status[0], //出库状态
        inspectionResults : scope.dropDownList.inspectionResults[0] //检验结果
    }

    //列表查询接口参数构造
    scope.listParam = function(){

        if(!scope.search){
            scope.search = {};
        }
        scope.search.pageSize = scope.pageSize;    //设置最大页
        if(scope.otherForm){    //展开了更多
            scope.search.oodStatus = scope.dropDownDefault.status.value;    //出库状态
            scope.search.oodCheckResult = scope.dropDownDefault.inspectionResults.value;    //检验结果
            return scope.search;
        }else{  //没有展开
            return {
                oodWaybillNo : scope.search.oodWaybillNo
            }
        }
    }

    //列表
    scope.fristSelect = true;   //判断是否是第一次查询
    scope.list = function(params){
        //设置分页参数
        if(!params){
            params = {};
        }
        params.pageSize = params.pageSize ? params.pageSize : scope.constant.page.pageSize;
        params.pageNo = params.pageNo ? params.pageNo : scope.constant.page.pageNo

        warehouseService.list(params)
            .then(function(result){
                if(result.success){
                    scope.warehouseList = result.data.outboundOrderList;
                    if(scope.fristSelect){
                        scope.Tools.page(document.getElementById('pages'),{
                            pages : result.data.outboundOrderListPageInfo.totalPage,
                            curr : result.data.outboundOrderListPageInfo.pageNo,
                            count : result.data.outboundOrderListPageInfo.totalRecord
                        },function(obj, first){
                            scope.fristSelect = first;  //设置状态
                            //初始化多选框
                            scope.Tools.initCheckBox();
                            if(!first){ //判断不是第一次加载
                                var param = scope.listParam();
                                param.pageNo = obj.curr;    //当前页
                                param.pageSize = obj.limit; //最大页
                                scope.pageNo = obj.curr; //设置当前页
                                scope.list(param);
                            }
                        })
                    }else{
                        //初始化多选框
                        scope.Tools.initCheckBox();
                    }

                }else{
                    scope.Tools.alert(result.message);
                }
            })
    }

    //列表
    scope.list();

    //回车按下查询
    scope.searchListKeyDown = function(e){
        var keycode = window.event ? e.keyCode:e.which;
        if(keycode == 13){  //回车键
            scope.fristSelect = true;   //设置为第一次查询
            scope.list(scope.listParam());
            e.preventDefault();
        }
    }

    //查询 click
    scope.searchClick = function(){
        scope.fristSelect = true;   //设置为第一次查询
        scope.list(scope.listParam());
    }

    //重置 click
    scope.resetClick = function(){
        scope.search = {};
    }

    //刷新 click
    scope.refreshClick = function(){
        scope.searchClick();
    }

}])
    .controller('warehouseCtrl.dialog',['$scope','warehouseService',function(scope,warehouseService){   //出库管理 dialog

        //判断是否有出库单数据    说明是生成备料单
        if(scope.ngDialogData.warehouseList){
            scope.warehouse = {
                blDispatchNo : scope.ngDialogData.warehouseList[0].oodDispatchNo
            }
        }

        //新增备料单
        scope.addWarehouseClick = function(){

            //获取指定装车道
            scope.warehouse.blLoadingRoadCode = scope.ngDialogData.dropDownDefault.truckLoading.value;
            scope.warehouse.blLoadingRoadName = scope.ngDialogData.dropDownDefault.truckLoading.name;
            //出库单数据
            scope.warehouse.wmsOutboundOrderVOList = scope.ngDialogData.warehouseList;

            warehouseService.addBLOrder(scope.warehouse)
                .then(function(result){
                    if(result.success){
                        scope.Tools.alert('生成备料单成功');
                        scope.closeThisDialog();
                    }else{
                        scope.Tools.alert(result.message);
                    }
                })
        }

        //判断是否出库质检数据
        if(scope.ngDialogData.warehouseCheckList){
            //选择默认值
            scope.ngDialogData.checkResult = new Array();
            angular.forEach(scope.ngDialogData.warehouseCheckList,function(value,key){
                //默认选中合格
                scope.ngDialogData.checkResult[key] = scope.ngDialogData.inspectionResults[0];
            })

            //出库质检 click
            scope.warehouseCheckClick = function(){

                angular.forEach(scope.ngDialogData.warehouseCheckList,function(value,key){
                    value.oodCheckResult = scope.ngDialogData.checkResult[key].value;
                })

                warehouseService.outboundCheck(scope.ngDialogData.warehouseCheckList)
                    .then(function(result){
                        if(result.success){
                            scope.Tools.alert('出库质检成功');
                            scope.list(scope.listParam());
                            scope.closeThisDialog();
                        }else{
                            scope.Tools.alert(result.message);
                        }
                    })
            }
        }
    }])