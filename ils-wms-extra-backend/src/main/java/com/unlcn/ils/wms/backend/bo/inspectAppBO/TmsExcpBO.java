package com.unlcn.ils.wms.backend.bo.inspectAppBO;

/**
 * Created by houjianhui on 2017/7/24.
 */
public class TmsExcpBO {
    private Long id;
    private Long pId;
    private Integer position;
    private String positionName;
    private String code;
    private String name;

    public Long getpId() {
        return pId;
    }

    public void setpId(Long pId) {
        this.pId = pId;
    }

    public String getPositionName() {
        return positionName;
    }

    public void setPositionName(String positionName) {
        this.positionName = positionName;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getPosition() {
        return position;
    }

    public void setPosition(Integer position) {
        this.position = position;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Override
    public String toString() {
        return "TmsExcpBO{" +
                "id=" + id +
                ", pId=" + pId +
                ", position=" + position +
                ", positionName='" + positionName + '\'' +
                ", code='" + code + '\'' +
                ", name='" + name + '\'' +
                '}';
    }
}
