package com.unlcn.ils.wms.backend.enums;

/**
 * 君马出入库调用接口反馈
 *
 * @author renml
 */
public enum WmsOutOfStorageStatus {
    ZSTATUS_SUCCESS("S", "成功"),
    ZSTATUS_FAILED("E", "失败");

    private final String value;
    private final String text;

    WmsOutOfStorageStatus(String value, String text) {
        this.value = value;
        this.text = text;
    }

    public String getValue() {
        return value;
    }

    public String getText() {
        return text;
    }

    public static WmsOutOfStorageStatus getByValue(String value) {
        for (WmsOutOfStorageStatus temp : WmsOutOfStorageStatus.values()) {
            if (temp.getValue().equals(value)) {
                return temp;
            }
        }
        return null;
    }
}
