package com.unlcn.ils.wms.backend.service.borrow;

import com.unlcn.ils.wms.base.model.stock.WmsBorrowCarDetail;
import com.unlcn.ils.wms.base.model.stock.WmsBorrowCarDetailExample;

import java.util.List;

/**
 * Created by lenovo on 2017/10/20.
 */
public interface WmsBorrowCarDetailService {
    /**
     * 新增明细
     * @param wmsBorrowCarDetail
     */
    void add(WmsBorrowCarDetail wmsBorrowCarDetail);

    /**
     * 编辑明细
     * @param wmsBorrowCarDetail
     */
    void modify(WmsBorrowCarDetail wmsBorrowCarDetail);

    /**
     * 删除明细
     * @param id
     */
    void delete(Long id);

    /**
     * 根据参数查询明细
     * @param wmsBorrowCarDetailExample
     * @return
     */
    List<WmsBorrowCarDetail> findByParams(WmsBorrowCarDetailExample wmsBorrowCarDetailExample);
}
