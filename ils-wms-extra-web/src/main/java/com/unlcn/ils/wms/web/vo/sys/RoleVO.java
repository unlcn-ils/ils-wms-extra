package com.unlcn.ils.wms.web.vo.sys;

import cn.huiyunche.commons.domain.PageVo;

import java.util.Date;
import java.util.List;

/**
 * 
 * @ClassName: RoleVO 
 * @Description: 角色VO
 * @author laishijian
 * @date 2017年8月8日 上午10:07:21 
 *
 */
public class RoleVO {
	
	//id
	private Integer id;
	//角色名
	private String name;
	//是否可以用状态
	private String enable;
	//创建人
	private String createPerson;
	//修改人
	private String updatePerson;
	//创建时间
	private Date gmtCreate;
	//修改时间
	private Date gmtModified;
	//分页
	private PageVo pageVO;
	//权限VOList
	private List<PermissionsVO> permissionsVOList;
	
	/**
	 * @return the id
	 */
	public Integer getId() {
		return id;
	}
	/**
	 * @param id the id to set
	 */
	public void setId(Integer id) {
		this.id = id;
	}
	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}
	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}
	/**
	 * @return the enable
	 */
	public String getEnable() {
		return enable;
	}
	/**
	 * @param enable the enable to set
	 */
	public void setEnable(String enable) {
		this.enable = enable;
	}
	/**
	 * @return the createPerson
	 */
	public String getCreatePerson() {
		return createPerson;
	}
	/**
	 * @param createPerson the createPerson to set
	 */
	public void setCreatePerson(String createPerson) {
		this.createPerson = createPerson;
	}
	/**
	 * @return the updatePerson
	 */
	public String getUpdatePerson() {
		return updatePerson;
	}
	/**
	 * @param updatePerson the updatePerson to set
	 */
	public void setUpdatePerson(String updatePerson) {
		this.updatePerson = updatePerson;
	}
	/**
	 * @return the gmtCreate
	 */
	public Date getGmtCreate() {
		return gmtCreate;
	}
	/**
	 * @param gmtCreate the gmtCreate to set
	 */
	public void setGmtCreate(Date gmtCreate) {
		this.gmtCreate = gmtCreate;
	}
	/**
	 * @return the gmtModified
	 */
	public Date getGmtModified() {
		return gmtModified;
	}
	/**
	 * @param gmtModified the gmtModified to set
	 */
	public void setGmtModified(Date gmtModified) {
		this.gmtModified = gmtModified;
	}
	/**
	 * @return the pageVO
	 */
	public PageVo getPageVO() {
		return pageVO;
	}
	/**
	 * @param pageVO the pageVO to set
	 */
	public void setPageVO(PageVo pageVO) {
		this.pageVO = pageVO;
	}
	/**
	 * @return the permissionsVOList
	 */
	public List<PermissionsVO> getPermissionsVOList() {
		return permissionsVOList;
	}
	/**
	 * @param permissionsVOList the permissionsVOList to set
	 */
	public void setPermissionsVOList(List<PermissionsVO> permissionsVOList) {
		this.permissionsVOList = permissionsVOList;
	}

	/*
	 * Title: toString
	 * Description: toString
	 * @return
	 * @see java.lang.Object#toString()
	 *
	 */
	@Override
	public String toString() {
		return "RoleVO [id=" + id + ", name=" + name + ", enable=" + enable + ", createPerson=" + createPerson
				+ ", updatePerson=" + updatePerson + ", gmtCreate=" + gmtCreate + ", gmtModified=" + gmtModified + "]";
	}
}
