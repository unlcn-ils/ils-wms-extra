package com.unlcn.ils.wms.backend.service.borrow.impl;

import com.unlcn.ils.wms.backend.service.borrow.WmsBorrowCarDetailService;
import com.unlcn.ils.wms.base.mapper.stock.WmsBorrowCarDetailMapper;
import com.unlcn.ils.wms.base.model.stock.WmsBorrowCarDetail;
import com.unlcn.ils.wms.base.model.stock.WmsBorrowCarDetailExample;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by lenovo on 2017/10/20.
 */
@Service
public class WmsBorrowCarDetailServiceImpl implements WmsBorrowCarDetailService {

    @Autowired
    private WmsBorrowCarDetailMapper wmsBorrowCarDetailMapper;

    @Override
    public void add(WmsBorrowCarDetail wmsBorrowCarDetail) {
        wmsBorrowCarDetailMapper.insertSelective(wmsBorrowCarDetail);
    }

    @Override
    public void modify(WmsBorrowCarDetail wmsBorrowCarDetail) {
        wmsBorrowCarDetailMapper.updateByPrimaryKeySelective(wmsBorrowCarDetail);
    }

    @Override
    public void delete(Long id) {
        WmsBorrowCarDetail wmsBorrowCarDetail = new WmsBorrowCarDetail();
        wmsBorrowCarDetail.setBdId(id);
        wmsBorrowCarDetail.setDeleteFlag(0);
        wmsBorrowCarDetailMapper.updateByPrimaryKeySelective(wmsBorrowCarDetail);
    }

    @Override
    public List<WmsBorrowCarDetail> findByParams(WmsBorrowCarDetailExample wmsBorrowCarDetailExample) {
        List<WmsBorrowCarDetail> wmsBorrowCarDetailList = wmsBorrowCarDetailMapper.selectByExample(wmsBorrowCarDetailExample);
        return wmsBorrowCarDetailList;
    }
}
