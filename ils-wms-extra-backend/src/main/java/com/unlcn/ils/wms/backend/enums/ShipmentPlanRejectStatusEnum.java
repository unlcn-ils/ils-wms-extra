package com.unlcn.ils.wms.backend.enums;

/**
 * @Auther linbao
 * @Date 2017-10-27
 * 发运计划驳回状态
 */
public enum ShipmentPlanRejectStatusEnum {
    NO_REJECT("10", "不驳回"),
    REJECT_ED("20", "已驳回");

    private final String code;

    private final String value;

    public String getCode() {
        return code;
    }

    public String getValue() {
        return value;
    }

    ShipmentPlanRejectStatusEnum(String code, String value) {
        this.code = code;
        this.value = value;
    }
}
