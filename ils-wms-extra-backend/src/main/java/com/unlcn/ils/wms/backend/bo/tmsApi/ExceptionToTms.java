package com.unlcn.ils.wms.backend.bo.tmsApi;

/**
 * Created by DELL on 2017/9/7.
 */
public class ExceptionToTms {
    /**
     * TMS任务单号
     */
    private String taskNo;

    /**
     * 异常类型
     * 10：带伤
     * 20：缺件
     */
    private String exceptionType;

    /**
     * 异常描述
     */
    private String exceptionContent;

    public String getTaskNo() {
        return taskNo;
    }

    public void setTaskNo(String taskNo) {
        this.taskNo = taskNo;
    }

    public String getExceptionType() {
        return exceptionType;
    }

    public void setExceptionType(String exceptionType) {
        this.exceptionType = exceptionType;
    }

    public String getExceptionContent() {
        return exceptionContent;
    }

    public void setExceptionContent(String exceptionContent) {
        this.exceptionContent = exceptionContent;
    }

    @Override
    public String toString() {
        return "ExceptionToTms{" +
                "taskNo='" + taskNo + '\'' +
                ", exceptionType='" + exceptionType + '\'' +
                ", exceptionContent='" + exceptionContent + '\'' +
                '}';
    }
}
