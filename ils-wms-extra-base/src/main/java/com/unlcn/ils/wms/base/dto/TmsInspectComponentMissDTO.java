package com.unlcn.ils.wms.base.dto;

import java.io.Serializable;

public class TmsInspectComponentMissDTO implements Serializable {
    private Long id;
    private String componentCode;
    private String componentName;
    private Long inspectId;
    private String inspectVin;
    private Boolean missingStatus;
    private Long componentId;

    public Long getComponentId() {
        return componentId;
    }

    public void setComponentId(Long componentId) {
        this.componentId = componentId;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getComponentCode() {
        return componentCode;
    }

    public void setComponentCode(String componentCode) {
        this.componentCode = componentCode;
    }

    public String getComponentName() {
        return componentName;
    }

    public void setComponentName(String componentName) {
        this.componentName = componentName;
    }

    public Long getInspectId() {
        return inspectId;
    }

    public void setInspectId(Long inspectId) {
        this.inspectId = inspectId;
    }

    public String getInspectVin() {
        return inspectVin;
    }

    public void setInspectVin(String inspectVin) {
        this.inspectVin = inspectVin;
    }

    public Boolean getMissingStatus() {
        return missingStatus;
    }

    public void setMissingStatus(Boolean missingStatus) {
        this.missingStatus = missingStatus;
    }
}
