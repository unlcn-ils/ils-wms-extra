package com.unlcn.ils.wms.backend.service.stock;

import cn.huiyunche.commons.domain.ResultDTOWithPagination;
import com.unlcn.ils.wms.backend.bo.stockBO.WmsVehicleTrackingBO;
import com.unlcn.ils.wms.base.model.stock.WmsVehicleTrackingWithBLOBs;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;

public interface WmsVehicleTrackingService {

    ResultDTOWithPagination<List<WmsVehicleTrackingWithBLOBs>> updateTrackingList(WmsVehicleTrackingBO trackingBO, String whCode, String userId) throws Exception;

    void getImportOutExcel(WmsVehicleTrackingBO trackingBO, HttpServletRequest request, HttpServletResponse response) throws Exception;
}
