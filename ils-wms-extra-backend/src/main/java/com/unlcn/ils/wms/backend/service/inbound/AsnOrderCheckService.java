package com.unlcn.ils.wms.backend.service.inbound;

import com.unlcn.ils.wms.backend.bo.inboundBO.WmsInboundCheckBO;
import com.unlcn.ils.wms.backend.bo.inboundBO.WmsInboundOrderBO;

import java.util.List;

/**
 * Created by DELL on 2017/8/7.
 */
public interface AsnOrderCheckService {
    /**
     * 质检
     * @param wmsInboundCheckBO
     * @return
     */
    boolean submitCheckAsnOrder(WmsInboundCheckBO wmsInboundCheckBO) throws Exception;
}
