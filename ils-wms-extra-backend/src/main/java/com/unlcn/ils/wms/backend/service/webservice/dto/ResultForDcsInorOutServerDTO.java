package com.unlcn.ils.wms.backend.service.webservice.dto;

import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;

@XmlRootElement
public class ResultForDcsInorOutServerDTO implements Serializable {
    private ResultForDcsInorOutServerBodyDTO bodyDTO;
    private ResultSapHeaderDTO headerDTO;

    //转换为xml1.类注解  2.构造方法


    public ResultForDcsInorOutServerDTO(ResultForDcsInorOutServerBodyDTO bodyDTO, ResultSapHeaderDTO headerDTO) {
        this.bodyDTO = bodyDTO;
        this.headerDTO = headerDTO;
    }

    public ResultForDcsInorOutServerDTO() {
        super();
    }


    public ResultForDcsInorOutServerBodyDTO getBodyDTO() {
        return bodyDTO;
    }

    public void setBodyDTO(ResultForDcsInorOutServerBodyDTO bodyDTO) {
        this.bodyDTO = bodyDTO;
    }

    public ResultSapHeaderDTO getHeaderDTO() {
        return headerDTO;
    }

    public void setHeaderDTO(ResultSapHeaderDTO headerDTO) {
        this.headerDTO = headerDTO;
    }
}
