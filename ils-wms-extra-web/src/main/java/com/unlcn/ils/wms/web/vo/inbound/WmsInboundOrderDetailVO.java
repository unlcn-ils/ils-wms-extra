package com.unlcn.ils.wms.web.vo.inbound;

/**
 * Created by DELL on 2017/9/13.
 */
public class WmsInboundOrderDetailVO {

    /**
     * 详情ID
     */
    private Long oddId;

    /**
     * 主表ID
     */
    private Long oddOdId;

    /**
     * 主表code
     */
    private String oddOdCode;

    /**
     * 品牌code
     */
    private String oddVehicleBrandCode;

    /**
     * 品牌name
     */
    private String oddVehicleBrandName;

    /**
     * 车系code
     */
    private String oddVehicleSeriesCode;

    /**
     * 车系name
     */
    private String oddVehicleSeriesName;

    /**
     * 车型code
     */
    private String oddVehicleSpecCode;

    /**
     * 车型name
     */
    private String oddVehicleSpecName;

    /**
     * 车型描述
     */
    private String oddVehicleSpecDesc;

    /**
     * 车牌号
     */
    private String oddVehiclePlate;

    /**
     * 底盘号
     */
    private String oddVin;

    /**
     * 发动机号
     */
    private String oddEngine;

    /**
     * 仓库code
     */
    private String oddWhCode;

    /**
     * 仓库名称
     */
    private String oddWhName;

    /**
     * 库区code
     */
    private String oddWhZoneCode;

    /**
     * 库区名称
     */
    private String oddWhZoneName;

    /**
     * 库位code
     */
    private String oddWhLocCode;

    /**
     * 库位名称
     */
    private String oddWhLocName;

    /**
     * 车长
     */
    private Long oddVehicleLength;

    /**
     * 车宽
     */
    private Long oddVehicleWidth;

    /**
     * 车高
     */
    private Long oddVehicleHigh;

    public Long getOddId() {
        return oddId;
    }

    public void setOddId(Long oddId) {
        this.oddId = oddId;
    }

    public Long getOddOdId() {
        return oddOdId;
    }

    public void setOddOdId(Long oddOdId) {
        this.oddOdId = oddOdId;
    }

    public String getOddOdCode() {
        return oddOdCode;
    }

    public void setOddOdCode(String oddOdCode) {
        this.oddOdCode = oddOdCode;
    }

    public String getOddVehicleBrandCode() {
        return oddVehicleBrandCode;
    }

    public void setOddVehicleBrandCode(String oddVehicleBrandCode) {
        this.oddVehicleBrandCode = oddVehicleBrandCode;
    }

    public String getOddVehicleBrandName() {
        return oddVehicleBrandName;
    }

    public void setOddVehicleBrandName(String oddVehicleBrandName) {
        this.oddVehicleBrandName = oddVehicleBrandName;
    }

    public String getOddVehicleSeriesCode() {
        return oddVehicleSeriesCode;
    }

    public void setOddVehicleSeriesCode(String oddVehicleSeriesCode) {
        this.oddVehicleSeriesCode = oddVehicleSeriesCode;
    }

    public String getOddVehicleSeriesName() {
        return oddVehicleSeriesName;
    }

    public void setOddVehicleSeriesName(String oddVehicleSeriesName) {
        this.oddVehicleSeriesName = oddVehicleSeriesName;
    }

    public String getOddVehicleSpecCode() {
        return oddVehicleSpecCode;
    }

    public void setOddVehicleSpecCode(String oddVehicleSpecCode) {
        this.oddVehicleSpecCode = oddVehicleSpecCode;
    }

    public String getOddVehicleSpecName() {
        return oddVehicleSpecName;
    }

    public void setOddVehicleSpecName(String oddVehicleSpecName) {
        this.oddVehicleSpecName = oddVehicleSpecName;
    }

    public String getOddVehicleSpecDesc() {
        return oddVehicleSpecDesc;
    }

    public void setOddVehicleSpecDesc(String oddVehicleSpecDesc) {
        this.oddVehicleSpecDesc = oddVehicleSpecDesc;
    }

    public String getOddVehiclePlate() {
        return oddVehiclePlate;
    }

    public void setOddVehiclePlate(String oddVehiclePlate) {
        this.oddVehiclePlate = oddVehiclePlate;
    }

    public String getOddVin() {
        return oddVin;
    }

    public void setOddVin(String oddVin) {
        this.oddVin = oddVin;
    }

    public String getOddEngine() {
        return oddEngine;
    }

    public void setOddEngine(String oddEngine) {
        this.oddEngine = oddEngine;
    }

    public String getOddWhCode() {
        return oddWhCode;
    }

    public void setOddWhCode(String oddWhCode) {
        this.oddWhCode = oddWhCode;
    }

    public String getOddWhName() {
        return oddWhName;
    }

    public void setOddWhName(String oddWhName) {
        this.oddWhName = oddWhName;
    }

    public String getOddWhZoneCode() {
        return oddWhZoneCode;
    }

    public void setOddWhZoneCode(String oddWhZoneCode) {
        this.oddWhZoneCode = oddWhZoneCode;
    }

    public String getOddWhZoneName() {
        return oddWhZoneName;
    }

    public void setOddWhZoneName(String oddWhZoneName) {
        this.oddWhZoneName = oddWhZoneName;
    }

    public String getOddWhLocCode() {
        return oddWhLocCode;
    }

    public void setOddWhLocCode(String oddWhLocCode) {
        this.oddWhLocCode = oddWhLocCode;
    }

    public String getOddWhLocName() {
        return oddWhLocName;
    }

    public void setOddWhLocName(String oddWhLocName) {
        this.oddWhLocName = oddWhLocName;
    }

    public Long getOddVehicleLength() {
        return oddVehicleLength;
    }

    public void setOddVehicleLength(Long oddVehicleLength) {
        this.oddVehicleLength = oddVehicleLength;
    }

    public Long getOddVehicleWidth() {
        return oddVehicleWidth;
    }

    public void setOddVehicleWidth(Long oddVehicleWidth) {
        this.oddVehicleWidth = oddVehicleWidth;
    }

    public Long getOddVehicleHigh() {
        return oddVehicleHigh;
    }

    public void setOddVehicleHigh(Long oddVehicleHigh) {
        this.oddVehicleHigh = oddVehicleHigh;
    }

    @Override
    public String toString() {
        return "WmsInboundOrderDetailVO{" +
                "oddId=" + oddId +
                ", oddOdId=" + oddOdId +
                ", oddOdCode='" + oddOdCode + '\'' +
                ", oddVehicleBrandCode='" + oddVehicleBrandCode + '\'' +
                ", oddVehicleBrandName='" + oddVehicleBrandName + '\'' +
                ", oddVehicleSeriesCode='" + oddVehicleSeriesCode + '\'' +
                ", oddVehicleSeriesName='" + oddVehicleSeriesName + '\'' +
                ", oddVehicleSpecCode='" + oddVehicleSpecCode + '\'' +
                ", oddVehicleSpecName='" + oddVehicleSpecName + '\'' +
                ", oddVehicleSpecDesc='" + oddVehicleSpecDesc + '\'' +
                ", oddVehiclePlate='" + oddVehiclePlate + '\'' +
                ", oddVin='" + oddVin + '\'' +
                ", oddEngine='" + oddEngine + '\'' +
                ", oddWhCode='" + oddWhCode + '\'' +
                ", oddWhName='" + oddWhName + '\'' +
                ", oddWhZoneCode='" + oddWhZoneCode + '\'' +
                ", oddWhZoneName='" + oddWhZoneName + '\'' +
                ", oddWhLocCode='" + oddWhLocCode + '\'' +
                ", oddWhLocName='" + oddWhLocName + '\'' +
                ", oddVehicleLength=" + oddVehicleLength +
                ", oddVehicleWidth=" + oddVehicleWidth +
                ", oddVehicleHigh=" + oddVehicleHigh +
                '}';
    }
}
