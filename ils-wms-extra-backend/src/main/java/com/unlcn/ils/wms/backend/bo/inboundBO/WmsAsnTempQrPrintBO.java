package com.unlcn.ils.wms.backend.bo.inboundBO;

import java.io.Serializable;

public class WmsAsnTempQrPrintBO implements Serializable {
    private String vin;

    public String getVin() {
        return vin;
    }

    public void setVin(String vin) {
        this.vin = vin;
    }
}
