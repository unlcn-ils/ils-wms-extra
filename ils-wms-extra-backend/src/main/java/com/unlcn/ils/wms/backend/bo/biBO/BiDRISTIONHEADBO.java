package com.unlcn.ils.wms.backend.bo.biBO;

import java.io.Serializable;

/**
 * Created by Nicky on 17/11/9.
 */
public class BiDRISTIONHEADBO implements Serializable {
    /**
     * 统计日期
     */
    private String rpt_date;

    /**
     * 发运数
     */
    private String out_qty;

    /**
     *在途数
     */
    private String onway_qty;

    /**
     *运抵数
     */
    private String arrival_qty;

    /**
     * 及时订单数
     */
    private String intime_qty;

    /**
     *配送及时率
     */
    private String intime_radio;


    public String getRpt_date() {
        return rpt_date;
    }

    public void setRpt_date(String rpt_date) {
        this.rpt_date = rpt_date;
    }

    public String getOut_qty() {
        return out_qty;
    }

    public void setOut_qty(String out_qty) {
        this.out_qty = out_qty;
    }

    public String getOnway_qty() {
        return onway_qty;
    }

    public void setOnway_qty(String onway_qty) {
        this.onway_qty = onway_qty;
    }

    public String getArrival_qty() {
        return arrival_qty;
    }

    public void setArrival_qty(String arrival_qty) {
        this.arrival_qty = arrival_qty;
    }

    public String getIntime_qty() {
        return intime_qty;
    }

    public void setIntime_qty(String intime_qty) {
        this.intime_qty = intime_qty;
    }

    public String getIntime_radio() {
        return intime_radio;
    }

    public void setIntime_radio(String intime_radio) {
        this.intime_radio = intime_radio;
    }

    @Override
    public String toString() {
        return "BiDRISTIONHEADBO{" +
                "rpt_date='" + rpt_date + '\'' +
                ", out_qty='" + out_qty + '\'' +
                ", onway_qty='" + onway_qty + '\'' +
                ", arrival_qty='" + arrival_qty + '\'' +
                ", intime_qty='" + intime_qty + '\'' +
                ", intime_radio='" + intime_radio + '\'' +
                '}';
    }
}
