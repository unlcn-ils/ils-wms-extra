package com.unlcn.ils.wms.backend.bo.outboundBO;

import java.util.Date;

/**
 * Created by DELL on 2017/9/26.
 */
public class WmsShipmentPlanBO {

    /**
     * 主键
     */
    private Long spId;

    /**
     * 调度单号
     */
    private String spDispatchNo;

    /**
     * 客户运单号
     */
    private String spWaybillNo;

    /**
     * 车架号(VIN码)
     */
    private String spVin;

    /**
     * 组板单号
     */
    private String spGroupBoardNo;

    /**
     * 订单号
     */
    private String spOrderNo;

    /**
     * 车型代码
     */
    private String spVehicleCode;

    /**
     * 车型名称
     */
    private String spVehicleName;

    /**
     * 配置
     */
    private String spConfigure;

    /**
     * 颜色
     */
    private String spCarColour;

    /**
     * 物料代码
     */
    private String spMaterialCode;

    /**
     * 组板数量
     */
    private Long spGroupBoardQuantity;

    /**
     * 经销商代码
     */
    private String spDealerCode;

    /**
     * 经销商名称
     */
    private String spDealerName;

    /**
     * 收货省
     */
    private String spProvince;

    /**
     * 收货市
     */
    private String spCity;

    /**
     * 收货区县
     */
    private String spDistrictCounty;

    /**
     * 收货详细地址
     */
    private String spDetailedAddress;

    /**
     * 承运商
     */
    private String spCarrier;

    /**
     * 板车车牌号
     */
    private String spSupplierVehiclePlate;

    /**
     * 预计装车时间
     */
    private Date spEstimateLoadingTime;

    public Long getSpId() {
        return spId;
    }

    public void setSpId(Long spId) {
        this.spId = spId;
    }

    public String getSpDispatchNo() {
        return spDispatchNo;
    }

    public void setSpDispatchNo(String spDispatchNo) {
        this.spDispatchNo = spDispatchNo;
    }

    public String getSpWaybillNo() {
        return spWaybillNo;
    }

    public void setSpWaybillNo(String spWaybillNo) {
        this.spWaybillNo = spWaybillNo;
    }

    public String getSpVin() {
        return spVin;
    }

    public void setSpVin(String spVin) {
        this.spVin = spVin;
    }

    public String getSpGroupBoardNo() {
        return spGroupBoardNo;
    }

    public void setSpGroupBoardNo(String spGroupBoardNo) {
        this.spGroupBoardNo = spGroupBoardNo;
    }

    public String getSpOrderNo() {
        return spOrderNo;
    }

    public void setSpOrderNo(String spOrderNo) {
        this.spOrderNo = spOrderNo;
    }

    public String getSpVehicleCode() {
        return spVehicleCode;
    }

    public void setSpVehicleCode(String spVehicleCode) {
        this.spVehicleCode = spVehicleCode;
    }

    public String getSpVehicleName() {
        return spVehicleName;
    }

    public void setSpVehicleName(String spVehicleName) {
        this.spVehicleName = spVehicleName;
    }

    public String getSpConfigure() {
        return spConfigure;
    }

    public void setSpConfigure(String spConfigure) {
        this.spConfigure = spConfigure;
    }

    public String getSpCarColour() {
        return spCarColour;
    }

    public void setSpCarColour(String spCarColour) {
        this.spCarColour = spCarColour;
    }

    public String getSpMaterialCode() {
        return spMaterialCode;
    }

    public void setSpMaterialCode(String spMaterialCode) {
        this.spMaterialCode = spMaterialCode;
    }

    public Long getSpGroupBoardQuantity() {
        return spGroupBoardQuantity;
    }

    public void setSpGroupBoardQuantity(Long spGroupBoardQuantity) {
        this.spGroupBoardQuantity = spGroupBoardQuantity;
    }

    public String getSpDealerCode() {
        return spDealerCode;
    }

    public void setSpDealerCode(String spDealerCode) {
        this.spDealerCode = spDealerCode;
    }

    public String getSpDealerName() {
        return spDealerName;
    }

    public void setSpDealerName(String spDealerName) {
        this.spDealerName = spDealerName;
    }

    public String getSpProvince() {
        return spProvince;
    }

    public void setSpProvince(String spProvince) {
        this.spProvince = spProvince;
    }

    public String getSpCity() {
        return spCity;
    }

    public void setSpCity(String spCity) {
        this.spCity = spCity;
    }

    public String getSpDistrictCounty() {
        return spDistrictCounty;
    }

    public void setSpDistrictCounty(String spDistrictCounty) {
        this.spDistrictCounty = spDistrictCounty;
    }

    public String getSpDetailedAddress() {
        return spDetailedAddress;
    }

    public void setSpDetailedAddress(String spDetailedAddress) {
        this.spDetailedAddress = spDetailedAddress;
    }

    public String getSpCarrier() {
        return spCarrier;
    }

    public void setSpCarrier(String spCarrier) {
        this.spCarrier = spCarrier;
    }

    public String getSpSupplierVehiclePlate() {
        return spSupplierVehiclePlate;
    }

    public void setSpSupplierVehiclePlate(String spSupplierVehiclePlate) {
        this.spSupplierVehiclePlate = spSupplierVehiclePlate;
    }

    public Date getSpEstimateLoadingTime() {
        return spEstimateLoadingTime;
    }

    public void setSpEstimateLoadingTime(Date spEstimateLoadingTime) {
        this.spEstimateLoadingTime = spEstimateLoadingTime;
    }

    @Override
    public String toString() {
        return "WmsShipmentPlanVO{" +
                "spId=" + spId +
                ", spDispatchNo='" + spDispatchNo + '\'' +
                ", spWaybillNo='" + spWaybillNo + '\'' +
                ", spVin='" + spVin + '\'' +
                ", spGroupBoardNo='" + spGroupBoardNo + '\'' +
                ", spOrderNo='" + spOrderNo + '\'' +
                ", spVehicleCode='" + spVehicleCode + '\'' +
                ", spVehicleName='" + spVehicleName + '\'' +
                ", spConfigure='" + spConfigure + '\'' +
                ", spCarColour='" + spCarColour + '\'' +
                ", spMaterialCode='" + spMaterialCode + '\'' +
                ", spGroupBoardQuantity=" + spGroupBoardQuantity +
                ", spDealerCode='" + spDealerCode + '\'' +
                ", spDealerName='" + spDealerName + '\'' +
                ", spProvince='" + spProvince + '\'' +
                ", spCity='" + spCity + '\'' +
                ", spDistrictCounty='" + spDistrictCounty + '\'' +
                ", spDetailedAddress='" + spDetailedAddress + '\'' +
                ", spCarrier='" + spCarrier + '\'' +
                ", spSupplierVehiclePlate='" + spSupplierVehiclePlate + '\'' +
                ", spEstimateLoadingTime=" + spEstimateLoadingTime +
                '}';
    }
}
