package com.unlcn.ils.wms.base.dto;

import org.springframework.format.annotation.DateTimeFormat;

import java.io.Serializable;

/**
 * <p>
 * tms接口出库确认推送订单信息到tms
 * </p>
 */
public class WmsAddTMSOutboundInterfaceDTO implements Serializable {
    private Long dataId;
    private String materialCode;
    private String handoverNumber;
    private String groupBoardNo;
    private String orderno;
    private Integer customId;
    private String customName;
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private String orderDate;
    private String creator;
    private String vin;
    private String modelId;
    private String modelName;
    private String modelDesc;
    private String departProvinceCode;
    private String departProvinceName;
    private String departCityCode;
    private String departCityName;
    private String departCountyCode;
    private String departCountyName;
    private String departAddr;
    private String departContact;
    private String departPhone;
    private String receiptProvinceCode;
    private String receiptProvinceName;
    private String receiptCityCode;
    private String receiptCityName;
    private String receiptCountyCode;
    private String receiptCountyName;
    private String receiptAddr;
    private String receiptContact;
    private String receiptPhone;
    private String distance;
    private String dtshipdate;
    private Integer burgent;
    private String dtarriveDate;
    private Integer count;
    private String vcrequire;
    private String remark;
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private String shipTime;
    private String whCode;

    public String getWhCode() {
        return whCode;
    }

    public void setWhCode(String whCode) {
        this.whCode = whCode;
    }

    public String getShipTime() {
        return shipTime;
    }

    public void setShipTime(String shipTime) {
        this.shipTime = shipTime;
    }

    public Long getDataId() {
        return dataId;
    }

    public void setDataId(Long dataId) {
        this.dataId = dataId;
    }

    public String getMaterialCode() {
        return materialCode;
    }

    public void setMaterialCode(String materialCode) {
        this.materialCode = materialCode;
    }

    public String getHandoverNumber() {
        return handoverNumber;
    }

    public void setHandoverNumber(String handoverNumber) {
        this.handoverNumber = handoverNumber;
    }

    public String getGroupBoardNo() {
        return groupBoardNo;
    }

    public void setGroupBoardNo(String groupBoardNo) {
        this.groupBoardNo = groupBoardNo;
    }

    public String getOrderno() {
        return orderno;
    }

    public void setOrderno(String orderno) {
        this.orderno = orderno;
    }

    public Integer getCustomId() {
        return customId;
    }

    public void setCustomId(Integer customId) {
        this.customId = customId;
    }

    public String getCustomName() {
        return customName;
    }

    public void setCustomName(String customName) {
        this.customName = customName;
    }

    public String getOrderDate() {
        return orderDate;
    }

    public void setOrderDate(String orderDate) {
        this.orderDate = orderDate;
    }

    public String getCreator() {
        return creator;
    }

    public void setCreator(String creator) {
        this.creator = creator;
    }

    public String getVin() {
        return vin;
    }

    public void setVin(String vin) {
        this.vin = vin;
    }

    public String getModelId() {
        return modelId;
    }

    public void setModelId(String modelId) {
        this.modelId = modelId;
    }

    public String getModelName() {
        return modelName;
    }

    public void setModelName(String modelName) {
        this.modelName = modelName;
    }

    public String getModelDesc() {
        return modelDesc;
    }

    public void setModelDesc(String modelDesc) {
        this.modelDesc = modelDesc;
    }

    public String getDepartProvinceCode() {
        return departProvinceCode;
    }

    public void setDepartProvinceCode(String departProvinceCode) {
        this.departProvinceCode = departProvinceCode;
    }

    public String getDepartProvinceName() {
        return departProvinceName;
    }

    public void setDepartProvinceName(String departProvinceName) {
        this.departProvinceName = departProvinceName;
    }

    public String getDepartCityCode() {
        return departCityCode;
    }

    public void setDepartCityCode(String departCityCode) {
        this.departCityCode = departCityCode;
    }

    public String getDepartCityName() {
        return departCityName;
    }

    public void setDepartCityName(String departCityName) {
        this.departCityName = departCityName;
    }

    public String getDepartCountyCode() {
        return departCountyCode;
    }

    public void setDepartCountyCode(String departCountyCode) {
        this.departCountyCode = departCountyCode;
    }

    public String getDepartCountyName() {
        return departCountyName;
    }

    public void setDepartCountyName(String departCountyName) {
        this.departCountyName = departCountyName;
    }

    public String getDepartAddr() {
        return departAddr;
    }

    public void setDepartAddr(String departAddr) {
        this.departAddr = departAddr;
    }

    public String getDepartContact() {
        return departContact;
    }

    public void setDepartContact(String departContact) {
        this.departContact = departContact;
    }

    public String getDepartPhone() {
        return departPhone;
    }

    public void setDepartPhone(String departPhone) {
        this.departPhone = departPhone;
    }

    public String getReceiptProvinceCode() {
        return receiptProvinceCode;
    }

    public void setReceiptProvinceCode(String receiptProvinceCode) {
        this.receiptProvinceCode = receiptProvinceCode;
    }

    public String getReceiptProvinceName() {
        return receiptProvinceName;
    }

    public void setReceiptProvinceName(String receiptProvinceName) {
        this.receiptProvinceName = receiptProvinceName;
    }

    public String getReceiptCityCode() {
        return receiptCityCode;
    }

    public void setReceiptCityCode(String receiptCityCode) {
        this.receiptCityCode = receiptCityCode;
    }

    public String getReceiptCityName() {
        return receiptCityName;
    }

    public void setReceiptCityName(String receiptCityName) {
        this.receiptCityName = receiptCityName;
    }

    public String getReceiptCountyCode() {
        return receiptCountyCode;
    }

    public void setReceiptCountyCode(String receiptCountyCode) {
        this.receiptCountyCode = receiptCountyCode;
    }

    public String getReceiptCountyName() {
        return receiptCountyName;
    }

    public void setReceiptCountyName(String receiptCountyName) {
        this.receiptCountyName = receiptCountyName;
    }

    public String getReceiptAddr() {
        return receiptAddr;
    }

    public void setReceiptAddr(String receiptAddr) {
        this.receiptAddr = receiptAddr;
    }

    public String getReceiptContact() {
        return receiptContact;
    }

    public void setReceiptContact(String receiptContact) {
        this.receiptContact = receiptContact;
    }

    public String getReceiptPhone() {
        return receiptPhone;
    }

    public void setReceiptPhone(String receiptPhone) {
        this.receiptPhone = receiptPhone;
    }

    public String getDistance() {
        return distance;
    }

    public void setDistance(String distance) {
        this.distance = distance;
    }

    public String getDtshipdate() {
        return dtshipdate;
    }

    public void setDtshipdate(String dtshipdate) {
        this.dtshipdate = dtshipdate;
    }

    public Integer getBurgent() {
        return burgent;
    }

    public void setBurgent(Integer burgent) {
        this.burgent = burgent;
    }

    public String getDtarriveDate() {
        return dtarriveDate;
    }

    public void setDtarriveDate(String dtarriveDate) {
        this.dtarriveDate = dtarriveDate;
    }

    public Integer getCount() {
        return count;
    }

    public void setCount(Integer count) {
        this.count = count;
    }

    public String getVcrequire() {
        return vcrequire;
    }

    public void setVcrequire(String vcrequire) {
        this.vcrequire = vcrequire;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }
}
