package com.unlcn.ils.wms.web.utils.JMSUtil;

import javax.jms.Message;

/**
 *  提供消息操作的回调接口
 * Created by DELL on 2017/8/28.
 */
public interface MessageHandler {
    public void handle(Message message);
}
