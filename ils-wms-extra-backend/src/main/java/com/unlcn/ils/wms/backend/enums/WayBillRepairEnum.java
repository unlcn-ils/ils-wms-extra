package com.unlcn.ils.wms.backend.enums;

/**
 * Created by zhiche024 on 2017/7/25.
 */
public enum  WayBillRepairEnum {
    WAYBILL_PROCESSED(20,"已处理"),
    WAYBILL_UNTREATED(10,"未处理");

    private final int value;
    private final String text;
    WayBillRepairEnum (int value,String text){
        this.value = value;
        this.text = text;
    }

    public int getValue() {
        return value;
    }

    public String getText() {
        return text;
    }

    public static WayBillRepairEnum getByValue(int value){
        for (WayBillRepairEnum temp : WayBillRepairEnum.values()) {
            if(temp.getValue() == value){
                return temp;
            }
        }
        return null;
    }
}
