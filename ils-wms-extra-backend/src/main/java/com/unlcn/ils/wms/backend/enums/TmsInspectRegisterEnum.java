package com.unlcn.ils.wms.backend.enums;

public enum TmsInspectRegisterEnum {
    PASS(20, "验车合格"),
    COMPROMISE_PASS(60, "让步合格"),
    DEFECT_PASS(90, "带伤发运");

    private final int value;
    private final String text;

    TmsInspectRegisterEnum(int value, String text) {
        this.value = value;
        this.text = text;
    }

    public int getValue() {
        return value;
    }


    public String getText() {
        return text;
    }

}
