package com.unlcn.ils.wms.web.vo.inbound;

import com.unlcn.ils.wms.backend.bo.baseDataBO.WmsCommonBO;

/**
 * Created by DELL on 2017/9/1.
 */
public class WmsInboundPloyGenResultVO extends WmsCommonBO {
    /**
     * 生成结果主id
     */
    private Long pygrrId;

    /**
     * 策略主id
     */
    private String pygrrPyId;

    /**
     * 策略主code
     */
    private String pygrrPyCode;

    /**
     * 策略条件id
     */
    private String pygrrPycId;

    /**
     * 生成结果
     */
    private String pygrrResult;

    public Long getPygrrId() {
        return pygrrId;
    }

    public void setPygrrId(Long pygrrId) {
        this.pygrrId = pygrrId;
    }

    public String getPygrrPyId() {
        return pygrrPyId;
    }

    public void setPygrrPyId(String pygrrPyId) {
        this.pygrrPyId = pygrrPyId;
    }

    public String getPygrrPyCode() {
        return pygrrPyCode;
    }

    public void setPygrrPyCode(String pygrrPyCode) {
        this.pygrrPyCode = pygrrPyCode;
    }

    public String getPygrrPycId() {
        return pygrrPycId;
    }

    public void setPygrrPycId(String pygrrPycId) {
        this.pygrrPycId = pygrrPycId;
    }

    public String getPygrrResult() {
        return pygrrResult;
    }

    public void setPygrrResult(String pygrrResult) {
        this.pygrrResult = pygrrResult;
    }

    @Override
    public String toString() {
        return "WmsInboundPloyGenResultVO{" +
                "pygrrId=" + pygrrId +
                ", pygrrPyId='" + pygrrPyId + '\'' +
                ", pygrrPyCode='" + pygrrPyCode + '\'' +
                ", pygrrPycId='" + pygrrPycId + '\'' +
                ", pygrrResult='" + pygrrResult + '\'' +
                '}';
    }
}
