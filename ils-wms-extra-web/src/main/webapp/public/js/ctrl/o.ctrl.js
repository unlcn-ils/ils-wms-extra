
/**
 * Created by houjianhui on 2017/7/19.
 */
'use strict';

wmsCtrl.controller('outboundOrderCtrl', ['$scope','outboundOrderService', function (scope,outboundOrderService) {   //备料单

    //更多条件默认值
    scope.moreText = scope.constant.moreText.spread;
    //更多条件
    scope.moreClick = function(){
        scope.otherForm = !scope.otherForm;
        if(scope.otherForm){
            scope.moreText = scope.constant.moreText.packUp;
        }else{
            scope.moreText = scope.constant.moreText.spread;
        }
    }

    //列表查询接口参数构造
    scope.listParam = function(){

        if(!scope.search){
            scope.search = {};
        }
        scope.search.pageSize = scope.pageSize;    //设置最大页
        if(scope.otherForm){    //展开了更多
            return scope.search;
        }else{  //没有展开
            return scope.search;
        }
    }

    //列表
    scope.fristSelect = true;   //判断是否是第一次查询
    scope.list = function(params){
        //设置分页参数
        if(!params){
            params = {};
        }
        params.pageSize = params.pageSize ? params.pageSize : scope.constant.page.pageSize;
        params.pageNo = params.pageNo ? params.pageNo : scope.constant.page.pageNo

        outboundOrderService.list(params)
            .then(function(result){
                if(result.success){
                    scope.outboundOrderList = result.data.outboundBLOrderList;
                    if(scope.fristSelect){
                        scope.Tools.page(document.getElementById('pages'),{
                            pages : result.data.outboundBLOrderListPageInfo.totalPage,
                            curr : result.data.outboundBLOrderListPageInfo.pageNo,
                            count : result.data.outboundBLOrderListPageInfo.totalRecord
                        },function(obj, first){
                            scope.fristSelect = first;  //设置状态
                            //初始化多选框
                            scope.Tools.initCheckBox();
                            if(!first){ //判断不是第一次加载
                                var param = scope.listParam();
                                param.pageNo = obj.curr;    //当前页
                                param.pageSize = obj.limit; //最大页
                                scope.pageSize = obj.limit; //设置最大页
                                scope.pageNo = obj.curr; //设置当前页
                                scope.list(param);
                            }
                        })
                    }else{
                        //初始化多选框
                        scope.Tools.initCheckBox();
                    }
                }else{
                    scope.Tools.alert(result.message);
                }
            })
    }

    //列表
    scope.list();

    //回车按下查询
    scope.searchListKeyDown = function(e){
        var keycode = window.event ? e.keyCode:e.which;
        if(keycode == 13){  //回车键
            scope.fristSelect = true;   //设置为第一次查询
            scope.list(scope.listParam());
            e.preventDefault();
        }
    }

    //点击每行列表的数据 click
    scope.selectItemClick = function(item){
        outboundOrderService.getOutOrderByBLId(item.blId)
            .then(function(result){
                if(result.success){
                    scope.outboundOrderDetailList = result.data
                }else{
                    scope.Tools.alert(result.message);
                }
            })
    }

    //查询 click
    scope.searchClick = function(){
        scope.fristSelect = true;   //设置为第一次查询
        scope.list(scope.listParam());
    }

    //重置 click
    scope.resetClick = function(){
        scope.search = {};
    }

    //刷新 click
    scope.refreshClick = function(){
        scope.searchClick();
    }

}]);