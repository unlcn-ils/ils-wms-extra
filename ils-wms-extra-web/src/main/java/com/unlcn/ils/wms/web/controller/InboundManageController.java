package com.unlcn.ils.wms.web.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpServletResponse;

/**
 * @Author ：Ligl
 * @Date : 2017/8/9.
 * 入库管理
 */
@Controller
@RequestMapping("/inboundManage")
public class InboundManageController {


    /**
     * 跳转到入库单管理
     *
     * @return
     * @throws Exception
     */
    @RequestMapping("/to_inbound_manage")
    public String toInboundManage(HttpServletResponse response) throws Exception {
        return "inbound/inbound-manage";
    }
}
