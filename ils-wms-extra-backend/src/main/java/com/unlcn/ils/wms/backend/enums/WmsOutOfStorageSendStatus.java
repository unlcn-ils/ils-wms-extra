package com.unlcn.ils.wms.backend.enums;

/**
 * 君马出入库发送状态
 *
 * @author renml
 */
public enum WmsOutOfStorageSendStatus {
    SEND_INIT("0", "未处理"),
    SEND_SUCCESS("1", "发送成功"),
    SEND_FAILED("2", "发送失败");

    private final String value;
    private final String text;

    WmsOutOfStorageSendStatus(String value, String text) {
        this.value = value;
        this.text = text;
    }

    public String getValue() {
        return value;
    }

    public String getText() {
        return text;
    }

    public static WmsOutOfStorageSendStatus getByValue(String value) {
        for (WmsOutOfStorageSendStatus temp : WmsOutOfStorageSendStatus.values()) {
            if (temp.getValue().equals(value)) {
                return temp;
            }
        }
        return null;
    }
}
