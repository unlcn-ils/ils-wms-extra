package com.unlcn.ils.wms.backend.bo.pickup;

import java.util.List;

/**
 * Created by houjianhui on 2017/7/24.
 */
public class TmsPickupExcpBO {
    private Long id;
    private Integer position;
    private Integer errorCount;
    private List<TmsExcpBO> excpList;
    private List<TmsExcpAttachBO> attachList;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Integer getErrorCount() {
        return errorCount;
    }

    public void setErrorCount(Integer errorCount) {
        this.errorCount = errorCount;
    }

    public List<TmsExcpBO> getExcpList() {
        return excpList;
    }

    public void setExcpList(List<TmsExcpBO> excpList) {
        this.excpList = excpList;
    }

    public List<TmsExcpAttachBO> getAttachList() {
        return attachList;
    }

    public void setAttachList(List<TmsExcpAttachBO> attachList) {
        this.attachList = attachList;
    }

    public Integer getPosition() {
        return position;
    }

    public void setPosition(Integer position) {
        this.position = position;
    }

    @Override
    public String toString() {
        return "TmsPickupExcpBO{" +
                "id=" + id +
                ", position=" + position +
                ", errorCount=" + errorCount +
                ", excpList=" + excpList +
                ", attachList=" + attachList +
                '}';
    }
}
