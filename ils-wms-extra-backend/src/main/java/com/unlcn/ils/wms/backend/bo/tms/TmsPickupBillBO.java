package com.unlcn.ils.wms.backend.bo.tms;

import java.io.Serializable;
import java.util.Date;

public class TmsPickupBillBO implements Serializable {
    private Long id;
    private String waybillCode;
    private Date generateTime;
    private String vehicle;
    private String destProvince;
    private String destCity;
    private String cacheWarehouse;
    private String vin;
    private String warehouseName;
    private String engineNo;
    private Integer pickupStatus;
    private Integer inspectStatus;
    private String pickupUserId;
    private Date pickupTime;
    private Date gmtCreate;
    private Date gmtUpdate;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getWaybillCode() {
        return waybillCode;
    }

    public void setWaybillCode(String waybillCode) {
        this.waybillCode = waybillCode;
    }

    public Date getGenerateTime() {
        return generateTime;
    }

    public void setGenerateTime(Date generateTime) {
        this.generateTime = generateTime;
    }

    public String getVehicle() {
        return vehicle;
    }

    public void setVehicle(String vehicle) {
        this.vehicle = vehicle;
    }

    public String getDestProvince() {
        return destProvince;
    }

    public void setDestProvince(String destProvince) {
        this.destProvince = destProvince;
    }

    public String getDestCity() {
        return destCity;
    }

    public void setDestCity(String destCity) {
        this.destCity = destCity;
    }

    public String getCacheWarehouse() {
        return cacheWarehouse;
    }

    public void setCacheWarehouse(String cacheWarehouse) {
        this.cacheWarehouse = cacheWarehouse;
    }

    public String getVin() {
        return vin;
    }

    public void setVin(String vin) {
        this.vin = vin;
    }

    public String getWarehouseName() {
        return warehouseName;
    }

    public void setWarehouseName(String warehouseName) {
        this.warehouseName = warehouseName;
    }

    public String getEngineNo() {
        return engineNo;
    }

    public void setEngineNo(String engineNo) {
        this.engineNo = engineNo;
    }

    public Integer getPickupStatus() {
        return pickupStatus;
    }

    public void setPickupStatus(Integer pickupStatus) {
        this.pickupStatus = pickupStatus;
    }

    public Integer getInspectStatus() {
        return inspectStatus;
    }

    public void setInspectStatus(Integer inspectStatus) {
        this.inspectStatus = inspectStatus;
    }

    public String getPickupUserId() {
        return pickupUserId;
    }

    public void setPickupUserId(String pickupUserId) {
        this.pickupUserId = pickupUserId;
    }

    public Date getPickupTime() {
        return pickupTime;
    }

    public void setPickupTime(Date pickupTime) {
        this.pickupTime = pickupTime;
    }

    public Date getGmtCreate() {
        return gmtCreate;
    }

    public void setGmtCreate(Date gmtCreate) {
        this.gmtCreate = gmtCreate;
    }

    public Date getGmtUpdate() {
        return gmtUpdate;
    }

    public void setGmtUpdate(Date gmtUpdate) {
        this.gmtUpdate = gmtUpdate;
    }

    @Override
    public String toString() {
        return "TmsPickupBillBO{" +
                "id=" + id +
                ", waybillCode='" + waybillCode + '\'' +
                ", generateTime=" + generateTime +
                ", vehicle='" + vehicle + '\'' +
                ", destProvince='" + destProvince + '\'' +
                ", destCity='" + destCity + '\'' +
                ", cacheWarehouse='" + cacheWarehouse + '\'' +
                ", vin='" + vin + '\'' +
                ", warehouseName='" + warehouseName + '\'' +
                ", engineNo='" + engineNo + '\'' +
                ", pickupStatus=" + pickupStatus +
                ", inspectStatus=" + inspectStatus +
                ", pickupUserId='" + pickupUserId + '\'' +
                ", pickupTime=" + pickupTime +
                ", gmtCreate=" + gmtCreate +
                ", gmtUpdate=" + gmtUpdate +
                '}';
    }
}
