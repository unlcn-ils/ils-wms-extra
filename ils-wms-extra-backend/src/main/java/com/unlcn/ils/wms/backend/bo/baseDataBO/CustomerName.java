package com.unlcn.ils.wms.backend.bo.baseDataBO;

import java.io.Serializable;

public class CustomerName implements Serializable {
    private String name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
