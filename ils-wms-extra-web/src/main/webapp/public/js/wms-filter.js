/**
 * Created by lsj on 2017/8/17.
 */
'use strict';
var wmsFilter = angular.module('wms.filter', [])
    .filter('billsTypeFilter',['constant',function(constant){   //单据类型过滤器
        return function(input,param){
            if(input == constant.billsType.systemSchedul){  //系统调度
                return constant.billsTypeName.systemSchedul;
            }else　if(input == constant.billsType.manualEntry){  //手工录入
                return constant.billsTypeName.manualEntry;
            }
            return input;
        }
    }])
    .filter('inspectionResultsFilter',['constant',function(constant){   //检验结果
        return function(input,param){
            if(input == constant.inspectionResults.asnOrderPass){   //合格
                return constant.inspectionResultsName.asnOrderPass;
            }else　if(input == constant.inspectionResults.asnOrderNotpassInRepair){  //不合格-库位返修
                return constant.inspectionResultsName.asnOrderNotpassInRepair;
            }else　if(input == constant.inspectionResults.asnOrderNotpassOutRepair){  //不合格-委外返工
                return constant.inspectionResultsName.asnOrderNotpassOutRepair;
            }else　if(input == constant.inspectionResults.asnOrderNotpass){  //不合格-退厂
                return constant.inspectionResultsName.asnOrderNotpass;
            }
            return input;
        }
    }])
    .filter('orderStatusFilter',['constant',function(constant){   //订单状态
        return function(input,param){
            if(input == constant.orderStatus.wmsInboundCreate){   //未收货
                return constant.orderStatusName.wmsInboundCreate;
            }else　if(input == constant.orderStatus.wmsInboundWaitAllocation){  //已收货
                return constant.orderStatusName.wmsInboundWaitAllocation;
            }else　if(input == constant.orderStatus.wmsInboundFinishAllocation){  ////已分配
                return constant.orderStatusName.wmsInboundFinishAllocation;
            }else　if(input == constant.orderStatus.wmsInboundConfirmAllocation){  //待入库
                return constant.orderStatusName.wmsInboundConfirmAllocation;
            }else　if(input == constant.orderStatus.wmsInboundFinish){  //已入库
                return constant.orderStatusName.wmsInboundFinish;
            }
            return input;
        }
    }])
    .filter('stockUnitFilter',['constant',function(constant){   //库位状态
        return function(input,param){
            if(input == constant.stockUnitStatus.start){   //启用
                return constant.stockUnitStatusName.start;
            }else　if(input == constant.stockUnitStatus.stop){  //停用
                return constant.stockUnitStatusName.stop;
            }
            return input;
        }
    }])
    .filter('repairTypeFilter',['constant',function(constant){   //维修类型
        return function(input,param){
            if(input == constant.repairType.inner){   //委内维修
                return constant.repairTypeName.inner;
            }else　if(input == constant.repairType.outer){  //委外维修
                return constant.repairTypeName.outer;
            }
            return input;
        }
    }])
    .filter('repairStatusFilter',['constant',function(constant){    //维修状态
        return function(input,param){
            if(input == constant.repairStatus.unprocessed){   //未处理
                return constant.repairStatusName.unprocessed;
            }else　if(input == constant.repairStatus.maintenance){  //维修中
                return constant.repairStatusName.maintenance;
            }else　if(input == constant.repairStatus.finish){  //已完成
                return constant.repairStatusName.finish;
            }
            return input;
        }
    }])
    .filter('repairResultFilter',['constant',function(constant){    //维修结果
        return function(input,param){
            if(input == constant.repairResult.qualified){   //合格
                return constant.repairResultName.qualified;
            }else　if(input == constant.repairResult.disqualification){  //不合格
                return constant.repairResultName.disqualification;
            }else　if(input == constant.repairResult.cancellinStocks){  //退库
                return constant.repairResultName.cancellinStocks;
            }
            return input;
        }
    }])
    .filter('warehouseFilter',['constant',function(constant){    //出库状态
        return function(input,param){
            if(input == constant.warehouse.unprocessed){   //未处理
                return constant.warehouseName.unprocessed;
            }else　if(input == constant.warehouse.material){  //已备料
                return constant.warehouseName.material;
            }else　if(input == constant.warehouse.outbound){  //待出库
                return constant.warehouseName.outbound;
            }else　if(input == constant.warehouse.haveOutbound){  //已出库
                return constant.warehouseName.haveOutbound;
            }
            return input;
        }
    }])
