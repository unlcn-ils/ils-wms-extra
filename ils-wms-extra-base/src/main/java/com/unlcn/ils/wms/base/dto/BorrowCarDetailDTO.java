package com.unlcn.ils.wms.base.dto;

/**
 * Created by lenovo on 2017/10/24.
 */
public class BorrowCarDetailDTO {

    private Long bdId;

    private Long bdBorrowId;

    private Long bdReturnId;

    private String bdVin;

    private String bdMaterialCode;

    private String bdMaterialName;

    private String bdCarColorname;

    private String bdCarSpecname;

    private String bdCarSeries;

    // 库区code
    private String invlocZoneCode;

    // 库位code
    private String invlocLocCode;

    // 库区名称
    private String invlocZoneName;

    // 库位名称
    private String invlocLocName;

    public Long getBdId() {
        return bdId;
    }

    public void setBdId(Long bdId) {
        this.bdId = bdId;
    }

    public Long getBdBorrowId() {
        return bdBorrowId;
    }

    public void setBdBorrowId(Long bdBorrowId) {
        this.bdBorrowId = bdBorrowId;
    }

    public Long getBdReturnId() {
        return bdReturnId;
    }

    public void setBdReturnId(Long bdReturnId) {
        this.bdReturnId = bdReturnId;
    }

    public String getBdVin() {
        return bdVin;
    }

    public void setBdVin(String bdVin) {
        this.bdVin = bdVin;
    }

    public String getBdMaterialCode() {
        return bdMaterialCode;
    }

    public void setBdMaterialCode(String bdMaterialCode) {
        this.bdMaterialCode = bdMaterialCode;
    }

    public String getBdMaterialName() {
        return bdMaterialName;
    }

    public void setBdMaterialName(String bdMaterialName) {
        this.bdMaterialName = bdMaterialName;
    }

    public String getBdCarColorname() {
        return bdCarColorname;
    }

    public void setBdCarColorname(String bdCarColorname) {
        this.bdCarColorname = bdCarColorname;
    }

    public String getBdCarSpecname() {
        return bdCarSpecname;
    }

    public void setBdCarSpecname(String bdCarSpecname) {
        this.bdCarSpecname = bdCarSpecname;
    }

    public String getBdCarSeries() {
        return bdCarSeries;
    }

    public void setBdCarSeries(String bdCarSeries) {
        this.bdCarSeries = bdCarSeries;
    }

    public String getInvlocZoneCode() {
        return invlocZoneCode;
    }

    public void setInvlocZoneCode(String invlocZoneCode) {
        this.invlocZoneCode = invlocZoneCode;
    }

    public String getInvlocLocCode() {
        return invlocLocCode;
    }

    public void setInvlocLocCode(String invlocLocCode) {
        this.invlocLocCode = invlocLocCode;
    }

    public String getInvlocZoneName() {
        return invlocZoneName;
    }

    public void setInvlocZoneName(String invlocZoneName) {
        this.invlocZoneName = invlocZoneName;
    }

    public String getInvlocLocName() {
        return invlocLocName;
    }

    public void setInvlocLocName(String invlocLocName) {
        this.invlocLocName = invlocLocName;
    }

    @Override
    public String toString() {
        return "BorrowCarDetailDTO{" +
                "bdId=" + bdId +
                ", bdBorrowId=" + bdBorrowId +
                ", bdReturnId=" + bdReturnId +
                ", bdVin='" + bdVin + '\'' +
                ", bdMaterialCode='" + bdMaterialCode + '\'' +
                ", bdMaterialName='" + bdMaterialName + '\'' +
                ", bdCarColorname='" + bdCarColorname + '\'' +
                ", bdCarSpecname='" + bdCarSpecname + '\'' +
                ", bdCarSeries='" + bdCarSeries + '\'' +
                ", invlocZoneCode='" + invlocZoneCode + '\'' +
                ", invlocLocCode='" + invlocLocCode + '\'' +
                ", invlocZoneName='" + invlocZoneName + '\'' +
                ", invlocLocName='" + invlocLocName + '\'' +
                '}';
    }
}
