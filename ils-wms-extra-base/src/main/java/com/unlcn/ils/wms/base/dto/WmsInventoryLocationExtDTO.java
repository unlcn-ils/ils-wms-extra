package com.unlcn.ils.wms.base.dto;

import java.util.Date;

/**
 * @Auther linbao
 * @Date 2017-11-13
 */
public class WmsInventoryLocationExtDTO {

    private Long invlocId;
    private String materialCode;
    private String materialName;
    private String colourCode;
    private String colourName;

    /**
     * 货主
     */
    private String invCustomerName;

    /**
     * 底盘号
     */
    private String invlocVin;

    /**
     * 库区
     */
    private String invlocZoneName;

    /**
     * 库位
     */
    private String invlocLocCode;

    /**
     * 仓库代码
     */
    private String invWhCode;

    /**
     * 仓库名称
     */
    private String invWhName;

    /**
     * 车名名称
     */
    private String invVehicleSpecName;

    /**
     * 车型描述
     */
    private String invVehicleSpecDesc;

    /**
     * 生产日期
     */
    private Date atProductionDate;

    /**
     * 下线日期
     */
    private Date atOfflineDate;

    /**
     * 入库日期
     */
    private Date gmtCreate;

    /**
     * 库存状态
     */
    private String status;

    /**
     * 库龄
     */
    private int warehouseAge;

    public Long getInvlocId() {
        return invlocId;
    }

    public void setInvlocId(Long invlocId) {
        this.invlocId = invlocId;
    }

    public String getMaterialCode() {
        return materialCode;
    }

    public void setMaterialCode(String materialCode) {
        this.materialCode = materialCode;
    }

    public String getMaterialName() {
        return materialName;
    }

    public void setMaterialName(String materialName) {
        this.materialName = materialName;
    }

    public String getColourCode() {
        return colourCode;
    }

    public void setColourCode(String colourCode) {
        this.colourCode = colourCode;
    }

    public String getColourName() {
        return colourName;
    }

    public void setColourName(String colourName) {
        this.colourName = colourName;
    }

    public String getInvCustomerName() {
        return invCustomerName;
    }

    public void setInvCustomerName(String invCustomerName) {
        this.invCustomerName = invCustomerName;
    }

    public String getInvlocVin() {
        return invlocVin;
    }

    public void setInvlocVin(String invlocVin) {
        this.invlocVin = invlocVin;
    }

    public String getInvlocZoneName() {
        return invlocZoneName;
    }

    public void setInvlocZoneName(String invlocZoneName) {
        this.invlocZoneName = invlocZoneName;
    }

    public String getInvlocLocCode() {
        return invlocLocCode;
    }

    public void setInvlocLocCode(String invlocLocCode) {
        this.invlocLocCode = invlocLocCode;
    }

    public String getInvWhCode() {
        return invWhCode;
    }

    public void setInvWhCode(String invWhCode) {
        this.invWhCode = invWhCode;
    }

    public String getInvWhName() {
        return invWhName;
    }

    public void setInvWhName(String invWhName) {
        this.invWhName = invWhName;
    }

    public String getInvVehicleSpecName() {
        return invVehicleSpecName;
    }

    public void setInvVehicleSpecName(String invVehicleSpecName) {
        this.invVehicleSpecName = invVehicleSpecName;
    }

    public String getInvVehicleSpecDesc() {
        return invVehicleSpecDesc;
    }

    public void setInvVehicleSpecDesc(String invVehicleSpecDesc) {
        this.invVehicleSpecDesc = invVehicleSpecDesc;
    }

    public Date getAtProductionDate() {
        return atProductionDate;
    }

    public void setAtProductionDate(Date atProductionDate) {
        this.atProductionDate = atProductionDate;
    }

    public Date getAtOfflineDate() {
        return atOfflineDate;
    }

    public void setAtOfflineDate(Date atOfflineDate) {
        this.atOfflineDate = atOfflineDate;
    }

    public Date getGmtCreate() {
        return gmtCreate;
    }

    public void setGmtCreate(Date gmtCreate) {
        this.gmtCreate = gmtCreate;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public int getWarehouseAge() {
        return warehouseAge;
    }

    public void setWarehouseAge(int warehouseAge) {
        this.warehouseAge = warehouseAge;
    }
}
