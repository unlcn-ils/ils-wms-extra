package com.unlcn.ils.wms.backend.service.inbound;

import com.unlcn.ils.wms.base.dto.WmsForTmsInterfaceDTO;

import java.util.ArrayList;
import java.util.Map;

public interface WmsInterfaceForTmsService {
    ArrayList<Map<String, String>> updateInboundOrderOld(WmsForTmsInterfaceDTO interfaceDTO);

    ArrayList<Map<String,String>> updateInboundOrder(WmsForTmsInterfaceDTO interfaceDTO);
}
