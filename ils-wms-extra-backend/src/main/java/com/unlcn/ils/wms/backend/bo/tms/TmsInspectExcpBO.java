package com.unlcn.ils.wms.backend.bo.tms;

/**
 * @Author ：Ligl
 * @Date : 2017/9/14.
 */
public class TmsInspectExcpBO {

    private Long inspectId;
    private String excpDetails;
    private String vin;
    private String userId;
    private Integer positionId;



    public Integer getPositionId() {
        return positionId;
    }

    public void setPositionId(Integer positionId) {
        this.positionId = positionId;
    }

    public Long getInspectId() {
        return inspectId;
    }

    public void setInspectId(Long inspectId) {
        this.inspectId = inspectId;
    }

    public String getExcpDetails() {
        return excpDetails;
    }

    public void setExcpDetails(String excpDetails) {
        this.excpDetails = excpDetails;
    }

    public String getVin() {
        return vin;
    }

    public void setVin(String vin) {
        this.vin = vin;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    @Override
    public String toString() {
        return "TmsInspectExcpBO{" +
                "inspectId=" + inspectId +
                ", excpDetails='" + excpDetails + '\'' +
                ", vin='" + vin + '\'' +
                ", userId='" + userId + '\'' +
                ", positionId=" + positionId +
                '}';
    }
}
