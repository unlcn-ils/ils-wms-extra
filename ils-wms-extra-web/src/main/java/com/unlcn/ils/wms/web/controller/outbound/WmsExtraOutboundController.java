package com.unlcn.ils.wms.web.controller.outbound;

import cn.huiyunche.commons.domain.ResultDTO;
import cn.huiyunche.commons.exception.BusinessException;
import com.unlcn.ils.wms.backend.bo.inboundBO.WmsInboundOrderBO;
import com.unlcn.ils.wms.backend.service.inbound.AsnOrderService;
import com.unlcn.ils.wms.backend.service.outbound.OutBoundTaskService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * Created by DELL on 2017/9/28.
 */
@Controller
@RequestMapping("/extraOutbound")
@ResponseBody
public class WmsExtraOutboundController {

    private Logger LOGGER = LoggerFactory.getLogger(WmsExtraOutboundController.class);
    @Autowired
    AsnOrderService asnOrderService;

    @Autowired
    OutBoundTaskService outBoundTaskService;

    /**
     * 重写钥匙匹配方法；
     *
     * @param vin
     * @return
     * @throws Exception
     */
    @RequestMapping("/contrastInfo/{vin}")
    public ResultDTO carInfo(@PathVariable("vin") String vin) throws Exception {
        LOGGER.info("WmsExtraOutboundController.carInfo param:{}", vin);
        ResultDTO<Object> resultDTO = new ResultDTO<>();
        try {
            WmsInboundOrderBO wmsInboundOrderBO = asnOrderService.getKeysInfoByVin(vin);
            resultDTO.setSuccess(true);
            resultDTO.setData(wmsInboundOrderBO);
        } catch (Exception e) {
            resultDTO.setData(null);
            resultDTO.setSuccess(false);
            resultDTO.setMessage("查询车架号信息异常!");
            e.printStackTrace();
        }
        return resultDTO;
    }

    /**
     * 对比成功领取钥匙之后修改出库任务状态为30：进行中
     *
     * @param vin
     * @return
     * @throws Exception
     */
    @RequestMapping("/updateStatus/{vin}")
    public ResultDTO updateStatus(@PathVariable("vin") String vin) throws Exception {
        ResultDTO resultDTO = new ResultDTO();
        try {
            outBoundTaskService.updateStatusByVin(vin);
            resultDTO.setSuccess(true);
            resultDTO.setMessage("修改出库单状态成功");
            return resultDTO;
        } catch (Exception ex) {
            throw new BusinessException(ex + "修改出库单状态异常");

        }
    }


}
