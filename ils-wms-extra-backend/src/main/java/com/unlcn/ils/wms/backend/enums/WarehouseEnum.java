package com.unlcn.ils.wms.backend.enums;

/**
 * @Auther linbao
 * @Date 2017-10-21
 */
public enum WarehouseEnum {

    UNLCN_XN_CQ("UNLCN_XN_CQ", "重庆库"),
    JM_CS("JM_CS", "君马长沙库"),
    JM_XY("JM_XY", "君马襄阳库"),
    JMC_NC_QS("JMC_NC_QS", "全顺库");

    private final String whCode;

    private final String whName;

    public String getWhCode() {
        return whCode;
    }

    public String getWhName() {
        return whName;
    }

    WarehouseEnum(String whCode, String whName) {
        this.whCode = whCode;
        this.whName = whName;
    }
}
