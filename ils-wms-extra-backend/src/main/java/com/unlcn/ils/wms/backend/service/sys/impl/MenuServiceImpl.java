package com.unlcn.ils.wms.backend.service.sys.impl;

import cn.huiyunche.commons.exception.BusinessException;
import com.unlcn.ils.wms.backend.bo.sys.MenuBO;
import com.unlcn.ils.wms.backend.enums.StatusEnum;
import com.unlcn.ils.wms.backend.service.sys.MenuService;
import com.unlcn.ils.wms.backend.service.sys.PermissionsService;
import com.unlcn.ils.wms.base.mapper.extmapper.SysMenuCustomMapper;
import com.unlcn.ils.wms.base.mapper.sys.SysMenuMapper;
import com.unlcn.ils.wms.base.model.sys.SysMenu;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * 
 * @ClassName: MenuServiceImpl 
 * @Description: 菜单 service impl
 * @author laishijian
 * @date 2017年8月9日 上午10:22:58 
 *
 */
@Service
public class MenuServiceImpl implements MenuService {
	
	@Autowired
	private SysMenuMapper menuMapper;
	
	@Autowired
	private SysMenuCustomMapper menuCustomMapper;
	
	@Autowired
	private PermissionsService permissionsService;
	
	@Override
	public Integer add(MenuBO menuBO) {
		
		SysMenu menu = new SysMenu();
		BeanUtils.copyProperties(menuBO, menu);
		
		validate(menu);	//校验属性
		
		//TODO 菜单名校验
		
		menu.setEnable(StatusEnum.RIGHT.getValue());	//状态
		menu.setGmtCreate(new Date());	//创建时间
		menu.setGmtModified(new Date());	//修改时间
		
		menuMapper.insert(menu);
		return menu.getId();
	}
	
	
	@Override
	public List<MenuBO> findMenuAndChildMenu() throws Exception{
		return findMenuAndChildByParentId(null);
	}
	
	@Override
	public List<MenuBO> findByParentId(Integer parentId) throws Exception{
		
		List<SysMenu> menuList = menuCustomMapper.selectByParentId(parentId);
		
		List<MenuBO> menuBOList = new ArrayList<>();
		for(SysMenu menu : menuList) {
			MenuBO menuBO = new MenuBO();
			BeanUtils.copyProperties(menu, menuBO);
			menuBO.setPermissionBO(permissionsService.findById(menuBO.getPermissionsId()));
			menuBOList.add(menuBO);
		}
		
		return menuBOList;
	}
	
	
	public List<MenuBO> findMenuAndChildByParentId(Integer parentId) throws Exception{
		
		List<SysMenu> menuList = menuCustomMapper.selectByParentId(parentId);
		
		List<MenuBO> menuBOList = new ArrayList<>();
		for(SysMenu menu : menuList) {
			MenuBO menuBO = new MenuBO();
			BeanUtils.copyProperties(menu, menuBO);
			menuBO.setPermissionBO(permissionsService.findById(menuBO.getPermissionsId()));
			menuBO.setChildMenuBOList(findMenuAndChildByParentId(menuBO.getId()));
			menuBOList.add(menuBO);
		}
		
		return menuBOList;
	}
	
	/**
	 * 
	 * @Title: validate 
	 * @Description: 校验菜单属性
	 * @param @param menu     
	 * @return void    返回类型 
	 * @throws 
	 *
	 */
	private void validate(SysMenu menu) {
		
		if(StringUtils.isBlank(menu.getTitle())) {
			throw new BusinessException("菜单标题不能为空");
		}
		
		if(StringUtils.isBlank(menu.getLevel())) {
			throw new BusinessException("菜单级别不能为空");
		}
		
		if(null == menu.getPermissionsId()) {
			throw new BusinessException("菜单所属权限不能为空");
		}
		
	}
	
}
