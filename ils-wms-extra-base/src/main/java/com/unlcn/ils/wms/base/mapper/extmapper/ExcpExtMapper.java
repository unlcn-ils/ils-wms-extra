package com.unlcn.ils.wms.base.mapper.extmapper;

import com.unlcn.ils.wms.base.dto.ExcpManageDTO;
import com.unlcn.ils.wms.base.dto.ExcpTypeDTO;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

/**
 * @Auther linbao
 * @Date 2017-11-23
 * 异常信息
 */
public interface ExcpExtMapper {

    /**
     * 异常信息分页查询
     *
     * @param singleParamMap
     * @param commonParamMap
     * @return
     */
    List<ExcpManageDTO> queryExcpManageDtoList(@Param("singleParamMap") Map<String, Object> singleParamMap,
                                               @Param("commonParamMap") Map<String, Object> commonParamMap);

    /**
     * 数据条数
     *
     * @param singleParamMap
     * @param commonParamMap
     * @return
     */
    Integer countExcpManageDto(@Param("singleParamMap") Map<String, Object> singleParamMap,
                               @Param("commonParamMap") Map<String, Object> commonParamMap);


    /**
     * 入库信息异常描述
     *
     * @param orderId
     * @return
     */
    List<ExcpTypeDTO> getInboundExcpTypeList(@Param("orderId")Long orderId, @Param("excpTimestamp")String excpTimestamp);

    /**
     * 出库异常信息描述
     *
     * @param orderId
     * @return
     */
    List<ExcpTypeDTO> getOutboundExcpTypeList(@Param("orderId") Long orderId, @Param("excpTimestamp") String excpTimestamp);

    /**
     * 入库缺件列表
     *
     * @param orderId
     * @param excpTimestamp
     * @return
     */
    List<String> getInboundMissList(@Param("orderId")Long orderId, @Param("excpTimestamp")String excpTimestamp);

    /**
     * 出库缺件列表
     *
     * @param orderId
     * @param excpTimestamp
     * @return
     */
    List<String> getOutboundMissList(@Param("orderId")Long orderId, @Param("excpTimestamp")String excpTimestamp);

    List<ExcpManageDTO> selectImportData(@Param("singleParamMap") Map<String, Object> singleParamMap,
                                         @Param("commonParamMap") Map<String, Object> commonParamMap);

}
