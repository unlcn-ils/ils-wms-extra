package com.unlcn.ils.wms.backend.service.webservice.bo;

import java.io.Serializable;

public class WmsResultInorOutDetailFromSapBO implements Serializable {
    private String vin;
    private String eMsg;

    public String getVin() {
        return vin;
    }

    public void setVin(String vin) {
        this.vin = vin;
    }

    public String geteMsg() {
        return eMsg;
    }

    public void seteMsg(String eMsg) {
        this.eMsg = eMsg;
    }
}
