package com.unlcn.ils.wms.base.dto;

import java.util.Date;

/**
 * @Auther linbao
 * @Date 2017-10-27
 */
public class WmsShipmentPlanRejectDTO {

    /**
     * id
     */
    private Long spId;

    /**
     * 组版单号
     */
    private String spGroupBoardNo;

    /**
     * 订单号
     */
    private String spOrderNo;

    /**
     * 状态
     */
    private String spSendBusinessFlag;

    /**
     * 是否有驳回
     */
    private String spIsReject;

    /**
     * 组版数量
     */
    private Long spGroupBoardQuantity;

    /**
     * 驳回数量
     */
    private Long spRejectNum;

    /**
     * 创建时间
     */
    private Date gmtCreate;

    /**
     * 发运类型
     */
    private String spDlvType;

    /**
     * 发货仓库
     */
    private String spDeliverWarehouseName;

    /**
     * 承运商
     */
    private String spCarrier;

    public Long getSpId() {
        return spId;
    }

    public void setSpId(Long spId) {
        this.spId = spId;
    }

    public String getSpGroupBoardNo() {
        return spGroupBoardNo;
    }

    public void setSpGroupBoardNo(String spGroupBoardNo) {
        this.spGroupBoardNo = spGroupBoardNo;
    }

    public String getSpOrderNo() {
        return spOrderNo;
    }

    public void setSpOrderNo(String spOrderNo) {
        this.spOrderNo = spOrderNo;
    }

    public String getSpSendBusinessFlag() {
        return spSendBusinessFlag;
    }

    public void setSpSendBusinessFlag(String spSendBusinessFlag) {
        this.spSendBusinessFlag = spSendBusinessFlag;
    }

    public String getSpIsReject() {
        return spIsReject;
    }

    public void setSpIsReject(String spIsReject) {
        this.spIsReject = spIsReject;
    }

    public Long getSpGroupBoardQuantity() {
        return spGroupBoardQuantity;
    }

    public void setSpGroupBoardQuantity(Long spGroupBoardQuantity) {
        this.spGroupBoardQuantity = spGroupBoardQuantity;
    }

    public Long getSpRejectNum() {
        return spRejectNum;
    }

    public void setSpRejectNum(Long spRejectNum) {
        this.spRejectNum = spRejectNum;
    }

    public Date getGmtCreate() {
        return gmtCreate;
    }

    public void setGmtCreate(Date gmtCreate) {
        this.gmtCreate = gmtCreate;
    }

    public String getSpDlvType() {
        return spDlvType;
    }

    public void setSpDlvType(String spDlvType) {
        this.spDlvType = spDlvType;
    }

    public String getSpDeliverWarehouseName() {
        return spDeliverWarehouseName;
    }

    public void setSpDeliverWarehouseName(String spDeliverWarehouseName) {
        this.spDeliverWarehouseName = spDeliverWarehouseName;
    }

    public String getSpCarrier() {
        return spCarrier;
    }

    public void setSpCarrier(String spCarrier) {
        this.spCarrier = spCarrier;
    }
}
