package com.unlcn.ils.wms.base.dto;

import java.io.Serializable;

public class WmsInboundOrderDetailDTO implements Serializable {
    private String oddId;
    private String oddVin;
    private String oddOdId;
    private String oddWhCode;

    public String getOddId() {
        return oddId;
    }

    public void setOddId(String oddId) {
        this.oddId = oddId;
    }

    public String getOddVin() {
        return oddVin;
    }

    public void setOddVin(String oddVin) {
        this.oddVin = oddVin;
    }

    public String getOddOdId() {
        return oddOdId;
    }

    public void setOddOdId(String oddOdId) {
        this.oddOdId = oddOdId;
    }

    public String getOddWhCode() {
        return oddWhCode;
    }

    public void setOddWhCode(String oddWhCode) {
        this.oddWhCode = oddWhCode;
    }
}
