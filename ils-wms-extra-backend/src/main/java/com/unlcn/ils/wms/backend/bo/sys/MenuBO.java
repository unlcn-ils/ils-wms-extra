package com.unlcn.ils.wms.backend.bo.sys;

import java.util.Date;
import java.util.List;

/**
 * 
 * @ClassName: MenuBO 
 * @Description: 菜单BO
 * @author laishijian
 * @date 2017年8月9日 上午9:54:35 
 *
 */
public class MenuBO {
	
	//id
	private Integer id;
	//菜单标题
	private String title;
	//父级菜单id
	private Integer parentId;
	//级别
	private String level;
	//权限id
	private Integer permissionsId;
	//状态
	private String enable;
	//创建人
	private String createPerson;
	//更新人
	private String updatePerson;
	//创建时间
	private Date gmtCreate;
	//修改时间
	private Date gmtModified;
	//权限
	private PermissionsBO permissionBO;
	//下级菜单BO
	private List<MenuBO> childMenuBOList;
	
	/**
	 * @return the id
	 */
	public Integer getId() {
		return id;
	}
	/**
	 * @param id the id to set
	 */
	public void setId(Integer id) {
		this.id = id;
	}
	/**
	 * @return the title
	 */
	public String getTitle() {
		return title;
	}
	/**
	 * @param title the title to set
	 */
	public void setTitle(String title) {
		this.title = title;
	}
	/**
	 * @return the parentId
	 */
	public Integer getParentId() {
		return parentId;
	}
	/**
	 * @param parentId the parentId to set
	 */
	public void setParentId(Integer parentId) {
		this.parentId = parentId;
	}
	/**
	 * @return the level
	 */
	public String getLevel() {
		return level;
	}
	/**
	 * @param level the level to set
	 */
	public void setLevel(String level) {
		this.level = level;
	}
	/**
	 * @return the permissionsId
	 */
	public Integer getPermissionsId() {
		return permissionsId;
	}
	/**
	 * @param permissionsId the permissionsId to set
	 */
	public void setPermissionsId(Integer permissionsId) {
		this.permissionsId = permissionsId;
	}
	/**
	 * @return the enable
	 */
	public String getEnable() {
		return enable;
	}
	/**
	 * @param enable the enable to set
	 */
	public void setEnable(String enable) {
		this.enable = enable;
	}
	/**
	 * @return the createPerson
	 */
	public String getCreatePerson() {
		return createPerson;
	}
	/**
	 * @param createPerson the createPerson to set
	 */
	public void setCreatePerson(String createPerson) {
		this.createPerson = createPerson;
	}
	/**
	 * @return the updatePerson
	 */
	public String getUpdatePerson() {
		return updatePerson;
	}
	/**
	 * @param updatePerson the updatePerson to set
	 */
	public void setUpdatePerson(String updatePerson) {
		this.updatePerson = updatePerson;
	}
	/**
	 * @return the gmtCreate
	 */
	public Date getGmtCreate() {
		return gmtCreate;
	}
	/**
	 * @param gmtCreate the gmtCreate to set
	 */
	public void setGmtCreate(Date gmtCreate) {
		this.gmtCreate = gmtCreate;
	}
	/**
	 * @return the gmtModified
	 */
	public Date getGmtModified() {
		return gmtModified;
	}
	/**
	 * @param gmtModified the gmtModified to set
	 */
	public void setGmtModified(Date gmtModified) {
		this.gmtModified = gmtModified;
	}
	/**
	 * @return the permissionBO
	 */
	public PermissionsBO getPermissionBO() {
		return permissionBO;
	}
	/**
	 * @param permissionBO the permissionBO to set
	 */
	public void setPermissionBO(PermissionsBO permissionBO) {
		this.permissionBO = permissionBO;
	}
	/**
	 * @return the childMenuBOList
	 */
	public List<MenuBO> getChildMenuBOList() {
		return childMenuBOList;
	}
	/**
	 * @param childMenuBOList the childMenuBOList to set
	 */
	public void setChildMenuBOList(List<MenuBO> childMenuBOList) {
		this.childMenuBOList = childMenuBOList;
	}
	
	/*
	 * Title: toString
	 * Description: toString
	 * @return
	 * @see java.lang.Object#toString()
	 *
	 */
	@Override
	public String toString() {
		return "MenuBO [id=" + id + ", title=" + title + ", parentId=" + parentId + ", level=" + level
				+ ", permissionsId=" + permissionsId + ", enable=" + enable + ", createPerson=" + createPerson
				+ ", updatePerson=" + updatePerson + ", gmtCreate=" + gmtCreate + ", gmtModified=" + gmtModified
				+ ", permissionBO=" + permissionBO + "]";
	}
	
}
