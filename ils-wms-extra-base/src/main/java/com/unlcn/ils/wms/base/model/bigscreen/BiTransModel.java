package com.unlcn.ils.wms.base.model.bigscreen;

import java.io.Serializable;
import java.util.Date;

public class BiTransModel implements Serializable {
    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column bi_trans_model.id
     *
     * @mbggenerated Wed Nov 08 19:47:33 CST 2017
     */
    private Long id;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column bi_trans_model.trans_model
     *
     * @mbggenerated Wed Nov 08 19:47:33 CST 2017
     */
    private String transModel;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column bi_trans_model.trans_model_count
     *
     * @mbggenerated Wed Nov 08 19:47:33 CST 2017
     */
    private Integer transModelCount;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column bi_trans_model.statistics_time
     *
     * @mbggenerated Wed Nov 08 19:47:33 CST 2017
     */
    private Date statisticsTime;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column bi_trans_model.version
     *
     * @mbggenerated Wed Nov 08 19:47:33 CST 2017
     */
    private String version;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column bi_trans_model.create_time
     *
     * @mbggenerated Wed Nov 08 19:47:33 CST 2017
     */
    private Date createTime;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database table bi_trans_model
     *
     * @mbggenerated Wed Nov 08 19:47:33 CST 2017
     */
    private static final long serialVersionUID = 1L;

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column bi_trans_model.id
     *
     * @return the value of bi_trans_model.id
     *
     * @mbggenerated Wed Nov 08 19:47:33 CST 2017
     */
    public Long getId() {
        return id;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column bi_trans_model.id
     *
     * @param id the value for bi_trans_model.id
     *
     * @mbggenerated Wed Nov 08 19:47:33 CST 2017
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column bi_trans_model.trans_model
     *
     * @return the value of bi_trans_model.trans_model
     *
     * @mbggenerated Wed Nov 08 19:47:33 CST 2017
     */
    public String getTransModel() {
        return transModel;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column bi_trans_model.trans_model
     *
     * @param transModel the value for bi_trans_model.trans_model
     *
     * @mbggenerated Wed Nov 08 19:47:33 CST 2017
     */
    public void setTransModel(String transModel) {
        this.transModel = transModel == null ? null : transModel.trim();
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column bi_trans_model.trans_model_count
     *
     * @return the value of bi_trans_model.trans_model_count
     *
     * @mbggenerated Wed Nov 08 19:47:33 CST 2017
     */
    public Integer getTransModelCount() {
        return transModelCount;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column bi_trans_model.trans_model_count
     *
     * @param transModelCount the value for bi_trans_model.trans_model_count
     *
     * @mbggenerated Wed Nov 08 19:47:33 CST 2017
     */
    public void setTransModelCount(Integer transModelCount) {
        this.transModelCount = transModelCount;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column bi_trans_model.statistics_time
     *
     * @return the value of bi_trans_model.statistics_time
     *
     * @mbggenerated Wed Nov 08 19:47:33 CST 2017
     */
    public Date getStatisticsTime() {
        return statisticsTime;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column bi_trans_model.statistics_time
     *
     * @param statisticsTime the value for bi_trans_model.statistics_time
     *
     * @mbggenerated Wed Nov 08 19:47:33 CST 2017
     */
    public void setStatisticsTime(Date statisticsTime) {
        this.statisticsTime = statisticsTime;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column bi_trans_model.version
     *
     * @return the value of bi_trans_model.version
     *
     * @mbggenerated Wed Nov 08 19:47:33 CST 2017
     */
    public String getVersion() {
        return version;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column bi_trans_model.version
     *
     * @param version the value for bi_trans_model.version
     *
     * @mbggenerated Wed Nov 08 19:47:33 CST 2017
     */
    public void setVersion(String version) {
        this.version = version == null ? null : version.trim();
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column bi_trans_model.create_time
     *
     * @return the value of bi_trans_model.create_time
     *
     * @mbggenerated Wed Nov 08 19:47:33 CST 2017
     */
    public Date getCreateTime() {
        return createTime;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column bi_trans_model.create_time
     *
     * @param createTime the value for bi_trans_model.create_time
     *
     * @mbggenerated Wed Nov 08 19:47:33 CST 2017
     */
    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table bi_trans_model
     *
     * @mbggenerated Wed Nov 08 19:47:33 CST 2017
     */
    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", id=").append(id);
        sb.append(", transModel=").append(transModel);
        sb.append(", transModelCount=").append(transModelCount);
        sb.append(", statisticsTime=").append(statisticsTime);
        sb.append(", version=").append(version);
        sb.append(", createTime=").append(createTime);
        sb.append("]");
        return sb.toString();
    }
}