package com.unlcn.ils.wms.backend.enums;

/**
 * @Auther linbao
 * @Date 2017-10-24
 */
public enum BarCodeTypeEnum {
    HANDOVER("10", "交接单"),
    REPIR("20", "维修单"),
    BORROW("30", "借车单");

    private final String code;

    private final String value;

    public String getCode() {
        return code;
    }

    public String getValue() {
        return value;
    }

    BarCodeTypeEnum(String code, String value) {
        this.code = code;
        this.value = value;
    }
}
