package com.unlcn.ils.wms.backend.service.inbound;

import com.unlcn.ils.wms.backend.bo.inboundBO.WmsRepairBO;
import com.unlcn.ils.wms.base.businessDTO.inbound.WmsRepairQueryDTO;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by DELL on 2017/8/7.
 */
public interface WmsRepairService {
    /**
     * 查询维修单
     * @param wmsRepairQueryDTO
     * @return
     */
    Map<String, Object> getListWmsRepairBO(WmsRepairQueryDTO wmsRepairQueryDTO) throws Exception;

    /**
     * 增加维修单
     * @param wmsRepairBO
     * @return
     */
    void addWmsRepair(WmsRepairBO wmsRepairBO);

    /**
     * 修改维修单
     * @param wmsRepairBO
     * @return
     */
    boolean updateWmsRepair(WmsRepairBO wmsRepairBO);

    /**
     * 提交维修
     * @param wmsRepairBOList
     * @param whCode
     * @param userId
     */
    void updateSubmitRepair(WmsRepairBO wmsRepairBOList, String whCode, String userId) throws Exception;

    /**
     * 维修之后不合格需要复制新增一条记录
     * @param wmsRepairBO
     */
    void submitAfterCheckCopyAndAdd(WmsRepairBO wmsRepairBO);

    /**
     * 维修之后检验
     * @param wmsRepairBO
     */
    void submitAfterCheck(WmsRepairBO wmsRepairBO);

    /**
     * 根据ID查询维修单
     * @param rpId
     * @return
     */
    WmsRepairBO getRepairById(String rpId);

    HashMap<String, Object> updatePrintRepair(WmsRepairBO bo, String whCode, String userId) throws Exception;
}
