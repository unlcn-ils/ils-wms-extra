package com.unlcn.ils.wms.backend.service.webservice.dto;

import java.io.Serializable;

public class ResultForDcsDealerCarbackDetailsDTO implements Serializable {
    private String eMsg;
    private String cbVin;

    public String geteMsg() {
        return eMsg;
    }

    public void seteMsg(String eMsg) {
        this.eMsg = eMsg;
    }

    public String getCbVin() {
        return cbVin;
    }

    public void setCbVin(String cbVin) {
        this.cbVin = cbVin;
    }
}
