package com.unlcn.ils.wms.base.dto;

import java.io.Serializable;


public class WmsInventoryFreezeDTO implements Serializable {
    private String invlocIds;//更新库存的状态为锁定,id用,分割
    private String whCode;

    public String getInvlocIds() {
        return invlocIds;
    }

    public void setInvlocIds(String invlocIds) {
        this.invlocIds = invlocIds;
    }

    public String getWhCode() {
        return whCode;
    }

    public void setWhCode(String whCode) {
        this.whCode = whCode;
    }
}
