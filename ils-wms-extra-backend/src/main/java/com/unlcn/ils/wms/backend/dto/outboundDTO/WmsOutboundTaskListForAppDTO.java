package com.unlcn.ils.wms.backend.dto.outboundDTO;

public class WmsOutboundTaskListForAppDTO extends AppWmsOutboundTaskListDTO {
    private String otLocCode;

    public String getOtLocCode() {
        return otLocCode;
    }

    public void setOtLocCode(String otLocCode) {
        this.otLocCode = otLocCode;
    }
}
