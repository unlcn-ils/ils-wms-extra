package com.unlcn.ils.wms.backend.bo.biBO;

import java.io.Serializable;

/**
 * Created by Nicky on 17/11/9.
 */
public class BiPickDataBO implements Serializable {

    /**
     * 统计日期
     */
    private String rpt_date;

    /**
     * 接单数
     */
    private String order_qty;

    /**
     *提车数
     */
    private String got_qty;

    /**
     *未提数
     */
    private String unget_qty;

    /**
     *及时提车数
     */
    private String intime_got_qty;

    /**
     * 提车及时率
     */
    private String intime_radio;

    public String getRpt_date() {
        return rpt_date;
    }

    public void setRpt_date(String rpt_date) {
        this.rpt_date = rpt_date;
    }

    public String getOrder_qty() {
        return order_qty;
    }

    public void setOrder_qty(String order_qty) {
        this.order_qty = order_qty;
    }

    public String getGot_qty() {
        return got_qty;
    }

    public void setGot_qty(String got_qty) {
        this.got_qty = got_qty;
    }

    public String getUnget_qty() {
        return unget_qty;
    }

    public void setUnget_qty(String unget_qty) {
        this.unget_qty = unget_qty;
    }

    public String getIntime_got_qty() {
        return intime_got_qty;
    }

    public void setIntime_got_qty(String intime_got_qty) {
        this.intime_got_qty = intime_got_qty;
    }

    public String getIntime_radio() {
        return intime_radio;
    }

    public void setIntime_radio(String intime_radio) {
        this.intime_radio = intime_radio;
    }

    @Override
    public String toString() {
        return "BiPickDataBO{" +
                "rpt_date='" + rpt_date + '\'' +
                ", order_qty='" + order_qty + '\'' +
                ", got_qty='" + got_qty + '\'' +
                ", unget_qty='" + unget_qty + '\'' +
                ", intime_got_qty='" + intime_got_qty + '\'' +
                ", intime_radio='" + intime_radio + '\'' +
                '}';
    }
}
