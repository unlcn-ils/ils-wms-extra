package com.unlcn.ils.wms.base.dto;

import com.unlcn.ils.wms.base.model.inbound.WmsInboundOrder;

public class WmsInboundOrderInsertProcedureDTO extends WmsInboundOrder {
    private String vin;

    public String getVin() {
        return vin;
    }

    public void setVin(String vin) {
        this.vin = vin;
    }
}
