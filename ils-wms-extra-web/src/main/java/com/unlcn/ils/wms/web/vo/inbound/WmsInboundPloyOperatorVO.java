package com.unlcn.ils.wms.web.vo.inbound;

import com.unlcn.ils.wms.backend.bo.baseDataBO.WmsCommonBO;

/**
 * Created by DELL on 2017/9/1.
 */
public class WmsInboundPloyOperatorVO extends WmsCommonBO {
    /**
     * 运算符主表id
     */
    private Long pyoId;

    /**
     * 主策略id
     */
    private String pyoPyId;

    /**
     * 主策略code
     */
    private String pyoPyCode;

    /**
     * 运算符code
     */
    private String pyoOperatorCode;

    /**
     * 运算符名称
     */
    private String pyoOperatorName;

    public Long getPyoId() {
        return pyoId;
    }

    public void setPyoId(Long pyoId) {
        this.pyoId = pyoId;
    }

    public String getPyoPyId() {
        return pyoPyId;
    }

    public void setPyoPyId(String pyoPyId) {
        this.pyoPyId = pyoPyId;
    }

    public String getPyoPyCode() {
        return pyoPyCode;
    }

    public void setPyoPyCode(String pyoPyCode) {
        this.pyoPyCode = pyoPyCode;
    }

    public String getPyoOperatorCode() {
        return pyoOperatorCode;
    }

    public void setPyoOperatorCode(String pyoOperatorCode) {
        this.pyoOperatorCode = pyoOperatorCode;
    }

    public String getPyoOperatorName() {
        return pyoOperatorName;
    }

    public void setPyoOperatorName(String pyoOperatorName) {
        this.pyoOperatorName = pyoOperatorName;
    }

    @Override
    public String toString() {
        return "WmsInboundPloyOperatorVO{" +
                "pyoId=" + pyoId +
                ", pyoPyId='" + pyoPyId + '\'' +
                ", pyoPyCode='" + pyoPyCode + '\'' +
                ", pyoOperatorCode='" + pyoOperatorCode + '\'' +
                ", pyoOperatorName='" + pyoOperatorName + '\'' +
                '}';
    }
}
