package com.unlcn.ils.wms.backend.service.borrow;

import com.unlcn.ils.wms.base.dto.BorrowDetailDTO;
import com.unlcn.ils.wms.base.dto.BorrowHistoryDTO;
import com.unlcn.ils.wms.base.dto.BorrowPrintDetailDTO;
import com.unlcn.ils.wms.base.model.stock.WmsBorrowCar;
import com.unlcn.ils.wms.base.model.stock.WmsBorrowCarDetail;

import java.util.List;
import java.util.Map;

/**
 * Created by lenovo on 2017/10/20.
 */
public interface WmsBorrowCarService {

    /**
     * 借用登记列表
     * @param paramsMap
     * @return
     */
    List<WmsBorrowCar> borrowLine(Map<String, Object> paramsMap);

    /**
     * 借用登记列表统计
     * @param paramsMap
     * @return
     */
    Integer borrowLineCount(Map<String, Object> paramsMap);

    /**
     * 借还记录列表统计
     * @param paramsMap
     * @return
     */
    Integer borrowHistoryLineCount(Map<String, Object> paramsMap);

    /**
     * 借还记录列表
     * @param paramsMap
     * @return
     */
    List<BorrowHistoryDTO> borrowHistoryLine(Map<String, Object> paramsMap);

    /**
     * 查找借车单明细
     * @param borrowId
     * @return
     */
    List<WmsBorrowCarDetail> findBorrowDetail(Long borrowId);

    /**
     * 查询编辑借车单的详细信息
     * @param bcId
     * @return
     */
    List<BorrowDetailDTO> queryModifyDetail(Long bcId);

    /**
     * 新增借车单
     * @param wmsBorrowCar
     * @param wmsBorrowCarDetailList
     */
    void add(WmsBorrowCar wmsBorrowCar, List<WmsBorrowCarDetail> wmsBorrowCarDetailList, Long userId);

    /**
     * 编辑借车单
     * @param wmsBorrowCar
     */
    void modify(WmsBorrowCar wmsBorrowCar, List<WmsBorrowCarDetail> wmsBorrowCarDetailList);

    /**
     * 删除借车单
     * @param ids
     */
    void delete(List<Long> ids);

    /**
     * 打印借车单信息
     * @param bcId
     * @return
     */
    BorrowPrintDetailDTO printInfo(Long bcId);

    /**
     * 根据车架号获取信息
     *
     * @param vin
     * @return
     * @throws Exception
     */
    WmsBorrowCarDetail getBorrowCarDetailByVin(String vin) throws Exception;

    /**
     * 根据借车单号获取借车详情
     *
     * @param borrowNo
     * @return
     * @throws Exception
     */
    List<WmsBorrowCarDetail> getBorrowCarDetaiListByBorrowNo(String borrowNo) throws Exception;

    /**
     * 根据id获取借车单
     *
     * @param id
     * @return
     * @throws Exception
     */
    WmsBorrowCar getWmsBorrowCarById(Long id) throws Exception;

    /**
     * 根据车架号更新状态
     *
     * @param vin
     * @throws Exception
     */
    void updateDetailStatusByVin(String vin, Integer status) throws Exception;
}
