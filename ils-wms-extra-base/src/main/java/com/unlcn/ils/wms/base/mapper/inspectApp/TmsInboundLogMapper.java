package com.unlcn.ils.wms.base.mapper.inspectApp;

import com.unlcn.ils.wms.base.model.inspectApp.TmsInboundLog;
import com.unlcn.ils.wms.base.model.inspectApp.TmsInboundLogExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface TmsInboundLogMapper {
    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table tms_inbound_log
     *
     * @mbggenerated Wed Sep 27 17:35:21 CST 2017
     */
    int countByExample(TmsInboundLogExample example);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table tms_inbound_log
     *
     * @mbggenerated Wed Sep 27 17:35:21 CST 2017
     */
    int deleteByExample(TmsInboundLogExample example);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table tms_inbound_log
     *
     * @mbggenerated Wed Sep 27 17:35:21 CST 2017
     */
    int deleteByPrimaryKey(Long id);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table tms_inbound_log
     *
     * @mbggenerated Wed Sep 27 17:35:21 CST 2017
     */
    int insert(TmsInboundLog record);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table tms_inbound_log
     *
     * @mbggenerated Wed Sep 27 17:35:21 CST 2017
     */
    int insertSelective(TmsInboundLog record);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table tms_inbound_log
     *
     * @mbggenerated Wed Sep 27 17:35:21 CST 2017
     */
    List<TmsInboundLog> selectByExample(TmsInboundLogExample example);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table tms_inbound_log
     *
     * @mbggenerated Wed Sep 27 17:35:21 CST 2017
     */
    TmsInboundLog selectByPrimaryKey(Long id);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table tms_inbound_log
     *
     * @mbggenerated Wed Sep 27 17:35:21 CST 2017
     */
    int updateByExampleSelective(@Param("record") TmsInboundLog record, @Param("example") TmsInboundLogExample example);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table tms_inbound_log
     *
     * @mbggenerated Wed Sep 27 17:35:21 CST 2017
     */
    int updateByExample(@Param("record") TmsInboundLog record, @Param("example") TmsInboundLogExample example);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table tms_inbound_log
     *
     * @mbggenerated Wed Sep 27 17:35:21 CST 2017
     */
    int updateByPrimaryKeySelective(TmsInboundLog record);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table tms_inbound_log
     *
     * @mbggenerated Wed Sep 27 17:35:21 CST 2017
     */
    int updateByPrimaryKey(TmsInboundLog record);
}