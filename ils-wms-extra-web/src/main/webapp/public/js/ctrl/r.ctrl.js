/**
 * Created by houjianhui on 2017/7/19.
 */
'use strict';
wmsCtrl.controller('reservoirCtrl',['$scope','reservoirService',function(scope,reservoirService){   //库区管理

    //仓库
    var shck = [
        {
            name:'我只是仓库',
            value:'1'
        },
        {
            name:'我只是仓库1',
            value:'2'
        },
        {
            name:'我只是仓库2',
            value:'3'
        }
    ]


    //新增库区 click
    scope.addReservoirClick = function(){
        scope.Tools.dialog({
            controller : 'reservoirCtrl.dialog',
            template : '../public/template/locationManagement/reservoirManagement/reservoir-form.html',
            closeByDocument : false,
            closeByEscape : false,
            data : {
                warehouse : shck,   //仓库
                dropDownDefault : {    //下拉框默认值
                    warehouse : shck[0] //仓库默认值
                }
            },
            width : 530,
            title : '新增库区',
            scope : scope
        })
    }

    //更新库区 click
    scope.modifyReservoirClick = function(){

        var indexArr = scope.Tools.getTableSelectItemIndex('.reservoirTable');  //获取table所选的下标

        if(indexArr.length == 0){
            scope.Tools.alert('请选择需要更新的数据');
            return;
        }

        if(indexArr.length > 1){
            scope.Tools.alert('只能选择一条数据进行更新');
            return;
        }

        reservoirService.getById(scope.reservoirList[indexArr[0]].zeId)
            .then(function(result){
                if(result.success){
                    scope.Tools.dialog({
                        controller : 'reservoirCtrl.dialog',
                        template : '../public/template/locationManagement/reservoirManagement/reservoir-form.html',
                        closeByDocument : false,
                        closeByEscape : false,
                        data : {
                            warehouse : shck,   //仓库
                            dropDownDefault : {    //下拉框默认值
                                warehouse : shck[0] //仓库默认值
                            },
                            reservoir : result.data
                        },
                        width : 530,
                        title : '更新库区',
                        scope : scope
                    })
                }else{
                    scope.Tools.alert(result.message);
                }
            })
    }

    //列表查询参数
    scope.listParam = function(){

        if(!scope.search){
            scope.search = {};
        }
        scope.search.pageSize = scope.pageSize;    //设置最大页
        return scope.search;
    }

    //列表
    scope.fristSelect = true;   //判断是否是第一次查询
    scope.list = function(params){
        //设置分页参数
        if(!params){
            params = {};
        }
        params.pageSize = params.pageSize ? params.pageSize : scope.constant.page.pageSize;
        params.pageNo = params.pageNo ? params.pageNo : scope.constant.page.pageNo

        reservoirService.list(params)
            .then(function(result){
                if(result.success){
                    scope.reservoirList = result.data.wmsZoneList;
                    if(scope.fristSelect){
                        scope.Tools.page(document.getElementById('pages'),{
                            pages : result.data.wmsZoneBO.totalPage,
                            curr : result.data.wmsZoneBO.pageNo,
                            count : result.data.wmsZoneBO.totalRecord
                        },function(obj, first){
                            scope.fristSelect = first;  //设置状态
                            //初始化多选框
                            scope.Tools.initCheckBox();
                            if(!first){ //判断不是第一次加载
                                var param = scope.listParam();
                                param.pageNo = obj.curr;    //当前页
                                param.pageSize = obj.limit; //最大页
                                scope.pageSize = obj.limit; //设置最大页
                                scope.pageNo = obj.curr; //设置当前页
                                scope.list(param);
                            }
                        })
                    }else{
                        //初始化多选框
                        scope.Tools.initCheckBox();
                    }

                }else{
                    scope.Tools.alert(result.message);
                }
            })
    }

    //列表
    scope.list();

    //查询 click
    scope.searchClick = function(){
        scope.fristSelect = true;   //设置为第一次查询
        scope.list(scope.listParam());
    }

    //刷新 click
    scope.refreshClick = function(){
        scope.searchClick();
    }

    //按下回车键查询
    scope.searchListKeyDown = function(e){
        var keycode = window.event ? e.keyCode:e.which;
        if(keycode == 13){  //回车键
            scope.fristSelect = true;   //设置为第一次查询
            scope.list(scope.listParam());
            e.preventDefault();
        }
    }
}])
.controller('reservoirCtrl.dialog',['$scope','reservoirService',function(scope,reservoirService){   //库区管理 dialog

    scope.saveOrUpdateReservoirClick = function(){  //新增库区

        //存在id 则为更新 不存在 新增
        if(scope.ngDialogData.reservoir.zeId){  //更新
            reservoirService.update(scope.ngDialogData.reservoir)
                .then(function(result){   //新增库区
                    if(result.success){
                        scope.Tools.alert('更新库区成功');
                        scope.list();
                        scope.closeThisDialog();
                    }else{
                        scope.Tools.alert(result.message);
                    }
                })
        }else{  //新增
            reservoirService.add(scope.ngDialogData.reservoir)
                .then(function(result){   //新增库区
                    if(result.success){
                        scope.Tools.alert('新增库区成功');
                        scope.$parent.fristSelect = true;   //设置为第一次查询
                        scope.list();
                        scope.closeThisDialog();
                    }else{
                        scope.Tools.alert(result.message);
                    }
                })
        }
    }

    //仓库修改 change
    scope.changeWarehouseChange = function(item){
        if(!scope.ngDialogData.reservoir){
            scope.ngDialogData.reservoir = {};
        }

        scope.ngDialogData.reservoir.zeWhName = item.name;
        scope.ngDialogData.reservoir.zeWhCode = item.name;
    }

    //默认选择仓库名称
    scope.changeWarehouseChange(scope.ngDialogData.dropDownDefault.warehouse);
}])
.controller('repairBillCtrl',['$scope','repairBillService',function(scope,repairBillService){  //维修单管理 controller

    /*状态数据*/
    scope.dropDownList = {
        repairType : scope.Tools.dropDown.repairTypeDropDown.concat(),   //维修类型
        repairStatus : scope.Tools.dropDown.repairStatusDropDown.concat(), //维修状态
        repairResult : scope.Tools.dropDown.repairResultDropDown.concat(),   //维修结果
        inspectionResults : scope.Tools.dropDown.inspectionResultsDropDown.concat() //检验结果
    }

    scope.dropDownList.repairType.unshift(scope.constant.defaultDropDownNullData);   //插入维修状态
    scope.dropDownList.repairStatus.unshift(scope.constant.defaultDropDownNullData);   //插入维修状态
    scope.dropDownList.repairResult.unshift(scope.constant.defaultDropDownNullData);   //插入维修结果
    scope.dropDownList.inspectionResults.unshift(scope.constant.defaultDropDownNullData);   //插入检验结果

    //设置默认值
    scope.dropDownDefault = {
        repairType : scope.dropDownList.repairType[0], //维修类型
        repairStatus : scope.dropDownList.repairStatus[0], //维修状态
        repairResult : scope.dropDownList.repairResult[0],   //维修结果
        inspectionResults : scope.dropDownList.inspectionResults[0] //检验结果
    }

    //列表查询参数
    scope.listParam = function(){

        if(!scope.search){
            scope.search = {};
        }
        scope.search.pageSize = scope.pageSize;    //设置最大页
        if(scope.otherForm){    //展开了更多
            scope.search.rpType = scope.dropDownDefault.repairType.value;   //维修类型
            scope.search.rpStatus = scope.dropDownDefault.repairStatus.value;    //维修状态
            scope.search.rpRepairResult = scope.dropDownDefault.repairResult.value;//维修结果
            scope.search.rpCheckResult = scope.dropDownDefault.inspectionResults.value;  //检验结果
            return scope.search;
        }else{
            return scope.search;
        }
    }

    //列表
    scope.fristSelect = true;   //判断是否是第一次查询
    scope.list = function(params){
        //设置分页参数
        if(!params){
            params = {};
        }
        params.pageSize = params.pageSize ? params.pageSize : scope.constant.page.pageSize;
        params.pageNo = params.pageNo ? params.pageNo : scope.constant.page.pageNo

        repairBillService.list(params)
            .then(function(result){
                if(result.success){

                    scope.repairBillList = result.data.wmsRepairList;
                    if(scope.fristSelect){
                        scope.Tools.page(document.getElementById('pages'),{
                            pages : result.data.wmsRepairQueryDTO.totalPage,
                            curr : result.data.wmsRepairQueryDTO.pageNo,
                            count : result.data.wmsRepairQueryDTO.totalRecord
                        },function(obj, first){
                            scope.fristSelect = first;  //设置状态
                            //初始化多选框
                            scope.Tools.initCheckBox();
                            if(!first){ //判断不是第一次加载
                                var param = scope.listParam();
                                param.pageNo = obj.curr;    //当前页
                                param.pageSize = obj.limit; //最大页
                                scope.pageSize = obj.limit; //设置最大页
                                scope.pageNo = obj.curr; //设置当前页
                                scope.list(param);
                            }
                        })
                    }else{
                        //初始化多选框
                        scope.Tools.initCheckBox();
                    }

                }else{
                    scope.Tools.alert(result.message);
                }
            })
    }

    //列表
    scope.list();

    //查询 click
    scope.searchClick = function(){
        scope.fristSelect = true;   //设置为第一次查询
        scope.list(scope.listParam());
    }

    //按下回车键查询
    scope.searchListKeyDown = function(e){
        var keycode = window.event ? e.keyCode:e.which;
        if(keycode == 13){  //回车键
            scope.fristSelect = true;   //设置为第一次查询
            scope.list(scope.listParam());
            e.preventDefault();
        }
    }

    //更多条件
    scope.moreText = scope.constant.moreText.spread;
    scope.moreClick = function(){
        scope.otherForm = !scope.otherForm;
        if(scope.otherForm){
            scope.moreText = scope.constant.moreText.packUp;
        }else{
            scope.moreText = scope.constant.moreText.spread;
        }
    }

    //刷新 click
    scope.refreshClick = function(){
        scope.searchClick();
    }

    //重置 click
    scope.resetClick = function(){
        scope.search = {};
    }

    //新增维修单
    scope.addRepairBillClick = function(){
        scope.Tools.dialog({
            controller:'repairBillCtrl.dialog',
            template : '../public/template/maintenanceManagement/repairBill/repair-bill-form.html',
            closeByDocument : false,
            closeByEscape : false,
            data:{
                repairType : scope.Tools.dropDown.repairTypeDropDown,    //维修类型
                inspectionResults : scope.Tools.dropDown.inspectionResultsDropDown,    //检验结果
                dropDownDefault : {
                    repairType : scope.Tools.dropDown.repairTypeDropDown[0], //维修类型默认值
                    inspectionResults : scope.Tools.dropDown.inspectionResultsDropDown[0] //检验结果默认值
                }
            },
            width : 770,
            title : '新增维修单',
            scope : scope
        })
    }

    //提交维修 click
    scope.submitRepairBillClick = function(){

        var indexArr = scope.Tools.getTableSelectItemIndex('.repairTable');  //获取table所选的下标

        if(indexArr.length == 0){
            scope.Tools.alert('请选择需要提交维修的数据');
            return;
        }
        //获取选中的数据
        var repairBillList = scope.Tools.getDataTOIndex(indexArr,scope.repairBillList);

        for(var i = 0;i < repairBillList.length;i++){
            if(repairBillList[i].rpStatus != scope.constant.repairStatus.unprocessed){
                scope.Tools.alert('只能选择未处理的数据');
                return;
            }
        }

        scope.Tools.dialogConfirm('确定要提交维修选中的数据吗？')
            .then(function(res){
                if(res){
                    repairBillService.submitRepair(repairBillList)
                        .then(function(result){
                            if(result.success){
                                scope.Tools.alert('提交维修成功');
                                scope.list(scope.listParam());
                            }else{
                                scope.Tools.alert(result.message);
                            }
                        })
                }
            })
    }

    //维修后检查 click
    scope.repairCheckClick = function(){

        var indexArr = scope.Tools.getTableSelectItemIndex('.repairTable');  //获取table所选的下标

        if(indexArr.length == 0){
            scope.Tools.alert('请选择需要维修后检验的数据');
            return;
        }

        if(indexArr.length > 1){
            scope.Tools.alert('只能选择一条维修后检验的数据');
            return;
        }

        //获取选中的数据
        var repairBillList = scope.Tools.getDataTOIndex(indexArr,scope.repairBillList);

        if(repairBillList[0].rpStatus != scope.constant.repairStatus.maintenance){
            scope.Tools.alert('只能选择维修中的数据');
            return;
        }

        repairBillService.getById(repairBillList[0].rpId)
            .then(function(result){
                if(result.success){

                    //维修结果默认值
                    var repairResult = null;
                    if(result.data.rpRepairResult){
                        repairResult = scope.Tools.getRepairResultDropDownByKey(result.data.rpRepairResult);
                    }else{
                        repairResult = scope.Tools.dropDown.repairResultDropDown[0]
                    }

                    scope.Tools.dialog({
                        controller:'repairBillCtrl.dialog',
                        template : '../public/template/maintenanceManagement/repairBill/repair-check-form.html',
                        closeByDocument : false,
                        closeByEscape : false,
                        data:{
                            repairType : scope.Tools.dropDown.repairTypeDropDown,    //维修类型
                            repairResult : scope.Tools.dropDown.repairResultDropDown,    //维修结果
                            inspectionResults : scope.Tools.dropDown.inspectionResultsDropDown,    //检验结果
                            dropDownDefault : {
                                repairType : scope.Tools.getRepairTypeDropDownByKey(result.data.rpType), //维修类型默认值
                                repairResult : repairResult, //维修结果默认值
                                inspectionResults : scope.Tools.getInspectionResultsDropDownByKey(result.data.rpCheckResult) //检验结果默认值
                            },
                            repair : result.data,
                            repairChange : {  //维修方 修改change的属性
                                rpRepairParty : result.data.rpRepairPerson
                            }
                        },
                        width : 770,
                        title : '维修后检查',
                        scope : scope
                    })
                }else{
                    scope.Tools.alert(result.message);
                }
            })
    }
}])
.controller('repairBillCtrl.dialog',['$scope','repairBillService',function(scope,repairBillService){  //维修单 controller  dialog

    //新增修改 维修单 click
    scope.saveOrUpdaterepairClick = function(){

        //判断是否有id 如果没有id则为新增、有则为修改
        if(scope.ngDialogData.repair.rpId){   //修改

            if(scope.ngDialogData.dropDownDefault.repairResult.value != scope.constant.repairResult.qualified){   //为不合格
                //维修类型
                if(scope.ngDialogData.dropDownDefault.repairType){
                    scope.ngDialogData.repair.rpType = scope.ngDialogData.dropDownDefault.repairType.value;
                }

                //检验结果
                if(scope.ngDialogData.dropDownDefault.inspectionResults){
                    scope.ngDialogData.repair.rpCheckResult = scope.ngDialogData.dropDownDefault.inspectionResults.value;
                }
                //维修方赋值
                scope.ngDialogData.repair.rpRepairParty = scope.ngDialogData.repairChange.rpRepairParty;

                //维修结果
                scope.ngDialogData.repair.rpRepairResult = scope.ngDialogData.dropDownDefault.repairResult.value;
            }else{  //合格
                //维修结果
                scope.ngDialogData.repair.rpRepairResult = scope.ngDialogData.dropDownDefault.repairResult.value;
            }

            repairBillService.submitAfterCheckAdd(scope.ngDialogData.repair)
                .then(function(result){
                    if(result.success){
                        scope.Tools.alert('更新维修单成功');
                        scope.list(scope.listParam());
                        scope.closeThisDialog();
                    }else{
                        scope.Tools.alert(result.message);
                    }
                })
        }else{  //新增

            //维修类型
            if(scope.ngDialogData.dropDownDefault.repairType){
                scope.ngDialogData.repair.rpType = scope.ngDialogData.dropDownDefault.repairType.value;
            }

            //检验结果
            if(scope.ngDialogData.dropDownDefault.inspectionResults){
                scope.ngDialogData.repair.rpCheckResult = scope.ngDialogData.dropDownDefault.inspectionResults.value;
            }

            repairBillService.addWmsRepair(scope.ngDialogData.repair)
                .then(function(result){
                    if(result.success){
                        scope.Tools.alert('新增维修单成功');
                        scope.$parent.fristSelect = true;   //设置为第一次查询
                        scope.list(scope.listParam());
                        scope.closeThisDialog();
                    }else{
                        scope.Tools.alert(result.message);
                    }
                })
        }
    }

    //维修结果 change
    scope.repairResultChange = function(item){
        //选择合格的时候把数据还原
        if(item.value == scope.constant.repairResult.qualified){
            //维修类型
            scope.ngDialogData.dropDownDefault.repairType = scope.Tools.getRepairTypeDropDownByKey(scope.ngDialogData.repair.rpType);

            //检验结果
            scope.ngDialogData.dropDownDefault.inspectionResults = scope.Tools.getInspectionResultsDropDownByKey(scope.ngDialogData.repair.rpCheckResult);
        }
    }

    //复制并新增 click
    scope.copyAddClick = function(){

        //维修类型
        if(scope.ngDialogData.dropDownDefault.repairType){
            scope.ngDialogData.repair.rpType = scope.ngDialogData.dropDownDefault.repairType.value;
        }

        //检验结果
        if(scope.ngDialogData.dropDownDefault.inspectionResults){
            scope.ngDialogData.repair.rpCheckResult = scope.ngDialogData.dropDownDefault.inspectionResults.value;
        }
        //维修方赋值
        scope.ngDialogData.repair.rpRepairParty = scope.ngDialogData.repairChange.rpRepairParty;
        //维修结果
        scope.ngDialogData.repair.rpRepairResult = scope.ngDialogData.dropDownDefault.repairResult.value;

        repairBillService.submitAfterCheckCopyAndAdd(scope.ngDialogData.repair)
            .then(function(result){
                if(result.success){
                    scope.Tools.alert('复制并新增维修单成功');
                    scope.list(scope.listParam());
                }else{
                    scope.Tools.alert(result.message);
                }
            })
    }
}])