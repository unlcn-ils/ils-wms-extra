package com.unlcn.ils.wms.backend.enums;

/**
 * @Auther linbao
 * @Date 2017-10-24
 * 入库类型
 */
public enum InboundOrderBusinessTypeEnum {

    Z1("Z1", "PDI合格入库"),
    Z2("Z2", "调拨入库"),
    Z3("Z3", "维修出库"),
    Z4("Z4", "维修后入库"),
    Z5("Z5", "其他入库"),
    Z6("Z6", "其他出库"),
    Z7("Z7", "借用出库"),
    Z8("Z8", "借用入库"),
    Z9("Z9", "退货入库");

    private final String code;

    private final String value;

    public String getCode() {
        return code;
    }

    public String getValue() {
        return value;
    }

    InboundOrderBusinessTypeEnum(String code, String value) {
        this.code = code;
        this.value = value;
    }
}
