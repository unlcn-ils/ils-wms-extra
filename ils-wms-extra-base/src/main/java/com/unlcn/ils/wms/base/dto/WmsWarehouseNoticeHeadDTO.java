package com.unlcn.ils.wms.base.dto;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

public class WmsWarehouseNoticeHeadDTO implements Serializable {
    private Long wnhId;
    private String wnhCustomerId;
    private String wnhCustomerCode;
    private String wnhCustomerName;
    private String wnhCustomerOrderNo;
    private String wnhCustomerDispatchNo;
    private String wnhCustomerWaybillNo;
    private String wnhUnlcnOrderNo;
    private String wnhUnlcnDisptchNo;
    private String wnhUnlcnWaybillNo;
    private String wnhOriginCode;
    private String wnhOriginName;
    private String wnhDestinationCode;
    private String wnhDestinationName;
    private String wnhSubcontractorId;
    private String wnhSubcontractorCode;
    private String wnhSubcontractorName;
    private String wnhSubcontractorPlateNo;
    private Integer wnhInboundNum;
    private Date wnhCreate;
    private Date wnhUpdate;
    private Integer wnhCreateUserId;
    private String wnhCreateUserName;
    private Integer wnhUpdateUserId;
    private String wnhUpdateUserName;
    private String wnhBatchNo;
    private Long wnhDataTimestamp;
    private String wnhSysSource;
    private String wnhBusinessStatus;
    private String wnhDataStatus;
    private String wnhWarehouseCode;
    private List<WmsWarehouseNoticeDetailDTOForSchedule> detailList;

    public Long getWnhId() {
        return wnhId;
    }

    public void setWnhId(Long wnhId) {
        this.wnhId = wnhId;
    }

    public String getWnhCustomerId() {
        return wnhCustomerId;
    }

    public void setWnhCustomerId(String wnhCustomerId) {
        this.wnhCustomerId = wnhCustomerId;
    }

    public String getWnhCustomerCode() {
        return wnhCustomerCode;
    }

    public void setWnhCustomerCode(String wnhCustomerCode) {
        this.wnhCustomerCode = wnhCustomerCode;
    }

    public String getWnhCustomerName() {
        return wnhCustomerName;
    }

    public void setWnhCustomerName(String wnhCustomerName) {
        this.wnhCustomerName = wnhCustomerName;
    }

    public String getWnhCustomerOrderNo() {
        return wnhCustomerOrderNo;
    }

    public void setWnhCustomerOrderNo(String wnhCustomerOrderNo) {
        this.wnhCustomerOrderNo = wnhCustomerOrderNo;
    }

    public String getWnhCustomerDispatchNo() {
        return wnhCustomerDispatchNo;
    }

    public void setWnhCustomerDispatchNo(String wnhCustomerDispatchNo) {
        this.wnhCustomerDispatchNo = wnhCustomerDispatchNo;
    }

    public String getWnhCustomerWaybillNo() {
        return wnhCustomerWaybillNo;
    }

    public void setWnhCustomerWaybillNo(String wnhCustomerWaybillNo) {
        this.wnhCustomerWaybillNo = wnhCustomerWaybillNo;
    }

    public String getWnhUnlcnOrderNo() {
        return wnhUnlcnOrderNo;
    }

    public void setWnhUnlcnOrderNo(String wnhUnlcnOrderNo) {
        this.wnhUnlcnOrderNo = wnhUnlcnOrderNo;
    }

    public String getWnhUnlcnDisptchNo() {
        return wnhUnlcnDisptchNo;
    }

    public void setWnhUnlcnDisptchNo(String wnhUnlcnDisptchNo) {
        this.wnhUnlcnDisptchNo = wnhUnlcnDisptchNo;
    }

    public String getWnhUnlcnWaybillNo() {
        return wnhUnlcnWaybillNo;
    }

    public void setWnhUnlcnWaybillNo(String wnhUnlcnWaybillNo) {
        this.wnhUnlcnWaybillNo = wnhUnlcnWaybillNo;
    }

    public String getWnhOriginCode() {
        return wnhOriginCode;
    }

    public void setWnhOriginCode(String wnhOriginCode) {
        this.wnhOriginCode = wnhOriginCode;
    }

    public String getWnhOriginName() {
        return wnhOriginName;
    }

    public void setWnhOriginName(String wnhOriginName) {
        this.wnhOriginName = wnhOriginName;
    }

    public String getWnhDestinationCode() {
        return wnhDestinationCode;
    }

    public void setWnhDestinationCode(String wnhDestinationCode) {
        this.wnhDestinationCode = wnhDestinationCode;
    }

    public String getWnhDestinationName() {
        return wnhDestinationName;
    }

    public void setWnhDestinationName(String wnhDestinationName) {
        this.wnhDestinationName = wnhDestinationName;
    }

    public String getWnhSubcontractorId() {
        return wnhSubcontractorId;
    }

    public void setWnhSubcontractorId(String wnhSubcontractorId) {
        this.wnhSubcontractorId = wnhSubcontractorId;
    }

    public String getWnhSubcontractorCode() {
        return wnhSubcontractorCode;
    }

    public void setWnhSubcontractorCode(String wnhSubcontractorCode) {
        this.wnhSubcontractorCode = wnhSubcontractorCode;
    }

    public String getWnhSubcontractorName() {
        return wnhSubcontractorName;
    }

    public void setWnhSubcontractorName(String wnhSubcontractorName) {
        this.wnhSubcontractorName = wnhSubcontractorName;
    }

    public String getWnhSubcontractorPlateNo() {
        return wnhSubcontractorPlateNo;
    }

    public void setWnhSubcontractorPlateNo(String wnhSubcontractorPlateNo) {
        this.wnhSubcontractorPlateNo = wnhSubcontractorPlateNo;
    }

    public Integer getWnhInboundNum() {
        return wnhInboundNum;
    }

    public void setWnhInboundNum(Integer wnhInboundNum) {
        this.wnhInboundNum = wnhInboundNum;
    }

    public Date getWnhCreate() {
        return wnhCreate;
    }

    public void setWnhCreate(Date wnhCreate) {
        this.wnhCreate = wnhCreate;
    }

    public Date getWnhUpdate() {
        return wnhUpdate;
    }

    public void setWnhUpdate(Date wnhUpdate) {
        this.wnhUpdate = wnhUpdate;
    }

    public Integer getWnhCreateUserId() {
        return wnhCreateUserId;
    }

    public void setWnhCreateUserId(Integer wnhCreateUserId) {
        this.wnhCreateUserId = wnhCreateUserId;
    }

    public String getWnhCreateUserName() {
        return wnhCreateUserName;
    }

    public void setWnhCreateUserName(String wnhCreateUserName) {
        this.wnhCreateUserName = wnhCreateUserName;
    }

    public Integer getWnhUpdateUserId() {
        return wnhUpdateUserId;
    }

    public void setWnhUpdateUserId(Integer wnhUpdateUserId) {
        this.wnhUpdateUserId = wnhUpdateUserId;
    }

    public String getWnhUpdateUserName() {
        return wnhUpdateUserName;
    }

    public void setWnhUpdateUserName(String wnhUpdateUserName) {
        this.wnhUpdateUserName = wnhUpdateUserName;
    }

    public String getWnhBatchNo() {
        return wnhBatchNo;
    }

    public void setWnhBatchNo(String wnhBatchNo) {
        this.wnhBatchNo = wnhBatchNo;
    }

    public Long getWnhDataTimestamp() {
        return wnhDataTimestamp;
    }

    public void setWnhDataTimestamp(Long wnhDataTimestamp) {
        this.wnhDataTimestamp = wnhDataTimestamp;
    }

    public String getWnhSysSource() {
        return wnhSysSource;
    }

    public void setWnhSysSource(String wnhSysSource) {
        this.wnhSysSource = wnhSysSource;
    }

    public String getWnhBusinessStatus() {
        return wnhBusinessStatus;
    }

    public void setWnhBusinessStatus(String wnhBusinessStatus) {
        this.wnhBusinessStatus = wnhBusinessStatus;
    }

    public String getWnhDataStatus() {
        return wnhDataStatus;
    }

    public void setWnhDataStatus(String wnhDataStatus) {
        this.wnhDataStatus = wnhDataStatus;
    }

    public String getWnhWarehouseCode() {
        return wnhWarehouseCode;
    }

    public void setWnhWarehouseCode(String wnhWarehouseCode) {
        this.wnhWarehouseCode = wnhWarehouseCode;
    }

    public List<WmsWarehouseNoticeDetailDTOForSchedule> getDetailList() {
        return detailList;
    }

    public void setDetailList(List<WmsWarehouseNoticeDetailDTOForSchedule> detailList) {
        this.detailList = detailList;
    }

    @Override
    public String toString() {
        return "WmsWarehouseNoticeHeadDTO{" +
                "wnhId=" + wnhId +
                ", wnhCustomerId='" + wnhCustomerId + '\'' +
                ", wnhCustomerCode='" + wnhCustomerCode + '\'' +
                ", wnhCustomerName='" + wnhCustomerName + '\'' +
                ", wnhCustomerOrderNo='" + wnhCustomerOrderNo + '\'' +
                ", wnhCustomerDispatchNo='" + wnhCustomerDispatchNo + '\'' +
                ", wnhCustomerWaybillNo='" + wnhCustomerWaybillNo + '\'' +
                ", wnhUnlcnOrderNo='" + wnhUnlcnOrderNo + '\'' +
                ", wnhUnlcnDisptchNo='" + wnhUnlcnDisptchNo + '\'' +
                ", wnhUnlcnWaybillNo='" + wnhUnlcnWaybillNo + '\'' +
                ", wnhOriginCode='" + wnhOriginCode + '\'' +
                ", wnhOriginName='" + wnhOriginName + '\'' +
                ", wnhDestinationCode='" + wnhDestinationCode + '\'' +
                ", wnhDestinationName='" + wnhDestinationName + '\'' +
                ", wnhSubcontractorId='" + wnhSubcontractorId + '\'' +
                ", wnhSubcontractorCode='" + wnhSubcontractorCode + '\'' +
                ", wnhSubcontractorName='" + wnhSubcontractorName + '\'' +
                ", wnhSubcontractorPlateNo='" + wnhSubcontractorPlateNo + '\'' +
                ", wnhInboundNum=" + wnhInboundNum +
                ", wnhCreate=" + wnhCreate +
                ", wnhUpdate=" + wnhUpdate +
                ", wnhCreateUserId=" + wnhCreateUserId +
                ", wnhCreateUserName='" + wnhCreateUserName + '\'' +
                ", wnhUpdateUserId=" + wnhUpdateUserId +
                ", wnhUpdateUserName='" + wnhUpdateUserName + '\'' +
                ", wnhBatchNo='" + wnhBatchNo + '\'' +
                ", wnhDataTimestamp=" + wnhDataTimestamp +
                ", wnhSysSource='" + wnhSysSource + '\'' +
                ", wnhBusinessStatus='" + wnhBusinessStatus + '\'' +
                ", wnhDataStatus='" + wnhDataStatus + '\'' +
                ", wnhWarehouseCode='" + wnhWarehouseCode + '\'' +
                ", detailList=" + detailList +
                '}';
    }
}
