package com.unlcn.ils.wms.backend.bo;

/**
 * Created by houjianhui on 2017/5/11.
 */
public class WarehouseUserBO {

    private Long id;

    private Long userId;

    private Long warehouseId;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public Long getWarehouseId() {
        return warehouseId;
    }

    public void setWarehouseId(Long warehouseId) {
        this.warehouseId = warehouseId;
    }

    @Override
    public String toString() {
        return "WarehouseUserBO{" +
                "id=" + id +
                ", userId=" + userId +
                ", warehouseId=" + warehouseId +
                '}';
    }
}
