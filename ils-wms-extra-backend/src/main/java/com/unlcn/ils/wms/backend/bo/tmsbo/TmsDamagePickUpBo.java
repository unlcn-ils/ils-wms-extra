package com.unlcn.ils.wms.backend.bo.tmsbo;

import java.io.Serializable;

/**
 * 封装带伤提车参数
 */
public class TmsDamagePickUpBo implements Serializable {
    private Long inspectId;        //验车信息ID
    private String vin;         //车架号
    private String picKey;         //图片KEY

    public Long getInspectId() {
        return inspectId;
    }

    public void setInspectId(Long inspectId) {
        this.inspectId = inspectId;
    }

    public String getVin() {
        return vin;
    }

    public void setVin(String vin) {
        this.vin = vin;
    }

    public String getPicKey() {
        return picKey;
    }

    public void setPicKey(String picKey) {
        this.picKey = picKey;
    }
}
