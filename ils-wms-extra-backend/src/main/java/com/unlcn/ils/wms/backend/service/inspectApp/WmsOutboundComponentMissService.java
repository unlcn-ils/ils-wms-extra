package com.unlcn.ils.wms.backend.service.inspectApp;

import com.unlcn.ils.wms.backend.bo.inspectAppBO.WmsOutboundDamageDispatchBO;
import com.unlcn.ils.wms.base.dto.TmsInspectComponentMissDTO;
import com.unlcn.ils.wms.base.dto.WmsInspectForAppDTO;

import java.util.List;

public interface WmsOutboundComponentMissService {


    List<TmsInspectComponentMissDTO> getComponentMissList(String vin, String whCode) throws Exception;

    void saveComponentMiss(WmsInspectForAppDTO dto) throws Exception;

    void updateDispatch(Long inspectId, String userId) throws Exception;

    void updateDamageDispatch(WmsOutboundDamageDispatchBO bo) throws Exception;

    String getMissComponentNames(String vin, String whCode) throws Exception;

    String getMissComponentNamesNew(Long id, String whcode) throws Exception;

    void updateInspectStatusById(Long inspectId, String userId, String whCode) throws Exception;
}
