package com.unlcn.ils.wms.api.webservice.server;

import com.google.common.collect.Lists;
import com.google.gson.Gson;
import com.unlcn.ils.wms.backend.service.outbound.WmsDealerCarBackService;
import com.unlcn.ils.wms.backend.service.outbound.WmsShipmentPlanService;
import com.unlcn.ils.wms.backend.service.webservice.dto.*;
import com.unlcn.ils.wms.backend.service.webservice.vo.WmsDealerCarBackVO;
import com.unlcn.ils.wms.backend.service.webservice.vo.WmsShipmentPlanVO;
import org.apache.commons.collections4.CollectionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.context.support.SpringBeanAutowiringSupport;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

/**
 * 中联作为客户端  出入库、交接单、退车。 中联---》君马
 * 中联作为服务端  发运计划。君马---》中联
 */
public class WmsBusinessServiceImpl {


    private Logger logger = LoggerFactory.getLogger(WmsBusinessServiceImpl.class);

    @Autowired
    private WmsShipmentPlanService wmsShipmentPlanService;

    @Autowired
    private WmsDealerCarBackService wmsDealerCarBackService;

    /**
     * 发运计划服务端
     */
    public String insertWmsShipmentPlan(WmsShipmentPlanVO[] wmsShipmentPlanVOArray, ResultSapHeaderDTO resultSapHeaderDTO) {
        logger.info("WmsBusinessServiceImpl.insertWmsShipmentPlan param{},{}", wmsShipmentPlanVOArray, resultSapHeaderDTO);
        //加入spring的上下文
        SpringBeanAutowiringSupport.processInjectionBasedOnCurrentContext(this);
        ResultForDcsInorOutServerDTO resultInfo = new ResultForDcsInorOutServerDTO();
        ResultForDcsInorOutServerBodyDTO bodyDTO = new ResultForDcsInorOutServerBodyDTO();
        ArrayList<ResultForDcsInorOutDetailsDTO> detailList = Lists.newArrayList();
        ResultForDcsInorOutDetailsDTO detailsDTO = new ResultForDcsInorOutDetailsDTO();
        ResultSapHeaderDTO headerDTO = new ResultSapHeaderDTO();
        if (Objects.equals(wmsShipmentPlanVOArray, null)) {
            //明细
            detailsDTO.setSpOrderNo(null);
            detailsDTO.setSpOrderNo(null);
            detailsDTO.setSpGroupBoardNo(null);
            //体
            detailsDTO.seteMsg("传入数据为空");
            detailList.add(detailsDTO);
            bodyDTO.setReturnCode("E");
            ResultForDcsInorOutDetailsDTO[] detailsDTOS = new ResultForDcsInorOutDetailsDTO[detailList.size()];
            bodyDTO.setMsgDetails(detailList.toArray(detailsDTOS));
            bodyDTO.setReturnMsg("传入数据为空");
            //头
            BeanUtils.copyProperties(resultSapHeaderDTO, headerDTO);
            resultInfo.setBodyDTO(bodyDTO);
            resultInfo.setHeaderDTO(headerDTO);
            return getResultForDcsInorOutServerDTO(resultInfo);
        }
        String s_body = new Gson().toJson(wmsShipmentPlanVOArray);
        String s_head = new Gson().toJson(resultSapHeaderDTO);
        try {
            List<ResultForDcsInorOutDetailsDTO> results = wmsShipmentPlanService.saveShipmentList(wmsShipmentPlanVOArray, s_body, s_head);
            if (CollectionUtils.isEmpty(results)) {
                //处理成功
                //明细
                detailsDTO.setSpOrderNo(null);
                detailsDTO.setSpOrderNo(null);
                detailsDTO.setSpGroupBoardNo(null);
                detailsDTO.seteMsg("处理成功");
                //体
                detailList.add(detailsDTO);
                bodyDTO.setReturnCode("S");
                ResultForDcsInorOutDetailsDTO[] detailsDTOS = new ResultForDcsInorOutDetailsDTO[detailList.size()];
                bodyDTO.setMsgDetails(detailList.toArray(detailsDTOS));
                bodyDTO.setReturnMsg("处理成功");
                //头
                BeanUtils.copyProperties(resultSapHeaderDTO, headerDTO);
                resultInfo.setBodyDTO(bodyDTO);
                resultInfo.setHeaderDTO(headerDTO);
                return getResultForDcsInorOutServerDTO(resultInfo);
            } else {
                //未通过
                //体已经设置过明细
                bodyDTO.setReturnCode("E");
                ResultForDcsInorOutDetailsDTO[] detailsDTOS = new ResultForDcsInorOutDetailsDTO[results.size()];
                bodyDTO.setMsgDetails(results.toArray(detailsDTOS));
                bodyDTO.setReturnMsg("处理失败");
                //头
                BeanUtils.copyProperties(resultSapHeaderDTO, headerDTO);
                resultInfo.setBodyDTO(bodyDTO);
                resultInfo.setHeaderDTO(headerDTO);
                return getResultForDcsInorOutServerDTO(resultInfo);
            }
        } catch (Exception e) {
            logger.error("WmsBusinessServiceImpl.insertWmsShipmentPlan param{},{}", e);
            //明细
            detailsDTO.setSpOrderNo(null);
            detailsDTO.setSpOrderNo(null);
            detailsDTO.setSpGroupBoardNo(null);
            detailsDTO.seteMsg("系统内部异常!");
            //体
            detailList.add(detailsDTO);
            bodyDTO.setReturnCode("E");
            ResultForDcsInorOutDetailsDTO[] detailsDTOS = new ResultForDcsInorOutDetailsDTO[detailList.size()];
            bodyDTO.setMsgDetails(detailList.toArray(detailsDTOS));
            bodyDTO.setReturnMsg("系统内部异常");
            //头
            BeanUtils.copyProperties(resultSapHeaderDTO, headerDTO);
            resultInfo.setBodyDTO(bodyDTO);
            resultInfo.setHeaderDTO(headerDTO);
            return getResultForDcsInorOutServerDTO(resultInfo);
        }
    }

    /**
     * 使用jdk转换对象为xml 防止soapui调用服务端报错
     */
    private String getResultForDcsInorOutServerDTO(Object resultInfo) {
        //使用jdk自带的将对象转为xml对象
        StringWriter stringWriter = new StringWriter();
        try {
            JAXBContext context = JAXBContext.newInstance(resultInfo.getClass());
            Marshaller marshaller = context.createMarshaller();
            marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
            marshaller.marshal(resultInfo, stringWriter);
        } catch (JAXBException e) {
            logger.error("WmsBusinessServiceImpl.getResultForDcsInorOutServerDTO param{},{}", e);
            e.printStackTrace();
        }
        return stringWriter.toString();
    }


    /**
     * 经销商退车申请服务端
     */
    public String insertWmsDealerCarBack(WmsDealerCarBackVO[] wmsDealerCarBackVOArray, ResultSapHeaderDTO resultSapHeaderDTO) {
        logger.info("WmsBusinessServiceImpl.insertWmsDealerCarBack param{},{}", wmsDealerCarBackVOArray, resultSapHeaderDTO);
        //加入spring的上下文
        SpringBeanAutowiringSupport.processInjectionBasedOnCurrentContext(this);
        ResultForDcsCarBackServerDTO resultInfo = new ResultForDcsCarBackServerDTO();
        ResultForDcsCarBackServerBodyDTO bodyDTO = new ResultForDcsCarBackServerBodyDTO();
        ResultSapHeaderDTO headerDTO = new ResultSapHeaderDTO();
        List<ResultForDcsDealerCarbackDetailsDTO> detailsDTOS = Lists.newArrayList();
        ResultForDcsDealerCarbackDetailsDTO detailsDTO = new ResultForDcsDealerCarbackDetailsDTO();
        if (Objects.equals(wmsDealerCarBackVOArray, null)) {
            //明细
            detailsDTO.seteMsg("发运计划数据为空");
            detailsDTO.setCbVin(null);
            //体
            bodyDTO.setReturnCode("E");
            detailsDTOS.add(detailsDTO);
            ResultForDcsDealerCarbackDetailsDTO[] detail = new ResultForDcsDealerCarbackDetailsDTO[detailsDTOS.size()];
            bodyDTO.setMsgDetails(detail);
            bodyDTO.setReturnMsg("传入数据为空");
            //头
            BeanUtils.copyProperties(resultSapHeaderDTO, headerDTO);
            resultInfo.setBodyDTO(bodyDTO);
            resultInfo.setHeaderDTO(headerDTO);
            return getResultForDcsInorOutServerDTO(resultInfo);
        }
        String s_body = new Gson().toJson(wmsDealerCarBackVOArray);
        String s_head = new Gson().toJson(resultSapHeaderDTO);
        try {
            ArrayList<ResultForDcsDealerCarbackDetailsDTO> results = wmsDealerCarBackService.saveWmsDealerCarBack(wmsDealerCarBackVOArray, s_body, s_head);
            if (CollectionUtils.isEmpty(results)) {
                //通过明细
                detailsDTO.seteMsg("处理成功");
                detailsDTO.setCbVin(null);
                //体
                bodyDTO.setReturnCode("S");
                detailsDTOS.add(detailsDTO);
                ResultForDcsDealerCarbackDetailsDTO[] detail = new ResultForDcsDealerCarbackDetailsDTO[results.size()];
                bodyDTO.setMsgDetails(detail);
                bodyDTO.setReturnMsg("处理成功");
                //头
                BeanUtils.copyProperties(resultSapHeaderDTO, headerDTO);
                resultInfo.setBodyDTO(bodyDTO);
                resultInfo.setHeaderDTO(headerDTO);
                return getResultForDcsInorOutServerDTO(resultInfo);
            } else {
                //体
                bodyDTO.setReturnCode("E");
                ResultForDcsDealerCarbackDetailsDTO[] detail = new ResultForDcsDealerCarbackDetailsDTO[results.size()];
                bodyDTO.setMsgDetails(results.toArray(detail));
                bodyDTO.setReturnMsg("处理失败!");
                //头
                BeanUtils.copyProperties(resultSapHeaderDTO, headerDTO);
                resultInfo.setBodyDTO(bodyDTO);
                resultInfo.setHeaderDTO(headerDTO);
                return getResultForDcsInorOutServerDTO(resultInfo);

            }
        } catch (Exception e) {
            logger.error("WmsBusinessServiceImpl.insertWmsDealerCarBack param{},{}", e);
            //明细
            detailsDTO.setCbVin(null);
            detailsDTO.seteMsg("系统内部异常!");
            detailsDTOS.add(detailsDTO);
            //体
            bodyDTO.setReturnCode("E");
            ResultForDcsDealerCarbackDetailsDTO[] results = new ResultForDcsDealerCarbackDetailsDTO[detailsDTOS.size()];
            bodyDTO.setMsgDetails(detailsDTOS.toArray(results));
            bodyDTO.setReturnMsg("系统内部异常");
            //头
            BeanUtils.copyProperties(resultSapHeaderDTO, headerDTO);
            resultInfo.setBodyDTO(bodyDTO);
            resultInfo.setHeaderDTO(headerDTO);
            return getResultForDcsInorOutServerDTO(resultInfo);
        }
    }
}
