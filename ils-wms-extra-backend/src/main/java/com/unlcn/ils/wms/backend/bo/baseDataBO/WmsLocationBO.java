package com.unlcn.ils.wms.backend.bo.baseDataBO;

/**
 * Created by DELL on 2017/8/18.
 */
public class WmsLocationBO {

    /**
     * 库位ID
     */
    private Long locId;

    /**
     * 仓库ID
     */
    private String locWhId;

    /**
     * 仓库CODE
     */
    private String locWhCode;

    /**
     * 仓库名称
     */
    private String locWhName;

    /**
     * 库区ID
     */
    private String locZoneId;

    /**
     * 库区CODE
     */
    private String locZoneCode;

    /**
     * 库区NAME
     */
    private String locZoneName;

    /**
     * 库位CODE
     */
    private String locCode;

    /**
     * 库位行
     */
    private Long locRow;

    /**
     * 库位列
     */
    private Long locColumn;

    /**
     * 库位层
     */
    private Long locLevel;

    /**
     * 库位长
     */
    private Long locLength;

    /**
     * 库位宽
     */
    private Long locWidth;

    /**
     * 启用禁用标识
     */
    private String locEnableFlag;

    /**
     * 逻辑删除
     */
    private Byte isDeleted;

    /**
     * 库位类型
     */
    private String locType;

    /**
     * 车辆类型
     */
    private String locSize;

    public Long getLocId() {
        return locId;
    }

    public void setLocId(Long locId) {
        this.locId = locId;
    }

    public String getLocWhId() {
        return locWhId;
    }

    public void setLocWhId(String locWhId) {
        this.locWhId = locWhId;
    }

    public String getLocWhCode() {
        return locWhCode;
    }

    public void setLocWhCode(String locWhCode) {
        this.locWhCode = locWhCode;
    }

    public String getLocWhName() {
        return locWhName;
    }

    public void setLocWhName(String locWhName) {
        this.locWhName = locWhName;
    }

    public String getLocZoneId() {
        return locZoneId;
    }

    public void setLocZoneId(String locZoneId) {
        this.locZoneId = locZoneId;
    }

    public String getLocZoneCode() {
        return locZoneCode;
    }

    public void setLocZoneCode(String locZoneCode) {
        this.locZoneCode = locZoneCode;
    }

    public String getLocZoneName() {
        return locZoneName;
    }

    public void setLocZoneName(String locZoneName) {
        this.locZoneName = locZoneName;
    }

    public String getLocCode() {
        return locCode;
    }

    public void setLocCode(String locCode) {
        this.locCode = locCode;
    }

    public Long getLocRow() {
        return locRow;
    }

    public void setLocRow(Long locRow) {
        this.locRow = locRow;
    }

    public Long getLocColumn() {
        return locColumn;
    }

    public void setLocColumn(Long locColumn) {
        this.locColumn = locColumn;
    }

    public Long getLocLevel() {
        return locLevel;
    }

    public void setLocLevel(Long locLevel) {
        this.locLevel = locLevel;
    }

    public Long getLocLength() {
        return locLength;
    }

    public void setLocLength(Long locLength) {
        this.locLength = locLength;
    }

    public Long getLocWidth() {
        return locWidth;
    }

    public void setLocWidth(Long locWidth) {
        this.locWidth = locWidth;
    }

    public String getLocEnableFlag() {
        return locEnableFlag;
    }

    public void setLocEnableFlag(String locEnableFlag) {
        this.locEnableFlag = locEnableFlag;
    }

    public Byte getIsDeleted() {
        return isDeleted;
    }

    public void setIsDeleted(Byte isDeleted) {
        this.isDeleted = isDeleted;
    }

    public String getLocType() {
        return locType;
    }

    public void setLocType(String locType) {
        this.locType = locType;
    }

    public String getLocSize() {
        return locSize;
    }

    public void setLocSize(String locSize) {
        this.locSize = locSize;
    }

    @Override
    public String toString() {
        return "WmsLocationBO{" +
                "locId=" + locId +
                ", locWhId='" + locWhId + '\'' +
                ", locWhCode='" + locWhCode + '\'' +
                ", locWhName='" + locWhName + '\'' +
                ", locZoneId='" + locZoneId + '\'' +
                ", locZoneCode='" + locZoneCode + '\'' +
                ", locZoneName='" + locZoneName + '\'' +
                ", locCode='" + locCode + '\'' +
                ", locRow=" + locRow +
                ", locColumn=" + locColumn +
                ", locLevel=" + locLevel +
                ", locLength=" + locLength +
                ", locWidth=" + locWidth +
                ", locEnableFlag=" + locEnableFlag +
                ", isDeleted=" + isDeleted +
                '}';
    }
}
