package com.unlcn.ils.wms.base.mapper.additional.wmsOutbound;

import com.unlcn.ils.wms.base.dto.WmsPreparationPlanResultDTO;
import com.unlcn.ils.wms.base.dto.WmsPreparationVehicleDetailDTO;
import com.unlcn.ils.wms.base.model.outbound.WmsPreparationPlan;
import com.unlcn.ils.wms.base.model.outbound.WmsPreparationVehicleDetail;
import org.apache.ibatis.annotations.Param;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by DELL on 2017/9/26.
 */
public interface WmsPreparationPlanExtMapper {
    /**
     * 查询备料计划数据
     *
     * @return
     */
    List<WmsPreparationPlan> queryForList(Map<String, Object> paramMap);

    /**
     * 查询备料计划数据-新, 支持车架号快捷查询
     * 2017-10-21
     *
     * @return
     */
    List<WmsPreparationPlan> queryForListNew(Map<String, Object> paramMap);

    /**
     * 查询备料计划记录数量
     *
     * @return
     */
    Integer queryForCount(Map<String, Object> paramMap);


    /**
     * 查询备料计划记录数量 - 新
     *
     * @return
     */
    Integer queryForCountNew(Map<String, Object> paramMap);

    /**
     * 根据主表ID查询详情
     *
     * @param ppId
     * @param del
     * @return
     */
    List<WmsPreparationVehicleDetail> getVehicleDetail(@Param("ppId") Long ppId, @Param("del") byte del);

    /**
     * 根据车架号查询车辆是否存在备料计划
     *
     * @param vin
     * @return
     */
    Integer queryByVin(String vin);

    void selectDispatchOrderByVin(@Param("vin") String oddVin, @Param("del") byte del);

    /**
     * 批量添加
     *
     * @param wmsPreparationPlans
     */
    void batchInsert(@Param("wmsPreparationPlans") List<WmsPreparationPlan> wmsPreparationPlans);

    /**
     * 根据借车单批量删除备料计划
     *
     * @param borrowNos
     */
    void batchDeleteByBorrowNo(@Param("borrowNos") List<String> borrowNos);

    List<WmsPreparationPlanResultDTO> selectStockTransferList(HashMap<String, Object> paramMap);

    int selectStockTransferCount(HashMap<String, Object> paramMap);

    List<WmsPreparationPlanResultDTO> selectStockTransferListByIds(ArrayList<Long> ids);

    List<WmsPreparationPlanResultDTO> selectStockTransferCustomerList(HashMap<String, Object> paramMap);

    List<WmsPreparationPlan> selectGateInDataByVehicle(HashMap<String, Object> paramMap);

    List<WmsPreparationPlan> selectPreparaingPlanByParams(HashMap<String, Object> paramMap);

    List<WmsPreparationVehicleDetailDTO> selectDetailListByParamForJM(HashMap<String, Object> params);
}
