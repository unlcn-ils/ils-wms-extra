package com.unlcn.ils.wms.backend.service.bigscreen.impl;

import cn.huiyunche.commons.exception.BusinessException;
import cn.huiyunche.commons.utils.HttpRequestUtil;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.google.common.base.Strings;
import com.unlcn.ils.wms.backend.bo.biBO.BiPickDataBO;
import com.unlcn.ils.wms.backend.dto.bigscreenDTO.PickDataDTO;
import com.unlcn.ils.wms.backend.enums.TmsUrlEnum;
import com.unlcn.ils.wms.backend.enums.TmsUrlTypeEnum;
import com.unlcn.ils.wms.backend.service.bigscreen.PickDataService;
import com.unlcn.ils.wms.backend.util.TimeUtil;
import com.unlcn.ils.wms.base.mapper.bigscreen.BiPickDataMapper;
import com.unlcn.ils.wms.base.mapper.extmapper.BiPickDataExtMapper;
import com.unlcn.ils.wms.base.mapper.sys.TmsCallHistoryMapper;
import com.unlcn.ils.wms.base.model.bigscreen.BiPickData;
import com.unlcn.ils.wms.base.model.sys.TmsCallHistory;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * 提车推移图service
 */
@Service
public class PickDataServiceImpl implements PickDataService {

    @Autowired
    private BiPickDataExtMapper biPickDataExtMapper;

    @Autowired
    private BiPickDataMapper biPickDataMapper;

    @Autowired
    private TmsCallHistoryMapper tmsCallHistoryMapper;


    @Value("${tms.pickup.host.url}")
    private String propertyUrl;

    @Value("${tms.pickup.host.timeout}")
    private String propertyTime;

    @Value("${tms.encode.key}")
    private String propertyKey;


    private Logger LOGGER = LoggerFactory.getLogger(PickDataServiceImpl.class);


    @Override
    public void addPickData() {
        SimpleDateFormat format = new SimpleDateFormat("yyyyMMdd");

        Calendar calendar = Calendar.getInstance();
        calendar.setTime(new Date());
        String dateStr = format.format(calendar.getTime());

        String strUrl=propertyUrl + TmsUrlEnum.BI_PICK_DATE.getText();
        Integer time = Integer.parseInt(propertyTime);
        String encode_key = propertyKey;
        Map<String, Object> map = new HashMap<>();
        Map<String, Object> headerMap = new HashMap<>();
        headerMap.put("encode-key", encode_key);

        map.put("date", dateStr);

        String result = null;

        TmsCallHistory tmsCallHistory=new TmsCallHistory();

        try {

            tmsCallHistory.setTmsUrl(strUrl);
            tmsCallHistory.setPullOrPush(0);
            tmsCallHistory.setCallStartTime(new Date());
            tmsCallHistory.setTmsUrlType(TmsUrlTypeEnum.BI_PICK_DATE.getValue());
            tmsCallHistory.setParameters(dateStr);

            result = HttpRequestUtil.sendHttpPost(strUrl, headerMap, map, time);

        } catch (Exception e) {
            LOGGER.error("PickDataServiceImpl.addPickData error : ", e);
            throw new BusinessException("访问tms接口失败");
        }

        try{
            if (StringUtils.isNotBlank(result)) {
                JSONObject jsonObject = JSONObject.parseObject(result);
                String msg = jsonObject.getString("message");
                String records = jsonObject.getString("records");
                Boolean success = jsonObject.getBoolean("success");

                tmsCallHistory.setCallEndTime(new Date());
                tmsCallHistory.setMessage(msg);
                tmsCallHistory.setResultContent(result);

                if (!success) {
                    LOGGER.error("PickDataServiceImpl callTms error msg: {}", msg);
                    tmsCallHistory.setResultCount(0);
                    tmsCallHistory.setSuccess(success);
                    tmsCallHistoryMapper.insertSelective(tmsCallHistory);
                }

                List<BiPickDataBO> tmsBOList = JSONArray.parseArray(records, BiPickDataBO.class);
                tmsCallHistory.setResultCount(tmsBOList.size());
                tmsCallHistory.setSuccess(success);
                tmsCallHistoryMapper.insertSelective(tmsCallHistory);

                SimpleDateFormat format2 = new SimpleDateFormat("yyyyMMddHHmmSS");
                Calendar calendar2 = Calendar.getInstance();
                calendar2.setTime(new Date());
                String dateStr2 = format2.format(calendar2.getTime());

                tmsBOList.forEach((BiPickDataBO v) -> {
                    //新增
                    BiPickData biPickData = new BiPickData();
                    biPickData.setOrderCount(Integer.parseInt(Strings.isNullOrEmpty(v.getOrder_qty())?"0":v.getOrder_qty()));
                    biPickData.setNotPickCount(Integer.parseInt(Strings.isNullOrEmpty(v.getUnget_qty())?"0":v.getUnget_qty()));
                    biPickData.setPickCount(Integer.parseInt(Strings.isNullOrEmpty(v.getGot_qty())?"0":v.getGot_qty()));
                    biPickData.setPickRate(Float.parseFloat(Strings.isNullOrEmpty(v.getIntime_radio())?"0":v.getIntime_radio()));
                    biPickData.setVersion(dateStr2);
                    SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");

                    try {

                        biPickData.setStatisticsTime(sdf.parse(v.getRpt_date()));
                    } catch (ParseException e) {
                        LOGGER.error("PickDataServiceImpl.addPickData error : ", e);
                        throw new BusinessException("日期转换异常!");
                    }

                    biPickDataMapper.insertSelective(biPickData);

                });
            }

        }
        catch (Exception e) {
            LOGGER.error("PickDataServiceImpl.addPickData error : ", e);
            throw new BusinessException("保存失败");
        }
    }

    @Override
    public List<PickDataDTO> pickDataCount() {
        SimpleDateFormat format = new SimpleDateFormat("yyyyMMdd");

        // 查询五天内最新的提车推移图统计数据
        List<BiPickData> biPickDataList = biPickDataExtMapper.selectCountData();

        PickDataDTO pickDataDTO1 = new PickDataDTO();
        PickDataDTO pickDataDTO2 = new PickDataDTO();
        PickDataDTO pickDataDTO3 = new PickDataDTO();
        PickDataDTO pickDataDTO4 = new PickDataDTO();

        pickDataDTO1.setName("下单");
        pickDataDTO2.setName("提车");
        pickDataDTO3.setName("未提");
        pickDataDTO4.setName("提车及时率");

        Integer[] count1 = new Integer[5];
        Integer[] count2 = new Integer[5];
        Integer[] count3 = new Integer[5];
        Float[] rate4 = new Float[5];

        String[] dates = TimeUtil.getBeforeDates1(5);
        for (int i=0; dates.length>i; i++) {
            String date = dates[i];
            for (int num=0; biPickDataList.size()>num; num++) {
                BiPickData biPickData= biPickDataList.get(num);
                if (format.format(biPickData.getStatisticsTime()).contains(date)) {
                    count1[i] = biPickData.getOrderCount();
                    count2[i] = biPickData.getPickCount();
                    count3[i] = biPickData.getNotPickCount();
                    rate4[i] = biPickData.getPickRate();
                    break;
                }
            }
        }
        for (Integer num=0; count1.length>num; num++) {
            Integer count = count1[num];
            if (count == null)
                count1[num] = 0;
        }
        for (Integer num=0; count2.length>num; num++) {
            Integer count = count2[num];
            if (count == null)
                count2[num] = 0;
        }
        for (Integer num=0; count3.length>num; num++) {
            Integer count = count3[num];
            if (count == null)
                count3[num] = 0;
        }
        for (Integer num=0; rate4.length>num; num++) {
            Float rate = rate4[num];
            if (rate == null)
                rate4[num] = new Float(0.0);
        }

        pickDataDTO1.setData(count1);
        pickDataDTO2.setData(count2);
        pickDataDTO3.setData(count3);
        pickDataDTO4.setRate(rate4);

        pickDataDTO1.setDate(TimeUtil.getBeforeDates2(5));
        pickDataDTO2.setDate(TimeUtil.getBeforeDates2(5));
        pickDataDTO3.setDate(TimeUtil.getBeforeDates2(5));
        pickDataDTO4.setDate(TimeUtil.getBeforeDates2(5));

        List<PickDataDTO> pickDataDTOList = new ArrayList<PickDataDTO>();
        pickDataDTOList.add(pickDataDTO1);
        pickDataDTOList.add(pickDataDTO2);
        pickDataDTOList.add(pickDataDTO3);
        pickDataDTOList.add(pickDataDTO4);

        return pickDataDTOList;
    }


}
