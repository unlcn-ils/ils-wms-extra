package com.unlcn.ils.wms.backend.service.webservice.bo;

import java.io.Serializable;
import java.util.List;

public class WmsInOutResultFromDcsBO implements Serializable {

    //{"returnMsg":[{"VIN":"?","eMsg":"出入库错误;"}],"returnCode":"E"}
    private List<XmlInboundReturnMsgBO> returnMsg;
    private String returnCode;
    private String vin;
    private String eMsg;
    private String returnMsgStr;

    public String getReturnMsgStr() {
        return returnMsgStr;
    }

    public void setReturnMsgStr(String returnMsgStr) {
        this.returnMsgStr = returnMsgStr;
    }

    public String getVin() {
        return vin;
    }

    public void setVin(String vin) {
        this.vin = vin;
    }

    public String geteMsg() {
        return eMsg;
    }

    public void seteMsg(String eMsg) {
        this.eMsg = eMsg;
    }

    public List<XmlInboundReturnMsgBO> getReturnMsg() {
        return returnMsg;
    }

    public void setReturnMsg(List<XmlInboundReturnMsgBO> returnMsg) {
        this.returnMsg = returnMsg;
    }

    public String getReturnCode() {
        return returnCode;
    }

    public void setReturnCode(String returnCode) {
        this.returnCode = returnCode;
    }
}
