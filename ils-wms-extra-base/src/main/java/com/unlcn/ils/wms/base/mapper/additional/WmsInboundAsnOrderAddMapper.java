package com.unlcn.ils.wms.base.mapper.additional;

import com.unlcn.ils.wms.base.businessDTO.inbound.AsnOrderDTO;
import com.unlcn.ils.wms.base.model.inbound.WmsInboundOrder;
import com.unlcn.ils.wms.base.model.inbound.WmsInboundOrderDetail;
import org.apache.ibatis.annotations.Param;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public interface WmsInboundAsnOrderAddMapper {
    /**
     * 根据条件查询订单数据
     *
     * @param map
     * @return
     */
    List<AsnOrderDTO> queryAsnOrderInfo(Map<String, Object> map);

    /**
     * 根据ID查询订单数据
     *
     * @param paramMap
     * @return
     */
    List<WmsInboundOrderDetail> selectByAsnOrderId(Map<String, Object> paramMap);

    Long insertAsnOrderReturnId(WmsInboundOrder wmsInboundAsnOrder);

    /**
     * 根据ID修改订单数据
     *
     * @param paramMap
     */
    void updateAsnOrderByAsnOrderId(Map<String, Object> paramMap);

    /**
     * 由于现在都是一单一车的情况，当时我考虑到一单多车所以建了主从表，
     *
     * @param asnId
     * @return
     */
    List<AsnOrderDTO> queryAllAsnInfo(String asnId);

    /**
     * 根据asnId查询数据
     *
     * @param paramMap
     * @return
     */
    List<AsnOrderDTO> queryAsnInfoByAsnIds(Map<String, Object> paramMap);

    /**
     * 查询入库单分页
     *
     * @param paramMap
     * @return
     */
    List<AsnOrderDTO> queryForList(Map<String, Object> paramMap);

    /**
     * 查询入库单记录数
     *
     * @param paramMap
     * @return
     */
    Integer queryForCount(Map<String, Object> paramMap);

    /**
     * 根据id批量删除
     *
     * @param ids
     */
    void deleteByAsnOrderIds(List<String> ids);

    /**
     * 根据id批量删除订单详情
     *
     * @param ids
     */
    void deleteDetailByAsnOrderIds(List<String> ids);

    /**
     * 根据id查询订单数据
     *
     * @param paramMap
     * @return
     */
    AsnOrderDTO getAsnOrderById(Map<String, Object> paramMap);

    /**
     * 根据运单号查询订单数据
     *
     * @param paramMap
     * @return
     */
    AsnOrderDTO getAsnOrderByBillId(Map<String, Object> paramMap);

    void updateOrderDetailByPrimaryKeySelective(WmsInboundOrderDetail wmsInboundOrderDetail);

    AsnOrderDTO queryByWaybillOrVin(String queryParam);

    AsnOrderDTO queryByVin(String queryParam);

    AsnOrderDTO queryByDriverId(@Param("driverId") Long driverId);

    /**
     * 查询入库记录
     *
     * @return
     */
    List<Map<String, Object>> queryInboundListForJm(Map<String, Object> paramMap);

    List<Map<String, Object>> queryinboundListForCQ(Map<String, Object> paramMap);

    /**
     * 查询入库记录数量
     *
     * @return
     */
    Integer queryinboundCountForJm(Map<String, Object> paramMap);

    Integer queryinboundCountForCQ(Map<String, Object> paramMap);

    List<WmsInboundOrder> selectAllCustomerName(@Param("del") byte del, @Param("whCode") String whCode);


    /**
     * @param @param  paramMap
     * @param @return
     * @return WmsInboundOrder    返回类型
     * @throws
     * @Title: selectInboundOrderByVin
     * @Description: 根据vin 获取入库单信息
     */
    WmsInboundOrder selectInboundOrderByVin(Map<String, Object> paramMap);

    /**
     * 备料钥匙领取
     *
     * @param vin
     * @return
     */
    AsnOrderDTO queryByVinForKey(@Param("vin") String vin);

    /**
     * 入库记录导出数据查询
     *
     * @param paramMap
     * @return
     */
    List<Map<String, Object>> selectInboundRecordImportExcelForCQ(HashMap<String, Object> paramMap);

    /**
     * 入库记录条数
     *
     * @param paramMap
     * @return
     */
    Integer countInboundRecord(HashMap<String, Object> paramMap);

    void updateSetSQLMode();

    List<Map<String,Object>> selectInboundRecordImportExcelForJM(HashMap<String, Object> paramMap);
}