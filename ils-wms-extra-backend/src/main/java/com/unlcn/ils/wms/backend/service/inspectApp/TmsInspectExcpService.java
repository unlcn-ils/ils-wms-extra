package com.unlcn.ils.wms.backend.service.inspectApp;


import com.unlcn.ils.wms.backend.bo.inspectAppBO.TmsInspectExcpBO;
import com.unlcn.ils.wms.backend.bo.inspectAppBO.TmsInspectExcpConstantBO;
import com.unlcn.ils.wms.backend.bo.inspectAppBO.TmsInspectExcpDetailBO;
import com.unlcn.ils.wms.base.dto.TmsInspectExcpDTO;
import com.unlcn.ils.wms.base.dto.WmsInspectForAppDTO;
import com.unlcn.ils.wms.base.model.inspectApp.TmsInspectExcp;

import java.util.List;

public interface TmsInspectExcpService {

    /**
     * 获取九大位置异常常量
     */
    List<TmsInspectExcpConstantBO> getInspectExcpList(WmsInspectForAppDTO position);

    /**
     * 通过验车单id获取绑定的异常状态
     */
    List<TmsInspectExcp> getTmsInspectExcpByBillId(Long id);

    /**
     * 标记异常
     */
    void saveNoteExcp(TmsInspectExcpBO bo) ;

    /**
     * 根据异常位置获取对应异常列表
     */
    List<TmsInspectExcpDetailBO> getInspectExcpListByPositionId(TmsInspectExcpBO bo) ;

    /**
     * 根据运单id获取九大区域和每个区域的异常数量
     */
    List<TmsInspectExcpDTO> getPositionExcpCountByInspectId(Long inspectId, String whCode);

    void deleteAllException(WmsInspectForAppDTO dto);
}
