package com.unlcn.ils.wms.backend.dto.bigscreenDTO;

import java.util.Arrays;
import java.util.List;

/**
 * Created by lenovo on 2017/11/8.
 */
public class InvoicingStatisticsLineDataDTO {

    //private String[] head;
    private List<String> head;

    private InvoicingStatisticsLineBody[] invoicingStatisticsLineBody;

    //public String[] getHead() {
    //    return head;
    //}
    //
    //public void setHead(String[] head) {
    //    this.head = head;
    //}

    public List<String> getHead() {
        return head;
    }

    public void setHead(List<String> head) {
        this.head = head;
    }

    public InvoicingStatisticsLineBody[] getInvoicingStatisticsLineBody() {
        return invoicingStatisticsLineBody;
    }

    public void setInvoicingStatisticsLineBody(InvoicingStatisticsLineBody[] invoicingStatisticsLineBody) {
        this.invoicingStatisticsLineBody = invoicingStatisticsLineBody;
    }

    @Override
    public String toString() {
        return "InvoicingStatisticsLineDataDTO{" +
                "head=" + head +
                ", invoicingStatisticsLineBody=" + Arrays.toString(invoicingStatisticsLineBody) +
                '}';
    }
    //@Override
    //public String toString() {
    //    return "InvoicingStatisticsLineDataDTO{" +
    //            "head=" + Arrays.toString(head) +
    //            ", invoicingStatisticsLineBody=" + Arrays.toString(invoicingStatisticsLineBody) +
    //            '}';
    //}
}
