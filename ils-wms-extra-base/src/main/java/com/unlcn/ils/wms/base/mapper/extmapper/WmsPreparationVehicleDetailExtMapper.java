package com.unlcn.ils.wms.base.mapper.extmapper;

import com.unlcn.ils.wms.base.model.outbound.WmsPreparationVehicleDetail;

import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface WmsPreparationVehicleDetailExtMapper {
    /**
     * 批量插入
     * @param wmsPreparationVehicleDetails
     */
    void batchInsert(@Param("wmsPreparationVehicleDetails") List<WmsPreparationVehicleDetail> wmsPreparationVehicleDetails);

    /**
    * 批量插入备料详情
    * @param detailList
     * */
    void insertDetailForBatch(@Param("detailList") List<WmsPreparationVehicleDetail> detailList);
}

