package com.unlcn.ils.wms.web.controller.sys;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import com.unlcn.ils.wms.backend.bo.sys.PermissionsBO;
import com.unlcn.ils.wms.backend.service.sys.PermissionsService;
import com.unlcn.ils.wms.web.vo.sys.PermissionsVO;

import cn.huiyunche.commons.domain.ResultDTO;
import cn.huiyunche.commons.exception.BusinessException;

/**
 * @author laishijian
 * @ClassName: PermissionsController
 * @Description: 权限
 * @date 2017年8月8日 下午3:47:37
 */
@Controller
@RequestMapping("/permissions")
@ResponseBody
public class PermissionsController {

    private Logger LOGGER = LoggerFactory.getLogger(PermissionsController.class);

    @Autowired
    private PermissionsService permissionsService;

    /**
     * @param @param  permissionsVO
     * @param @return
     * @return ResultDTO<Object>    返回类型
     * @throws
     * @Title: add
     * @Description: 新增权限
     */
    @RequestMapping(value = "/add", method = RequestMethod.POST)
    public ResultDTO<Object> add(@RequestBody PermissionsVO permissionsVO) {

        LOGGER.info("PermissionsController.add param : {}", permissionsVO);

        ResultDTO<Object> result = new ResultDTO<>(true);
        try {

            PermissionsBO permissionsBO = new PermissionsBO();
            BeanUtils.copyProperties(permissionsVO, permissionsBO);

            permissionsService.add(permissionsBO);

            result.setMessage("新增权限成功");

        } catch (BusinessException be) {
            result.setSuccess(false);
            result.setMessage(be.getMessage());
            LOGGER.error("PermissionsController.add error: {}", be);
        } catch (Exception e) {
            result.setSuccess(false);
            result.setMessage("系统异常");
            LOGGER.error("PermissionsController.add error: {}", e);
        }
        return result;
    }

    /**
     * 查询所有权限
     *
     * @return
     */
    @RequestMapping(value = "/list", method = RequestMethod.POST)
    public ResultDTO<Object> list() {
        LOGGER.info("PermissionsController.list");

        ResultDTO<Object> result = new ResultDTO<>(true, null, "查询失败");
        try {
            result.setData(permissionsService.findAllForEnabled());
        } catch (Exception e) {
            LOGGER.info("PermissionsController.list");
            result.setSuccess(false);
            result.setMessage("查询失败");
        }
        return result;
    }

    /**
     * 根据用户id获取权限
     *
     * @param id
     * @return
     */
    @RequestMapping(value = "/getPermissListByUserId/{id}", method = RequestMethod.GET)
    public ResultDTO<Object> getPermissListByUserId(@PathVariable Integer id) {
        LOGGER.info("PermissionsController.getPermissListByUserId id: {}", id);
        ResultDTO<Object> result = new ResultDTO<>(true, null, "获取权限成功");

        try {
            result.setData(permissionsService.getPermissListByUserId(id));
        } catch (BusinessException e) {
            LOGGER.info("PermissionsController.getPermissListByUserId businessException: {}", e);
            result.setSuccess(false);
            result.setMessage(e.getMessage());
        } catch (Exception e) {
            LOGGER.info("PermissionsController.getPermissListByUserId error: {}", e);
            result.setSuccess(false);
            result.setMessage("获取权限失败");
        }
        return result;
    }
}
