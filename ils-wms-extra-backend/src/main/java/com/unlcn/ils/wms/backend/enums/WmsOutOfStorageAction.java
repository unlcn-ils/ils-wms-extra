package com.unlcn.ils.wms.backend.enums;

/**
 * 君马出入库接动作
 *
 * @author renml
 */
public enum WmsOutOfStorageAction {
    ACTION_INBOUND("C2", "入库"),
    ACTION_OUTBOUND("C1", "出库");

    private final String value;
    private final String text;

    WmsOutOfStorageAction(String value, String text) {
        this.value = value;
        this.text = text;
    }

    public String getValue() {
        return value;
    }

    public String getText() {
        return text;
    }

    public static WmsOutOfStorageAction getByValue(String value) {
        for (WmsOutOfStorageAction temp : WmsOutOfStorageAction.values()) {
            if (temp.getValue().equals(value)) {
                return temp;
            }
        }
        return null;
    }
}
