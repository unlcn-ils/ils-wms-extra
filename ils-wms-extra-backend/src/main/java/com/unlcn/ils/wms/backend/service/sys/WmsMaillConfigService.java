package com.unlcn.ils.wms.backend.service.sys;

import com.unlcn.ils.wms.base.model.inbound.WmsMailConfig;

import java.util.List;
import java.util.Map;

/**
 * Created by lenovo on 2017/10/25.
 */
public interface WmsMaillConfigService {
    /**
     * 根据参数获取邮件配置信息
     * @param paramsMap
     * @return
     */
    List<WmsMailConfig> findParams(Map<String, Object> paramsMap);
 }
