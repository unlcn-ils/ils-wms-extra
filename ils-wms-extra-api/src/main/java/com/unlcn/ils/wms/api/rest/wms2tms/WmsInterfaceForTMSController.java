package com.unlcn.ils.wms.api.rest.wms2tms;


import cn.huiyunche.commons.domain.ResultDTO;
import cn.huiyunche.commons.exception.BusinessException;
import com.unlcn.ils.wms.backend.service.inbound.WmsInterfaceForTmsService;
import com.unlcn.ils.wms.base.dto.WmsForTmsInterfaceDTO;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.Map;

/**
 * wms提供给TMS系统接口
 *
 * @author user
 */
@Controller
@ResponseBody
@RequestMapping(value = "/wmsInterface")
@Api(value = "/wmsInterface", description = "WMS系统提供给TMS系统的接口")
public class WmsInterfaceForTMSController {
    private Logger logger = LoggerFactory.getLogger(WmsInterfaceForTMSController.class);

    private WmsInterfaceForTmsService wmsInterfaceForTmsService;

    @Autowired
    public void setWmsInterfaceForTmsService(WmsInterfaceForTmsService wmsInterfaceForTmsService) {
        this.wmsInterfaceForTmsService = wmsInterfaceForTmsService;
    }


    /**
     * <p>
     * 2018-4-19 该方法因入库通知单重构--新方法{@link WmsInterfaceForTMSController#updateInboundOrder}
     * </p>
     */
    @RequestMapping(value = "/updateInboundOrderOld", method = RequestMethod.POST)
    @ApiOperation(value = "/updateInboundOrderOld", notes = "此接口用于TMS系统更新入库信息")
    @Deprecated
    public ResultDTO<Object> updateInboundOrderOld(WmsForTmsInterfaceDTO interfaceDTO, HttpServletRequest request) {
        logger.info("WmsInterfaceForTMSController.updateInboundOrderOld param:{}", interfaceDTO);
        ResultDTO<Object> resultDTO = new ResultDTO<>(true, null, "更新信息成功");
        try {
            String hKey = request.getHeader("encode-key");
            interfaceDTO.sethKey(hKey);
            ArrayList<Map<String, String>> result = wmsInterfaceForTmsService.updateInboundOrderOld(interfaceDTO);
            resultDTO.setData(result);
        } catch (BusinessException e) {
            logger.error("WmsInterfaceForTMSController.updateInboundOrderOld  BusinessException: {}", e);
            resultDTO.setMessage(e.getMessage());
            resultDTO.setSuccess(false);
            resultDTO.setData(null);
        } catch (Exception ex) {
            resultDTO.setMessage("接口异常");
            resultDTO.setSuccess(false);
            resultDTO.setData(null);
            logger.error("WmsInterfaceForTMSController.updateInboundOrderOld error: {}", ex);
        }
        return resultDTO;
    }

    @RequestMapping(value = "/updateInboundOrder", method = RequestMethod.POST)
    @ApiOperation(value = "/updateInboundOrder", notes = "此接口用于TMS系统更新入库信息")
    public ResultDTO<Object> updateInboundOrder(WmsForTmsInterfaceDTO interfaceDTO, HttpServletRequest request) {
        logger.info("WmsInterfaceForTMSController.updateInboundOrder param:{}", interfaceDTO);
        ResultDTO<Object> resultDTO = new ResultDTO<>(true, null, "更新信息成功");
        try {
            String hKey = request.getHeader("encode-key");
            interfaceDTO.sethKey(hKey);
            ArrayList<Map<String, String>> result = wmsInterfaceForTmsService.updateInboundOrder(interfaceDTO);
            resultDTO.setData(result);
        } catch (BusinessException e) {
            logger.error("WmsInterfaceForTMSController.updateInboundOrder  BusinessException: {}", e);
            resultDTO.setMessage(e.getMessage());
            resultDTO.setSuccess(false);
            resultDTO.setData(null);
        } catch (Exception ex) {
            resultDTO.setMessage("接口异常");
            resultDTO.setSuccess(false);
            resultDTO.setData(null);
            logger.error("WmsInterfaceForTMSController.updateInboundOrder error: {}", ex);
        }
        return resultDTO;
    }
}


