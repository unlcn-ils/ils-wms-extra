package com.unlcn.ils.wms.backend.service.outbound;

import com.unlcn.ils.wms.backend.bo.outboundBO.WmsDepartureBO;
import com.unlcn.ils.wms.base.model.outbound.WmsDepartureRegister;

import java.util.Map;

/**
 * @Auther linbao
 * @Date 2017-11-14
 */
public interface WmsDepartureRegisterService {

    /**
     * 板车离场登记分页查询
     *
     * @param wmsDepartureBO
     *@param whCode @return
     * @throws Exception
     */
    Map<String, Object> queryDepartureListForPage(WmsDepartureBO wmsDepartureBO, String whCode) throws Exception;

    /**
     * 正常离场
     *
     * @param wmsDepartureBO
     * @throws Exception
     */
    void updateToNormalDeparture(WmsDepartureBO wmsDepartureBO) throws Exception;

    /**
     * 异常离场
     *
     * @param wmsDepartureBO
     * @throws Exception
     */
    void updateToExceptionDeparture(WmsDepartureBO wmsDepartureBO) throws Exception;

    /**
     * 新增
     *
     * @param wmsDepartureRegister
     * @throws Exception
     */
    Long addDeparture(WmsDepartureRegister wmsDepartureRegister) throws Exception;

    /**
     * 根据id获取
     * @param drId
     * @return
     * @throws Exception
     */
    WmsDepartureRegister getByPrimarkKey(Long drId) throws Exception;
}
