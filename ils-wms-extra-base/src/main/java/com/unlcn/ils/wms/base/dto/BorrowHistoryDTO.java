package com.unlcn.ils.wms.base.dto;

import java.util.Date;

/**
 * Created by lenovo on 2017/10/20.
 */
public class BorrowHistoryDTO {
    // 车架号(VIN码)
    private String vin;

    // 还车单id
    private Long returnId;

    // 还车状态
    private Integer returnStatus;

    // 借用人
    private String borrowName;

    // 借用日期
    private Date borrowDate;

    // 预计还车日期
    private Date estimatedReturnDate;

    // 还车人
    private String returnName;

    // 实际还车日期
    private Date returnDate;

    public String getVin() {
        return vin;
    }

    public void setVin(String vin) {
        this.vin = vin;
    }

    public Long getReturnId() {
        return returnId;
    }

    public void setReturnId(Long returnId) {
        this.returnId = returnId;
    }

    public Integer getReturnStatus() {
        return returnStatus;
    }

    public void setReturnStatus(Integer returnStatus) {
        this.returnStatus = returnStatus;
    }

    public String getBorrowName() {
        return borrowName;
    }

    public void setBorrowName(String borrowName) {
        this.borrowName = borrowName;
    }

    public Date getBorrowDate() {
        return borrowDate;
    }

    public void setBorrowDate(Date borrowDate) {
        this.borrowDate = borrowDate;
    }

    public Date getEstimatedReturnDate() {
        return estimatedReturnDate;
    }

    public void setEstimatedReturnDate(Date estimatedReturnDate) {
        this.estimatedReturnDate = estimatedReturnDate;
    }

    public String getReturnName() {
        return returnName;
    }

    public void setReturnName(String returnName) {
        this.returnName = returnName;
    }

    public Date getReturnDate() {
        return returnDate;
    }

    public void setReturnDate(Date returnDate) {
        this.returnDate = returnDate;
    }

    @Override
    public String toString() {
        return "BorrowHistoryDTO{" +
                "vin='" + vin + '\'' +
                ", returnId=" + returnId +
                ", returnStatus=" + returnStatus +
                ", borrowName='" + borrowName + '\'' +
                ", borrowDate=" + borrowDate +
                ", estimatedReturnDate=" + estimatedReturnDate +
                ", returnName='" + returnName + '\'' +
                ", returnDate=" + returnDate +
                '}';
    }
}
