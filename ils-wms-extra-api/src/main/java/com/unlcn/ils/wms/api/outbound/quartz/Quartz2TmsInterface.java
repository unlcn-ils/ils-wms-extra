package com.unlcn.ils.wms.api.outbound.quartz;

import com.unlcn.ils.wms.backend.service.outbound.WmsCQOutboundService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component("quartzTmsInterface")
public class Quartz2TmsInterface {

    @Autowired
    private WmsCQOutboundService wmsCQOutboundService;

    public void sendOutboundDataToTMSForJM() {
        wmsCQOutboundService.updateSendOutboundDataToTMS();
    }

    public void sendOutboundDataExcpToTMSForJM() {
        wmsCQOutboundService.sendOutboundDataExcpToTMSForJM();
    }

}
