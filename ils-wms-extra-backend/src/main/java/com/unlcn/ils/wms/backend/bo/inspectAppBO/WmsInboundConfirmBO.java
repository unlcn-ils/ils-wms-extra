package com.unlcn.ils.wms.backend.bo.inspectAppBO;

public class WmsInboundConfirmBO {
	private String warehousePos;
	private String userId;
	private String vin;
	private String whCode;

	public String getVin() {
		return vin;
	}

	public void setVin(String vin) {
		this.vin = vin;
	}

	public String getWarehousePos() {
		return warehousePos;
	}

	public void setWarehousePos(String warehousePos) {
		this.warehousePos = warehousePos;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getWhCode() {
		return whCode;
	}

	public void setWhCode(String whCode) {
		this.whCode = whCode;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("WmsInboundConfirmBO [warehousePos=");
		builder.append(warehousePos);
		builder.append(", userId=");
		builder.append(userId);
		builder.append(", vin=");
		builder.append(vin);
		builder.append(", whCode=");
		builder.append(whCode);
		builder.append("]");
		return builder.toString();
	}

}
