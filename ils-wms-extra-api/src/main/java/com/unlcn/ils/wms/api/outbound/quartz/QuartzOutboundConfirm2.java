package com.unlcn.ils.wms.api.outbound.quartz;

import com.unlcn.ils.wms.backend.service.outbound.OutBoundTaskService;
import org.quartz.JobExecutionException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component("outboundConfirmJob2")
public class QuartzOutboundConfirm2 {

    private Logger logger = LoggerFactory.getLogger(QuartzOutboundConfirm2.class);

    @Autowired
    private OutBoundTaskService outBoundTaskService;

    public void outboundConfirm2() throws JobExecutionException {
        logger.info("QuartzOutboundConfirm.outboundConfirm2 定时任务start========");
        outBoundTaskService.getOutboundSendMailData();
        logger.info("QuartzOutboundConfirm.outboundConfirm2 定时任务end========");
    }
}
