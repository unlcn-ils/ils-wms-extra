package com.unlcn.ils.wms.backend.enums;

/**
 * 用于维修的异常处理状态记录
 */
public enum WmsExcpStatusEnum {
    UNTREATED(10,"未处理"),
    REPAIRING(20,"维修中"),
    CLOSED_EXCP(30,"已关闭"),
    EXCP_COMP(40,"让步");

    private final int value;
    private final String text;
    WmsExcpStatusEnum(int value, String text){
        this.value = value;
        this.text = text;
    }

    public int getValue() {
        return value;
    }

    public String getText() {
        return text;
    }

    public static WmsExcpStatusEnum getByValue(int value){
        for (WmsExcpStatusEnum temp : WmsExcpStatusEnum.values()) {
            if(temp.getValue() == value){
                return temp;
            }
        }
        return null;
    }
}
