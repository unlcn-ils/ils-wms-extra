package com.unlcn.ils.wms.web.controller.sys;

import cn.huiyunche.commons.domain.ResultDTO;
import cn.huiyunche.commons.exception.BusinessException;
import com.unlcn.ils.wms.backend.bo.sys.SysUserBO;
import com.unlcn.ils.wms.backend.service.sys.SysUserService;
import com.unlcn.ils.wms.backend.service.verify.RedisService;
import com.unlcn.ils.wms.base.dto.SysUserDTO;
import com.unlcn.ils.wms.web.vo.sys.SysUserVO;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.UUID;

/**
 * @Auther linbao
 * @Date 2017-10-12
 */
@Controller
@RequestMapping("/sysUser")
public class SysUserController {

    private Logger LOGGER = LoggerFactory.getLogger(SysUserController.class);

    @Autowired
    private SysUserService sysUserService;

    @Autowired
    private RedisService redisService;

    /**
     * pc登录
     *
     * @param sysUserVO
     * @return
     */
    @RequestMapping(value = "/loginForWeb", method = RequestMethod.POST)
    public @ResponseBody
    ResultDTO<Object> loginForWeb(@RequestBody SysUserVO sysUserVO) {
        LOGGER.info("LoginController.loginForWeb sysUserVO: {}", sysUserVO);
        ResultDTO<Object> resultDTO = new ResultDTO<>(true, null, "登录成功");
        try {
            SysUserBO sysUserBO = new SysUserBO();
            BeanUtils.copyProperties(sysUserVO, sysUserBO);
            SysUserDTO sysUserDTO = sysUserService.loginForWeb(sysUserBO);
            sysUserDTO.setToken(UUID.randomUUID().toString());
            resultDTO.setData(sysUserDTO);
            //保存redis用户
            //VerifyBO verifyBO = new VerifyBO();
            //verifyBO.setTime(System.currentTimeMillis());
            //verifyBO.setToken(sysUserDTO.getToken());
            //verifyBO.setUserId(sysUserDTO.getId());
            //redisService.rpush(JSONObject.toJSONString(verifyBO));
        } catch (BusinessException e) {
            LOGGER.error("SysUserController.loginForWeb businessException: {}", e);
            resultDTO.setMessage(e.getMessage());
            resultDTO.setSuccess(false);
        } catch (Exception e) {
            LOGGER.error("SysUserController.loginForWeb error: {}", e);
            resultDTO.setMessage("登录失败");
            resultDTO.setSuccess(false);
        }
        return resultDTO;
    }

    /**
     * pc注销登出
     *
     * @param token
     * @return
     */
    @RequestMapping(value = "/loginOutForWeb/{token}", method = RequestMethod.GET)
    public @ResponseBody
    ResultDTO<Object> loginOutForWeb(@PathVariable String token) {
        LOGGER.info("LoginController.loginOutForWeb token: {}", token);
        ResultDTO<Object> resultDTO = new ResultDTO<>(true, null, "注销成功");

        try {
            if (StringUtils.isNotBlank(token)) {
                redisService.removeDataByToken(token);
            }
        } catch (Exception e) {
            LOGGER.error("LoginController.loginOutForWeb error: {}", e);
            resultDTO.setSuccess(false);
            resultDTO.setMessage("注销失败");
        }
        return resultDTO;
    }

    /**
     * 列表查询
     *
     * @param sysUserVO
     * @return
     */
    @RequestMapping(value = "/listUser", method = RequestMethod.POST)
    public @ResponseBody
    ResultDTO<Object> listUser(@RequestBody SysUserVO sysUserVO, HttpSession session) {
        LOGGER.info("SysUserController.listUser param: {}", sysUserVO);
        ResultDTO<Object> resultDTO = new ResultDTO<>(true, null, "查询成功");
        try {
            SysUserBO sysUserBO = new SysUserBO();
            BeanUtils.copyProperties(sysUserVO, sysUserBO);
            resultDTO.setData(sysUserService.listUser(sysUserBO));
        } catch (BusinessException e) {
            LOGGER.error("SysUserController.listUser businessException: {}", e);
            resultDTO.setMessage(e.getMessage());
            resultDTO.setSuccess(false);
        } catch (Exception e) {
            LOGGER.error("SysUserController.listUser error: {}", e);
            resultDTO.setMessage("查询失败");
            resultDTO.setSuccess(false);
        }
        return resultDTO;
    }

    /**
     * 新增用户
     *
     * @param sysUserVO
     * @return
     */
    @RequestMapping(value = "/addSysUser", method = RequestMethod.POST)
    public @ResponseBody
    ResultDTO<Object> addSysUser(@RequestBody SysUserVO sysUserVO) {
        LOGGER.info("SysUserController.addSysUser param: {}", sysUserVO);
        ResultDTO<Object> resultDTO = new ResultDTO<>(true, null, "新增用户成功");
        try {
            SysUserBO sysUserBO = new SysUserBO();
            BeanUtils.copyProperties(sysUserVO, sysUserBO);
            sysUserService.addSysUser(sysUserBO);
        } catch (BusinessException e) {
            LOGGER.error("SysUserController.addSysUser businessException: {}", e);
            resultDTO.setMessage(e.getMessage());
            resultDTO.setSuccess(false);
        } catch (Exception e) {
            LOGGER.error("SysUserController.addSysUser error: {}", e);
            resultDTO.setMessage("新增用户失败");
            resultDTO.setSuccess(false);
        }
        return resultDTO;
    }

    /**
     * 修改用户
     *
     * @param sysUserVO
     * @return
     */
    @RequestMapping(value = "/updateSysUser", method = RequestMethod.POST)
    public @ResponseBody
    ResultDTO<Object> updateSysUser(@RequestBody SysUserVO sysUserVO) {
        LOGGER.info("SysUserController.updateSysUser param: {}", sysUserVO);
        ResultDTO<Object> resultDTO = new ResultDTO<>(true, null, "修改用户成功");
        try {
            SysUserBO sysUserBO = new SysUserBO();
            BeanUtils.copyProperties(sysUserVO, sysUserBO);
            sysUserService.updateSysUser(sysUserBO);
        } catch (BusinessException e) {
            LOGGER.error("SysUserController.updateSysUser businessException: {}", e);
            resultDTO.setMessage(e.getMessage());
            resultDTO.setSuccess(false);
        } catch (Exception e) {
            LOGGER.error("SysUserController.updateSysUser error: {}", e);
            resultDTO.setMessage("修改用户失败");
            resultDTO.setSuccess(false);
        }
        return resultDTO;
    }

    /**
     * 删除用户
     *
     * @param sysUserVO
     * @return
     */
    @RequestMapping(value = "/deleteSysUser", method = RequestMethod.POST)
    public @ResponseBody
    ResultDTO<Object> deleteSysUser(@RequestBody SysUserVO sysUserVO) {
        LOGGER.info("SysUserController.deleteSysUser param: {}", sysUserVO);
        ResultDTO<Object> resultDTO = new ResultDTO<>(true, null, "删除用户成功");
        try {
            SysUserBO sysUserBO = new SysUserBO();
            BeanUtils.copyProperties(sysUserVO, sysUserBO);
            sysUserService.deleteSysUser(sysUserBO);
        } catch (BusinessException e) {
            LOGGER.error("SysUserController.deleteSysUser businessException: {}", e);
            resultDTO.setMessage(e.getMessage());
            resultDTO.setSuccess(false);
        } catch (Exception e) {
            LOGGER.error("SysUserController.deleteSysUser error: {}", e);
            resultDTO.setMessage("删除用户失败");
            resultDTO.setSuccess(false);
        }
        return resultDTO;
    }

    /**
     * 根据iD获取用户信息和角色 - 用于编辑
     *
     * @param id
     * @return
     */
    @RequestMapping(value = "/findSysUserById/{id}", method = RequestMethod.GET)
    public @ResponseBody
    ResultDTO<Object> findSysUserById(@PathVariable Integer id) {
        LOGGER.info("SysUserController.findSysUserById param: {}", id);
        ResultDTO<Object> resultDTO = new ResultDTO<>(true, null, "获取用户成功");
        try {
            SysUserBO sysUserBO = new SysUserBO();
            sysUserBO.setId(id);
            resultDTO.setData(sysUserService.findSysUserById(sysUserBO));
        } catch (BusinessException e) {
            LOGGER.error("SysUserController.findSysUserById businessException: {}", e);
            resultDTO.setMessage(e.getMessage());
            resultDTO.setSuccess(false);
        } catch (Exception e) {
            LOGGER.error("SysUserController.findSysUserById error: {}", e);
            resultDTO.setMessage("获取用户失败");
            resultDTO.setSuccess(false);
        }
        return resultDTO;
    }

    /**
     * 获取仓库中所有司机
     */
    @RequestMapping(value = "/getUserByWarehouse", method = RequestMethod.GET)
    public @ResponseBody
    ResultDTO<Object> getUserByWarehouse(HttpServletRequest request) {
        LOGGER.info("SysUserController.getUserByWarehouse param: {}");
        ResultDTO<Object> resultDTO = new ResultDTO<>(true, null, "获取用户成功");
        try {
            // 获取登录人所属仓库code
            String whCode = request.getHeader("whCode");
            SysUserBO sysUserBO = new SysUserBO();
            Byte type = new Byte("0");
            sysUserBO.setType(type);
            sysUserBO.setWarehouseCode(whCode);
            resultDTO = new ResultDTO<>(true, sysUserService.getUserByWarehouse(sysUserBO), "成功获取仓库中的所有司机");
        } catch (BusinessException e) {
            LOGGER.error("SysUserController.getUserByWarehouse businessException: {}", e);
            resultDTO.setMessage(e.getMessage());
            resultDTO.setSuccess(false);
        } catch (Exception e) {
            LOGGER.error("SysUserController.getUserByWarehouse error: {}", e);
            resultDTO.setMessage("获取仓库中所有司机失败");
            resultDTO.setSuccess(false);
        }
        return resultDTO;
    }
}
