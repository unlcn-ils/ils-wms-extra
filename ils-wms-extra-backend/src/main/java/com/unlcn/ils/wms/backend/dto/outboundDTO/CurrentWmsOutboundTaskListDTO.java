package com.unlcn.ils.wms.backend.dto.outboundDTO;

import java.io.Serializable;
import java.util.Date;

public class CurrentWmsOutboundTaskListDTO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private Long otId;

	private String otCode;

	private String otVin;

	private String otStatus;

	private Date gmtCreate;

	private Date gmtModify;

	private String otLocationCode;

	private String otLcoationName;

	private String otEstimateLane;

	private String otPreparationMaterialNo;

	public Long getOtId() {
		return otId;
	}

	public void setOtId(Long otId) {
		this.otId = otId;
	}

	public String getOtCode() {
		return otCode;
	}

	public void setOtCode(String otCode) {
		this.otCode = otCode;
	}

	public String getOtVin() {
		return otVin;
	}

	public void setOtVin(String otVin) {
		this.otVin = otVin;
	}

	public String getOtStatus() {
		return otStatus;
	}

	public void setOtStatus(String otStatus) {
		this.otStatus = otStatus;
	}

	public Date getGmtCreate() {
		return gmtCreate;
	}

	public void setGmtCreate(Date gmtCreate) {
		this.gmtCreate = gmtCreate;
	}

	public Date getGmtModify() {
		return gmtModify;
	}

	public void setGmtModify(Date gmtModify) {
		this.gmtModify = gmtModify;
	}

	public String getOtLocationCode() {
		return otLocationCode;
	}

	public void setOtLocationCode(String otLocationCode) {
		this.otLocationCode = otLocationCode;
	}

	public String getOtLcoationName() {
		return otLcoationName;
	}

	public void setOtLcoationName(String otLcoationName) {
		this.otLcoationName = otLcoationName;
	}

	public String getOtEstimateLane() {
		return otEstimateLane;
	}

	public void setOtEstimateLane(String otEstimateLane) {
		this.otEstimateLane = otEstimateLane;
	}

	public String getOtPreparationMaterialNo() {
		return otPreparationMaterialNo;
	}

	public void setOtPreparationMaterialNo(String otPreparationMaterialNo) {
		this.otPreparationMaterialNo = otPreparationMaterialNo;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("CurrentWmsOutboundTaskListDTO [otId=");
		builder.append(otId);
		builder.append(", otCode=");
		builder.append(otCode);
		builder.append(", otVin=");
		builder.append(otVin);
		builder.append(", otStatus=");
		builder.append(otStatus);
		builder.append(", gmtCreate=");
		builder.append(gmtCreate);
		builder.append(", gmtModify=");
		builder.append(gmtModify);
		builder.append(", otLocationCode=");
		builder.append(otLocationCode);
		builder.append(", otLcoationName=");
		builder.append(otLcoationName);
		builder.append(", otEstimateLane=");
		builder.append(otEstimateLane);
		builder.append(", otPreparationMaterialNo=");
		builder.append(otPreparationMaterialNo);
		builder.append("]");
		return builder.toString();
	}
}