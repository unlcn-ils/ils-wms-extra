package com.unlcn.ils.wms.web.controller.repair;

import cn.huiyunche.commons.domain.ResultDTO;
import cn.huiyunche.commons.exception.BusinessException;
import com.unlcn.ils.wms.backend.bo.inboundBO.WmsRepairBO;
import com.unlcn.ils.wms.backend.enums.WmsRepairStatusEnum;
import com.unlcn.ils.wms.backend.service.inbound.WmsRepairService;
import com.unlcn.ils.wms.base.businessDTO.inbound.WmsRepairQueryDTO;
import com.unlcn.ils.wms.web.vo.repair.WmsRepairVO;
import jodd.util.StringUtil;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Objects;

/**
 * 维修单管理：维修单修改、删除操作
 */
@Controller
@RequestMapping("/wmsRepair")
@ResponseBody
public class WmsRepairController {
    private Logger LOGGER = LoggerFactory.getLogger(WmsRepairController.class);
    @Autowired
    private WmsRepairService wmsRepairService;


    //时间类型转换
    @InitBinder
    public void initBinder(WebDataBinder binder) {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        binder.registerCustomEditor(Date.class, new CustomDateEditor(sdf, true));
    }

    /**
     * 查询维修单
     *
     * @param wmsRepairVO
     * @param wmsRepairVO
     * @return
     */
    @RequestMapping(value = "/list", method = RequestMethod.POST)
    public ResultDTO list(@RequestBody WmsRepairVO wmsRepairVO, HttpServletRequest request) {
        LOGGER.info("WmsRepairController.getRepairList param: {}", wmsRepairVO);
        ResultDTO<Object> resultDTO = new ResultDTO<>(true, null, "查询成功");
        try {
            String whCode = request.getHeader("whCode");
            WmsRepairQueryDTO wmsRepairQueryDTO = new WmsRepairQueryDTO();
            BeanUtils.copyProperties(wmsRepairVO, wmsRepairQueryDTO);
            wmsRepairQueryDTO.setRpWhCode(whCode);
            resultDTO.setData(wmsRepairService.getListWmsRepairBO(wmsRepairQueryDTO));
        } catch (BusinessException e) {
            LOGGER.error("WmsRepairController.list error: {}", e);
            resultDTO.setData(null);
            resultDTO.setMessage(e.getMessage());
            resultDTO.setSuccess(false);
        } catch (Exception e) {
            LOGGER.error("WmsRepairController.list error: {}", e);
            resultDTO.setData(null);
            resultDTO.setMessage("查询异常");
            resultDTO.setSuccess(false);
        }
        return resultDTO;
    }


    /**
     * 提交维修单
     *
     * @param wmsRepairVO
     * @return
     */
    @RequestMapping(value = "/submitRepair", method = RequestMethod.POST)
    public ResultDTO submitRepair(@RequestBody WmsRepairVO wmsRepairVO, HttpServletRequest request) {
        LOGGER.info("WmsRepairController.updateSubmitRepair param: {}", wmsRepairVO);
        ResultDTO resultDTO = new ResultDTO();
        try {
            WmsRepairBO bo = new WmsRepairBO();
            BeanUtils.copyProperties(wmsRepairVO, bo);
            String whCode = request.getHeader("whCode");
            String userId = request.getHeader("userId");
            wmsRepairService.updateSubmitRepair(bo, whCode, userId);
            resultDTO.setSuccess(true);
            resultDTO.setMessage("提交维修单成功");
            return resultDTO;
        } catch (BusinessException ex) {
            LOGGER.error("WmsRepairController.updateSubmitRepair error: {}", ex);
            resultDTO.setSuccess(false);
            resultDTO.setMessage(ex.getMessage());
            return resultDTO;
        } catch (Exception e) {
            LOGGER.error("WmsRepairController.updateSubmitRepair error: {}", e);
            resultDTO.setSuccess(false);
            resultDTO.setMessage("提交维修单异常");
            return resultDTO;
        }
    }

    /**
     * 打印维修单
     *
     * @param wmsRepairVO
     * @return
     */
    @RequestMapping(value = "/printRepair", method = RequestMethod.POST)
    public ResultDTO printRepair(@RequestBody WmsRepairVO wmsRepairVO, HttpServletRequest request) {
        LOGGER.info("WmsRepairController.printRepair param: {}", wmsRepairVO);
        ResultDTO<HashMap<String, Object>> resultDTO = new ResultDTO<>();
        try {
            WmsRepairBO bo = new WmsRepairBO();
            BeanUtils.copyProperties(wmsRepairVO, bo);
            String whCode = request.getHeader("whCode");
            String userId = request.getHeader("userId");
            resultDTO.setData(wmsRepairService.updatePrintRepair(bo, whCode, userId));
            resultDTO.setSuccess(true);
            resultDTO.setMessage("打印维修单成功");
            return resultDTO;
        } catch (BusinessException ex) {
            LOGGER.error("WmsRepairController.printRepair error: {}", ex);
            resultDTO.setSuccess(false);
            resultDTO.setMessage(ex.getMessage());
            return resultDTO;
        } catch (Exception e) {
            LOGGER.error("WmsRepairController.printRepair error: {}", e);
            resultDTO.setSuccess(false);
            resultDTO.setMessage("打印维修单异常");
            return resultDTO;
        }
    }


    /**
     * 新增维修单
     *
     * @param wmsRepairVO
     * @return
     */
    @RequestMapping(value = "/addWmsRepair", method = RequestMethod.POST)
    public ResultDTO addWmsRepair(@RequestBody WmsRepairVO wmsRepairVO, HttpServletRequest request) {
        LOGGER.info("WmsRepairController.addWmsRepair wmsRepairVO: {}", wmsRepairVO);
        if (Objects.equals(wmsRepairVO, null)) {
            LOGGER.error("==============WmsRepairController.addWmsRepair参数为空=================");
            throw new BusinessException("维修单参数为空");
        }
        ResultDTO<Object> resultDTO = new ResultDTO<Object>(true, null, "查询成功");
        try {
            String whCode = request.getHeader("whCode");
            if (StringUtils.isBlank(whCode)) {
                resultDTO.setSuccess(false);
                resultDTO.setMessage("当前登录用户未绑定车库");
                return resultDTO;
            }
            wmsRepairVO.setIsDeleted((byte) 1);
            wmsRepairVO.setRpStatus(String.valueOf(WmsRepairStatusEnum.WMS_REPAIR_STATUS_NOT_HANDLE.getValue()));
            WmsRepairBO wmsRepairBO = new WmsRepairBO();
            BeanUtils.copyProperties(wmsRepairVO, wmsRepairBO);
            wmsRepairBO.setRpWhCode(whCode);
            wmsRepairService.addWmsRepair(wmsRepairBO);
            resultDTO.setSuccess(true);
            resultDTO.setMessage("新增成功");
            return resultDTO;
        } catch (Exception e) {
            LOGGER.error("WmsRepairController.addWmsRepair error: {}", e);
            resultDTO.setSuccess(false);
            resultDTO.setMessage("新增异常");
            return resultDTO;
        }
    }

    /**
     * 修改维修单
     *
     * @param wmsRepairVO
     * @return
     */
    @RequestMapping(value = "/updateWmsRepair", method = RequestMethod.POST)
    public ResultDTO updateWmsRepair(@RequestBody WmsRepairVO wmsRepairVO) {
        LOGGER.info("WmsRepairController.addWmsRepair wmsRepairVO: {}", wmsRepairVO);
        if (Objects.equals(wmsRepairVO, null)) {
            LOGGER.error("WmsRepairController.updateWmsRepair error: {} 参数为空");
        }
        ResultDTO<Object> resultDTO = new ResultDTO<Object>(true, null, "查询成功");
        try {
            WmsRepairBO wmsRepairBO = new WmsRepairBO();
            BeanUtils.copyProperties(wmsRepairVO, wmsRepairBO);
            boolean result = wmsRepairService.updateWmsRepair(wmsRepairBO);
            if (result) {
                resultDTO.setMessage("修改维修单成功");
                resultDTO.setSuccess(true);
                resultDTO.setMessageCode("200");
                return resultDTO;
            } else {
                resultDTO.setMessage("修改维修单失败");
                resultDTO.setSuccess(false);
                resultDTO.setMessageCode("500");
                return resultDTO;
            }
        } catch (Exception e) {
            LOGGER.error("AsnOrderController.updateWmsRepair error: {}", e);
            throw new BusinessException(e);
        }
    }


    @RequestMapping(value = "/getRepairById/{rpId}")
    public ResultDTO getRepairById(@PathVariable("rpId") String rpId) {
        if (StringUtil.isEmpty(rpId)) {
            LOGGER.error("WmsRepairController.getRepairById error: {} 参数为空");
        }
        ResultDTO resultDTO = new ResultDTO();
        try {
            resultDTO.setData(wmsRepairService.getRepairById(rpId));
            resultDTO.setSuccess(true);
            resultDTO.setMessage("查询维修单成功");
            return resultDTO;
        } catch (BusinessException ex) {
            LOGGER.error("WmsRepairController.getRepairById error: {}", ex);
            resultDTO.setSuccess(false);
            resultDTO.setMessage("查询维修单异常");
            return resultDTO;
        }
    }

    /**
     * 维修后检验
     *
     * @param wmsRepairVO
     * @return
     */
    @RequestMapping(value = "/submitAfterCheckCopyAndAdd", method = RequestMethod.POST)
    public ResultDTO submitAfterCheckCopyAndAdd(@RequestBody WmsRepairVO wmsRepairVO) {
        if (Objects.equals(wmsRepairVO, null)) {
            LOGGER.error("WmsRepairController.submitAfterCheckCopyAndAdd error: {} 参数为空");
        }
        ResultDTO resultDTO = new ResultDTO();
        try {
            WmsRepairBO wmsRepairBO = new WmsRepairBO();
            BeanUtils.copyProperties(wmsRepairVO, wmsRepairBO);
            wmsRepairService.submitAfterCheckCopyAndAdd(wmsRepairBO);
            resultDTO.setSuccess(true);
            resultDTO.setMessage("提交维修后检查生成成功");
            return resultDTO;
        } catch (BusinessException ex) {
            LOGGER.error("WmsRepairController.submitAfterCheckCopyAndAdd error: {}", ex);
            resultDTO.setSuccess(false);
            resultDTO.setMessage("提交维修后检查生成异常");
            return resultDTO;
        }
    }

    /**
     * 维修后检验
     *
     * @param wmsRepairVO
     * @return
     */
    @RequestMapping(value = "/submitAfterCheckAdd", method = RequestMethod.POST)
    public ResultDTO submitAfterCheckAdd(@RequestBody WmsRepairVO wmsRepairVO) {
        if (Objects.equals(wmsRepairVO, null)) {
            LOGGER.error("WmsRepairController.submitAfterCheck error: {} 参数为空");
        }
        ResultDTO resultDTO = new ResultDTO();
        try {
            WmsRepairBO wmsRepairBO = new WmsRepairBO();
            BeanUtils.copyProperties(wmsRepairVO, wmsRepairBO);
            wmsRepairService.submitAfterCheck(wmsRepairBO);
            resultDTO.setSuccess(true);
            resultDTO.setMessage("提交维修后检查成功");
            return resultDTO;
        } catch (BusinessException ex) {
            LOGGER.error("WmsRepairController.submitAfterCheck error: {}", ex);
            resultDTO.setSuccess(false);
            resultDTO.setMessage("提交维修后检查异常");
            return resultDTO;
        }
    }

}
