package com.unlcn.ils.wms.base.mapper.extmapper;

import com.unlcn.ils.wms.base.model.outbound.WmsDepartureRegister;

import java.util.HashMap;
import java.util.List;

public interface WmsDepartureRegisterExtMapper {


    List<WmsDepartureRegister> selectListByParam(HashMap<String, Object> paramMap);

    int selectListCountByParam(HashMap<String, Object> paramMap);

    List<WmsDepartureRegister> selectAllRecords(HashMap<String, Object> paramMap);

    List<WmsDepartureRegister> selectTodayData(HashMap<String, Object> paramMap);

    List<WmsDepartureRegister> selectGateOutDataByParam(HashMap<String, Object> params);

    List<WmsDepartureRegister> selectDepartureOutTime(HashMap<String, Object> paramMap);
}