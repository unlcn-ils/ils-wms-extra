package com.unlcn.ils.wms.web.controller.inventoryverfication;

import cn.huiyunche.commons.domain.ResultDTO;
import cn.huiyunche.commons.domain.ResultDTOWithPagination;
import cn.huiyunche.commons.exception.BusinessException;
import com.unlcn.ils.wms.backend.service.inventorymanager.WmsInventoryVerificationService;
import com.unlcn.ils.wms.base.businessDTO.outbound.WmsOutboundTaskDTOForQuery;
import com.unlcn.ils.wms.base.dto.WmsInventoryManagerParamDTO;
import com.unlcn.ils.wms.base.dto.WmsInventoryManagerResultDTO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;
import java.util.Map;

/**
 * <p>
 * 2018-3-28 新需求:WMS 的入库记录/出库记录/库存记录查询(可能含所有仓库数据)
 * </p>
 */
@Controller
@ResponseBody
@RequestMapping("/inventoryManager")
public class WmsInventoryVerificationController {
    private Logger logger = LoggerFactory.getLogger(getClass());

    @Autowired
    private WmsInventoryVerificationService wmsInventoryManagerService;

    /**
     * 入库记录查询
     *
     * @param dto     参数封装
     * @param request 请求体
     * @return 返回值
     */
    @RequestMapping(value = "/getInboundManageList", method = RequestMethod.POST)
    public ResultDTOWithPagination<Object> inboundRecord(@RequestBody WmsInventoryManagerParamDTO dto, HttpServletRequest request) {
        if (logger.isInfoEnabled()) {
            logger.info("WmsInventoryVerificationController.getInboundRecord params: {}", dto);
        }
        ResultDTOWithPagination<Object> resultDTO = new ResultDTOWithPagination<>(true, null, "查询入库记录成功");
        try {
            String userId = request.getHeader("userId");
            dto.setUserId(userId);
            ResultDTOWithPagination<List<Map<String, Object>>> data = wmsInventoryManagerService.getInboundManageRecordsByCustomer(dto);
            resultDTO.setData(data.getData());
            resultDTO.setPageVo(data.getPageVo());
        } catch (BusinessException be) {
            logger.error("WmsInventoryVerificationController.getInboundRecord  BusinessException: {}", be);
            resultDTO.setSuccess(false);
            resultDTO.setMessage(be.getMessage());
        } catch (Exception ex) {
            logger.error("WmsInventoryVerificationController.getInboundRecord error: {}", ex);
            resultDTO.setMessage("查询入库记录异常");
            resultDTO.setSuccess(false);
        }
        return resultDTO;
    }

    /**
     * 入库记录导出
     *
     * @param dto     参数封装
     * @param request 请求体
     * @return 返回值
     */
    @RequestMapping(value = "/getInboundManageExport", method = RequestMethod.GET)
    public ResultDTO<Object> inboundRecordExport(WmsInventoryManagerParamDTO dto,
                                                 HttpServletRequest request,
                                                 HttpServletResponse response) {
        if (logger.isInfoEnabled()) {
            logger.info("WmsInventoryVerificationController.getInboundManageExport params: {}", dto);
        }
        ResultDTO<Object> resultDTO = new ResultDTO<>(true, null, "导出记录成功");
        try {
            wmsInventoryManagerService.getInboundManageRecordsByCustomerAndExport(dto, request, response);
        } catch (BusinessException be) {
            logger.error("WmsInventoryVerificationController.getInboundManageExport  BusinessException: {}", be);
            resultDTO.setSuccess(false);
            resultDTO.setMessage(be.getMessage());
        } catch (IOException ie) {
            logger.error("WmsInventoryVerificationController.getInboundManageExport IOException: {}", ie);
            resultDTO.setMessage(ie.getMessage());
            resultDTO.setSuccess(false);
        } catch (Exception ex) {
            logger.error("WmsInventoryVerificationController.getInboundManageExport error: {}", ex);
            resultDTO.setMessage("导出记录异常");
            resultDTO.setSuccess(false);
        }
        return resultDTO;
    }


    /**
     * 出库记录查询
     *
     * @param dto     参数封装
     * @param request 请求体
     * @return 返回值
     */
    @RequestMapping(value = "/getOutboundManageList", method = RequestMethod.POST)
    public ResultDTOWithPagination<Object> getOutboundRecord(@RequestBody WmsInventoryManagerParamDTO dto, HttpServletRequest request) {
        if (logger.isInfoEnabled()) {
            logger.info("WmsInventoryVerificationController.getOutboundRecord params: {}", dto);
        }
        ResultDTOWithPagination<Object> resultDTO = new ResultDTOWithPagination<>(true, null, "查询入库记录成功");
        try {
            String userId = request.getHeader("userId");
            dto.setUserId(userId);
            ResultDTOWithPagination<List<WmsOutboundTaskDTOForQuery>> result = wmsInventoryManagerService.getOutboundManageRecordsByCustomer(dto);
            resultDTO.setData(result.getData());
            resultDTO.setPageVo(result.getPageVo());
        } catch (BusinessException be) {
            logger.error("WmsInventoryVerificationController.getOutboundRecord  BusinessException: {}", be);
            resultDTO.setSuccess(false);
            resultDTO.setMessage(be.getMessage());
        } catch (Exception ex) {
            logger.error("WmsInventoryVerificationController.getOutboundRecord error: {}", ex);
            resultDTO.setMessage("查询入库记录异常");
            resultDTO.setSuccess(false);
        }
        return resultDTO;
    }

    /**
     * 出库记录导出
     *
     * @param dto     参数封装
     * @param request 请求体
     * @return 返回值
     */
    @RequestMapping(value = "/getOutboundManageExport", method = RequestMethod.GET)
    public ResultDTO<Object> outboundRecordExport(WmsInventoryManagerParamDTO dto,
                                                  HttpServletRequest request,
                                                  HttpServletResponse response) {
        if (logger.isInfoEnabled()) {
            logger.info("WmsInventoryVerificationController.getOutboundManageExport params: {}", dto);
        }
        ResultDTO<Object> resultDTO = new ResultDTO<>(true, null, "导出记录成功");
        try {
            wmsInventoryManagerService.getOutboundManageRecordsByCustomerAndExport(dto, request, response);
        } catch (BusinessException be) {
            logger.error("WmsInventoryVerificationController.getOutboundManageExport  BusinessException: {}", be);
            resultDTO.setSuccess(false);
            resultDTO.setMessage(be.getMessage());
        } catch (IOException ie) {
            logger.error("WmsInventoryVerificationController.getOutboundManageExport IOException: {}", ie);
            resultDTO.setMessage(ie.getMessage());
            resultDTO.setSuccess(false);
        } catch (Exception ex) {
            logger.error("WmsInventoryVerificationController.getOutboundManageExport error: {}", ex);
            resultDTO.setMessage("导出记录异常");
            resultDTO.setSuccess(false);
        }
        return resultDTO;
    }

    /**
     * 库存记录查询
     *
     * @param dto     参数封装
     * @param request 请求体
     * @return 返回值
     */
    @RequestMapping(value = "/getInventoryManageList", method = RequestMethod.POST)
    public ResultDTOWithPagination<Object> getInventoryRecord(@RequestBody WmsInventoryManagerParamDTO dto, HttpServletRequest request) {
        if (logger.isInfoEnabled()) {
            logger.info("WmsInventoryVerificationController.getInventoryRecord params: {}", dto);
        }
        ResultDTOWithPagination<Object> resultDTO = new ResultDTOWithPagination<>(true, null, "查询入库记录成功");
        try {
            String userId = request.getHeader("userId");
            dto.setUserId(userId);
            ResultDTOWithPagination<List<WmsInventoryManagerResultDTO>> result = wmsInventoryManagerService.getInventoryManageRecordsByCustomer(dto);
            resultDTO.setData(result.getData());
            resultDTO.setPageVo(result.getPageVo());
        } catch (BusinessException be) {
            logger.error("WmsInventoryVerificationController.getInventoryRecord  BusinessException: {}", be);
            resultDTO.setSuccess(false);
            resultDTO.setMessage(be.getMessage());
        } catch (Exception ex) {
            logger.error("WmsInventoryVerificationController.getInventoryRecord error: {}", ex);
            resultDTO.setMessage("查询入库记录异常");
            resultDTO.setSuccess(false);
        }
        return resultDTO;
    }

    /**
     * 库存记录导出
     *
     * @param dto     参数封装
     * @param request 请求体
     * @return 返回值
     */
    @RequestMapping(value = "/getInventoryManageExport", method = RequestMethod.GET)
    public ResultDTO<Object> inventoryRecordExport(WmsInventoryManagerParamDTO dto,
                                                   HttpServletRequest request,
                                                   HttpServletResponse response) {
        if (logger.isInfoEnabled()) {
            logger.info("WmsInventoryVerificationController.getInventoryManageExport params: {}", dto);
        }
        ResultDTO<Object> resultDTO = new ResultDTO<>(true, null, "导出记录成功");
        try {
            wmsInventoryManagerService.getInventoryManageRecordsByCustomerAndExport(dto, request, response);
        } catch (BusinessException be) {
            logger.error("WmsInventoryVerificationController.getInventoryManageExport  BusinessException: {}", be);
            resultDTO.setSuccess(false);
            resultDTO.setMessage(be.getMessage());
        } catch (IOException ie) {
            logger.error("WmsInventoryVerificationController.getInventoryManageExport IOException: {}", ie);
            resultDTO.setMessage(ie.getMessage());
            resultDTO.setSuccess(false);
        } catch (Exception ex) {
            logger.error("WmsInventoryVerificationController.getInventoryManageExport error: {}", ex);
            resultDTO.setMessage("导出记录异常");
            resultDTO.setSuccess(false);
        }
        return resultDTO;
    }


}
