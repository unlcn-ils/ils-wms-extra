package com.unlcn.ils.wms.backend.bo.sys;

import java.util.Date;
import java.util.List;

public class PermissionsBO {
	
	//id
	private Integer id;
	//权限名
	private String name;
	//父级id
	private Integer parentId;
	//级别
	private String level;
	//可用状态
	private String enable;
	//创建人
	private String createPerson;
	//修改人
	private String updatePerson;
	//创建时间
	private Date gmtCreate;
	//修改时间
	private Date gmtModified;
	//下级权限
	private List<PermissionsBO> childPermissionsBOList;
	
	/**
	 * @return the id
	 */
	public Integer getId() {
		return id;
	}
	/**
	 * @param id the id to set
	 */
	public void setId(Integer id) {
		this.id = id;
	}
	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}
	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}
	/**
	 * @return the parentId
	 */
	public Integer getParentId() {
		return parentId;
	}
	/**
	 * @param parentId the parentId to set
	 */
	public void setParentId(Integer parentId) {
		this.parentId = parentId;
	}
	/**
	 * @return the level
	 */
	public String getLevel() {
		return level;
	}
	/**
	 * @param level the level to set
	 */
	public void setLevel(String level) {
		this.level = level;
	}
	/**
	 * @return the enable
	 */
	public String getEnable() {
		return enable;
	}
	/**
	 * @param enable the enable to set
	 */
	public void setEnable(String enable) {
		this.enable = enable;
	}
	/**
	 * @return the createPerson
	 */
	public String getCreatePerson() {
		return createPerson;
	}
	/**
	 * @param createPerson the createPerson to set
	 */
	public void setCreatePerson(String createPerson) {
		this.createPerson = createPerson;
	}
	/**
	 * @return the updatePerson
	 */
	public String getUpdatePerson() {
		return updatePerson;
	}
	/**
	 * @param updatePerson the updatePerson to set
	 */
	public void setUpdatePerson(String updatePerson) {
		this.updatePerson = updatePerson;
	}
	/**
	 * @return the gmtCreate
	 */
	public Date getGmtCreate() {
		return gmtCreate;
	}
	/**
	 * @param gmtCreate the gmtCreate to set
	 */
	public void setGmtCreate(Date gmtCreate) {
		this.gmtCreate = gmtCreate;
	}
	/**
	 * @return the gmtModified
	 */
	public Date getGmtModified() {
		return gmtModified;
	}
	/**
	 * @param gmtModified the gmtModified to set
	 */
	public void setGmtModified(Date gmtModified) {
		this.gmtModified = gmtModified;
	}
	
	/**
	 * @return the childPermissionsBOList
	 */
	public List<PermissionsBO> getChildPermissionsBOList() {
		return childPermissionsBOList;
	}
	/**
	 * @param childPermissionsBOList the childPermissionsBOList to set
	 */
	public void setChildPermissionsBOList(List<PermissionsBO> childPermissionsBOList) {
		this.childPermissionsBOList = childPermissionsBOList;
	}
	
	/*
	 * Title: toString
	 * Description: toString
	 * @return
	 * @see java.lang.Object#toString()
	 *
	 */
	@Override
	public String toString() {
		return "PermissionsBO [id=" + id + ", name=" + name + ", parentId=" + parentId + ", level=" + level
				+ ", enable=" + enable + ", createPerson=" + createPerson + ", updatePerson=" + updatePerson
				+ ", gmtCreate=" + gmtCreate + ", gmtModified=" + gmtModified + "]";
	}
	
}
