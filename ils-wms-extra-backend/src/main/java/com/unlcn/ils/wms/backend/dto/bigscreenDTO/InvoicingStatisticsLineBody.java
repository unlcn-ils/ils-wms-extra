package com.unlcn.ils.wms.backend.dto.bigscreenDTO;

import java.util.Arrays;

/**
 * Created by lenovo on 2017/11/8.
 */
public class InvoicingStatisticsLineBody {

    private String name;

    private Integer[] data;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer[] getData() {
        return data;
    }

    public void setData(Integer[] data) {
        this.data = data;
    }

    @Override
    public String toString() {
        return "InvoicingStatisticsLineBody{" +
                "name='" + name + '\'' +
                ", data=" + Arrays.toString(data) +
                '}';
    }
}
