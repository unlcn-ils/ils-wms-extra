package com.unlcn.ils.wms.api.outbound.quartz;


import cn.huiyunche.commons.exception.BusinessException;
import com.unlcn.ils.wms.backend.service.inbound.WmsJMInboundService;
import com.unlcn.ils.wms.backend.service.outbound.OutBoundTaskService;
import com.unlcn.ils.wms.backend.service.outbound.WmsDealerCarBackService;
import com.unlcn.ils.wms.backend.service.outbound.WmsShipmentPlanService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * 定时任务抓取tms发运计划数据
 */
@Component("myJobDetail")
public class QuartzJobDetail {

    private Logger logger = LoggerFactory.getLogger(QuartzJobDetail.class);

    @Autowired
    private WmsShipmentPlanService wmsShipmentPlanService;

    @Autowired
    private WmsJMInboundService wmsJMInboundService;

    @Autowired
    private OutBoundTaskService outBoundTaskService;

    @Autowired
    private WmsDealerCarBackService wmsDealerCarBackService;


    public void fetchTmsShipmentPlan() {
        logger.info("QuartzJobDetail.fetchTmsShipmentPlan start:=====");
        try {
            wmsShipmentPlanService.fetchShipmentPlanListFromTMS();
        } catch (BusinessException e) {
            logger.error("QuartzJobDetail.fetchShipmentPlanListFromTMS BusinessException:", e);
        } catch (Exception e) {
            logger.error("QuartzJobDetail.fetchShipmentPlanListFromTMS error:", e);
        }
    }

    /**
     * 定时任务发送出入库的数据,只发送一次  失败的时候往出入库接口异常表记录 再定时任务3调用5次
     */
    public void updateOutOfStorage() {
        logger.info("QuartzJobDetail.updateOutOfStorage start:=====");
        try {
            //查询出入库接口表数据--两个事务中处理减少重复数据
            wmsJMInboundService.updateOutOfStorageDcs();
        } catch (BusinessException e) {
            logger.error("QuartzJobDetail.updateOutOfStorageDcs BusinessException:", e);
        } catch (Exception ex) {
            logger.error("QuartzJobDetail.updateOutOfStorageDcs error:", ex);
        }
        //2018-9-26  取消入库发送SAP
        //2018-10-9  发送SAP
        try {
            wmsJMInboundService.updateOutOfStorageSap();
        } catch (BusinessException e) {
            logger.error("QuartzJobDetail.updateOutOfStorageSap BusinessException:", e);
        } catch (Exception ex) {
            logger.error("QuartzJobDetail.updateOutOfStorageSap error:", ex);
        }
    }

    /**
     * 失败的时候往出入库接口异常表记录 再定时任务调用接口同步数据调用5次
     */
    public void updateOutOfStorageExcp() {
        logger.info("QuartzJobDetail.updateOutOfStorageExcp start=========");
        //dcs
        try {
            //System.err.print("------------------"+(++i)+"--------------");
            wmsJMInboundService.updateOutOfStorageExcpForDcs();
        } catch (BusinessException e) {
            logger.error("QuartzJobDetail.updateOutOfStorageExcpForDcs BusinessException:", e);
        } catch (Exception ex) {
            logger.error("QuartzJobDetail.updateOutOfStorageExcpForDcs error:", ex);
        }
        //2018-9-26  取消入库发送SAP
        //2018-10-9  发送SAP
        try {
            //System.err.print("------------------"+(++i)+"--------------");
            wmsJMInboundService.updateOutOfStorageExcpForSap();
        } catch (BusinessException e) {
            logger.error("QuartzJobDetail.updateOutOfStorageExcpForSap BusinessException:", e);
        } catch (Exception ex) {
            logger.error("QuartzJobDetail.updateOutOfStorageExcpForSap error:", ex);
        }
    }


    /**
     * 定时任务发送交接单的数据,只发送一次  失败的时候往交接单接口异常表记录 再定时任务3调用5次
     */
    public void updateHandOverOrder() {
        logger.info("QuartzJobDetail.updateHandOverOrderDcs start=========");
        try {
            //查询出入库接口表数据
            outBoundTaskService.updateHandOverOrderDcs();
        } catch (BusinessException e) {
            logger.error("QuartzJobDetail.updateHandOverOrderDcs BusinessException:", e);
        } catch (Exception ex) {
            logger.error("QuartzJobDetail.updateHandOverOrderDcs error:", ex);
        }
        try {
            //查询出入库接口表数据
            outBoundTaskService.updateHandOverOrderSap();
        } catch (BusinessException e) {
            logger.error("QuartzJobDetail.updateHandOverOrderSap BusinessException:", e);
        } catch (Exception ex) {
            logger.error("QuartzJobDetail.updateHandOverOrderSap error:", ex);
        }
    }

    /**
     * 失败的时候往交接单接口异常表记录 再定时任务调用接口同步数据调用5次
     * 2018-3-1  新需求:交接单发运信息增加发送数据到君马crm系统(单条传输,使用定时间隔5分钟发送一次)
     */
    public void updateHandOverOrderExcp() {
        logger.info("QuartzJobDetail.updateHandOverOrderDcsExcp start=========");
        //CRM
        try {
            //避免同时发送异常和正常数据到crm,先判断是否存在未发送完成的异常数据
            outBoundTaskService.updateHandOverOrderToCRM();
        } catch (BusinessException e) {
            logger.error("QuartzJobDetail.updateHandOverOrderToCRM BusinessException:", e);
        } catch (Exception ex) {
            logger.error("QuartzJobDetail.updateHandOverOrderToCRM error:", ex);
        }
        //DCS
        try {
            outBoundTaskService.updateHandOverOrderDcsExcp();
        } catch (BusinessException e) {
            logger.error("QuartzJobDetail.updateHandOverOrderDcsExcp BusinessException:", e);
        } catch (Exception ex) {
            logger.error("QuartzJobDetail.updateHandOverOrderDcsExcp error:", ex);
        }
        //SAP
        try {
            outBoundTaskService.updateHandOverOrderSapExcp();
        } catch (BusinessException e) {
            logger.error("QuartzJobDetail.updateHandOverOrderSapExcp BusinessException:", e);
        } catch (Exception ex) {
            logger.error("QuartzJobDetail.updateHandOverOrderSapExcp error:", ex);
        }
    }


    /**
     * 发运计划驳回接口--对接dcs,只发送一次  失败的时候往交接单接口异常表记录 再定时任务3调用5次
     */
    public void updateShipmentCancel() {
        logger.info("QuartzJobDetail.updateShipmentCancel start=========");
        try {
            //查询出入库接口表数据
            wmsShipmentPlanService.updateShipmentCancel();
        } catch (BusinessException e) {
            logger.error("QuartzJobDetail.updateShipmentCancel BusinessException:", e);
        } catch (Exception ex) {
            logger.error("QuartzJobDetail.updateShipmentCancel error:", ex);
        }
    }

    /**
     * 失败的时候往交接单接口异常表记录 再定时任务调用接口同步数据调用5次
     */
    public void updateShipmentCancelExcp() throws Exception {
        logger.info("QuartzJobDetail.updateShipmentCancelExcp start=========");
        try {
            //System.err.print("------------------"+(++i)+"--------------");
            wmsShipmentPlanService.updateShipmentCancleExcp();
        } catch (BusinessException e) {
            logger.error("QuartzJobDetail.updateShipmentCancelExcp BusinessException:", e);
        } catch (Exception ex) {
            logger.error("QuartzJobDetail.updateShipmentCancelExcp error:", ex);
        }
    }

    /**
     * 退车sap--,只发送一次  失败的时候往交接单接口异常表记录 再定时任务3调用5次
     */
    public void updateDelearCarBack() {
        logger.info("QuartzJobDetail.updateDelearCarBack start=========");
        try {
            //查询出入库接口表数据
            wmsDealerCarBackService.updateDelearCarBack();
        } catch (BusinessException e) {
            logger.error("QuartzJobDetail.updateDelearCarBack BusinessException:", e);
        } catch (Exception ex) {
            logger.error("QuartzJobDetail.updateDelearCarBack error:", ex);
        }
    }

    /**
     * 退车sap--,只发送一次  失败的时候往交接单接口异常表记录 再定时任务3调用5次
     */
    public void updateDelearCarBackExcp() {
        logger.info("QuartzJobDetail.updateDelearCarBackExcp start=========");
        try {
            //查询出入库接口表数据
            wmsDealerCarBackService.updateDelearCarBackExcp();
        } catch (BusinessException e) {
            logger.error("QuartzJobDetail.updateDelearCarBackExcp BusinessException:", e);
        } catch (Exception ex) {
            logger.error("QuartzJobDetail.updateDelearCarBackExcp error:", ex);
        }
    }

}
