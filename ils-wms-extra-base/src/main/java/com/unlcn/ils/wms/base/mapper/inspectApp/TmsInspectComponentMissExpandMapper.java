package com.unlcn.ils.wms.base.mapper.inspectApp;

import com.unlcn.ils.wms.base.dto.TmsInspectComponentMissDTO;
import org.apache.ibatis.annotations.Param;

import java.util.HashMap;
import java.util.List;

public interface TmsInspectComponentMissExpandMapper {


    List<TmsInspectComponentMissDTO> selectMissComponent(@Param("vin") String vin,@Param("del") int deletedValue);

    List<TmsInspectComponentMissDTO> selectNotMissComponent(@Param("vin")String vin,@Param("del")  int deletedValue);

    List<TmsInspectComponentMissDTO> selectMissComponentNew(@Param("inspectId") Long inspectId,@Param("del")  int deletedValue);

    List<TmsInspectComponentMissDTO> selectMissComponentListNew(HashMap<String, Object> params);

    List<TmsInspectComponentMissDTO> selectNotMissComponentNew(HashMap<String, Object> params);

}
