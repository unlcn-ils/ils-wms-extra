package com.unlcn.ils.wms.backend.service.outbound;

import cn.huiyunche.commons.domain.ResultDTOWithPagination;
import com.unlcn.ils.wms.backend.bo.outboundBO.CreatePreparationPlanBO;
import com.unlcn.ils.wms.backend.bo.outboundBO.GetShipmentPLanListBO;
import com.unlcn.ils.wms.backend.service.webservice.dto.ResultForDcsInorOutDetailsDTO;
import com.unlcn.ils.wms.backend.service.webservice.vo.WmsShipmentPlanVO;
import com.unlcn.ils.wms.base.businessDTO.outbound.WmsShipmentPlanVehicleDTO;
import com.unlcn.ils.wms.base.model.stock.WmsShipmentPlan;

import java.util.List;

/**
 * Created by DELL on 2017/9/22.
 */
public interface WmsShipmentPlanService {

    List<ResultForDcsInorOutDetailsDTO> saveShipmentList(WmsShipmentPlanVO[] wmsShipmentPlan, String s_body, String s_head) throws Exception;

    /**
     * 发运计划列表
     */
    ResultDTOWithPagination<Object> getShipmentPLanList(GetShipmentPLanListBO getShipmentPLanListBO) throws Exception;

    /**
     * 发运计划列表 - 君马
     */
    ResultDTOWithPagination<Object> getShipmentPLanListForNew(GetShipmentPLanListBO getShipmentPLanListBO) throws Exception;

    /**
     * 发运计划对应车型列表
     */
    List<WmsShipmentPlanVehicleDTO> getShipmentPLanVehicleList(String spId) throws Exception;

    /**
     * 生成备料计划
     */
    void createPreparationPlan(List<CreatePreparationPlanBO> boList, String userId) throws Exception;

    /**
     * 生成备料计划 - 君马库
     */
    void createPreparationPlanForJunMa(List<CreatePreparationPlanBO> preparationPlanBOList, String whCode, String userId) throws Exception;


    /**
     * 根据orderno 跟 route_end 确定唯一记录
     */
    void updateShipmentPlanByInfo(List<WmsShipmentPlan> wmsShipmentPlanList);

    /**
     * 根据id获取发运计划
     */
    WmsShipmentPlan getShipmentPlanById(Long spId) throws Exception;

    /**
     * 根据交接单号获取发运计划
     */
    List<WmsShipmentPlan> getShipmentPlanListByHandoverNo(String handoverNo) throws Exception;

    /**
     * 根据车架号查询发运计划
     */
    WmsShipmentPlan getShipmentPlanByVin(String vin) throws Exception;

    /**
     * 根据物料编码, 颜色, 车型名称, 仓库获取发运计划
     */
    WmsShipmentPlan getShipmentByMateriaCodeAndColorAndVehicel(String materiaCode, String color, String vehicle, String whCode) throws Exception;

    /**
     * 发运计划驳回的接口调用
     */
    void updateShipmentCancel() throws Exception;

    void updateShipmentCancleExcp() throws Exception;


    /**
     * 从tms抓取发运西南库发运计划
     */
    void fetchShipmentPlanListFromTMS() throws Exception;

    /**
     * 根据orderno 跟 route_end 确定唯一记录
     */
    void updateShipmentPlanByInfoNew(List<WmsShipmentPlan> wmsShipmentPlanList, String shipno, String timestamp);

    void insertMqToLogNew(String message);

    void insertMqToLog(List<WmsShipmentPlan> wmsShipmentPlanList, String shipno, String timestamp) throws Exception;
}
