package com.unlcn.ils.wms.backend.service.inspectApp.impl;


import cn.huiyunche.commons.exception.BusinessException;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.unlcn.ils.wms.backend.bo.inspectAppBO.TmsInboundInspectBO;
import com.unlcn.ils.wms.backend.enums.*;
import com.unlcn.ils.wms.backend.enums.excp.ExcpStatusEnum;
import com.unlcn.ils.wms.backend.service.inbound.WmsBatchNoService;
import com.unlcn.ils.wms.backend.service.inspectApp.TmsInspectComponentMissService;
import com.unlcn.ils.wms.backend.service.inspectApp.TmsInspectExcpService;
import com.unlcn.ils.wms.backend.service.inspectApp.TmsInspectService;
import com.unlcn.ils.wms.backend.util.jpush.JpushClientUtils;
import com.unlcn.ils.wms.base.dto.*;
import com.unlcn.ils.wms.base.mapper.extmapper.WmsInboundOrderDetailExtMapper;
import com.unlcn.ils.wms.base.mapper.extmapper.WmsTmsOrderExtMapper;
import com.unlcn.ils.wms.base.mapper.extmapper.WmsWarehouseNoticeExtMapper;
import com.unlcn.ils.wms.base.mapper.inbound.*;
import com.unlcn.ils.wms.base.mapper.inspectApp.*;
import com.unlcn.ils.wms.base.mapper.outbound.WmsDealerCarBackMapper;
import com.unlcn.ils.wms.base.mapper.outbound.WmsPreparationPlanMapper;
import com.unlcn.ils.wms.base.mapper.outbound.WmsPreparationVehicleDetailMapper;
import com.unlcn.ils.wms.base.mapper.stock.WmsBorrowCarDetailMapper;
import com.unlcn.ils.wms.base.mapper.stock.WmsWarehouseMapper;
import com.unlcn.ils.wms.base.mapper.sys.SysUserMapper;
import com.unlcn.ils.wms.base.mapper.sys.SysUserRoleMapper;
import com.unlcn.ils.wms.base.model.inbound.*;
import com.unlcn.ils.wms.base.model.inspectApp.*;
import com.unlcn.ils.wms.base.model.outbound.*;
import com.unlcn.ils.wms.base.model.stock.WmsBorrowCarDetail;
import com.unlcn.ils.wms.base.model.stock.WmsBorrowCarDetailExample;
import com.unlcn.ils.wms.base.model.stock.WmsWarehouse;
import com.unlcn.ils.wms.base.model.sys.SysUser;
import com.unlcn.ils.wms.base.model.sys.SysUserRole;
import com.unlcn.ils.wms.base.model.sys.SysUserRoleExample;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.text.SimpleDateFormat;
import java.util.*;

@Service
public class TmsInspectServiceImpl implements TmsInspectService {

    private Logger logger = LoggerFactory.getLogger(TmsInspectServiceImpl.class);

    private static final int VIN_LENGTH = 17;

    private static final String TYPE_MOVECAR = "1001";//待移车列表推送类别

    @Value("${tms.pickup.host.url}")
    private String propertyUrl;

    @Value("${tms.pickup.host.timeout}")
    private String propertyTime;

    @Value("${tms.encode.key}")
    private String propertyKey;

    @Autowired
    private TmsInspectLogMapper tmsInspectLogMapper;
    @Autowired
    private TmsFetchBillLogMapper tmsFetchBillLogMapper;
    @Autowired
    private TmsInspectExcpMapper tmsInspectExcpMapper;
    @Autowired
    private TmsInspectComponentMissMapper tmsInspectComponentMissMapper;
    @Autowired
    private WmsInboundOrderMapper wmsInboundOrderMapper;
    @Autowired
    private WmsInboundOrderDetailMapper wmsInboundOrderDetailMapper;
    @Autowired
    private WmsInboundInspectExpandMapper wmsInboundInspectExpandMapper;
    @Autowired
    private WmsAsnTempMapper wmsAsnTempMapper;
    @Autowired
    private WmsBorrowCarDetailMapper wmsBorrowCarDetailMapper;
    @Autowired
    private WmsDealerCarBackMapper wmsDealerCarBackMapper;
    @Autowired
    private WmsPreparationVehicleDetailMapper wmsPreparationVehicleDetailMapper;
    @Autowired
    private WmsPreparationPlanMapper wmsPreparationPlanMapper;
    @Autowired
    private WmsBatchNoService wmsBatchNoService;
    @Autowired
    private TmsInspectExcpTypeMapper tmsInspectExcpTypeMapper;
    @Autowired
    private WmsInboundRepairMapper wmsInboundRepairMapper;
    @Autowired
    private TmsInspectExcpExpandMapper tmsInspectExcpExpandMapper;
    @Autowired
    private SysUserMapper sysUserMapper;
    @Autowired
    private SysUserRoleMapper SysUserRoleMapper;
    @Autowired
    private WmsWarehouseMapper WmsWarehouseMapper;
    @Autowired
    private TmsInspectExcpAttachsMapper tmsInspectExcpAttachsMapper;
    @Autowired
    private WmsInboundOrderDetailExtMapper wmsInboundOrderDetailExtMapper;
    @Autowired
    private TmsInspectExcpService tmsInspectExcpService;
    @Autowired
    private TmsInspectComponentMissService tmsInspectComponentMissService;
    @Autowired
    private WmsTmsOrderExtMapper wmsTmsOrderExtMapper;
    @Autowired
    private WmsWarehouseNoticeDetailMapper wmsWarehouseNoticeDetailMapper;
    @Autowired
    private WmsWarehouseNoticeExtMapper wmsWarehouseNoticeExtMapper;


    /**
     * 获取验车单信息
     * <p>
     * 2018-1-31 在进行模糊查询车架号的时候  需要判断类型 获取数据的流程-->order-->rest方式修改-->http
     * 2018-4-19 重构入库通知单表单。此方法过时--新方法 {@link TmsInspectServiceImpl#updateFetchInspectBill}
     * </p>
     *
     * @param vin 车架号
     * @return TmsInspectDetailBO 返回值
     * @throws Exception 异常
     */
    @Override
    @Deprecated
    public Map<String, Object> updateFetchInspectBillOld(String vin, String whCode, String whName) throws Exception {
        logger.info("TmsInspectServiceImpl.updateFetchInspectBillOld param vin:{}.", vin);
        if (StringUtils.isBlank(vin)) {
            logger.error("TmsInspectServiceImpl.updateFetchInspectBillOld param vin must not be null");
            throw new IllegalArgumentException("输入信息不能为空");
        }
        if (StringUtils.isBlank(whCode)) {
            logger.error("TmsInspectServiceImpl.updateFetchInspectBillOld param whCode must not be null");
            throw new IllegalArgumentException("仓库编码不能为空");
        }
        if (vin.endsWith("\n")) {
            vin = vin.replaceAll("\n", "");
        }
        Map<String, Object> resultMap = Maps.newHashMap();
        ArrayList<TmsInboundInspectBO> bos = Lists.newArrayList();
        //校验是否是17位完整车架号(重庆库)
        if (vin.length() != VIN_LENGTH) {
            if (vin.length() < 6)
                throw new BusinessException("请输入数据长度至少6位");
            if (!WhCodeEnum.UNLCN_XN_CQ.getValue().equals(whCode))
                throw new BusinessException("此仓库不支持该操作");
            //模糊匹配或者运单号查询
            Map<String, Object> paramMap = Maps.newHashMap();
            paramMap.put("vin", "%" + vin + "%");
            paramMap.put("whCode", WhCodeEnum.UNLCN_XN_CQ.getValue());
            paramMap.put("notDel", DeleteFlagEnum.NORMAL.getValue());
            paramMap.put("orderBy", "t.gmt_create desc");
            paramMap.put("whText", WhCodeEnum.UNLCN_XN_CQ.getText());
            paramMap.put("del", WmsInboundOrderStockTransferEnum.OD_DELETED.getCode());
            //防止sql 报错,去除严格校验
            wmsTmsOrderExtMapper.updateSetSqlMode();
            List<WmsTmsOrder> data = wmsTmsOrderExtMapper.selectVinListLikeByParam(paramMap);
            if (CollectionUtils.isNotEmpty(data)) {
                data.forEach(v -> {
                    TmsInboundInspectBO detailBO = new TmsInboundInspectBO();
                    detailBO.setVin(v.getOddVin());
                    bos.add(detailBO);
                });
            }
            resultMap.put("searchType", WmsSysSourceEnum.LIKE.getValue());
            resultMap.put("bos", bos);
        } else {
            //区分仓库 精确匹配
            TmsInboundInspectBO detailBO = null;
            resultMap.put("searchType", WmsSysSourceEnum.ACCURATE.getValue());
            if (WhCodeEnum.UNLCN_XN_CQ.getValue().equals(whCode)) {
                //重庆库
                //就要进行精确查询并同步-->order-->rest方式修改-->http  回显
                detailBO = updateInspectBoActualForCQOld(vin, whCode, whName);
            }
            if (WhCodeEnum.JM_CS.getValue().equals(whCode) || WhCodeEnum.JM_XY.getValue().equals(whCode)) {
                //君马库 从order或者asn导入的数据
                //看是否在维修单中--重庆库和君马业务都有(目前重庆库暂无维修功能)
                WmsInboundRepairExample repairExample = new WmsInboundRepairExample();
                repairExample.createCriteria().andIsDeletedEqualTo(DeleteFlagEnum.NORMAL.getValue())
                        .andRpStatusNotEqualTo(String.valueOf(WmsRepairStatusEnum.WMS_REPAIR_STATUS_COMPLETE.getValue()))
                        .andRpVinEqualTo(vin);
                List<WmsInboundRepair> wmsInboundRepairs = wmsInboundRepairMapper.selectByExample(repairExample);
                if (CollectionUtils.isNotEmpty(wmsInboundRepairs))
                    throw new BusinessException("该车架号处于维修状态,需进维修页面进行处理");
                detailBO = getInspectBoForJMOld(vin, whCode, whName);
            }
            //正常业务的异常信息回显
            if (detailBO == null)
                throw new BusinessException("未查询到对应车辆信息");
            //判断维修的类型--去对应的入库和出库单关联查询对应的异常回显
            List<TmsInspectExcpDTO> positionCountList = tmsInspectExcpService.getPositionExcpCountByInspectId(detailBO.getId(), whCode);
            detailBO.setPositionAndExcpCount(positionCountList);
            String missComponentNames = tmsInspectComponentMissService.getMissComponentNamesNew(detailBO.getId(), whCode);
            detailBO.setMissComponentNames(missComponentNames);
            bos.add(detailBO);
            resultMap.put("bos", bos);
        }
        return resultMap;
    }


    /**
     * 获取验车单信息
     * <p>
     * 2018-1-31 在进行模糊查询车架号的时候  需要判断类型 获取数据的流程-->order-->rest方式修改-->http
     * 2018-4-19 重构入库通知单表单。此新方法
     * </p>
     */
    @Override
    public Map<String, Object> updateFetchInspectBill(WmsInspectForAppDTO appDTO) throws Exception {
        logger.info("TmsInspectServiceImpl.updateFetchInspectBill param:{}", appDTO);
        if (appDTO == null) {
            throw new BusinessException("传入参数不能为空");
        }
        String vin = appDTO.getVin();
        String whCode = appDTO.getWhCode();
        String whName = appDTO.getWhName();
        if (StringUtils.isBlank(vin)) {
            logger.error("TmsInspectServiceImpl.updateFetchInspectBill param vin must not be null");
            throw new IllegalArgumentException("输入信息不能为空");
        }
        if (StringUtils.isBlank(whCode)) {
            logger.error("TmsInspectServiceImpl.updateFetchInspectBill param whCode must not be null");
            throw new IllegalArgumentException("仓库编码不能为空");
        }
        if (vin.endsWith("\n")) {
            vin = vin.replaceAll("\n", "");
        }
        Map<String, Object> resultMap = Maps.newHashMap();
        ArrayList<TmsInboundInspectBO> bos = Lists.newArrayList();
        //校验是否是17位完整车架号(重庆库)
        if (vin.length() != VIN_LENGTH) {
            if (vin.length() < 6)
                throw new BusinessException("请输入数据长度至少6位");
            if (!WhCodeEnum.UNLCN_XN_CQ.getValue().equals(whCode))
                throw new BusinessException("此仓库不支持该操作");
            //模糊匹配或者运单号查询
            Map<String, Object> paramMap = Maps.newHashMap();
            paramMap.put("vin", "%" + vin + "%");
            paramMap.put("whCode", WhCodeEnum.UNLCN_XN_CQ.getValue());
            paramMap.put("notDel", DeleteFlagEnum.NORMAL.getValue());
            paramMap.put("orderBy", "t.gmt_create desc");
            paramMap.put("destination", WhCodeEnum.UNLCN_XN_CQ.getText());
            paramMap.put("del", WmsInboundOrderStockTransferEnum.OD_DELETED.getCode());
            //防止sql 报错,去除严格校验
            //wmsTmsOrderExtMapper.updateSetSqlMode();
            //List<WmsTmsOrder> data = wmsTmsOrderExtMapper.selectVinListLikeByParam(paramMap);
            wmsWarehouseNoticeExtMapper.updateSetSqlMode();
            List<WmsWarehouseNoticeDetailDTOForSchedule> data = wmsWarehouseNoticeExtMapper.selectVinListLikeByParam(paramMap);

            if (CollectionUtils.isNotEmpty(data)) {
                data.forEach(v -> {
                    TmsInboundInspectBO detailBO = new TmsInboundInspectBO();
                    detailBO.setVin(v.getWndVin());
                    bos.add(detailBO);
                });
            }
            resultMap.put("searchType", WmsSysSourceEnum.LIKE.getValue());
            resultMap.put("bos", bos);
        } else {
            //区分仓库 精确匹配
            TmsInboundInspectBO detailBO = null;
            resultMap.put("searchType", WmsSysSourceEnum.ACCURATE.getValue());
            if (WhCodeEnum.UNLCN_XN_CQ.getValue().equals(whCode)) {
                //重庆库
                //就要进行精确查询并同步-->order-->rest方式修改-->http  回显
                detailBO = updateInspectBoActualForCQ(vin, whCode, whName);
            }
            if (WhCodeEnum.JM_CS.getValue().equals(whCode) || WhCodeEnum.JM_XY.getValue().equals(whCode)) {
                //君马库 从order或者asn导入的数据
                //看是否在维修单中--重庆库和君马业务都有(目前重庆库暂无维修功能)
                WmsInboundRepairExample repairExample = new WmsInboundRepairExample();
                repairExample.createCriteria().andIsDeletedEqualTo(DeleteFlagEnum.NORMAL.getValue())
                        .andRpStatusNotEqualTo(String.valueOf(WmsRepairStatusEnum.WMS_REPAIR_STATUS_COMPLETE.getValue()))
                        .andRpWhCodeEqualTo(whCode)
                        .andRpVinEqualTo(vin);
                List<WmsInboundRepair> wmsInboundRepairs = wmsInboundRepairMapper.selectByExample(repairExample);
                //判断维修的类型
                if (CollectionUtils.isNotEmpty(wmsInboundRepairs)) {
                    throw new BusinessException("该车架号处于维修状态,需进维修页面进行处理");
                }
                detailBO = getInspectBoForJM(vin, whCode, whName);
            }
            //正常业务的异常信息回显
            if (detailBO == null) {
                throw new BusinessException("未查询到对应车辆信息");
            }
            //去对应的入库和出库单关联查询对应的异常回显
            List<TmsInspectExcpDTO> positionCountList = tmsInspectExcpService.getPositionExcpCountByInspectId(detailBO.getId(), whCode);
            detailBO.setPositionAndExcpCount(positionCountList);
            String missComponentNames = tmsInspectComponentMissService.getMissComponentNamesNew(detailBO.getId(), whCode);
            detailBO.setMissComponentNames(missComponentNames);
            bos.add(detailBO);
            resultMap.put("bos", bos);
        }
        return resultMap;
    }


    /**
     * 验车登记 更新验车状态为合格
     * --或者异常锁定状态
     *
     * @param id     id
     * @param userId 用户id
     */
    @Override
    public void updateInspectStatusById(Long id, String userId) {
        logger.info("TmsInspectServiceImpl.updateInspectStatusById param id: {}", id);
        if (Objects.equals(id, null) || Objects.equals(id, 0L)) {
            throw new BusinessException("验车ID不能为空");
        }
        if (StringUtils.isBlank(userId)) {
            throw new BusinessException("用户Id不能为空");
        }
        try {
            SysUser sysUser = sysUserMapper.selectByPrimaryKey(Integer.valueOf(userId));
            if (Objects.equals(sysUser, null)) {
                throw new BusinessException("未查询到对应的用户信息");
            }
            SysUserRoleExample roleExample = new SysUserRoleExample();
            roleExample.createCriteria().andUserIdEqualTo(sysUser.getId());
            List<SysUserRole> sysUserRoles = SysUserRoleMapper.selectByExample(roleExample);
            WmsWarehouse warehouse = null;
            if (CollectionUtils.isNotEmpty(sysUserRoles)) {
                Long warehouseId = sysUserRoles.get(0).getWarehouseId();
                warehouse = WmsWarehouseMapper.selectByPrimaryKey(warehouseId);
            }
            WmsInboundOrder bill = wmsInboundOrderMapper.selectByPrimaryKey(id);
            //合格状态不能再进行验车登记
            if (Objects.equals(bill.getInspectStatus(), TmsInspectStatusEnum.WAYBILL_QUALIFY.getValue())) {
                throw new BusinessException("该车辆已经验车合格,无需重复操作!");
            }
            if (Objects.equals(bill.getInspectStatus(), TmsInspectStatusEnum.WAYBILL_CONFIRM_EXCP.getValue())) {
                throw new BusinessException("该车辆已经进行了该操作,无需重复操作!");
            }
            //异常的锁定--(君马和重庆库都有的业务)
            if (Objects.equals(bill.getInspectStatus(), TmsInspectStatusEnum.WAYBILL_EXCP.getValue())) {
                //throw new BusinessException("该车辆处于异常状态,不能进行验车登记操作!");
                //锁定异常状态变为70;
                bill.setInspectStatus(TmsInspectStatusEnum.WAYBILL_CONFIRM_EXCP.getValue());
                WmsCheckResultEnum resultEnum = WmsCheckResultEnum.getByValue(TmsInspectStatusEnum.WAYBILL_CONFIRM_EXCP.getValue());
                if (resultEnum != null) {
                    bill.setOdCheckResult(resultEnum.getText());
                }
                WmsCheckDescEnum descEnum = WmsCheckDescEnum.getByValue(TmsInspectStatusEnum.WAYBILL_CONFIRM_EXCP.getValue());
                if (descEnum != null) {
                    bill.setOdCheckDesc(descEnum.getText());
                }

                //支持维修的分批显示
                //int count1 = 0;
                //int count2 = 0;
                //count1 = updateExcpToDelAndInsertNew(id, count1);
                ////删除缺件的异常再插入新的
                //count2 = updateMissToDelAndInsertNew(id, count2);

                //2.查看异常的类型并关联对应的异常类型---新增业务(重庆/君马都有)
                //统计非质损异常数量
                //if (warehouse != null && StringUtils.isNotBlank(warehouse.getWhCode())) {
                //if (WhCodeEnum.JM_CS.getValue().equals(warehouse.getWhCode()) ||
                //        WhCodeEnum.JM_XY.getValue().equals(warehouse.getWhCode())) {
                int excpValue = TmsInsepectExcpStatusEnum.BILL_UNTREATED.getValue();
                int del_flag = DeleteFlagEnum.DELETED.getValue();
                int count1 = tmsInspectExcpExpandMapper.selectExcpCountByInspectId(bill.getOdId(), excpValue, del_flag);
                //统计缺件异常数量
                TmsInspectComponentMissExample missExample = new TmsInspectComponentMissExample();
                missExample.createCriteria()
                        .andIsDeletedNotEqualTo(DeleteFlagEnum.DELETED.getValue())
                        .andInspectIdEqualTo(bill.getOdId());
                int count2 = tmsInspectComponentMissMapper.countByExample(missExample);
                updateExcpTypeAndTimestampToOneBatch(bill, count1, count2);
                //}
                //}
            }
            //合格状态
            if (Objects.equals(bill.getInspectStatus(), TmsInspectStatusEnum.WAYBILL_INIT.getValue())) {
                //验车合格
                bill.setInspectStatus(TmsInspectStatusEnum.WAYBILL_QUALIFY.getValue());
                WmsCheckResultEnum resultEnum = WmsCheckResultEnum.getByValue(TmsInspectStatusEnum.WAYBILL_QUALIFY.getValue());
                if (resultEnum != null) {
                    bill.setOdCheckResult(resultEnum.getText());
                }
                WmsCheckDescEnum descEnum = WmsCheckDescEnum.getByValue(TmsInspectStatusEnum.WAYBILL_QUALIFY.getValue());
                if (descEnum != null) {
                    bill.setOdCheckDesc(descEnum.getText());
                }
            }
            bill.setModifyUserId(userId);
            bill.setModifyUserName(sysUser.getName());
            bill.setInspectUserId(userId);
            bill.setGmtUpdate(new Date());
            bill.setInspectTime(new Date());
            wmsInboundOrderMapper.updateByPrimaryKeySelective(bill);
            //君马库进行通知推送
            if (warehouse != null && StringUtils.isNotBlank(warehouse.getWhCode())) {
                if (WhCodeEnum.JM_CS.getValue().equals(warehouse.getWhCode()) ||
                        WhCodeEnum.JM_XY.getValue().equals(warehouse.getWhCode())) {
                    //君马库需要推送可移车任务到app-只能是合格和缺件异常的车辆能够推送
                    String excpTypeTypeCode = null;
                    if (bill.getOdInspectExcpType() != null) {
                        TmsInspectExcpType tmsInspectExcpType = tmsInspectExcpTypeMapper.selectByPrimaryKey(bill.getOdInspectExcpType());
                        if (tmsInspectExcpType != null) {
                            excpTypeTypeCode = tmsInspectExcpType.getTypeCode();
                        }
                    }
                    if (TmsInspectStatusEnum.WAYBILL_QUALIFY.getValue() == bill.getInspectStatus() ||
                            (StringUtils.isNotBlank(excpTypeTypeCode) && WmsInspectExcpTypeEnum.TYPE_MISS.getCode().equals(excpTypeTypeCode))) {
                        WmsWarehouse finalWarehouse = warehouse;
                        new Thread(() -> {
                            try {
                                //推送消息
                                //String name = sysUser.getName();
                                String notiTile = "有新的待移车任务,请点击APP进行查收![中联物流]";
                                JpushClientUtils.sendToAllAndroidWithTag(notiTile, "", "", "", finalWarehouse.getWhCode(), TYPE_MOVECAR);
                            } catch (Exception e) {
                                logger.error("TmsInspectServiceImpl.updateInspectStatusById 推送通知失败 error:", e);
                            }
                        }).start();
                    }
                }
            }
            new Thread(() -> {
                //验车日志
                TmsInspectLog log = new TmsInspectLog();
                saveLog(id, userId, log);
                log.setMsg("updateInspectStatusById success!");
                tmsInspectLogMapper.insertSelective(log);
            }).start();
        } catch (BusinessException e) {
            logger.error("TmsInspectServiceImpl.updateInspectStatusById BusinessException: {}", e);
            throw new BusinessException(e.getMessage());
        } catch (Exception ex) {
            logger.error("TmsInspectServiceImpl.updateInspectStatusById error: {}", ex);
            throw new BusinessException("验车登记异常!");
        }
    }


    /**
     * 走正常业务之前查询该车架号是否在维修表中
     *
     * @param vin    车架号
     * @param whCode 仓库code
     * @return 返回值
     */
    @Override
    public TmsInboundInspectBO getBoFromRepair(String vin, String whCode) {
        WmsInboundRepairExample repairExample = new WmsInboundRepairExample();
        WmsInboundRepairExample.Criteria rpCriteria = repairExample.createCriteria().andIsDeletedNotEqualTo(DeleteFlagEnum.DELETED.getValue())
                .andRpStatusEqualTo(String.valueOf(WmsRepairStatusEnum.WMS_REPAIR_STATUS_ING.getValue()));
        if (vin.trim().length() == VIN_LENGTH) {
            rpCriteria.andRpVinEqualTo(vin);
        } else {
            rpCriteria.andRpWaybillNoEqualTo(vin);
        }
        List<WmsInboundRepair> wmsInboundRepairs = wmsInboundRepairMapper.selectByExample(repairExample);
        if (CollectionUtils.isNotEmpty(wmsInboundRepairs)) {
            //返回值--设置车架号  维修单id
            WmsInboundRepair repair = wmsInboundRepairs.get(0);
            if (StringUtils.isNotBlank(repair.getRpWhCode()) && !(whCode.equals(repair.getRpWhCode()))) {
                throw new BusinessException("该用户不具有该仓库的操作权限");
            }
            if (StringUtils.isNotBlank(repair.getRepairInspectPlace())
                    && !String.valueOf(WmsRepairInspectPlaceEnum.INBOUND_PLACE.getValue()).equals(repair.getRepairInspectPlace())) {
                throw new BusinessException("该维修车辆不能在入库区验车");
            }
            //构造返回结果
            TmsInboundInspectBO rpReturnBo = new TmsInboundInspectBO();
            rpReturnBo.setVin(repair.getRpVin());
            rpReturnBo.setStyle(repair.getRpVehicleSpecName());
            rpReturnBo.setRpId(String.valueOf(repair.getRpId()));
            rpReturnBo.setId(StringUtils.isNotBlank(repair.getRpOdId()) ? Long.valueOf(repair.getRpOdId()) : null);
            rpReturnBo.setRpInAndOutType(repair.getRpInandoutType());
            rpReturnBo.setInspectStatusValue(TmsInspectStatusEnum.WAYBILL_REPAIRING.getValue());//维修状态
            rpReturnBo.setInspectStatusText(TmsInspectStatusEnum.WAYBILL_REPAIRING.getText());//维修中
            //对应的入库和出库维修类型 --关联查询对应的出库任务id关联对应的异常信息展示
            //构造返回结果
            return rpReturnBo;
        }
        return null;
    }


    /**
     * 取消验车
     */
    @Override
    public void updateToInspectCancel(Long inspectId, String userId) {
        logger.info("TmsInspectServiceImpl.inspectCancel param id: {},{}", inspectId, userId);
        if (Objects.equals(inspectId, null) || Objects.equals(inspectId, 0L)) {
            logger.info("TmsInspectServiceImpl.inspectCancel param inspectId must not be null");
            throw new IllegalArgumentException("验车单ID不能为空");
        }
        if (Objects.equals(userId, null)) {
            logger.info("TmsInspectServiceImpl.inspectCancel param userId must not be null");
            throw new IllegalArgumentException("用户ID不能为空");
        }
        //删除原来绑定的异常;
        WmsInboundOrder inboundInspect = wmsInboundOrderMapper.selectByPrimaryKey(inspectId);
        if (Objects.equals(inboundInspect, null)) {
            throw new BusinessException("未查询到对应的异常信息!");
        }
        //删除对应缺件异常
        TmsInspectComponentMissExample missExample = new TmsInspectComponentMissExample();
        TmsInspectComponentMissExample.Criteria criteria = missExample.createCriteria();
        criteria.andInspectIdEqualTo(inboundInspect.getOdId());
        tmsInspectComponentMissMapper.deleteByExample(missExample);
        //删除对应标记异常
        TmsInspectExcpExample excpExample = new TmsInspectExcpExample();
        excpExample.createCriteria().andInspectIdEqualTo(inboundInspect.getOdId());
        tmsInspectExcpMapper.deleteByExample(excpExample);
        //更新验车状态为待验车
        inboundInspect.setGmtUpdate(new Date());
        inboundInspect.setInspectUserId(userId);
        inboundInspect.setInspectStatus(TmsInspectStatusEnum.WAYBILL_INIT.getValue());
        wmsInboundOrderMapper.updateByPrimaryKeySelective(inboundInspect);
        //插入日志表
        new Thread(() -> {
            //验车日志
            TmsInspectLog log = new TmsInspectLog();
            saveLog(inspectId, userId, log);
            log.setMsg("updateToInspectCancel success!");
            tmsInspectLogMapper.insertSelective(log);
        }).start();
    }

    /**
     * <p>
     * 2018-4-20 入库通知单重构,此方法 新方法
     * </p>
     *
     * @param vin    车架号
     * @param whCode 仓库code
     * @param whName 仓库name
     * @return 返回值
     */
    @Override
    public TmsInboundInspectBO getInspectBoForJM(String vin, String whCode, String whName) {
        // 查询入库表数据
        byte del = DeleteFlagEnum.DELETED.getValue();
        int finishValue = WmsInboundStatusEnum.WMS_INBOUND_FINISH.getValue();//用于多条数据的过滤以前入库的数据
        List<WmsInboundInspectDTO> wmsInboundInspectDTOS = wmsInboundInspectExpandMapper.selectJMInboundInspectByVin(vin, del, finishValue);
        if (null != wmsInboundInspectDTOS && wmsInboundInspectDTOS.size() > 1) {
            throw new BusinessException("该车架号:" + vin + "未出库状态,存在多条记录!");
        }

        // 返回对象
        TmsInboundInspectBO inspectBO = null;

        // 先查询入库单，有则直接返回
        if (CollectionUtils.isNotEmpty(wmsInboundInspectDTOS)) {
            inspectBO = new TmsInboundInspectBO();
            //校验仓库code
            if (!whCode.equals(wmsInboundInspectDTOS.get(0).getWhCode())) {
                throw new BusinessException("该用户不具有该仓库的操作权限");
            }
            getInspectBoByDTO(inspectBO, wmsInboundInspectDTOS.get(0));
        } else {
            // 2018-4-20 重构入库通知单  重写此代码
            HashMap<String, Object> params = Maps.newHashMap();
            params.put("vin", vin);
            params.put("notDel", DeleteFlagEnum.NORMAL.getValue());
            params.put("notSend", SendBusinessFlagEnum.SEND_N.getValue());
            params.put("whCode", whCode);
            params.put("businessType", InboundOrderBusinessTypeEnum.Z1.getCode());
            params.put("orderBy", "a.wnd_id desc");
            params.put("start", 0);
            params.put("end", 1);
            List<WmsWarehouseNoticeHeadDTO> data = wmsWarehouseNoticeExtMapper.selectNotInboundNoticeByParam(params);
            //data 是可以为空的
            if (CollectionUtils.isNotEmpty(data)) {
                WmsInboundOrder inboundOrder = new WmsInboundOrder();
                WmsInboundOrderDetail orderDetail = new WmsInboundOrderDetail();
                SyncNoticeToOrder(data, whCode, whName, inboundOrder, orderDetail);

                // 构造返回bo
                inspectBO = new TmsInboundInspectBO();
                getInspectBo(inspectBO, inboundOrder, orderDetail);

            } else {
                // 查询借车单
                WmsBorrowCarDetailExample borrowCarExample = new WmsBorrowCarDetailExample();
                WmsBorrowCarDetailExample.Criteria borrowCriteria = borrowCarExample.createCriteria();
                borrowCriteria.andBdVinEqualTo(vin);
                borrowCriteria.andDeleteFlagNotEqualTo(false);
                List<WmsBorrowCarDetail> borrowCarList = wmsBorrowCarDetailMapper.selectByExample(borrowCarExample);
                if (CollectionUtils.isNotEmpty(borrowCarList)) {
                    WmsBorrowCarDetail wmsBorrowCarDetail = borrowCarList.get(0);
                    //校验仓库code
                    if (StringUtils.isNotBlank(wmsBorrowCarDetail.getBdWhcode()) && (!whCode.equals(wmsBorrowCarDetail.getBdWhcode()))) {
                        throw new BusinessException("该用户不具有该仓库的操作权限");
                    }
                    /*WmsReturnCar wmsReturnCar = wmsReturnCarMapper.selectByPrimaryKey(wmsBorrowCarDetail.getBdReturnId());
                    if (null == wmsReturnCar) {
                        throw new BusinessException("没有还车单");
                    }*/

                    WmsInboundOrder inboundOrder = new WmsInboundOrder();
                    WmsInboundOrderDetail orderDetail = new WmsInboundOrderDetail();
                    SyncReturnToOrder(wmsBorrowCarDetail, whCode, whName, inboundOrder, orderDetail);

                    // 构造返回bo
                    inspectBO = new TmsInboundInspectBO();
                    getInspectBo(inspectBO, inboundOrder, orderDetail);
                } else {

                    // 查询退车表
                    WmsDealerCarBackExample dealerCarBackExample = new WmsDealerCarBackExample();
                    WmsDealerCarBackExample.Criteria dealerCarBackCriteria = dealerCarBackExample.createCriteria();
                    dealerCarBackCriteria.andCbVinEqualTo(vin);

                    List<WmsDealerCarBack> carBackList = wmsDealerCarBackMapper.selectByExample(dealerCarBackExample);
                    if (CollectionUtils.isNotEmpty(carBackList)) {
                        WmsDealerCarBack wmsDealerCarBack = carBackList.get(0);
                        //校验仓库code
                        if (StringUtils.isNotBlank(wmsDealerCarBack.getCbWhCode()) && (!whCode.equals(wmsDealerCarBack.getCbWhCode()))) {
                            throw new BusinessException("该用户不具有该仓库的操作权限");
                        }
                        WmsInboundOrder inboundOrder = new WmsInboundOrder();
                        WmsInboundOrderDetail orderDetail = new WmsInboundOrderDetail();
                        SyncDealerCarBackToOrder(wmsDealerCarBack, whCode, whName, inboundOrder, orderDetail);

                        // 更新接收时间为当前时间
                        wmsDealerCarBack.setCbReceiveTime(new Date());
                        wmsDealerCarBackMapper.updateByPrimaryKey(wmsDealerCarBack);

                        // 构造返回bo
                        inspectBO = new TmsInboundInspectBO();
                        getInspectBo(inspectBO, inboundOrder, orderDetail);
                    } else {
                        // 查询发运计划表---现在改成关联备料计划表查询
                        //DeleteFlagEnum.NOT_QUIT.getValue();
                        //wmsPreparationVehicleDetailCustomMapper.selectPreparationByVin(vin,DeleteFlagEnum.NOT_QUIT.getValue());
                        WmsPreparationVehicleDetailExample detailExample = new WmsPreparationVehicleDetailExample();
                        detailExample.setOrderByClause("gmt_create desc");
                        detailExample.createCriteria().andVdVinEqualTo(vin)
                                .andIsDeletedNotEqualTo(DeleteFlagEnum.DELETED.getValue());
                        List<WmsPreparationVehicleDetail> vehicleDetailList = wmsPreparationVehicleDetailMapper.selectByExample(detailExample);
                        if (CollectionUtils.isNotEmpty(vehicleDetailList)) {
                            for (WmsPreparationVehicleDetail wmsPreparationVehicleDetail : vehicleDetailList) {
                                WmsPreparationPlan wmsPreparationPlan = wmsPreparationPlanMapper.selectByPrimaryKey(wmsPreparationVehicleDetail.getVdPpId());
                                if (!Objects.equals(wmsPreparationPlan.getPpDlvType(), null)) {
                                    //主要是调拨类型的入库扫码
                                    if (wmsPreparationPlan.getPpDlvType().equals(WmsShipmentPlanDlvTypeEnum.A2.getValue())) {
                                        WmsInboundOrder inboundOrder = new WmsInboundOrder();
                                        WmsInboundOrderDetail orderDetail = new WmsInboundOrderDetail();
                                        syncPreparationToOrder(wmsPreparationPlan, wmsPreparationVehicleDetail, whCode, whName, inboundOrder, orderDetail);
                                        // 构造返回bo
                                        inspectBO = new TmsInboundInspectBO();
                                        getInspectBo(inspectBO, inboundOrder, orderDetail);
                                    }
                                }
                            }
                        }

                    }
                }
            }
        }
        return inspectBO;
    }


    /**
     * <p>
     * 2018-4-20 入库通知单重构,此方法过时: 新方法{@link TmsInspectServiceImpl#getInspectBoForJM}
     * </p>
     *
     * @param vin    车架号
     * @param whCode 仓库code
     * @param whName 仓库name
     * @return 返回值
     */
    @Deprecated
    @Override
    public TmsInboundInspectBO getInspectBoForJMOld(String vin, String whCode, String whName) {
        // 查询入库表数据
        byte del = DeleteFlagEnum.DELETED.getValue();
        int finishValue = WmsInboundStatusEnum.WMS_INBOUND_FINISH.getValue();//用于多条数据的过滤以前入库的数据
        List<WmsInboundInspectDTO> wmsInboundInspectDTOS = wmsInboundInspectExpandMapper.selectJMInboundInspectByVin(vin, del, finishValue);
        if (null != wmsInboundInspectDTOS && wmsInboundInspectDTOS.size() > 1) {
            throw new BusinessException("该车架号:" + vin + "未出库状态,存在多条记录!");
        }

        // 返回对象
        TmsInboundInspectBO inspectBO = null;

        // 先查询入库单，有则直接返回
        if (CollectionUtils.isNotEmpty(wmsInboundInspectDTOS)) {
            inspectBO = new TmsInboundInspectBO();
            //校验仓库code
            if (!whCode.equals(wmsInboundInspectDTOS.get(0).getWhCode())) {
                throw new BusinessException("该用户不具有该仓库的操作权限");
            }
            getInspectBoByDTO(inspectBO, wmsInboundInspectDTOS.get(0));
        } else {
            // 再查询本地入库业务单(asn)
            WmsAsnTempExample asnTempExample = new WmsAsnTempExample();
            asnTempExample.createCriteria().andAtVinEqualTo(vin)
                    .andAtSendBusinessFlagEqualTo(String.valueOf(SendBusinessFlagEnum.SEND_N.getValue()));
            List<WmsAsnTemp> wmsAsnTemps = wmsAsnTempMapper.selectByExample(asnTempExample);
            if (CollectionUtils.isNotEmpty(wmsAsnTemps)) {
                WmsAsnTemp wmsAsnTemp = wmsAsnTemps.get(0);
                //校验仓库code
                if (StringUtils.isNotBlank(wmsAsnTemp.getWhcode()) && (!whCode.equals(wmsAsnTemp.getWhcode()))) {
                    throw new BusinessException("该用户不具有该仓库的操作权限");
                }
                WmsInboundOrder inboundOrder = new WmsInboundOrder();
                WmsInboundOrderDetail orderDetail = new WmsInboundOrderDetail();
                SyncAsnToOrderOld(wmsAsnTemp, whCode, whName, inboundOrder, orderDetail);

                // 构造返回bo
                inspectBO = new TmsInboundInspectBO();
                getInspectBo(inspectBO, inboundOrder, orderDetail);
            } else {

                // 查询借车单
                WmsBorrowCarDetailExample borrowCarExample = new WmsBorrowCarDetailExample();
                WmsBorrowCarDetailExample.Criteria borrowCriteria = borrowCarExample.createCriteria();
                borrowCriteria.andBdVinEqualTo(vin);
                borrowCriteria.andDeleteFlagNotEqualTo(false);
                List<WmsBorrowCarDetail> borrowCarList = wmsBorrowCarDetailMapper.selectByExample(borrowCarExample);
                if (CollectionUtils.isNotEmpty(borrowCarList)) {
                    WmsBorrowCarDetail wmsBorrowCarDetail = borrowCarList.get(0);
                    //校验仓库code
                    if (StringUtils.isNotBlank(wmsBorrowCarDetail.getBdWhcode()) && (!whCode.equals(wmsBorrowCarDetail.getBdWhcode()))) {
                        throw new BusinessException("该用户不具有该仓库的操作权限");
                    }
                    /*WmsReturnCar wmsReturnCar = wmsReturnCarMapper.selectByPrimaryKey(wmsBorrowCarDetail.getBdReturnId());
                    if (null == wmsReturnCar) {
                        throw new BusinessException("没有还车单");
                    }*/

                    WmsInboundOrder inboundOrder = new WmsInboundOrder();
                    WmsInboundOrderDetail orderDetail = new WmsInboundOrderDetail();
                    SyncReturnToOrder(wmsBorrowCarDetail, whCode, whName, inboundOrder, orderDetail);

                    // 构造返回bo
                    inspectBO = new TmsInboundInspectBO();
                    getInspectBo(inspectBO, inboundOrder, orderDetail);
                } else {

                    // 查询退车表
                    WmsDealerCarBackExample dealerCarBackExample = new WmsDealerCarBackExample();
                    WmsDealerCarBackExample.Criteria dealerCarBackCriteria = dealerCarBackExample.createCriteria();
                    dealerCarBackCriteria.andCbVinEqualTo(vin);

                    List<WmsDealerCarBack> carBackList = wmsDealerCarBackMapper.selectByExample(dealerCarBackExample);
                    if (CollectionUtils.isNotEmpty(carBackList)) {
                        WmsDealerCarBack wmsDealerCarBack = carBackList.get(0);
                        //校验仓库code
                        if (StringUtils.isNotBlank(wmsDealerCarBack.getCbWhCode()) && (!whCode.equals(wmsDealerCarBack.getCbWhCode()))) {
                            throw new BusinessException("该用户不具有该仓库的操作权限");
                        }
                        WmsInboundOrder inboundOrder = new WmsInboundOrder();
                        WmsInboundOrderDetail orderDetail = new WmsInboundOrderDetail();
                        SyncDealerCarBackToOrder(wmsDealerCarBack, whCode, whName, inboundOrder, orderDetail);

                        // 更新接收时间为当前时间
                        wmsDealerCarBack.setCbReceiveTime(new Date());
                        wmsDealerCarBackMapper.updateByPrimaryKey(wmsDealerCarBack);

                        // 构造返回bo
                        inspectBO = new TmsInboundInspectBO();
                        getInspectBo(inspectBO, inboundOrder, orderDetail);
                    } else {
                        // 查询发运计划表---现在改成关联备料计划表查询
                        //DeleteFlagEnum.NOT_QUIT.getValue();
                        //wmsPreparationVehicleDetailCustomMapper.selectPreparationByVin(vin,DeleteFlagEnum.NOT_QUIT.getValue());
                        WmsPreparationVehicleDetailExample detailExample = new WmsPreparationVehicleDetailExample();
                        detailExample.setOrderByClause("gmt_create desc");
                        detailExample.createCriteria().andVdVinEqualTo(vin)
                                .andIsDeletedNotEqualTo(DeleteFlagEnum.DELETED.getValue());
                        List<WmsPreparationVehicleDetail> vehicleDetailList = wmsPreparationVehicleDetailMapper.selectByExample(detailExample);
                        if (CollectionUtils.isNotEmpty(vehicleDetailList)) {
                            for (WmsPreparationVehicleDetail wmsPreparationVehicleDetail : vehicleDetailList) {
                                WmsPreparationPlan wmsPreparationPlan = wmsPreparationPlanMapper.selectByPrimaryKey(wmsPreparationVehicleDetail.getVdPpId());
                                if (!Objects.equals(wmsPreparationPlan.getPpDlvType(), null)) {
                                    //主要是调拨类型的入库扫码
                                    if (wmsPreparationPlan.getPpDlvType().equals(WmsShipmentPlanDlvTypeEnum.A2.getValue())) {
                                        WmsInboundOrder inboundOrder = new WmsInboundOrder();
                                        WmsInboundOrderDetail orderDetail = new WmsInboundOrderDetail();
                                        syncPreparationToOrder(wmsPreparationPlan, wmsPreparationVehicleDetail, whCode, whName, inboundOrder, orderDetail);
                                        // 构造返回bo
                                        inspectBO = new TmsInboundInspectBO();
                                        getInspectBo(inspectBO, inboundOrder, orderDetail);
                                    }
                                }
                            }
                        }

                    }
                }
            }
        }
        return inspectBO;
    }


    /**
     * 记录日志
     *
     * @param vin 车架号
     * @param msg 消息记录
     */
    @Override
    public void saveTmsLog(String vin, String msg) {
        TmsFetchBillLog log = new TmsFetchBillLog();
        log.setVin(vin);
        log.setMsg(msg);
        log.setGmtCreate(new Date());
        log.setGmtUpdate(new Date());
        tmsFetchBillLogMapper.insertSelective(log);
    }

    /**
     * 重庆库正常查询车架号业务
     * <p>
     * 2017/12/25 新需求:西南库股份业务 到重庆前置库中转的车辆也能进行收货入库
     * 2018-1-31  新需求:新增定时任务获取入库信息-->order-->rest方式修改-->http  回显(此方法用于精确查找)
     * 2018-4-19 重构入库通知单 新方法：
     * </p>
     *
     * @param vin    车架号
     * @param whCode 仓库code
     * @param whName 仓库name
     * @return 返回值
     */
    private TmsInboundInspectBO updateInspectBoActualForCQ(String vin, String whCode, String whName) {
        TmsInboundInspectBO inspectBO = new TmsInboundInspectBO();
        // 说明传入的是车架号
        // 查询对应的业务表数据
        Map<String, Object> paramMap = Maps.newHashMap();
        paramMap.put("vin", vin);
        paramMap.put("notDel", DeleteFlagEnum.NORMAL.getValue());
        paramMap.put("whCode", whCode);
        List<WmsInboundInspectDTO> wmsInboundInspectDTOList = wmsInboundInspectExpandMapper.selectInboundInspectByVin(paramMap);
        // 如果业务表已经存在数据直接构造bo返回
        if (CollectionUtils.isNotEmpty(wmsInboundInspectDTOList)) {
            if (wmsInboundInspectDTOList.size() > 1)
                throw new BusinessException("该车辆:" + vin + "在order中存在多条记录");
            // 构造返回bo
            WmsInboundInspectDTO wmsInboundInspectDTO = wmsInboundInspectDTOList.get(0);
            getInspectBoByDTO(inspectBO, wmsInboundInspectDTO);
        } else {
            //查接口定时任务REST 修改的数据,并同步到order表中
            paramMap.put("modifyType", WmsSysSourceEnum.REST.getValue());
            paramMap.put("notDel", DeleteFlagEnum.NORMAL.getValue());
            paramMap.put("vin", vin);
            paramMap.put("orderBy", "a.wnd_id desc");
            paramMap.put("start", 0);
            paramMap.put("end", 1);
            List<WmsWarehouseNoticeHeadDTO> restData = wmsWarehouseNoticeExtMapper.selectLastRESTInfoByParam(paramMap);
            //List<WmsTmsOrder> restData = wmsTmsOrderExtMapper.selectLastRESTInfoByParam(paramMap);
            if (CollectionUtils.isNotEmpty(restData)) {
                WmsWarehouseNoticeHeadDTO head = restData.get(0);
                WmsWarehouseNoticeDetailDTOForSchedule detail = head.getDetailList().get(0);
                //校验是否被删除
                if (WmsInboundOrderStockTransferEnum.OD_DELETED.getCode().equals(detail.getWndModifyType())) {
                    throw new BusinessException("该车架号:" + vin + "已经更新删除,无法入重庆库");
                }
                if (!WhCodeEnum.UNLCN_XN_CQ.getText().equals(head.getWnhDestinationName())) {
                    throw new BusinessException("该车架号:" + vin + "目的地非前置库");
                }
                //同步到业务表中(order和orderDetail)
                WmsInboundOrder wmsInboundOrder = saveWmsInboundOrder(vin, whCode, whName, restData);
                WmsInboundOrderInsertProcedureDTO procedureDTO = new WmsInboundOrderInsertProcedureDTO();
                BeanUtils.copyProperties(wmsInboundOrder, procedureDTO);
                procedureDTO.setVin(vin);
                wmsInboundOrderDetailExtMapper.insertInboundOrderByProcedure(procedureDTO);
                wmsInboundOrder.setOdId(procedureDTO.getOdId());
                //插入关联的detail表
                WmsInboundOrderDetail orderDetail = saveWmsInboundOrderDetail(whCode, whName, restData, wmsInboundOrder);
                wmsInboundOrderDetailExtMapper.insertInboundOrderDetailByProcedure(orderDetail);
                // 构造返回bo
                getInspectBo(inspectBO, wmsInboundOrder, orderDetail);

            } else {
                //查询定时方式同步的数据
                paramMap.put("createType", WmsSysSourceEnum.HTTP.getValue());
                List<WmsWarehouseNoticeHeadDTO> httpData = wmsWarehouseNoticeExtMapper.selectLastHTTPInfoByParam(paramMap);
                //List<WmsTmsOrder> httpData = wmsTmsOrderExtMapper.selectLastHTTPInfoByParam(paramMap);
                if (CollectionUtils.isEmpty(httpData)) {
                    throw new BusinessException("未查询到该车辆:" + vin + "的信息");
                }
                //同步到业务表中(order和orderDetail)
                WmsInboundOrder wmsInboundOrder = saveWmsInboundOrder(vin, whCode, whName, httpData);
                WmsInboundOrderInsertProcedureDTO procedureDTO = new WmsInboundOrderInsertProcedureDTO();
                BeanUtils.copyProperties(wmsInboundOrder, procedureDTO);
                procedureDTO.setVin(vin);
                wmsInboundOrderDetailExtMapper.insertInboundOrderByProcedure(procedureDTO);
                wmsInboundOrder.setOdId(procedureDTO.getOdId());
                //插入关联的detail表
                WmsInboundOrderDetail orderDetail = saveWmsInboundOrderDetail(whCode, whName, httpData, wmsInboundOrder);
                wmsInboundOrderDetailExtMapper.insertInboundOrderDetailByProcedure(orderDetail);
                // 构造返回bo
                getInspectBo(inspectBO, wmsInboundOrder, orderDetail);
            }

        }
        return inspectBO;
    }


    /**
     * 重庆库正常查询车架号业务
     * <p>
     * 2017/12/25 新需求:西南库股份业务 到重庆前置库中转的车辆也能进行收货入库
     * 2018-1-31  新需求:新增定时任务获取入库信息-->order-->rest方式修改-->http  回显(此方法用于精确查找)
     * 2018-4-19 重构入库通知单 新方法：{@link TmsInspectServiceImpl#updateInspectBoActualForCQ}
     * </p>
     *
     * @param vin    车架号
     * @param whCode 仓库code
     * @param whName 仓库name
     * @return 返回值
     */
    @Deprecated
    private TmsInboundInspectBO updateInspectBoActualForCQOld(String vin, String whCode, String whName) {
        TmsInboundInspectBO inspectBO = new TmsInboundInspectBO();
        // 说明传入的是车架号-查询对应的业务表数据
        Map<String, Object> paramMap = Maps.newHashMap();
        paramMap.put("vin", vin);
        paramMap.put("notDel", DeleteFlagEnum.NORMAL.getValue());
        List<WmsInboundInspectDTO> wmsInboundInspectDTOList = wmsInboundInspectExpandMapper.selectInboundInspectByVin(paramMap);
        // 如果业务表已经存在数据直接构造bo返回
        if (CollectionUtils.isNotEmpty(wmsInboundInspectDTOList)) {
            if (wmsInboundInspectDTOList.size() > 1)
                throw new BusinessException("该车辆:" + vin + "在order中存在多条记录");
            // 构造返回bo
            WmsInboundInspectDTO wmsInboundInspectDTO = wmsInboundInspectDTOList.get(0);
            getInspectBoByDTO(inspectBO, wmsInboundInspectDTO);
        } else {
            //查接口定时任务REST 修改的数据,并同步到order表中
            paramMap.put("modifyType", WmsSysSourceEnum.REST.getValue());
            paramMap.put("notDel", DeleteFlagEnum.NORMAL.getValue());
            paramMap.put("vin", vin);
            paramMap.put("whCode", WhCodeEnum.UNLCN_XN_CQ.getValue());
            paramMap.put("orderBy", "id desc");
            paramMap.put("start", 0);
            paramMap.put("end", 1);
            List<WmsTmsOrder> restData = wmsTmsOrderExtMapper.selectLastRESTInfoByParam(paramMap);
            if (CollectionUtils.isNotEmpty(restData)) {
                //校验是否被删除
                if (WmsInboundOrderStockTransferEnum.OD_DELETED.getCode().equals(restData.get(0).getOdStatus())) {
                    throw new BusinessException("该车架号:" + vin + "已经更新删除,无法入重庆库");
                }
                if (!WhCodeEnum.UNLCN_XN_CQ.getText().equals(restData.get(0).getOdDest())) {
                    throw new BusinessException("该车架号:" + vin + "目的地非前置库");
                }
                //同步到业务表中(order和orderDetail)
                WmsInboundOrder wmsInboundOrder = saveWmsInboundOrderOld(vin, whCode, whName, restData);
                WmsInboundOrderInsertProcedureDTO procedureDTO = new WmsInboundOrderInsertProcedureDTO();
                BeanUtils.copyProperties(wmsInboundOrder, procedureDTO);
                procedureDTO.setVin(vin);
                wmsInboundOrderDetailExtMapper.insertInboundOrderByProcedure(procedureDTO);
                wmsInboundOrder.setOdId(procedureDTO.getOdId());
                //插入关联的detail表
                WmsInboundOrderDetail orderDetail = saveWmsInboundOrderDetailOld(whCode, whName, restData, wmsInboundOrder);
                wmsInboundOrderDetailExtMapper.insertInboundOrderDetailByProcedure(orderDetail);
                // 构造返回bo
                getInspectBo(inspectBO, wmsInboundOrder, orderDetail);

            } else {
                //查询定时方式同步的数据
                paramMap.put("createType", WmsSysSourceEnum.HTTP.getValue());
                List<WmsTmsOrder> httpData = wmsTmsOrderExtMapper.selectLastHTTPInfoByParam(paramMap);
                if (CollectionUtils.isEmpty(httpData)) {
                    throw new BusinessException("未查询到该车辆:" + vin + "的信息");
                }
                //同步到业务表中(order和orderDetail)
                WmsInboundOrder wmsInboundOrder = saveWmsInboundOrderOld(vin, whCode, whName, httpData);
                WmsInboundOrderInsertProcedureDTO procedureDTO = new WmsInboundOrderInsertProcedureDTO();
                BeanUtils.copyProperties(wmsInboundOrder, procedureDTO);
                procedureDTO.setVin(vin);
                wmsInboundOrderDetailExtMapper.insertInboundOrderByProcedure(procedureDTO);
                wmsInboundOrder.setOdId(procedureDTO.getOdId());
                //插入关联的detail表
                WmsInboundOrderDetail orderDetail = saveWmsInboundOrderDetailOld(whCode, whName, httpData, wmsInboundOrder);
                wmsInboundOrderDetailExtMapper.insertInboundOrderDetailByProcedure(orderDetail);
                // 构造返回bo
                getInspectBo(inspectBO, wmsInboundOrder, orderDetail);
            }

        }
        return inspectBO;
    }


    private void getInspectBoByDTO(TmsInboundInspectBO inspectBO, WmsInboundInspectDTO wmsInboundInspectDTO) {
        //构造返回bo
        inspectBO.setInspectStatusValue(wmsInboundInspectDTO.getInspectStatus());
        TmsInspectStatusEnum statusEnum = TmsInspectStatusEnum.getByValue(wmsInboundInspectDTO.getInspectStatus());
        if (statusEnum != null) {
            inspectBO.setInspectStatusText(statusEnum.getText());
        }
        inspectBO.setWaybillCode(wmsInboundInspectDTO.getWaybillCode());
        inspectBO.setGmtCreate(wmsInboundInspectDTO.getGenerateTime());
        inspectBO.setGmtUpdate(wmsInboundInspectDTO.getInspectTime());
        inspectBO.setId(wmsInboundInspectDTO.getId());
        inspectBO.setStyle(wmsInboundInspectDTO.getVehicle());
        inspectBO.setStyleDesc(wmsInboundInspectDTO.getVehicleDesc());
        inspectBO.setVin(wmsInboundInspectDTO.getVin());
    }


    /**
     * 保存tms 的数据到详情表
     *
     * @param whCode          仓库code
     * @param whName          仓库名称
     * @param notices         参数封装对象
     * @param wmsInboundOrder 传入的order
     * @return 返回值
     */
    private WmsInboundOrderDetail saveWmsInboundOrderDetail(String whCode,
                                                            String whName,
                                                            List<WmsWarehouseNoticeHeadDTO> notices,
                                                            WmsInboundOrder wmsInboundOrder) {
        WmsWarehouseNoticeHeadDTO head = notices.get(0);
        WmsWarehouseNoticeDetailDTOForSchedule detail = head.getDetailList().get(0);
        WmsInboundOrderDetail orderDetail = new WmsInboundOrderDetail();
        if (WmsInboundOrderStockTransferEnum.STOCK_NOTCQ.getCode().equals(detail.getWndTransferFlag())) {
            orderDetail.setOddStockTransfer(WmsInboundOrderStockTransferEnum.STOCK_NOTCQ.getCode());
            orderDetail.setOddStockTransferStatus(WmsInboundOrderStockTransferEnum.ST_STATUS_NOTDEAL.getCode());
        } else {
            orderDetail.setOddStockTransfer(WmsInboundOrderStockTransferEnum.STOCK_NORMAL.getCode());
            orderDetail.setOddStockTransferStatus(WmsInboundOrderStockTransferEnum.ST_STATUS_10.getCode());
        }
        orderDetail.setOddOdCode(String.valueOf(detail.getWndId()));
        orderDetail.setIsDeleted(DeleteFlagEnum.NORMAL.getValue());
        orderDetail.setGmtCreate(new Date());
        orderDetail.setGmtUpdate(new Date());
        orderDetail.setFactoryWhno(detail.getWndCustomerWhno());
        orderDetail.setOddVin(detail.getWndVin());
        orderDetail.setOddWhCode(whCode);
        orderDetail.setOddWhName(whName);
        orderDetail.setOddVehicleSpecCode(detail.getWndVehicleCode());
        orderDetail.setOddVehicleSpecName(detail.getWndVehicleName());
        orderDetail.setOddVehicleSpecDesc(detail.getWndVehicleDesc());
        orderDetail.setOddOdId(wmsInboundOrder.getOdId());
        return orderDetail;
    }

    /**
     * 保存tms传送的order数据
     *
     * @param vin      车架号
     * @param whCode   仓库code
     * @param whName   仓库名称
     * @param restData 参数对象
     * @return 返回值
     */
    private WmsInboundOrder saveWmsInboundOrder(String vin,
                                                String whCode,
                                                String whName,
                                                List<WmsWarehouseNoticeHeadDTO> restData) {
        WmsWarehouseNoticeHeadDTO head = restData.get(0);
        WmsWarehouseNoticeDetailDTOForSchedule detail = head.getDetailList().get(0);
        WmsInboundOrder wmsInboundOrder = new WmsInboundOrder();
        wmsInboundOrder.setOdCode(String.valueOf(head.getWnhId()));
        wmsInboundOrder.setOdCustomerOrderNo(head.getWnhCustomerOrderNo());//客户订单
        wmsInboundOrder.setOdWaybillNo(head.getWnhCustomerWaybillNo());//客户运单号
        wmsInboundOrder.setOdWaybillType(head.getWnhSysSource());//系统来源
        wmsInboundOrder.setOdStatus(String.valueOf(WmsInboundStatusEnum.WMS_INBOUND_CREATE.getValue()));//订单状态
        wmsInboundOrder.setOdCustomerName(head.getWnhCustomerName());
        wmsInboundOrder.setOdWhCode(whCode);
        wmsInboundOrder.setOdWhName(whName);
        wmsInboundOrder.setOdCode(String.valueOf(head.getWnhId()));
        wmsInboundOrder.setInspectStatus(TmsInspectStatusEnum.WAYBILL_INIT.getValue());
        if (detail.getWndVehicleId() == null) {
            throw new BusinessException("TMS接口该车架号:" + vin + "没有对应车型ID数据");
        }
        //车型id
        wmsInboundOrder.setOdStyleId(detail.getWndVehicleId());
        wmsInboundOrder.setIsDeleted(DeleteFlagEnum.NORMAL.getValue());
        wmsInboundOrder.setOdOrigin(head.getWnhOriginName());
        wmsInboundOrder.setOdDest(head.getWnhDestinationName());
        wmsInboundOrder.setOdOrderno(head.getWnhUnlcnOrderNo());
        wmsInboundOrder.setGmtCreate(new Date());
        wmsInboundOrder.setGmtUpdate(new Date());
        wmsInboundOrder.setOdSendMailFlag(InventoryLocationStatusEnum.MAI_NOT_SEND.getCode());
        return wmsInboundOrder;
    }

    /**
     * 保存tms 的数据到详情表
     *
     * @param whCode          仓库code
     * @param whName          仓库名称
     * @param tmsOrders       参数封装对象
     * @param wmsInboundOrder 传入的order
     * @return 返回值
     */
    private WmsInboundOrderDetail saveWmsInboundOrderDetailOld(String whCode, String whName, List<WmsTmsOrder> tmsOrders, WmsInboundOrder wmsInboundOrder) {
        WmsTmsOrder tmsOrder = tmsOrders.get(0);
        WmsInboundOrderDetail orderDetail = new WmsInboundOrderDetail();
        if (WmsInboundOrderStockTransferEnum.STOCK_NOTCQ.getCode().equals(tmsOrder.getOddStockTransfer())) {
            orderDetail.setOddStockTransfer(WmsInboundOrderStockTransferEnum.STOCK_NOTCQ.getCode());
            orderDetail.setOddStockTransferStatus(WmsInboundOrderStockTransferEnum.ST_STATUS_NOTDEAL.getCode());
        } else {
            orderDetail.setOddStockTransfer(WmsInboundOrderStockTransferEnum.STOCK_NORMAL.getCode());
            orderDetail.setOddStockTransferStatus(WmsInboundOrderStockTransferEnum.ST_STATUS_10.getCode());
        }
        orderDetail.setIsDeleted(DeleteFlagEnum.NORMAL.getValue());
        orderDetail.setGmtCreate(new Date());
        orderDetail.setGmtUpdate(new Date());
        orderDetail.setFactoryWhno(tmsOrder.getFactoryWhno());
        orderDetail.setOddVin(tmsOrder.getOddVin());
        orderDetail.setOddWhCode(whCode);
        orderDetail.setOddWhName(whName);
        orderDetail.setOddVehicleSpecName(tmsOrder.getOddVehicleSpecName());
        orderDetail.setOddVehicleSpecDesc(tmsOrder.getOddVehicleSpecDesc());
        orderDetail.setOddOdId(wmsInboundOrder.getOdId());
        return orderDetail;
    }


    /**
     * 保存tms传送的order数据
     *
     * @param vin       车架号
     * @param whCode    仓库code
     * @param whName    仓库名称
     * @param tmsOrders 参数对象
     * @return 返回值
     */
    @Deprecated
    private WmsInboundOrder saveWmsInboundOrderOld(String vin, String whCode, String whName, List<WmsTmsOrder> tmsOrders) {
        WmsTmsOrder tmsOrder = tmsOrders.get(0);
        WmsInboundOrder wmsInboundOrder = new WmsInboundOrder();
        wmsInboundOrder.setOdCustomerOrderNo(tmsOrder.getOdCustomerOrderNo());//客户订单
        wmsInboundOrder.setOdWaybillNo(tmsOrder.getOdWaybillNo());//客户运单号
        wmsInboundOrder.setOdWaybillType(WmsSysSourceEnum.TMS.getValue());//系统来源
        wmsInboundOrder.setOdStatus(String.valueOf(WmsInboundStatusEnum.WMS_INBOUND_CREATE.getValue()));//订单状态
        wmsInboundOrder.setOdCustomerName(tmsOrder.getOdCustomerName());
        wmsInboundOrder.setOdWhCode(whCode);
        wmsInboundOrder.setOdWhName(whName);
        wmsInboundOrder.setOdCode(String.valueOf(tmsOrder.getId()));
        wmsInboundOrder.setInspectStatus(TmsInspectStatusEnum.WAYBILL_INIT.getValue());
        if (tmsOrder.getOdStyleId() == null) {
            throw new BusinessException("TMS接口该车架号:" + vin + "没有对应车型ID数据");
        }
        //车型id
        wmsInboundOrder.setOdStyleId(tmsOrder.getOdStyleId());
        wmsInboundOrder.setIsDeleted(DeleteFlagEnum.NORMAL.getValue());
        wmsInboundOrder.setOdOrigin(tmsOrder.getOdOrigin());
        wmsInboundOrder.setOdDest(tmsOrder.getOdDest());
        wmsInboundOrder.setOdOrderno(tmsOrder.getOdOrderno());
        wmsInboundOrder.setGmtCreate(new Date());
        wmsInboundOrder.setGmtUpdate(new Date());
        wmsInboundOrder.setOdSendMailFlag(InventoryLocationStatusEnum.MAI_NOT_SEND.getCode());
        return wmsInboundOrder;
    }

    /**
     * 查询验车信息的bo字段设置
     */
    private void getInspectBo(TmsInboundInspectBO inspectBO, WmsInboundOrder wmsInboundOrder, WmsInboundOrderDetail orderDetail) {
        inspectBO.setInspectStatusValue(wmsInboundOrder.getInspectStatus());
        TmsInspectStatusEnum statusEnum = TmsInspectStatusEnum.getByValue(wmsInboundOrder.getInspectStatus());
        if (statusEnum != null) {
            inspectBO.setInspectStatusText(statusEnum.getText());
        }
        inspectBO.setWaybillCode(wmsInboundOrder.getOdWaybillNo());
        inspectBO.setGmtCreate(wmsInboundOrder.getGmtCreate());
        inspectBO.setGmtUpdate(wmsInboundOrder.getInspectTime());//验车时间
        inspectBO.setId(wmsInboundOrder.getOdId());
        inspectBO.setStyle(orderDetail.getOddVehicleSpecName());
        inspectBO.setStyleDesc(orderDetail.getOddVehicleSpecDesc());
        inspectBO.setVin(orderDetail.getOddVin());
    }


    /**
     * 更新时间戳--支持维修分批显示异常
     */
    private void updateExcpTypeAndTimestampToOneBatch(WmsInboundOrder bill, int count1, int count2) {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss:SSS");
        String timestamp = sdf.format(new Date());
        if (count1 >= 1 && count2 >= 1) {
            //两种类型的异常  查询对应code的异常
            updateExcpType(bill, WmsInspectExcpTypeEnum.TYPE_MISS_AND_EXCP.getCode());
            //1. 更新异常明细的类型
            TmsInspectExcp inspectExcp = new TmsInspectExcp();
            inspectExcp.setExcpTypeCode(WmsInspectExcpTypeEnum.TYPE_MISS_AND_EXCP.getCode());
            inspectExcp.setGmtUpdate(new Date());
            inspectExcp.setExcpTimestamp(timestamp);
            TmsInspectExcpExample excpExample = new TmsInspectExcpExample();
            //未删除的异常
            excpExample.or().andInspectIdEqualTo(bill.getOdId())
                    .andExcpRepairIdIsNull()
                    .andDelFlagEqualTo(true);
            //维修合格的异常
            excpExample.or().andInspectIdEqualTo(bill.getOdId())
                    .andExcpRepairIdIsNull()
                    .andDelFlagEqualTo(false)
                    .andExcpTimestampIsNotNull();//说明以前登记过的异常 只是现在维修合格了
            tmsInspectExcpMapper.updateByExampleSelective(inspectExcp, excpExample);
            //2 更新缺件明细的类型
            TmsInspectComponentMiss componentMiss = new TmsInspectComponentMiss();
            componentMiss.setExcpTypeCode(WmsInspectExcpTypeEnum.TYPE_MISS_AND_EXCP.getCode());
            componentMiss.setGmtUpdate(new Date());
            componentMiss.setExcpTimestamp(timestamp);
            TmsInspectComponentMissExample componentMissExample = new TmsInspectComponentMissExample();
            componentMissExample.or().andInspectIdEqualTo(bill.getOdId())
                    .andExcpRepairIdIsNull()
                    .andIsDeletedEqualTo(DeleteFlagEnum.NORMAL.getValue());
            componentMissExample.or().andInspectIdEqualTo(bill.getOdId())
                    .andExcpRepairIdIsNull()
                    .andIsDeletedEqualTo(DeleteFlagEnum.DELETED.getValue())
                    .andExcpTimestampIsNotNull();
            tmsInspectComponentMissMapper.updateByExampleSelective(componentMiss, componentMissExample);
        }
        if (count1 >= 1 && count2 < 1) {
            //一种类型的异常,非质损异常
            updateExcpType(bill, WmsInspectExcpTypeEnum.TYPE_EXCP.getCode());
            //1. 更新异常明细的类型
            TmsInspectExcp inspectExcp = new TmsInspectExcp();
            inspectExcp.setExcpTypeCode(WmsInspectExcpTypeEnum.TYPE_EXCP.getCode());
            inspectExcp.setGmtUpdate(new Date());
            inspectExcp.setExcpTimestamp(timestamp);
            TmsInspectExcpExample excpExample = new TmsInspectExcpExample();
            excpExample.or().andInspectIdEqualTo(bill.getOdId())
                    .andExcpRepairIdIsNull()
                    .andDelFlagEqualTo(true);
            excpExample.or().andInspectIdEqualTo(bill.getOdId())
                    .andExcpRepairIdIsNull()
                    .andDelFlagEqualTo(false)
                    .andExcpTimestampIsNotNull();
            tmsInspectExcpMapper.updateByExampleSelective(inspectExcp, excpExample);
        }
        if (count1 < 1 && count2 >= 1) {
            //一种类型的异常,缺件异常
            updateExcpType(bill, WmsInspectExcpTypeEnum.TYPE_MISS.getCode());
            //2 更新缺件明细的类型
            TmsInspectComponentMiss componentMiss = new TmsInspectComponentMiss();
            componentMiss.setExcpTypeCode(WmsInspectExcpTypeEnum.TYPE_MISS.getCode());
            componentMiss.setGmtUpdate(new Date());
            componentMiss.setExcpTimestamp(timestamp);
            TmsInspectComponentMissExample componentMissExample = new TmsInspectComponentMissExample();
            componentMissExample.or().andInspectIdEqualTo(bill.getOdId())
                    .andExcpRepairIdIsNull()
                    .andIsDeletedEqualTo(DeleteFlagEnum.NORMAL.getValue());
            componentMissExample.or().andInspectIdEqualTo(bill.getOdId())
                    .andExcpRepairIdIsNull()
                    .andIsDeletedEqualTo(DeleteFlagEnum.DELETED.getValue())
                    .andExcpTimestampIsNotNull();
            tmsInspectComponentMissMapper.updateByExampleSelective(componentMiss, componentMissExample);
        }
    }


    /**
     * 更新时间戳--支持维修分批显示异常
     */
    private void updateExcpTypeAndTimestamp(WmsInboundOrder bill, int count1, int count2) {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss:SSS");
        String timestamp = sdf.format(new Date());
        if (count1 >= 1 && count2 >= 1) {
            //两种类型的异常  查询对应code的异常
            updateExcpType(bill, WmsInspectExcpTypeEnum.TYPE_MISS_AND_EXCP.getCode());
            //1. 更新异常明细的类型
            TmsInspectExcp inspectExcp = new TmsInspectExcp();
            inspectExcp.setExcpTypeCode(WmsInspectExcpTypeEnum.TYPE_MISS_AND_EXCP.getCode());
            inspectExcp.setGmtUpdate(new Date());
            inspectExcp.setExcpTimestamp(timestamp);
            TmsInspectExcpExample excpExample = new TmsInspectExcpExample();
            excpExample.createCriteria().andInspectIdEqualTo(bill.getOdId())
                    .andDelFlagNotEqualTo(false)
                    .andExcpTimestampIsNull();
            tmsInspectExcpMapper.updateByExampleSelective(inspectExcp, excpExample);
            //2 更新缺件明细的类型
            TmsInspectComponentMiss componentMiss = new TmsInspectComponentMiss();
            componentMiss.setExcpTypeCode(WmsInspectExcpTypeEnum.TYPE_MISS_AND_EXCP.getCode());
            componentMiss.setGmtUpdate(new Date());
            componentMiss.setExcpTimestamp(timestamp);
            TmsInspectComponentMissExample componentMissExample = new TmsInspectComponentMissExample();
            componentMissExample.createCriteria().andInspectIdEqualTo(bill.getOdId())
                    .andIsDeletedNotEqualTo(DeleteFlagEnum.DELETED.getValue())
                    .andExcpTimestampIsNull();
            tmsInspectComponentMissMapper.updateByExampleSelective(componentMiss, componentMissExample);
        }
        if (count1 >= 1 && count2 < 1) {
            //一种类型的异常,非质损异常
            updateExcpType(bill, WmsInspectExcpTypeEnum.TYPE_EXCP.getCode());
            //1. 更新异常明细的类型
            TmsInspectExcp inspectExcp = new TmsInspectExcp();
            inspectExcp.setExcpTypeCode(WmsInspectExcpTypeEnum.TYPE_EXCP.getCode());
            inspectExcp.setGmtUpdate(new Date());
            inspectExcp.setExcpTimestamp(timestamp);
            TmsInspectExcpExample excpExample = new TmsInspectExcpExample();
            excpExample.createCriteria().andInspectIdEqualTo(bill.getOdId())
                    .andDelFlagNotEqualTo(false)
                    .andExcpTimestampIsNull();
            tmsInspectExcpMapper.updateByExampleSelective(inspectExcp, excpExample);
        }
        if (count1 < 1 && count2 >= 1) {
            //一种类型的异常,缺件异常
            updateExcpType(bill, WmsInspectExcpTypeEnum.TYPE_MISS.getCode());
            //2 更新缺件明细的类型
            TmsInspectComponentMiss componentMiss = new TmsInspectComponentMiss();
            componentMiss.setExcpTypeCode(WmsInspectExcpTypeEnum.TYPE_MISS.getCode());
            componentMiss.setGmtUpdate(new Date());
            componentMiss.setExcpTimestamp(timestamp);
            TmsInspectComponentMissExample componentMissExample = new TmsInspectComponentMissExample();
            componentMissExample.createCriteria().andInspectIdEqualTo(bill.getOdId())
                    .andIsDeletedNotEqualTo(DeleteFlagEnum.DELETED.getValue())
                    .andExcpTimestampIsNull();
            tmsInspectComponentMissMapper.updateByExampleSelective(componentMiss, componentMissExample);
        }
    }

    /**
     * 删除老的缺件异常  插入新的异常  支持多次维修分批显示异常
     */
    private int updateMissToDelAndInsertNew(Long id, int count2) {
        TmsInspectComponentMissExample missExample = new TmsInspectComponentMissExample();
        missExample.createCriteria().andIsDeletedNotEqualTo(DeleteFlagEnum.DELETED.getValue())
                .andInspectIdEqualTo(id);
        List<TmsInspectComponentMiss> tmsInspectComponentMisses = tmsInspectComponentMissMapper.selectByExample(missExample);
        if (CollectionUtils.isNotEmpty(tmsInspectComponentMisses)) {
            count2 = tmsInspectComponentMisses.size();
            //删除老的
            tmsInspectComponentMisses.forEach(v -> {
                v.setGmtUpdate(new Date());
                v.setIsDeleted(DeleteFlagEnum.DELETED.getValue());
                tmsInspectComponentMissMapper.updateByPrimaryKeySelective(v);
                //插入新的
                TmsInspectComponentMiss miss = new TmsInspectComponentMiss();
                BeanUtils.copyProperties(v, miss);
                miss.setGmtUpdate(new Date());
                miss.setGmtCreate(new Date());
                miss.setExcpStatus(ExcpStatusEnum.NOT_DEAL.getCode());
                miss.setIsDeleted(DeleteFlagEnum.NORMAL.getValue());
                miss.setId(null);
                miss.setExcpTimestamp(null);
                tmsInspectComponentMissMapper.insertSelective(miss);
            });
        }
        return count2;
    }

    /**
     * 验车登记时删除老的异常，插入新的异常  支持多次维修分批显示异常
     */
    private int updateExcpToDelAndInsertNew(Long id, int count1) {
        //为了分批显示异常状态，需要先删除老的异常。再插入新的异常
        TmsInspectExcpExample excpExample = new TmsInspectExcpExample();
        excpExample.createCriteria().andDelFlagNotEqualTo(false)
                .andInspectIdEqualTo(id);
        List<TmsInspectExcp> tmsInspectExcps = tmsInspectExcpMapper.selectByExample(excpExample);
        //删除关联的异常图片
        if (CollectionUtils.isNotEmpty(tmsInspectExcps)) {
            count1 = tmsInspectExcps.size();
            tmsInspectExcps.forEach((TmsInspectExcp v) -> {
                //删除旧的异常--逻辑
                v.setGmtUpdate(new Date());
                v.setDelFlag(false);
                tmsInspectExcpMapper.updateByPrimaryKeySelective(v);
                //插入新的
                TmsInspectExcp inspectExcp = new TmsInspectExcp();
                BeanUtils.copyProperties(v, inspectExcp);
                inspectExcp.setDelFlag(true);
                inspectExcp.setId(null);
                inspectExcp.setExcpStatus(ExcpStatusEnum.NOT_DEAL.getCode());
                inspectExcp.setGmtCreate(new Date());
                inspectExcp.setGmtUpdate(new Date());
                inspectExcp.setExcpTimestamp(null);
                tmsInspectExcpMapper.insertSelective(inspectExcp);
                TmsInspectExcpAttachsExample attachsExample = new TmsInspectExcpAttachsExample();
                attachsExample.createCriteria().andDelFlagNotEqualTo(false)
                        .andExcpIdEqualTo(v.getId());
                List<TmsInspectExcpAttachs> tmsInspectExcpAttachs = tmsInspectExcpAttachsMapper.selectByExample(attachsExample);
                if (CollectionUtils.isNotEmpty(tmsInspectExcpAttachs)) {
                    tmsInspectExcpAttachs.forEach(attach -> {
                        attach.setGmtUpdate(new Date());
                        attach.setDelFlag(false);
                        tmsInspectExcpAttachsMapper.updateByPrimaryKeySelective(attach);
                        //插入新的
                        TmsInspectExcpAttachs excpAttachs = new TmsInspectExcpAttachs();
                        excpAttachs.setDelFlag(true);
                        excpAttachs.setGmtCreate(new Date());
                        excpAttachs.setGmtUpdate(new Date());
                        excpAttachs.setPicKey(attach.getPicKey());
                        excpAttachs.setExcpId(inspectExcp.getId());//
                        tmsInspectExcpAttachsMapper.insertSelective(excpAttachs);
                    });
                }
            });
        }
        return count1;
    }

    private void updateExcpType(WmsInboundOrder bill, String typeCode) {
        TmsInspectExcpTypeExample typeExample = new TmsInspectExcpTypeExample();
        typeExample.createCriteria().andDelFlagNotEqualTo(false)
                .andTypeCodeEqualTo(typeCode);
        List<TmsInspectExcpType> tmsInspectExcpTypes = tmsInspectExcpTypeMapper.selectByExample(typeExample);
        if (CollectionUtils.isNotEmpty(tmsInspectExcpTypes)) {
            //设置入库单关联对应的id
            bill.setOdInspectExcpType(tmsInspectExcpTypes.get(0).getId());
        }
    }


    private void saveLog(Long inspectId, String userId, TmsInspectLog log) {
        log.setMsg("取消验车成功!");
        log.setUserId(userId);
        log.setGmtCreate(new Date());
        log.setGmtUpdate(new Date());
        log.setInspectId(inspectId);
    }

    /**
     * ASN表同步记录到业务表中(order和orderDetail)
     * <p>
     * 2018-4-20 重构入库通知单
     * </p>
     *
     * @param noticeData   通知单data
     * @param whCode       仓库code
     * @param whName       仓库名称
     * @param inboundOrder order表-主
     * @param orderDetail  detail表-子
     */
    private void SyncNoticeToOrder(List<WmsWarehouseNoticeHeadDTO> noticeData,
                                   String whCode,
                                   String whName,
                                   WmsInboundOrder inboundOrder,
                                   WmsInboundOrderDetail orderDetail) {
        WmsWarehouseNoticeHeadDTO head = noticeData.get(0);
        WmsWarehouseNoticeDetailDTOForSchedule detail = head.getDetailList().get(0);
        inboundOrder.setOdCode(wmsBatchNoService.addSerialNum("IB", "IB"));
        inboundOrder.setOdCustomerOrderNo(head.getWnhCustomerOrderNo());
        inboundOrder.setOdCustomerCode(head.getWnhCustomerCode());
        inboundOrder.setOdCustomerName(head.getWnhCustomerName());
        inboundOrder.setOdWaybillType(head.getWnhSysSource());// 系统来源
        inboundOrder.setOdStatus(String.valueOf(WmsInboundStatusEnum.WMS_INBOUND_CREATE.getValue()));// 订单状态
        inboundOrder.setOdWhCode(whCode);
        inboundOrder.setOdWhName(whName);
        inboundOrder.setInspectStatus(TmsInspectStatusEnum.WAYBILL_INIT.getValue());
        inboundOrder.setGmtCreate(new Date());
        inboundOrder.setGmtUpdate(new Date());
        inboundOrder.setIsDeleted(DeleteFlagEnum.NORMAL.getValue());
        inboundOrder.setOdBusinessType(detail.getWndBusinessType());
        inboundOrder.setOdBatchNo(head.getWnhBatchNo());//批次号
        wmsInboundOrderMapper.insertSelective(inboundOrder);
        orderDetail.setOddVin(detail.getWndVin());
        orderDetail.setOddOdId(inboundOrder.getOdId());
        orderDetail.setOddWhCode(whCode);
        orderDetail.setOddWhName(whName);
        orderDetail.setOddOdCode(inboundOrder.getOdCode());
        orderDetail.setGmtCreate(new Date());
        orderDetail.setGmtUpdate(new Date());
        orderDetail.setIsDeleted(DeleteFlagEnum.NORMAL.getValue());
        orderDetail.setOddCarColourCode(detail.getWndColorCode());
        orderDetail.setOddCarColour(detail.getWndColorName());
        orderDetail.setOddMaterialCode(detail.getWndMaterialCode());
        orderDetail.setOddMaterialName(detail.getWndMaterialName());
        orderDetail.setOddVehicleSpecCode(detail.getWndVehicleCode());
        orderDetail.setOddVehicleSpecName(detail.getWndVehicleName());
        orderDetail.setOddEngine(detail.getWndEngineNo());
        wmsInboundOrderDetailMapper.insertSelective(orderDetail);
        // 更新asn状态为已同步
        detail.setWndSendBusinessFlag(String.valueOf(SendBusinessFlagEnum.SEND_Y.getValue()));
        detail.setWndUpdate(new Date());
        WmsWarehouseNoticeDetail noticeDetail = new WmsWarehouseNoticeDetail();
        BeanUtils.copyProperties(detail, noticeDetail);
        wmsWarehouseNoticeDetailMapper.updateByPrimaryKeySelective(noticeDetail);
    }

    /**
     * ASN表同步记录到业务表中(order和orderDetail)
     * <p>
     * 2018-4-20 重构入库通知单  此方法过时  {@link TmsInspectServiceImpl#SyncNoticeToOrder}
     * </p>
     *
     * @param wmsAsnTemp   asn接口表
     * @param whCode       仓库code
     * @param whName       仓库名称
     * @param inboundOrder order表-主
     * @param orderDetail  detail表-子
     */
    @Deprecated
    private void SyncAsnToOrderOld(WmsAsnTemp wmsAsnTemp, String whCode, String whName,
                                   WmsInboundOrder inboundOrder, WmsInboundOrderDetail orderDetail) {
        inboundOrder.setOdCode(wmsBatchNoService.addSerialNum("IB", "IB"));
        inboundOrder.setOdCustomerOrderNo(wmsAsnTemp.getAtCustomerOrderNo());
        inboundOrder.setOdCustomerCode(WmsCustomerEnum.JM.getCode());
        inboundOrder.setOdCustomerName(wmsAsnTemp.getAtCustomerName());
        inboundOrder.setOdDispatchNo(wmsAsnTemp.getAtDispatchNo());// 调度单号对应tms指令号
        inboundOrder.setOdWaybillNo(wmsAsnTemp.getAtCustshipno());// 运单号
        inboundOrder.setOdWaybillType(WmsSysSourceEnum.DCS_IMPORT.getValue());// 系统来源
        inboundOrder.setOdSupplierName(wmsAsnTemp.getAtSupplierName());
        inboundOrder.setOdStatus(String.valueOf(WmsInboundStatusEnum.WMS_INBOUND_CREATE.getValue()));// 订单状态
        inboundOrder.setOdOrigin(wmsAsnTemp.getAtOrigin());
        inboundOrder.setOdDest(wmsAsnTemp.getAtDest());
        inboundOrder.setOdWhCode(whCode);
        inboundOrder.setOdWhName(whName);
        inboundOrder.setInspectStatus(TmsInspectStatusEnum.WAYBILL_INIT.getValue());
        inboundOrder.setGmtCreate(new Date());
        inboundOrder.setGmtUpdate(new Date());
        inboundOrder.setIsDeleted(DeleteFlagEnum.NORMAL.getValue());
        inboundOrder.setOdBusinessType(WmsOutOfStorageType.ZTYPE_INBOUND_Z1.getValue());
        inboundOrder.setOdBatchNo(wmsAsnTemp.getAtBatchNo());//批次号
        wmsInboundOrderMapper.insertSelective(inboundOrder);
        orderDetail.setOddVin(wmsAsnTemp.getAtVin());
        orderDetail.setOddOdId(inboundOrder.getOdId());
        orderDetail.setOddWhCode(whCode);
        orderDetail.setOddWhName(whName);
        orderDetail.setOddVehiclePlate(wmsAsnTemp.getAtSupplierVehiclePlate());// 板车车牌号
        orderDetail.setOddOdCode(inboundOrder.getOdCode());
        orderDetail.setGmtCreate(new Date());
        orderDetail.setGmtUpdate(new Date());
        orderDetail.setIsDeleted(DeleteFlagEnum.NORMAL.getValue());
        orderDetail.setOddCarColourCode(wmsAsnTemp.getAtCarColourCode());
        orderDetail.setOddCarColour(wmsAsnTemp.getAtCarColour());
        orderDetail.setOddMaterialCode(wmsAsnTemp.getAtMaterialCode());
        orderDetail.setOddMaterialName(wmsAsnTemp.getAtMaterialName());
        orderDetail.setOddVehicleSpecCode(wmsAsnTemp.getAtVehicleCode());
        orderDetail.setOddVehicleSpecName(wmsAsnTemp.getAtVehicleName());
        orderDetail.setOddEngine(wmsAsnTemp.getAtEngineNumber());
        wmsInboundOrderDetailMapper.insertSelective(orderDetail);

        // 更新asn状态为已同步
        wmsAsnTemp.setAtSendBusinessFlag(String.valueOf(SendBusinessFlagEnum.SEND_Y.getValue()));
        wmsAsnTemp.setGmtUpdate(new Date());
        wmsAsnTempMapper.updateByPrimaryKeySelective(wmsAsnTemp);
    }

    /**
     * 同步还车单到order业务表
     */
    private void SyncReturnToOrder(WmsBorrowCarDetail borrowCarDetail,
                                   String whCode, String whName, WmsInboundOrder inboundOrder,
                                   WmsInboundOrderDetail orderDetail) {
        inboundOrder.setOdCode(wmsBatchNoService.addSerialNum("IB", "IB"));
        inboundOrder.setOdCustomerCode(WmsCustomerEnum.JM.getCode());
        inboundOrder.setOdCustomerName(WmsCustomerEnum.JM.getName());
        inboundOrder.setOdWaybillType(WmsSysSourceEnum.WMS_RETURN.getValue());// 系统来源
        inboundOrder.setOdStatus(String.valueOf(WmsInboundStatusEnum.WMS_INBOUND_CREATE.getValue()));// 订单状态
        inboundOrder.setOdWhCode(whCode);
        inboundOrder.setOdWhName(whName);
        inboundOrder.setInspectStatus(TmsInspectStatusEnum.WAYBILL_INIT.getValue());
        inboundOrder.setGmtCreate(new Date());
        inboundOrder.setGmtUpdate(new Date());
        //找批次号,上次的借车要用上次的因为不走asn,但是该车一定是以前asn导入的,再将信息同步过来
        //WmsAsnTempExample asnTempExample = new WmsAsnTempExample();
        //asnTempExample.createCriteria().andAtVinEqualTo(borrowCarDetail.getBdVin())
        //        .andAtBatchNoIsNotNull()
        //        .andAtSendBusinessFlagEqualTo(String.valueOf(SendBusinessFlagEnum.SEND_Y.getValue()));
        //List<WmsAsnTemp> wmsAsnTemps = wmsAsnTempMapper.selectByExample(asnTempExample);
        //找批次号,上次的借车要用上次的因为不走asn,但是该车一定是以前asn导入的,再将信息同步过来
        HashMap<String, Object> params = Maps.newHashMap();
        params.put("notDel", DeleteFlagEnum.NORMAL.getValue());
        params.put("vin", borrowCarDetail.getBdVin());
        params.put("whCode", whCode);
        params.put("orderBy", "a.wnh_id desc");
        params.put("start", 0);
        params.put("end", 1);
        List<WmsWarehouseNoticeHeadDTO> heads = wmsWarehouseNoticeExtMapper.selectBatchNoForSyncPreparationToOrder(params);
        if (CollectionUtils.isNotEmpty(heads)) {
            WmsWarehouseNoticeHeadDTO headDTO = heads.get(0);
            inboundOrder.setOdBatchNo(headDTO.getWnhBatchNo());
        }
        inboundOrder.setIsDeleted(DeleteFlagEnum.NORMAL.getValue());
        inboundOrder.setOdBusinessType(WmsOutOfStorageType.ZTYPE_INBOUND_Z8.getValue());
        wmsInboundOrderMapper.insertSelective(inboundOrder);

        //查上一次入库的车的物料code
        WmsInboundOrderDetailExample detailExample = new WmsInboundOrderDetailExample();
        detailExample.createCriteria().andOddVinEqualTo(borrowCarDetail.getBdVin())
                .andIsDeletedNotEqualTo(DeleteFlagEnum.DELETED.getValue())
                .andOddVehicleSpecCodeIsNotNull()
                .andOddCarColourCodeIsNotNull();
        List<WmsInboundOrderDetail> wmsInboundOrderDetails = wmsInboundOrderDetailMapper.selectByExample(detailExample);
        if (CollectionUtils.isNotEmpty(wmsInboundOrderDetails)) {
            WmsInboundOrderDetail oldDetail = wmsInboundOrderDetails.get(0);
            orderDetail.setOddVehicleSpecCode(oldDetail.getOddVehicleSpecCode());
            orderDetail.setOddCarColourCode(oldDetail.getOddCarColourCode());
            orderDetail.setOddEngine(oldDetail.getOddEngine());
            orderDetail.setOddMaterialCode(oldDetail.getOddMaterialCode());
        }

        orderDetail.setOddOdCode(inboundOrder.getOdCode());
        orderDetail.setOddVehicleSpecName(borrowCarDetail.getBdCarSpecname());
        orderDetail.setOddMaterialName(borrowCarDetail.getBdMaterialName());
        orderDetail.setOddCarColour(borrowCarDetail.getBdCarColorname());
        orderDetail.setOddVin(borrowCarDetail.getBdVin());
        orderDetail.setOddMaterialCode(borrowCarDetail.getBdMaterialCode());
        orderDetail.setOddOdId(inboundOrder.getOdId());
        orderDetail.setOddWhCode(whCode);
        orderDetail.setOddWhName(whName);

//		orderDetail.setOddVehiclePlate();// 板车车牌号
        orderDetail.setOddOdCode(inboundOrder.getOdCode());
        orderDetail.setGmtCreate(new Date());
        orderDetail.setGmtUpdate(new Date());
        orderDetail.setIsDeleted(DeleteFlagEnum.NORMAL.getValue());
        wmsInboundOrderDetailMapper.insertSelective(orderDetail);

        // 更新asn状态为已同步
        /*returnCar.setSendBusinessFlag(HAS_SEND);
        returnCar.setGmtUpdate(new Date());
        wmsReturnCarMapper.updateByPrimaryKeySelective(returnCar);*/
    }

    /**
     * 同步退车单到order业务表
     */
    private void SyncDealerCarBackToOrder(WmsDealerCarBack carBack,
                                          String whCode, String whName, WmsInboundOrder inboundOrder,
                                          WmsInboundOrderDetail orderDetail) {

        inboundOrder.setOdWaybillNo(carBack.getCbApplyNo());// 运单号
        inboundOrder.setInspectStatus(TmsInspectStatusEnum.WAYBILL_INIT.getValue());
        inboundOrder.setGmtCreate(new Date());
        inboundOrder.setOdDispatchNo(carBack.getCbApplyNo());// 调度单号对应退车接口表的退车单号
        inboundOrder.setOdWaybillType(WmsSysSourceEnum.DCS_DEALER_BACK.getValue());// 系统来源
        inboundOrder.setOdStatus(String.valueOf(WmsInboundStatusEnum.WMS_INBOUND_CREATE.getValue()));// 订单状态
        inboundOrder.setOdWhCode(whCode);
        inboundOrder.setOdWhName(whName);

        inboundOrder.setOdCode(wmsBatchNoService.addSerialNum("IB", "IB"));
        inboundOrder.setOdCustomerCode(WmsCustomerEnum.JM.getCode());
        inboundOrder.setOdCustomerName(WmsCustomerEnum.JM.getName());
        //找批次号,上次的借车要用上次的因为不走asn,但是该车一定是以前asn导入的,再将信息同步过来
        inboundOrder.setInspectStatus(TmsInspectStatusEnum.WAYBILL_INIT.getValue());
        inboundOrder.setGmtCreate(new Date());
        inboundOrder.setGmtUpdate(new Date());
        inboundOrder.setIsDeleted(DeleteFlagEnum.NORMAL.getValue());
        inboundOrder.setOdBusinessType(WmsOutOfStorageType.ZTYPE_INBOUND_Z9.getValue());
        wmsInboundOrderMapper.insertSelective(inboundOrder);


        //查上一次入库的车的物料code
        WmsInboundOrderDetailExample detailExample = new WmsInboundOrderDetailExample();
        detailExample.createCriteria()
                .andOddWhCodeEqualTo(whCode)
                .andOddVinEqualTo(carBack.getCbVin())
                .andOddVehicleSpecCodeIsNotNull()
                .andOddCarColourIsNotNull();
        List<WmsInboundOrderDetail> wmsInboundOrderDetails = wmsInboundOrderDetailMapper.selectByExample(detailExample);
        if (CollectionUtils.isNotEmpty(wmsInboundOrderDetails)) {
            WmsInboundOrderDetail oldDetail = wmsInboundOrderDetails.get(0);
            orderDetail.setOddVehicleSpecCode(oldDetail.getOddVehicleSpecCode());
            orderDetail.setOddVehicleSpecName(oldDetail.getOddVehicleSpecName());
            orderDetail.setOddCarColourCode(oldDetail.getOddCarColourCode());
            orderDetail.setOddCarColour(oldDetail.getOddCarColour());
            orderDetail.setOddEngine(oldDetail.getOddEngine());
        }

        orderDetail.setOddMaterialCode(carBack.getCbMaterialCode());
        orderDetail.setOddMaterialName(carBack.getCbMaterialName());
        orderDetail.setOddVin(carBack.getCbVin());
        orderDetail.setOddOdId(inboundOrder.getOdId());
        orderDetail.setOddWhCode(whCode);
        orderDetail.setOddWhName(whName);
        orderDetail.setOddOdCode(inboundOrder.getOdCode());
        orderDetail.setGmtCreate(new Date());
        orderDetail.setGmtUpdate(new Date());
        orderDetail.setIsDeleted(DeleteFlagEnum.NORMAL.getValue());
        wmsInboundOrderDetailMapper.insertSelective(orderDetail);

        // 更新经销商还车单状态为已同步
        carBack.setCbSendBusinessFlag(String.valueOf(SendBusinessFlagEnum.SEND_Y.getValue()));
        carBack.setGmtUpdate(new Date());
        wmsDealerCarBackMapper.updateByPrimaryKeySelective(carBack);
    }

    /**
     * 同步备料计划到order
     */
    private void syncPreparationToOrder(WmsPreparationPlan wmsPreparationPlan,
                                        WmsPreparationVehicleDetail wmsPreparationVehicleDetail,
                                        String whCode,
                                        String whName,
                                        WmsInboundOrder inboundOrder,
                                        WmsInboundOrderDetail orderDetail) {
        //校验仓库code
        if (StringUtils.isNotBlank(wmsPreparationPlan.getPpWhCode()) && (!whCode.equals(wmsPreparationPlan.getPpWhCode()))) {
            throw new BusinessException("该用户不具有该仓库的操作权限");
        }
        inboundOrder.setOdCustomerName(WmsCustomerEnum.JM.getName());
        inboundOrder.setOdDispatchNo(wmsPreparationPlan.getPpDispOrderNo());// 调度单号对应发运类型的备料任务的调拨单号
        inboundOrder.setOdWaybillNo(wmsPreparationVehicleDetail.getVdWaybillNo());// 运单号
        inboundOrder.setOdWaybillType(WmsSysSourceEnum.DCS_SYNC.getValue());// 系统来源
        inboundOrder.setOdStatus(String.valueOf(WmsInboundStatusEnum.WMS_INBOUND_CREATE.getValue()));// 订单状态
        inboundOrder.setOdWhCode(whCode);
        inboundOrder.setOdWhName(whName);

        inboundOrder.setOdCode(wmsBatchNoService.addSerialNum("IB", "IB"));
        inboundOrder.setOdCustomerCode(WmsCustomerEnum.JM.getCode());
        inboundOrder.setOdCustomerName(WmsCustomerEnum.JM.getName());
        //找批次号,上次的借车要用上次的因为不走asn,但是该车一定是以前asn导入的,再将信息同步过来
        HashMap<String, Object> params = Maps.newHashMap();
        params.put("notDel", DeleteFlagEnum.NORMAL.getValue());
        params.put("vin", wmsPreparationVehicleDetail.getVdVin());
        params.put("whCode", whCode);
        params.put("orderBy", "a.wnh_id desc");
        params.put("start", 0);
        params.put("end", 1);
        List<WmsWarehouseNoticeHeadDTO> heads = wmsWarehouseNoticeExtMapper.selectBatchNoForSyncPreparationToOrder(params);
        //WmsAsnTempExample asnTempExample = new WmsAsnTempExample();
        //asnTempExample.createCriteria().andAtVinEqualTo(wmsPreparationVehicleDetail.getVdVin())
        //        .andAtBatchNoIsNotNull()
        //        .andAtSendBusinessFlagEqualTo(String.valueOf(SendBusinessFlagEnum.SEND_Y.getValue()));
        //List<WmsAsnTemp> wmsAsnTemps = wmsAsnTempMapper.selectByExample(asnTempExample);
        if (CollectionUtils.isNotEmpty(heads)) {
            WmsWarehouseNoticeHeadDTO headDTO = heads.get(0);
            inboundOrder.setOdBatchNo(headDTO.getWnhBatchNo());
        }

        inboundOrder.setInspectStatus(TmsInspectStatusEnum.WAYBILL_INIT.getValue());
        inboundOrder.setGmtCreate(new Date());
        inboundOrder.setGmtUpdate(new Date());
        inboundOrder.setIsDeleted(DeleteFlagEnum.NORMAL.getValue());
        inboundOrder.setOdBusinessType(WmsOutOfStorageType.ZTYPE_INBOUND_Z2.getValue());
        wmsInboundOrderMapper.insertSelective(inboundOrder);

        //查上一次入库的车的物料code
        WmsInboundOrderDetailExample detailExample = new WmsInboundOrderDetailExample();
        detailExample.createCriteria().andOddVinEqualTo(wmsPreparationVehicleDetail.getVdVin())
                .andIsDeletedNotEqualTo(DeleteFlagEnum.DELETED.getValue())
                .andOddMaterialCodeIsNotNull()
                .andOddCarColourIsNotNull()
                .andOddVehicleSpecCodeIsNotNull();
        List<WmsInboundOrderDetail> wmsInboundOrderDetails = wmsInboundOrderDetailMapper.selectByExample(detailExample);
        if (CollectionUtils.isNotEmpty(wmsInboundOrderDetails)) {
            WmsInboundOrderDetail oldDetail = wmsInboundOrderDetails.get(0);
            orderDetail.setOddCarColour(oldDetail.getOddCarColour());
            orderDetail.setOddCarColourCode(oldDetail.getOddCarColourCode());
            orderDetail.setOddMaterialCode(oldDetail.getOddMaterialCode());
            orderDetail.setOddMaterialName(oldDetail.getOddMaterialName());
            orderDetail.setOddVehicleSpecCode(oldDetail.getOddVehicleSpecCode());
            orderDetail.setOddVehicleSpecName(oldDetail.getOddVehicleSpecName());
            orderDetail.setOddEngine(oldDetail.getOddEngine());
        }
        orderDetail.setOddVin(wmsPreparationVehicleDetail.getVdVin());
        orderDetail.setOddVehicleSpecName(wmsPreparationVehicleDetail.getVdVehicleName());
        orderDetail.setOddOdId(inboundOrder.getOdId());
        orderDetail.setOddWhCode(whCode);
        orderDetail.setOddWhName(whName);
        orderDetail.setOddVehiclePlate(wmsPreparationPlan.getPpSupplierVehiclePlate());// 板车车牌号
        orderDetail.setOddOdCode(inboundOrder.getOdCode());
        orderDetail.setGmtCreate(new Date());
        orderDetail.setGmtUpdate(new Date());
        orderDetail.setIsDeleted(DeleteFlagEnum.NORMAL.getValue());
        wmsInboundOrderDetailMapper.insertSelective(orderDetail);
    }
}
