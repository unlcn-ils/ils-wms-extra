package com.unlcn.ils.wms.base.dto;

import java.io.Serializable;

public class WmsInspectForAppDTO implements Serializable {
    private String whCode;
    private String userId;
    private String vin;
    private String whName;
    private Integer positionId;
    private Long inspectId;
    private String missIds;

    public Long getInspectId() {
        return inspectId;
    }

    public void setInspectId(Long inspectId) {
        this.inspectId = inspectId;
    }

    public String getMissIds() {
        return missIds;
    }

    public void setMissIds(String missIds) {
        this.missIds = missIds;
    }

    public Integer getPositionId() {
        return positionId;
    }

    public void setPositionId(Integer positionId) {
        this.positionId = positionId;
    }

    public String getVin() {
        return vin;
    }

    public void setVin(String vin) {
        this.vin = vin;
    }

    public String getWhName() {
        return whName;
    }

    public void setWhName(String whName) {
        this.whName = whName;
    }

    public String getWhCode() {
        return whCode;
    }

    public void setWhCode(String whCode) {
        this.whCode = whCode;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    @Override
    public String toString() {
        return "WmsInspectForAppDTO{" +
                "whCode='" + whCode + '\'' +
                ", userId='" + userId + '\'' +
                ", vin='" + vin + '\'' +
                ", whName='" + whName + '\'' +
                ", positionId=" + positionId +
                ", inspectId=" + inspectId +
                ", missIds='" + missIds + '\'' +
                '}';
    }
}
