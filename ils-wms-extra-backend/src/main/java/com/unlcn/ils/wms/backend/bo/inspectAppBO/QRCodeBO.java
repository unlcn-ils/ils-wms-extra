package com.unlcn.ils.wms.backend.bo.inspectAppBO;

import java.io.Serializable;

public class QRCodeBO implements Serializable {
    private String vin;
    private String userId;
    //private String whId;
    private String whCode;

    public String getWhCode() {
        return whCode;
    }

    public void setWhCode(String whCode) {
        this.whCode = whCode;
    }

    public String getVin() {
        return vin;
    }

    public void setVin(String vin) {
        this.vin = vin;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }
}
