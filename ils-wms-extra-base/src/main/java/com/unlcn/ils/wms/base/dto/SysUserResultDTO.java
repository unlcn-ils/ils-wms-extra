package com.unlcn.ils.wms.base.dto;

import com.unlcn.ils.wms.base.model.sys.SysUser;

/**
 * @Auther linbao
 * @Date 2017-10-16
 */
public class SysUserResultDTO  extends SysUser{

    private String whName;

    public String getWhName() {
        return whName;
    }

    public void setWhName(String whName) {
        this.whName = whName;
    }
}
