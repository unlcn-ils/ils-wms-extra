package com.unlcn.ils.wms.base.mapper.inspectApp;

import com.unlcn.ils.wms.base.dto.WmsOutboundInspectDTO;
import com.unlcn.ils.wms.base.dto.WmsVehicleTrackingExcpDTO;
import org.apache.ibatis.annotations.Param;

import java.util.HashMap;
import java.util.List;

public interface WmsOutboundInspectExpandMapper {
    List<WmsOutboundInspectDTO> selectOutboundByVin(@Param("vin") String vin, @Param("del") Byte del, @Param("hasQiut") byte normalValue, @Param("cancleValue") String cancleValue,@Param("finishedValue") String finishedValue);

    List<WmsOutboundInspectDTO> selectOutboundByWaybillNo(@Param("waybillNo") String waybillNo, @Param("del") byte del, @Param("hasQiut") byte normalValue, @Param("cancleValue") String cancleValue,@Param("finishedValue") String finishedValue);

    List<WmsVehicleTrackingExcpDTO> selectExcpListByInspectId(HashMap<String, Object> paramMap);
}
