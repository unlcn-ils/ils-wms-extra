package com.unlcn.ils.wms.backend.service.operation.impl;

import cn.huiyunche.commons.domain.PageVo;
import cn.huiyunche.commons.domain.ResultDTOWithPagination;
import cn.huiyunche.commons.exception.BusinessException;
import com.google.common.collect.Maps;
import com.unlcn.ils.wms.backend.enums.SendStatusEnum;
import com.unlcn.ils.wms.backend.enums.WebServiceWhcodeEnum;
import com.unlcn.ils.wms.backend.enums.WhCodeEnum;
import com.unlcn.ils.wms.backend.service.operation.WmsJmHandoverOrderOperationService;
import com.unlcn.ils.wms.base.dto.WmsHandoverOrderOperationListResultDTO;
import com.unlcn.ils.wms.base.dto.WmsJmHandoverOrderOperationParamDTO;
import com.unlcn.ils.wms.base.mapper.extmapper.WmsHandoverOrderCustomMapper;
import com.unlcn.ils.wms.base.mapper.junmadcs.WmsHandoverOrderExcpMapper;
import com.unlcn.ils.wms.base.mapper.junmadcs.WmsHandoverOrderMapper;
import com.unlcn.ils.wms.base.mapper.sys.SysUserMapper;
import com.unlcn.ils.wms.base.model.junmadcs.WmsHandoverOrder;
import com.unlcn.ils.wms.base.model.junmadcs.WmsHandoverOrderExcp;
import com.unlcn.ils.wms.base.model.sys.SysUser;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

@Service
public class WmsJmHandoverOrderOperationServiceImpl implements WmsJmHandoverOrderOperationService {

    private Logger logger = LoggerFactory.getLogger(getClass());

    private WmsHandoverOrderMapper wmsHandoverOrderMapper;

    private WmsHandoverOrderCustomMapper wmsHandoverOrderCustomMapper;

    private WmsHandoverOrderExcpMapper wmsHandoverOrderExcpMapper;

    private SysUserMapper sysUserMapper;

    @Autowired
    public void setSysUserMapper(SysUserMapper sysUserMapper) {
        this.sysUserMapper = sysUserMapper;
    }

    @Autowired
    public void setWmsHandoverOrderMapper(WmsHandoverOrderMapper wmsHandoverOrderMapper) {
        this.wmsHandoverOrderMapper = wmsHandoverOrderMapper;
    }

    @Autowired
    public void setWmsHandoverOrderCustomMapper(WmsHandoverOrderCustomMapper wmsHandoverOrderCustomMapper) {
        this.wmsHandoverOrderCustomMapper = wmsHandoverOrderCustomMapper;
    }

    @Autowired
    public void setWmsHandoverOrderExcpMapper(WmsHandoverOrderExcpMapper wmsHandoverOrderExcpMapper) {
        this.wmsHandoverOrderExcpMapper = wmsHandoverOrderExcpMapper;
    }

    @Override
    public ResultDTOWithPagination<List<WmsHandoverOrderOperationListResultDTO>> listHandoverOrderExcp(WmsJmHandoverOrderOperationParamDTO dto) throws Exception {
        logger.info("WmsJmHandoverOrderOperationServiceImpl.listHandoverOrderExcp  param:{}", dto);
        if (dto == null)
            throw new BusinessException("传入的参数不能为空!");
        String whCode = dto.getWhCode();
        String userId = dto.getUserId();
        if (StringUtils.isBlank(whCode))
            throw new BusinessException("仓库code不能为空!");
        if (StringUtils.isBlank(userId))
            throw new BusinessException("用户Id不能为空!");
        if (!(WhCodeEnum.JM_CS.getValue().equals(whCode) || WhCodeEnum.JM_XY.getValue().equals(whCode))) {
            throw new BusinessException("该仓库不支持此操作!");
        }
        HashMap<String, Object> params = Maps.newHashMap();
        getParams(dto, whCode, params);
        List<WmsHandoverOrderOperationListResultDTO> list = wmsHandoverOrderCustomMapper.selectOperationListByParam(params);
        int count = wmsHandoverOrderCustomMapper.countOperationRecordsByParam(params);
        //校验数据并设置接口的发送最终状态
        list.forEach(v -> {
            if (SendStatusEnum.SEND_FAILED.getValue().equals(v.getSendStatus())) {
                if (StringUtils.isBlank(v.getFinalDcsStatus())) {
                    v.setFinalDcsStatus(SendStatusEnum.NOT_FINAL.getValue());
                }
                if (SendStatusEnum.SEND_FAILED.getValue().equals(v.getFinalDcsStatus())) {
                    v.setFinalDcsStatus(SendStatusEnum.FINAL_FAIL.getValue());
                }
            }
            if (SendStatusEnum.SEND_SUCCESS.getValue().equals(v.getSendStatus())) {
                v.setFinalDcsStatus(SendStatusEnum.FINAL_SUCCESS.getValue());
            }
            if (SendStatusEnum.SEND_INIT.getValue().equals(v.getSendStatus())) {
                v.setFinalDcsStatus(SendStatusEnum.NOT_SEND.getValue());
            }
            //sap
            if (SendStatusEnum.SEND_FAILED.getValue().equals(v.getSendStatusSap())) {
                if (StringUtils.isBlank(v.getFinalSapStatus())) {
                    v.setFinalSapStatus(SendStatusEnum.NOT_FINAL.getValue());
                }
                if (SendStatusEnum.SEND_FAILED.getValue().equals(v.getFinalSapStatus())) {
                    v.setFinalSapStatus(SendStatusEnum.FINAL_FAIL.getValue());
                }
            }
            if (SendStatusEnum.SEND_SUCCESS.getValue().equals(v.getSendStatusSap())) {
                v.setFinalSapStatus(SendStatusEnum.FINAL_SUCCESS.getValue());
            }
            if (SendStatusEnum.SEND_INIT.getValue().equals(v.getSendStatusSap())) {
                v.setFinalSapStatus(SendStatusEnum.NOT_SEND.getValue());
            }
            //crm
            if (SendStatusEnum.SEND_FAILED.getValue().equals(v.getSendStatusCrm())) {
                if (StringUtils.isBlank(v.getFinalCrmStatus())) {
                    v.setFinalCrmStatus(SendStatusEnum.NOT_FINAL.getValue());
                }
                if (SendStatusEnum.SEND_FAILED.getValue().equals(v.getFinalCrmStatus())) {
                    v.setFinalCrmStatus(SendStatusEnum.FINAL_FAIL.getValue());
                }
            }
            if (SendStatusEnum.SEND_SUCCESS.getValue().equals(v.getSendStatusCrm())) {
                v.setFinalCrmStatus(SendStatusEnum.FINAL_SUCCESS.getValue());
            }
            if (SendStatusEnum.SEND_INIT.getValue().equals(v.getSendStatusCrm())) {
                v.setFinalCrmStatus(SendStatusEnum.NOT_SEND.getValue());
            }
        });
        PageVo pagevo = new PageVo();
        pagevo.setTotalRecord(count);
        pagevo.setPageSize(dto.getPageSize());
        pagevo.setPageNo(dto.getPageNo());
        pagevo.setOrder(dto.getOrder());
        ResultDTOWithPagination<List<WmsHandoverOrderOperationListResultDTO>> result = new ResultDTOWithPagination<>();
        result.setPageVo(pagevo);
        result.setData(list);
        return result;
    }

    /**
     * 重发数据到dcs
     *
     * @param dto 参数
     * @throws Exception 异常
     */
    @Override
    public void updateSendToDCSAgain(WmsJmHandoverOrderOperationParamDTO dto) throws Exception {
        logger.info("WmsJmHandoverOrderOperationServiceImpl.updateSendToDCSAgain  param:{}", dto);
        if (dto == null)
            throw new BusinessException("参数不能为空!");
        String dataIds_str = dto.getDataIds();
        String whCode = dto.getWhCode();
        String userId = dto.getUserId();
        if (StringUtils.isBlank(dataIds_str)) {
            throw new BusinessException("请选择需要重发的数据");
        }
        if (StringUtils.isBlank(userId))
            throw new BusinessException("传入的用户id不能为空");
        if (StringUtils.isBlank(whCode))
            throw new BusinessException("仓库code不能为空!");
        if (!(WhCodeEnum.JM_XY.getValue().equals(whCode) || WhCodeEnum.JM_CS.getValue().equals(whCode)))
            throw new BusinessException("该仓库不支持此操作!");
        SysUser sysUser = sysUserMapper.selectByPrimaryKey(Integer.valueOf(userId));
        if (sysUser == null)
            throw new BusinessException("未查询到操作用户信息");
        String[] dataIds = dataIds_str.split(",");
        List<String> ids = Arrays.asList(dataIds);
        List<WmsHandoverOrderExcp> list = wmsHandoverOrderCustomMapper.selectExcpListByDataIds(ids);
        if (CollectionUtils.isEmpty(list))
            throw new BusinessException("未查询到对应的数据");
        if (list.size() != ids.size()) {
            throw new BusinessException("选择的数目条数和查询的异常条目数不一致!");
        }
        list.forEach(v -> {
            if (StringUtils.isBlank(v.getFinalSendStatusDcs())) {
                throw new BusinessException("该组板单号:" + v.getGbno() +
                        ",订单号:" + v.getVbeln() + "未确定最终发送状态,请稍后再进行该操作!");
            }
            if (!(SendStatusEnum.SEND_FAILED.getValue().equals(v.getFinalSendStatusDcs()))) {
                throw new BusinessException("该组板单号:" + v.getGbno() +
                        ",订单号:" + v.getVbeln() + "只支持失败的数据,进行该操作");
            }
        });
        HashMap<String, Object> params = Maps.newHashMap();
        params.put("username", sysUser.getName());
        params.put("ids", ids);
        wmsHandoverOrderCustomMapper.updateSendTODCSAgainByParam(params);


    }

    /**
     * 重发失败的交接单数据到sap系统
     *
     * @param dto 参数封装
     * @throws Exception 异常
     */
    @Override
    public void updateSendToSAPAgain(WmsJmHandoverOrderOperationParamDTO dto) throws Exception {
        logger.info("WmsJmHandoverOrderOperationServiceImpl.updateSendToSAPAgain  param:{}", dto);
        if (dto == null)
            throw new BusinessException("参数不能为空!");
        String dataIds_str = dto.getDataIds();
        String whCode = dto.getWhCode();
        String userId = dto.getUserId();
        if (StringUtils.isBlank(dataIds_str)) {
            throw new BusinessException("请选择需要重发的数据");
        }
        if (StringUtils.isBlank(userId))
            throw new BusinessException("传入的用户id不能为空");
        if (StringUtils.isBlank(whCode))
            throw new BusinessException("仓库code不能为空!");
        if (!(WhCodeEnum.JM_XY.getValue().equals(whCode) || WhCodeEnum.JM_CS.getValue().equals(whCode)))
            throw new BusinessException("该仓库不支持此操作!");
        SysUser sysUser = sysUserMapper.selectByPrimaryKey(Integer.valueOf(userId));
        if (sysUser == null)
            throw new BusinessException("未查询到操作用户信息");
        String[] dataIds = dataIds_str.split(",");
        List<String> ids = Arrays.asList(dataIds);
        List<WmsHandoverOrderExcp> list = wmsHandoverOrderCustomMapper.selectExcpListByDataIds(ids);
        if (CollectionUtils.isEmpty(list))
            throw new BusinessException("未查询到对应的数据");
        if (list.size() != ids.size()) {
            throw new BusinessException("选择的数目条数和查询的异常条目数不一致!");
        }
        list.forEach(v -> {
            if (StringUtils.isBlank(v.getFinalSendStatusSap())) {
                throw new BusinessException("该组板单号:" + v.getGbno() +
                        ",订单号:" + v.getVbeln() + "未确定最终发送状态,请稍后再进行该操作!");
            }
            if (!(SendStatusEnum.SEND_FAILED.getValue().equals(v.getFinalSendStatusSap()))) {
                throw new BusinessException("该组板单号:" + v.getGbno() +
                        ",订单号:" + v.getVbeln() + "只支持失败的数据,进行该操作");
            }
        });
        HashMap<String, Object> params = Maps.newHashMap();
        params.put("username", sysUser.getName());
        params.put("ids", ids);
        wmsHandoverOrderCustomMapper.updateSendTOSAPAgainByParam(params);
    }

    /**
     * 实现交接单数据重发CRM系统
     *
     * @param dto 参数封装
     * @throws Exception 异常
     */
    @Override
    public void updateSendToCRMAgain(WmsJmHandoverOrderOperationParamDTO dto) throws Exception {
        logger.info("WmsJmHandoverOrderOperationServiceImpl.updateSendToCRMAgain  param:{}", dto);
        if (dto == null)
            throw new BusinessException("参数不能为空!");
        String dataIds_str = dto.getDataIds();
        String whCode = dto.getWhCode();
        String userId = dto.getUserId();
        if (StringUtils.isBlank(dataIds_str)) {
            throw new BusinessException("请选择需要重发的数据");
        }
        if (StringUtils.isBlank(userId))
            throw new BusinessException("传入的用户id不能为空");
        if (StringUtils.isBlank(whCode))
            throw new BusinessException("仓库code不能为空!");
        if (!(WhCodeEnum.JM_XY.getValue().equals(whCode) || WhCodeEnum.JM_CS.getValue().equals(whCode)))
            throw new BusinessException("该仓库不支持此操作!");
        SysUser sysUser = sysUserMapper.selectByPrimaryKey(Integer.valueOf(userId));
        if (sysUser == null)
            throw new BusinessException("未查询到操作用户信息");
        String[] dataIds = dataIds_str.split(",");
        List<String> ids = Arrays.asList(dataIds);
        List<WmsHandoverOrderExcp> list = wmsHandoverOrderCustomMapper.selectExcpListByDataIds(ids);
        if (CollectionUtils.isEmpty(list))
            throw new BusinessException("未查询到对应的数据");
        if (list.size() != ids.size()) {
            throw new BusinessException("选择的数目条数和查询的异常条目数不一致!");
        }
        list.forEach(v -> {
            if (StringUtils.isBlank(v.getFinalSendStatusCrm())) {
                throw new BusinessException("该组板单号:" + v.getGbno() +
                        ",订单号:" + v.getVbeln() + "未确定最终发送状态,请稍后再进行该操作!");
            }
            if (!(SendStatusEnum.SEND_FAILED.getValue().equals(v.getFinalSendStatusCrm()))) {
                throw new BusinessException("该组板单号:" + v.getGbno() +
                        ",订单号:" + v.getVbeln() + "只支持失败的数据,进行该操作");
            }
        });
        HashMap<String, Object> params = Maps.newHashMap();
        params.put("username", sysUser.getName());
        params.put("ids", ids);
        wmsHandoverOrderCustomMapper.updateSendTOCRMAgainByParam(params);

    }

    /**
     * 手动
     *
     * @param dto 参数封装
     * @throws Exception 异常
     */
    @Override
    public void updateSendDCSToSuccess(WmsJmHandoverOrderOperationParamDTO dto) throws Exception {
        logger.info("WmsJmHandoverOrderOperationServiceImpl.updateSendDCSToSuccess  param:{}", dto);
        if (dto == null)
            throw new BusinessException("参数不能为空!");
        String whCode = dto.getWhCode();
        String userId = dto.getUserId();
        String dataIds = dto.getDataIds();
        if (StringUtils.isBlank(whCode))
            throw new BusinessException("仓库code不能为空!");
        if (!(WhCodeEnum.JM_CS.getValue().equals(whCode) || WhCodeEnum.JM_XY.getValue().equals(whCode))) {
            throw new BusinessException("该仓库不支持此操作!");
        }
        if (StringUtils.isBlank(dataIds))
            throw new BusinessException("请选择数据,进行后续的重传!");
        if (StringUtils.isBlank(userId))
            throw new BusinessException("用户id不能为空!");
        SysUser sysUser = sysUserMapper.selectByPrimaryKey(Integer.valueOf(userId));
        if (sysUser == null)
            throw new BusinessException("未查询到对应的用户信息");
        String[] ids_str = dataIds.split(",");
        List<String> ids = Arrays.asList(ids_str);
        List<WmsHandoverOrder> list = wmsHandoverOrderCustomMapper.selectOrderListByIds(ids);
        if (CollectionUtils.sizeIsEmpty(list))
            throw new BusinessException("未查询到对应的数据");
        list.forEach(v -> {
            if (SendStatusEnum.SEND_SUCCESS.getValue().equals(v.getSendStatus())) {
                throw new BusinessException("组板单:" + v.getGbno() +
                        ",订单号:" + v.getVbeln() + "发送DCS状态为成功,不能进行该操作");
            }
        });
        HashMap<String, Object> params = Maps.newHashMap();
        params.put("username", sysUser.getName());
        params.put("ids", ids);
        params.put("msg", "创建物料凭证成功!--web页面手动");
        params.put("success", SendStatusEnum.SEND_SUCCESS.getValue());
        wmsHandoverOrderCustomMapper.updateSendDCSResultSuccess(params);


    }

    /**
     * 手动设值sap发送状态为成功
     *
     * @param dto 参数封装
     * @throws Exception 异常
     */
    @Override
    public void updateSendSAPToSuccess(WmsJmHandoverOrderOperationParamDTO dto) throws Exception {
        logger.info("WmsJmHandoverOrderOperationServiceImpl.updateSendSAPToSuccess  param:{}", dto);
        if (dto == null)
            throw new BusinessException("参数不能为空!");
        String whCode = dto.getWhCode();
        String userId = dto.getUserId();
        String dataIds = dto.getDataIds();
        if (StringUtils.isBlank(whCode))
            throw new BusinessException("仓库code不能为空!");
        if (!(WhCodeEnum.JM_CS.getValue().equals(whCode) || WhCodeEnum.JM_XY.getValue().equals(whCode))) {
            throw new BusinessException("该仓库不支持此操作!");
        }
        if (StringUtils.isBlank(dataIds))
            throw new BusinessException("请选择数据,进行后续的重传!");
        if (StringUtils.isBlank(userId))
            throw new BusinessException("用户id不能为空!");
        SysUser sysUser = sysUserMapper.selectByPrimaryKey(Integer.valueOf(userId));
        if (sysUser == null)
            throw new BusinessException("未查询到对应的用户信息");
        String[] ids_str = dataIds.split(",");
        List<String> ids = Arrays.asList(ids_str);
        List<WmsHandoverOrder> list = wmsHandoverOrderCustomMapper.selectOrderListByIds(ids);
        if (CollectionUtils.sizeIsEmpty(list))
            throw new BusinessException("未查询到对应的数据");
        list.forEach(v -> {
            if (SendStatusEnum.SEND_SUCCESS.getValue().equals(v.getSendStatusSap())) {
                throw new BusinessException("组板单:" + v.getGbno() +
                        ",订单号:" + v.getVbeln() + "发送SAP状态为成功,不能进行该操作");
            }
        });
        HashMap<String, Object> params = Maps.newHashMap();
        params.put("username", sysUser.getName());
        params.put("ids", ids);
        params.put("msg", "创建物料凭证成功!--web页面手动");
        params.put("success", SendStatusEnum.SEND_SUCCESS.getValue());
        wmsHandoverOrderCustomMapper.updateSendSAPResultSuccess(params);
    }

    /**
     * 实现CRM系统的手动成功
     *
     * @param dto 参数
     * @throws Exception 异常
     */
    @Override
    public void updateSendCRMToSuccess(WmsJmHandoverOrderOperationParamDTO dto) throws Exception {
        logger.info("WmsJmHandoverOrderOperationServiceImpl.updateSendCRMToSuccess  param:{}", dto);
        if (dto == null)
            throw new BusinessException("参数不能为空!");
        String whCode = dto.getWhCode();
        String userId = dto.getUserId();
        String dataIds = dto.getDataIds();
        if (StringUtils.isBlank(whCode))
            throw new BusinessException("仓库code不能为空!");
        if (!(WhCodeEnum.JM_CS.getValue().equals(whCode) || WhCodeEnum.JM_XY.getValue().equals(whCode))) {
            throw new BusinessException("该仓库不支持此操作!");
        }
        if (StringUtils.isBlank(dataIds))
            throw new BusinessException("请选择数据,进行后续的重传!");
        if (StringUtils.isBlank(userId))
            throw new BusinessException("用户id不能为空!");
        SysUser sysUser = sysUserMapper.selectByPrimaryKey(Integer.valueOf(userId));
        if (sysUser == null)
            throw new BusinessException("未查询到对应的用户信息");
        String[] ids_str = dataIds.split(",");
        List<String> ids = Arrays.asList(ids_str);
        List<WmsHandoverOrder> list = wmsHandoverOrderCustomMapper.selectOrderListByIds(ids);
        if (CollectionUtils.sizeIsEmpty(list))
            throw new BusinessException("未查询到对应的数据");
        list.forEach(v -> {
            if (SendStatusEnum.SEND_SUCCESS.getValue().equals(v.getSendStatusCrm())) {
                throw new BusinessException("组板单:" + v.getGbno() +
                        ",订单号:" + v.getVbeln() + "发送CRM状态为成功,不能进行该操作");
            }
        });
        HashMap<String, Object> params = Maps.newHashMap();
        params.put("username", sysUser.getName());
        params.put("ids", ids);
        params.put("msg", "创建物料凭证成功!--web页面手动");
        params.put("success", SendStatusEnum.SEND_SUCCESS.getValue());
        wmsHandoverOrderCustomMapper.updateSendCRMResultSuccess(params);

    }


    private void getParams(WmsJmHandoverOrderOperationParamDTO dto, String whCode, HashMap<String, Object> params) {
        params.put("whCode", whCode);
        //转换状态的值
        swithSendStatus(dto, params);

        params.put("dcsStatus", StringUtils.isNotBlank(dto.getSendDcsStatus()) ? dto.getSendDcsStatus() : SendStatusEnum.SEND_FAILED.getValue());
        params.put("sapStatus", StringUtils.isNotBlank(dto.getSendSapStatus()) ? dto.getSendSapStatus() : SendStatusEnum.SEND_FAILED.getValue());
        params.put("crmStatus", StringUtils.isNotBlank(dto.getSendCrmStatus()) ? dto.getSendCrmStatus() : SendStatusEnum.SEND_FAILED.getValue());
        switch (whCode) {
            case "JM_CS":
                params.put("whCode", WebServiceWhcodeEnum.CS01.getValue());
                break;
            case "JM_XY":
                params.put("whCode", WebServiceWhcodeEnum.XY01.getValue());
                break;
            default:
                throw new BusinessException("仓库code:" + whCode + "不支持此操作!");
                //break;
        }
        params.put("start", dto.getStartIndex());
        params.put("end", dto.getPageSize());
        params.put("orderBy", StringUtils.isBlank(dto.getOrder()) ? "a.DATA_ID desc" : dto.getOrder());
        if (StringUtils.isNotBlank(dto.getVin())) {
            params.put("vin", dto.getVin());
        }
        if (StringUtils.isNotBlank(dto.getOrderno())) {
            params.put("orderno", dto.getOrderno());
        }
        if (StringUtils.isNotBlank(dto.getHandoverNumber())) {
            params.put("handoverOrderNumber", dto.getHandoverNumber());
        }
        if (StringUtils.isNotBlank(dto.getGbno())) {
            params.put("gbno", dto.getGbno());
        }
        if (StringUtils.isNotBlank(dto.getStartTime())) {
            params.put("startTime", dto.getStartTime() + " 00:00:00");
        }
        if (StringUtils.isNotBlank(dto.getEndTime())) {
            params.put("endTime", dto.getEndTime() + " 23:59:59");
        }
    }

    private void swithSendStatus(WmsJmHandoverOrderOperationParamDTO dto, HashMap<String, Object> params) {
        if (StringUtils.isNotBlank(dto.getSendDcsStatus())) {
            switch (dto.getSendDcsStatus()) {
                case "40":
                    dto.setSendDcsStatus(SendStatusEnum.SEND_INIT.getValue());
                    break;
                case "20":
                    dto.setSendDcsStatus(SendStatusEnum.SEND_FAILED.getValue());
                    params.put("finalDCSStatus", "is not null");
                    break;
                case "30":
                    dto.setSendDcsStatus(SendStatusEnum.SEND_FAILED.getValue());
                    params.put("finalDCSStatus", "is null");
                    break;
                case "10":
                    dto.setSendDcsStatus(SendStatusEnum.SEND_SUCCESS.getValue());
                    break;
            }
        }

        if (StringUtils.isNotBlank(dto.getSendSapStatus())) {
            switch (dto.getSendSapStatus()) {
                case "40":
                    dto.setSendSapStatus(SendStatusEnum.SEND_INIT.getValue());
                    break;
                case "30":
                    dto.setSendSapStatus(SendStatusEnum.SEND_FAILED.getValue());
                    params.put("finalSAPStatus", "is null");
                    break;
                case "10":
                    dto.setSendSapStatus(SendStatusEnum.SEND_SUCCESS.getValue());
                    break;
                case "20":
                    dto.setSendSapStatus(SendStatusEnum.SEND_FAILED.getValue());
                    params.put("finalSAPStatus", "is not null");
                    break;
            }
        }

        if (StringUtils.isNotBlank(dto.getSendCrmStatus())) {
            switch (dto.getSendCrmStatus()) {
                case "40":
                    dto.setSendCrmStatus(SendStatusEnum.SEND_INIT.getValue());
                    break;
                case "30":
                    dto.setSendCrmStatus(SendStatusEnum.SEND_FAILED.getValue());
                    params.put("finalCRMStatus", "is null");
                    break;
                case "10":
                    dto.setSendCrmStatus(SendStatusEnum.SEND_SUCCESS.getValue());
                    break;
                case "20":
                    dto.setSendCrmStatus(SendStatusEnum.SEND_FAILED.getValue());
                    params.put("finalCRMStatus", "is not null");
                    break;
            }
        }
    }


}
