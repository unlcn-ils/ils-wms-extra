package com.unlcn.ils.wms.base.mapper.extmapper;

import com.unlcn.ils.wms.base.model.bigscreen.BiPickData;
import com.unlcn.ils.wms.base.model.bigscreen.BiPickDataExample;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface BiPickDataExtMapper {

    /**
     * 查询提车推移图数据
     * @return
     */
    List<BiPickData> selectCountData();
}