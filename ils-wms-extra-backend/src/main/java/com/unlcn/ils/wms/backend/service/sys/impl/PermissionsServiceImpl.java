package com.unlcn.ils.wms.backend.service.sys.impl;


import cn.huiyunche.commons.exception.BusinessException;
import com.unlcn.ils.wms.backend.bo.sys.PermissionsBO;
import com.unlcn.ils.wms.backend.dto.sysDTO.PermissNewDTO;
import com.unlcn.ils.wms.backend.enums.StatusEnum;
import com.unlcn.ils.wms.backend.service.sys.PermissionsService;
import com.unlcn.ils.wms.backend.service.sys.SysRoleService;
import com.unlcn.ils.wms.base.dto.SysPermissDTO;
import com.unlcn.ils.wms.base.mapper.extmapper.SysPermissionsCustomMapper;
import com.unlcn.ils.wms.base.mapper.sys.SysPermissionsMapper;
import com.unlcn.ils.wms.base.model.sys.SysPermissions;
import com.unlcn.ils.wms.base.model.sys.SysRole;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Objects;

@Service
public class PermissionsServiceImpl implements PermissionsService {
	
	@Autowired
	private SysPermissionsMapper permissionsMapper;
	
	@Autowired
	private SysPermissionsCustomMapper permissionsCustomMapper;

	@Autowired
	private SysRoleService sysRoleService;
	
	@Override
	public Integer add(PermissionsBO permissionsBO) {
		
		SysPermissions permissions = new SysPermissions();
		BeanUtils.copyProperties(permissionsBO, permissions);
		
		validate(permissions);	//校验属性
		
		//TODO 权限名重复判断
		
		permissions.setEnable(StatusEnum.RIGHT.getValue());	//设置状态
		permissions.setGmtCreate(new Date());	//设置创建时间
		permissions.setGmtModified(new Date());	//设置修改时间
		
		return permissionsMapper.insert(permissions);
	}
	
	@Override
	public PermissionsBO findById(Integer id) throws Exception {
		
		SysPermissions permissions = permissionsMapper.selectByPrimaryKey(id);
		PermissionsBO permissiosBO = new PermissionsBO();
		BeanUtils.copyProperties(permissions, permissiosBO);
		return permissiosBO;
	}
	
	@Override
	public List<PermissionsBO> findByRoleId(Integer roleId,Integer permissionsId) throws Exception {
		
		 
		List<SysPermissions> permissionsList = permissionsCustomMapper.selectByRoleId(roleId, permissionsId, StatusEnum.RIGHT.getValue());
		List<PermissionsBO> permissionsBOList = new ArrayList<>();
		
		for(SysPermissions permissions : permissionsList) {
			
			PermissionsBO permissionsBO = new PermissionsBO();
			BeanUtils.copyProperties(permissions, permissionsBO);
			//获取下级的权限
			permissionsBO.setChildPermissionsBOList(findByRoleId(roleId,permissions.getId()));
			
			permissionsBOList.add(permissionsBO);
		}
		
		return permissionsBOList;
	}
	
	@Override
	public List<PermissionsBO> findAll() {
		// TODO Auto-generated method stub
		return findPermissionsAndChildByParentId(null);
	}

	/**
	 * 查询所有的可用的权限
	 *
	 * @return
	 */
	@Override
	public List<PermissionsBO> findAllForEnabled() {
		return findPermissionsAndChildByParentIdForEnabled(null);

	}

	/**
	 * 查询权限
	 *
	 * @param parentId
	 * @return
	 */
	private List<PermissionsBO> findPermissionsAndChildByParentIdForEnabled(Integer parentId){
		List<SysPermissions> SysPermissionsList = permissionsCustomMapper.selectByParentIdForEnabled(parentId);
		List<PermissionsBO> permissionsBOList = new ArrayList<>();

		for(SysPermissions permissions : SysPermissionsList) {

			PermissionsBO permissionsBO = new PermissionsBO();
			BeanUtils.copyProperties(permissions, permissionsBO);
			//获取下级的权限
			permissionsBO.setChildPermissionsBOList(findPermissionsAndChildByParentIdForEnabled(permissionsBO.getId()));
			permissionsBOList.add(permissionsBO);
		}
		return permissionsBOList;
	}

	@Override
	public List<PermissionsBO> findPermissionsAndChildByParentId(Integer parentId) {
		
		List<SysPermissions> SysPermissionsList = permissionsCustomMapper.selectByParentId(parentId);
		List<PermissionsBO> permissionsBOList = new ArrayList<>();
		
		for(SysPermissions permissions : SysPermissionsList) {
			
			PermissionsBO permissionsBO = new PermissionsBO();
			BeanUtils.copyProperties(permissions, permissionsBO);
			//获取下级的权限
			permissionsBO.setChildPermissionsBOList(findPermissionsAndChildByParentId(permissionsBO.getId()));
			permissionsBOList.add(permissionsBO);
		}
		
		return permissionsBOList;
	}

	/**
	 * 根据角色ID集合获取权限
	 * @param roleIdList
	 * @return
	 * @throws Exception
	 */
	@Override
	public List<SysPermissDTO> getPermissListByRoleIdList(List<Integer> roleIdList) throws Exception {

		List<SysPermissDTO> dtoList = new ArrayList<>();
		SysPermissDTO sysPermissDTO = null;
		List<SysPermissDTO> permissList = getPermissList(roleIdList, null);
		if (CollectionUtils.isNotEmpty(permissList)){
			for (SysPermissDTO permissDTO : permissList){
				sysPermissDTO = new SysPermissDTO();
				BeanUtils.copyProperties(permissDTO, sysPermissDTO);

				sysPermissDTO.setChildPermiss(getPermissList(roleIdList, permissDTO.getId()));
				dtoList.add(sysPermissDTO);
			}
			return dtoList;
		}
		return null;
	}

	/**
	 * 根据用户ID获取权限
	 *
	 * @param userId
	 * @return
	 * @throws Exception
	 */
	@Override
	public List<SysPermissDTO> getPermissListByUserId(Integer userId) throws Exception {

		if (!Objects.equals(userId, null)){

			//获取权限
			List<SysRole> sysRoleList = sysRoleService.getRoleListByUserId(userId);
			if (CollectionUtils.isNotEmpty(sysRoleList)) {
				List<Integer> roleIdList = new ArrayList<>();
				sysRoleList.stream()
						.filter(sysRole -> !Objects.equals(sysRole, null))
						.filter(sysRole -> !Objects.equals(sysRole.getId(), null))
						.forEach(sysRole -> roleIdList.add(sysRole.getId()));

				if (CollectionUtils.isNotEmpty(roleIdList)){
					return getPermissListByRoleIdList(roleIdList);
				}
			}
		}
		return null;
	}

	/**
	 * 根据用户id 获取菜单
	 * @param parentId
	 * @param userId
	 * @return
	 */
	@Override
	public List<PermissNewDTO> findPermissionsAndChildByParentIdAndUserId(Integer parentId, Integer userId, Long warehouseId) {

		List<SysPermissions> permissionsList = permissionsCustomMapper.selectPermissListByParentAndUserId(parentId, userId, warehouseId);
		List<PermissNewDTO> permissionsBOList = new ArrayList<>();

		for(SysPermissions permissions : permissionsList) {

			PermissNewDTO permissNewDTO = new PermissNewDTO();
			BeanUtils.copyProperties(permissions, permissNewDTO);
			//获取下级的权限
			permissNewDTO.setChildPermissionsBOList(findPermissionsAndChildByParentIdAndUserId(permissions.getId(), userId, warehouseId));
			permissionsBOList.add(permissNewDTO);
		}

		return permissionsBOList;
	}

	/**
	 * 获取权限
	 * @param roleIdList
	 * @param parentId
	 * @return
	 */
	private List<SysPermissDTO> getPermissList(List<Integer> roleIdList, Integer parentId){
		List<SysPermissions> sysPermissionsList = permissionsCustomMapper.selectPermissListByRoleIdListAndParentId(roleIdList, parentId);
		if (CollectionUtils.isNotEmpty(sysPermissionsList)){
			List<SysPermissDTO> dtoList = new ArrayList<>();
			SysPermissDTO permissDTO = null;
			for (SysPermissions permissions : sysPermissionsList){
				permissDTO = new SysPermissDTO();
				BeanUtils.copyProperties(permissions, permissDTO);
				dtoList.add(permissDTO);
			}
			return dtoList;
		}
		return null;
	}
	/**
	 * 
	 * @Title: validate 
	 * @Description: 验证属性
	 * @param @param permissions     
	 * @return void    返回类型 
	 * @throws 
	 *
	 */
	private void validate(SysPermissions permissions) {
		
		if(StringUtils.isBlank(permissions.getName())) {	
			throw new BusinessException("权限名不能为空");
		}
		if(StringUtils.isBlank(permissions.getLevel())) {
			throw new BusinessException("权限级别不能为空");
		}
		
	}
	
}
