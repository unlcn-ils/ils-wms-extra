package com.unlcn.ils.wms.backend.service.inbound;

import com.unlcn.ils.wms.backend.bo.baseDataBO.VehicleSpecBO;
import com.unlcn.ils.wms.backend.bo.inboundBO.WmsInboundAllocationBO;
import com.unlcn.ils.wms.backend.bo.inboundBO.WmsInboundOrderBO;
import com.unlcn.ils.wms.backend.dto.inboundDTO.WmsInboundOrderDTO;
import com.unlcn.ils.wms.base.businessDTO.inbound.AsnOrderDTO;
import com.unlcn.ils.wms.base.businessDTO.inbound.WmsAsnOrderQueryDTO;
import com.unlcn.ils.wms.base.model.inbound.WmsWarehouseNoticeDetail;

import java.util.List;
import java.util.Map;

/**
 * Created by DELL on 2017/8/4.
 */

public interface AsnOrderService {
    /**
     * 查询订单列表
     *
     * @param wmsAsnOrderQueryDTO
     * @return
     */
    Map<String, Object> getListAsnOrderBO(WmsAsnOrderQueryDTO wmsAsnOrderQueryDTO) throws Exception;

    /**
     * 新增订单
     *
     * @param wmsInboundOrderBO
     */
    boolean saveAsnOrder(WmsInboundOrderBO wmsInboundOrderBO);

    /**
     * 根据ID查询订单数据
     *
     * @param odId
     * @return
     */
    WmsInboundOrderBO getAsnOrderById(String odId);

    /**
     * 删除订单以及订单详情
     *
     * @param wmsInboundOrderBOList
     */
    void deleteAsnOrderByAsnOrderId(List<WmsInboundOrderBO> wmsInboundOrderBOList);

    /*
     * 分配库位
     * @param inboundAsnOrderBO
     * @param type
     * @return
     */
    //String allocTasks(WmsInboundOrderBO wmsInboundOrderBO,String type);

    /**
     * 查询订单信息
     *
     * @param asnOrderList
     * @return
     */
    List<AsnOrderDTO> queryAsnOrderDetailListByAsnOrderId(List<WmsInboundOrderBO> asnOrderList);

    /**
     * 根据订单ID修改订单数据
     *
     * @param asnOrderVOList
     */
    void updateAsnOrderByAsnOrderId(List<WmsInboundOrderBO> asnOrderVOList, String asnStatus);

    /**
     * 根据对象修改订单数据
     *
     * @param wmsInboundOrderBO
     */
    boolean updateAsnOrderByEntity(WmsInboundOrderBO wmsInboundOrderBO);

    /**
     * 入库确认
     *
     * @param wmsInboundAllocationBO
     * @param userId
     * @return
     * @throws Exception
     */
    void updateConfirmInbound(WmsInboundAllocationBO wmsInboundAllocationBO, String userId) throws Exception;

    /**
     * 确认入库发送邮件
     *
     * @param vin
     */
    void confirmInboundMailTo(String vin);

    /**
     * 根据订单号或者底盘号查询订单
     *
     * @param queryParam
     * @return
     */
    WmsInboundOrderBO queryByWaybillOrVin(String queryParam);

    /**
     * 根据底盘号查询订单数据
     *
     * @param queryParam
     * @return
     */
    WmsInboundOrderBO queryByVin(String queryParam);

    /**
     * 查询入库记录
     *
     * @param paramMap
     * @return
     */
    Map<String, Object> queryInboundRecord(Map<String,Object> paramMap);

    List<WmsWarehouseNoticeDetail> getVehicleSpec(String whCode);

    /**
     * 从订单表拿车型
     *
     * @return
     */
    List<VehicleSpecBO> getVehicleSpecOld(String whCode);

    /**
     * 收货入库 - 君马
     *
     * @param wmsInboundOrderBO
     * @throws Exception
     */
    void updateToInConfirm(WmsInboundOrderBO wmsInboundOrderBO) throws Exception;

    /**
     * 根据车架号获取数据
     *
     * @param vin
     * @return
     * @throws Exception
     */
    WmsInboundOrderDTO getInBoundInfoByVin(String vin) throws Exception;

    /**
     * 根据车架号获取数据
     *
     * @param vin
     * @return
     * @throws Exception
     */
    WmsInboundOrderDTO updateToInConfirm2(String vin) throws Exception;

    /**
     * 获取一个小时内入库的车辆信息
     *
     * @return
     */
    void getInboundSendMailData();

    /**
     * 获取颜色列表
     *
     * @param whCode
     * @return
     * @throws Exception
     */
    List<String> getColorListOld(String whCode) throws Exception;

    /**
     * 用于钥匙匹配的时候使用车架号匹配
     *
     * @param vin
     * @return
     * @throws Exception
     */
    WmsInboundOrderBO getKeysInfoByVin(String vin) throws Exception;

    void updateCancelInbound(WmsInboundOrderDTO dto) throws Exception;

    List<WmsWarehouseNoticeDetail> getColorList(String whCode);
}
