package com.unlcn.ils.wms.backend.enums;

import java.util.Objects;

/**
 * 君马出入库接口业务类型
 * @author renml
 *
 */
public enum WmsOutOfStorageType {
    ZTYPE_INBOUND_Z1("Z1","合格入库"),
    ZTYPE_INBOUND_Z2("Z2","调拨入库"),
    ZTYPE_OUTBOUND_Z3("Z3","维修出库"),
    ZTYPE_INBOUND_Z4("Z4","维修后入库"),
    ZTYPE_INBOUND_Z5("Z5","其他入库"),
    ZTYPE_OUTBOUND_Z6("Z6","其他出库"),
    ZTYPE_OUTBOUND_Z7("Z7","借用出库"),
    ZTYPE_INBOUND_Z8("Z8","借用入库"),
    ZTYPE_INBOUND_Z9("Z9","退货入库");

    private final String value;
    private final String text;
    
    WmsOutOfStorageType(String value, String text){
        this.value = value;
        this.text = text;
    }

    public String getValue() {
        return value;
    }

    public String getText() {
        return text;
    }

    public static WmsOutOfStorageType getByValue(String value){
        for (WmsOutOfStorageType temp : WmsOutOfStorageType.values()) {
            if(Objects.equals(temp.getValue(), value)){
                return temp;
            }
        }
        return null;
    }
}
