package com.unlcn.ils.wms.backend.service.sys.impl;

import cn.huiyunche.commons.exception.BusinessException;
import com.unlcn.ils.wms.backend.bo.sys.GateConfigBO;
import com.unlcn.ils.wms.backend.enums.WhCodeEnum;
import com.unlcn.ils.wms.backend.enums.WmsGateControllerTypeEnum;
import com.unlcn.ils.wms.backend.service.sys.GateConfigService;
import com.unlcn.ils.wms.base.dto.SysGateConfigDTO;
import com.unlcn.ils.wms.base.mapper.sys.SysGateConfigMapper;
import com.unlcn.ils.wms.base.model.sys.SysGateConfig;
import com.unlcn.ils.wms.base.model.sys.SysGateConfigExample;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Objects;

/**
 * @author laishijian
 * @ClassName: GateConfigServiceImpl
 * @Description: 闸门配置 service impl
 * @date 2017年11月15日 下午3:26:16
 */
@Service
public class GateConfigServiceImpl implements GateConfigService {

    @Autowired
    private SysGateConfigMapper sysGateConfigMapper;

    @Override
    public GateConfigBO getByType(String whCode, SysGateConfigDTO dto) {
        if (Objects.equals(dto, null))
            throw new BusinessException("参数为空!");
        if (StringUtils.isBlank(whCode))
            throw new BusinessException("仓库不能为空");
        if (StringUtils.isBlank(dto.getGateType()))
            throw new BusinessException("闸门类型不能为空");
        if (StringUtils.isBlank(dto.getInOutType()))
            throw new BusinessException("出入场类型不能为空");
        if (!WhCodeEnum.UNLCN_XN_CQ.getValue().equals(whCode))
            throw new BusinessException("该仓库不支持该操作!");
        if (WmsGateControllerTypeEnum.GATE_IN.getCode().equals(dto.getInOutType()))
            dto.setInOutType("10");
        if (WmsGateControllerTypeEnum.GATE_OUT.getCode().equals(dto.getInOutType()))
            dto.setInOutType("20");
        SysGateConfigExample gateConfigExample = new SysGateConfigExample();
        SysGateConfigExample.Criteria gateConfigCriteria = gateConfigExample.createCriteria();
        gateConfigCriteria.andTypeEqualTo(dto.getInOutType());
        gateConfigCriteria.andGateTypeEqualTo(dto.getGateType());
        gateConfigCriteria.andWhCodeEqualTo(whCode);
        List<SysGateConfig> gateConfigList = sysGateConfigMapper.selectByExample(gateConfigExample);
        if (gateConfigList.isEmpty()) {
            throw new BusinessException("没有找到闸门配置的数据");
        }

        GateConfigBO gateConfigBO = new GateConfigBO();
        BeanUtils.copyProperties(gateConfigList.get(0), gateConfigBO);
        return gateConfigBO;
    }
}
