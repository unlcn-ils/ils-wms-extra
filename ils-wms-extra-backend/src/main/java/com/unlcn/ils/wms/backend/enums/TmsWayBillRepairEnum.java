package com.unlcn.ils.wms.backend.enums;

/**
 * Created by zhiche024 on 2017/7/25.
 */
public enum TmsWayBillRepairEnum {
    WAYBILL_PROCESSED(20,"已处理"),
    WAYBILL_UNTREATED(10,"未处理");

    private final int value;
    private final String text;
    TmsWayBillRepairEnum(int value, String text){
        this.value = value;
        this.text = text;
    }

    public int getValue() {
        return value;
    }

    public String getText() {
        return text;
    }

    public static TmsWayBillRepairEnum getByValue(int value){
        for (TmsWayBillRepairEnum temp : TmsWayBillRepairEnum.values()) {
            if(temp.getValue() == value){
                return temp;
            }
        }
        return null;
    }
}
