package com.unlcn.ils.wms.backend.service.operatorLog.Impl;

import com.unlcn.ils.wms.backend.bo.logBO.OperatorLogBO;
import com.unlcn.ils.wms.backend.service.operatorLog.OperatorLogService;
import com.unlcn.ils.wms.base.mapper.operatorLog.WmsOperatorLogMapper;
import com.unlcn.ils.wms.base.model.operatorLog.WmsOperatorLog;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Created by DELL on 2017/9/12.
 */
@Service
public class OperatorLogServiceImpl implements OperatorLogService{
    @Autowired
    private WmsOperatorLogMapper wmsOperatorLogMapper;

    @Override
    public void insert(OperatorLogBO operatorLogBO) {
        WmsOperatorLog wmsOperatorLog = new WmsOperatorLog();
        BeanUtils.copyProperties(operatorLogBO,wmsOperatorLog);
        wmsOperatorLogMapper.insert(wmsOperatorLog);
    }
}
