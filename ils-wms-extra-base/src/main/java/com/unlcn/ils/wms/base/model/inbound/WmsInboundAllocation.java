package com.unlcn.ils.wms.base.model.inbound;

import java.io.Serializable;
import java.util.Date;

public class WmsInboundAllocation implements Serializable {
    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column wms_inbound_allocation.al_id
     *
     * @mbggenerated Wed Aug 30 11:06:18 CST 2017
     */
    private Long alId;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column wms_inbound_allocation.al_code
     *
     * @mbggenerated Wed Aug 30 11:06:18 CST 2017
     */
    private String alCode;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column wms_inbound_allocation.al_type
     *
     * @mbggenerated Wed Aug 30 11:06:18 CST 2017
     */
    private String alType;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column wms_inbound_allocation.al_status
     *
     * @mbggenerated Wed Aug 30 11:06:18 CST 2017
     */
    private String alStatus;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column wms_inbound_allocation.al_wh_id
     *
     * @mbggenerated Wed Aug 30 11:06:18 CST 2017
     */
    private String alWhId;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column wms_inbound_allocation.al_od_id
     *
     * @mbggenerated Wed Aug 30 11:06:18 CST 2017
     */
    private String alOdId;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column wms_inbound_allocation.al_customer_id
     *
     * @mbggenerated Wed Aug 30 11:06:18 CST 2017
     */
    private String alCustomerId;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column wms_inbound_allocation.al_customer_code
     *
     * @mbggenerated Wed Aug 30 11:06:18 CST 2017
     */
    private String alCustomerCode;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column wms_inbound_allocation.al_customer_name
     *
     * @mbggenerated Wed Aug 30 11:06:18 CST 2017
     */
    private String alCustomerName;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column wms_inbound_allocation.al_source_zone_id
     *
     * @mbggenerated Wed Aug 30 11:06:18 CST 2017
     */
    private String alSourceZoneId;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column wms_inbound_allocation.al_source_zone_code
     *
     * @mbggenerated Wed Aug 30 11:06:18 CST 2017
     */
    private String alSourceZoneCode;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column wms_inbound_allocation.al_source_zone_name
     *
     * @mbggenerated Wed Aug 30 11:06:18 CST 2017
     */
    private String alSourceZoneName;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column wms_inbound_allocation.al_target_zone_id
     *
     * @mbggenerated Wed Aug 30 11:06:18 CST 2017
     */
    private String alTargetZoneId;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column wms_inbound_allocation.al_target_zone_code
     *
     * @mbggenerated Wed Aug 30 11:06:18 CST 2017
     */
    private String alTargetZoneCode;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column wms_inbound_allocation.al_target_zone_name
     *
     * @mbggenerated Wed Aug 30 11:06:18 CST 2017
     */
    private String alTargetZoneName;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column wms_inbound_allocation.al_source_loc_id
     *
     * @mbggenerated Wed Aug 30 11:06:18 CST 2017
     */
    private String alSourceLocId;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column wms_inbound_allocation.al_source_loc_code
     *
     * @mbggenerated Wed Aug 30 11:06:18 CST 2017
     */
    private String alSourceLocCode;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column wms_inbound_allocation.al_source_loc_name
     *
     * @mbggenerated Wed Aug 30 11:06:18 CST 2017
     */
    private String alSourceLocName;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column wms_inbound_allocation.al_target_loc_id
     *
     * @mbggenerated Wed Aug 30 11:06:18 CST 2017
     */
    private String alTargetLocId;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column wms_inbound_allocation.al_target_loc_code
     *
     * @mbggenerated Wed Aug 30 11:06:18 CST 2017
     */
    private String alTargetLocCode;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column wms_inbound_allocation.al_target_loc_name
     *
     * @mbggenerated Wed Aug 30 11:06:18 CST 2017
     */
    private String alTargetLocName;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column wms_inbound_allocation.al_waybill_no
     *
     * @mbggenerated Wed Aug 30 11:06:18 CST 2017
     */
    private String alWaybillNo;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column wms_inbound_allocation.al_vin
     *
     * @mbggenerated Wed Aug 30 11:06:18 CST 2017
     */
    private String alVin;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column wms_inbound_allocation.al_vehicle_spec_code
     *
     * @mbggenerated Wed Aug 30 11:06:18 CST 2017
     */
    private String alVehicleSpecCode;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column wms_inbound_allocation.al_vehicle_spec_name
     *
     * @mbggenerated Wed Aug 30 11:06:18 CST 2017
     */
    private String alVehicleSpecName;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column wms_inbound_allocation.create_user_id
     *
     * @mbggenerated Wed Aug 30 11:06:18 CST 2017
     */
    private String createUserId;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column wms_inbound_allocation.create_user_name
     *
     * @mbggenerated Wed Aug 30 11:06:18 CST 2017
     */
    private String createUserName;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column wms_inbound_allocation.modify_user_id
     *
     * @mbggenerated Wed Aug 30 11:06:18 CST 2017
     */
    private String modifyUserId;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column wms_inbound_allocation.modify_user_name
     *
     * @mbggenerated Wed Aug 30 11:06:18 CST 2017
     */
    private String modifyUserName;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column wms_inbound_allocation.gmt_create
     *
     * @mbggenerated Wed Aug 30 11:06:18 CST 2017
     */
    private Date gmtCreate;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column wms_inbound_allocation.gmt_update
     *
     * @mbggenerated Wed Aug 30 11:06:18 CST 2017
     */
    private Date gmtUpdate;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column wms_inbound_allocation.is_deleted
     *
     * @mbggenerated Wed Aug 30 11:06:18 CST 2017
     */
    private Byte isDeleted;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column wms_inbound_allocation.versions
     *
     * @mbggenerated Wed Aug 30 11:06:18 CST 2017
     */
    private Long versions;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database table wms_inbound_allocation
     *
     * @mbggenerated Wed Aug 30 11:06:18 CST 2017
     */
    private static final long serialVersionUID = 1L;

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column wms_inbound_allocation.al_id
     *
     * @return the value of wms_inbound_allocation.al_id
     *
     * @mbggenerated Wed Aug 30 11:06:18 CST 2017
     */
    public Long getAlId() {
        return alId;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column wms_inbound_allocation.al_id
     *
     * @param alId the value for wms_inbound_allocation.al_id
     *
     * @mbggenerated Wed Aug 30 11:06:18 CST 2017
     */
    public void setAlId(Long alId) {
        this.alId = alId;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column wms_inbound_allocation.al_code
     *
     * @return the value of wms_inbound_allocation.al_code
     *
     * @mbggenerated Wed Aug 30 11:06:18 CST 2017
     */
    public String getAlCode() {
        return alCode;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column wms_inbound_allocation.al_code
     *
     * @param alCode the value for wms_inbound_allocation.al_code
     *
     * @mbggenerated Wed Aug 30 11:06:18 CST 2017
     */
    public void setAlCode(String alCode) {
        this.alCode = alCode == null ? null : alCode.trim();
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column wms_inbound_allocation.al_type
     *
     * @return the value of wms_inbound_allocation.al_type
     *
     * @mbggenerated Wed Aug 30 11:06:18 CST 2017
     */
    public String getAlType() {
        return alType;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column wms_inbound_allocation.al_type
     *
     * @param alType the value for wms_inbound_allocation.al_type
     *
     * @mbggenerated Wed Aug 30 11:06:18 CST 2017
     */
    public void setAlType(String alType) {
        this.alType = alType == null ? null : alType.trim();
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column wms_inbound_allocation.al_status
     *
     * @return the value of wms_inbound_allocation.al_status
     *
     * @mbggenerated Wed Aug 30 11:06:18 CST 2017
     */
    public String getAlStatus() {
        return alStatus;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column wms_inbound_allocation.al_status
     *
     * @param alStatus the value for wms_inbound_allocation.al_status
     *
     * @mbggenerated Wed Aug 30 11:06:18 CST 2017
     */
    public void setAlStatus(String alStatus) {
        this.alStatus = alStatus == null ? null : alStatus.trim();
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column wms_inbound_allocation.al_wh_id
     *
     * @return the value of wms_inbound_allocation.al_wh_id
     *
     * @mbggenerated Wed Aug 30 11:06:18 CST 2017
     */
    public String getAlWhId() {
        return alWhId;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column wms_inbound_allocation.al_wh_id
     *
     * @param alWhId the value for wms_inbound_allocation.al_wh_id
     *
     * @mbggenerated Wed Aug 30 11:06:18 CST 2017
     */
    public void setAlWhId(String alWhId) {
        this.alWhId = alWhId == null ? null : alWhId.trim();
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column wms_inbound_allocation.al_od_id
     *
     * @return the value of wms_inbound_allocation.al_od_id
     *
     * @mbggenerated Wed Aug 30 11:06:18 CST 2017
     */
    public String getAlOdId() {
        return alOdId;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column wms_inbound_allocation.al_od_id
     *
     * @param alOdId the value for wms_inbound_allocation.al_od_id
     *
     * @mbggenerated Wed Aug 30 11:06:18 CST 2017
     */
    public void setAlOdId(String alOdId) {
        this.alOdId = alOdId == null ? null : alOdId.trim();
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column wms_inbound_allocation.al_customer_id
     *
     * @return the value of wms_inbound_allocation.al_customer_id
     *
     * @mbggenerated Wed Aug 30 11:06:18 CST 2017
     */
    public String getAlCustomerId() {
        return alCustomerId;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column wms_inbound_allocation.al_customer_id
     *
     * @param alCustomerId the value for wms_inbound_allocation.al_customer_id
     *
     * @mbggenerated Wed Aug 30 11:06:18 CST 2017
     */
    public void setAlCustomerId(String alCustomerId) {
        this.alCustomerId = alCustomerId == null ? null : alCustomerId.trim();
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column wms_inbound_allocation.al_customer_code
     *
     * @return the value of wms_inbound_allocation.al_customer_code
     *
     * @mbggenerated Wed Aug 30 11:06:18 CST 2017
     */
    public String getAlCustomerCode() {
        return alCustomerCode;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column wms_inbound_allocation.al_customer_code
     *
     * @param alCustomerCode the value for wms_inbound_allocation.al_customer_code
     *
     * @mbggenerated Wed Aug 30 11:06:18 CST 2017
     */
    public void setAlCustomerCode(String alCustomerCode) {
        this.alCustomerCode = alCustomerCode == null ? null : alCustomerCode.trim();
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column wms_inbound_allocation.al_customer_name
     *
     * @return the value of wms_inbound_allocation.al_customer_name
     *
     * @mbggenerated Wed Aug 30 11:06:18 CST 2017
     */
    public String getAlCustomerName() {
        return alCustomerName;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column wms_inbound_allocation.al_customer_name
     *
     * @param alCustomerName the value for wms_inbound_allocation.al_customer_name
     *
     * @mbggenerated Wed Aug 30 11:06:18 CST 2017
     */
    public void setAlCustomerName(String alCustomerName) {
        this.alCustomerName = alCustomerName == null ? null : alCustomerName.trim();
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column wms_inbound_allocation.al_source_zone_id
     *
     * @return the value of wms_inbound_allocation.al_source_zone_id
     *
     * @mbggenerated Wed Aug 30 11:06:18 CST 2017
     */
    public String getAlSourceZoneId() {
        return alSourceZoneId;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column wms_inbound_allocation.al_source_zone_id
     *
     * @param alSourceZoneId the value for wms_inbound_allocation.al_source_zone_id
     *
     * @mbggenerated Wed Aug 30 11:06:18 CST 2017
     */
    public void setAlSourceZoneId(String alSourceZoneId) {
        this.alSourceZoneId = alSourceZoneId == null ? null : alSourceZoneId.trim();
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column wms_inbound_allocation.al_source_zone_code
     *
     * @return the value of wms_inbound_allocation.al_source_zone_code
     *
     * @mbggenerated Wed Aug 30 11:06:18 CST 2017
     */
    public String getAlSourceZoneCode() {
        return alSourceZoneCode;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column wms_inbound_allocation.al_source_zone_code
     *
     * @param alSourceZoneCode the value for wms_inbound_allocation.al_source_zone_code
     *
     * @mbggenerated Wed Aug 30 11:06:18 CST 2017
     */
    public void setAlSourceZoneCode(String alSourceZoneCode) {
        this.alSourceZoneCode = alSourceZoneCode == null ? null : alSourceZoneCode.trim();
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column wms_inbound_allocation.al_source_zone_name
     *
     * @return the value of wms_inbound_allocation.al_source_zone_name
     *
     * @mbggenerated Wed Aug 30 11:06:18 CST 2017
     */
    public String getAlSourceZoneName() {
        return alSourceZoneName;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column wms_inbound_allocation.al_source_zone_name
     *
     * @param alSourceZoneName the value for wms_inbound_allocation.al_source_zone_name
     *
     * @mbggenerated Wed Aug 30 11:06:18 CST 2017
     */
    public void setAlSourceZoneName(String alSourceZoneName) {
        this.alSourceZoneName = alSourceZoneName == null ? null : alSourceZoneName.trim();
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column wms_inbound_allocation.al_target_zone_id
     *
     * @return the value of wms_inbound_allocation.al_target_zone_id
     *
     * @mbggenerated Wed Aug 30 11:06:18 CST 2017
     */
    public String getAlTargetZoneId() {
        return alTargetZoneId;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column wms_inbound_allocation.al_target_zone_id
     *
     * @param alTargetZoneId the value for wms_inbound_allocation.al_target_zone_id
     *
     * @mbggenerated Wed Aug 30 11:06:18 CST 2017
     */
    public void setAlTargetZoneId(String alTargetZoneId) {
        this.alTargetZoneId = alTargetZoneId == null ? null : alTargetZoneId.trim();
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column wms_inbound_allocation.al_target_zone_code
     *
     * @return the value of wms_inbound_allocation.al_target_zone_code
     *
     * @mbggenerated Wed Aug 30 11:06:18 CST 2017
     */
    public String getAlTargetZoneCode() {
        return alTargetZoneCode;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column wms_inbound_allocation.al_target_zone_code
     *
     * @param alTargetZoneCode the value for wms_inbound_allocation.al_target_zone_code
     *
     * @mbggenerated Wed Aug 30 11:06:18 CST 2017
     */
    public void setAlTargetZoneCode(String alTargetZoneCode) {
        this.alTargetZoneCode = alTargetZoneCode == null ? null : alTargetZoneCode.trim();
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column wms_inbound_allocation.al_target_zone_name
     *
     * @return the value of wms_inbound_allocation.al_target_zone_name
     *
     * @mbggenerated Wed Aug 30 11:06:18 CST 2017
     */
    public String getAlTargetZoneName() {
        return alTargetZoneName;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column wms_inbound_allocation.al_target_zone_name
     *
     * @param alTargetZoneName the value for wms_inbound_allocation.al_target_zone_name
     *
     * @mbggenerated Wed Aug 30 11:06:18 CST 2017
     */
    public void setAlTargetZoneName(String alTargetZoneName) {
        this.alTargetZoneName = alTargetZoneName == null ? null : alTargetZoneName.trim();
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column wms_inbound_allocation.al_source_loc_id
     *
     * @return the value of wms_inbound_allocation.al_source_loc_id
     *
     * @mbggenerated Wed Aug 30 11:06:18 CST 2017
     */
    public String getAlSourceLocId() {
        return alSourceLocId;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column wms_inbound_allocation.al_source_loc_id
     *
     * @param alSourceLocId the value for wms_inbound_allocation.al_source_loc_id
     *
     * @mbggenerated Wed Aug 30 11:06:18 CST 2017
     */
    public void setAlSourceLocId(String alSourceLocId) {
        this.alSourceLocId = alSourceLocId == null ? null : alSourceLocId.trim();
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column wms_inbound_allocation.al_source_loc_code
     *
     * @return the value of wms_inbound_allocation.al_source_loc_code
     *
     * @mbggenerated Wed Aug 30 11:06:18 CST 2017
     */
    public String getAlSourceLocCode() {
        return alSourceLocCode;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column wms_inbound_allocation.al_source_loc_code
     *
     * @param alSourceLocCode the value for wms_inbound_allocation.al_source_loc_code
     *
     * @mbggenerated Wed Aug 30 11:06:18 CST 2017
     */
    public void setAlSourceLocCode(String alSourceLocCode) {
        this.alSourceLocCode = alSourceLocCode == null ? null : alSourceLocCode.trim();
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column wms_inbound_allocation.al_source_loc_name
     *
     * @return the value of wms_inbound_allocation.al_source_loc_name
     *
     * @mbggenerated Wed Aug 30 11:06:18 CST 2017
     */
    public String getAlSourceLocName() {
        return alSourceLocName;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column wms_inbound_allocation.al_source_loc_name
     *
     * @param alSourceLocName the value for wms_inbound_allocation.al_source_loc_name
     *
     * @mbggenerated Wed Aug 30 11:06:18 CST 2017
     */
    public void setAlSourceLocName(String alSourceLocName) {
        this.alSourceLocName = alSourceLocName == null ? null : alSourceLocName.trim();
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column wms_inbound_allocation.al_target_loc_id
     *
     * @return the value of wms_inbound_allocation.al_target_loc_id
     *
     * @mbggenerated Wed Aug 30 11:06:18 CST 2017
     */
    public String getAlTargetLocId() {
        return alTargetLocId;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column wms_inbound_allocation.al_target_loc_id
     *
     * @param alTargetLocId the value for wms_inbound_allocation.al_target_loc_id
     *
     * @mbggenerated Wed Aug 30 11:06:18 CST 2017
     */
    public void setAlTargetLocId(String alTargetLocId) {
        this.alTargetLocId = alTargetLocId == null ? null : alTargetLocId.trim();
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column wms_inbound_allocation.al_target_loc_code
     *
     * @return the value of wms_inbound_allocation.al_target_loc_code
     *
     * @mbggenerated Wed Aug 30 11:06:18 CST 2017
     */
    public String getAlTargetLocCode() {
        return alTargetLocCode;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column wms_inbound_allocation.al_target_loc_code
     *
     * @param alTargetLocCode the value for wms_inbound_allocation.al_target_loc_code
     *
     * @mbggenerated Wed Aug 30 11:06:18 CST 2017
     */
    public void setAlTargetLocCode(String alTargetLocCode) {
        this.alTargetLocCode = alTargetLocCode == null ? null : alTargetLocCode.trim();
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column wms_inbound_allocation.al_target_loc_name
     *
     * @return the value of wms_inbound_allocation.al_target_loc_name
     *
     * @mbggenerated Wed Aug 30 11:06:18 CST 2017
     */
    public String getAlTargetLocName() {
        return alTargetLocName;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column wms_inbound_allocation.al_target_loc_name
     *
     * @param alTargetLocName the value for wms_inbound_allocation.al_target_loc_name
     *
     * @mbggenerated Wed Aug 30 11:06:18 CST 2017
     */
    public void setAlTargetLocName(String alTargetLocName) {
        this.alTargetLocName = alTargetLocName == null ? null : alTargetLocName.trim();
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column wms_inbound_allocation.al_waybill_no
     *
     * @return the value of wms_inbound_allocation.al_waybill_no
     *
     * @mbggenerated Wed Aug 30 11:06:18 CST 2017
     */
    public String getAlWaybillNo() {
        return alWaybillNo;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column wms_inbound_allocation.al_waybill_no
     *
     * @param alWaybillNo the value for wms_inbound_allocation.al_waybill_no
     *
     * @mbggenerated Wed Aug 30 11:06:18 CST 2017
     */
    public void setAlWaybillNo(String alWaybillNo) {
        this.alWaybillNo = alWaybillNo == null ? null : alWaybillNo.trim();
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column wms_inbound_allocation.al_vin
     *
     * @return the value of wms_inbound_allocation.al_vin
     *
     * @mbggenerated Wed Aug 30 11:06:18 CST 2017
     */
    public String getAlVin() {
        return alVin;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column wms_inbound_allocation.al_vin
     *
     * @param alVin the value for wms_inbound_allocation.al_vin
     *
     * @mbggenerated Wed Aug 30 11:06:18 CST 2017
     */
    public void setAlVin(String alVin) {
        this.alVin = alVin == null ? null : alVin.trim();
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column wms_inbound_allocation.al_vehicle_spec_code
     *
     * @return the value of wms_inbound_allocation.al_vehicle_spec_code
     *
     * @mbggenerated Wed Aug 30 11:06:18 CST 2017
     */
    public String getAlVehicleSpecCode() {
        return alVehicleSpecCode;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column wms_inbound_allocation.al_vehicle_spec_code
     *
     * @param alVehicleSpecCode the value for wms_inbound_allocation.al_vehicle_spec_code
     *
     * @mbggenerated Wed Aug 30 11:06:18 CST 2017
     */
    public void setAlVehicleSpecCode(String alVehicleSpecCode) {
        this.alVehicleSpecCode = alVehicleSpecCode == null ? null : alVehicleSpecCode.trim();
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column wms_inbound_allocation.al_vehicle_spec_name
     *
     * @return the value of wms_inbound_allocation.al_vehicle_spec_name
     *
     * @mbggenerated Wed Aug 30 11:06:18 CST 2017
     */
    public String getAlVehicleSpecName() {
        return alVehicleSpecName;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column wms_inbound_allocation.al_vehicle_spec_name
     *
     * @param alVehicleSpecName the value for wms_inbound_allocation.al_vehicle_spec_name
     *
     * @mbggenerated Wed Aug 30 11:06:18 CST 2017
     */
    public void setAlVehicleSpecName(String alVehicleSpecName) {
        this.alVehicleSpecName = alVehicleSpecName == null ? null : alVehicleSpecName.trim();
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column wms_inbound_allocation.create_user_id
     *
     * @return the value of wms_inbound_allocation.create_user_id
     *
     * @mbggenerated Wed Aug 30 11:06:18 CST 2017
     */
    public String getCreateUserId() {
        return createUserId;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column wms_inbound_allocation.create_user_id
     *
     * @param createUserId the value for wms_inbound_allocation.create_user_id
     *
     * @mbggenerated Wed Aug 30 11:06:18 CST 2017
     */
    public void setCreateUserId(String createUserId) {
        this.createUserId = createUserId == null ? null : createUserId.trim();
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column wms_inbound_allocation.create_user_name
     *
     * @return the value of wms_inbound_allocation.create_user_name
     *
     * @mbggenerated Wed Aug 30 11:06:18 CST 2017
     */
    public String getCreateUserName() {
        return createUserName;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column wms_inbound_allocation.create_user_name
     *
     * @param createUserName the value for wms_inbound_allocation.create_user_name
     *
     * @mbggenerated Wed Aug 30 11:06:18 CST 2017
     */
    public void setCreateUserName(String createUserName) {
        this.createUserName = createUserName == null ? null : createUserName.trim();
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column wms_inbound_allocation.modify_user_id
     *
     * @return the value of wms_inbound_allocation.modify_user_id
     *
     * @mbggenerated Wed Aug 30 11:06:18 CST 2017
     */
    public String getModifyUserId() {
        return modifyUserId;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column wms_inbound_allocation.modify_user_id
     *
     * @param modifyUserId the value for wms_inbound_allocation.modify_user_id
     *
     * @mbggenerated Wed Aug 30 11:06:18 CST 2017
     */
    public void setModifyUserId(String modifyUserId) {
        this.modifyUserId = modifyUserId == null ? null : modifyUserId.trim();
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column wms_inbound_allocation.modify_user_name
     *
     * @return the value of wms_inbound_allocation.modify_user_name
     *
     * @mbggenerated Wed Aug 30 11:06:18 CST 2017
     */
    public String getModifyUserName() {
        return modifyUserName;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column wms_inbound_allocation.modify_user_name
     *
     * @param modifyUserName the value for wms_inbound_allocation.modify_user_name
     *
     * @mbggenerated Wed Aug 30 11:06:18 CST 2017
     */
    public void setModifyUserName(String modifyUserName) {
        this.modifyUserName = modifyUserName == null ? null : modifyUserName.trim();
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column wms_inbound_allocation.gmt_create
     *
     * @return the value of wms_inbound_allocation.gmt_create
     *
     * @mbggenerated Wed Aug 30 11:06:18 CST 2017
     */
    public Date getGmtCreate() {
        return gmtCreate;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column wms_inbound_allocation.gmt_create
     *
     * @param gmtCreate the value for wms_inbound_allocation.gmt_create
     *
     * @mbggenerated Wed Aug 30 11:06:18 CST 2017
     */
    public void setGmtCreate(Date gmtCreate) {
        this.gmtCreate = gmtCreate;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column wms_inbound_allocation.gmt_update
     *
     * @return the value of wms_inbound_allocation.gmt_update
     *
     * @mbggenerated Wed Aug 30 11:06:18 CST 2017
     */
    public Date getGmtUpdate() {
        return gmtUpdate;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column wms_inbound_allocation.gmt_update
     *
     * @param gmtUpdate the value for wms_inbound_allocation.gmt_update
     *
     * @mbggenerated Wed Aug 30 11:06:18 CST 2017
     */
    public void setGmtUpdate(Date gmtUpdate) {
        this.gmtUpdate = gmtUpdate;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column wms_inbound_allocation.is_deleted
     *
     * @return the value of wms_inbound_allocation.is_deleted
     *
     * @mbggenerated Wed Aug 30 11:06:18 CST 2017
     */
    public Byte getIsDeleted() {
        return isDeleted;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column wms_inbound_allocation.is_deleted
     *
     * @param isDeleted the value for wms_inbound_allocation.is_deleted
     *
     * @mbggenerated Wed Aug 30 11:06:18 CST 2017
     */
    public void setIsDeleted(Byte isDeleted) {
        this.isDeleted = isDeleted;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column wms_inbound_allocation.versions
     *
     * @return the value of wms_inbound_allocation.versions
     *
     * @mbggenerated Wed Aug 30 11:06:18 CST 2017
     */
    public Long getVersions() {
        return versions;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column wms_inbound_allocation.versions
     *
     * @param versions the value for wms_inbound_allocation.versions
     *
     * @mbggenerated Wed Aug 30 11:06:18 CST 2017
     */
    public void setVersions(Long versions) {
        this.versions = versions;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table wms_inbound_allocation
     *
     * @mbggenerated Wed Aug 30 11:06:18 CST 2017
     */
    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", alId=").append(alId);
        sb.append(", alCode=").append(alCode);
        sb.append(", alType=").append(alType);
        sb.append(", alStatus=").append(alStatus);
        sb.append(", alWhId=").append(alWhId);
        sb.append(", alOdId=").append(alOdId);
        sb.append(", alCustomerId=").append(alCustomerId);
        sb.append(", alCustomerCode=").append(alCustomerCode);
        sb.append(", alCustomerName=").append(alCustomerName);
        sb.append(", alSourceZoneId=").append(alSourceZoneId);
        sb.append(", alSourceZoneCode=").append(alSourceZoneCode);
        sb.append(", alSourceZoneName=").append(alSourceZoneName);
        sb.append(", alTargetZoneId=").append(alTargetZoneId);
        sb.append(", alTargetZoneCode=").append(alTargetZoneCode);
        sb.append(", alTargetZoneName=").append(alTargetZoneName);
        sb.append(", alSourceLocId=").append(alSourceLocId);
        sb.append(", alSourceLocCode=").append(alSourceLocCode);
        sb.append(", alSourceLocName=").append(alSourceLocName);
        sb.append(", alTargetLocId=").append(alTargetLocId);
        sb.append(", alTargetLocCode=").append(alTargetLocCode);
        sb.append(", alTargetLocName=").append(alTargetLocName);
        sb.append(", alWaybillNo=").append(alWaybillNo);
        sb.append(", alVin=").append(alVin);
        sb.append(", alVehicleSpecCode=").append(alVehicleSpecCode);
        sb.append(", alVehicleSpecName=").append(alVehicleSpecName);
        sb.append(", createUserId=").append(createUserId);
        sb.append(", createUserName=").append(createUserName);
        sb.append(", modifyUserId=").append(modifyUserId);
        sb.append(", modifyUserName=").append(modifyUserName);
        sb.append(", gmtCreate=").append(gmtCreate);
        sb.append(", gmtUpdate=").append(gmtUpdate);
        sb.append(", isDeleted=").append(isDeleted);
        sb.append(", versions=").append(versions);
        sb.append("]");
        return sb.toString();
    }
}