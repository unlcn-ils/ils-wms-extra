package com.unlcn.ils.wms.web.controller.inbound;

import cn.huiyunche.commons.domain.ResultDTO;
import cn.huiyunche.commons.exception.BusinessException;
import com.unlcn.ils.wms.backend.bo.inboundBO.WmsAllocationBO;
import com.unlcn.ils.wms.backend.bo.inboundBO.WmsInboundOrderBO;
import com.unlcn.ils.wms.backend.enums.AsnOrderCheckEnum;
import com.unlcn.ils.wms.backend.service.inbound.AsnOrderService;
import com.unlcn.ils.wms.backend.service.inbound.WmsStrategyService;
import com.unlcn.ils.wms.base.businessDTO.inbound.WmsAsnOrderQueryDTO;
import com.unlcn.ils.wms.web.utils.Vo2Bo;
import com.unlcn.ils.wms.web.vo.inbound.WmsAllocationVO;
import com.unlcn.ils.wms.web.vo.inbound.WmsInboundOrderVO;
import org.apache.commons.collections4.CollectionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Objects;

/**
 * 分配业务：人工分配、自动分配操作
 * Created by DELL on 2017/8/8.
 */
@Controller
@RequestMapping("/wmsAlloc")
@ResponseBody
public class WmsAllocationController {
    private Logger LOGGER = LoggerFactory.getLogger(WmsAllocationController.class);
    @Autowired
    private AsnOrderService asnOrderService;

    @Autowired
    private WmsStrategyService wmsStrategyService;
    /**
     * 查询状态为合格的订单
     * @param asnOrderVO
     * @return
     * @throws BusinessException
     */
    @RequestMapping(value = "/getAsnOrderPassList", method = RequestMethod.POST)
    public ResultDTO getAsnOrderPassList(@ModelAttribute("AsnOrderVO") WmsInboundOrderVO asnOrderVO) throws BusinessException {
        LOGGER.info("WmsAllocationController.getAsnOrderPassList param:{}.", asnOrderVO);
        if(Objects.equals(asnOrderVO,null)){
            BusinessException ex = new BusinessException("参数为空");
            throw ex;
        }
        ResultDTO<Object> resultDTO = new ResultDTO<>(true, null, "查询成功");
        try {
            WmsAsnOrderQueryDTO wmsAsnOrderQueryDTO = new WmsAsnOrderQueryDTO();
            BeanUtils.copyProperties(asnOrderVO, wmsAsnOrderQueryDTO);
            wmsAsnOrderQueryDTO.setOdStatus(String.valueOf(AsnOrderCheckEnum.ASNORDER_PASS.getValue()));
            resultDTO.setData(asnOrderService.getListAsnOrderBO(wmsAsnOrderQueryDTO));
            resultDTO.setSuccess(true);
            resultDTO.setMessage("查询合格订单成功");
        } catch (Exception ex) {
            LOGGER.error("WmsAllocationController.getAsnOrderPassList error: {}", ex);
            resultDTO.setSuccess(false);
            resultDTO.setMessage("查询合格订单异常");
        }

        return resultDTO;
    }

    /*@RequestMapping(value = "/autoAllcation")
    public ResultDTO autoAllcation() throws Exception {
        ResultDTO resultDTO = new ResultDTO();
        try{
            List<WmsAllocationBO> wmsAllocationBOList = Lists.newArrayList();
            WmsAllocationBO wmsAllocationBO = new WmsAllocationBO();
            wmsAllocationBO.setAlOdId("127");
            wmsAllocationBO.setAlCustomerId("1174");
            wmsAllocationBO.setAlCustomerCode("江铃控股");
            wmsAllocationBO.setAlVehicleSpecCode("13");
            wmsAllocationBO.setAlVehicleSpecName("众泰Z200HB 2011款 1.5L 自动豪华型");
            wmsAllocationBO.setAlWaybillNo("bill001");
            wmsAllocationBO.setAlVin("vin");
            wmsAllocationBOList.add(wmsAllocationBO);
            List<WmsAllocationBO> result = wmsStrategyService.updateAutoAllocation(wmsAllocationBOList,"001");
            resultDTO.setSuccess(true);
            resultDTO.setMessage("自动分配成功");
            resultDTO.setData(result);
            return resultDTO;
        }catch(BusinessException ex){
            LOGGER.info("WmsAllocationController.autoAllcation param:{}.");
            resultDTO.setSuccess(false);
            resultDTO.setMessage("自动分配异常");
            return resultDTO;
        }
    }*/

    @RequestMapping(value = "/autoAllcation/{whCode}",method = RequestMethod.POST)
    public ResultDTO autoAllcation(@RequestBody List<WmsAllocationVO> wmsAllocationVOList,
                                   @PathVariable("whCode") String whCode) throws Exception {
        ResultDTO resultDTO = new ResultDTO();
        try{
            Vo2Bo converter = new Vo2Bo();
            List<WmsAllocationBO> wmsAllocationBOList = converter.convert(wmsAllocationVOList, WmsAllocationBO.class);
            List<WmsAllocationBO> result = wmsStrategyService.updateAutoAllocation(wmsAllocationBOList,whCode);
            resultDTO.setSuccess(true);
            resultDTO.setMessage("自动分配成功");
            resultDTO.setData(result);
            return resultDTO;
        }catch(BusinessException ex){
            LOGGER.info("WmsAllocationController.autoAllcation param:{}.");
            resultDTO.setSuccess(false);
            resultDTO.setMessage("自动分配异常");
            return resultDTO;
        }
    }
    /**
     * 创建一个分配记录
     * @param wmsAllocationVOList
     * @return
     */
   /* @RequestMapping(value = "/createAlloc/{allocType}",method = RequestMethod.POST)
    public ResultDTO createAllocation (@RequestBody List<WmsAllocationVO> wmsAllocationVOList,
                                  @PathVariable("allocType") String allocationType) throws IOException,Exception {
        LOGGER.info("WmsAllocationController.createAlloc param:{}.");
        ResultDTO<Object> resultDTO = new ResultDTO<Object>();
        //以转之后的对象属性BO为准 VO没有 BO差集即为初始值
        if(CollectionUtils.isEmpty(wmsAllocationVOList)){
            Exception ex = new Exception("参数为空");
            throw ex;
        }
        try {
            Vo2Bo converter = new Vo2Bo();
            List<WmsAllocationBO> wmsAllocationBOList = converter.convert(wmsAllocationVOList, WmsAllocationBO.class);
            String result = wmsStrategyService.addAllocation(wmsAllocationBOList, allocationType);
            resultDTO.setData(result);
            resultDTO.setSuccess(true);
            resultDTO.setMessage("分配成功");
            resultDTO.setMessageCode("200");
        }catch(Exception ex){
            LOGGER.error("WmsAllocationController.createAllocation error: {}", ex);
            resultDTO.setSuccess(false);
            resultDTO.setMessage("分配异常");
            resultDTO.setMessageCode("500");
        }
        return resultDTO;
    }*/

    /**
     * 确认分配，确认分配后再次修改只能在库存概况处修改
     * @param asnOrderVOList
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/confirmAllocation", method=RequestMethod.POST)
    public ResultDTO confirmAllocation(@RequestBody List<WmsInboundOrderVO> asnOrderVOList)throws Exception{
        ResultDTO<Object> resultDTO = new ResultDTO<Object>();
        if(CollectionUtils.isEmpty(asnOrderVOList)){
            Exception ex = new Exception("参数为空");
            throw ex;
        }
        try{
            Vo2Bo converter = new Vo2Bo();
            List<WmsInboundOrderBO> wmsInboundOrderBOList = converter.convert(asnOrderVOList, WmsInboundOrderBO.class);
            wmsStrategyService.updateToConfirmAllocation(wmsInboundOrderBOList);
            resultDTO.setSuccess(true);
            resultDTO.setMessageCode("200");
        }catch(Exception ex){
            LOGGER.error("WmsAllocationController.confirmAllocation error: {}", ex);
            resultDTO.setSuccess(false);
            resultDTO.setMessage("分配确认异常，请联系管理员!");
        }
        return resultDTO;
    }

    public static void main(String[]args){
        System.out.println("B-002-001".substring("B-002-001".indexOf("-")+1,"B-002-001".lastIndexOf("-")));
    }
}