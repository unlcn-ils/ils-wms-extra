package com.unlcn.ils.wms.base.model.stock;

import java.io.Serializable;
import java.util.Date;

public class WmsBorrowCar implements Serializable {
    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column wms_borrow_car.bc_id
     *
     * @mbggenerated Sat Nov 04 18:33:26 CST 2017
     */
    private Long bcId;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column wms_borrow_car.bc_borrow_no
     *
     * @mbggenerated Sat Nov 04 18:33:26 CST 2017
     */
    private String bcBorrowNo;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column wms_borrow_car.bc_borrow_name
     *
     * @mbggenerated Sat Nov 04 18:33:26 CST 2017
     */
    private String bcBorrowName;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column wms_borrow_car.bc_borrow_department
     *
     * @mbggenerated Sat Nov 04 18:33:26 CST 2017
     */
    private String bcBorrowDepartment;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column wms_borrow_car.bc_borrow_tel
     *
     * @mbggenerated Sat Nov 04 18:33:26 CST 2017
     */
    private String bcBorrowTel;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column wms_borrow_car.bc_borrow_date
     *
     * @mbggenerated Sat Nov 04 18:33:26 CST 2017
     */
    private Date bcBorrowDate;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column wms_borrow_car.bc_estimated_return_date
     *
     * @mbggenerated Sat Nov 04 18:33:26 CST 2017
     */
    private Date bcEstimatedReturnDate;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column wms_borrow_car.bc_borrow_reason
     *
     * @mbggenerated Sat Nov 04 18:33:26 CST 2017
     */
    private String bcBorrowReason;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column wms_borrow_car.bc_whcode
     *
     * @mbggenerated Sat Nov 04 18:33:26 CST 2017
     */
    private String bcWhcode;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column wms_borrow_car.gmt_create
     *
     * @mbggenerated Sat Nov 04 18:33:26 CST 2017
     */
    private Date gmtCreate;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column wms_borrow_car.gmt_update
     *
     * @mbggenerated Sat Nov 04 18:33:26 CST 2017
     */
    private Date gmtUpdate;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column wms_borrow_car.delete_flag
     *
     * @mbggenerated Sat Nov 04 18:33:26 CST 2017
     */
    private Boolean deleteFlag;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column wms_borrow_car.upload_key
     *
     * @mbggenerated Sat Nov 04 18:33:26 CST 2017
     */
    private String uploadKey;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database table wms_borrow_car
     *
     * @mbggenerated Sat Nov 04 18:33:26 CST 2017
     */
    private static final long serialVersionUID = 1L;

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column wms_borrow_car.bc_id
     *
     * @return the value of wms_borrow_car.bc_id
     *
     * @mbggenerated Sat Nov 04 18:33:26 CST 2017
     */
    public Long getBcId() {
        return bcId;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column wms_borrow_car.bc_id
     *
     * @param bcId the value for wms_borrow_car.bc_id
     *
     * @mbggenerated Sat Nov 04 18:33:26 CST 2017
     */
    public void setBcId(Long bcId) {
        this.bcId = bcId;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column wms_borrow_car.bc_borrow_no
     *
     * @return the value of wms_borrow_car.bc_borrow_no
     *
     * @mbggenerated Sat Nov 04 18:33:26 CST 2017
     */
    public String getBcBorrowNo() {
        return bcBorrowNo;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column wms_borrow_car.bc_borrow_no
     *
     * @param bcBorrowNo the value for wms_borrow_car.bc_borrow_no
     *
     * @mbggenerated Sat Nov 04 18:33:26 CST 2017
     */
    public void setBcBorrowNo(String bcBorrowNo) {
        this.bcBorrowNo = bcBorrowNo == null ? null : bcBorrowNo.trim();
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column wms_borrow_car.bc_borrow_name
     *
     * @return the value of wms_borrow_car.bc_borrow_name
     *
     * @mbggenerated Sat Nov 04 18:33:26 CST 2017
     */
    public String getBcBorrowName() {
        return bcBorrowName;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column wms_borrow_car.bc_borrow_name
     *
     * @param bcBorrowName the value for wms_borrow_car.bc_borrow_name
     *
     * @mbggenerated Sat Nov 04 18:33:26 CST 2017
     */
    public void setBcBorrowName(String bcBorrowName) {
        this.bcBorrowName = bcBorrowName == null ? null : bcBorrowName.trim();
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column wms_borrow_car.bc_borrow_department
     *
     * @return the value of wms_borrow_car.bc_borrow_department
     *
     * @mbggenerated Sat Nov 04 18:33:26 CST 2017
     */
    public String getBcBorrowDepartment() {
        return bcBorrowDepartment;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column wms_borrow_car.bc_borrow_department
     *
     * @param bcBorrowDepartment the value for wms_borrow_car.bc_borrow_department
     *
     * @mbggenerated Sat Nov 04 18:33:26 CST 2017
     */
    public void setBcBorrowDepartment(String bcBorrowDepartment) {
        this.bcBorrowDepartment = bcBorrowDepartment == null ? null : bcBorrowDepartment.trim();
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column wms_borrow_car.bc_borrow_tel
     *
     * @return the value of wms_borrow_car.bc_borrow_tel
     *
     * @mbggenerated Sat Nov 04 18:33:26 CST 2017
     */
    public String getBcBorrowTel() {
        return bcBorrowTel;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column wms_borrow_car.bc_borrow_tel
     *
     * @param bcBorrowTel the value for wms_borrow_car.bc_borrow_tel
     *
     * @mbggenerated Sat Nov 04 18:33:26 CST 2017
     */
    public void setBcBorrowTel(String bcBorrowTel) {
        this.bcBorrowTel = bcBorrowTel == null ? null : bcBorrowTel.trim();
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column wms_borrow_car.bc_borrow_date
     *
     * @return the value of wms_borrow_car.bc_borrow_date
     *
     * @mbggenerated Sat Nov 04 18:33:26 CST 2017
     */
    public Date getBcBorrowDate() {
        return bcBorrowDate;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column wms_borrow_car.bc_borrow_date
     *
     * @param bcBorrowDate the value for wms_borrow_car.bc_borrow_date
     *
     * @mbggenerated Sat Nov 04 18:33:26 CST 2017
     */
    public void setBcBorrowDate(Date bcBorrowDate) {
        this.bcBorrowDate = bcBorrowDate;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column wms_borrow_car.bc_estimated_return_date
     *
     * @return the value of wms_borrow_car.bc_estimated_return_date
     *
     * @mbggenerated Sat Nov 04 18:33:26 CST 2017
     */
    public Date getBcEstimatedReturnDate() {
        return bcEstimatedReturnDate;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column wms_borrow_car.bc_estimated_return_date
     *
     * @param bcEstimatedReturnDate the value for wms_borrow_car.bc_estimated_return_date
     *
     * @mbggenerated Sat Nov 04 18:33:26 CST 2017
     */
    public void setBcEstimatedReturnDate(Date bcEstimatedReturnDate) {
        this.bcEstimatedReturnDate = bcEstimatedReturnDate;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column wms_borrow_car.bc_borrow_reason
     *
     * @return the value of wms_borrow_car.bc_borrow_reason
     *
     * @mbggenerated Sat Nov 04 18:33:26 CST 2017
     */
    public String getBcBorrowReason() {
        return bcBorrowReason;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column wms_borrow_car.bc_borrow_reason
     *
     * @param bcBorrowReason the value for wms_borrow_car.bc_borrow_reason
     *
     * @mbggenerated Sat Nov 04 18:33:26 CST 2017
     */
    public void setBcBorrowReason(String bcBorrowReason) {
        this.bcBorrowReason = bcBorrowReason == null ? null : bcBorrowReason.trim();
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column wms_borrow_car.bc_whcode
     *
     * @return the value of wms_borrow_car.bc_whcode
     *
     * @mbggenerated Sat Nov 04 18:33:26 CST 2017
     */
    public String getBcWhcode() {
        return bcWhcode;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column wms_borrow_car.bc_whcode
     *
     * @param bcWhcode the value for wms_borrow_car.bc_whcode
     *
     * @mbggenerated Sat Nov 04 18:33:26 CST 2017
     */
    public void setBcWhcode(String bcWhcode) {
        this.bcWhcode = bcWhcode == null ? null : bcWhcode.trim();
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column wms_borrow_car.gmt_create
     *
     * @return the value of wms_borrow_car.gmt_create
     *
     * @mbggenerated Sat Nov 04 18:33:26 CST 2017
     */
    public Date getGmtCreate() {
        return gmtCreate;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column wms_borrow_car.gmt_create
     *
     * @param gmtCreate the value for wms_borrow_car.gmt_create
     *
     * @mbggenerated Sat Nov 04 18:33:26 CST 2017
     */
    public void setGmtCreate(Date gmtCreate) {
        this.gmtCreate = gmtCreate;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column wms_borrow_car.gmt_update
     *
     * @return the value of wms_borrow_car.gmt_update
     *
     * @mbggenerated Sat Nov 04 18:33:26 CST 2017
     */
    public Date getGmtUpdate() {
        return gmtUpdate;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column wms_borrow_car.gmt_update
     *
     * @param gmtUpdate the value for wms_borrow_car.gmt_update
     *
     * @mbggenerated Sat Nov 04 18:33:26 CST 2017
     */
    public void setGmtUpdate(Date gmtUpdate) {
        this.gmtUpdate = gmtUpdate;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column wms_borrow_car.delete_flag
     *
     * @return the value of wms_borrow_car.delete_flag
     *
     * @mbggenerated Sat Nov 04 18:33:26 CST 2017
     */
    public Boolean getDeleteFlag() {
        return deleteFlag;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column wms_borrow_car.delete_flag
     *
     * @param deleteFlag the value for wms_borrow_car.delete_flag
     *
     * @mbggenerated Sat Nov 04 18:33:26 CST 2017
     */
    public void setDeleteFlag(Boolean deleteFlag) {
        this.deleteFlag = deleteFlag;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column wms_borrow_car.upload_key
     *
     * @return the value of wms_borrow_car.upload_key
     *
     * @mbggenerated Sat Nov 04 18:33:26 CST 2017
     */
    public String getUploadKey() {
        return uploadKey;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column wms_borrow_car.upload_key
     *
     * @param uploadKey the value for wms_borrow_car.upload_key
     *
     * @mbggenerated Sat Nov 04 18:33:26 CST 2017
     */
    public void setUploadKey(String uploadKey) {
        this.uploadKey = uploadKey == null ? null : uploadKey.trim();
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table wms_borrow_car
     *
     * @mbggenerated Sat Nov 04 18:33:26 CST 2017
     */
    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", bcId=").append(bcId);
        sb.append(", bcBorrowNo=").append(bcBorrowNo);
        sb.append(", bcBorrowName=").append(bcBorrowName);
        sb.append(", bcBorrowDepartment=").append(bcBorrowDepartment);
        sb.append(", bcBorrowTel=").append(bcBorrowTel);
        sb.append(", bcBorrowDate=").append(bcBorrowDate);
        sb.append(", bcEstimatedReturnDate=").append(bcEstimatedReturnDate);
        sb.append(", bcBorrowReason=").append(bcBorrowReason);
        sb.append(", bcWhcode=").append(bcWhcode);
        sb.append(", gmtCreate=").append(gmtCreate);
        sb.append(", gmtUpdate=").append(gmtUpdate);
        sb.append(", deleteFlag=").append(deleteFlag);
        sb.append(", uploadKey=").append(uploadKey);
        sb.append("]");
        return sb.toString();
    }
}