package com.unlcn.ils.wms.base.mapper.additional;

import com.unlcn.ils.wms.base.model.stock.WmsZone;

import java.util.List;
import java.util.Map;

/**
 * 库区
 * Created by DELL on 2017/8/14.
 */
public interface WmsZoneExtMapper {

    /**
     * 通过条件获得库区数据
     * @param paramMap
     * @return
     */
    List<WmsZone> getAllZone(Map<String,Object> paramMap);

    /**
     * 分页数据
     * @param paramMap
     * @return
     */
    List<WmsZone> queryForList(Map<String,Object> paramMap);

    /**
     * 分页数据总数
     * @param paramMap
     * @return
     */
    Integer queryForCount(Map<String,Object> paramMap);

}
