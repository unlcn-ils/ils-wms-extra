package com.unlcn.ils.wms.base.dto;

import com.unlcn.ils.wms.base.model.stock.WmsBorrowCarDetail;

import java.util.Date;
import java.util.List;

/**
 * Created by lenovo on 2017/10/20.
 */
public class BorrowDetailDTO {
    // 借车单id
    private Long bcId;

    // 借车单号
    private String bcBorrowNo;

    // 借用人姓名
    private String bcBorrowName;

    // 借用部门
    private String bcBorrowDepartment;

    // 联系电话
    private String bcBorrowTel;

    // 借用日期
    private Date bcBorrowDate;

    // 预计还车日期
    private Date bcEstimatedReturnDate;

    // 借车原因
    private String bcBorrowReason;

    // 上传凭证key
    private String uploadKey;

    // 上传凭证url
    private String uploadPath;

    // 借车单详情
    List<WmsBorrowCarDetail> wmsBorrowCarDetailList;

    public Long getBcId() {
        return bcId;
    }

    public void setBcId(Long bcId) {
        this.bcId = bcId;
    }

    public String getBcBorrowNo() {
        return bcBorrowNo;
    }

    public void setBcBorrowNo(String bcBorrowNo) {
        this.bcBorrowNo = bcBorrowNo;
    }

    public String getBcBorrowName() {
        return bcBorrowName;
    }

    public void setBcBorrowName(String bcBorrowName) {
        this.bcBorrowName = bcBorrowName;
    }

    public String getBcBorrowDepartment() {
        return bcBorrowDepartment;
    }

    public void setBcBorrowDepartment(String bcBorrowDepartment) {
        this.bcBorrowDepartment = bcBorrowDepartment;
    }

    public String getBcBorrowTel() {
        return bcBorrowTel;
    }

    public void setBcBorrowTel(String bcBorrowTel) {
        this.bcBorrowTel = bcBorrowTel;
    }

    public Date getBcBorrowDate() {
        return bcBorrowDate;
    }

    public void setBcBorrowDate(Date bcBorrowDate) {
        this.bcBorrowDate = bcBorrowDate;
    }

    public Date getBcEstimatedReturnDate() {
        return bcEstimatedReturnDate;
    }

    public void setBcEstimatedReturnDate(Date bcEstimatedReturnDate) {
        this.bcEstimatedReturnDate = bcEstimatedReturnDate;
    }

    public String getBcBorrowReason() {
        return bcBorrowReason;
    }

    public void setBcBorrowReason(String bcBorrowReason) {
        this.bcBorrowReason = bcBorrowReason;
    }

    public String getUploadKey() {
        return uploadKey;
    }

    public void setUploadKey(String uploadKey) {
        this.uploadKey = uploadKey;
    }

    public String getUploadPath() {
        return uploadPath;
    }

    public void setUploadPath(String uploadPath) {
        this.uploadPath = uploadPath;
    }

    public List<WmsBorrowCarDetail> getWmsBorrowCarDetailList() {
        return wmsBorrowCarDetailList;
    }

    public void setWmsBorrowCarDetailList(List<WmsBorrowCarDetail> wmsBorrowCarDetailList) {
        this.wmsBorrowCarDetailList = wmsBorrowCarDetailList;
    }

    @Override
    public String toString() {
        return "BorrowDetailDTO{" +
                "bcId=" + bcId +
                ", bcBorrowNo='" + bcBorrowNo + '\'' +
                ", bcBorrowName='" + bcBorrowName + '\'' +
                ", bcBorrowDepartment='" + bcBorrowDepartment + '\'' +
                ", bcBorrowTel='" + bcBorrowTel + '\'' +
                ", bcBorrowDate=" + bcBorrowDate +
                ", bcEstimatedReturnDate=" + bcEstimatedReturnDate +
                ", bcBorrowReason='" + bcBorrowReason + '\'' +
                ", uploadKey='" + uploadKey + '\'' +
                ", uploadPath='" + uploadPath + '\'' +
                ", wmsBorrowCarDetailList=" + wmsBorrowCarDetailList +
                '}';
    }
}
