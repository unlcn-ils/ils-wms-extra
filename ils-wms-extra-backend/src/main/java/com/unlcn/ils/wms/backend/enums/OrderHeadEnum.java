package com.unlcn.ils.wms.backend.enums;

/**
 * @Auther linbao
 * @Date 2017-11-06
 */
public enum OrderHeadEnum {

    HANDOVER("HA", "交接单"),
    REPIR("RE", "维修单"),
    BORROW("BO", "借车单");

    private final String code;

    private final String value;

    public String getCode() {
        return code;
    }

    public String getValue() {
        return value;
    }

    OrderHeadEnum(String code, String value) {
        this.code = code;
        this.value = value;
    }
}
