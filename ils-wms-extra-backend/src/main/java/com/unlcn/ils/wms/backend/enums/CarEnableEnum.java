package com.unlcn.ils.wms.backend.enums;

/**
 * @Auther linbao
 * @Date 2017-11-08
 * 门禁卡可用状态
 */
public enum CarEnableEnum {

    USABLE(1, "可用"),
    DISABLED(0, "禁用");

    private final Integer code;
    private final String value;

    public Integer getCode() {
        return code;
    }

    public String getValue() {
        return value;
    }

    CarEnableEnum(Integer code, String value) {
        this.code = code;
        this.value = value;
    }
}
