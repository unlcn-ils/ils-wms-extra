package com.unlcn.ils.wms.backend.dto.sysDTO;

import java.util.List;

/**
 * @Auther linbao
 * @Date 2017-11-09
 */
public class UserNewDTO {

    private Integer userId;

    private String userName;

    private String token;

    /**
     * 二维码超时时间
     */
    private String timeout;

    public String getTimeout() {
        return timeout;
    }

    public void setTimeout(String timeout) {
        this.timeout = timeout;
    }

    private List<PermissNewDTO> permissionsBOList;

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public String getUserName() {
        return userName;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public List<PermissNewDTO> getPermissionsBOList() {
        return permissionsBOList;
    }

    public void setPermissionsBOList(List<PermissNewDTO> permissionsBOList) {
        this.permissionsBOList = permissionsBOList;
    }
}
