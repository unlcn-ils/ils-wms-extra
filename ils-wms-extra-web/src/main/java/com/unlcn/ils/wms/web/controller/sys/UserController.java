package com.unlcn.ils.wms.web.controller.sys;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.unlcn.ils.wms.backend.bo.sys.PermissionsBO;
import com.unlcn.ils.wms.backend.bo.sys.RoleBO;
import com.unlcn.ils.wms.backend.bo.sys.UserBO;
import com.unlcn.ils.wms.backend.service.sys.UserService;
import com.unlcn.ils.wms.web.vo.sys.PermissionsVO;
import com.unlcn.ils.wms.web.vo.sys.RoleVO;
import com.unlcn.ils.wms.web.vo.sys.UserVO;

import cn.huiyunche.commons.domain.PageVo;
import cn.huiyunche.commons.domain.ResultDTO;
import cn.huiyunche.commons.domain.ResultDTOWithPagination;
import cn.huiyunche.commons.exception.BusinessException;

@Controller
@RequestMapping("/user")
@ResponseBody
public class UserController {
	
	private Logger LOGGER = LoggerFactory.getLogger(UserController.class);
	
	@Autowired
	private UserService userService;
	
	/**
	 * 
	 * @Title: add 
	 * @Description: 新增用户
	 * @param @param userVO
	 * @param @return     
	 * @return ResultDTO<Object>    返回类型 
	 * @throws 
	 *
	 */
	@RequestMapping(value = "/add",method=RequestMethod.POST)
	public ResultDTO<Object> add(@RequestBody UserVO userVO){
		
		LOGGER.info("UserController.add param : {}", userVO);
		
		ResultDTO<Object> result = new ResultDTO<>(true);
		try {
			UserBO userBO = new UserBO();
			BeanUtils.copyProperties(userVO, userBO);
			
			if(userVO.getRoleVOList() != null) {
				userBO.setRoleBOList(new ArrayList<RoleBO>());
				for(RoleVO roleVO : userVO.getRoleVOList()) {
					RoleBO roleBO = new RoleBO();
					BeanUtils.copyProperties(roleVO, roleBO);
					userBO.getRoleBOList().add(roleBO);
				}
			}
			
			userService.addUser(userBO);
			result.setMessage("新增用户成功");
			
		}catch(BusinessException be) {
			result.setSuccess(false);
			result.setMessage(be.getMessage());
			LOGGER.error("UserController.add error: {}", be);
		}catch(Exception e) {
			result.setSuccess(false);
			result.setMessage("系统异常");
			LOGGER.error("UserController.add error: {}", e);
		}
		return result;
	}
	
	/**
	 * 
	 * @Title: get 
	 * @Description: 根据id查询用户
	 * @param @param id
	 * @param @return     
	 * @return ResultDTO<UserVO>    返回类型 
	 * @throws 
	 *
	 */
	@RequestMapping(value = "/get/{id}",method=RequestMethod.GET)
	public ResultDTO<UserVO> get(@PathVariable("id") Integer id){
		
		LOGGER.info("UserController.get param : {}", id);
		
		ResultDTO<UserVO> result = new ResultDTO<>(true);
		try {
			UserBO userBO = userService.findById(id);
			
			if(null != userBO) {
				UserVO userVO = new UserVO();
				BeanUtils.copyProperties(userBO, userVO);
				result.setData(userVO);
			}
			
			result.setMessage("查询用户成功");
			
		}catch(BusinessException be) {
			result.setSuccess(false);
			result.setMessage(be.getMessage());
			LOGGER.error("UserController.get error: {}", be);
		}catch(Exception e) {
			result.setSuccess(false);
			result.setMessage("系统异常");
			LOGGER.error("UserController.get error: {}", e);
		}
		return result;
	}
	
	/**
	 * 
	 * @Title: update 
	 * @Description: 更新用户
	 * @param @param userVO
	 * @param @return     
	 * @return ResultDTO<Object>    返回类型 
	 * @throws 
	 *
	 */
	@RequestMapping(value = "/update",method=RequestMethod.PUT)
	public ResultDTO<Object> update(@RequestBody UserVO userVO){
		
		LOGGER.info("UserController.update param : {}", userVO);
		
		ResultDTO<Object> result = new ResultDTO<>(true);
		try {
			UserBO userBO = new UserBO();
			BeanUtils.copyProperties(userVO, userBO);
			userService.updateUser(userBO);
			result.setMessage("更新用户成功");
			
		}catch(BusinessException be) {
			result.setSuccess(false);
			result.setMessage(be.getMessage());
			LOGGER.error("UserController.update error: {}", be);
		}catch(Exception e) {
			result.setSuccess(false);
			result.setMessage("系统异常");
			LOGGER.error("UserController.update error: {}", e);
		}
		return result;
	}
	
	/**
	 * 
	 * @Title: delete 
	 * @Description: 删除用户
	 * @param @param ids
	 * @param @return     
	 * @return ResultDTO<Object>    返回类型 
	 * @throws 
	 *
	 */
	@RequestMapping(value="/delete/{ids}",method=RequestMethod.DELETE)
	public ResultDTO<Object> delete(@PathVariable("ids") String ids){
		
		LOGGER.info("UserController.delete param : {}", ids);
		
		ResultDTO<Object> result = new ResultDTO<>(true);
		try {
			userService.deleteUserByIds(ids);
			result.setMessage("删除用户成功");
		}catch(BusinessException be) {
			result.setSuccess(false);
			result.setMessage(be.getMessage());
			LOGGER.error("UserController.delete error: {}", be);
		}catch(Exception e) {
			result.setSuccess(false);
			result.setMessage("系统异常");
			LOGGER.error("UserController.delete error: {}", e);
		}
		return result;
	}
	
	/**
	 * 
	 * @Title: list 
	 * @Description: 列表
	 * @param @param userVO
	 * @param @return     
	 * @return ResultDTOWithPagination<List<UserVO>>    返回类型 
	 * @throws 
	 *
	 */
	@RequestMapping(value="/list",method=RequestMethod.POST)
	public ResultDTOWithPagination<List<UserVO>> list(@RequestBody UserVO userVO){
		
		LOGGER.info("UserController.list param : {}", userVO);
		
		ResultDTOWithPagination<List<UserVO>> result = new ResultDTOWithPagination<>(true);
		try {
			PageVo pageVo = userVO.getPageVO();
			
			UserBO userBO = new UserBO();
			BeanUtils.copyProperties(userVO, userBO);
			List<UserBO> userBOList = userService.list(userBO, pageVo);
			
			List<UserVO> userVOList = new ArrayList<>();
			for (UserBO uBO : userBOList) {
				UserVO newUserVO = new UserVO();
				BeanUtils.copyProperties(uBO, newUserVO);
				userVOList.add(newUserVO);
			}
			
			result.setData(userVOList);
			result.setMessage("获取用户列表成功");
		}catch(BusinessException be) {
			result.setSuccess(false);
			result.setMessage(be.getMessage());
			LOGGER.error("UserController.list error: {}", be);
		}catch(Exception e) {
			result.setSuccess(false);
			result.setMessage("系统异常");
			LOGGER.error("UserController.list error: {}", e);
		}
		return result;
	}
	
	/**
	 * 
	 * @Title: login 
	 * @Description: 登陆
	 * @param @param userVO
	 * @param @return     
	 * @return ResultDTO<UserVO>    返回类型 
	 * @throws 
	 *
	 */
	@RequestMapping(value="/login",method=RequestMethod.POST)
	public ResultDTO<UserVO> login(@RequestBody UserVO userVO){
		
		LOGGER.info("UserController.login param : {}", userVO);
		
		ResultDTO<UserVO> result = new ResultDTO<>(true);
		try {
			UserBO userBO = new UserBO();
			BeanUtils.copyProperties(userVO, userBO);
			UserBO newUserBO = userService.getLoginInfo(userBO);
			
			UserVO resultUserVO = new UserVO();
			BeanUtils.copyProperties(newUserBO, resultUserVO);
			
			//转换角色
			resultUserVO.setRoleVOList(new ArrayList<RoleVO>());
			for(RoleBO roleBO : newUserBO.getRoleBOList()) {
				RoleVO roleVO = new RoleVO();
				BeanUtils.copyProperties(roleBO, roleVO);
				roleVO.setPermissionsVOList(converPermissionsBOToVO(roleBO.getPermissionsBOList()));	//转换权限
				resultUserVO.getRoleVOList().add(roleVO);
			}
			
			result.setData(resultUserVO);
			result.setMessage("登陆成功");
		}catch(BusinessException be) {
			result.setSuccess(false);
			result.setMessage(be.getMessage());
			LOGGER.error("UserController.login error: {}", be);
		}catch(Exception e) {
			result.setSuccess(false);
			result.setMessage("系统异常");
			LOGGER.error("UserController.login error: {}", e);
		}
		return result;
	}
	
	/**
	 * 
	 * @Title: converPermissionsBOToVO 
	 * @Description: 转换权限VO
	 * @param @param permissionsBOList
	 * @param @return     
	 * @return List<PermissionsVO>    返回类型 
	 * @throws 
	 *
	 */
	private List<PermissionsVO> converPermissionsBOToVO(List<PermissionsBO> permissionsBOList){
		
		List<PermissionsVO> permissionsVOList = new ArrayList<>();
		for(PermissionsBO permissionsBO : permissionsBOList) {
			PermissionsVO permissionsVO = new PermissionsVO();
			BeanUtils.copyProperties(permissionsBO, permissionsVO);
			permissionsVO.setChildPermissionsVOList(converPermissionsBOToVO(permissionsBO.getChildPermissionsBOList()));
			permissionsVOList.add(permissionsVO);
		}
		return permissionsVOList;
			
	}
}
