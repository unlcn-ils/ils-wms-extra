package com.unlcn.ils.wms.backend.service.webservice.client.enums;

/**
 * @Date : 2017/9/13.
 */
public enum WmsWebserviceInterfaceEnum {


    INBOUND_OUT_SAP("IF_WMS2SAP_Vehicle_GIGR", "SAP出入库"),
    HANDOVER_SAP("IF_WMS2SAP_Vehicle_PGI", "SAP交接单接口"),
    DEALERCAR_SAP("IF_WMS2SAP_Vehicle_PGR", "SAP退车接口"),
    INBOUND_OUT_DCS("DCS_GIGR", "DCS出入库"),
    HANDOVER_DCS("DCS_PGI", "DCS交接单接口"),
    BILLCANCEL_DCS("BILLCANCEL", "发运驳回接口");

    private final String value;
    private final String text;

    WmsWebserviceInterfaceEnum(String value, String text) {
        this.value = value;
        this.text = text;
    }

    public String getValue() {
        return value;
    }

    public String getText() {
        return text;
    }

    public static WmsWebserviceInterfaceEnum getByValue(String value) {
        for (WmsWebserviceInterfaceEnum temp : WmsWebserviceInterfaceEnum.values()) {
            if (temp.getValue().equals(value)) {
                return temp;
            }
        }
        return null;
    }
}
