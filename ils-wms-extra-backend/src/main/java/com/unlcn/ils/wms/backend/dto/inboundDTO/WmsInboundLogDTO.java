package com.unlcn.ils.wms.backend.dto.inboundDTO;

import java.util.Date;

/**
 * Created by DELL on 2017/8/18.
 */
public class WmsInboundLogDTO {

    private Long id;

    private Date gmtCreate;

    private Date gmtModify;

    private Long entryId;

    private Long dcId;

    private String msgCode;

    private String waybillCode;

    private String msg;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Date getGmtCreate() {
        return gmtCreate;
    }

    public void setGmtCreate(Date gmtCreate) {
        this.gmtCreate = gmtCreate;
    }

    public Date getGmtModify() {
        return gmtModify;
    }

    public void setGmtModify(Date gmtModify) {
        this.gmtModify = gmtModify;
    }

    public Long getEntryId() {
        return entryId;
    }

    public void setEntryId(Long entryId) {
        this.entryId = entryId;
    }

    public Long getDcId() {
        return dcId;
    }

    public void setDcId(Long dcId) {
        this.dcId = dcId;
    }

    public String getMsgCode() {
        return msgCode;
    }

    public void setMsgCode(String msgCode) {
        this.msgCode = msgCode;
    }

    public String getWaybillCode() {
        return waybillCode;
    }

    public void setWaybillCode(String waybillCode) {
        this.waybillCode = waybillCode;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    @Override
    public String toString() {
        return "WmsInboundLogDTO{" +
                "id=" + id +
                ", gmtCreate=" + gmtCreate +
                ", gmtModify=" + gmtModify +
                ", entryId=" + entryId +
                ", dcId=" + dcId +
                ", msgCode='" + msgCode + '\'' +
                ", waybillCode='" + waybillCode + '\'' +
                ", msg='" + msg + '\'' +
                '}';
    }
}
