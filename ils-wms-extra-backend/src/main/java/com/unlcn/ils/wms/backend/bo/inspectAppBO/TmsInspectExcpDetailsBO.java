package com.unlcn.ils.wms.backend.bo.inspectAppBO;

/**
 * @Author ：Ligl
 * @Date : 2017/9/14.
 */
public class TmsInspectExcpDetailsBO {
    private int code;
    private int partId;
    private int positionId;
    private String name;
    private String attachs;

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public int getPartId() {
        return partId;
    }

    public void setPartId(int partId) {
        this.partId = partId;
    }

    public int getPositionId() {
        return positionId;
    }

    public void setPositionId(int positionId) {
        this.positionId = positionId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAttachs() {
        return attachs;
    }

    public void setAttachs(String attachs) {
        this.attachs = attachs;
    }

    @Override
    public String toString() {
        return "TmsInspectExcpDetailsBO{" +
                "code=" + code +
                ", partId=" + partId +
                ", positionId=" + positionId +
                ", name='" + name + '\'' +
                ", attachs='" + attachs + '\'' +
                '}';
    }
}
