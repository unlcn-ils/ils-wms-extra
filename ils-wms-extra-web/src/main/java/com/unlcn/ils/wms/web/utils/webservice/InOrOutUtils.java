package com.unlcn.ils.wms.web.utils.webservice;

import org.dom4j.Document;
import org.dom4j.DocumentException;
import org.dom4j.Element;
import org.dom4j.io.SAXReader;

import java.io.ByteArrayInputStream;

public class InOrOutUtils {
    public static String xmlParse(String xmlResult) {
        SAXReader reader = new SAXReader();
        //if (StringUtils.isBlank(xmlResult)) {
        //    throw new BusinessException("未连接到dcs连接服务!");
        //}
        ByteArrayInputStream inputStream = new ByteArrayInputStream(xmlResult.getBytes());
        Document document = null;
        try {
            document = reader.read(inputStream);
        } catch (DocumentException e) {
            e.printStackTrace();
        }
        Element rootElement = document.getRootElement();
        Element body = rootElement.element("Body");
        Element excuteServiceResponse = body.element("excuteServiceResponse");
        Element excuteServiceReturn = excuteServiceResponse.element("excuteServiceReturn");
        Element returnCode = excuteServiceReturn.element("returnCode");
        String text = returnCode.getText();
        return text;
    }

}
