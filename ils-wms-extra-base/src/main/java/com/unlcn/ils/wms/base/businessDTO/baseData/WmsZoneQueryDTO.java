package com.unlcn.ils.wms.base.businessDTO.baseData;

import cn.huiyunche.commons.domain.PageVo;

import java.util.Date;

/**
 * Created by DELL on 2017/8/18.
 */
public class WmsZoneQueryDTO extends PageVo {

    private Long zeId;

    private String zeWhCode;

    private String zeWhName;

    private String zeZoneCode;

    private String zeZoneName;

    private String zeRemark;

    private String createUserId;

    private String createUserName;

    private String modifyUserId;

    private String modifyUserName;

    private Date gmtCreate;

    private Date gmtUpdate;

    private Byte isDeleted;

    private Long versions;

    private int limitStart;

    private int limitEnd;

    public Long getZeId() {
        return zeId;
    }

    public void setZeId(Long zeId) {
        this.zeId = zeId;
    }

    public String getZeWhCode() {
        return zeWhCode;
    }

    public void setZeWhCode(String zeWhCode) {
        this.zeWhCode = zeWhCode;
    }

    public String getZeWhName() {
        return zeWhName;
    }

    public void setZeWhName(String zeWhName) {
        this.zeWhName = zeWhName;
    }

    public String getZeZoneCode() {
        return zeZoneCode;
    }

    public void setZeZoneCode(String zeZoneCode) {
        this.zeZoneCode = zeZoneCode;
    }

    public String getZeZoneName() {
        return zeZoneName;
    }

    public void setZeZoneName(String zeZoneName) {
        this.zeZoneName = zeZoneName;
    }

    public String getZeRemark() {
        return zeRemark;
    }

    public void setZeRemark(String zeRemark) {
        this.zeRemark = zeRemark;
    }

    public String getCreateUserId() {
        return createUserId;
    }

    public void setCreateUserId(String createUserId) {
        this.createUserId = createUserId;
    }

    public String getCreateUserName() {
        return createUserName;
    }

    public void setCreateUserName(String createUserName) {
        this.createUserName = createUserName;
    }

    public String getModifyUserId() {
        return modifyUserId;
    }

    public void setModifyUserId(String modifyUserId) {
        this.modifyUserId = modifyUserId;
    }

    public String getModifyUserName() {
        return modifyUserName;
    }

    public void setModifyUserName(String modifyUserName) {
        this.modifyUserName = modifyUserName;
    }

    public Date getGmtCreate() {
        return gmtCreate;
    }

    public void setGmtCreate(Date gmtCreate) {
        this.gmtCreate = gmtCreate;
    }

    public Date getGmtUpdate() {
        return gmtUpdate;
    }

    public void setGmtUpdate(Date gmtUpdate) {
        this.gmtUpdate = gmtUpdate;
    }

    public Byte getIsDeleted() {
        return isDeleted;
    }

    public void setIsDeleted(Byte isDeleted) {
        this.isDeleted = isDeleted;
    }

    public Long getVersions() {
        return versions;
    }

    public void setVersions(Long versions) {
        this.versions = versions;
    }

    public int getLimitStart() {
        return limitStart;
    }

    public void setLimitStart(int limitStart) {
        this.limitStart = limitStart;
    }

    public int getLimitEnd() {
        return limitEnd;
    }

    public void setLimitEnd(int limitEnd) {
        this.limitEnd = limitEnd;
    }

    @Override
    public String toString() {
        return "WmsZoneQueryDTO{" +
                "zeId=" + zeId +
                ", zeWhCode='" + zeWhCode + '\'' +
                ", zeWhName='" + zeWhName + '\'' +
                ", zeZoneCode='" + zeZoneCode + '\'' +
                ", zeZoneName='" + zeZoneName + '\'' +
                ", zeRemark='" + zeRemark + '\'' +
                ", createUserId='" + createUserId + '\'' +
                ", createUserName='" + createUserName + '\'' +
                ", modifyUserId='" + modifyUserId + '\'' +
                ", modifyUserName='" + modifyUserName + '\'' +
                ", gmtCreate=" + gmtCreate +
                ", gmtUpdate=" + gmtUpdate +
                ", isDeleted=" + isDeleted +
                ", versions=" + versions +
                ", limitStart=" + limitStart +
                ", limitEnd=" + limitEnd +
                '}';
    }
}
