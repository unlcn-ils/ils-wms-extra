package com.unlcn.ils.wms.base.model.bigscreen;

import java.io.Serializable;
import java.util.Date;

public class BiFrontShipment implements Serializable {
    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column bi_front_shipment.id
     *
     * @mbggenerated Wed Nov 08 19:47:33 CST 2017
     */
    private Long id;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column bi_front_shipment.in_bound_count
     *
     * @mbggenerated Wed Nov 08 19:47:33 CST 2017
     */
    private Integer inBoundCount;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column bi_front_shipment.shipment_count
     *
     * @mbggenerated Wed Nov 08 19:47:33 CST 2017
     */
    private Integer shipmentCount;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column bi_front_shipment.inventory_count
     *
     * @mbggenerated Wed Nov 08 19:47:33 CST 2017
     */
    private Integer inventoryCount;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column bi_front_shipment.shipment_rate
     *
     * @mbggenerated Wed Nov 08 19:47:33 CST 2017
     */
    private Float shipmentRate;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column bi_front_shipment.statistics_time
     *
     * @mbggenerated Wed Nov 08 19:47:33 CST 2017
     */
    private Date statisticsTime;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column bi_front_shipment.version
     *
     * @mbggenerated Wed Nov 08 19:47:33 CST 2017
     */
    private String version;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column bi_front_shipment.create_time
     *
     * @mbggenerated Wed Nov 08 19:47:33 CST 2017
     */
    private Date createTime;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database table bi_front_shipment
     *
     * @mbggenerated Wed Nov 08 19:47:33 CST 2017
     */
    private static final long serialVersionUID = 1L;

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column bi_front_shipment.id
     *
     * @return the value of bi_front_shipment.id
     *
     * @mbggenerated Wed Nov 08 19:47:33 CST 2017
     */
    public Long getId() {
        return id;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column bi_front_shipment.id
     *
     * @param id the value for bi_front_shipment.id
     *
     * @mbggenerated Wed Nov 08 19:47:33 CST 2017
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column bi_front_shipment.in_bound_count
     *
     * @return the value of bi_front_shipment.in_bound_count
     *
     * @mbggenerated Wed Nov 08 19:47:33 CST 2017
     */
    public Integer getInBoundCount() {
        return inBoundCount;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column bi_front_shipment.in_bound_count
     *
     * @param inBoundCount the value for bi_front_shipment.in_bound_count
     *
     * @mbggenerated Wed Nov 08 19:47:33 CST 2017
     */
    public void setInBoundCount(Integer inBoundCount) {
        this.inBoundCount = inBoundCount;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column bi_front_shipment.shipment_count
     *
     * @return the value of bi_front_shipment.shipment_count
     *
     * @mbggenerated Wed Nov 08 19:47:33 CST 2017
     */
    public Integer getShipmentCount() {
        return shipmentCount;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column bi_front_shipment.shipment_count
     *
     * @param shipmentCount the value for bi_front_shipment.shipment_count
     *
     * @mbggenerated Wed Nov 08 19:47:33 CST 2017
     */
    public void setShipmentCount(Integer shipmentCount) {
        this.shipmentCount = shipmentCount;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column bi_front_shipment.inventory_count
     *
     * @return the value of bi_front_shipment.inventory_count
     *
     * @mbggenerated Wed Nov 08 19:47:33 CST 2017
     */
    public Integer getInventoryCount() {
        return inventoryCount;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column bi_front_shipment.inventory_count
     *
     * @param inventoryCount the value for bi_front_shipment.inventory_count
     *
     * @mbggenerated Wed Nov 08 19:47:33 CST 2017
     */
    public void setInventoryCount(Integer inventoryCount) {
        this.inventoryCount = inventoryCount;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column bi_front_shipment.shipment_rate
     *
     * @return the value of bi_front_shipment.shipment_rate
     *
     * @mbggenerated Wed Nov 08 19:47:33 CST 2017
     */
    public Float getShipmentRate() {
        return shipmentRate;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column bi_front_shipment.shipment_rate
     *
     * @param shipmentRate the value for bi_front_shipment.shipment_rate
     *
     * @mbggenerated Wed Nov 08 19:47:33 CST 2017
     */
    public void setShipmentRate(Float shipmentRate) {
        this.shipmentRate = shipmentRate;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column bi_front_shipment.statistics_time
     *
     * @return the value of bi_front_shipment.statistics_time
     *
     * @mbggenerated Wed Nov 08 19:47:33 CST 2017
     */
    public Date getStatisticsTime() {
        return statisticsTime;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column bi_front_shipment.statistics_time
     *
     * @param statisticsTime the value for bi_front_shipment.statistics_time
     *
     * @mbggenerated Wed Nov 08 19:47:33 CST 2017
     */
    public void setStatisticsTime(Date statisticsTime) {
        this.statisticsTime = statisticsTime;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column bi_front_shipment.version
     *
     * @return the value of bi_front_shipment.version
     *
     * @mbggenerated Wed Nov 08 19:47:33 CST 2017
     */
    public String getVersion() {
        return version;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column bi_front_shipment.version
     *
     * @param version the value for bi_front_shipment.version
     *
     * @mbggenerated Wed Nov 08 19:47:33 CST 2017
     */
    public void setVersion(String version) {
        this.version = version == null ? null : version.trim();
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column bi_front_shipment.create_time
     *
     * @return the value of bi_front_shipment.create_time
     *
     * @mbggenerated Wed Nov 08 19:47:33 CST 2017
     */
    public Date getCreateTime() {
        return createTime;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column bi_front_shipment.create_time
     *
     * @param createTime the value for bi_front_shipment.create_time
     *
     * @mbggenerated Wed Nov 08 19:47:33 CST 2017
     */
    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table bi_front_shipment
     *
     * @mbggenerated Wed Nov 08 19:47:33 CST 2017
     */
    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", id=").append(id);
        sb.append(", inBoundCount=").append(inBoundCount);
        sb.append(", shipmentCount=").append(shipmentCount);
        sb.append(", inventoryCount=").append(inventoryCount);
        sb.append(", shipmentRate=").append(shipmentRate);
        sb.append(", statisticsTime=").append(statisticsTime);
        sb.append(", version=").append(version);
        sb.append(", createTime=").append(createTime);
        sb.append("]");
        return sb.toString();
    }
}