package com.unlcn.ils.wms.app.vo.tms;

import java.io.Serializable;

public class WmsInboundInspectVO implements Serializable {
    private String vin;
    private String whCode;
    private String whName;

    public String getVin() {
        return vin;
    }

    public void setVin(String vin) {
        this.vin = vin;
    }

    public String getWhCode() {
        return whCode;
    }

    public void setWhCode(String whCode) {
        this.whCode = whCode;
    }

    public String getWhName() {
        return whName;
    }

    public void setWhName(String whName) {
        this.whName = whName;
    }
}
