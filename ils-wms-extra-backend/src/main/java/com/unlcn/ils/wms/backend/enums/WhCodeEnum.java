package com.unlcn.ils.wms.backend.enums;

import java.util.Objects;

public enum WhCodeEnum {

    JMC_NC_QS("JMC_NC_QS", "全顺库"),
    UNLCN_XN_CQ("UNLCN_XN_CQ", "重庆前置库"),
    XY01("XY01", "MES系统仓库code"),
    JM_CS("JM_CS", "君马库长沙库"),
    JM_XY("JM_XY", "君马库襄阳库");

    private final String value;
    private final String text;

    WhCodeEnum(String value, String text) {
        this.value = value;
        this.text = text;
    }

    /**
     * @return the value
     */
    public String getValue() {
        return value;
    }

    /**
     * @return the text
     */
    public String getText() {
        return text;
    }

    /**
     * @param @param  value
     * @param @return
     * @return StatusEnum    返回类型
     * @throws
     * @Title: getByValue
     * @Description: 获取值
     */
    public static WhCodeEnum getByValue(String value) {
        for (WhCodeEnum temp : WhCodeEnum.values()) {
            if (Objects.equals(temp.getValue(), value)) {
                return temp;
            }
        }
        return null;
    }

}
