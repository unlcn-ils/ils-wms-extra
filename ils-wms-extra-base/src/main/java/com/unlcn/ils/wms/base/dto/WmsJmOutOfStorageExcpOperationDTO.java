package com.unlcn.ils.wms.base.dto;

import cn.huiyunche.commons.domain.PageVo;

import java.io.Serializable;

public class WmsJmOutOfStorageExcpOperationDTO extends PageVo implements Serializable {
    private String whCode;
    private String userId;
    private String dataIds;
    private String excpId;
    private String sendDcsStatus;
    private String sendSapStatus;
    private String vin;//车架号
    private String zaction;//出入库类型
    private String startTime;//起始时间 格式(yyyy-MM-dd)
    private String endTime; //截止时间


    public String getWhCode() {
        return whCode;
    }

    public void setWhCode(String whCode) {
        this.whCode = whCode;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getDataIds() {
        return dataIds;
    }

    public void setDataIds(String dataIds) {
        this.dataIds = dataIds;
    }

    public String getExcpId() {
        return excpId;
    }

    public void setExcpId(String excpId) {
        this.excpId = excpId;
    }

    public String getVin() {
        return vin;
    }

    public void setVin(String vin) {
        this.vin = vin;
    }

    public String getSendDcsStatus() {
        return sendDcsStatus;
    }

    public void setSendDcsStatus(String sendDcsStatus) {
        this.sendDcsStatus = sendDcsStatus;
    }

    public String getSendSapStatus() {
        return sendSapStatus;
    }

    public void setSendSapStatus(String sendSapStatus) {
        this.sendSapStatus = sendSapStatus;
    }

    public String getZaction() {
        return zaction;
    }

    public void setZaction(String zaction) {
        this.zaction = zaction;
    }

    public String getStartTime() {
        return startTime;
    }

    public void setStartTime(String startTime) {
        this.startTime = startTime;
    }

    public String getEndTime() {
        return endTime;
    }

    public void setEndTime(String endTime) {
        this.endTime = endTime;
    }
}
