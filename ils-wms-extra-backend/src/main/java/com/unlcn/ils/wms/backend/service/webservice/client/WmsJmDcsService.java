package com.unlcn.ils.wms.backend.service.webservice.client;

import com.unlcn.ils.wms.base.model.junmadcs.WmsHandoverOrder;
import com.unlcn.ils.wms.base.model.junmadcs.WmsOutOfStorage;
import com.unlcn.ils.wms.base.model.junmadcs.WmsShipmentPlanCancel;

import java.util.List;

public interface WmsJmDcsService {

    void updateSendInOrOutboundRequest(List<WmsOutOfStorage> wmsOutOfStorageList) throws Exception;

    //String updateSendOutboundRequest(List<WmsOutOfStorage> wmsOutOfStorageList) throws Exception;

    void saveDlvBillInfoImport(List<WmsHandoverOrder> handoverOrderList) throws  Exception;

    void updateBillCancelAction(WmsShipmentPlanCancel wmsShipmentPlanCancel) throws  Exception;
}
