package com.unlcn.ils.wms.backend.bo.inboundBO;

import com.unlcn.ils.wms.backend.bo.baseDataBO.WmsCommonBO;

import java.util.Date;

/**
 * Created by DELL on 2017/8/8.
 */
public class WmsInventoryLocationBO extends WmsCommonBO{

    private Long invlocId;

    private Long invlocInvId;

    private Long invlocWhId;

    private String invlocWhCode;

    private String invlocWhName;

    private String invlocWhType;

    private Long invlocZoneId;

    private String invlocZoneCode;

    private String invlocZoneName;

    private Long invlocLocId;

    private String invlocLocCode;

    private String invlocLocName;

    private String invlocOdId;

    private String invlocCustomerCode;

    private String invlocCustomerName;

    private String invlocWaybillNo;

    private String invlocLot;

    private String invlocVin;

    public Long getInvlocId() {
        return invlocId;
    }

    public void setInvlocId(Long invlocId) {
        this.invlocId = invlocId;
    }

    public Long getInvlocInvId() {
        return invlocInvId;
    }

    public void setInvlocInvId(Long invlocInvId) {
        this.invlocInvId = invlocInvId;
    }

    public Long getInvlocWhId() {
        return invlocWhId;
    }

    public void setInvlocWhId(Long invlocWhId) {
        this.invlocWhId = invlocWhId;
    }

    public String getInvlocWhCode() {
        return invlocWhCode;
    }

    public void setInvlocWhCode(String invlocWhCode) {
        this.invlocWhCode = invlocWhCode;
    }

    public String getInvlocWhName() {
        return invlocWhName;
    }

    public void setInvlocWhName(String invlocWhName) {
        this.invlocWhName = invlocWhName;
    }

    public String getInvlocWhType() {
        return invlocWhType;
    }

    public void setInvlocWhType(String invlocWhType) {
        this.invlocWhType = invlocWhType;
    }

    public Long getInvlocZoneId() {
        return invlocZoneId;
    }

    public void setInvlocZoneId(Long invlocZoneId) {
        this.invlocZoneId = invlocZoneId;
    }

    public String getInvlocZoneCode() {
        return invlocZoneCode;
    }

    public void setInvlocZoneCode(String invlocZoneCode) {
        this.invlocZoneCode = invlocZoneCode;
    }

    public String getInvlocZoneName() {
        return invlocZoneName;
    }

    public void setInvlocZoneName(String invlocZoneName) {
        this.invlocZoneName = invlocZoneName;
    }

    public Long getInvlocLocId() {
        return invlocLocId;
    }

    public void setInvlocLocId(Long invlocLocId) {
        this.invlocLocId = invlocLocId;
    }

    public String getInvlocLocCode() {
        return invlocLocCode;
    }

    public void setInvlocLocCode(String invlocLocCode) {
        this.invlocLocCode = invlocLocCode;
    }

    public String getInvlocLocName() {
        return invlocLocName;
    }

    public void setInvlocLocName(String invlocLocName) {
        this.invlocLocName = invlocLocName;
    }

    public String getInvlocOdId() {
        return invlocOdId;
    }

    public void setInvlocOdId(String invlocOdId) {
        this.invlocOdId = invlocOdId;
    }

    public String getInvlocCustomerCode() {
        return invlocCustomerCode;
    }

    public void setInvlocCustomerCode(String invlocCustomerCode) {
        this.invlocCustomerCode = invlocCustomerCode;
    }

    public String getInvlocCustomerName() {
        return invlocCustomerName;
    }

    public void setInvlocCustomerName(String invlocCustomerName) {
        this.invlocCustomerName = invlocCustomerName;
    }

    public String getInvlocWaybillNo() {
        return invlocWaybillNo;
    }

    public void setInvlocWaybillNo(String invlocWaybillNo) {
        this.invlocWaybillNo = invlocWaybillNo;
    }

    public String getInvlocLot() {
        return invlocLot;
    }

    public void setInvlocLot(String invlocLot) {
        this.invlocLot = invlocLot;
    }

    public String getInvlocVin() {
        return invlocVin;
    }

    public void setInvlocVin(String invlocVin) {
        this.invlocVin = invlocVin;
    }

    @Override
    public String toString() {
        return "WmsInventoryLocationBO{" +
                "invlocId=" + invlocId +
                ", invlocInvId=" + invlocInvId +
                ", invlocWhId=" + invlocWhId +
                ", invlocWhCode='" + invlocWhCode + '\'' +
                ", invlocWhName='" + invlocWhName + '\'' +
                ", invlocWhType='" + invlocWhType + '\'' +
                ", invlocZoneId=" + invlocZoneId +
                ", invlocZoneCode='" + invlocZoneCode + '\'' +
                ", invlocZoneName='" + invlocZoneName + '\'' +
                ", invlocLocId=" + invlocLocId +
                ", invlocLocCode='" + invlocLocCode + '\'' +
                ", invlocLocName='" + invlocLocName + '\'' +
                ", invlocOdId='" + invlocOdId + '\'' +
                ", invlocCustomerCode='" + invlocCustomerCode + '\'' +
                ", invlocCustomerName='" + invlocCustomerName + '\'' +
                ", invlocWaybillNo='" + invlocWaybillNo + '\'' +
                ", invlocLot='" + invlocLot + '\'' +
                ", invlocVin='" + invlocVin + '\'' +
                '}';
    }
}
