package com.unlcn.ils.wms.base.mapper.inspectApp;

import com.unlcn.ils.wms.base.model.inspectApp.TmsInspectLog;
import com.unlcn.ils.wms.base.model.inspectApp.TmsInspectLogExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface TmsInspectLogMapper {
    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table tms_inspect_log
     *
     * @mbggenerated Wed Sep 27 10:44:04 CST 2017
     */
    int countByExample(TmsInspectLogExample example);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table tms_inspect_log
     *
     * @mbggenerated Wed Sep 27 10:44:04 CST 2017
     */
    int deleteByExample(TmsInspectLogExample example);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table tms_inspect_log
     *
     * @mbggenerated Wed Sep 27 10:44:04 CST 2017
     */
    int deleteByPrimaryKey(Long id);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table tms_inspect_log
     *
     * @mbggenerated Wed Sep 27 10:44:04 CST 2017
     */
    int insert(TmsInspectLog record);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table tms_inspect_log
     *
     * @mbggenerated Wed Sep 27 10:44:04 CST 2017
     */
    int insertSelective(TmsInspectLog record);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table tms_inspect_log
     *
     * @mbggenerated Wed Sep 27 10:44:04 CST 2017
     */
    List<TmsInspectLog> selectByExample(TmsInspectLogExample example);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table tms_inspect_log
     *
     * @mbggenerated Wed Sep 27 10:44:04 CST 2017
     */
    TmsInspectLog selectByPrimaryKey(Long id);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table tms_inspect_log
     *
     * @mbggenerated Wed Sep 27 10:44:04 CST 2017
     */
    int updateByExampleSelective(@Param("record") TmsInspectLog record, @Param("example") TmsInspectLogExample example);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table tms_inspect_log
     *
     * @mbggenerated Wed Sep 27 10:44:04 CST 2017
     */
    int updateByExample(@Param("record") TmsInspectLog record, @Param("example") TmsInspectLogExample example);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table tms_inspect_log
     *
     * @mbggenerated Wed Sep 27 10:44:04 CST 2017
     */
    int updateByPrimaryKeySelective(TmsInspectLog record);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table tms_inspect_log
     *
     * @mbggenerated Wed Sep 27 10:44:04 CST 2017
     */
    int updateByPrimaryKey(TmsInspectLog record);
}