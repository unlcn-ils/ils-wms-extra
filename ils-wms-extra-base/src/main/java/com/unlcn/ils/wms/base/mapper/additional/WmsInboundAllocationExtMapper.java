package com.unlcn.ils.wms.base.mapper.additional;

import com.unlcn.ils.wms.base.businessDTO.baseData.WmsEmptyLocationDTO;
import com.unlcn.ils.wms.base.businessDTO.stock.WmsInventoryDTO;
import com.unlcn.ils.wms.base.model.inbound.WmsInboundAllocation;
import com.unlcn.ils.wms.base.model.stock.WmsLocation;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 库位
 * Created by DELL on 2017/8/14.
 */
public interface WmsInboundAllocationExtMapper {
    /**
     * 分页数据
     * @param paramMap
     * @return
     */
    List<WmsInboundAllocation> queryForList(Map<String, Object> paramMap);

    /**
     * 分页数据总数
     * @param paramMap
     * @return
     */
    Integer queryForCount(Map<String, Object> paramMap);

    List<WmsInboundAllocation> selectDataByParam(HashMap<String, Object> paramMap);

    void insertWmsAllocationByProcedure(WmsInboundAllocation wmsInboundAllocation);
}
