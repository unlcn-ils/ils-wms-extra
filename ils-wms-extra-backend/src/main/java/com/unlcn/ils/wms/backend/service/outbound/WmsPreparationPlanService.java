package com.unlcn.ils.wms.backend.service.outbound;

import cn.huiyunche.commons.domain.ResultDTOWithPagination;
import com.unlcn.ils.wms.backend.bo.outboundBO.WmsPreparationPlanBO;
import com.unlcn.ils.wms.backend.bo.outboundBO.WmsPreparationVehicleDetailBO;
import com.unlcn.ils.wms.backend.util.CommonException;
import com.unlcn.ils.wms.base.dto.WmsPreparationPlanDTO;
import com.unlcn.ils.wms.base.dto.WmsPreparationPlanResultDTO;
import com.unlcn.ils.wms.base.model.outbound.WmsPreparationPlan;
import com.unlcn.ils.wms.base.model.outbound.WmsPreparationVehicleDetail;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Created by DELL on 2017/9/26.
 */
public interface WmsPreparationPlanService {
    /**
     * 分页查询
     *
     * @param paramMap
     * @return
     */
    Map<String, Object> list(Map<String, Object> paramMap) throws CommonException;

    /**
     * 根据主表id查询详情
     *
     * @param ppId
     * @return
     */
    ArrayList<WmsPreparationVehicleDetailBO> getVehicleDetail(String ppId);

    /**
     * 创建备料任务
     *
     * @param estimateLaneId
     * @param wmsPreparationPlanBOList
     */
    void savePreparationTask(Long estimateLaneId, List<WmsPreparationPlanBO> wmsPreparationPlanBOList, String whCode) throws Exception;

    /**
     * 创建备料任务 - 西南新
     * linbao 2017-11-21 需要找寻老的任务进行判断
     *
     * @param estimateLaneId
     * @param wmsPreparationPlanBOList
     */
    void savePreparationTaskForCqNew(Long estimateLaneId, List<WmsPreparationPlanBO> wmsPreparationPlanBOList, String whCode) throws Exception;

    /**
     * 根据交接单号获取备料详情
     *
     * @param handoverNo
     * @return
     * @throws Exception
     */
    List<WmsPreparationVehicleDetail> getDetailListByHandoverNo(String handoverNo) throws Exception;

    /**
     * 根据组版单号获取备料计划
     *
     * @param groupDispNo
     * @return
     * @throws Exception
     */
    WmsPreparationPlan getPreparationPlanByGroupDispNo(String groupDispNo) throws Exception;

    /**
     * 发运变更 - 增车
     *
     * @param ppId
     * @throws Exception
     */
    void updateShipToPreparePlan(Long ppId) throws Exception;

    /**
     * 发运变更 - 备料剔除
     *
     * @param vdId
     * @throws Exception
     */
    void deletePreparetionDetail(Long vdId) throws Exception;

    /**
     * 发运变更 - 任务
     *
     * @param ppId
     * @throws Exception
     */
    void updateShipmentToTask(Long ppId, String whCode) throws Exception;

    ResultDTOWithPagination<List<WmsPreparationPlanResultDTO>> getStockTransferList(WmsPreparationPlanDTO dto) throws Exception;

    void savePreparationPlanStockTransfer(WmsPreparationPlanDTO dto) throws Exception;

    List<WmsPreparationPlanResultDTO> getCustomerNameList(WmsPreparationPlanDTO dto) throws Exception;

    HSSFWorkbook getImportExcelData(WmsPreparationPlanDTO dto) throws Exception;
}
