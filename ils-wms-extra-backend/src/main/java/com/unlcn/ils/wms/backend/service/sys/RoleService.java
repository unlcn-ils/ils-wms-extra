package com.unlcn.ils.wms.backend.service.sys;

import java.util.List;

import com.unlcn.ils.wms.backend.bo.sys.RoleBO;

import cn.huiyunche.commons.domain.PageVo;
import com.unlcn.ils.wms.backend.dto.sysDTO.UserNewDTO;

/**
 * 
 * @ClassName: RoleService 
 * @Description: 角色 service
 * @author laishijian
 * @date 2017年8月8日 上午10:21:25 
 *
 */
public interface RoleService {
	
	/**
	 * 
	 * @Title: addRole 
	 * @Description: 新增角色
	 * @param @param roleBO
	 * @param @return     
	 * @return Integer    返回类型 
	 * @throws 
	 *
	 */
	Integer addRole(RoleBO roleBO) throws Exception;
	
	/**
	 * 
	 * @Title: findById 
	 * @Description: 根据id获取
	 * @param @param id
	 * @param @return
	 * @param @throws Exception     
	 * @return RoleBO    返回类型 
	 * @throws 
	 *
	 */
	RoleBO findById(Integer id) throws Exception;
	
	/**
	 * 
	 * @Title: updateRole 
	 * @Description: 更新角色
	 * @param @param roleBO
	 * @param @return
	 * @param @throws Exception     
	 * @return RoleBO    返回类型 
	 * @throws 
	 *
	 */
	void updateRole(RoleBO roleBO) throws Exception;
	
	/**
	 * 
	 * @Title: deleteByIds 
	 * @Description: 删除角色
	 * @param @param ids
	 * @param @throws Exception     
	 * @return void    返回类型 
	 * @throws 
	 *
	 */
	void deleteByIds(String ids) throws Exception;
	
	/**
	 * 
	 * @Title: list 
	 * @Description: 角色列表
	 * @param @param roleBO
	 * @param @param pageVO
	 * @param @return     
	 * @return List<RoleBO>    返回类型 
	 * @throws 
	 *
	 */
	List<RoleBO> list(RoleBO roleBO,PageVo pageVO) throws Exception;
	
	/**
	 * 
	 * @Title: findByUserId 
	 * @Description: 根据userId查询角色
	 * @param @param userId
	 * @param @return     
	 * @return List<RoleBO>    返回类型 
	 * @throws 
	 *
	 */
	List<RoleBO> findByUserId(Integer userId) throws Exception;

	/**
	 * 根据用户id和仓库id获取对应的权限
	 *
	 * @param userId
	 * @param warehouseId
	 * @return
	 * @throws Exception
	 */
	List<RoleBO> findByUserIdAndWarhouseId(Integer userId, Long warehouseId) throws Exception;

	/**
	 * 根据用户id和仓库id获取对应的权限
	 *
	 * @param userId
	 * @param warehouseId
	 * @return
	 * @throws Exception
	 */
	UserNewDTO findByUserIdAndWarhouseIdNew(Integer userId, Long warehouseId) throws Exception;
	
	/**
	 * 
	 * @Title: findAll 
	 * @Description: 获取所有角色
	 * @param @return
	 * @param @throws Exception     
	 * @return List<RoleBO>    返回类型 
	 * @throws 
	 *
	 */
	List<RoleBO> findAll() throws Exception;
	
	/**
	 * 
	 * @Title: findRoleAndChildByParentId 
	 * @Description: 根据父id获取角色及子角色
	 * @param @param parentId
	 * @param @return     
	 * @return List<RoleBO>    返回类型 
	 * @throws 
	 *
	 */
	List<RoleBO> findRoleAndChildByParentId(Integer parentId) throws Exception;
	
}
