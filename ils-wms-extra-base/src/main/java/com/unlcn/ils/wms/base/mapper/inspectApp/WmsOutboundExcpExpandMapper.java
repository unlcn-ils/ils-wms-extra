package com.unlcn.ils.wms.base.mapper.inspectApp;

import com.unlcn.ils.wms.base.dto.WmsOutboundInspectExcpDTO;
import org.apache.ibatis.annotations.Param;

import java.util.HashMap;
import java.util.List;

public interface WmsOutboundExcpExpandMapper {

    List<WmsOutboundInspectExcpDTO> selectExcpListByPositionId(HashMap<String, Object> params);

    List<WmsOutboundInspectExcpDTO> selectPositionsByInspectId(HashMap<String, Object> params);

    int selectExcpCountByPositionIdAndInspectId(HashMap<String,Object> params);

    int selectExcpCountByInspectId(@Param("inspectId") Long id, @Param("excpValue") int excpValue, @Param("del") int del_value);

    int selectExcpTotalCount(@Param("excpValue") int excpValue, @Param("damageValue") int damageValue, @Param("compromiseValue") int compromiseValue, @Param("hasNotPickup") int hasNotPickup, @Param("closedExcp") int closedExcp, @Param("del") int del_value);

    void updateExceptionToDeleteByInspectId(HashMap<String, Object> params);
}