package com.unlcn.ils.wms.backend.bo.outboundBO;

/**
 * Created by DELL on 2017/9/26.
 */
public class WmsPreparationPlanBO {

    /**
     * 计划ID
     */
    private Long ppId;

    /**
     * 备料单号
     */
    private String ppPreparationMaterialNo;

    /**
     * 计划装车道
     */
    private String ppEstimateLane;

    /**
     * 计划装车时间
     */
    private String ppEstimateLoadingTime;

    /**
     * 预计完成时间
     */
    private String ppEstimateFinishTime;

    /**
     * 备料操作员
     */
    private String ppOperationStaff;

    /**
     * 备料状态
     */
    private String ppStatus;

    /**
     * 出库状态
     */
    private String ppOutstockStatus;

    /**
     * 出库类型
     */
    private String ppOutstockType;

    /**
     * 组板单号/调度单号
     */
    private String ppGroupDispNo;

    /**
     * 订单号
     */
    private String ppOrderNo;

    /**
     * 车型代码
     */
    private String ppVehicleCode;

    /**
     * 车型名称
     */
    private String ppVehicleName;

    /**
     * 配置
     */
    private String ppConfigure;

    /**
     * 颜色
     */
    private String ppCarColour;

    /**
     * 物料代码
     */
    private String ppMaterialCode;

    /**
     * 组板数量
     */
    private Long ppGroupBoardQuantity;

    /**
     * 经销商代码
     */
    private String ppDealerCode;

    /**
     * 经销商名称
     */
    private String ppDealerName;

    /**
     * 收货省
     */
    private String ppProvince;

    /**
     * 收货市
     */
    private String ppCity;

    /**
     * 收货区县
     */
    private String ppDistrictCounty;

    /**
     * 收货详细地址
     */
    private String ppDetailedAddress;

    /**
     * 承运商
     */
    private String ppCarrier;

    /**
     * 板车车牌号
     */
    private String ppSupplierVehiclePlate;

    /**
     * 创建时间
     */
    private String gmtCreate;

    /**
     * 仓库code
     */
    private String ppWhCode;

    /**
     * 仓库name
     */
    private String ppWhName;

    private String ppGateInTime;

    private String ppGateOutTime;


    public String getPpGateInTime() {
        return ppGateInTime;
    }

    public void setPpGateInTime(String ppGateInTime) {
        this.ppGateInTime = ppGateInTime;
    }

    public String getPpGateOutTime() {
        return ppGateOutTime;
    }

    public void setPpGateOutTime(String ppGateOutTime) {
        this.ppGateOutTime = ppGateOutTime;
    }

    public Long getPpId() {
        return ppId;
    }

    public void setPpId(Long ppId) {
        this.ppId = ppId;
    }

    public String getPpPreparationMaterialNo() {
        return ppPreparationMaterialNo;
    }

    public void setPpPreparationMaterialNo(String ppPreparationMaterialNo) {
        this.ppPreparationMaterialNo = ppPreparationMaterialNo;
    }

    public String getPpEstimateLane() {
        return ppEstimateLane;
    }

    public void setPpEstimateLane(String ppEstimateLane) {
        this.ppEstimateLane = ppEstimateLane;
    }

    public String getPpEstimateLoadingTime() {
        return ppEstimateLoadingTime;
    }

    public void setPpEstimateLoadingTime(String ppEstimateLoadingTime) {
        this.ppEstimateLoadingTime = ppEstimateLoadingTime;
    }

    public String getPpEstimateFinishTime() {
        return ppEstimateFinishTime;
    }

    public void setPpEstimateFinishTime(String ppEstimateFinishTime) {
        this.ppEstimateFinishTime = ppEstimateFinishTime;
    }

    public String getPpOperationStaff() {
        return ppOperationStaff;
    }

    public void setPpOperationStaff(String ppOperationStaff) {
        this.ppOperationStaff = ppOperationStaff;
    }

    public String getPpStatus() {
        return ppStatus;
    }

    public void setPpStatus(String ppStatus) {
        this.ppStatus = ppStatus;
    }

    public String getPpOutstockStatus() {
        return ppOutstockStatus;
    }

    public void setPpOutstockStatus(String ppOutstockStatus) {
        this.ppOutstockStatus = ppOutstockStatus;
    }

    public String getPpOutstockType() {
        return ppOutstockType;
    }

    public void setPpOutstockType(String ppOutstockType) {
        this.ppOutstockType = ppOutstockType;
    }

    public String getPpGroupDispNo() {
        return ppGroupDispNo;
    }

    public void setPpGroupDispNo(String ppGroupDispNo) {
        this.ppGroupDispNo = ppGroupDispNo;
    }

    public String getPpOrderNo() {
        return ppOrderNo;
    }

    public void setPpOrderNo(String ppOrderNo) {
        this.ppOrderNo = ppOrderNo;
    }

    public String getPpVehicleCode() {
        return ppVehicleCode;
    }

    public void setPpVehicleCode(String ppVehicleCode) {
        this.ppVehicleCode = ppVehicleCode;
    }

    public String getPpVehicleName() {
        return ppVehicleName;
    }

    public void setPpVehicleName(String ppVehicleName) {
        this.ppVehicleName = ppVehicleName;
    }

    public String getPpConfigure() {
        return ppConfigure;
    }

    public void setPpConfigure(String ppConfigure) {
        this.ppConfigure = ppConfigure;
    }

    public String getPpCarColour() {
        return ppCarColour;
    }

    public void setPpCarColour(String ppCarColour) {
        this.ppCarColour = ppCarColour;
    }

    public String getPpMaterialCode() {
        return ppMaterialCode;
    }

    public void setPpMaterialCode(String ppMaterialCode) {
        this.ppMaterialCode = ppMaterialCode;
    }

    public Long getPpGroupBoardQuantity() {
        return ppGroupBoardQuantity;
    }

    public void setPpGroupBoardQuantity(Long ppGroupBoardQuantity) {
        this.ppGroupBoardQuantity = ppGroupBoardQuantity;
    }

    public String getPpDealerCode() {
        return ppDealerCode;
    }

    public void setPpDealerCode(String ppDealerCode) {
        this.ppDealerCode = ppDealerCode;
    }

    public String getPpDealerName() {
        return ppDealerName;
    }

    public void setPpDealerName(String ppDealerName) {
        this.ppDealerName = ppDealerName;
    }

    public String getPpProvince() {
        return ppProvince;
    }

    public void setPpProvince(String ppProvince) {
        this.ppProvince = ppProvince;
    }

    public String getPpCity() {
        return ppCity;
    }

    public void setPpCity(String ppCity) {
        this.ppCity = ppCity;
    }

    public String getPpDistrictCounty() {
        return ppDistrictCounty;
    }

    public void setPpDistrictCounty(String ppDistrictCounty) {
        this.ppDistrictCounty = ppDistrictCounty;
    }

    public String getPpDetailedAddress() {
        return ppDetailedAddress;
    }

    public void setPpDetailedAddress(String ppDetailedAddress) {
        this.ppDetailedAddress = ppDetailedAddress;
    }

    public String getPpCarrier() {
        return ppCarrier;
    }

    public void setPpCarrier(String ppCarrier) {
        this.ppCarrier = ppCarrier;
    }

    public String getPpSupplierVehiclePlate() {
        return ppSupplierVehiclePlate;
    }

    public void setPpSupplierVehiclePlate(String ppSupplierVehiclePlate) {
        this.ppSupplierVehiclePlate = ppSupplierVehiclePlate;
    }

    public String getGmtCreate() {
        return gmtCreate;
    }

    public void setGmtCreate(String gmtCreate) {
        this.gmtCreate = gmtCreate;
    }

    public String getPpWhCode() {
        return ppWhCode;
    }

    public void setPpWhCode(String ppWhCode) {
        this.ppWhCode = ppWhCode;
    }

    public String getPpWhName() {
        return ppWhName;
    }

    public void setPpWhName(String ppWhName) {
        this.ppWhName = ppWhName;
    }

    @Override
    public String toString() {
        return "WmsPreparationPlanBO{" +
                "ppId=" + ppId +
                ", ppPreparationMaterialNo='" + ppPreparationMaterialNo + '\'' +
                ", ppEstimateLane='" + ppEstimateLane + '\'' +
                ", ppEstimateLoadingTime='" + ppEstimateLoadingTime + '\'' +
                ", ppEstimateFinishTime='" + ppEstimateFinishTime + '\'' +
                ", ppOperationStaff='" + ppOperationStaff + '\'' +
                ", ppStatus='" + ppStatus + '\'' +
                ", ppOutstockStatus='" + ppOutstockStatus + '\'' +
                ", ppOutstockType='" + ppOutstockType + '\'' +
                ", ppGroupDispNo='" + ppGroupDispNo + '\'' +
                ", ppOrderNo='" + ppOrderNo + '\'' +
                ", ppVehicleCode='" + ppVehicleCode + '\'' +
                ", ppVehicleName='" + ppVehicleName + '\'' +
                ", ppConfigure='" + ppConfigure + '\'' +
                ", ppCarColour='" + ppCarColour + '\'' +
                ", ppMaterialCode='" + ppMaterialCode + '\'' +
                ", ppGroupBoardQuantity=" + ppGroupBoardQuantity +
                ", ppDealerCode='" + ppDealerCode + '\'' +
                ", ppDealerName='" + ppDealerName + '\'' +
                ", ppProvince='" + ppProvince + '\'' +
                ", ppCity='" + ppCity + '\'' +
                ", ppDistrictCounty='" + ppDistrictCounty + '\'' +
                ", ppDetailedAddress='" + ppDetailedAddress + '\'' +
                ", ppCarrier='" + ppCarrier + '\'' +
                ", ppSupplierVehiclePlate='" + ppSupplierVehiclePlate + '\'' +
                ", gmtCreate='" + gmtCreate + '\'' +
                ", ppWhCode='" + ppWhCode + '\'' +
                ", ppWhName='" + ppWhName + '\'' +
                '}';
    }
}
