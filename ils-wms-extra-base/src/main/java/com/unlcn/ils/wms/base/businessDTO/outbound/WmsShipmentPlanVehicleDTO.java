package com.unlcn.ils.wms.base.businessDTO.outbound;

import java.io.Serializable;
import java.util.Date;

public class WmsShipmentPlanVehicleDTO implements Serializable {

	private Long spId;

	private String spDispatchNo;

	private String spWaybillNo;

	private String spVin;

	private String spGroupBoardNo;

	private String spOrderNo;

	private String spVehicleCode;

	private String spVehicleName;

	private String spVehicleDesc;

	private String spSendBusinessFlag;

	private Date gmtCreate;

	private static final long serialVersionUID = 1L;

	public Long getSpId() {
		return spId;
	}

	public void setSpId(Long spId) {
		this.spId = spId;
	}

	public String getSpDispatchNo() {
		return spDispatchNo;
	}

	public void setSpDispatchNo(String spDispatchNo) {
		this.spDispatchNo = spDispatchNo;
	}

	public String getSpWaybillNo() {
		return spWaybillNo;
	}

	public void setSpWaybillNo(String spWaybillNo) {
		this.spWaybillNo = spWaybillNo;
	}

	public String getSpVin() {
		return spVin;
	}

	public void setSpVin(String spVin) {
		this.spVin = spVin;
	}

	public String getSpGroupBoardNo() {
		return spGroupBoardNo;
	}

	public void setSpGroupBoardNo(String spGroupBoardNo) {
		this.spGroupBoardNo = spGroupBoardNo;
	}

	public String getSpOrderNo() {
		return spOrderNo;
	}

	public void setSpOrderNo(String spOrderNo) {
		this.spOrderNo = spOrderNo;
	}

	public String getSpVehicleCode() {
		return spVehicleCode;
	}

	public void setSpVehicleCode(String spVehicleCode) {
		this.spVehicleCode = spVehicleCode;
	}

	public String getSpVehicleName() {
		return spVehicleName;
	}

	public void setSpVehicleName(String spVehicleName) {
		this.spVehicleName = spVehicleName;
	}

	public String getSpVehicleDesc() {
		return spVehicleDesc;
	}

	public void setSpVehicleDesc(String spVehicleDesc) {
		this.spVehicleDesc = spVehicleDesc;
	}

	public Date getGmtCreate() {
		return gmtCreate;
	}

	public void setGmtCreate(Date gmtCreate) {
		this.gmtCreate = gmtCreate;
	}

	public String getSpSendBusinessFlag() {
		return spSendBusinessFlag;
	}

	public void setSpSendBusinessFlag(String spSendBusinessFlag) {
		this.spSendBusinessFlag = spSendBusinessFlag;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("WmsShipmentPlanVehicleDTO [spId=");
		builder.append(spId);
		builder.append(", spDispatchNo=");
		builder.append(spDispatchNo);
		builder.append(", spWaybillNo=");
		builder.append(spWaybillNo);
		builder.append(", spVin=");
		builder.append(spVin);
		builder.append(", spGroupBoardNo=");
		builder.append(spGroupBoardNo);
		builder.append(", spOrderNo=");
		builder.append(spOrderNo);
		builder.append(", spVehicleCode=");
		builder.append(spVehicleCode);
		builder.append(", spVehicleName=");
		builder.append(spVehicleName);
		builder.append(", spVehicleDesc=");
		builder.append(spVehicleDesc);
		builder.append(", spSendBusinessFlag=");
		builder.append(spSendBusinessFlag);
		builder.append(", gmtCreate=");
		builder.append(gmtCreate);
		builder.append("]");
		return builder.toString();
	}

}