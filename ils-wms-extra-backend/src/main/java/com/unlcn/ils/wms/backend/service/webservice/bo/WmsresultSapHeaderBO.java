package com.unlcn.ils.wms.backend.service.webservice.bo;

import java.io.Serializable;

public class WmsresultSapHeaderBO implements Serializable {
    // pi头信息
    private String msgid;
    private String busid;
    private String tlgid;
    private String tlgname;
    private String dtsend;
    private String sender;
    private String receiver;
    private String freeuse;

    public String getMsgid() {
        return msgid;
    }

    public void setMsgid(String msgid) {
        this.msgid = msgid;
    }

    public String getBusid() {
        return busid;
    }

    public void setBusid(String busid) {
        this.busid = busid;
    }

    public String getTlgid() {
        return tlgid;
    }

    public void setTlgid(String tlgid) {
        this.tlgid = tlgid;
    }

    public String getTlgname() {
        return tlgname;
    }

    public void setTlgname(String tlgname) {
        this.tlgname = tlgname;
    }

    public String getDtsend() {
        return dtsend;
    }

    public void setDtsend(String dtsend) {
        this.dtsend = dtsend;
    }

    public String getSender() {
        return sender;
    }

    public void setSender(String sender) {
        this.sender = sender;
    }

    public String getReceiver() {
        return receiver;
    }

    public void setReceiver(String receiver) {
        this.receiver = receiver;
    }

    public String getFreeuse() {
        return freeuse;
    }

    public void setFreeuse(String freeuse) {
        this.freeuse = freeuse;
    }
}
