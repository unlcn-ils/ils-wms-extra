package com.unlcn.ils.wms.backend.bo.biBO;

import java.io.Serializable;

/**
 * Created by Nicky on 17/11/9.
 */
public class BiInOutDrayListBO implements Serializable {

    /**
     * 指令号
     */
    private String shipno;

    /**
     * 台数
     */
    private String qty;

    /**
     *承运车牌
     */
    private String vehicle;

    /**
     *分供方
     */
    private String supplier;

    /**
     *司机
     */
    private String driver;

    /**
     *联系电话
     */
    private String mobile;

    /**
     *指令起运地
     */
    private String route_start;

    /**
     *指令板车入场时间
     */
    private String come_date;

    /**
     *指令发运时间
     */
    private String out_date;

    /**
     *车辆当前位置
     */
    private String position;

    /**
     *经度
     */
    private String latitude;

    /**
     *纬度
     */
    private String longitude;

    public String getShipno() {
        return shipno;
    }

    public void setShipno(String shipno) {
        this.shipno = shipno;
    }

    public String getQty() {
        return qty;
    }

    public void setQty(String qty) {
        this.qty = qty;
    }

    public String getVehicle() {
        return vehicle;
    }

    public void setVehicle(String vehicle) {
        this.vehicle = vehicle;
    }

    public String getSupplier() {
        return supplier;
    }

    public void setSupplier(String supplier) {
        this.supplier = supplier;
    }

    public String getDriver() {
        return driver;
    }

    public void setDriver(String driver) {
        this.driver = driver;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getRoute_start() {
        return route_start;
    }

    public void setRoute_start(String route_start) {
        this.route_start = route_start;
    }

    public String getCome_date() {
        return come_date;
    }

    public void setCome_date(String come_date) {
        this.come_date = come_date;
    }

    public String getOut_date() {
        return out_date;
    }

    public void setOut_date(String out_date) {
        this.out_date = out_date;
    }

    public String getPosition() {
        return position;
    }

    public void setPosition(String position) {
        this.position = position;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    @Override
    public String toString() {
        return "BiInOutDrayListBO{" +
                "shipno='" + shipno + '\'' +
                ", qty='" + qty + '\'' +
                ", vehicle='" + vehicle + '\'' +
                ", supplier='" + supplier + '\'' +
                ", driver='" + driver + '\'' +
                ", mobile='" + mobile + '\'' +
                ", route_start='" + route_start + '\'' +
                ", come_date='" + come_date + '\'' +
                ", out_date='" + out_date + '\'' +
                ", position='" + position + '\'' +
                ", latitude='" + latitude + '\'' +
                ", longitude='" + longitude + '\'' +
                '}';
    }
}
