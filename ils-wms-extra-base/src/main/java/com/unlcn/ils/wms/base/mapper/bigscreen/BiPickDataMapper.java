package com.unlcn.ils.wms.base.mapper.bigscreen;

import com.unlcn.ils.wms.base.model.bigscreen.BiPickData;
import com.unlcn.ils.wms.base.model.bigscreen.BiPickDataExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface BiPickDataMapper {
    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table bi_pick_data
     *
     * @mbggenerated Wed Nov 08 23:21:59 CST 2017
     */
    int countByExample(BiPickDataExample example);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table bi_pick_data
     *
     * @mbggenerated Wed Nov 08 23:21:59 CST 2017
     */
    int deleteByExample(BiPickDataExample example);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table bi_pick_data
     *
     * @mbggenerated Wed Nov 08 23:21:59 CST 2017
     */
    int deleteByPrimaryKey(Long id);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table bi_pick_data
     *
     * @mbggenerated Wed Nov 08 23:21:59 CST 2017
     */
    int insert(BiPickData record);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table bi_pick_data
     *
     * @mbggenerated Wed Nov 08 23:21:59 CST 2017
     */
    int insertSelective(BiPickData record);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table bi_pick_data
     *
     * @mbggenerated Wed Nov 08 23:21:59 CST 2017
     */
    List<BiPickData> selectByExample(BiPickDataExample example);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table bi_pick_data
     *
     * @mbggenerated Wed Nov 08 23:21:59 CST 2017
     */
    BiPickData selectByPrimaryKey(Long id);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table bi_pick_data
     *
     * @mbggenerated Wed Nov 08 23:21:59 CST 2017
     */
    int updateByExampleSelective(@Param("record") BiPickData record, @Param("example") BiPickDataExample example);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table bi_pick_data
     *
     * @mbggenerated Wed Nov 08 23:21:59 CST 2017
     */
    int updateByExample(@Param("record") BiPickData record, @Param("example") BiPickDataExample example);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table bi_pick_data
     *
     * @mbggenerated Wed Nov 08 23:21:59 CST 2017
     */
    int updateByPrimaryKeySelective(BiPickData record);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table bi_pick_data
     *
     * @mbggenerated Wed Nov 08 23:21:59 CST 2017
     */
    int updateByPrimaryKey(BiPickData record);
}