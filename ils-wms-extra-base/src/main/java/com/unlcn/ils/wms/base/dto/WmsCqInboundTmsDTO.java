package com.unlcn.ils.wms.base.dto;

import cn.huiyunche.commons.domain.PageVo;

import java.io.Serializable;

public class WmsCqInboundTmsDTO extends PageVo implements Serializable {
    private String departureReason;
    private String departureType;
    private String departureInOutType;
    private String vehicle;
    private String status;
    private String endDepartureTime;
    private String startDeparturTime;
    private String drId;
    private String userId;
    private String whCode;

    public String getDepartureInOutType() {
        return departureInOutType;
    }

    public void setDepartureInOutType(String departureInOutType) {
        this.departureInOutType = departureInOutType;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getWhCode() {
        return whCode;
    }

    public void setWhCode(String whCode) {
        this.whCode = whCode;
    }

    public String getDrId() {
        return drId;
    }

    public void setDrId(String drId) {
        this.drId = drId;
    }

    public String getEndDepartureTime() {
        return endDepartureTime;
    }

    public void setEndDepartureTime(String endDepartureTime) {
        this.endDepartureTime = endDepartureTime;
    }

    public String getStartDeparturTime() {
        return startDeparturTime;
    }

    public void setStartDeparturTime(String startDeparturTime) {
        this.startDeparturTime = startDeparturTime;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getVehicle() {
        return vehicle;
    }

    public void setVehicle(String vehicle) {
        this.vehicle = vehicle;
    }

    public String getDepartureReason() {
        return departureReason;
    }

    public void setDepartureReason(String departureReason) {
        this.departureReason = departureReason;
    }

    public String getDepartureType() {
        return departureType;
    }

    public void setDepartureType(String departureType) {
        this.departureType = departureType;
    }
}
