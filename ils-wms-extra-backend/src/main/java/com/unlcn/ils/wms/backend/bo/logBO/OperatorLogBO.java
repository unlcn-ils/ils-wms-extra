package com.unlcn.ils.wms.backend.bo.logBO;

/**
 * Created by DELL on 2017/9/12.
 */
public class OperatorLogBO {

    /**
     * 主键ID
     */
    private Long opId;

    /**
     * 操作人用户id
     */
    private String opUserId;

    /**
     * 操作人用户名
     */
    private String opUserName;

    /**
     * 请求路径
     */
    private String opRequestUrl;

    /**
     * 操作时间
     */
    private String opTime;

    /**
     * 操作状态10：成功；20：失败
     */
    private String opStatus;

    /**
     * 操作模块
     */
    private String opModules;

    /**
     * 请求方法
     */
    private String opMethods;

    /**
     * 操作者机器ip
     */
    private String opIp;

    /**
     * 其他描述
     */
    private String opDescription;

    public Long getOpId() {
        return opId;
    }

    public void setOpId(Long opId) {
        this.opId = opId;
    }

    public String getOpUserId() {
        return opUserId;
    }

    public void setOpUserId(String opUserId) {
        this.opUserId = opUserId;
    }

    public String getOpUserName() {
        return opUserName;
    }

    public void setOpUserName(String opUserName) {
        this.opUserName = opUserName;
    }

    public String getOpRequestUrl() {
        return opRequestUrl;
    }

    public void setOpRequestUrl(String opRequestUrl) {
        this.opRequestUrl = opRequestUrl;
    }

    public String getOpTime() {
        return opTime;
    }

    public void setOpTime(String opTime) {
        this.opTime = opTime;
    }

    public String getOpStatus() {
        return opStatus;
    }

    public void setOpStatus(String opStatus) {
        this.opStatus = opStatus;
    }

    public String getOpModules() {
        return opModules;
    }

    public void setOpModules(String opModules) {
        this.opModules = opModules;
    }

    public String getOpMethods() {
        return opMethods;
    }

    public void setOpMethods(String opMethods) {
        this.opMethods = opMethods;
    }

    public String getOpIp() {
        return opIp;
    }

    public void setOpIp(String opIp) {
        this.opIp = opIp;
    }

    public String getOpDescription() {
        return opDescription;
    }

    public void setOpDescription(String opDescription) {
        this.opDescription = opDescription;
    }

    @Override
    public String toString() {
        return "OperatorLogBO{" +
                "opId=" + opId +
                ", opUserId='" + opUserId + '\'' +
                ", opUserName='" + opUserName + '\'' +
                ", opRequestUrl='" + opRequestUrl + '\'' +
                ", opTime='" + opTime + '\'' +
                ", opStatus='" + opStatus + '\'' +
                ", opModules='" + opModules + '\'' +
                ", opMethods='" + opMethods + '\'' +
                ", opIp='" + opIp + '\'' +
                ", opDescription='" + opDescription + '\'' +
                '}';
    }
}
