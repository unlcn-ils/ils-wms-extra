package com.unlcn.ils.wms.backend.bo.outboundBO;

import cn.huiyunche.commons.domain.PageVo;

/**
 * @Auther linbao
 * @Date 2017-10-27
 */
public class WmsShipmentPlanRejectBO extends PageVo {

    /**
     * 任务id
     */
    private Long taskId;

    /**
     * 备料计划id
     */
    private Long planId;


    /**
     * 备料详情id
     */
    private Long planDetailId;

    /**
     * 取消原因
     */
    private String cancleReason;

    /**
     * 组版单号
     */
    private String spGroupBoardNo;

    /**
     * 订单号
     */
    private String spOrderNo;

    /**
     * 发货仓库
     */
    private String spDeliverWarehouseCode;

    /**
     * 承运商
     */
    private String spCarrier;

    /**
     * 状态
     */
    private String spSendBusinessFlag;

    /**
     * 发运类型
     */
    private String spDlvType;

    /**
     * 是否有驳回
     */
    private String spIsReject;

    /**
     * 创建开始时间
     */
    private String startCreateTime;

    /**
     * 创建结束时间
     */
    private String endCreateTime;

    private String whCode;

    public String getWhCode() {
        return whCode;
    }

    public void setWhCode(String whCode) {
        this.whCode = whCode;
    }

    public String getSpGroupBoardNo() {
        return spGroupBoardNo;
    }

    public void setSpGroupBoardNo(String spGroupBoardNo) {
        this.spGroupBoardNo = spGroupBoardNo;
    }

    public String getSpOrderNo() {
        return spOrderNo;
    }

    public void setSpOrderNo(String spOrderNo) {
        this.spOrderNo = spOrderNo;
    }

    public String getSpDeliverWarehouseCode() {
        return spDeliverWarehouseCode;
    }

    public void setSpDeliverWarehouseCode(String spDeliverWarehouseCode) {
        this.spDeliverWarehouseCode = spDeliverWarehouseCode;
    }

    public String getSpCarrier() {
        return spCarrier;
    }

    public void setSpCarrier(String spCarrier) {
        this.spCarrier = spCarrier;
    }

    public String getSpSendBusinessFlag() {
        return spSendBusinessFlag;
    }

    public void setSpSendBusinessFlag(String spSendBusinessFlag) {
        this.spSendBusinessFlag = spSendBusinessFlag;
    }

    public String getSpDlvType() {
        return spDlvType;
    }

    public void setSpDlvType(String spDlvType) {
        this.spDlvType = spDlvType;
    }

    public String getSpIsReject() {
        return spIsReject;
    }

    public void setSpIsReject(String spIsReject) {
        this.spIsReject = spIsReject;
    }

    public String getStartCreateTime() {
        return startCreateTime;
    }

    public void setStartCreateTime(String startCreateTime) {
        this.startCreateTime = startCreateTime;
    }

    public String getEndCreateTime() {
        return endCreateTime;
    }

    public void setEndCreateTime(String endCreateTime) {
        this.endCreateTime = endCreateTime;
    }

    public Long getTaskId() {
        return taskId;
    }

    public void setTaskId(Long taskId) {
        this.taskId = taskId;
    }

    public String getCancleReason() {
        return cancleReason;
    }

    public void setCancleReason(String cancleReason) {
        this.cancleReason = cancleReason;
    }

    public Long getPlanId() {
        return planId;
    }

    public void setPlanId(Long planId) {
        this.planId = planId;
    }

    public Long getPlanDetailId() {
        return planDetailId;
    }

    public void setPlanDetailId(Long planDetailId) {
        this.planDetailId = planDetailId;
    }

    @Override
    public String toString() {
        return "WmsShipmentPlanRejectBO{" +
                "taskId=" + taskId +
                ", planId=" + planId +
                ", planDetailId=" + planDetailId +
                ", cancleReason='" + cancleReason + '\'' +
                ", spGroupBoardNo='" + spGroupBoardNo + '\'' +
                ", spOrderNo='" + spOrderNo + '\'' +
                ", spDeliverWarehouseCode='" + spDeliverWarehouseCode + '\'' +
                ", spCarrier='" + spCarrier + '\'' +
                ", spSendBusinessFlag='" + spSendBusinessFlag + '\'' +
                ", spDlvType='" + spDlvType + '\'' +
                ", spIsReject='" + spIsReject + '\'' +
                ", startCreateTime='" + startCreateTime + '\'' +
                ", endCreateTime='" + endCreateTime + '\'' +
                '}';
    }
}
