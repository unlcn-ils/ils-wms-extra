package com.unlcn.ils.wms.backend.bo.tms;

/**
 * @Author ：Ligl
 * @Date : 2017/9/15.
 */
public class TmsRepairOperBO {
    private Long inspectId;
    private Long repairTimeId;
    private String userId;
    private Integer inspectStatus;

    public Long getInspectId() {
        return inspectId;
    }

    public void setInspectId(Long inspectId) {
        this.inspectId = inspectId;
    }

    public Long getRepairTimeId() {
        return repairTimeId;
    }

    public void setRepairTimeId(Long repairTimeId) {
        this.repairTimeId = repairTimeId;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public Integer getInspectStatus() {
        return inspectStatus;
    }

    public void setInspectStatus(Integer inspectStatus) {
        this.inspectStatus = inspectStatus;
    }

    @Override
    public String toString() {
        return "TmsRepairOperBO{" +
                "inspectId=" + inspectId +
                ", repairTimeId=" + repairTimeId +
                ", userId='" + userId + '\'' +
                ", inspectStatus=" + inspectStatus +
                '}';
    }
}
