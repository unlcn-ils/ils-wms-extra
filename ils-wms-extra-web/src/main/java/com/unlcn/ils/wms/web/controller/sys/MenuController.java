package com.unlcn.ils.wms.web.controller.sys;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.unlcn.ils.wms.backend.bo.sys.MenuBO;
import com.unlcn.ils.wms.backend.service.sys.MenuService;
import com.unlcn.ils.wms.web.vo.sys.MenuVO;
import com.unlcn.ils.wms.web.vo.sys.PermissionsVO;

import cn.huiyunche.commons.domain.ResultDTO;
import cn.huiyunche.commons.exception.BusinessException;

/**
 * 
 * @ClassName: MenuController 
 * @Description: 菜单
 * @author laishijian
 * @date 2017年8月9日 上午9:42:12 
 *
 */
@Controller
@RequestMapping("/menu")
@ResponseBody
public class MenuController {
	
	private Logger LOGGER = LoggerFactory.getLogger(MenuController.class);
	
	@Autowired
	private MenuService menuService;
	
	/**
	 * 
	 * @Title: add 
	 * @Description: 菜单新增
	 * @param @param menuVO
	 * @param @return     
	 * @return ResultDTO<Object>    返回类型 
	 * @throws 
	 *
	 */
	@RequestMapping(value="/add",method=RequestMethod.POST)
	public ResultDTO<Object> add(@RequestBody MenuVO menuVO){
		
		LOGGER.info("MenuController.add param : {}", menuVO);
		
		ResultDTO<Object> result = new ResultDTO<>(true);
		try {
			
			MenuBO menuBO = new MenuBO();
			BeanUtils.copyProperties(menuVO, menuBO);
			menuService.add(menuBO);
			
			result.setMessage("新增菜单成功");
			
		}catch(BusinessException be) {
			result.setSuccess(false);
			result.setMessage(be.getMessage());
			LOGGER.error("MenuController.add error: {}", be);
		}catch(Exception e) {
			result.setSuccess(false);
			result.setMessage("系统异常");
			LOGGER.error("MenuController.add error: {}", e);
		}
		return result;
	}
	
	/**
	 * 
	 * @Title: getAll 
	 * @Description: 获取所有菜单
	 * @param @return     
	 * @return ResultDTO<List<MenuVO>>    返回类型 
	 * @throws 
	 *
	 */
	@RequestMapping(value="/getAll",method=RequestMethod.GET)
	public ResultDTO<List<MenuVO>> getAll(){
		
		LOGGER.info("MenuController.getAll ");
		
		ResultDTO<List<MenuVO>> result = new ResultDTO<>(true);
		try {
			
			List<MenuBO> menuBOList = menuService.findMenuAndChildMenu();
			
			List<MenuVO> menuVOList = converMenuBOToVO(menuBOList);
			result.setData(menuVOList);
			result.setMessage("获取所有菜单成功");
		}catch(BusinessException be) {
			result.setSuccess(false);
			result.setMessage(be.getMessage());
			LOGGER.error("MenuController.getAll error: {}", be);
		}catch(Exception e) {
			result.setSuccess(false);
			result.setMessage("系统异常");
			LOGGER.error("MenuController.getAll error: {}", e);
		}
		return result;
	}
	
	/**
	 * 
	 * @Title: getByParentId 
	 * @Description: 根据父id查询
	 * @param @param parentId
	 * @param @return     
	 * @return ResultDTO<List<MenuVO>>    返回类型 
	 * @throws 
	 *
	 */
	@RequestMapping(value="/getByParentId",method=RequestMethod.POST)
	public ResultDTO<List<MenuVO>> getByParentId(@RequestBody MenuVO menuVO){
		
		LOGGER.info("MenuController.getByLevelAndParentId param : {}",menuVO);
		
		ResultDTO<List<MenuVO>> result = new ResultDTO<>(true);
		try {
			
			List<MenuBO> menuBOList = menuService.findByParentId(menuVO.getParentId());
			
			List<MenuVO> menuVOList = converMenuBOToVO(menuBOList);
			result.setData(menuVOList);
			result.setMessage("获取菜单成功");
		}catch(BusinessException be) {
			result.setSuccess(false);
			result.setMessage(be.getMessage());
			LOGGER.error("MenuController.getByLevelAndParentId error: {}", be);
		}catch(Exception e) {
			result.setSuccess(false);
			result.setMessage("系统异常");
			LOGGER.error("MenuController.getByLevelAndParentId error: {}", e);
		}
		return result;
	}
	
	/**
	 * 
	 * @Title: converMenuBOToVO 
	 * @Description: 菜单BO - VO
	 * @param @return     
	 * @return List<MenuVO>    返回类型 
	 * @throws 
	 *
	 */
	private List<MenuVO> converMenuBOToVO(List<MenuBO> menuBOList){
		
		List<MenuVO> menuVOList = new ArrayList<>();
		for (MenuBO menuBO : menuBOList) {
			MenuVO menuVO = new MenuVO();
			BeanUtils.copyProperties(menuBO, menuVO);
			
			if(null != menuBO.getPermissionBO()) {
				PermissionsVO permissionsVO = new PermissionsVO();
				BeanUtils.copyProperties(menuBO.getPermissionBO(), permissionsVO);
				menuVO.setPermissionsVO(permissionsVO);
			}
			
			if(null != menuBO.getChildMenuBOList()) {
				menuVO.setChildMenuVOList(converMenuBOToVO(menuBO.getChildMenuBOList()));
			}
			
			menuVOList.add(menuVO);
		}
		return menuVOList;
	}
	
}
