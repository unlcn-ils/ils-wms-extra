package com.unlcn.ils.wms.backend.enums;

/**
 * 维修单维修结果-合格/不合格
 */
public enum WmsRepairResultEnum {
    REPAIR_PASS(10, "维修合格"),
    REPAIR_EXCP(20, "维修不合格");

    private final int code;
    private final String name;

    WmsRepairResultEnum(int code, String name) {
        this.code = code;
        this.name = name;
    }

    public int getCode() {
        return code;
    }

    public String getName() {
        return name;
    }

    public static WmsRepairResultEnum getByValue(int value) {
        for (WmsRepairResultEnum temp : WmsRepairResultEnum.values()) {
            if (temp.getCode() == value) {
                return temp;
            }
        }
        return null;
    }
}
