package com.unlcn.ils.wms.backend.service.webservice.client;

import com.alibaba.fastjson.JSONObject;
import com.google.gson.Gson;
import com.unlcn.ils.wms.backend.util.webservice.WebServiceSoapUtils;
import com.unlcn.ils.wms.backend.util.webservice.XmlParseUtils;
import org.apache.commons.lang3.StringEscapeUtils;
import org.apache.commons.lang3.StringUtils;

import java.util.Map;

public class ClientForTest {

    //测试sap客户端
    //出入库 http://58.144.142.90:50000/dir/wsdl?p=sa/ce4f72569b363ba6a7579cbfcfa772c4
    //退车

    public static void main(String[] args) throws Exception {

        StringBuilder soapXmlBuf = new StringBuilder();
        //testGIGR(soapXmlBuf);
        //testPGR(soapXmlBuf);
        //testPGI(soapXmlBuf);
        testCRM(soapXmlBuf);

        //出入库
        //String soapUrl = "http://58.144.142.90:50000/XISOAPAdapter/MessageServlet?senderParty=&senderService=BC_WMS&receiverParty=&receiverService=&interface=SI_DCS2SAP_GIGR_OUT&interfaceNamespace=http://zjunma.com/TRAUM/WMS/ECC";
        //经销商退车
        //String soapUrl = "http://58.144.142.90:50000/XISOAPAdapter/MessageServlet?senderParty=&senderService=BC_WMS&receiverParty=&receiverService=&interface=SI_WMS2SAP_PGR_OUT&interfaceNamespace=http://zjunma.com/TRAUM/WMS/ECC";
        //出入交接单 --同步
        //String soapUrl = "http://58.144.142.90:50000/XISOAPAdapter/MessageServlet?senderParty=&senderService=BC_DCS&receiverParty=&receiverService=&interface=SI_DCS2SAP_VEHICLE_PGI_OUT&interfaceNamespace=http://zjunma.com/TRAUM/DCS/ECC";
        //String soapHost = "58.144.142.90:50000";
        //String soapAction = "http://sap.com/xi/WebService/soap1.1";
        //crm 发运接口
        String soapUrl = "http://58.144.142.90:50000/XISOAPAdapter/MessageServlet?senderParty=&senderService=BC_WMS&receiverParty=&receiverService=&interface=SI_WMS2CRM_Vehicle_OUT&interfaceNamespace=http://zjunma.com/TRAUM/WMS/CRM";
        String soapHost = "58.144.142.90:50000";
        String soapAction = "http://sap.com/xi/WebService/soap1.1";

        String result = WebServiceSoapUtils.postSoapToPi(soapUrl, soapHost, soapXmlBuf.toString(), soapAction, "TRAUM-WMSO", "WMS253.2");
        //转义返回的字符串
        String xmlResult = StringEscapeUtils.unescapeHtml3(result);
        System.out.println("入库调用回执:" + xmlResult);
        if (StringUtils.isNotBlank(xmlResult)) {
            Map<String, Object> objectMap = XmlParseUtils.xml2map(xmlResult);
            //获取返回结果
            String json = new Gson().toJson(objectMap);
            JSONObject jsonObject = JSONObject.parseObject(json);
            String en_str = jsonObject.getString("Body");
            JSONObject jsonObject1 = JSONObject.parseObject(en_str);
            String body_str = jsonObject1.getString("Body");
            JSONObject jsonObject2 = JSONObject.parseObject(body_str);
            String er_str = jsonObject2.getString("MT_VEHICLE_PGI_RSP");
            JSONObject jsonObject3 = JSONObject.parseObject(er_str);
            String code_str = jsonObject3.getString("ZSTATUS");
            JSONObject jsonObject4 = JSONObject.parseObject(code_str);
            String returnCode_str = jsonObject4.getString("ZSTATUS");
            //if (StringUtils.isNotBlank(returnCode_str)) {
            //    if ("N".equals(returnCode_str)) {
            //        //错误
            //        String error_str = jsonObject3.getString("ZMSG");
            //        if (StringUtils.isNotBlank(error_str)) {
            //            JSONObject jsonObject5 = JSONObject.parseObject(error_str);
            //            String de_str = jsonObject5.getString("ZMSG");
            //            //失败
            //            WmsHandoverOrderMapper
            //
            //        }
            //    }
            //    if ("S".equals(returnCode_str)) {
            //        //成功
            //    }
            //
            //}

        }
    }

    private static void testCRM(StringBuilder soapXmlBuf) {
        soapXmlBuf.append("<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:tem=\"http://tempuri.org/\">\n" +
                "   <soapenv:Header/>\n" +
                "   <soapenv:Body>\n" +
                "      <tem:IF_Crm2SAP_Vehicle_PGI>\n" +
                "         <!--Optional:-->\n" +
                "         <tem:BODY>\n" +
                "            <tem:VBELN>测试pi002</tem:VBELN>\n" +
                "            <tem:POSNR>测试pi002</tem:POSNR>\n" +
                "            <tem:ZACTION>测试pi002</tem:ZACTION>\n" +
                "            <tem:LGORT>测试pi002</tem:LGORT>\n" +
                "            <tem:ZLGORT>测试pi002</tem:ZLGORT>\n" +
                "            <tem:MATNR>测试pi002</tem:MATNR>\n" +
                "            <tem:SERNR>测试pi002</tem:SERNR>\n" +
                "         </tem:BODY>\n" +
                "      </tem:IF_Crm2SAP_Vehicle_PGI>\n" +
                "   </soapenv:Body>\n" +
                "</soapenv:Envelope>");

    }

    private static void testPGI(StringBuilder soapXmlBuf) {
        soapXmlBuf.append("<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:ecc=\"http://zjunma.com/TRAUM/DCS/ECC\">\n" +
                "   <soapenv:Header/>\n" +
                "   <soapenv:Body>\n" +
                "      <ecc:MT_VEHICLE_PGI_REQ>\n" +
                "         <TP_HEADER>\n" +
                "            <MSGID>1</MSGID>\n" +
                "            <BUSID>1</BUSID>\n" +
                "            <TLGID>1</TLGID>\n" +
                "            <TLGNAME>1</TLGNAME>\n" +
                "            <DTSEND>1</DTSEND>\n" +
                "            <SENDER>1</SENDER>\n" +
                "            <RECEIVER>1</RECEIVER>\n" +
                "            <!--Optional:-->\n" +
                "            <FREEUSE>1</FREEUSE>\n" +
                "         </TP_HEADER>\n" +
                "         <!--1 or more repetitions:-->\n" +
                "         <VEHICLE_PGI_REQ>\n" +
                "            <DATA_ID>1</DATA_ID>\n" +
                "            <VBELN>1</VBELN>\n" +
                "            <POSNR>1</POSNR>\n" +
                "            <ZACTION>1</ZACTION>\n" +
                "            <LGORT>1</LGORT>\n" +
                "            <ZLGORT>1</ZLGORT>\n" +
                "            <MATNR>1</MATNR>\n" +
                "            <WERKS>1</WERKS>\n" +
                "            <VSTEL>1</VSTEL>\n" +
                "            <SERNR>1</SERNR>\n" +
                "            <ZSERNR>1</ZSERNR>\n" +
                "            <MBDAT>1</MBDAT>\n" +
                "            <!--Optional:-->\n" +
                "            <FREEUSE1>1</FREEUSE1>\n" +
                "            <!--Optional:-->\n" +
                "            <FREEUSE2>1</FREEUSE2>\n" +
                "            <!--Optional:-->\n" +
                "            <FREEUSE3>1</FREEUSE3>\n" +
                "            <!--Optional:-->\n" +
                "            <FREEUSE4>1</FREEUSE4>\n" +
                "            <!--Optional:-->\n" +
                "            <FREEUSE5>1</FREEUSE5>\n" +
                "         </VEHICLE_PGI_REQ>\n" +
                "      </ecc:MT_VEHICLE_PGI_REQ>\n" +
                "   </soapenv:Body>\n" +
                "</soapenv:Envelope>");
    }

    private static void testPGR(StringBuilder soapXmlBuf) {
        soapXmlBuf.append("<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:ecc=\"http://zjunma.com/TRAUM/WMS/ECC\">\n" +
                "   <soapenv:Header/>\n" +
                "   <soapenv:Body>\n" +
                "      <ecc:MT_PGR>\n" +
                "         <TP_HEADER>\n" +
                "            <MSGID>1</MSGID>\n" +
                "            <BUSID>1</BUSID>\n" +
                "            <TLGID>1</TLGID>\n" +
                "            <TLGNAME>1</TLGNAME>\n" +
                "            <DTSEND>1</DTSEND>\n" +
                "            <SENDER>1</SENDER>\n" +
                "            <RECEIVER>1</RECEIVER>\n" +
                "            <!--Optional:-->\n" +
                "            <FREEUSE>1</FREEUSE>\n" +
                "         </TP_HEADER>\n" +
                "         <!--Zero or more repetitions:-->\n" +
                "         <TP_BODY>\n" +
                "            <!--Optional:-->\n" +
                "            <DATA_ID>1</DATA_ID>\n" +
                "            <!--Optional:-->\n" +
                "            <VBELN>1</VBELN>\n" +
                "            <!--Optional:-->\n" +
                "            <POSNR>1</POSNR>\n" +
                "            <!--Optional:-->\n" +
                "            <KUNNR>1</KUNNR>\n" +
                "            <!--Optional:-->\n" +
                "            <NAME1>1</NAME1>\n" +
                "            <!--Optional:-->\n" +
                "            <MATNR>1</MATNR>\n" +
                "            <!--Optional:-->\n" +
                "            <MAKTX>1</MAKTX>\n" +
                "            <!--Optional:-->\n" +
                "            <SERNR>1</SERNR>\n" +
                "            <!--Optional:-->\n" +
                "            <LGORT>1</LGORT>\n" +
                "            <!--Optional:-->\n" +
                "            <MBDAT>1</MBDAT>\n" +
                "            <!--Optional:-->\n" +
                "            <FREEUSE1>1</FREEUSE1>\n" +
                "            <!--Optional:-->\n" +
                "            <FREEUSE2>1</FREEUSE2>\n" +
                "            <!--Optional:-->\n" +
                "            <FREEUSE3>1</FREEUSE3>\n" +
                "            <!--Optional:-->\n" +
                "            <FREEUSE4>1</FREEUSE4>\n" +
                "         </TP_BODY>\n" +
                "      </ecc:MT_PGR>\n" +
                "   </soapenv:Body>\n" +
                "</soapenv:Envelope>");
    }

    private static void testGIGR(StringBuilder soapXmlBuf) {
        //soapXmlBuf.append("<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:ecc=\"http://zjunma.com/TRAUM/WMS/ECC\">\n")
        //        .append(" <soapenv:Header/>\n")
        //        .append("<soapenv:Body>\n")
        //        .append("<ecc:MT_GIGR>\n")
        //        .append("<TP_HEADER>\n")
        //        .append("<MSGID>1</MSGID>\n")//消息id
        //        .append("<BUSID>1</BUSID>")//业务id
        //        .append("<TLGID>1</TLGID>\n")//接口id
        //        .append("<TLGNAME>1</TLGNAME>\n")//接口名称
        //        .append("<DTSEND>1</DTSEND>\n")//发送时间
        //        .append("<SENDER>1</SENDER>\n")//发送方
        //        .append("<RECEIVER>1</RECEIVER>")//接收方
        //        .append("<FREEUSE>1</FREEUSE>")//备用 (选填)
        //        .append("</TP_HEADER>\n")
        //        .append("<SPPO>")
        //        //必填
        //        .append("<DATA_ID>00001</DATA_ID>")//唯一标识
        //        .append("<MBLNR>123122</MBLNR>")//调拨单号
        //        .append("<SERNR>LEFDJ3BB6GTP16989</SERNR>")//车架号
        //        .append("<MATNR>测试001</MATNR>")//物料编码
        //        .append("<ZACTION>测试001</ZACTION>")//出入库
        //        .append("<ZTYPE>C1</ZTYPE>")//业务类型
        //        //选填
        //        .append("<BLDAT>1</BLDAT>")//出入库日期
        //        .append("<WERKS>1</WERKS>")//出入库工厂sap默认
        //        .append("<LGORT>1</LGORT>")//出入库仓库sap默认
        //        .append("<UMWRK>1</UMWRK>")//工厂
        //        .append("<UMLGO>1</UMLGO>")//库存地点
        //        .append("<ZTFLAG>1</ZTFLAG>")//特殊车辆标识
        //        .append("<ZDEMO>1</ZDEMO>")//备注
        //        .append("<FREEUSE1>1</FREEUSE1>")//备注
        //        .append("<FREEUSE2>1</FREEUSE2>")//备注
        //        .append("<FREEUSE3>1</FREEUSE3>")//备注
        //        .append("<FREEUSE4>1</FREEUSE4>")//备注
        //        .append("<FREEUSE5>1</FREEUSE5>")//备注
        //        .append("</SPPO>")
        //        .append("</ecc:MT_GIGR>\n")
        //        .append("</soapenv:Body>\n")
        //        .append("</soapenv:Envelope>");
        soapXmlBuf.append("<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:ecc=\"http://zjunma.com/TRAUM/WMS/ECC\">\n" +
                "   <soapenv:Header/>\n" +
                "   <soapenv:Body>\n" +
                "      <ecc:MT_GIGR>\n" +
                "        <TP_HEADER> \n" +
                "        <MSGID>27</MSGID>  \n" +
                "        <BUSID>27</BUSID>\n" +
                "        <TLGID>IF_WMS2SAP_Vehicle_GIGR</TLGID>  \n" +
                "        <TLGNAME>SAP出入库</TLGNAME>  \n" +
                "        <DTSEND>Tue Oct 31 20:06:19 CST 2017</DTSEND>  \n" +
                "        <SENDER>发送方WMS</SENDER>  \n" +
                "        <RECEIVER>接收方SAP</RECEIVER>\n" +
                "        <FREEUSE>备用</FREEUSE>\n" +
                "      </TP_HEADER>  \n" +
                "      <SPPO>\n" +
                "        <DATA_ID>27</DATA_ID>\n" +
                "        <MBLNR/>\n" +
                "        <SERNR>LEFDJABB4GTP57973</SERNR>\n" +
                "        <MATNR>null</MATNR>\n" +
                "        <ZACTION>C2</ZACTION>\n" +
                "        <ZTYPE>Z1-PDI</ZTYPE>\n" +
                "        <BLDAT>Tue Oct 31 20:06:10 CST 2017</BLDAT>\n" +
                "        <WERKS/>\n" +
                "        <LGORT/>\n" +
                "        <UMWRK/>\n" +
                "        <UMLGO>JM_CS</UMLGO>\n" +
                "        <ZTFLAG>否</ZTFLAG>\n" +
                "        <ZDEMO>null</ZDEMO>\n" +
                "      </SPPO>\n" +
                "      </ecc:MT_GIGR>\n" +
                "   </soapenv:Body>\n" +
                "</soapenv:Envelope>");


    }
}
