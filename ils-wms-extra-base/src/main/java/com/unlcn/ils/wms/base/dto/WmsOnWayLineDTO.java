package com.unlcn.ils.wms.base.dto;

import cn.huiyunche.commons.domain.PageVo;

import java.io.Serializable;

/**
 * 前段在途大屏参数封装对象
 */
public class WmsOnWayLineDTO extends PageVo implements Serializable {
    private String status;//查询状态

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}
