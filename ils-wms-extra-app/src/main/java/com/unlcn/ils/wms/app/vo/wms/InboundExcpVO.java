package com.unlcn.ils.wms.app.vo.wms;

/**
 * Created by houjianhui on 2017/5/8.
 */
public class InboundExcpVO {

    private Long entryId;

    private String waybillCode;

    private String excpType;

    private String detailVOS;

    public Long getEntryId() {
        return entryId;
    }

    public void setEntryId(Long entryId) {
        this.entryId = entryId;
    }

    public String getWaybillCode() {
        return waybillCode;
    }

    public void setWaybillCode(String waybillCode) {
        this.waybillCode = waybillCode;
    }

    public String getExcpType() {
        return excpType;
    }

    public void setExcpType(String excpType) {
        this.excpType = excpType;
    }

    public String getDetailVOS() {
        return detailVOS;
    }

    public void setDetailVOS(String detailVOS) {
        this.detailVOS = detailVOS;
    }

    @Override
    public String toString() {
        return "InboundExcpVO{" +
                "entryId=" + entryId +
                ", waybillCode='" + waybillCode + '\'' +
                ", excpType=" + excpType +
                ", detailVOS='" + detailVOS + '\'' +
                '}';
    }
}
