package com.unlcn.ils.wms.backend.service.excp;

import com.unlcn.ils.wms.backend.bo.excpBO.ExcpFormBO;
import com.unlcn.ils.wms.backend.dto.excpDTO.ExcpImageDTO;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;

import java.util.List;
import java.util.Map;

/**
 * @Auther linbao
 * @Date 2017-11-21
 * 异常信息管理
 */
public interface ExcpManageService {

    /**
     * 分页列表查询
     *
     * @param excpFormBO
     * @return
     * @throws Exception
     */
    Map<String, Object> queryExcpListForPage(ExcpFormBO excpFormBO) throws Exception;

    /**
     * 异常处理
     *
     * @param excpFormBO
     * @throws Exception
     */
    void updateToDealExcp(ExcpFormBO excpFormBO) throws Exception;

    /**
     * 异常关闭
     *
     * @param excpFormBO
     * @throws Exception
     */
    void updateToCloseExcp(ExcpFormBO excpFormBO) throws Exception;

    /**
     * 获取异常图片信息
     *
     * @param excpFormBO
     * @return
     * @throws Exception
     */
    List<ExcpImageDTO> getExcpImageList(ExcpFormBO excpFormBO) throws Exception;

    HSSFWorkbook getImportOutExcel(ExcpFormBO excpFormBO) throws Exception;
}
