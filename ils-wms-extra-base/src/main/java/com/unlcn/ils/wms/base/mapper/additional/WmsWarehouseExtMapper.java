package com.unlcn.ils.wms.base.mapper.additional;



import com.unlcn.ils.wms.base.businessDTO.baseData.WmsWarehouseDTO;
import com.unlcn.ils.wms.base.model.stock.WmsWarehouse;

import java.util.List;
import java.util.Map;

/**
 * 仓库
 * Created by DELL on 2017/8/14.
 */
public interface WmsWarehouseExtMapper {

    /**
     * 分页数据
     * @param paramMap
     * @return
     */
    List<WmsWarehouse> queryForList(Map<String, Object> paramMap);

    /**
     * 分页数据总数
     * @param paramMap
     * @return
     */
    Integer queryForCount(Map<String, Object> paramMap);

    /**
     * APP首页查询仓库
     *
     * @return
     */
    List<WmsWarehouseDTO> listWarehouseForApp();

}
