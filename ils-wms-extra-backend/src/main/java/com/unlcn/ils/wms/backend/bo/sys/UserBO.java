package com.unlcn.ils.wms.backend.bo.sys;

import java.util.Date;
import java.util.List;

/**
 * Created by houjianhui on 2017/5/11.
 */
public class UserBO {

    //id
    private Integer id;
    //用户名
    private String username;
    //密码
    private String password;
    //创建人
    private String createPerson;
    //修改人
    private String updatePersion;
    //创建时间
    private Date gmtCreate;
    //修改时间
    private Date gmtModified;
    //是否可用
    private String enable;
    //角色BO list
    private List<RoleBO> roleBOList;

    private Integer ilineid;

    private String vcuserno;

    private String vcusername;

    private String warehouseid;

    private String bvehicle_supplier;

    private Integer ideptid;

    //仓库代码
    private String whCode;

    public String getBvehicle_supplier() {
        return bvehicle_supplier;
    }

    public void setBvehicle_supplier(String bvehicle_supplier) {
        this.bvehicle_supplier = bvehicle_supplier;
    }

    public Integer getIdeptid() {
        return ideptid;
    }

    public void setIdeptid(Integer ideptid) {
        this.ideptid = ideptid;
    }

    public String getWhCode() {
        return whCode;
    }

    public void setWhCode(String whCode) {
        this.whCode = whCode;
    }

    public Integer getIlineid() {
        return ilineid;
    }

    public void setIlineid(Integer ilineid) {
        this.ilineid = ilineid;
    }

    public String getVcuserno() {
        return vcuserno;
    }

    public void setVcuserno(String vcuserno) {
        this.vcuserno = vcuserno;
    }

    public String getVcusername() {
        return vcusername;
    }

    public void setVcusername(String vcusername) {
        this.vcusername = vcusername;
    }

    public String getWarehouseid() {
        return warehouseid;
    }

    public void setWarehouseid(String warehouseid) {
        this.warehouseid = warehouseid;
    }

    /**
     * @return the id
     */
    public Integer getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * @return the username
     */
    public String getUsername() {
        return username;
    }

    /**
     * @param username the username to set
     */
    public void setUsername(String username) {
        this.username = username;
    }

    /**
     * @return the password
     */
    public String getPassword() {
        return password;
    }

    /**
     * @param password the password to set
     */
    public void setPassword(String password) {
        this.password = password;
    }

    /**
     * @return the createPerson
     */
    public String getCreatePerson() {
        return createPerson;
    }

    /**
     * @param createPerson the createPerson to set
     */
    public void setCreatePerson(String createPerson) {
        this.createPerson = createPerson;
    }

    /**
     * @return the updatePersion
     */
    public String getUpdatePersion() {
        return updatePersion;
    }

    /**
     * @param updatePersion the updatePersion to set
     */
    public void setUpdatePersion(String updatePersion) {
        this.updatePersion = updatePersion;
    }

    /**
     * @return the gmtCreate
     */
    public Date getGmtCreate() {
        return gmtCreate;
    }

    /**
     * @param gmtCreate the gmtCreate to set
     */
    public void setGmtCreate(Date gmtCreate) {
        this.gmtCreate = gmtCreate;
    }

    /**
     * @return the gmtModified
     */
    public Date getGmtModified() {
        return gmtModified;
    }

    /**
     * @param gmtModified the gmtModified to set
     */
    public void setGmtModified(Date gmtModified) {
        this.gmtModified = gmtModified;
    }

    /**
     * @return the enable
     */
    public String getEnable() {
        return enable;
    }

    /**
     * @param enable the enable to set
     */
    public void setEnable(String enable) {
        this.enable = enable;
    }

    /**
     * @return the roleBOList
     */
    public List<RoleBO> getRoleBOList() {
        return roleBOList;
    }

    /**
     * @param roleBOList the roleBOList to set
     */
    public void setRoleBOList(List<RoleBO> roleBOList) {
        this.roleBOList = roleBOList;
    }

	/*
     * Title: toString
	 * Description: toString
	 * @return
	 * @see java.lang.Object#toString()
	 *
	 */

    @Override
    public String toString() {
        return "UserBO{" +
                "id=" + id +
                ", username='" + username + '\'' +
                ", password='" + password + '\'' +
                ", createPerson='" + createPerson + '\'' +
                ", updatePersion='" + updatePersion + '\'' +
                ", gmtCreate=" + gmtCreate +
                ", gmtModified=" + gmtModified +
                ", enable='" + enable + '\'' +
                ", roleBOList=" + roleBOList +
                ", ilineid=" + ilineid +
                ", vcuserno='" + vcuserno + '\'' +
                ", vcusername='" + vcusername + '\'' +
                ", warehouseid='" + warehouseid + '\'' +
                ", whCode='" + whCode + '\'' +
                '}';
    }
}
