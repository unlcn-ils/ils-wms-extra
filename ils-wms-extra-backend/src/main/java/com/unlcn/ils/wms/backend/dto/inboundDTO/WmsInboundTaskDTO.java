package com.unlcn.ils.wms.backend.dto.inboundDTO;

import java.util.Date;

/**
 * Created by DELL on 2017/8/18.
 */
public class WmsInboundTaskDTO {
    private Long tskId;

    private Long tskWhId;

    private String tskAsnOrderId;

    private String tskBillId;

    private String tskAlId;

    private Long tskStorerId;

    private String tskType;

    private String tskTypeName;

    private String tskStatus;

    private String tskStatusName;

    private String tskFinishPerson;

    private Date tskFinishTime;

    private String tskOpenPerson;

    private Date tskOpenTime;

    private String tskSourceLocation;

    private String tskTargetLocation;

    private String createPerson;

    private String updatePerson;

    private Date gmtCreate;

    private Date gmtUpdate;

    private Byte isDeleted;

    private Long versions;

    public Long getTskId() {
        return tskId;
    }

    public void setTskId(Long tskId) {
        this.tskId = tskId;
    }

    public Long getTskWhId() {
        return tskWhId;
    }

    public void setTskWhId(Long tskWhId) {
        this.tskWhId = tskWhId;
    }

    public String getTskAsnOrderId() {
        return tskAsnOrderId;
    }

    public void setTskAsnOrderId(String tskAsnOrderId) {
        this.tskAsnOrderId = tskAsnOrderId;
    }

    public String getTskBillId() {
        return tskBillId;
    }

    public void setTskBillId(String tskBillId) {
        this.tskBillId = tskBillId;
    }

    public String getTskAlId() {
        return tskAlId;
    }

    public void setTskAlId(String tskAlId) {
        this.tskAlId = tskAlId;
    }

    public Long getTskStorerId() {
        return tskStorerId;
    }

    public void setTskStorerId(Long tskStorerId) {
        this.tskStorerId = tskStorerId;
    }

    public String getTskType() {
        return tskType;
    }

    public void setTskType(String tskType) {
        this.tskType = tskType;
    }

    public String getTskTypeName() {
        return tskTypeName;
    }

    public void setTskTypeName(String tskTypeName) {
        this.tskTypeName = tskTypeName;
    }

    public String getTskStatus() {
        return tskStatus;
    }

    public void setTskStatus(String tskStatus) {
        this.tskStatus = tskStatus;
    }

    public String getTskStatusName() {
        return tskStatusName;
    }

    public void setTskStatusName(String tskStatusName) {
        this.tskStatusName = tskStatusName;
    }

    public String getTskFinishPerson() {
        return tskFinishPerson;
    }

    public void setTskFinishPerson(String tskFinishPerson) {
        this.tskFinishPerson = tskFinishPerson;
    }

    public Date getTskFinishTime() {
        return tskFinishTime;
    }

    public void setTskFinishTime(Date tskFinishTime) {
        this.tskFinishTime = tskFinishTime;
    }

    public String getTskOpenPerson() {
        return tskOpenPerson;
    }

    public void setTskOpenPerson(String tskOpenPerson) {
        this.tskOpenPerson = tskOpenPerson;
    }

    public Date getTskOpenTime() {
        return tskOpenTime;
    }

    public void setTskOpenTime(Date tskOpenTime) {
        this.tskOpenTime = tskOpenTime;
    }

    public String getTskSourceLocation() {
        return tskSourceLocation;
    }

    public void setTskSourceLocation(String tskSourceLocation) {
        this.tskSourceLocation = tskSourceLocation;
    }

    public String getTskTargetLocation() {
        return tskTargetLocation;
    }

    public void setTskTargetLocation(String tskTargetLocation) {
        this.tskTargetLocation = tskTargetLocation;
    }

    public String getCreatePerson() {
        return createPerson;
    }

    public void setCreatePerson(String createPerson) {
        this.createPerson = createPerson;
    }

    public String getUpdatePerson() {
        return updatePerson;
    }

    public void setUpdatePerson(String updatePerson) {
        this.updatePerson = updatePerson;
    }

    public Date getGmtCreate() {
        return gmtCreate;
    }

    public void setGmtCreate(Date gmtCreate) {
        this.gmtCreate = gmtCreate;
    }

    public Date getGmtUpdate() {
        return gmtUpdate;
    }

    public void setGmtUpdate(Date gmtUpdate) {
        this.gmtUpdate = gmtUpdate;
    }

    public Byte getIsDeleted() {
        return isDeleted;
    }

    public void setIsDeleted(Byte isDeleted) {
        this.isDeleted = isDeleted;
    }

    public Long getVersions() {
        return versions;
    }

    public void setVersions(Long versions) {
        this.versions = versions;
    }

    @Override
    public String toString() {
        return "WmsInboundTaskDTO{" +
                "tskId=" + tskId +
                ", tskWhId=" + tskWhId +
                ", tskAsnOrderId='" + tskAsnOrderId + '\'' +
                ", tskBillId='" + tskBillId + '\'' +
                ", tskAlId='" + tskAlId + '\'' +
                ", tskStorerId=" + tskStorerId +
                ", tskType='" + tskType + '\'' +
                ", tskTypeName='" + tskTypeName + '\'' +
                ", tskStatus='" + tskStatus + '\'' +
                ", tskStatusName='" + tskStatusName + '\'' +
                ", tskFinishPerson='" + tskFinishPerson + '\'' +
                ", tskFinishTime=" + tskFinishTime +
                ", tskOpenPerson='" + tskOpenPerson + '\'' +
                ", tskOpenTime=" + tskOpenTime +
                ", tskSourceLocation='" + tskSourceLocation + '\'' +
                ", tskTargetLocation='" + tskTargetLocation + '\'' +
                ", createPerson='" + createPerson + '\'' +
                ", updatePerson='" + updatePerson + '\'' +
                ", gmtCreate=" + gmtCreate +
                ", gmtUpdate=" + gmtUpdate +
                ", isDeleted=" + isDeleted +
                ", versions=" + versions +
                '}';
    }
}
