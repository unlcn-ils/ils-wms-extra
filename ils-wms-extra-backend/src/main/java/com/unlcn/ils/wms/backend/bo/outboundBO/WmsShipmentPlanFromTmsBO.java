package com.unlcn.ils.wms.backend.bo.outboundBO;

import java.io.Serializable;

public class WmsShipmentPlanFromTmsBO implements Serializable {
    private String custshipno;
    private String orderno;
    private String vin;
    private String style;
    private String styleDesc;
    private String origin;
    private String dest;
    private String customer;
    private String shipno;
    private String supplier;
    private String vehicle;
    private String dtship;
    private String route_end;
    private String driver;
    private String mobile;

    public String getDriver() {
        return driver;
    }

    public void setDriver(String driver) {
        this.driver = driver;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getCustshipno() {
        return custshipno;
    }

    public void setCustshipno(String custshipno) {
        this.custshipno = custshipno;
    }

    public String getOrderno() {
        return orderno;
    }

    public void setOrderno(String orderno) {
        this.orderno = orderno;
    }

    public String getVin() {
        return vin;
    }

    public void setVin(String vin) {
        this.vin = vin;
    }

    public String getStyle() {
        return style;
    }

    public void setStyle(String style) {
        this.style = style;
    }

    public String getStyleDesc() {
        return styleDesc;
    }

    public void setStyleDesc(String styleDesc) {
        this.styleDesc = styleDesc;
    }

    public String getOrigin() {
        return origin;
    }

    public void setOrigin(String origin) {
        this.origin = origin;
    }

    public String getDest() {
        return dest;
    }

    public void setDest(String dest) {
        this.dest = dest;
    }

    public String getCustomer() {
        return customer;
    }

    public void setCustomer(String customer) {
        this.customer = customer;
    }

    public String getShipno() {
        return shipno;
    }

    public void setShipno(String shipno) {
        this.shipno = shipno;
    }

    public String getSupplier() {
        return supplier;
    }

    public void setSupplier(String supplier) {
        this.supplier = supplier;
    }

    public String getVehicle() {
        return vehicle;
    }

    public void setVehicle(String vehicle) {
        this.vehicle = vehicle;
    }

    public String getDtship() {
        return dtship;
    }

    public void setDtship(String dtship) {
        this.dtship = dtship;
    }

    public String getRoute_end() {
        return route_end;
    }

    public void setRoute_end(String route_end) {
        this.route_end = route_end;
    }

    @Override
    public String toString() {
        return "WmsShipmentPlanFromTmsBO{" +
                "custshipno='" + custshipno + '\'' +
                ", orderno='" + orderno + '\'' +
                ", vin='" + vin + '\'' +
                ", style='" + style + '\'' +
                ", styleDesc='" + styleDesc + '\'' +
                ", origin='" + origin + '\'' +
                ", dest='" + dest + '\'' +
                ", customer='" + customer + '\'' +
                ", shipno='" + shipno + '\'' +
                ", supplier='" + supplier + '\'' +
                ", vehicle='" + vehicle + '\'' +
                ", dtship='" + dtship + '\'' +
                ", route_end='" + route_end + '\'' +
                '}';
    }
}
