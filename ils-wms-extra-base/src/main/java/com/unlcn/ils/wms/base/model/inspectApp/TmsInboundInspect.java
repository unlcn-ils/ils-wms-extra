package com.unlcn.ils.wms.base.model.inspectApp;

import java.io.Serializable;
import java.util.Date;

public class TmsInboundInspect implements Serializable {
    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column tms_inbound_inspect.id
     *
     * @mbggenerated Wed Sep 27 10:44:04 CST 2017
     */
    private Long id;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column tms_inbound_inspect.orderno
     *
     * @mbggenerated Wed Sep 27 10:44:04 CST 2017
     */
    private String orderno;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column tms_inbound_inspect.cust_orderno
     *
     * @mbggenerated Wed Sep 27 10:44:04 CST 2017
     */
    private String custOrderno;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column tms_inbound_inspect.cust_shipno
     *
     * @mbggenerated Wed Sep 27 10:44:04 CST 2017
     */
    private String custShipno;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column tms_inbound_inspect.customer
     *
     * @mbggenerated Wed Sep 27 10:44:04 CST 2017
     */
    private String customer;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column tms_inbound_inspect.origin
     *
     * @mbggenerated Wed Sep 27 10:44:04 CST 2017
     */
    private String origin;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column tms_inbound_inspect.dest
     *
     * @mbggenerated Wed Sep 27 10:44:04 CST 2017
     */
    private String dest;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column tms_inbound_inspect.vin
     *
     * @mbggenerated Wed Sep 27 10:44:04 CST 2017
     */
    private String vin;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column tms_inbound_inspect.style
     *
     * @mbggenerated Wed Sep 27 10:44:04 CST 2017
     */
    private String style;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column tms_inbound_inspect.style_desc
     *
     * @mbggenerated Wed Sep 27 10:44:04 CST 2017
     */
    private String styleDesc;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column tms_inbound_inspect.inspect_status
     *
     * @mbggenerated Wed Sep 27 10:44:04 CST 2017
     */
    private Integer inspectStatus;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column tms_inbound_inspect.inspect_user_id
     *
     * @mbggenerated Wed Sep 27 10:44:04 CST 2017
     */
    private String inspectUserId;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column tms_inbound_inspect.gmt_create
     *
     * @mbggenerated Wed Sep 27 10:44:04 CST 2017
     */
    private Date gmtCreate;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column tms_inbound_inspect.gmt_update
     *
     * @mbggenerated Wed Sep 27 10:44:04 CST 2017
     */
    private Date gmtUpdate;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database table tms_inbound_inspect
     *
     * @mbggenerated Wed Sep 27 10:44:04 CST 2017
     */
    private static final long serialVersionUID = 1L;

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column tms_inbound_inspect.id
     *
     * @return the value of tms_inbound_inspect.id
     *
     * @mbggenerated Wed Sep 27 10:44:04 CST 2017
     */
    public Long getId() {
        return id;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column tms_inbound_inspect.id
     *
     * @param id the value for tms_inbound_inspect.id
     *
     * @mbggenerated Wed Sep 27 10:44:04 CST 2017
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column tms_inbound_inspect.orderno
     *
     * @return the value of tms_inbound_inspect.orderno
     *
     * @mbggenerated Wed Sep 27 10:44:04 CST 2017
     */
    public String getOrderno() {
        return orderno;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column tms_inbound_inspect.orderno
     *
     * @param orderno the value for tms_inbound_inspect.orderno
     *
     * @mbggenerated Wed Sep 27 10:44:04 CST 2017
     */
    public void setOrderno(String orderno) {
        this.orderno = orderno == null ? null : orderno.trim();
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column tms_inbound_inspect.cust_orderno
     *
     * @return the value of tms_inbound_inspect.cust_orderno
     *
     * @mbggenerated Wed Sep 27 10:44:04 CST 2017
     */
    public String getCustOrderno() {
        return custOrderno;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column tms_inbound_inspect.cust_orderno
     *
     * @param custOrderno the value for tms_inbound_inspect.cust_orderno
     *
     * @mbggenerated Wed Sep 27 10:44:04 CST 2017
     */
    public void setCustOrderno(String custOrderno) {
        this.custOrderno = custOrderno == null ? null : custOrderno.trim();
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column tms_inbound_inspect.cust_shipno
     *
     * @return the value of tms_inbound_inspect.cust_shipno
     *
     * @mbggenerated Wed Sep 27 10:44:04 CST 2017
     */
    public String getCustShipno() {
        return custShipno;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column tms_inbound_inspect.cust_shipno
     *
     * @param custShipno the value for tms_inbound_inspect.cust_shipno
     *
     * @mbggenerated Wed Sep 27 10:44:04 CST 2017
     */
    public void setCustShipno(String custShipno) {
        this.custShipno = custShipno == null ? null : custShipno.trim();
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column tms_inbound_inspect.customer
     *
     * @return the value of tms_inbound_inspect.customer
     *
     * @mbggenerated Wed Sep 27 10:44:04 CST 2017
     */
    public String getCustomer() {
        return customer;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column tms_inbound_inspect.customer
     *
     * @param customer the value for tms_inbound_inspect.customer
     *
     * @mbggenerated Wed Sep 27 10:44:04 CST 2017
     */
    public void setCustomer(String customer) {
        this.customer = customer == null ? null : customer.trim();
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column tms_inbound_inspect.origin
     *
     * @return the value of tms_inbound_inspect.origin
     *
     * @mbggenerated Wed Sep 27 10:44:04 CST 2017
     */
    public String getOrigin() {
        return origin;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column tms_inbound_inspect.origin
     *
     * @param origin the value for tms_inbound_inspect.origin
     *
     * @mbggenerated Wed Sep 27 10:44:04 CST 2017
     */
    public void setOrigin(String origin) {
        this.origin = origin == null ? null : origin.trim();
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column tms_inbound_inspect.dest
     *
     * @return the value of tms_inbound_inspect.dest
     *
     * @mbggenerated Wed Sep 27 10:44:04 CST 2017
     */
    public String getDest() {
        return dest;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column tms_inbound_inspect.dest
     *
     * @param dest the value for tms_inbound_inspect.dest
     *
     * @mbggenerated Wed Sep 27 10:44:04 CST 2017
     */
    public void setDest(String dest) {
        this.dest = dest == null ? null : dest.trim();
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column tms_inbound_inspect.vin
     *
     * @return the value of tms_inbound_inspect.vin
     *
     * @mbggenerated Wed Sep 27 10:44:04 CST 2017
     */
    public String getVin() {
        return vin;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column tms_inbound_inspect.vin
     *
     * @param vin the value for tms_inbound_inspect.vin
     *
     * @mbggenerated Wed Sep 27 10:44:04 CST 2017
     */
    public void setVin(String vin) {
        this.vin = vin == null ? null : vin.trim();
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column tms_inbound_inspect.style
     *
     * @return the value of tms_inbound_inspect.style
     *
     * @mbggenerated Wed Sep 27 10:44:04 CST 2017
     */
    public String getStyle() {
        return style;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column tms_inbound_inspect.style
     *
     * @param style the value for tms_inbound_inspect.style
     *
     * @mbggenerated Wed Sep 27 10:44:04 CST 2017
     */
    public void setStyle(String style) {
        this.style = style == null ? null : style.trim();
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column tms_inbound_inspect.style_desc
     *
     * @return the value of tms_inbound_inspect.style_desc
     *
     * @mbggenerated Wed Sep 27 10:44:04 CST 2017
     */
    public String getStyleDesc() {
        return styleDesc;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column tms_inbound_inspect.style_desc
     *
     * @param styleDesc the value for tms_inbound_inspect.style_desc
     *
     * @mbggenerated Wed Sep 27 10:44:04 CST 2017
     */
    public void setStyleDesc(String styleDesc) {
        this.styleDesc = styleDesc == null ? null : styleDesc.trim();
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column tms_inbound_inspect.inspect_status
     *
     * @return the value of tms_inbound_inspect.inspect_status
     *
     * @mbggenerated Wed Sep 27 10:44:04 CST 2017
     */
    public Integer getInspectStatus() {
        return inspectStatus;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column tms_inbound_inspect.inspect_status
     *
     * @param inspectStatus the value for tms_inbound_inspect.inspect_status
     *
     * @mbggenerated Wed Sep 27 10:44:04 CST 2017
     */
    public void setInspectStatus(Integer inspectStatus) {
        this.inspectStatus = inspectStatus;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column tms_inbound_inspect.inspect_user_id
     *
     * @return the value of tms_inbound_inspect.inspect_user_id
     *
     * @mbggenerated Wed Sep 27 10:44:04 CST 2017
     */
    public String getInspectUserId() {
        return inspectUserId;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column tms_inbound_inspect.inspect_user_id
     *
     * @param inspectUserId the value for tms_inbound_inspect.inspect_user_id
     *
     * @mbggenerated Wed Sep 27 10:44:04 CST 2017
     */
    public void setInspectUserId(String inspectUserId) {
        this.inspectUserId = inspectUserId == null ? null : inspectUserId.trim();
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column tms_inbound_inspect.gmt_create
     *
     * @return the value of tms_inbound_inspect.gmt_create
     *
     * @mbggenerated Wed Sep 27 10:44:04 CST 2017
     */
    public Date getGmtCreate() {
        return gmtCreate;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column tms_inbound_inspect.gmt_create
     *
     * @param gmtCreate the value for tms_inbound_inspect.gmt_create
     *
     * @mbggenerated Wed Sep 27 10:44:04 CST 2017
     */
    public void setGmtCreate(Date gmtCreate) {
        this.gmtCreate = gmtCreate;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column tms_inbound_inspect.gmt_update
     *
     * @return the value of tms_inbound_inspect.gmt_update
     *
     * @mbggenerated Wed Sep 27 10:44:04 CST 2017
     */
    public Date getGmtUpdate() {
        return gmtUpdate;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column tms_inbound_inspect.gmt_update
     *
     * @param gmtUpdate the value for tms_inbound_inspect.gmt_update
     *
     * @mbggenerated Wed Sep 27 10:44:04 CST 2017
     */
    public void setGmtUpdate(Date gmtUpdate) {
        this.gmtUpdate = gmtUpdate;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table tms_inbound_inspect
     *
     * @mbggenerated Wed Sep 27 10:44:04 CST 2017
     */
    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", id=").append(id);
        sb.append(", orderno=").append(orderno);
        sb.append(", custOrderno=").append(custOrderno);
        sb.append(", custShipno=").append(custShipno);
        sb.append(", customer=").append(customer);
        sb.append(", origin=").append(origin);
        sb.append(", dest=").append(dest);
        sb.append(", vin=").append(vin);
        sb.append(", style=").append(style);
        sb.append(", styleDesc=").append(styleDesc);
        sb.append(", inspectStatus=").append(inspectStatus);
        sb.append(", inspectUserId=").append(inspectUserId);
        sb.append(", gmtCreate=").append(gmtCreate);
        sb.append(", gmtUpdate=").append(gmtUpdate);
        sb.append("]");
        return sb.toString();
    }
}