package com.unlcn.ils.wms.backend.util;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

/**
 * Created by lenovo on 2017/11/13.
 */
public class TimeUtil {

    public static void main(String[] args) {
        System.out.println(TimeUtil.getEndTime(1));
    }

    /**
     * 获取num前的开始时间
     * @param num
     * @return
     */
    public static String getStartTime(Integer num) {
        Date dNow = new Date();
        Date dBefore = new Date();
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(dNow);
        calendar.add(Calendar.DAY_OF_MONTH, -num);
        dBefore = calendar.getTime();
        SimpleDateFormat sdf=new SimpleDateFormat("yyyy-MM-dd");
        String defaultStartDate = sdf.format(dBefore);
        defaultStartDate = defaultStartDate+" 00:00:00";
        return defaultStartDate.substring(0,10)+" 00:00:00";
    }

    /**
     * 获取num前的结束时间
     * @param num
     * @return
     */
    public static String getEndTime(Integer num) {
        Date dNow = new Date();
        Date dBefore = new Date();
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(dNow);
        calendar.add(Calendar.DAY_OF_MONTH, -num);
        dBefore = calendar.getTime();
        SimpleDateFormat sdf=new SimpleDateFormat("yyyy-MM-dd");
        String defaultStartDate = sdf.format(dBefore);
        defaultStartDate = defaultStartDate+" 00:00:00";
        return defaultStartDate.substring(0,10)+" 23:59:59";
    }

    /**
     * 获取前num天到现在所有的日期（格式  yyyyMMdd）
     * @param num
     * @return
     */
    public static String[] getBeforeDates1(int num) {
        SimpleDateFormat format = new SimpleDateFormat("yyyyMMdd");
        String[] dates = new String[num];
        Integer j = 0;
        for (int i=0; num>i; num--) {
            Calendar calendar = Calendar.getInstance();
            calendar.set(Calendar.DAY_OF_YEAR, calendar.get(Calendar.DAY_OF_YEAR) -num);
            Date beforeFiveDate = calendar.getTime();
            String startTime = format.format(beforeFiveDate);
            dates[j] = startTime;
            j ++ ;
        }
        return dates;
    }

    /**
     * 获取前num天到现在所有的日期（格式  MM/dd）
     * @param num
     * @return
     */
    public static String[] getBeforeDates2(int num) {
        SimpleDateFormat format = new SimpleDateFormat("MM/dd");
        String[] dates = new String[num];
        Integer j = 0;
        for (int i=0; num>i; num--) {
            Calendar calendar = Calendar.getInstance();
            calendar.set(Calendar.DAY_OF_YEAR, calendar.get(Calendar.DAY_OF_YEAR) - num);
            Date beforeFiveDate = calendar.getTime();
            String startTime = format.format(beforeFiveDate);
            dates[j] = startTime;
            j ++ ;
        }
        return dates;
    }
}
