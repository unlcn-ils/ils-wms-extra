package com.unlcn.ils.wms.base.dto;

import cn.huiyunche.commons.domain.PageVo;

import java.io.Serializable;

public class WmsJmHandoverOrderOperationParamDTO extends PageVo implements Serializable {
    private String whCode;
    private String userId;
    private String dataIds;
    private String excpId;
    private String sendDcsStatus;
    private String sendSapStatus;
    private String sendCrmStatus;
    private String gbno;//组板单号
    private String orderno;//订单号
    private String vin;
    private String handoverNumber;//交接单号
    private String startTime;//起始时间 格式(yyyy-MM-dd)
    private String endTime; //截止时间

    public String getVin() {
        return vin;
    }

    public void setVin(String vin) {
        this.vin = vin;
    }

    public String getWhCode() {
        return whCode;
    }

    public void setWhCode(String whCode) {
        this.whCode = whCode;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getDataIds() {
        return dataIds;
    }

    public void setDataIds(String dataIds) {
        this.dataIds = dataIds;
    }

    public String getExcpId() {
        return excpId;
    }

    public void setExcpId(String excpId) {
        this.excpId = excpId;
    }


    public String getSendDcsStatus() {
        return sendDcsStatus;
    }

    public void setSendDcsStatus(String sendDcsStatus) {
        this.sendDcsStatus = sendDcsStatus;
    }

    public String getSendSapStatus() {
        return sendSapStatus;
    }

    public void setSendSapStatus(String sendSapStatus) {
        this.sendSapStatus = sendSapStatus;
    }

    public String getStartTime() {
        return startTime;
    }

    public void setStartTime(String startTime) {
        this.startTime = startTime;
    }

    public String getEndTime() {
        return endTime;
    }

    public void setEndTime(String endTime) {
        this.endTime = endTime;
    }

    public String getSendCrmStatus() {
        return sendCrmStatus;
    }

    public void setSendCrmStatus(String sendCrmStatus) {
        this.sendCrmStatus = sendCrmStatus;
    }

    public String getGbno() {
        return gbno;
    }

    public void setGbno(String gbno) {
        this.gbno = gbno;
    }

    public String getOrderno() {
        return orderno;
    }

    public void setOrderno(String orderno) {
        this.orderno = orderno;
    }

    public String getHandoverNumber() {
        return handoverNumber;
    }

    public void setHandoverNumber(String handoverNumber) {
        this.handoverNumber = handoverNumber;
    }
}
