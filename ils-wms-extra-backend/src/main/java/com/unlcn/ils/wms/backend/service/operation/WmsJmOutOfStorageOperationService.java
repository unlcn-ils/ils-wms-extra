package com.unlcn.ils.wms.backend.service.operation;

import com.unlcn.ils.wms.base.dto.WmsJmOutOfStorageExcpOperationDTO;

import java.util.HashMap;

public interface WmsJmOutOfStorageOperationService {
    HashMap<String, Object> listOutOfStorageExcp(WmsJmOutOfStorageExcpOperationDTO dto) throws Exception;

    void updateSendToDCSAgain(WmsJmOutOfStorageExcpOperationDTO dto) throws Exception;

    void updateSendToSapAgain(WmsJmOutOfStorageExcpOperationDTO dto) throws Exception;

    void updateSendToSapSuccess(WmsJmOutOfStorageExcpOperationDTO dto) throws Exception;

    void updateDcsToSuccess(WmsJmOutOfStorageExcpOperationDTO dto)throws Exception;

}
