package com.unlcn.ils.wms.base.mapper.additional.wmsOutbound;

import com.unlcn.ils.wms.base.businessDTO.outbound.WmsOutboundTaskDTO;
import com.unlcn.ils.wms.base.businessDTO.outbound.WmsOutboundTaskDTOForQuery;
import com.unlcn.ils.wms.base.dto.OutboundExcelDTO;
import com.unlcn.ils.wms.base.dto.WmsAddTMSOutboundInterfaceDTO;
import com.unlcn.ils.wms.base.model.outbound.WmsOutboundTask;
import org.apache.ibatis.annotations.Param;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * created by linbao on 2017-09-28
 */
public interface WmsOutboundTaskExtMapper {


    Integer selectOutboundNum(@Param("startTime") String startTime, @Param("endTime") String endTime);

    List<WmsOutboundTaskDTO> listOutboundRecordsForPage(@Param("paramMap") Map<String, Object> paramMap);

    Integer countOutboundRecordsForPage(@Param("paramMap") Map<String, Object> paramMap);

    List<WmsOutboundTaskDTO> getPreparateInfoByVin(Map<String, Object> paramMap);

    List<WmsOutboundTaskDTO> getPreparateInfoByMaterialNo(Map<String, Object> paramMap);

    void batchInsert(@Param("wmsOutboundTasks") List<WmsOutboundTask> wmsOutboundTasks);

    void deleteByVin(@Param("vinList") List<String> vinList);

    List<OutboundExcelDTO> outboundExcelData(@Param("startTime") String startTime, @Param("endTime") String endTime);

    int countOutboundRecordsForPageJm(@Param("paramMap") Map<String, Object> paramMap);

    List<WmsOutboundTaskDTOForQuery> listOutboundRecordsForPageJm(@Param("paramMap") Map<String, Object> paramMap);

    Integer countTaskForShipUpdate(Map<String, Object> paramMap);

    String getStatusForDeleteShipment(@Param("dispNo") String dispNo, @Param("vin") String vin);

    Integer countShipStatusForDeleteShipment(@Param("dispNo") String dispNo, @Param("vin") String vin);

    WmsOutboundTaskDTO selecByVehiclePlateNum(@Param("vehiclePlateNum") String vehiclePlateNum);

    List<WmsOutboundTask> selectHasPrepareFinishedList(@Param("orderNo") String orderNo, @Param("finishValue") String finishValue);

    List<WmsOutboundTask> selectNotPrepareFinishedList(@Param("orderNo") String orderNo, @Param("cancelValue") String cancelValue, @Param("finishedValue") String finishedValue);

    List<WmsOutboundTaskDTO> listOutboundRecordForImport(@Param("paramMap") Map<String, Object> paramMap);

    Integer countOutboundRecordForImport(@Param("paramMap") Map<String, Object> paramMap);

    List<WmsOutboundTask> selectHasGateOutList(HashMap<String, Object> paramMap);

    void updateBatch(List<WmsOutboundTask> list);

    void updateBatchBlConfirmTimeForJM(WmsOutboundTask wmsOutboundTask);

    List<WmsOutboundTaskDTOForQuery> listOutboundRecordForManageByParam(HashMap<String, Object> params);

    void updateSQLmode();

    Long countOutboundRecordForManageByParam(HashMap<String, Object> params);

    List<WmsOutboundTaskDTO> listOutboundTaskPrintForPage(@Param("paramMap") Map<String, Object> paramMap);

    int countOutboundTaskPrintForPage(@Param("paramMap") Map<String, Object> paramMap);

    int countOutboundTasksForPage(@Param("paramMap") Map<String, Object> paramMap);

    List<WmsOutboundTaskDTO> listOutboundTasksForPage(@Param("paramMap") Map<String, Object> paramMap);

    List<WmsOutboundTaskDTOForQuery> listOutboundRecordsForPageJmWithNotice(@Param("paramMap") Map<String, Object> paramMap);

    Integer countOutboundRecordsForPageJmWithNotice(@Param("paramMap") Map<String, Object> paramMap);

    List<WmsOutboundTaskDTOForQuery> listOutboundRecordForManageByParamWithNotice(HashMap<String, Object> params);

    Long countOutboundRecordForManageByParamWithNotice(HashMap<String, Object> params);

    List<WmsOutboundTaskDTOForQuery> selectOutboundRecordsByParamForCQ(@Param("paramMap") Map<String, Object> paramMap);

    int countOutboundRecordsByParamForCQ(@Param("paramMap") Map<String, Object> paramMap);

    List<WmsAddTMSOutboundInterfaceDTO> selectSendOutboundDataForTmsByParams(HashMap<Object, Object> params);

    List<WmsAddTMSOutboundInterfaceDTO> selectSendOutboundExcpDataForTmsByParams(HashMap<Object, Object> params);
}

