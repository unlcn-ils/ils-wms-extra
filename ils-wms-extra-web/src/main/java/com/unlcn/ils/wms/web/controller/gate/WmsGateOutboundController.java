package com.unlcn.ils.wms.web.controller.gate;

import cn.huiyunche.commons.domain.ResultDTO;
import cn.huiyunche.commons.exception.BusinessException;
import com.unlcn.ils.wms.backend.bo.outboundBO.WmsDepartureBO;
import com.unlcn.ils.wms.backend.service.outbound.WmsCQOutboundService;
import com.unlcn.ils.wms.backend.service.outbound.WmsDepartureRegisterService;
import com.unlcn.ils.wms.base.dto.WmsCqInboundTmsDTO;
import com.unlcn.ils.wms.web.vo.outbound.WmsDepartureVO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * @Auther linbao
 * @Date 2017-11-14
 * 板车离场登记
 */
@RestController
@RequestMapping("/departrueRegister")
public class WmsGateOutboundController {

    private final static Logger LOGGER = LoggerFactory.getLogger(WmsGateOutboundController.class);

    @Autowired
    private WmsDepartureRegisterService departureRegisterService;


    /**
     * 正常离场
     *
     * @param wmsDepartureVO
     * @return
     */
    @RequestMapping(value = "/normarDeparture", method = RequestMethod.POST)
    public ResultDTO<Object> normarDeparture(@RequestBody WmsDepartureVO wmsDepartureVO) {
        LOGGER.info("WmsGateOutboundController.normarDeparture wmsDepartureVO: {}", wmsDepartureVO);
        ResultDTO<Object> resultDTO = new ResultDTO<>(true, null, "正常离场成功");
        try {
            WmsDepartureBO departureBO = new WmsDepartureBO();
            BeanUtils.copyProperties(wmsDepartureVO, departureBO);
            departureRegisterService.updateToNormalDeparture(departureBO);
        } catch (BusinessException e) {
            LOGGER.error("WmsGateOutboundController.normarDeparture businessException: {}", e);
            resultDTO.setMessage(e.getMessage());
            resultDTO.setSuccess(false);
        } catch (Exception e) {
            LOGGER.error("WmsGateOutboundController.normarDeparture error: {}", e);
            resultDTO.setMessage("正常离场失败");
            resultDTO.setSuccess(false);
        }
        return resultDTO;
    }

    /**
     * 异常离场
     *
     * @param wmsDepartureVO
     * @return
     */
    @RequestMapping(value = "/errorDeparture", method = RequestMethod.POST)
    public ResultDTO<Object> errorDeparture(@RequestBody WmsDepartureVO wmsDepartureVO) {
        LOGGER.info("WmsGateOutboundController.errorDeparture wmsDepartureVO: {}", wmsDepartureVO);
        ResultDTO<Object> resultDTO = new ResultDTO<>(true, null, "异常离场成功");
        try {
            WmsDepartureBO departureBO = new WmsDepartureBO();
            BeanUtils.copyProperties(wmsDepartureVO, departureBO);
            departureRegisterService.updateToExceptionDeparture(departureBO);
        } catch (BusinessException e) {
            LOGGER.error("WmsGateOutboundController.errorDeparture businessException: {}", e);
            resultDTO.setMessage(e.getMessage());
            resultDTO.setSuccess(false);
        } catch (Exception e) {
            LOGGER.error("WmsGateOutboundController.errorDeparture error: {}", e);
            resultDTO.setMessage("异常离场失败");
            resultDTO.setSuccess(false);
        }
        return resultDTO;
    }
}
