package com.unlcn.ils.wms.backend.enums;

/**
 * The type Tms url constant.
 */
public enum TransModeEnum {

    ROAD("ROAD", "陆运"),
    RALT("RALT", "铁运"),
    WATER("WATER", "水运"),
    PERSON("PERSON", "人送");


    private final String value;
    private final String text;

    public String getValue() {
        return value;
    }

    public String getText() {
        return text;
    }


    TransModeEnum(String value, String text) {
        this.value = value;
        this.text = text;
    }

    public static TransModeEnum getByValue(String value) {
        for (TransModeEnum temp : TransModeEnum.values()) {
            if (temp.getValue().equals(value)) {
                return temp;
            }
        }
        return null;
    }


}
