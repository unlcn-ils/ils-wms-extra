package com.unlcn.ils.wms.backend.service.inventorymanager;

import cn.huiyunche.commons.domain.ResultDTOWithPagination;
import com.unlcn.ils.wms.base.businessDTO.outbound.WmsOutboundTaskDTOForQuery;
import com.unlcn.ils.wms.base.dto.WmsInventoryManagerParamDTO;
import com.unlcn.ils.wms.base.dto.WmsInventoryManagerResultDTO;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;
import java.util.Map;

public interface WmsInventoryVerificationService {

    ResultDTOWithPagination<List<Map<String, Object>>> getInboundManageRecordsByCustomer(WmsInventoryManagerParamDTO dto) throws Exception;

    ResultDTOWithPagination<List<WmsOutboundTaskDTOForQuery>> getOutboundManageRecordsByCustomer(WmsInventoryManagerParamDTO dto) throws Exception;

    ResultDTOWithPagination<List<WmsInventoryManagerResultDTO>> getInventoryManageRecordsByCustomer(WmsInventoryManagerParamDTO dto) throws Exception;

    void getInboundManageRecordsByCustomerAndExport(WmsInventoryManagerParamDTO dto, HttpServletRequest request, HttpServletResponse response) throws Exception;

    void getOutboundManageRecordsByCustomerAndExport(WmsInventoryManagerParamDTO dto, HttpServletRequest request, HttpServletResponse response) throws Exception;

    void getInventoryManageRecordsByCustomerAndExport(WmsInventoryManagerParamDTO dto, HttpServletRequest request, HttpServletResponse response) throws Exception;
}
