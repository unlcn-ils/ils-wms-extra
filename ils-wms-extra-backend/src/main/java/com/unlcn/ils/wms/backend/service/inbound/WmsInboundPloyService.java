package com.unlcn.ils.wms.backend.service.inbound;

import com.unlcn.ils.wms.backend.bo.inboundBO.WmsInboundPloyBO;
import com.unlcn.ils.wms.backend.bo.inboundBO.WmsInboundPloyConditionBO;
import com.unlcn.ils.wms.backend.bo.inboundBO.WmsInboundPloyFlowtoBO;
import com.unlcn.ils.wms.backend.bo.inboundBO.WmsInboundPloyLocationBO;
import com.unlcn.ils.wms.base.model.inbound.WmsInboundPloy;
import com.unlcn.ils.wms.base.model.inbound.WmsInboundPloyCondition;
import com.unlcn.ils.wms.base.model.inbound.WmsInboundPloyFlowto;
import com.unlcn.ils.wms.base.model.inbound.WmsInboundPloyLocation;

import java.util.List;

/**
 * 策略增删改查
 * Created by DELL on 2017/9/1.
 */
public interface WmsInboundPloyService {

    /**
     * 查询主策略
     * @param whCode
     * @return
     */
    List<WmsInboundPloy> getPloyList(String whCode);

    /**
     * 新增策略
     * @param wmsInboundPloyBO
     */
    void addPloy(WmsInboundPloyBO wmsInboundPloyBO);

    /**
     * 更新策略
     * @param wmsInboundPloyBO
     */
    void updatePloy(WmsInboundPloyBO wmsInboundPloyBO);

    /**
     * 删除策略
     * @param wmsInboundPloyBOList
     */
    void deletePloy(List<WmsInboundPloyBO> wmsInboundPloyBOList);

    /**
     * 通过主策略code查询主条件
     * @param ployCode
     * @return
     */
    List<WmsInboundPloyCondition> getPloyConditionList(String ployCode);

    /**
     * 新增策略条件
     * @param wmsInboundPloyConditionBO
     */
    void addPloyCondition(List<WmsInboundPloyConditionBO> wmsInboundPloyConditionBO);

    /**
     * 删除策略条件
     * @param wmsInboundPloyConditionBO
     */
    void deletePloyCondition(List<WmsInboundPloyConditionBO> wmsInboundPloyConditionBO);

    /**
     * 通过主策略code查询流向数据
     * @param ployCode
     * @return
     */
    List<WmsInboundPloyFlowto> getPloyFlowtoList(String ployCode);

    /**
     * 新增流向数据
     * @param wmsInboundPloyFlowtoBO
     */
    void addPloyFlowto(List<WmsInboundPloyFlowtoBO> wmsInboundPloyFlowtoBO);

    /**
     * 删除流向数据
     * @param wmsInboundPloyFlowtoBO
     */
    void deletePloyFlowto(List<WmsInboundPloyFlowtoBO> wmsInboundPloyFlowtoBO);

    /**
     * 通过主策略CODE查询库区库位
     * @param ployCode
     * @return
     */
    List<WmsInboundPloyLocation> getPloyLocationList(String ployCode);

    /**
     * 新增库位数据
     * @param wmsInboundPloyLocationBO
     */
    void addPloyLocation(List<WmsInboundPloyLocationBO> wmsInboundPloyLocationBO);

    /**
     * 删除库位数据
     * @param wmsInboundPloyLocationBO
     */
    void deletePloyLocation(List<WmsInboundPloyLocationBO> wmsInboundPloyLocationBO);



}
