package com.unlcn.ils.wms.app.vo.tms;

import java.io.Serializable;

public class TmsInspectExcpVO implements Serializable {
    private Long inspectId;
    private String excpDetails;
    private String vin;
    private String userId;
    private Integer positionId;
    private String rpId;
    private String whCode;

    public String getWhCode() {
        return whCode;
    }

    public void setWhCode(String whCode) {
        this.whCode = whCode;
    }

    public String getRpId() {
        return rpId;
    }

    public void setRpId(String rpId) {
        this.rpId = rpId;
    }

    public Integer getPositionId() {
        return positionId;
    }

    public void setPositionId(Integer positionId) {
        this.positionId = positionId;
    }

    public Long getInspectId() {
        return inspectId;
    }

    public void setInspectId(Long inspectId) {
        this.inspectId = inspectId;
    }

    public String getExcpDetails() {
        return excpDetails;
    }

    public void setExcpDetails(String excpDetails) {
        this.excpDetails = excpDetails;
    }

    public String getVin() {
        return vin;
    }

    public void setVin(String vin) {
        this.vin = vin;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    @Override
    public String toString() {
        return "TmsInspectExcpVO{" +
                "inspectId=" + inspectId +
                ", excpDetails='" + excpDetails + '\'' +
                ", vin='" + vin + '\'' +
                ", userId='" + userId + '\'' +
                ", positionId=" + positionId +
                '}';
    }
}
