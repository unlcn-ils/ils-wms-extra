package com.unlcn.ils.wms.api.bi.quzrtz;


import cn.huiyunche.commons.exception.BusinessException;
import com.unlcn.ils.wms.backend.service.bigscreen.*;
import com.unlcn.ils.wms.backend.service.inbound.WmsInventoryService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

/**
 * 定时任务抓取tms数据
 */
@Component("biDataJobDetail")
public class BiDataJobDetail {

    private Logger logger = LoggerFactory.getLogger(BiDataJobDetail.class);

    @Autowired
    private PickDataService pickDataService;

    @Autowired
    private FrontShipmentService frontShipmentService;

    @Autowired
    private OverReasonService overReasonService;

    @Autowired
    private TransModelService transModelService;

    @Autowired
    private FrontOnWayService frontOnWayService;

    @Autowired
    private InOutDrayService inOutDrayService;

    @Autowired
    private DistributionService distributionService;

    @Autowired
    private WmsInventoryService wmsInventoryService;

    /**
     * 定时任务执行查找提车推移表
     *
     * @throws Exception
     */
    public void fetchTmsPickData() throws Exception {
        SimpleDateFormat format = new SimpleDateFormat("yyyy/MM/dd HH:mm:SS");
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(new Date());
        String dateStr = format.format(calendar.getTime());

        logger.info("fetchTmsPickData execute start : {}", dateStr);

        try {
            pickDataService.addPickData();
            calendar.setTime(new Date());
            dateStr = format.format(calendar.getTime());
            logger.info("fetchTmsPickData execute end : {}", dateStr);

        } catch (Exception e) {
            logger.error("fetchTmsPickData execute param {}", e);
            throw new BusinessException(e.getMessage());
        }
    }

    /**
     * 定时任务执行查找前端发运推移图
     *
     * @throws Exception
     */
    public void fetchTmsFrontShipment() throws Exception {
        SimpleDateFormat format = new SimpleDateFormat("yyyy/MM/dd HH:mm:SS");

        Calendar calendar = Calendar.getInstance();
        calendar.setTime(new Date());
        String dateStr = format.format(calendar.getTime());

        logger.info("fetchTmsFrontShipment execute start : {}", dateStr);

        try {
            frontShipmentService.addFrontShipmentData();
            calendar.setTime(new Date());
            dateStr = format.format(calendar.getTime());
            logger.info("fetchTmsFrontShipment execute end : {}", dateStr);
        } catch (Exception e) {
            logger.error("fetchTmsFrontShipment execute param {}", e);
            throw new BusinessException(e.getMessage());
        }
    }

    /**
     * 定时任务执行超期原因分析
     *
     * @throws Exception
     */
    public void fetchTmsOverReason() throws Exception {

        SimpleDateFormat format = new SimpleDateFormat("yyyy/MM/dd HH:mm:SS");
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(new Date());
        String dateStr = format.format(calendar.getTime());

        logger.info("fetchTmsOverReason execute start : {}", dateStr);

        try {
            overReasonService.addReasonInfo();
            calendar.setTime(new Date());
            dateStr = format.format(calendar.getTime());
            logger.info("fetchTmsOverReason execute end : {}", dateStr);
        } catch (Exception e) {
            logger.error("fetchTmsOverReason execute param {}", e);
            throw new BusinessException(e.getMessage());
        }
    }


    /**
     * 定时任务执行发运模式推移图
     *
     * @throws Exception
     */
    public void fetchTmsTransMode() throws Exception {
        SimpleDateFormat format = new SimpleDateFormat("yyyy/MM/dd HH:mm:SS");
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(new Date());
        String dateStr = format.format(calendar.getTime());

        logger.info("fetchTmsTransMode execute start : {}", dateStr);

        try {

            transModelService.addTransMode();
            calendar.setTime(new Date());
            dateStr = format.format(calendar.getTime());
            logger.info("fetchTmsTransMode execute end : {}", dateStr);
        } catch (Exception e) {
            logger.error("fetchTmsTransMode execute param {}", e);
            throw new BusinessException(e.getMessage());
        }
    }

    /**
     * 定时任务执行前端在途
     *
     * @throws Exception
     */
    public void fetchTmsFrontOnWay() throws Exception {
        SimpleDateFormat format = new SimpleDateFormat("yyyy/MM/dd HH:mm:SS");
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(new Date());
        String dateStr = format.format(calendar.getTime());

        logger.info("fetchTmsFrontOnWay execute start : {}", dateStr);

        try {
            frontOnWayService.addFrontOnWay();
            calendar.setTime(new Date());
            dateStr = format.format(calendar.getTime());
            logger.info("fetchTmsFrontOnWay execute end : {}", dateStr);
        } catch (Exception e) {
            logger.error("fetchTmsFrontOnWay execute param {}", e);
            throw new BusinessException(e.getMessage());
        }
    }

    /**
     * 定时任务执行板车进出
     *
     * @throws Exception
     */
    public void fetchTmsInOutDrayHead() throws Exception {
        SimpleDateFormat format = new SimpleDateFormat("yyyy/MM/dd HH:mm:SS");
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(new Date());
        String dateStr = format.format(calendar.getTime());

        logger.info("fetchTmsInOutDrayHead execute start : {}", dateStr);

        try {
            inOutDrayService.addDrayChart();
            calendar.setTime(new Date());
            dateStr = format.format(calendar.getTime());
            logger.info("fetchTmsInOutDrayHead execute end : {}", dateStr);
        } catch (Exception e) {
            logger.error("fetchTmsInOutDrayHead execute param {}", e);
            throw new BusinessException(e.getMessage());
        }
    }

    /**
     * 板车进出场
     * 定时任务执行板车列表
     *
     * @throws Exception
     */
    public void fetchTmsInOutDrayLine() throws Exception {
        SimpleDateFormat format = new SimpleDateFormat("yyyy/MM/dd HH:mm:SS");
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(new Date());
        String dateStr = format.format(calendar.getTime());

        logger.info("fetchTmsInOutDrayLine execute start : {}", dateStr);

        try {
            //以wms的出入场时间为准,并存入数据库的erp入场时间和发运时间字段
            inOutDrayService.addDrayLineInfoNew();
            //以tms的出入场时间为准,并存入数据库的erp入场时间和发运时间字段
            //inOutDrayService.addDrayLineInfo();
            calendar.setTime(new Date());
            dateStr = format.format(calendar.getTime());
            logger.info("fetchTmsInOutDrayLine execute end : {}", dateStr);
        } catch (Exception e) {
            logger.error("fetchTmsInOutDrayLine execute param {}", e);
            throw new BusinessException(e.getMessage());
        }
    }

    /**
     * 定时任务执行配送图
     *
     * @throws Exception
     */
    public void fetchTmsDributionHead() throws Exception {
        SimpleDateFormat format = new SimpleDateFormat("yyyy/MM/dd HH:mm:SS");
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(new Date());
        String dateStr = format.format(calendar.getTime());

        logger.info("fetchTmsDributionHead execute start : {}", dateStr);

        try {
            distributionService.addDistributionChart();
            calendar.setTime(new Date());
            dateStr = format.format(calendar.getTime());
            logger.info("fetchTmsDributionHead execute end : {}", dateStr);
        } catch (Exception e) {
            logger.error("fetchTmsDributionHead execute param {}", e);
            throw new BusinessException(e.getMessage());
        }
    }

    /**
     * 配送
     * 定时任务执行配送列表
     *
     * @throws Exception
     */
    public void fetchTmsDributionLine() throws Exception {
        SimpleDateFormat format = new SimpleDateFormat("yyyy/MM/dd HH:mm:SS");
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(new Date());
        String dateStr = format.format(calendar.getTime());

        logger.info("fetchTmsDributionLine execute start : {}", dateStr);

        try {
            distributionService.addDistributionLine();
            calendar.setTime(new Date());
            dateStr = format.format(calendar.getTime());
            logger.info("fetchTmsDributionLine execute end : {}", dateStr);
        } catch (Exception e) {
            logger.error("fetchTmsDributionLine execute param {}", e);
            throw new BusinessException(e.getMessage());
        }
    }

    /**
     * 保存每天进销推移图统计定时任务
     *
     * @throws Exception
     */
    public void addInOutInventoryChart() throws Exception {
        SimpleDateFormat format = new SimpleDateFormat("yyyy/MM/dd HH:mm:SS");
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(new Date());
        String dateStr = format.format(calendar.getTime());
        logger.info("addInOutInventoryChart execute start : {}", dateStr);

        try {
            wmsInventoryService.addinvoicingStatisticsChart();
            calendar.setTime(new Date());
            dateStr = format.format(calendar.getTime());
            logger.info("addInOutInventoryChart execute end : {}", dateStr);
        } catch (Exception e) {
            logger.error("addInOutInventoryChart execute param {}", e);
            throw new BusinessException(e.getMessage());
        }
    }
}
