package com.unlcn.ils.wms.backend.enums;

/**
 * @Auther linbao
 * @Date 2017-10-25
 */
public enum BorrowReturnEnum {
    NO_SEND(10, "供出"),
    SEND_ED(20, "已归还");

    private final Integer code;

    private final String value;

    public Integer getCode() {
        return code;
    }

    public String getValue() {
        return value;
    }

    BorrowReturnEnum(Integer code, String value) {
        this.code = code;
        this.value = value;
    }
}
