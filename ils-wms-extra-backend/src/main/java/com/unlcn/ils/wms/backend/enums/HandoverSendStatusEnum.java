package com.unlcn.ils.wms.backend.enums;

/**
 * @Auther linbao
 * @Date 2017-10-24
 * 交接单的发送状态
 */
public enum HandoverSendStatusEnum {

    NO_SEND("0", "未处理"),
    SEND_SUCCESS("1", "发送成功"),
    SEND_FAIL("2", "发送失败");

    private final String statusCode;

    private final String statusName;

    public String getStatusCode() {
        return statusCode;
    }

    public String getStatusName() {
        return statusName;
    }

    HandoverSendStatusEnum(String statusCode, String statusName) {
        this.statusCode = statusCode;
        this.statusName = statusName;
    }
}
