package com.unlcn.ils.wms.base.mapper.extmapper;

import org.apache.ibatis.annotations.Param;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public interface WmsInboundOrderExtMapper {

    List<Map<String, Object>> selectOrderInfoByOrderNoParam(Map<String, Object> paramMap);

    List<Map<String,Object>> selectOrderStatusByVinList(@Param("param") Map<String, Object> paramMap,@Param("vinList") ArrayList<String> newVinList);
}

