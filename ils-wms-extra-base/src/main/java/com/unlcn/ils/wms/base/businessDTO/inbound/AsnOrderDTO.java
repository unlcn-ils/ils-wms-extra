package com.unlcn.ils.wms.base.businessDTO.inbound;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

/**
 * Created by DELL on 2017/8/11.
 */
public class AsnOrderDTO implements Serializable {

    private String odSupplierId;//移库司机id

    private Integer inspectStatus; //验车状态
    private Date inspectTime; //验车时间
    private String inspectUserId; //验车用户id
    private String odOrderno;//订单号
    private String odBarcodekey;//barcode存储key
    private String odIsSpecial;//是否特殊
    private String odInCheckResult; //验车结果
    private String odInCheckDesc;//验车描述
    private Long odStyleId;//车型id
    private Integer odInspectExcpType;//异常类型关联id
    /**
     * 入库订单主ID
     */
    private Long odId;

    /**
     * 订单CODE,WMS专用
     */
    private String odCode;

    /**
     * 货主ID
     */
    private Long odCustomerId;

    /**
     * 货主CODE
     */
    private String odCustomerCode;

    /**
     * 货主名称
     */
    private String odCustomerName;

    /**
     * 调度单号
     */
    private String odDispatchNo;

    /**
     * 运单号
     */
    private String odWaybillNo;

    /**
     * 运单类型
     */
    private String odWaybillType;

    /**
     * 订单状态
     */
    private String odStatus;

    /**
     * 提货地址
     */
    private String odPickUpAddr;

    /**
     * 仓库ID
     */
    private Long odWhId;

    /**
     * 仓库CODE
     */
    private String odWhCode;

    /**
     * 仓库NAME
     */
    private String odWhName;

    /**
     * 订单商品数量
     */
    private Long odInboundNum;

    /**
     * 交货方
     */
    private String odSupplierParty;

    /**
     * 交货方CODE
     */
    private String odSupplierCode;

    /**
     * 交货方名称
     */
    private String odSupplierName;

    /**
     * 交货方联系人手机号
     */
    private String odSupplierPhone;

    /**
     * 收货人ID
     */
    private String odConsigneeId;

    /**
     * 收货人CODE
     */
    private String odConsigneeCode;

    /**
     * 收货人名称
     */
    private String odConsigneeName;

    /**
     * 收货时间
     */
    private Date odConsigneeDate;

    /**
     * 检验结果
     */
    private String odCheckResult;

    /**
     * 检验描述
     */
    private String odCheckDesc;

    /**
     * 目的地
     */
    private String odDestCode;

    /**
     * 起始地
     */
    private String odOriginCode;

    /**
     * 目的地
     */
    private String odDest;

    /**
     * 起始地
     */
    private String odOrigin;

    /**
     * 备注
     */
    private String odRemark;

    private String createUserId;

    private String createUserName;

    private String modifyUserId;

    private String modifyUserName;

    private Date gmtCreate;

    private Date gmtUpdate;

    private Byte isDeleted;

    private Long versions;

    /**
     * 入库类型
     */
    private String odBusinessType;

    /**
     * 维修方
     */
    private String odRepairParty;

    private List<AsnOrderDetailDTO> asnOrderDetailDTOList = null;

    private String odCustomerOrderNo;

    private String odTaskNo;

    private Integer confirmInboundUserId;


    public Date getInspectTime() {
        return inspectTime;
    }

    public void setInspectTime(Date inspectTime) {
        this.inspectTime = inspectTime;
    }

    public String getInspectUserId() {
        return inspectUserId;
    }

    public void setInspectUserId(String inspectUserId) {
        this.inspectUserId = inspectUserId;
    }

    public String getOdOrderno() {
        return odOrderno;
    }

    public void setOdOrderno(String odOrderno) {
        this.odOrderno = odOrderno;
    }

    public String getOdBarcodekey() {
        return odBarcodekey;
    }

    public void setOdBarcodekey(String odBarcodekey) {
        this.odBarcodekey = odBarcodekey;
    }

    public String getOdIsSpecial() {
        return odIsSpecial;
    }

    public void setOdIsSpecial(String odIsSpecial) {
        this.odIsSpecial = odIsSpecial;
    }

    public String getOdInCheckResult() {
        return odInCheckResult;
    }

    public void setOdInCheckResult(String odInCheckResult) {
        this.odInCheckResult = odInCheckResult;
    }

    public String getOdInCheckDesc() {
        return odInCheckDesc;
    }

    public void setOdInCheckDesc(String odInCheckDesc) {
        this.odInCheckDesc = odInCheckDesc;
    }

    public Long getOdStyleId() {
        return odStyleId;
    }

    public void setOdStyleId(Long odStyleId) {
        this.odStyleId = odStyleId;
    }

    public Integer getOdInspectExcpType() {
        return odInspectExcpType;
    }

    public void setOdInspectExcpType(Integer odInspectExcpType) {
        this.odInspectExcpType = odInspectExcpType;
    }

    public Integer getInspectStatus() {
        return inspectStatus;
    }

    public void setInspectStatus(Integer inspectStatus) {
        this.inspectStatus = inspectStatus;
    }

    public Integer getConfirmInboundUserId() {
        return confirmInboundUserId;
    }

    public void setConfirmInboundUserId(Integer confirmInboundUserId) {
        this.confirmInboundUserId = confirmInboundUserId;
    }

    public String getOdSupplierId() {
        return odSupplierId;
    }

    public void setOdSupplierId(String odSupplierId) {
        this.odSupplierId = odSupplierId;
    }

    public Long getOdId() {
        return odId;
    }

    public void setOdId(Long odId) {
        this.odId = odId;
    }

    public String getOdCode() {
        return odCode;
    }

    public void setOdCode(String odCode) {
        this.odCode = odCode;
    }

    public Long getOdCustomerId() {
        return odCustomerId;
    }

    public void setOdCustomerId(Long odCustomerId) {
        this.odCustomerId = odCustomerId;
    }

    public String getOdCustomerCode() {
        return odCustomerCode;
    }

    public void setOdCustomerCode(String odCustomerCode) {
        this.odCustomerCode = odCustomerCode;
    }

    public String getOdCustomerName() {
        return odCustomerName;
    }

    public void setOdCustomerName(String odCustomerName) {
        this.odCustomerName = odCustomerName;
    }

    public String getOdDispatchNo() {
        return odDispatchNo;
    }

    public void setOdDispatchNo(String odDispatchNo) {
        this.odDispatchNo = odDispatchNo;
    }

    public String getOdWaybillNo() {
        return odWaybillNo;
    }

    public void setOdWaybillNo(String odWaybillNo) {
        this.odWaybillNo = odWaybillNo;
    }

    public String getOdWaybillType() {
        return odWaybillType;
    }

    public void setOdWaybillType(String odWaybillType) {
        this.odWaybillType = odWaybillType;
    }

    public String getOdStatus() {
        return odStatus;
    }

    public void setOdStatus(String odStatus) {
        this.odStatus = odStatus;
    }

    public String getOdPickUpAddr() {
        return odPickUpAddr;
    }

    public void setOdPickUpAddr(String odPickUpAddr) {
        this.odPickUpAddr = odPickUpAddr;
    }

    public Long getOdWhId() {
        return odWhId;
    }

    public void setOdWhId(Long odWhId) {
        this.odWhId = odWhId;
    }

    public String getOdWhCode() {
        return odWhCode;
    }

    public void setOdWhCode(String odWhCode) {
        this.odWhCode = odWhCode;
    }

    public Long getOdInboundNum() {
        return odInboundNum;
    }

    public void setOdInboundNum(Long odInboundNum) {
        this.odInboundNum = odInboundNum;
    }

    public String getOdSupplierParty() {
        return odSupplierParty;
    }

    public void setOdSupplierParty(String odSupplierParty) {
        this.odSupplierParty = odSupplierParty;
    }

    public String getOdSupplierCode() {
        return odSupplierCode;
    }

    public void setOdSupplierCode(String odSupplierCode) {
        this.odSupplierCode = odSupplierCode;
    }

    public String getOdSupplierName() {
        return odSupplierName;
    }

    public void setOdSupplierName(String odSupplierName) {
        this.odSupplierName = odSupplierName;
    }

    public String getOdSupplierPhone() {
        return odSupplierPhone;
    }

    public void setOdSupplierPhone(String odSupplierPhone) {
        this.odSupplierPhone = odSupplierPhone;
    }

    public String getOdConsigneeId() {
        return odConsigneeId;
    }

    public void setOdConsigneeId(String odConsigneeId) {
        this.odConsigneeId = odConsigneeId;
    }

    public String getOdConsigneeCode() {
        return odConsigneeCode;
    }

    public void setOdConsigneeCode(String odConsigneeCode) {
        this.odConsigneeCode = odConsigneeCode;
    }

    public String getOdConsigneeName() {
        return odConsigneeName;
    }

    public void setOdConsigneeName(String odConsigneeName) {
        this.odConsigneeName = odConsigneeName;
    }

    public Date getOdConsigneeDate() {
        return odConsigneeDate;
    }

    public void setOdConsigneeDate(Date odConsigneeDate) {
        this.odConsigneeDate = odConsigneeDate;
    }

    public String getOdCheckResult() {
        return odCheckResult;
    }

    public void setOdCheckResult(String odCheckResult) {
        this.odCheckResult = odCheckResult;
    }

    public String getOdCheckDesc() {
        return odCheckDesc;
    }

    public void setOdCheckDesc(String odCheckDesc) {
        this.odCheckDesc = odCheckDesc;
    }

    public String getOdDest() {
        return odDest;
    }

    public void setOdDest(String odDest) {
        this.odDest = odDest;
    }

    public String getOdOrigin() {
        return odOrigin;
    }

    public void setOdOrigin(String odOrigin) {
        this.odOrigin = odOrigin;
    }

    public String getOdRemark() {
        return odRemark;
    }

    public void setOdRemark(String odRemark) {
        this.odRemark = odRemark;
    }

    public String getCreateUserId() {
        return createUserId;
    }

    public void setCreateUserId(String createUserId) {
        this.createUserId = createUserId;
    }

    public String getCreateUserName() {
        return createUserName;
    }

    public void setCreateUserName(String createUserName) {
        this.createUserName = createUserName;
    }

    public String getModifyUserId() {
        return modifyUserId;
    }

    public void setModifyUserId(String modifyUserId) {
        this.modifyUserId = modifyUserId;
    }

    public String getModifyUserName() {
        return modifyUserName;
    }

    public void setModifyUserName(String modifyUserName) {
        this.modifyUserName = modifyUserName;
    }

    public Date getGmtCreate() {
        return gmtCreate;
    }

    public void setGmtCreate(Date gmtCreate) {
        this.gmtCreate = gmtCreate;
    }

    public Date getGmtUpdate() {
        return gmtUpdate;
    }

    public void setGmtUpdate(Date gmtUpdate) {
        this.gmtUpdate = gmtUpdate;
    }

    public Byte getIsDeleted() {
        return isDeleted;
    }

    public void setIsDeleted(Byte isDeleted) {
        this.isDeleted = isDeleted;
    }

    public Long getVersions() {
        return versions;
    }

    public void setVersions(Long versions) {
        this.versions = versions;
    }

    public String getOdRepairParty() {
        return odRepairParty;
    }

    public void setOdRepairParty(String odRepairParty) {
        this.odRepairParty = odRepairParty;
    }

    public List<AsnOrderDetailDTO> getAsnOrderDetailDTOList() {
        return asnOrderDetailDTOList;
    }

    public void setAsnOrderDetailDTOList(List<AsnOrderDetailDTO> asnOrderDetailDTOList) {
        this.asnOrderDetailDTOList = asnOrderDetailDTOList;
    }

    public String getOdDestCode() {
        return odDestCode;
    }

    public void setOdDestCode(String odDestCode) {
        this.odDestCode = odDestCode;
    }

    public String getOdOriginCode() {
        return odOriginCode;
    }

    public void setOdOriginCode(String odOriginCode) {
        this.odOriginCode = odOriginCode;
    }

    public String getOdCustomerOrderNo() {
        return odCustomerOrderNo;
    }

    public void setOdCustomerOrderNo(String odCustomerOrderNo) {
        this.odCustomerOrderNo = odCustomerOrderNo;
    }

    public String getOdTaskNo() {
        return odTaskNo;
    }

    public void setOdTaskNo(String odTaskNo) {
        this.odTaskNo = odTaskNo;
    }

    public String getOdWhName() {
        return odWhName;
    }

    public void setOdWhName(String odWhName) {
        this.odWhName = odWhName;
    }

    public String getOdBusinessType() {
        return odBusinessType;
    }

    public void setOdBusinessType(String odBusinessType) {
        this.odBusinessType = odBusinessType;
    }
}
