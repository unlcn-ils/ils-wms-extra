package com.unlcn.ils.wms.backend.bo.pickup;

/**
 * Created by zhiche024 on 2017/7/25.
 */
public class TmsRepairPortBO {
    private Long pickId;
    private Long repairTimeId;
    private String userId;
    private Integer checkStatus;


    public Long getRepairTimeId() {
        return repairTimeId;
    }

    public void setRepairTimeId(Long repairTimeId) {
        this.repairTimeId = repairTimeId;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public Integer getCheckStatus() {
        return checkStatus;
    }

    public void setCheckStatus(Integer checkStatus) {
        this.checkStatus = checkStatus;
    }

    public Long getPickId() {
        return pickId;
    }

    public void setPickId(Long pickId) {
        this.pickId = pickId;
    }

    @Override
    public String toString() {
        return "TmsRepairPortVO{" +
                "pickId=" + pickId +
                ", repairTimeId=" + repairTimeId +
                ", userId='" + userId + '\'' +
                ", checkStatus=" + checkStatus +
                '}';
    }
}
