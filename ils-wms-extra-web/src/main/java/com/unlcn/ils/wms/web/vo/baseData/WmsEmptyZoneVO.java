package com.unlcn.ils.wms.web.vo.baseData;

/**
 * Created by DELL on 2017/8/18.
 */
public class WmsEmptyZoneVO {
    private String locZoneId;
    private String locZoneCode;


    public String getLocZoneId() {
        return locZoneId;
    }

    public void setLocZoneId(String locZoneId) {
        this.locZoneId = locZoneId;
    }

    public String getLocZoneCode() {
        return locZoneCode;
    }

    public void setLocZoneCode(String locZoneCode) {
        this.locZoneCode = locZoneCode;
    }

    @Override
    public String toString() {
        return "WmsEmptyZoneVO{" +
                "locZoneId='" + locZoneId + '\'' +
                ", locZoneCode='" + locZoneCode + '\'' +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        WmsEmptyZoneVO wmsEmptyZoneVO = (WmsEmptyZoneVO) o;

        if (!locZoneId.equals(wmsEmptyZoneVO.locZoneId)) return false;
        return locZoneId.equals(wmsEmptyZoneVO.locZoneId);

    }

    @Override
    public int hashCode() {
        int result = locZoneId.hashCode();
        result = 31 * result + locZoneId.hashCode();
        return result;
    }
}
