package com.unlcn.ils.wms.base.dto;

import java.io.Serializable;


public class WmsInventoryChangeLocCodeDTO implements Serializable {
    private String invlocId;//更新库存的状态为
    private String whCode;
    private String invlocVin;
    private String invlocCodeNew;


    public String getInvlocCodeNew() {
        return invlocCodeNew;
    }

    public void setInvlocCodeNew(String invlocCodeNew) {
        this.invlocCodeNew = invlocCodeNew;
    }

    public String getInvlocVin() {
        return invlocVin;
    }

    public void setInvlocVin(String invlocVin) {
        this.invlocVin = invlocVin;
    }

    public String getInvlocId() {
        return invlocId;
    }

    public void setInvlocId(String invlocId) {
        this.invlocId = invlocId;
    }

    public String getWhCode() {
        return whCode;
    }

    public void setWhCode(String whCode) {
        this.whCode = whCode;
    }
}
