/**
 * Created by houjianhui on 2017/7/18.
 */

'use strict';

angular.module('kas.services', [])

// 全局服务请求service. 调用时请调用此服务, 便于全局管理
    .service('HttpService', ['$http', '$q', '$state', 'Settings', function($http, $q, $state, Settings) {
        return{
            post:function(url, params){
                var token = Tools.getToken();
                var deferred = $q.defer();
                return $http.post(Settings.Context.path + url, params, {
                    headers: {
                        'Authorization': token
                    }
                })
                    .success(function(data, status, headers, config){
                        deferred.resolve(data, status, headers, config);
                    })
                    .error(function(data, status, headers, config){
                        deferred.reject(data, status, headers, config);
                    })
                return deferred.promise;
            },
            get:function(url){
                var token = Tools.getToken();
                var deferred = $q.defer();
                return $http.get(Settings.Context.path + url, {
                    headers: {
                        'Authorization': token
                    }
                })
                    .success(function(data, status, headers, config){
                        deferred.resolve(data, status, headers, config);
                    })
                    .error(function(data, status, headers, config){
                        deferred.reject(data, status, headers, config);
                    })
                return deferred.promise;
            }
        };

    }])

    .service('WaybillService', ['HttpService', function(HttpService) {
        var url = '/otd';
        return {
            getWaybillCode: function(params) {
                var action = '/getWaybillCode?waybillCode=' + params;
                return HttpService.get(url + action);
            }
        };
    }])
    .filter('trustHtml', ['$sce', function ($sce) {
        return function (input) {
            return $sce.trustAsHtml(input);
        }
    }]);