package com.unlcn.ils.wms.base.mapper.extmapper;

import com.unlcn.ils.wms.base.model.inbound.WmsMailConfig;
import com.unlcn.ils.wms.base.model.inbound.WmsMailConfigExample;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

public interface WmsMailConfigExtMapper {
    /**
     * 根据参数查询邮件配置信息
     * @param paramsMap
     * @return
     */
    List<WmsMailConfig> findParams(@Param("paramsMap") Map<String, Object> paramsMap);
}