package com.unlcn.ils.wms.base.mapper.extmapper;

import com.unlcn.ils.wms.base.model.bigscreen.BiDistributionHead;
import com.unlcn.ils.wms.base.model.bigscreen.BiDistributionHeadExample;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface BiDistributionHeadExtMapper {

    /**
     * 配送推移图统计
     * @return
     */
    List<BiDistributionHead> distributionChartCount();
}