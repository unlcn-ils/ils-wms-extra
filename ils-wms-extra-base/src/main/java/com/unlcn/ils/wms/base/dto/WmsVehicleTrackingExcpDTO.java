package com.unlcn.ils.wms.base.dto;

import java.io.Serializable;
import java.util.Date;

/**
 * 订单跟踪界面的异常参数封装对象
 */
public class WmsVehicleTrackingExcpDTO implements Serializable {
    private String excpDesc;
    private Date inspectTime;
    private String vin;
    private String position;
    private String body;
    private String hurt;

    public String getVin() {
        return vin;
    }

    public void setVin(String vin) {
        this.vin = vin;
    }

    public String getPosition() {
        return position;
    }

    public void setPosition(String position) {
        this.position = position;
    }

    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        this.body = body;
    }

    public String getHurt() {
        return hurt;
    }

    public void setHurt(String hurt) {
        this.hurt = hurt;
    }

    public String getExcpDesc() {
        return excpDesc;
    }

    public void setExcpDesc(String excpDesc) {
        this.excpDesc = excpDesc;
    }

    public Date getInspectTime() {
        return inspectTime;
    }

    public void setInspectTime(Date inspectTime) {
        this.inspectTime = inspectTime;
    }
}
