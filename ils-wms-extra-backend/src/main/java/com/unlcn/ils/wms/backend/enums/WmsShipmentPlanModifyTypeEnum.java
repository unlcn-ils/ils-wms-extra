package com.unlcn.ils.wms.backend.enums;

public enum WmsShipmentPlanModifyTypeEnum {
    //10-正常修改 20-修改删除  30-修改新增 40-数据未修改
    NORMAL("10", "正常修改"),
    DELETE("20", "修改删除"),
    ADD("30", "修改新增"),
    NOT_CHANGE("40", "数据未修改");

    private final String value;
    private final String text;

    WmsShipmentPlanModifyTypeEnum(String value, String text) {
        this.value = value;
        this.text = text;
    }

    /**
     * @return the value
     */
    public String getValue() {
        return value;
    }

    /**
     * @return the text
     */
    public String getText() {
        return text;
    }

    public static WmsShipmentPlanModifyTypeEnum getByValue(String value) {
        for (WmsShipmentPlanModifyTypeEnum temp : WmsShipmentPlanModifyTypeEnum.values()) {
            if (temp.getValue().equals(value)) {
                return temp;
            }
        }
        return null;
    }
}
