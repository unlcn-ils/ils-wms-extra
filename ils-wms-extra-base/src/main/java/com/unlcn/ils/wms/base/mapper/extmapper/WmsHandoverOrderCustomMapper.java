package com.unlcn.ils.wms.base.mapper.extmapper;

import com.unlcn.ils.wms.base.dto.WmsHandoverOrderOperationListResultDTO;
import com.unlcn.ils.wms.base.model.junmadcs.WmsHandoverOrder;
import com.unlcn.ils.wms.base.model.junmadcs.WmsHandoverOrderExcp;
import org.apache.ibatis.annotations.Param;

import java.util.HashMap;
import java.util.List;

/**
 * @Auther linbao
 * @Date 2017-10-23
 */
public interface WmsHandoverOrderCustomMapper {

    /**
     * 批量插入
     *
     * @param handoverOrderList
     * @throws Exception
     */
    void insertForBatch(@Param("handoverOrderList") List<WmsHandoverOrder> handoverOrderList);

    String selectDcsGroupByGBNO();

    String selectGroupByGBNODcsExcp();

    List<WmsHandoverOrderExcp> selectExcpHandoverOrderForCRM(HashMap<String, Object> params);

    List<WmsHandoverOrder> selectHandoverOrderForCRM(HashMap<String, Object> params);

    List<WmsHandoverOrderOperationListResultDTO> selectOperationListByParam(HashMap<String, Object> params);

    int countOperationRecordsByParam(HashMap<String, Object> params);

    List<WmsHandoverOrderExcp> selectExcpListByDataIds(List<String> ids);

    void updateSendTODCSAgainByParam(@Param("params") HashMap<String, Object> params);

    void updateSendTOSAPAgainByParam(@Param("params") HashMap<String, Object> params);

    void updateSendTOCRMAgainByParam(@Param("params") HashMap<String, Object> params);

    List<WmsHandoverOrder> selectOrderListByIds(List<String> ids);

    void updateSendDCSResultSuccess(@Param("params") HashMap<String, Object> params);

    void updateSendSAPResultSuccess(@Param("params") HashMap<String, Object> params);

    void updateSendCRMResultSuccess(@Param("params") HashMap<String, Object> params);

    String selectSapGroupByGBNO();

    String selectGroupByGBNOSapExcp();
}
