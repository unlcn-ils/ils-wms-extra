package com.unlcn.ils.wms.backend.bo.outboundBO;

/**
 * <p>
 * 2018-3-16 君马条件查询 增加物料名称字段条件查询
 * </p>
 */
public class WmsOutboundRecordListBO extends WmsOutboundTaskFormBO {
    private String inventoryConfiguration;
    private String otBlId;
    private String materialCode;
    private String spCarrier;
    private String spDealer;


    public String getSpDealer() {
        return spDealer;
    }

    public void setSpDealer(String spDealer) {
        this.spDealer = spDealer;
    }

    public String getSpCarrier() {
        return spCarrier;
    }

    public void setSpCarrier(String spCarrier) {
        this.spCarrier = spCarrier;
    }

    public String getMaterialCode() {
        return materialCode;
    }

    public void setMaterialCode(String materialCode) {
        this.materialCode = materialCode;
    }

    public String getOtBlId() {
        return otBlId;
    }

    public void setOtBlId(String otBlId) {
        this.otBlId = otBlId;
    }

    public String getInventoryConfiguration() {
        return inventoryConfiguration;
    }

    public void setInventoryConfiguration(String inventoryConfiguration) {
        this.inventoryConfiguration = inventoryConfiguration;
    }
}
