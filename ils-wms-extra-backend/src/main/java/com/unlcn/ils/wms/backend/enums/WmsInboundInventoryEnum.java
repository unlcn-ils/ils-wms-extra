package com.unlcn.ils.wms.backend.enums;

public enum WmsInboundInventoryEnum {
    INIT(10, "待入库"),
    MOVEMENT(20, "已入库");

    private final int value;
    private final String text;

    WmsInboundInventoryEnum(int value, String text) {
        this.value = value;
        this.text = text;
    }

    public int getValue() {
        return value;
    }

    public String getText() {
        return text;
    }

    public static WmsInboundInventoryEnum getByValue(int value){
        for (WmsInboundInventoryEnum temp : WmsInboundInventoryEnum.values()) {
            if(temp.getValue() == value){
                return temp;
            }
        }
        return null;
    }
}
