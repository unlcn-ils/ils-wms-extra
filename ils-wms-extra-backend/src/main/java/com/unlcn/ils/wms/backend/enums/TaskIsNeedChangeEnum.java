package com.unlcn.ils.wms.backend.enums;

/**
 * @Auther linbao
 * @Date 2017-10-28
 */
public enum TaskIsNeedChangeEnum {
    NO_CHANGE("10", "未换车"),
    CHANGE_ED("20", "已换车"),
    NOT_CAR_TO_CHANGE("30", "无可可换");

    private final String code;

    private final String desc;

    public String getCode() {
        return code;
    }

    public String getDesc() {
        return desc;
    }

    TaskIsNeedChangeEnum(String code, String desc) {
        this.code = code;
        this.desc = desc;
    }
}
