package com.unlcn.ils.wms.app.vo.wms;

import java.util.Date;
import java.util.List;

/**
 * 
 * @ClassName: MenuVO 
 * @Description: 菜单VO
 * @author laishijian
 * @date 2017年8月9日 上午9:44:19 
 *
 */
public class MenuVO {
	
	//id
	private Integer id;
	//菜单标题
	private String title;
	//父级菜单id
	private Integer parentId;
	//级别
	private String level;
	//权限id
	private Integer permissionsId;
	//状态
	private String enable;
	//创建人
	private String createPerson;
	//更新人
	private String updatePerson;
	//创建时间
	private Date gmtCreate;
	//修改时间
	private Date gmtModified;
	//权限VO
	private PermissionsVO permissionsVO;
	//菜单VO
	private List<MenuVO> childMenuVOList;
	
	/**
	 * @return the id
	 */
	public Integer getId() {
		return id;
	}
	/**
	 * @param id the id to set
	 */
	public void setId(Integer id) {
		this.id = id;
	}
	/**
	 * @return the title
	 */
	public String getTitle() {
		return title;
	}
	/**
	 * @param title the title to set
	 */
	public void setTitle(String title) {
		this.title = title;
	}
	/**
	 * @return the parentId
	 */
	public Integer getParentId() {
		return parentId;
	}
	/**
	 * @param parentId the parentId to set
	 */
	public void setParentId(Integer parentId) {
		this.parentId = parentId;
	}
	/**
	 * @return the level
	 */
	public String getLevel() {
		return level;
	}
	/**
	 * @param level the level to set
	 */
	public void setLevel(String level) {
		this.level = level;
	}
	/**
	 * @return the permissionsId
	 */
	public Integer getPermissionsId() {
		return permissionsId;
	}
	/**
	 * @param permissionsId the permissionsId to set
	 */
	public void setPermissionsId(Integer permissionsId) {
		this.permissionsId = permissionsId;
	}
	/**
	 * @return the enable
	 */
	public String getEnable() {
		return enable;
	}
	/**
	 * @param enable the enable to set
	 */
	public void setEnable(String enable) {
		this.enable = enable;
	}
	/**
	 * @return the createPerson
	 */
	public String getCreatePerson() {
		return createPerson;
	}
	/**
	 * @param createPerson the createPerson to set
	 */
	public void setCreatePerson(String createPerson) {
		this.createPerson = createPerson;
	}
	/**
	 * @return the updatePerson
	 */
	public String getUpdatePerson() {
		return updatePerson;
	}
	/**
	 * @param updatePerson the updatePerson to set
	 */
	public void setUpdatePerson(String updatePerson) {
		this.updatePerson = updatePerson;
	}
	/**
	 * @return the gmtCreate
	 */
	public Date getGmtCreate() {
		return gmtCreate;
	}
	/**
	 * @param gmtCreate the gmtCreate to set
	 */
	public void setGmtCreate(Date gmtCreate) {
		this.gmtCreate = gmtCreate;
	}
	/**
	 * @return the gmtModified
	 */
	public Date getGmtModified() {
		return gmtModified;
	}
	/**
	 * @param gmtModified the gmtModified to set
	 */
	public void setGmtModified(Date gmtModified) {
		this.gmtModified = gmtModified;
	}
	/**
	 * @return the permissionsVO
	 */
	public PermissionsVO getPermissionsVO() {
		return permissionsVO;
	}
	/**
	 * @param permissionsVO the permissionsVO to set
	 */
	public void setPermissionsVO(PermissionsVO permissionsVO) {
		this.permissionsVO = permissionsVO;
	}
	/**
	 * @return the childMenuVOList
	 */
	public List<MenuVO> getChildMenuVOList() {
		return childMenuVOList;
	}
	/**
	 * @param childMenuVOList the childMenuVOList to set
	 */
	public void setChildMenuVOList(List<MenuVO> childMenuVOList) {
		this.childMenuVOList = childMenuVOList;
	}
	
	/*
	 * Title: toString
	 * Description: toString
	 * @return
	 * @see java.lang.Object#toString()
	 *
	 */
	@Override
	public String toString() {
		return "MenuVO [id=" + id + ", title=" + title + ", parentId=" + parentId + ", level=" + level
				+ ", permissionsId=" + permissionsId + ", enable=" + enable + ", createPerson=" + createPerson
				+ ", updatePerson=" + updatePerson + ", gmtCreate=" + gmtCreate + ", gmtModified=" + gmtModified + "]";
	}
	
}
