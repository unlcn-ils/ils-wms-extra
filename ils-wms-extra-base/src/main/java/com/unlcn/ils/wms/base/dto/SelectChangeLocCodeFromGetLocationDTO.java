package com.unlcn.ils.wms.base.dto;

import java.io.Serializable;

public class SelectChangeLocCodeFromGetLocationDTO implements Serializable {
    private String locId;
    private String locWhId;
    private String locWhCode;
    private String locZoneId;
    private String locZoneCode;
    private String locZoneName;
    private String locEnableFlag;
    private String isDeleted;
    private String usefulLocCode;

    public String getLocId() {
        return locId;
    }

    public void setLocId(String locId) {
        this.locId = locId;
    }

    public String getLocWhId() {
        return locWhId;
    }

    public void setLocWhId(String locWhId) {
        this.locWhId = locWhId;
    }

    public String getLocWhCode() {
        return locWhCode;
    }

    public void setLocWhCode(String locWhCode) {
        this.locWhCode = locWhCode;
    }

    public String getLocZoneId() {
        return locZoneId;
    }

    public void setLocZoneId(String locZoneId) {
        this.locZoneId = locZoneId;
    }

    public String getLocZoneCode() {
        return locZoneCode;
    }

    public void setLocZoneCode(String locZoneCode) {
        this.locZoneCode = locZoneCode;
    }

    public String getLocZoneName() {
        return locZoneName;
    }

    public void setLocZoneName(String locZoneName) {
        this.locZoneName = locZoneName;
    }

    public String getLocEnableFlag() {
        return locEnableFlag;
    }

    public void setLocEnableFlag(String locEnableFlag) {
        this.locEnableFlag = locEnableFlag;
    }

    public String getIsDeleted() {
        return isDeleted;
    }

    public void setIsDeleted(String isDeleted) {
        this.isDeleted = isDeleted;
    }

    public String getUsefulLocCode() {
        return usefulLocCode;
    }

    public void setUsefulLocCode(String usefulLocCode) {
        this.usefulLocCode = usefulLocCode;
    }
}
