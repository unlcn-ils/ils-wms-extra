package com.unlcn.ils.wms.base.dto;

import java.io.Serializable;

public class WmsVehicleTrackingDTO implements Serializable {
    private String custshipno;
    private String orderno;
    private String custorderno;
    private String vin;
    private String order_date;
    private String supplier;
    private String driver;
    private String assign_date;
    private String sap_out_date;
    private String erp_load_date;
    private String erp_out_date;
    private String erp_arrival_date;
    private String sap_receive_date;
    private String dealer;
    private String inboundTime;
    private String outbounTime;
    private String mobile;
    private String dealer_mobile;

    public String getDealer_mobile() {
        return dealer_mobile;
    }

    public void setDealer_mobile(String dealer_mobile) {
        this.dealer_mobile = dealer_mobile;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getInboundTime() {
        return inboundTime;
    }

    public void setInboundTime(String inboundTime) {
        this.inboundTime = inboundTime;
    }

    public String getOutbounTime() {
        return outbounTime;
    }

    public void setOutbounTime(String outbounTime) {
        this.outbounTime = outbounTime;
    }

    public String getCustshipno() {
        return custshipno;
    }

    public void setCustshipno(String custshipno) {
        this.custshipno = custshipno;
    }

    public String getOrderno() {
        return orderno;
    }

    public void setOrderno(String orderno) {
        this.orderno = orderno;
    }

    public String getCustorderno() {
        return custorderno;
    }

    public void setCustorderno(String custorderno) {
        this.custorderno = custorderno;
    }

    public String getVin() {
        return vin;
    }

    public void setVin(String vin) {
        this.vin = vin;
    }

    public String getOrder_date() {
        return order_date;
    }

    public void setOrder_date(String order_date) {
        this.order_date = order_date;
    }

    public String getSupplier() {
        return supplier;
    }

    public void setSupplier(String supplier) {
        this.supplier = supplier;
    }

    public String getDriver() {
        return driver;
    }

    public void setDriver(String driver) {
        this.driver = driver;
    }

    public String getAssign_date() {
        return assign_date;
    }

    public void setAssign_date(String assign_date) {
        this.assign_date = assign_date;
    }

    public String getSap_out_date() {
        return sap_out_date;
    }

    public void setSap_out_date(String sap_out_date) {
        this.sap_out_date = sap_out_date;
    }

    public String getErp_load_date() {
        return erp_load_date;
    }

    public void setErp_load_date(String erp_load_date) {
        this.erp_load_date = erp_load_date;
    }

    public String getErp_out_date() {
        return erp_out_date;
    }

    public void setErp_out_date(String erp_out_date) {
        this.erp_out_date = erp_out_date;
    }

    public String getErp_arrival_date() {
        return erp_arrival_date;
    }

    public void setErp_arrival_date(String erp_arrival_date) {
        this.erp_arrival_date = erp_arrival_date;
    }

    public String getSap_receive_date() {
        return sap_receive_date;
    }

    public void setSap_receive_date(String sap_receive_date) {
        this.sap_receive_date = sap_receive_date;
    }

    public String getDealer() {
        return dealer;
    }

    public void setDealer(String dealer) {
        this.dealer = dealer;
    }
}
