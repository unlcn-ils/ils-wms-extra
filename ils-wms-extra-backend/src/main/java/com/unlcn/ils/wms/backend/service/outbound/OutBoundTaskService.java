package com.unlcn.ils.wms.backend.service.outbound;

import cn.huiyunche.commons.domain.PageVo;
import com.unlcn.ils.wms.backend.bo.outboundBO.*;
import com.unlcn.ils.wms.backend.dto.outboundDTO.CurrentWmsOutboundTaskListDTO;
import com.unlcn.ils.wms.backend.dto.outboundDTO.HandoverDTO;
import com.unlcn.ils.wms.backend.dto.outboundDTO.WmsPreparationPlanDTO;
import com.unlcn.ils.wms.base.businessDTO.outbound.WmsOutboundTaskDTO;
import com.unlcn.ils.wms.base.dto.WmsOutboundImportExcelDTO;
import com.unlcn.ils.wms.base.dto.WmsOutboundResultDTO;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;
import java.util.Map;

/**
 * @Author ：Ligl
 * @Date : 2017/9/26.
 */
public interface OutBoundTaskService {

    /**
     * 查询出库任务(备料任务)司机抢单
     */
    List<WmsPreparationPlanDTO> getOutboundTaskList(PageVo pageVo, WmsOutboundTaskBO wmsOutboundTaskBO) throws Exception;

    /**
     * 司机抢备料任务单
     */
    void updateCompeteTask(WmsOutboundTaskBO wmsOutboundTaskBO) throws Exception;

    /**
     * 当前有未完成任务的备料单ID
     *
     * @return
     */
    String updateCurrentBlId(String zoneCode);

    /**
     * 当前任务
     *
     * @param wmsOutboundTaskBO
     * @return
     * @throws Exception
     */
    CurrentWmsOutboundTaskListDTO updateCurrentTask(WmsOutboundTaskBO wmsOutboundTaskBO) throws Exception;

    /**
     * 历史任务
     *
     * @param pageVo
     * @param wmsOutboundTaskBO
     * @return
     * @throws Exception
     */
    List<CurrentWmsOutboundTaskListDTO> getFinishedTaskList(PageVo pageVo, WmsOutboundTaskBO wmsOutboundTaskBO) throws Exception;

    /**
     * 出库作业管理列表查询
     *
     * @param wmsOutboundTaskFormBO
     * @return
     * @throws Exception
     */
    Map<String, Object> getOutboundTaskListForPage(WmsOutboundTaskFormBO wmsOutboundTaskFormBO) throws Exception;

    /**
     * 出库作业管理列表查询
     *
     * @param wmsOutboundTaskFormBO
     * @return
     * @throws Exception
     */
    Map<String, Object> getPrintOutboundTaskList(WmsOutboundTaskFormBO wmsOutboundTaskFormBO) throws Exception;

    /**
     * 更新打印信息
     *
     * @param otId
     * @throws Exception
     */
    void updatePrintInfo(Long otId, Integer userId) throws Exception;

    /**
     * 根据车架号查询对应的备料信息
     *
     * @param wmsOutboundTaskFormBO
     * @return
     * @throws Exception
     */
    WmsOutboundTaskDTO getPreparateInfoByVin(WmsOutboundTaskFormBO wmsOutboundTaskFormBO) throws Exception;

    /**
     * 根据备料单号查询对应的备料信息
     *
     * @param wmsOutboundTaskFormBO
     * @return
     * @throws Exception
     */
    List<WmsOutboundTaskDTO> getPreparateInfoByMaterialNo(WmsOutboundTaskFormBO wmsOutboundTaskFormBO) throws Exception;

    /**
     * 备料出库确认
     *
     * @param wmsOutboundTaskFormBO
     * @throws Exception
     */
    void updateTaskToOutbound(WmsOutboundTaskFormBO wmsOutboundTaskFormBO) throws Exception;

    /**
     * 出库确认发送邮件
     *
     * @param vin
     */
    void outboundToMail(String vin);

    void getOutboundSendMailDataNew(String startTime, String endTime) throws Exception;

    /**
     * 发送出库任务excel邮件
     */
    void getOutboundSendMailData();

    /**
     * 备料完成确认
     *
     * @param wmsOutboundTaskFormBO
     * @throws Exception
     */
    void updateTaskToConfirm(WmsOutboundTaskFormBO wmsOutboundTaskFormBO) throws Exception;

    /**
     * 出库记录查询
     *
     * @param wmsOutboundTaskFormBO
     * @return
     * @throws Exception
     */
    Map<String, Object> getOutboundRecordList(WmsOutboundRecordListBO wmsOutboundTaskFormBO) throws Exception;

    /**
     * 根据vin修改出库任务状态
     *
     * @param vin
     */

    void updateStatusByVin(String vin) throws Exception;

    /**
     * 开始任务
     *
     * @param outboundTaskStartBO
     */
    void saveStartOrFinishTask(OutboundTaskStartBO outboundTaskStartBO);

    /**
     * 获取交接单打印信息
     *
     * @param otId
     * @throws Exception
     */
    HandoverDTO getPrintInfo(Long otId) throws Exception;

    /**
     * 出库确认 - 君马库
     *
     * @param barCodeBO
     * @throws Exception
     */
    List<String> getVinList(BarCodeBO barCodeBO) throws Exception;

    /**
     * 出库确认 - 君马库
     *
     * @param taskConfirmFormBO
     * @throws Exception
     */
    List<String> updateToOutConfirm(OutboundTaskConfirmFormBO taskConfirmFormBO, String whCode) throws Exception;

    void updateHandOverOrderDcs() throws Exception;

    void updateHandOverOrderDcsExcp() throws Exception;


    /**
     * 任务取消
     *
     * @param outboundTaskFormBO
     * @throws Exception
     */
    void updateTaskToCancleForCq(WmsOutboundTaskFormBO outboundTaskFormBO) throws Exception;

    /**
     * 备料退库
     *
     * @param outboundTaskFormBO
     * @throws Exception
     */
    void updateTaskToQuitForCq(WmsOutboundTaskFormBO outboundTaskFormBO) throws Exception;

    ;

    WmsOutboundResultDTO getOrderMsg(OutboundConfirmBO bo) throws Exception;

    void updateOutboundByVin(OutboundConfirmBO bo) throws Exception;

    /**
     * 出库记录导出
     *
     * @param dto
     * @param request
     * @param response
     * @throws Exception
     */
    void updateOutboundImportExcel(WmsOutboundImportExcelDTO dto, HttpServletRequest request, HttpServletResponse response) throws Exception;

    @Deprecated
    void updateOutboundImportExcelOld(WmsOutboundImportExcelDTO dto,
                                      HttpServletRequest request,
                                      HttpServletResponse response) throws Exception;

    /**
     * 出库记录条数
     *
     * @param dto
     * @return
     * @throws Exception
     */
    Integer countOutboundRecord(WmsOutboundImportExcelDTO dto) throws Exception;

    void updateHandOverOrderToCRM() throws Exception;

    void updateHandOverOrderSap() throws Exception;

    void updateHandOverOrderSapExcp() throws Exception;
}
