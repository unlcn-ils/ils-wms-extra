package com.unlcn.ils.wms.backend.enums;

/**
 * Created by houjianhui on 2017/6/10.
 */
public enum WayBillStatusEnum {

    WAYBILL_INIT(10, "待验车"),
    WAYBILL_QUALIFY(20, "合格"),
    WAYBILL_EXCP(30, "异常"),
    WAYBILL_DAMAGE(40, "带伤提车"),
    WAYBILL_REPAIR(50, "返修"),
    WAYBILL_COMPROMISE(60, "让步合格");

    private final int value;
    private final String text;

    WayBillStatusEnum(int value, String text) {
        this.value = value;
        this.text = text;
    }

    public int getValue() {
        return value;
    }

    public String getText() {
        return text;
    }

    public static WayBillStatusEnum getByValue(int value){
        for (WayBillStatusEnum temp : WayBillStatusEnum.values()) {
            if(temp.getValue() == value){
                return temp;
            }
        }
        return null;
    }
}
