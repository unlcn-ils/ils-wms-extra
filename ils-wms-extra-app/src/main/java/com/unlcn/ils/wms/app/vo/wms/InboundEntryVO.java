package com.unlcn.ils.wms.app.vo.wms;

/**
 * Created by houjianhui on 2017/5/5.
 */
public class InboundEntryVO {

    private String waybillCode;
    private String waybillOrigin;
    private String waybillDest;
    private Integer vehicleAmt;
    private String vin;
    private String driverName;
    private String checkResult;
    private String warehouseId;
    private String userNo;
    private String details;
    private String excpDetails;
    private Integer userId;
    private String userName;

    public String getWaybillCode() {
        return waybillCode;
    }

    public void setWaybillCode(String waybillCode) {
        this.waybillCode = waybillCode;
    }

    public String getWaybillOrigin() {
        return waybillOrigin;
    }

    public void setWaybillOrigin(String waybillOrigin) {
        this.waybillOrigin = waybillOrigin;
    }

    public String getWaybillDest() {
        return waybillDest;
    }

    public void setWaybillDest(String waybillDest) {
        this.waybillDest = waybillDest;
    }

    public Integer getVehicleAmt() {
        return vehicleAmt;
    }

    public void setVehicleAmt(Integer vehicleAmt) {
        this.vehicleAmt = vehicleAmt;
    }

    public String getVin() {
        return vin;
    }

    public void setVin(String vin) {
        this.vin = vin;
    }

    public String getDriverName() {
        return driverName;
    }

    public void setDriverName(String driverName) {
        this.driverName = driverName;
    }

    public String getCheckResult() {
        return checkResult;
    }

    public void setCheckResult(String checkResult) {
        this.checkResult = checkResult;
    }

    public String getWarehouseId() {
        return warehouseId;
    }

    public void setWarehouseId(String warehouseId) {
        this.warehouseId = warehouseId;
    }

    public String getUserNo() {
        return userNo;
    }

    public void setUserNo(String userNo) {
        this.userNo = userNo;
    }

    public String getDetails() {
        return details;
    }

    public void setDetails(String details) {
        this.details = details;
    }

    public String getExcpDetails() {
        return excpDetails;
    }

    public void setExcpDetails(String excpDetails) {
        this.excpDetails = excpDetails;
    }    

    public Integer getUserId() {
		return userId;
	}

	public void setUserId(Integer userId) {
		this.userId = userId;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	@Override
    public String toString() {
        return "InboundEntryVO{" +
                "waybillCode='" + waybillCode + '\'' +
                ", waybillOrigin='" + waybillOrigin + '\'' +
                ", waybillDest='" + waybillDest + '\'' +
                ", vehicleAmt=" + vehicleAmt +
                ", vin='" + vin + '\'' +
                ", driverName='" + driverName + '\'' +
                ", checkResult='" + checkResult + '\'' +
                ", warehouseId='" + warehouseId + '\'' +
                ", userNo='" + userNo + '\'' +
                ", details='" + details + '\'' +
                ", excpDetails='" + excpDetails + '\'' +
                ", userId='" + userId + '\'' +
                ", userName='" + userName + '\'' +
                '}';
    }
}
