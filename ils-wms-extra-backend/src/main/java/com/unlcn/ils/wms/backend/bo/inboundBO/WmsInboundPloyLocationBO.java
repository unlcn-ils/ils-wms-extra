package com.unlcn.ils.wms.backend.bo.inboundBO;

import com.unlcn.ils.wms.backend.bo.baseDataBO.WmsCommonBO;

/**
 * Created by DELL on 2017/9/1.
 */
public class WmsInboundPloyLocationBO extends WmsCommonBO{
    /**
     * 库位id
     */
    private Long pylId;

    /**
     * 主策略id
     */
    private String pylPyId;

    /**
     * 主策略code
     */
    private String pylPyCode;

    /**
     * 库区id
     */
    private String pylZoneId;

    /**
     * 库区code
     */
    private String pylZoneCode;

    /**
     * 库区name
     */
    private String pylZoneName;

    /**
     * 开始行
     */
    private String pylStartRow;

    /**
     * 开始列
     */
    private String pylStartColumn;

    /**
     * 结束行
     */
    private String pylEndRow;

    /**
     * 结束列
     */
    private String pylEndColumn;

    /**
     * 优先级
     */
    private String pylPriority;

    public Long getPylId() {
        return pylId;
    }

    public void setPylId(Long pylId) {
        this.pylId = pylId;
    }

    public String getPylPyId() {
        return pylPyId;
    }

    public void setPylPyId(String pylPyId) {
        this.pylPyId = pylPyId;
    }

    public String getPylPyCode() {
        return pylPyCode;
    }

    public void setPylPyCode(String pylPyCode) {
        this.pylPyCode = pylPyCode;
    }

    public String getPylZoneId() {
        return pylZoneId;
    }

    public void setPylZoneId(String pylZoneId) {
        this.pylZoneId = pylZoneId;
    }

    public String getPylZoneCode() {
        return pylZoneCode;
    }

    public void setPylZoneCode(String pylZoneCode) {
        this.pylZoneCode = pylZoneCode;
    }

    public String getPylZoneName() {
        return pylZoneName;
    }

    public void setPylZoneName(String pylZoneName) {
        this.pylZoneName = pylZoneName;
    }

    public String getPylStartRow() {
        return pylStartRow;
    }

    public void setPylStartRow(String pylStartRow) {
        this.pylStartRow = pylStartRow;
    }

    public String getPylStartColumn() {
        return pylStartColumn;
    }

    public void setPylStartColumn(String pylStartColumn) {
        this.pylStartColumn = pylStartColumn;
    }

    public String getPylEndRow() {
        return pylEndRow;
    }

    public void setPylEndRow(String pylEndRow) {
        this.pylEndRow = pylEndRow;
    }

    public String getPylEndColumn() {
        return pylEndColumn;
    }

    public void setPylEndColumn(String pylEndColumn) {
        this.pylEndColumn = pylEndColumn;
    }

    public String getPylPriority() {
        return pylPriority;
    }

    public void setPylPriority(String pylPriority) {
        this.pylPriority = pylPriority;
    }

    @Override
    public String toString() {
        return "WmsInboundPloyLocationBO{" +
                "pylId=" + pylId +
                ", pylPyId='" + pylPyId + '\'' +
                ", pylPyCode='" + pylPyCode + '\'' +
                ", pylZoneId='" + pylZoneId + '\'' +
                ", pylZoneCode='" + pylZoneCode + '\'' +
                ", pylZoneName='" + pylZoneName + '\'' +
                ", pylStartRow='" + pylStartRow + '\'' +
                ", pylStartColumn='" + pylStartColumn + '\'' +
                ", pylEndRow='" + pylEndRow + '\'' +
                ", pylEndColumn='" + pylEndColumn + '\'' +
                ", pylPriority='" + pylPriority + '\'' +
                '}';
    }
}
