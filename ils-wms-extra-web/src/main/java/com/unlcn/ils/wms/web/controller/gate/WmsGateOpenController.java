package com.unlcn.ils.wms.web.controller.gate;

import cn.huiyunche.commons.domain.ResultDTO;
import cn.huiyunche.commons.domain.ResultDTOWithPagination;
import cn.huiyunche.commons.exception.BusinessException;
import com.unlcn.ils.wms.backend.service.gate.GateControllerService;
import com.unlcn.ils.wms.base.dto.SysGateConfigDTO;
import com.unlcn.ils.wms.base.dto.WmsCqInboundTmsDTO;
import com.unlcn.ils.wms.base.model.outbound.WmsDepartureRegister;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;

@Controller
@RequestMapping("/gate")
@ResponseBody
public class WmsGateOpenController {
    private final static Logger LOGGER = LoggerFactory.getLogger(WmsGateOpenController.class);

    @Autowired
    private GateControllerService gateControllerService;

    /**
     * 用于查询所有的入库闸门开启失败的数据--展示于页面进行手动开启闸门
     *
     * @return 返回值
     */
    @RequestMapping(value = "/gateOpenFailList", method = RequestMethod.POST)
    public ResultDTOWithPagination<Object> gateOpenFailList(@RequestBody WmsCqInboundTmsDTO dto, HttpServletRequest request) {
        LOGGER.info("WmsGateOpenController.gateOpenFailList param:{}", dto);
        ResultDTOWithPagination<Object> result = new ResultDTOWithPagination<>(true, null, "查询成功");
        String userId = request.getHeader("userId");
        String whCode = request.getHeader("whCode");
        try {
            ResultDTOWithPagination<List<WmsDepartureRegister>> data = gateControllerService.getFailedOpenList(dto, userId, whCode);
            result.setData(data.getData());
            result.setPageVo(data.getPageVo());
        } catch (BusinessException e) {
            LOGGER.error("WmsGateOpenController.gateOpenFailList param:{}", e);
            result.setMessage(e.getMessage());
            result.setSuccess(false);
        } catch (Exception e) {
            LOGGER.error("WmsGateOpenController.gateOpenFailList param:{}", e);
            result.setMessage("系统异常!");
            result.setSuccess(false);
        }
        return result;
    }


    /**
     * 该接口用于手动开启闸门
     *
     * @param dto     参数
     * @param request 请求
     * @return 返回值
     */
    @RequestMapping(value = "/gateOpenByHm", method = RequestMethod.POST)
    public ResultDTO<Object> inboundGate(@RequestBody WmsCqInboundTmsDTO dto, HttpServletRequest request) {
        LOGGER.info("WmsGateOpenController.gateOpenByHm param:{}", dto);
        ResultDTO<Object> result = new ResultDTO<>(true, null, "操作成功");
        String userId = request.getHeader("userId");
        String whCode = request.getHeader("whCode");
        try {
            gateControllerService.addOpenByHm(dto, userId, whCode);
        } catch (BusinessException e) {
            LOGGER.error("WmsGateOpenController.gateOpenByHm error:{}", e);
            result.setMessage(e.getMessage());
            result.setSuccess(false);
        } catch (Exception e) {
            LOGGER.error("WmsGateOpenController.gateOpenByHm error:{}", e);
            result.setMessage("系统异常!");
            result.setSuccess(false);
        }
        return result;
    }

    /**
     * 该接口用于入库道闸开启的记录导出
     *
     * @param dto     参数
     * @param request 请求
     * @return 返回值
     */
    @RequestMapping(value = "/gateRecordsImpot", method = RequestMethod.GET)
    public ResultDTO<Object> gateRecordsImpotExcel(WmsCqInboundTmsDTO dto, HttpServletRequest request, HttpServletResponse response) {
        LOGGER.info("WmsGateOpenController.gateRecordsImpot param:{}", dto);
        ResultDTO<Object> result = new ResultDTO<>(true, null, "操作成功");
        try {
            gateControllerService.gateRecordsImpotExcel(dto, request, response);
        } catch (BusinessException e) {
            LOGGER.error("WmsGateOpenController.gateRecordsImpot error:{}", e);
            result.setMessage(e.getMessage());
            result.setSuccess(false);
        } catch (Exception e) {
            LOGGER.error("WmsGateOpenController.gateRecordsImpot error:{}", e);
            result.setMessage("系统异常!");
            result.setSuccess(false);
        }
        return result;
    }

    /**
     * 用于君马的闸门url地址查询回显
     *
     * @return 返回值
     */
    @RequestMapping(value = "/getJmGateUrl", method = RequestMethod.GET)
    public ResultDTO<Object> getJmGateUrl(HttpServletRequest request) {
        LOGGER.info("WmsGateOpenController.getChannel param:{}");
        ResultDTO<Object> result = new ResultDTO<>(true, null, "查询成功");
        String whCode = request.getHeader("whCode");
        try {
            result.setData(gateControllerService.getJmGateUrl(whCode));
        } catch (BusinessException e) {
            LOGGER.error("WmsGateOpenController.getChannel BussinessException:", e);
            result.setSuccess(false);
            result.setMessage(e.getMessage());
        } catch (Exception ex) {
            LOGGER.error("WmsGateOpenController.getChannel error:", ex);
            result.setSuccess(false);
            result.setMessage("查询异常");
        }
        return result;
    }

    /**
     * 用于君马的闸门url地址查询回显
     *
     * @return 返回值
     */
    @RequestMapping(value = "/updateJmGateUrl", method = RequestMethod.POST)
    public ResultDTO<Object> updateJmGateUrl(@RequestBody SysGateConfigDTO dto, HttpServletRequest request) {
        LOGGER.info("WmsGateOpenController.updateJmGateUrl param:{}");
        ResultDTO<Object> result = new ResultDTO<>(true, null, "操作成功");
        String whCode = request.getHeader("whCode");
        dto.setWhCode(whCode);
        try {
            gateControllerService.updateJmGateUrl(dto);
        } catch (BusinessException e) {
            LOGGER.error("WmsGateOpenController.updateJmGateUrl BussinessException:", e);
            result.setSuccess(false);
            result.setMessage(e.getMessage());
        } catch (Exception ex) {
            LOGGER.error("WmsGateOpenController.updateJmGateUrl error:", ex);
            result.setSuccess(false);
            result.setMessage("更新数据异常");
        }
        return result;
    }

}
