package com.unlcn.ils.wms.backend.service.inspectApp;

import com.unlcn.ils.wms.backend.bo.inspectAppBO.TmsInboundInspectBO;
import com.unlcn.ils.wms.backend.bo.inspectAppBO.TmsInspectExcpDetailBO;
import com.unlcn.ils.wms.base.dto.TmsInspectComponentMissDTO;
import com.unlcn.ils.wms.base.dto.WmsRepairInspectDTO;
import com.unlcn.ils.wms.base.dto.WmsRepairInspectPlaceDTO;

import java.util.ArrayList;
import java.util.List;

public interface WmsRepairInspectService {

    void updateRepairPass(WmsRepairInspectDTO dto) throws Exception;

    void updateRepairExcp(WmsRepairInspectDTO dto) throws Exception;

    ArrayList<WmsRepairInspectPlaceDTO> getRepairInspectPlace(String rpId) throws Exception;

    TmsInboundInspectBO getRepairInfo(WmsRepairInspectDTO dto) throws Exception;

    List<TmsInspectExcpDetailBO> getExcpInfoByPositionId(WmsRepairInspectDTO dto) throws Exception;

    List<TmsInspectComponentMissDTO> getRpComponentMissInfo(WmsRepairInspectDTO dto) throws Exception;
}
