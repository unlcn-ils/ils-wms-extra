package com.unlcn.ils.wms.base.mapper.additional.wmsInboundPloy;

import com.unlcn.ils.wms.base.model.inbound.WmsInboundPloy;

import java.util.List;

/**
 * Created by DELL on 2017/9/1.
 */
public interface WmsInboundPloyExtMapper {
    List<WmsInboundPloy> getPloyList(String pyWhCode);
}
