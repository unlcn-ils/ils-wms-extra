package com.unlcn.ils.wms.backend.dto.bigscreenDTO;

import java.util.Arrays;

/**
 * Created by lenovo on 2017/11/8.
 */
public class OutDrayLineDataDTO {

    private String name;

    private Integer[] data;

    private Float[] rate;

    private String[] date;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer[] getData() {
        return data;
    }

    public void setData(Integer[] data) {
        this.data = data;
    }

    public Float[] getRate() {
        return rate;
    }

    public void setRate(Float[] rate) {
        this.rate = rate;
    }

    public String[] getDate() {
        return date;
    }

    public void setDate(String[] date) {
        this.date = date;
    }

    @Override
    public String toString() {
        return "PickDataDTO{" +
                "name='" + name + '\'' +
                ", data=" + Arrays.toString(data) +
                ", rate=" + Arrays.toString(rate) +
                ", date=" + Arrays.toString(date) +
                '}';
    }
}
