package com.unlcn.ils.wms.backend.service.webservice.dto;

import java.io.Serializable;

public class ResultForDcsInorOutServerBodyDTO implements Serializable {
    private String returnCode;
    private String returnMsg;

    private ResultForDcsInorOutDetailsDTO[] msgDetails;

    public ResultForDcsInorOutDetailsDTO[] getMsgDetails() {
        return msgDetails;
    }

    public void setMsgDetails(ResultForDcsInorOutDetailsDTO[] msgDetails) {
        this.msgDetails = msgDetails;
    }

    public String getReturnCode() {
        return returnCode;
    }

    public void setReturnCode(String returnCode) {
        this.returnCode = returnCode;
    }

    public String getReturnMsg() {
        return returnMsg;
    }

    public void setReturnMsg(String returnMsg) {
        this.returnMsg = returnMsg;
    }
}
