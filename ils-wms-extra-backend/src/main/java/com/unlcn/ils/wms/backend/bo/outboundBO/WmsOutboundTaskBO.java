package com.unlcn.ils.wms.backend.bo.outboundBO;

import java.util.Date;

/**
 * @Author ：Ligl
 * @Date : 2017/9/26.
 */
public class WmsOutboundTaskBO {

    private Long otId;

    private String otCode;

    private String otCustomerId;

    private String otCustomerCode;

    private String otCustomerName;

    private String otDispatchNo;

    private String otWaybillNo;

    private String otOrigin;

    private String otDest;

    private String otVehicleSpecCode;

    private String otVehicleSpecName;

    private String otVehicleSpecDesc;

    private String otVin;

    private String otSupplierId;

    private String otSupplierDriver;

    private String otSupplierPhone;

    private String otVehicleNumber;

    private String otSupplierVsCode;

    private String otSupplierVsName;

    private String otStatus;

    private String otBlId;

    private String otWhCode;

    private String otWhName;

    private String otZoneCode;

    private String otZoneName;

    private String otLocationCode;

    private String otLcoationName;

    private String otCheckResult;

    private String otCheckDesc;

    private String otRemark;

    private String otExceptionNo;

    private String otExceptionType;

    private String otExceptionContent;

    private String createUserId;

    private String createUserName;

    private String modifyUserId;

    private String modifyUserName;

    private Date gmtCreate;

    private Date gmtModify;

    private Byte isDeleted;

    private Byte version;

    private String otOriginCode;

    private String otDestCode;

    private String otCustomerOrderno;

    private String otTaskNo;

    private String otEstimateLane;

    private String otDriver;

    private Date otTaskStartTime;

    private Date otTaskEndTime;

    /**
     * 备料单号
     */
    private String otPreparationMaterialNo;

    /**
     * 备料确认时间
     */
    private Date otBlConfirmTime;

    public Long getOtId() {
        return otId;
    }

    public void setOtId(Long otId) {
        this.otId = otId;
    }

    public String getOtCode() {
        return otCode;
    }

    public void setOtCode(String otCode) {
        this.otCode = otCode;
    }

    public String getOtCustomerId() {
        return otCustomerId;
    }

    public void setOtCustomerId(String otCustomerId) {
        this.otCustomerId = otCustomerId;
    }

    public String getOtCustomerCode() {
        return otCustomerCode;
    }

    public void setOtCustomerCode(String otCustomerCode) {
        this.otCustomerCode = otCustomerCode;
    }

    public String getOtCustomerName() {
        return otCustomerName;
    }

    public void setOtCustomerName(String otCustomerName) {
        this.otCustomerName = otCustomerName;
    }

    public String getOtDispatchNo() {
        return otDispatchNo;
    }

    public void setOtDispatchNo(String otDispatchNo) {
        this.otDispatchNo = otDispatchNo;
    }

    public String getOtWaybillNo() {
        return otWaybillNo;
    }

    public void setOtWaybillNo(String otWaybillNo) {
        this.otWaybillNo = otWaybillNo;
    }

    public String getOtOrigin() {
        return otOrigin;
    }

    public void setOtOrigin(String otOrigin) {
        this.otOrigin = otOrigin;
    }

    public String getOtDest() {
        return otDest;
    }

    public void setOtDest(String otDest) {
        this.otDest = otDest;
    }

    public String getOtVehicleSpecCode() {
        return otVehicleSpecCode;
    }

    public void setOtVehicleSpecCode(String otVehicleSpecCode) {
        this.otVehicleSpecCode = otVehicleSpecCode;
    }

    public String getOtVehicleSpecName() {
        return otVehicleSpecName;
    }

    public void setOtVehicleSpecName(String otVehicleSpecName) {
        this.otVehicleSpecName = otVehicleSpecName;
    }

    public String getOtVehicleSpecDesc() {
        return otVehicleSpecDesc;
    }

    public void setOtVehicleSpecDesc(String otVehicleSpecDesc) {
        this.otVehicleSpecDesc = otVehicleSpecDesc;
    }

    public String getOtVin() {
        return otVin;
    }

    public void setOtVin(String otVin) {
        this.otVin = otVin;
    }

    public String getOtSupplierId() {
        return otSupplierId;
    }

    public void setOtSupplierId(String otSupplierId) {
        this.otSupplierId = otSupplierId;
    }

    public String getOtSupplierDriver() {
        return otSupplierDriver;
    }

    public void setOtSupplierDriver(String otSupplierDriver) {
        this.otSupplierDriver = otSupplierDriver;
    }

    public String getOtSupplierPhone() {
        return otSupplierPhone;
    }

    public void setOtSupplierPhone(String otSupplierPhone) {
        this.otSupplierPhone = otSupplierPhone;
    }

    public String getOtVehicleNumber() {
        return otVehicleNumber;
    }

    public void setOtVehicleNumber(String otVehicleNumber) {
        this.otVehicleNumber = otVehicleNumber;
    }

    public String getOtSupplierVsCode() {
        return otSupplierVsCode;
    }

    public void setOtSupplierVsCode(String otSupplierVsCode) {
        this.otSupplierVsCode = otSupplierVsCode;
    }

    public String getOtSupplierVsName() {
        return otSupplierVsName;
    }

    public void setOtSupplierVsName(String otSupplierVsName) {
        this.otSupplierVsName = otSupplierVsName;
    }

    public String getOtStatus() {
        return otStatus;
    }

    public void setOtStatus(String otStatus) {
        this.otStatus = otStatus;
    }

    public String getOtBlId() {
        return otBlId;
    }

    public void setOtBlId(String otBlId) {
        this.otBlId = otBlId;
    }

    public String getOtWhCode() {
        return otWhCode;
    }

    public void setOtWhCode(String otWhCode) {
        this.otWhCode = otWhCode;
    }

    public String getOtWhName() {
        return otWhName;
    }

    public void setOtWhName(String otWhName) {
        this.otWhName = otWhName;
    }

    public String getOtZoneCode() {
        return otZoneCode;
    }

    public void setOtZoneCode(String otZoneCode) {
        this.otZoneCode = otZoneCode;
    }

    public String getOtZoneName() {
        return otZoneName;
    }

    public void setOtZoneName(String otZoneName) {
        this.otZoneName = otZoneName;
    }

    public String getOtLocationCode() {
        return otLocationCode;
    }

    public void setOtLocationCode(String otLocationCode) {
        this.otLocationCode = otLocationCode;
    }

    public String getOtLcoationName() {
        return otLcoationName;
    }

    public void setOtLcoationName(String otLcoationName) {
        this.otLcoationName = otLcoationName;
    }

    public String getOtCheckResult() {
        return otCheckResult;
    }

    public void setOtCheckResult(String otCheckResult) {
        this.otCheckResult = otCheckResult;
    }

    public String getOtCheckDesc() {
        return otCheckDesc;
    }

    public void setOtCheckDesc(String otCheckDesc) {
        this.otCheckDesc = otCheckDesc;
    }

    public String getOtRemark() {
        return otRemark;
    }

    public void setOtRemark(String otRemark) {
        this.otRemark = otRemark;
    }

    public String getOtExceptionNo() {
        return otExceptionNo;
    }

    public void setOtExceptionNo(String otExceptionNo) {
        this.otExceptionNo = otExceptionNo;
    }

    public String getOtExceptionType() {
        return otExceptionType;
    }

    public void setOtExceptionType(String otExceptionType) {
        this.otExceptionType = otExceptionType;
    }

    public String getOtExceptionContent() {
        return otExceptionContent;
    }

    public void setOtExceptionContent(String otExceptionContent) {
        this.otExceptionContent = otExceptionContent;
    }

    public String getCreateUserId() {
        return createUserId;
    }

    public void setCreateUserId(String createUserId) {
        this.createUserId = createUserId;
    }

    public String getCreateUserName() {
        return createUserName;
    }

    public void setCreateUserName(String createUserName) {
        this.createUserName = createUserName;
    }

    public String getModifyUserId() {
        return modifyUserId;
    }

    public void setModifyUserId(String modifyUserId) {
        this.modifyUserId = modifyUserId;
    }

    public String getModifyUserName() {
        return modifyUserName;
    }

    public void setModifyUserName(String modifyUserName) {
        this.modifyUserName = modifyUserName;
    }

    public Date getGmtCreate() {
        return gmtCreate;
    }

    public void setGmtCreate(Date gmtCreate) {
        this.gmtCreate = gmtCreate;
    }

    public Date getGmtModify() {
        return gmtModify;
    }

    public void setGmtModify(Date gmtModify) {
        this.gmtModify = gmtModify;
    }

    public Byte getIsDeleted() {
        return isDeleted;
    }

    public void setIsDeleted(Byte isDeleted) {
        this.isDeleted = isDeleted;
    }

    public Byte getVersion() {
        return version;
    }

    public void setVersion(Byte version) {
        this.version = version;
    }

    public String getOtOriginCode() {
        return otOriginCode;
    }

    public void setOtOriginCode(String otOriginCode) {
        this.otOriginCode = otOriginCode;
    }

    public String getOtDestCode() {
        return otDestCode;
    }

    public void setOtDestCode(String otDestCode) {
        this.otDestCode = otDestCode;
    }

    public String getOtCustomerOrderno() {
        return otCustomerOrderno;
    }

    public void setOtCustomerOrderno(String otCustomerOrderno) {
        this.otCustomerOrderno = otCustomerOrderno;
    }

    public String getOtTaskNo() {
        return otTaskNo;
    }

    public void setOtTaskNo(String otTaskNo) {
        this.otTaskNo = otTaskNo;
    }

    public String getOtEstimateLane() {
        return otEstimateLane;
    }

    public void setOtEstimateLane(String otEstimateLane) {
        this.otEstimateLane = otEstimateLane;
    }

    public String getOtDriver() {
        return otDriver;
    }

    public void setOtDriver(String otDriver) {
        this.otDriver = otDriver;
    }

    public Date getOtTaskStartTime() {
        return otTaskStartTime;
    }

    public void setOtTaskStartTime(Date otTaskStartTime) {
        this.otTaskStartTime = otTaskStartTime;
    }

    public Date getOtTaskEndTime() {
        return otTaskEndTime;
    }

    public void setOtTaskEndTime(Date otTaskEndTime) {
        this.otTaskEndTime = otTaskEndTime;
    }

    public String getOtPreparationMaterialNo() {
        return otPreparationMaterialNo;
    }

    public void setOtPreparationMaterialNo(String otPreparationMaterialNo) {
        this.otPreparationMaterialNo = otPreparationMaterialNo;
    }

    public Date getOtBlConfirmTime() {
        return otBlConfirmTime;
    }

    public void setOtBlConfirmTime(Date otBlConfirmTime) {
        this.otBlConfirmTime = otBlConfirmTime;
    }
}
