package com.unlcn.ils.wms.backend.bo.inspectAppBO;

import java.util.List;

/**
 * Created by houjianhui on 2017/7/26.
 */
public class TmsExceptionBO {
    private Long exceptionId;
    private String vin;
    private String pos;
    private String body;
    private String hurt;
    private String type;
    private String userno;
    private List<TmsExceptionAttachBO> picList;

    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        this.body = body;
    }

    public String getHurt() {
        return hurt;
    }

    public void setHurt(String hurt) {
        this.hurt = hurt;
    }

    public Long getExceptionId() {
        return exceptionId;
    }

    public void setExceptionId(Long exceptionId) {
        this.exceptionId = exceptionId;
    }

    public String getVin() {
        return vin;
    }

    public void setVin(String vin) {
        this.vin = vin;
    }

    public String getPos() {
        return pos;
    }

    public void setPos(String pos) {
        this.pos = pos;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getUserno() {
        return userno;
    }

    public void setUserno(String userno) {
        this.userno = userno;
    }

    public List<TmsExceptionAttachBO> getPicList() {
        return picList;
    }

    public void setPicList(List<TmsExceptionAttachBO> picList) {
        this.picList = picList;
    }

    @Override
    public String toString() {
        return "{" +
                "exceptionId=" + exceptionId +
                ", vin='" + vin + '\'' +
                ", pos='" + pos + '\'' +
                ", type='" + type + '\'' +
                ", userno='" + userno + '\'' +
                ", picList=" + picList +
                '}';
    }
}
