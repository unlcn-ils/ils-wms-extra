package com.unlcn.ils.wms.base.mapper.additional;

import com.unlcn.ils.wms.base.businessDTO.stock.WmsInventoryDTO;
import com.unlcn.ils.wms.base.dto.InventoryDetailDTO;

import com.unlcn.ils.wms.base.dto.WmsInventoryLocationExtDTO;

import com.unlcn.ils.wms.base.dto.InvoicingStatisticsDTO;
import com.unlcn.ils.wms.base.dto.WmsInventoryManagerResultDTO;
import com.unlcn.ils.wms.base.model.stock.WmsInventory;
import com.unlcn.ils.wms.base.model.stock.WmsInventoryLocation;
import org.apache.ibatis.annotations.Param;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 分配规则
 * Created by DELL on 2017/8/14.
 */
public interface WmsInventoryExtMapper {

    List<WmsInventoryDTO> queryWmsInventoryByInfo(Map<String,Object> paramMap);

    List<WmsInventory> queryWmsInventoryByBrandSeriesSpec(Map<String,Object> paramMap);

    void updateNumByInvId(Long invId);

    Long insertInventory(WmsInventory wmsInventory);

    void insertInventoryLocation(WmsInventoryLocation wmsInventoryLocation);

    List<WmsInventoryDTO> queryForList(Map<String,Object> paramMap);

    Integer queryForCount(Map<String,Object> paramMap);

    List<WmsInventoryLocationExtDTO> queryInventoryLocationForJunMa(Map<String,Object> paramMap);

    Integer countForJunMaList(Map<String,Object> paramMap);

    void subtractInventoryNum(Map<String,Object> paramMap);

    void deleteInventoryByStorerIdAndBillNo(Map<String,Object> paramMap);

    void deleteInventoryLocationByStorerIdAndBillNo(Map<String,Object> paramMap);

    List<InventoryDetailDTO> queryBySpecAndColor(@Param("carSpec") String carSpec, @Param("carColor") String carColor);

    List<InventoryDetailDTO> queryByVin(@Param("vin") String vin);

    List<InvoicingStatisticsDTO> invoicingStatistics(@Param("minDay") Integer minDay, @Param("maxDay") Integer maxDay);

    Integer selectInboundNum(@Param("startTime") String startTime, @Param("endTime") String endTime);

    Integer selectOutboundNum(@Param("startTime") String startTime, @Param("endTime") String endTime);

    Integer selectInventoryNum(@Param("endTime") String  endTime);

    List<WmsInventoryDTO> queryInventoryListForCqImport(Map<String,Object> paramMap);

    List<WmsInventoryLocationExtDTO> queryInventoryListForJmImport(Map<String,Object> paramMap);

    Integer countInventoryCountForCq(Map<String,Object> paramMap);

    Integer countInventoryCountForJm(Map<String,Object> paramMap);

    Integer selectNotGateOutNumNum(@Param("endTime") String endDate);

    List<WmsInventoryLocationExtDTO> selectColourListForJMInventory(Map<String, Object> paramMap);

    List<WmsInventoryLocationExtDTO> selectMaterialListForJMInventory(Map<String, Object> paramMap);

    List<WmsInventory> selectChangeVinNewForJM(HashMap<String, Object> params);

    List<WmsInventoryManagerResultDTO> selectInventoryRecordForManageByParam(HashMap<String, Object> params);

    Long countInventoryRecordForManageByParam(HashMap<String, Object> params);

    List<WmsInventoryLocationExtDTO> queryInventoryLocationForJunMaWithNotice(Map<String, Object> paramMap);

    int countForJunMaListWithNotice(Map<String, Object> paramMap);

    List<WmsInventoryLocationExtDTO> queryInventoryListForJmImportWithNotice(Map<String, Object> paramMap);

    Integer countInventoryCountForJmWithNotice(Map<String, Object> paramMap);

    List<WmsInventoryLocationExtDTO> selectColourListForJMInventoryWithNotice(Map<String, Object> paramMap);

    List<WmsInventoryLocationExtDTO> selectMaterialListForJMInventoryWithNotice(Map<String,Object> paramMap);

    List<WmsInventoryManagerResultDTO> selectInventoryRecordForManageByParamWithNotice(HashMap<String, Object> params);

    Long countInventoryRecordForManageByParamWithNotice(HashMap<String, Object> params);
}
