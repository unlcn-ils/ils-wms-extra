package com.unlcn.ils.wms.backend.service.inbound.impl;

import cn.huiyunche.commons.exception.BusinessException;
import com.google.common.collect.Maps;
import com.unlcn.ils.wms.backend.service.inbound.WmsZoneLocationService;
import com.unlcn.ils.wms.base.businessDTO.baseData.WmsEmptyLocationDTO;
import com.unlcn.ils.wms.base.mapper.additional.WmsLocationExtMapper;
import com.unlcn.ils.wms.base.mapper.additional.WmsZoneExtMapper;
import com.unlcn.ils.wms.base.mapper.stock.WmsZoneMapper;
import com.unlcn.ils.wms.base.model.stock.WmsLocation;
import com.unlcn.ils.wms.base.model.stock.WmsZone;
import org.apache.ibatis.annotations.Param;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

/**
 * 库区库位service
 * Created by DELL on 2017/8/8.
 */
@Service("wmsZoneLocationServiceImpl")
public class WmsZoneLocationServiceImpl implements WmsZoneLocationService {
    @Autowired
    WmsZoneMapper wmsZoneMapper;

    @Autowired
    WmsLocationExtMapper wmsLocationExtMapper;

    @Autowired
    WmsZoneExtMapper wmsZoneExtMapper;


    @Override
    public List<WmsZone> getZone(@Param(value = "whCode") String whCode) {
        Map<String,Object> paramMap = Maps.newHashMap();
        paramMap.put("zeWhCode",whCode);
        List<WmsZone> wmsZoneList = wmsZoneExtMapper.getAllZone(paramMap);
        return wmsZoneList;
    }

    @Override
    public List<WmsEmptyLocationDTO> getEmptyZone(@Param(value = "whCode") String whCode) {
        Map<String,Object> paramMap = Maps.newHashMap();
        paramMap.put("whCode",whCode);
        return wmsLocationExtMapper.getWmsEmptyZone(paramMap);
    }

    @Override
    public List<WmsLocation> getLocation(@Param(value = "whCode")  String whCode) {
        return wmsLocationExtMapper.getAllLocation(whCode);
    }

    @Override
    public List<WmsEmptyLocationDTO> getEmptyLocation(@Param(value = "whCode") String whCode,@Param(value = "zoneId") String zoneId) throws BusinessException{
        Map<String,Object> paramMap = Maps.newHashMap();
        paramMap.put("whCode",whCode);
        paramMap.put("zoneId",zoneId);
        return wmsLocationExtMapper.getWmsEmptyLocation(paramMap);
    }

    @Override
    public List<WmsEmptyLocationDTO> queryQualifiedZone(String whCode, String whType) {
        Map<String,Object> paramMap = Maps.newHashMap();
        paramMap.put("whCode",whCode);
        paramMap.put("whType",whType);
        return wmsLocationExtMapper.queryQualifiedZone(paramMap);
    }
}
