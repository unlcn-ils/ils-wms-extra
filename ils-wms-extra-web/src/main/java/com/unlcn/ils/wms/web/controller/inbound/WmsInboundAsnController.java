package com.unlcn.ils.wms.web.controller.inbound;

import cn.huiyunche.commons.domain.ResultDTO;
import cn.huiyunche.commons.domain.ResultDTOWithPagination;
import cn.huiyunche.commons.exception.BusinessException;
import com.alibaba.fastjson.JSON;
import com.unlcn.ils.wms.backend.bo.inboundBO.WmsAsnTempBO;
import com.unlcn.ils.wms.backend.service.inbound.WmsInboundAsnService;
import com.unlcn.ils.wms.base.dto.WmsWarehouseNoticeHeadForASNListResultDTO;
import com.unlcn.ils.wms.web.vo.inbound.WmsAsnTempVO;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import java.util.List;
import java.util.Map;

/**
 * Created by DELL on 2017/9/21.
 */
@Controller
@RequestMapping("/asn")
@ResponseBody
public class WmsInboundAsnController {

    private Logger logger = LoggerFactory.getLogger(WmsAllocationController.class);
    @Autowired
    WmsInboundAsnService wmsInboundAsnService;

    /**
     * asn管理列表查询接口
     * <p>
     * 2018-4-20 重构入库通知单  此方法新方法
     * </p>
     *
     * @param jsonStr json参数
     * @return 返回值
     */
    @RequestMapping(value = "/getAsnList", method = RequestMethod.POST)
    public ResultDTOWithPagination<List<WmsWarehouseNoticeHeadForASNListResultDTO>> listInboundNotice(@RequestBody String jsonStr, HttpServletRequest request) {
        if (logger.isInfoEnabled()) {
            logger.info("WmsInboundAsnController.listInboundNotice jsonStr: {}", jsonStr);
        }
        ResultDTOWithPagination<List<WmsWarehouseNoticeHeadForASNListResultDTO>> result = new ResultDTOWithPagination<>(true, null, "查询成功");
        try {
            if (jsonStr == null) {
                throw new Exception("参数为null");
            }
            String whCode = request.getHeader("whCode");
            Map<String, Object> paramMap = JSON.parseObject(jsonStr);
            if (StringUtils.isNotBlank(whCode)) {
                paramMap.put("whCode", whCode);
            }
            ResultDTOWithPagination<List<WmsWarehouseNoticeHeadForASNListResultDTO>> data = wmsInboundAsnService.getInboundNoticeList(paramMap);
            result.setPageVo(data.getPageVo());
            result.setData(data.getData());
        } catch (BusinessException be) {
            logger.error("WmsInboundAsnController BusinessException:", be);
            result.setSuccess(false);
            result.setMessage(be.getMessage());
        } catch (Exception ex) {
            logger.error("WmsInboundAsnController error:", ex);
            result.setSuccess(false);
            result.setMessage("ASN查询异常");
        }
        return result;
    }


    /**
     * asn管理列表查询接口
     * <p>
     * 2018-4-20 重构入库通知单  此方法过时: 新方法{@link WmsInboundAsnController#listInboundNotice}
     * </p>
     *
     * @param jsonStr json参数
     * @return 返回值
     */
    @Deprecated
    @RequestMapping(value = "/getAsnList-old", method = RequestMethod.POST)
    public ResultDTO<Map<String, Object>> listOld(@RequestBody String jsonStr, HttpServletRequest request) throws Exception {
        logger.info("WmsInboundAsnController.list jsonStr: {}", jsonStr);
        if (jsonStr == null) {
            throw new Exception("参数为null");
        }
        ResultDTO<Map<String, Object>> resultDTO = new ResultDTO<>(true, null, "ASN查询成功");
        try {
            String whCode = request.getHeader("whCode");
            Map<String, Object> paramMap = JSON.parseObject(jsonStr);
            if (StringUtils.isNotBlank(whCode)) {
                paramMap.put("whCode", whCode);
            }
            resultDTO.setData(wmsInboundAsnService.getAsnListOld(paramMap));
        } catch (BusinessException be) {
            logger.error("WmsInboundAsnController BusinessException:", be);
            resultDTO.setSuccess(false);
            resultDTO.setMessage(be.getMessage());
        } catch (Exception ex) {
            logger.error("WmsInboundAsnController error:", ex);
            resultDTO.setSuccess(false);
            resultDTO.setMessage("ASN查询异常");
        }
        return resultDTO;

    }

    /**
     * 查询
     * <p>
     * 2018-4-23 因入库通知单重构,此方法为新方法
     * </p>
     *
     * @param wmsAsnTempVO 参数封装
     * @return 返回值
     */
    @RequestMapping(value = "/queryListByTime", method = RequestMethod.POST)
    public ResultDTO<Object> queryListByTime(@RequestBody WmsAsnTempVO wmsAsnTempVO,
                                             HttpServletRequest request) {
        logger.info("WmsInboundAsnController.queryListByTimeOld wmsAsnTempVO: {}", wmsAsnTempVO);
        ResultDTO<Object> resultDTO = new ResultDTO<>(true, null, "获取数据成功");
        try {
            WmsAsnTempBO wmsAsnTempBO = new WmsAsnTempBO();
            BeanUtils.copyProperties(wmsAsnTempVO, wmsAsnTempBO);
            String whCode = request.getHeader("whCode");
            resultDTO.setData(wmsInboundAsnService.queryListByTime(wmsAsnTempBO, whCode));
        } catch (BusinessException e) {
            logger.info("WmsInboundAsnController.queryListByTimeOld businessException: {}", e);
            resultDTO.setSuccess(false);
            resultDTO.setMessage(e.getMessage());
        } catch (Exception e) {
            logger.info("WmsInboundAsnController.queryListByTimeOld error: {}", e);
            resultDTO.setSuccess(false);
            resultDTO.setMessage("获取数据失败");
        }
        return resultDTO;
    }

    /**
     * 查询
     * <p>
     * 2018-4-23 因入库通知单重构,此方法过时 新方法{@link WmsInboundAsnController#queryListByTime}
     * </p>
     *
     * @param wmsAsnTempVO 参数封装
     * @return 返回值
     */
    @RequestMapping(value = "/queryListByTime-old", method = RequestMethod.POST)
    @Deprecated
    public ResultDTO<Object> queryListByTimeOld(@RequestBody WmsAsnTempVO wmsAsnTempVO) {
        logger.info("WmsInboundAsnController.queryListByTimeOld wmsAsnTempVO: {}", wmsAsnTempVO);
        ResultDTO<Object> resultDTO = new ResultDTO<>(true, null, "获取数据成功");
        try {
            WmsAsnTempBO wmsAsnTempBO = new WmsAsnTempBO();
            BeanUtils.copyProperties(wmsAsnTempVO, wmsAsnTempBO);
            resultDTO.setData(wmsInboundAsnService.queryListByTimeOld(wmsAsnTempBO));
        } catch (BusinessException e) {
            logger.info("WmsInboundAsnController.queryListByTimeOld businessException: {}", e);
            resultDTO.setSuccess(false);
            resultDTO.setMessage(e.getMessage());
        } catch (Exception e) {
            logger.info("WmsInboundAsnController.queryListByTimeOld error: {}", e);
            resultDTO.setSuccess(false);
            resultDTO.setMessage("获取数据失败");
        }
        return resultDTO;
    }
}
