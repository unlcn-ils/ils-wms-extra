package com.unlcn.ils.wms.base.mapper.extmapper;

import com.unlcn.ils.wms.base.model.sys.SysUser;
import com.unlcn.ils.wms.base.model.sys.SysUserExample;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

public interface SysUserExtMapper {
    /**
     * 根据参数查询
     * @param paramsMap
     * @return
     */
    List<SysUser> findByParams(@Param("paramsMap") Map<String, Object> paramsMap);

    /**
     * 判断用户和仓库是否一致
     *
     * @param paramsMap
     * @return
     */
    Integer countUserWarehouse(@Param("paramsMap") Map<String, Object> paramsMap);
}