package com.unlcn.ils.wms.app.controller.wms;

import cn.huiyunche.commons.domain.ResultDTO;
import cn.huiyunche.commons.exception.BusinessException;
import cn.huiyunche.commons.utils.QiniuUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * Created by houjianhui on 2017/5/12.
 */
@Controller
@RequestMapping("/qiniu")
public class QiNiuController {

    private Logger logger = LoggerFactory.getLogger(QiNiuController.class);

    /**
     * 获取上传凭证
     *
     * @return
     */
    @RequestMapping(value = "/uploadToken", method = RequestMethod.GET)
    public @ResponseBody
    ResultDTO<Object> generateSimpleUploadToken() {
        ResultDTO<Object> result = new ResultDTO<>(true, null, "获取成功");
        try {
            result.setData(QiniuUtil.generateSimpleUploadToken());
        } catch (Exception e) {
            logger.error("QiNiuController.generateSimpleUploadToken error: {}", e);
            throw new BusinessException(e.getMessage());
        }
        return result;
    }

    /**
     * 获取下载图片接口
     *
     * @param key  图片KEY
     * @return
     */
    @RequestMapping(value = "/downloadUrl", method = RequestMethod.GET)
    public @ResponseBody
    ResultDTO<Object> generateDownloadURL(String key) {
        ResultDTO<Object> result = new ResultDTO<>(true, null, "获取下载链接成功");
        try {
            result.setData(QiniuUtil.generateDownloadURL(key, ""));
        } catch (Exception e) {
            logger.error("QiNiuController.generateDownloadURL error: {}", e);
            throw new BusinessException(e.getMessage());
        }
        return result;
    }
}
