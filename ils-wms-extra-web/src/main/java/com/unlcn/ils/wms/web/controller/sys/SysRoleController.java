package com.unlcn.ils.wms.web.controller.sys;

import cn.huiyunche.commons.domain.ResultDTO;
import cn.huiyunche.commons.exception.BusinessException;
import com.unlcn.ils.wms.backend.bo.sys.SysRoleBO;
import com.unlcn.ils.wms.backend.service.sys.SysRoleService;
import com.unlcn.ils.wms.web.vo.sys.SysRoleVO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 * @Auther linbao
 * @Date 2017-10-12
 */
@RestController
@RequestMapping("/sysRole")
public class SysRoleController {

    private Logger LOGGER = LoggerFactory.getLogger(SysRoleController.class);

    @Autowired
    private SysRoleService sysRoleService;

    /**
     * 分页列表查询
     *
     * @param sysRoleVO
     * @return
     */
    @RequestMapping(value = "/listRole", method = RequestMethod.POST)
    public ResultDTO<Object> listRole(@RequestBody SysRoleVO sysRoleVO) {
        LOGGER.info("SysRoleController.listRole param: {}", sysRoleVO);
        ResultDTO<Object> resultDTO = new ResultDTO<>(true, null, "查询成功");
        try {
            SysRoleBO sysRoleBO = new SysRoleBO();
            BeanUtils.copyProperties(sysRoleVO, sysRoleBO);
            resultDTO.setData(sysRoleService.listRole(sysRoleBO));
        } catch (BusinessException e) {
            LOGGER.error("SysRoleController.listRole businessException: {}", e);
            resultDTO.setSuccess(false);
            resultDTO.setMessage(e.getMessage());
        } catch (Exception e) {
            LOGGER.error("SysRoleController.listRole error: {}", e);
            resultDTO.setSuccess(false);
            resultDTO.setMessage("查询失败");
        }
        return resultDTO;
    }

    /**
     * 新增
     *
     * @param sysRoleVO
     * @return
     */
    @RequestMapping(value = "/addRole", method = RequestMethod.POST)
    public ResultDTO<Object> addRole(@RequestBody SysRoleVO sysRoleVO){
        LOGGER.info("SysRoleController.addRole param: {}", sysRoleVO);
        ResultDTO<Object> resultDTO = new ResultDTO<>(true, null, "新增角色成功");
        try {
            SysRoleBO sysRoleBO = new SysRoleBO();
            BeanUtils.copyProperties(sysRoleVO, sysRoleBO);
            sysRoleService.addSysRole(sysRoleBO);
        } catch (BusinessException e) {
            LOGGER.error("SysRoleController.addRole businessException: {}", e);
            resultDTO.setSuccess(false);
            resultDTO.setMessage(e.getMessage());
        } catch (Exception e) {
            LOGGER.error("SysRoleController.addRole error: {}", e);
            resultDTO.setSuccess(false);
            resultDTO.setMessage("新增角色失败");
        }
        return resultDTO;
    }

    /**
     * 编辑
     *
     * @param sysRoleVO
     * @return
     */
    @RequestMapping(value = "/updateRole", method = RequestMethod.POST)
    public ResultDTO<Object> updateRole(@RequestBody SysRoleVO sysRoleVO){
        LOGGER.info("SysRoleController.updateRole param: {}", sysRoleVO);
        ResultDTO<Object> resultDTO = new ResultDTO<>(true, null, "修改角色成功");
        try {
            SysRoleBO sysRoleBO = new SysRoleBO();
            BeanUtils.copyProperties(sysRoleVO, sysRoleBO);
            sysRoleService.updateSysRole(sysRoleBO);
        } catch (BusinessException e) {
            LOGGER.error("SysRoleController.updateRole businessException: {}", e);
            resultDTO.setSuccess(false);
            resultDTO.setMessage(e.getMessage());
        } catch (Exception e) {
            LOGGER.error("SysRoleController.updateRole error: {}", e);
            resultDTO.setSuccess(false);
            resultDTO.setMessage("修改角色失败");
        }
        return resultDTO;
    }

    /**
     * 删除
     *
     * @param sysRoleVO
     * @return
     */
    @RequestMapping(value = "/deleteRole", method = RequestMethod.POST)
    public ResultDTO<Object> deleteRole(@RequestBody SysRoleVO sysRoleVO){
        LOGGER.info("SysRoleController.updateRole param: {}", sysRoleVO);
        ResultDTO<Object> resultDTO = new ResultDTO<>(true, null, "删除角色成功");
        try {
            SysRoleBO sysRoleBO = new SysRoleBO();
            BeanUtils.copyProperties(sysRoleVO, sysRoleBO);
            sysRoleService.deleteSysRole(sysRoleBO);
        } catch (BusinessException e) {
            LOGGER.error("SysRoleController.deleteRole businessException: {}", e);
            resultDTO.setSuccess(false);
            resultDTO.setMessage(e.getMessage());
        } catch (Exception e) {
            LOGGER.error("SysRoleController.deleteRole error: {}", e);
            resultDTO.setSuccess(false);
            resultDTO.setMessage("删除角色失败");
        }
        return resultDTO;
    }

    /**
     * 根据角色id获取对应的权限菜单
     *
     * @param sysRoleVO
     * @return
     */
    @RequestMapping(value = "/listPermiss", method = RequestMethod.POST)
    public ResultDTO<Object> listPermiss(@RequestBody SysRoleVO sysRoleVO){
        LOGGER.info("SysRoleController.listPermiss param: {}", sysRoleVO);
        ResultDTO<Object> resultDTO = new ResultDTO<>(true, null, "获取权限成功");
        try {
            SysRoleBO sysRoleBO = new SysRoleBO();
            BeanUtils.copyProperties(sysRoleVO, sysRoleBO);
            resultDTO.setData(sysRoleService.listPermissByRoleId(sysRoleBO));
        } catch (BusinessException e) {
            LOGGER.error("SysRoleController.listPermiss businessException: {}", e);
            resultDTO.setSuccess(false);
            resultDTO.setMessage(e.getMessage());
        } catch (Exception e) {
            LOGGER.error("SysRoleController.listPermiss error: {}", e);
            resultDTO.setSuccess(false);
            resultDTO.setMessage("获取权限失败");
        }
        return resultDTO;
    }
}
