package com.unlcn.ils.wms.backend.service.verify.impl;

import com.alibaba.fastjson.JSONObject;
import com.unlcn.ils.wms.backend.bo.VerifyBO;
import com.unlcn.ils.wms.backend.service.verify.RedisService;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisPool;

import java.util.Objects;

/**
 * @Auther linbao
 * @Date 2017-10-18
 */
@Service
public class RedisServiceImpl implements RedisService {

    private static Logger LOGGER = LoggerFactory.getLogger(RedisServiceImpl.class);

    private final String key = "WMS_EXTRA_WEB_LOGIN_LIST";

    private final Long timeOut = 1000L * 60L * 60L;

    @Autowired
    private JedisPool jedisPool;

    /**
     * 验证
     *
     * @param token
     * @return
     * @throws Exception
     */
    @Override
    public VerifyBO verifyUser(String token) throws Exception {
        LOGGER.info("RedisServiceImpl.verifyUser token: {}", token);
        Jedis jedis = jedisPool.getResource();
        String data = getData(token);
        VerifyBO resultBO = null;
        if (StringUtils.isNotBlank(data)) {
            VerifyBO verifyBO = JSONObject.parseObject(data, VerifyBO.class);
            if (!Objects.equals(verifyBO, null)) {
                if (!Objects.equals(verifyBO.getTime(), null)) {
                    if ((System.currentTimeMillis() - verifyBO.getTime()) < timeOut) {
                        resultBO = verifyBO;
                    } else {
                        //清除
                        jedis.lrem(key, 0L, data);
                    }
                }
            }
        }
        jedis.close();
        return resultBO;
    }

    /**
     * 加入
     *
     * @param value
     * @throws Exception
     */
    @Override
    public void rpush(String value) throws Exception {
        LOGGER.info("RedisServiceImpl.rpush value: {}", value);
        Jedis jedis = jedisPool.getResource();
        jedis.rpush(key, value);
        jedis.close();
    }

    /**
     * 获取数据
     *
     * @return
     * @throws Exception
     */
    @Override
    public String getData(String token) throws Exception {
        LOGGER.info("RedisServiceImpl.getData token: {}", token);
        Jedis jedis = jedisPool.getResource();
        Long length = jedis.llen(key);
        for (int i = 0; i < length; i++) {
            String indexVal = jedis.lindex(key, i);
            if (StringUtils.isNotBlank(indexVal) && indexVal.contains(token)) {
                jedis.close();
                return indexVal;
            }
        }
        jedis.close();
        return null;
    }

    /**
     * 根据数据移除
     *
     * @param value
     * @throws Exception
     */
    @Override
    public void removeDataByValue(String value) throws Exception {
        LOGGER.info("RedisServiceImpl.removeDataByValue value: {}", value);
        Jedis jedis = jedisPool.getResource();
        if (StringUtils.isNotBlank(value)) {
            jedis.lrem(key, 1L, value);
        }
        jedis.close();
    }

    /**
     * 移除
     *
     * @param token
     * @throws Exception
     */
    @Override
    public void removeDataByToken(String token) throws Exception {
        LOGGER.info("RedisServiceImpl.removeDataByToken token: {}", token);
        Jedis jedis = jedisPool.getResource();
        if (StringUtils.isNotBlank(token)) {
            String data = getData(token);
            if (StringUtils.isNotBlank(data)) {
                jedis.lrem(key, 1L, data);
            }
        }
        jedis.close();
    }
}
