package com.unlcn.ils.wms.backend.service.webservice.bo;

import java.io.Serializable;

/**
 * Created by DELL on 2017/9/22.
 */
public class WmsShipmentPlanResultDTO implements Serializable{

    /**
     * 组板单号
     */
    private String spGroupBoardNo;

    /**
     * 订单号
     */
    private String spOrderNo;

    /**
     * 物料代码
     */
    private String spMaterialCode;

    /**
     * 错误信息
     */
    private String eMsg;


    /**
     * 车型代码
     *//*
    private String spVehicleCode;

    *//**
     * 车型名称
     *//*
    private String spVehicleName;

    *//**
     * 配置
     *//*
    private String spConfigure;

    *//**
     * 颜色
     *//*
    private String spCarColour;

    *//**
     * 物料代码
     *//*
    private String spMaterialCode;

    *//**
     * 组板数量
     *//*
    private Long spGroupBoardQuantity;

    *//**
     * 经销商代码
     *//*
    private String spDealerCode;

    *//**
     * 经销商名称
     *//*
    private String spDealerName;

    *//**
     * 收货省
     *//*
    private String spProvince;

    *//**
     * 收货市
     *//*
    private String spCity;

    *//**
     * 收货区县
     *//*
    private String spDistrictCounty;

    *//**
     * 收货详细地址
     *//*
    private String spDetailedAddress;

    *//**
     * 承运商
     *//*
    private String spCarrier;

    *//**
     * 预计装车时间
     *//*
    private Date spEstimateLoadingTime;*/

    public String getSpGroupBoardNo() {
        return spGroupBoardNo;
    }

    public void setSpGroupBoardNo(String spGroupBoardNo) {
        this.spGroupBoardNo = spGroupBoardNo;
    }

    public String getSpOrderNo() {
        return spOrderNo;
    }

    public void setSpOrderNo(String spOrderNo) {
        this.spOrderNo = spOrderNo;
    }

    public String getSpMaterialCode() {
        return spMaterialCode;
    }

    public void setSpMaterialCode(String spMaterialCode) {
        this.spMaterialCode = spMaterialCode;
    }

    public String geteMsg() {
        return eMsg;
    }

    public void seteMsg(String eMsg) {
        this.eMsg = eMsg;
    }

    @Override
    public String toString() {
        return "WmsShipmentPlanResultDTO{" +
                "spGroupBoardNo='" + spGroupBoardNo + '\'' +
                ", spOrderNo='" + spOrderNo + '\'' +
                ", spMaterialCode='" + spMaterialCode + '\'' +
                ", eMsg='" + eMsg + '\'' +
                '}';
    }
}
