package com.unlcn.ils.wms.backend.service.inbound.impl;

import cn.huiyunche.commons.domain.IdSnowFlake;
import com.unlcn.ils.wms.backend.service.inbound.WmsBatchNoService;
import com.unlcn.ils.wms.base.mapper.extmapper.WmsBatchNoExtMapper;
import com.unlcn.ils.wms.base.mapper.inbound.WmsBatchNoMapper;
import com.unlcn.ils.wms.base.model.inbound.WmsBatchNo;
import org.apache.commons.collections4.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by lenovo on 2017/10/27.
 */
@Service
public class WmsBatchNoServiceImpl implements WmsBatchNoService {

    @Autowired
    private WmsBatchNoMapper wmsBatchNoMapper;

    @Autowired
    private WmsBatchNoExtMapper wmsBatchNoExtMapper;

    @Override
    public String addSerialNum(String serialCode, String bnPrefix) {
        // 日期格式化对象
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
        SimpleDateFormat formatter2 = new SimpleDateFormat("yyyyMMdd");
        // 返回的批次号对象
        String batchNo = null;

        // 查询当前天的批号
//        WmsBatchNoExample wmsBatchNoExample = new WmsBatchNoExample();
//        wmsBatchNoExample.createCriteria().andBnDateEqualTo(formatter.format(new Date()));
//        wmsBatchNoExample.createCriteria().andBnCodeEqualTo(serialCode);
//        wmsBatchNoExample.setOrderByClause(" gmt_create desc ");
//        List<WmsBatchNo> wmsBatchNoList  = wmsBatchNoMapper.selectByExample(wmsBatchNoExample);

        Map<String, Object> paramsMap = new HashMap<String, Object>();
        paramsMap.put("bnDate", formatter.format(new Date()));
        paramsMap.put("bnCode", serialCode);
        paramsMap.put("orderByClause", "gmt_create desc");
        List<WmsBatchNo> wmsBatchNoList  = wmsBatchNoExtMapper.findByParams(paramsMap);

        String afterNum = null;
        if (CollectionUtils.isEmpty(wmsBatchNoList)) {
            afterNum = "001";
        } else {
            afterNum = wmsBatchNoList.get(0).getBnCurrentNo();
            afterNum = getAfterNum(afterNum);
        }
        batchNo = formatter2.format(new Date()) + afterNum;

        IdSnowFlake iw = new IdSnowFlake(1, 2, 0);
        String id = String.valueOf(iw.getId());
        id = id.substring((id.length()-3), (id.length()-1));
        batchNo = id + batchNo;

        if (bnPrefix != null)
            batchNo = bnPrefix + batchNo;

        // 插入生成的批次号
        WmsBatchNo wmsBatchNo = new WmsBatchNo();
        wmsBatchNo.setBnCode(serialCode);
        wmsBatchNo.setBnDate(formatter.format(new Date()));
        wmsBatchNo.setBnPrefix(bnPrefix);
        wmsBatchNo.setBnCurrentNo(afterNum);
        wmsBatchNo.setGmtCreate(new Date());
        wmsBatchNoMapper.insert(wmsBatchNo);

        return batchNo;
    }

    public String getAfterNum(String afterNum) {
        if (afterNum==null || afterNum.equals("000"))
            return "001";

        String numStr = null;
        Integer numInt = null;
        Integer num = null;

        for (int i=0; afterNum.length()>i; i++) {
            numStr = afterNum.substring(i, i+1);
            numInt = Integer.valueOf(numStr);
            Integer j = null;
            if (numInt != 0) {
                j = i;
            }
            if (j != null) {
                numStr = afterNum.substring(j, afterNum.length());
                num = Integer.valueOf(numStr);
                break;
            }
        }

        num = num + 1;

        numStr = String.valueOf(num);
        if (numStr.length() == 1) {
            afterNum = "00" + numStr;
        }
        if (numStr.length() == 2) {
            afterNum = "0" + numStr;
        }
        if (numStr.length() == 3) {
            afterNum = numStr;
        }

        return afterNum;
    }


}
