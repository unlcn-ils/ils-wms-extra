package com.unlcn.ils.wms.api.webservice.dto;

import java.io.Serializable;

public class ResultInboundAndOutBoundSapDTO implements Serializable {
	// pi参数:
	private String returnCode;
	private String vin;
	private String returnMsg;
	private String dataId;
	private String zstatus;


	public String getDataId() {
		return dataId;
	}

	public void setDataId(String dataId) {
		this.dataId = dataId;
	}

	public String getZstatus() {
		return zstatus;
	}

	public void setZstatus(String zstatus) {
		this.zstatus = zstatus;
	}

	public String getVin() {
		return vin;
	}

	public void setVin(String vin) {
		this.vin = vin;
	}

	public void setReturnMsg(String returnMsg) {
		this.returnMsg = returnMsg;
	}

	public String getReturnMsg() {
		return returnMsg;
	}

	public String getReturnCode() {
		return returnCode;
	}

	public void setReturnCode(String returnCode) {
		this.returnCode = returnCode;
	}
}
