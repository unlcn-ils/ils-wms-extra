package com.unlcn.ils.wms.base.mapper.additional.wmsInboundPloy;

import com.unlcn.ils.wms.base.model.inbound.WmsInboundPloyCondition;

import java.util.List;

/**
 * Created by DELL on 2017/9/1.
 */
public interface WmsInboundPloyConditionExtMapper {
    /**
     * 根据策略主CODE查询策略条件
     * @param ployCode
     * @return
     */
    List<WmsInboundPloyCondition> getPloyConditionList(String ployCode);
}
