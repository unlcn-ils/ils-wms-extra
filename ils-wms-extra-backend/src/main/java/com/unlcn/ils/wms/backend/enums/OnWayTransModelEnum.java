package com.unlcn.ils.wms.backend.enums;

/**
 * 前端发运数据运输模式枚举
 */
public enum OnWayTransModelEnum {

    ROAD("ROAD", "路运"),
    RAIL("RAIL", "铁运"),
    WATER("WATER", "船运");

    private final String value;
    private final String text;

    OnWayTransModelEnum(String value, String text) {
        this.value = value;
        this.text = text;
    }

    public String getValue() {
        return value;
    }

    public String getText() {
        return text;
    }

    public static OnWayTransModelEnum getByValue(String value) {
        for (OnWayTransModelEnum temp : OnWayTransModelEnum.values()) {
            if (temp.getValue().equals(value)) {
                return temp;
            }
        }
        return null;
    }

}
