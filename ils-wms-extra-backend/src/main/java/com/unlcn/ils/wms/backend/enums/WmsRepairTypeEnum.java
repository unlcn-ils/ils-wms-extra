package com.unlcn.ils.wms.backend.enums;

/**
 * 维修单类型
 * Created by DELL on 2017/8/24.
 */
public enum WmsRepairTypeEnum {
    WMS_REPAIR_TYPE_INBOUND(10,"库内维修"),
    WMS_REPAIR_TYPE_OUTBOUND(20,"库外维修");

    private final int value;
    private final String text;
    WmsRepairTypeEnum(int value, String text){
        this.value = value;
        this.text = text;
    }

    public int getValue() {
        return value;
    }

    public String getText() {
        return text;
    }

    public static WmsRepairTypeEnum getByValue(int value){
        for (WmsRepairTypeEnum temp : WmsRepairTypeEnum.values()) {
            if(temp.getValue() == value){
                return temp;
            }
        }
        return null;
    }
}
