package com.unlcn.ils.wms.api.outbound.quartz;

import com.unlcn.ils.wms.backend.service.outbound.OutBoundTaskService;
import org.quartz.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component("outboundConfirmJob")
public class QuartzOutboundConfirm {

    private Logger logger = LoggerFactory.getLogger(QuartzOutboundConfirm.class);

    @Autowired
    private OutBoundTaskService outBoundTaskService;

    public void outboundConfirm() throws JobExecutionException {
        logger.info("QuartzOutboundConfirm.getOutboundSendMailData 定时任务start========");
        outBoundTaskService.getOutboundSendMailData();
        logger.info("QuartzOutboundConfirm.getOutboundSendMailData 定时任务end========");
    }
}
