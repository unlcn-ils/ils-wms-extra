package com.unlcn.ils.wms.base.businessDTO.outbound;

import cn.huiyunche.commons.domain.PageVo;

import java.util.Date;

/**
 * Created by DELL on 2017/8/24.
 */
public class WmsOutboundOrderQueryDTO extends PageVo{

    /**
     * 出库单ID
     */
    private Long oodId;

    /**
     * 出库单CODE
     */
    private String oodCode;

    /**
     * 货主id
     */
    private String oodCustomerId;

    /**
     * 货主CODE
     */
    private String oodCustomerCode;

    /**
     * 货主code
     */
    private String oodCustomerName;

    /**
     * 调度号
     */
    private String oodDispatchNo;

    /**
     * 运单号
     */
    private String oodWaybillNo;

    /**
     * 起始地code
     */
    private String oodOriginCode;

    /**
     * 起始地
     */
    private String oodOrigin;

    /**
     * 目的地code
     */
    private String oodDestCode;

    /**
     * 目的地
     */
    private String oodDest;

    /**
     * 车品牌code
     */
    private String oodVehicleSpecCode;

    /**
     * 车品牌
     */
    private String oodVehicleSpecName;

    /**
     * 车品牌描述
     */
    private String oodVehicleSpecDesc;

    /**
     * 底盘号
     */
    private String oodVin;

    /**
     * 分供方（交货方）id
     */
    private String oodSupplierId;

    /**
     * 分供方司机
     */
    private String oodSupplierDriver;

    /**
     * 分供方司机电话
     */
    private String oodSupplierPhone;

    /**
     * 车牌号
     */
    private String oodVehicleNumber;

    /**
     * 分供方车型code
     */
    private String oodSupplierVsCode;

    /**
     * 分供方车型名称
     */
    private String oodSupplierVsName;

    /**
     * 出库单状态
     */
    private String oodStatus;

    /**
     * 备料单ID
     */
    private String oodBlId;

    /**
     * 仓库code
     */
    private String oodWhCode;

    /**
     * 仓库名称
     */
    private String oodWhName;

    /**
     * 库区code
     */
    private String oodZoneCode;

    /**
     * 库区名称
     */
    private String oodZoneName;

    /**
     * 库位code
     */
    private String oodLocationCode;

    /**
     * 库位名称
     */
    private String oodLcoationName;

    /**
     * 检验结果
     */
    private String oodCheckResult;

    /**
     * 检验描述
     */
    private String oodCheckDesc;

    /**
     * 备注
     */
    private String oodRemark;

    /**
     * 异常单号
     */
    private String oodExceptionNo;

    /**
     * 异常类型
     */
    private String oodExceptionType;

    /**
     * 异常原因
     */
    private String oodExceptionContent;

    /**
     * 创建人用户ID
     */
    private String createUserId;

    /**
     * 创建人用户名
     */
    private String createUserName;

    /**
     * 修改人用户ID
     */
    private String modifyUserId;

    /**
     * 修改人用户名
     */
    private String modifyUserName;

    /**
     * 创建日期
     */
    private Date gmtCreate;

    /**
     * 修改日期
     */
    private Date gmtModify;

    /**
     * 逻辑删除
     */
    private Byte isDeleted;

    /**
     * 版本号
     */
    private Byte version;
    /**
     * 创建开始日期
     */
    private String createStartDate;

    /**
     * 创建结束日期
     */
    private String createEndDate;

    /**
     * 收货开始日期
     */
    private String consigneeStartDate;

    /**
     * 收货结束日期
     */
    private String consigneeEndDate;


    /**
     * 分页开始索引
     */
    private int limitStart = -1;

    /**
     * 分页结束索引
     */
    private int limitEnd = -1;

    public Long getOodId() {
        return oodId;
    }

    public void setOodId(Long oodId) {
        this.oodId = oodId;
    }

    public String getOodCode() {
        return oodCode;
    }

    public void setOodCode(String oodCode) {
        this.oodCode = oodCode;
    }

    public String getOodCustomerId() {
        return oodCustomerId;
    }

    public void setOodCustomerId(String oodCustomerId) {
        this.oodCustomerId = oodCustomerId;
    }

    public String getOodCustomerCode() {
        return oodCustomerCode;
    }

    public void setOodCustomerCode(String oodCustomerCode) {
        this.oodCustomerCode = oodCustomerCode;
    }

    public String getOodCustomerName() {
        return oodCustomerName;
    }

    public void setOodCustomerName(String oodCustomerName) {
        this.oodCustomerName = oodCustomerName;
    }

    public String getOodDispatchNo() {
        return oodDispatchNo;
    }

    public void setOodDispatchNo(String oodDispatchNo) {
        this.oodDispatchNo = oodDispatchNo;
    }

    public String getOodWaybillNo() {
        return oodWaybillNo;
    }

    public void setOodWaybillNo(String oodWaybillNo) {
        this.oodWaybillNo = oodWaybillNo;
    }
    public String getOodOriginCode() {
        return oodOriginCode;
    }

    public void setOodOriginCode(String oodOriginCode) {
        this.oodOriginCode = oodOriginCode;
    }

    public String getOodOrigin() {
        return oodOrigin;
    }

    public void setOodOrigin(String oodOrigin) {
        this.oodOrigin = oodOrigin;
    }

    public String getOodDestCode() {
        return oodDestCode;
    }

    public void setOodDestCode(String oodDestCode) {
        this.oodDestCode = oodDestCode;
    }

    public String getOodDest() {
        return oodDest;
    }

    public void setOodDest(String oodDest) {
        this.oodDest = oodDest;
    }

    public String getOodVehicleSpecCode() {
        return oodVehicleSpecCode;
    }

    public void setOodVehicleSpecCode(String oodVehicleSpecCode) {
        this.oodVehicleSpecCode = oodVehicleSpecCode;
    }

    public String getOodVehicleSpecName() {
        return oodVehicleSpecName;
    }

    public void setOodVehicleSpecName(String oodVehicleSpecName) {
        this.oodVehicleSpecName = oodVehicleSpecName;
    }

    public String getOodVehicleSpecDesc() {
        return oodVehicleSpecDesc;
    }

    public void setOodVehicleSpecDesc(String oodVehicleSpecDesc) {
        this.oodVehicleSpecDesc = oodVehicleSpecDesc;
    }

    public String getOodVin() {
        return oodVin;
    }

    public void setOodVin(String oodVin) {
        this.oodVin = oodVin;
    }

    public String getOodSupplierId() {
        return oodSupplierId;
    }

    public void setOodSupplierId(String oodSupplierId) {
        this.oodSupplierId = oodSupplierId;
    }

    public String getOodSupplierDriver() {
        return oodSupplierDriver;
    }

    public void setOodSupplierDriver(String oodSupplierDriver) {
        this.oodSupplierDriver = oodSupplierDriver;
    }

    public String getOodSupplierPhone() {
        return oodSupplierPhone;
    }

    public void setOodSupplierPhone(String oodSupplierPhone) {
        this.oodSupplierPhone = oodSupplierPhone;
    }

    public String getOodVehicleNumber() {
        return oodVehicleNumber;
    }

    public void setOodVehicleNumber(String oodVehicleNumber) {
        this.oodVehicleNumber = oodVehicleNumber;
    }

    public String getOodSupplierVsCode() {
        return oodSupplierVsCode;
    }

    public void setOodSupplierVsCode(String oodSupplierVsCode) {
        this.oodSupplierVsCode = oodSupplierVsCode;
    }

    public String getOodSupplierVsName() {
        return oodSupplierVsName;
    }

    public void setOodSupplierVsName(String oodSupplierVsName) {
        this.oodSupplierVsName = oodSupplierVsName;
    }

    public String getOodStatus() {
        return oodStatus;
    }

    public void setOodStatus(String oodStatus) {
        this.oodStatus = oodStatus;
    }

    public String getOodBlId() {
        return oodBlId;
    }

    public void setOodBlId(String oodBlId) {
        this.oodBlId = oodBlId;
    }

    public String getOodWhCode() {
        return oodWhCode;
    }

    public void setOodWhCode(String oodWhCode) {
        this.oodWhCode = oodWhCode;
    }

    public String getOodWhName() {
        return oodWhName;
    }

    public void setOodWhName(String oodWhName) {
        this.oodWhName = oodWhName;
    }

    public String getOodZoneCode() {
        return oodZoneCode;
    }

    public void setOodZoneCode(String oodZoneCode) {
        this.oodZoneCode = oodZoneCode;
    }

    public String getOodZoneName() {
        return oodZoneName;
    }

    public void setOodZoneName(String oodZoneName) {
        this.oodZoneName = oodZoneName;
    }

    public String getOodLocationCode() {
        return oodLocationCode;
    }

    public void setOodLocationCode(String oodLocationCode) {
        this.oodLocationCode = oodLocationCode;
    }

    public String getOodLcoationName() {
        return oodLcoationName;
    }

    public void setOodLcoationName(String oodLcoationName) {
        this.oodLcoationName = oodLcoationName;
    }

    public String getOodCheckResult() {
        return oodCheckResult;
    }

    public void setOodCheckResult(String oodCheckResult) {
        this.oodCheckResult = oodCheckResult;
    }

    public String getOodCheckDesc() {
        return oodCheckDesc;
    }

    public void setOodCheckDesc(String oodCheckDesc) {
        this.oodCheckDesc = oodCheckDesc;
    }

    public String getOodRemark() {
        return oodRemark;
    }

    public void setOodRemark(String oodRemark) {
        this.oodRemark = oodRemark;
    }

    public String getOodExceptionNo() {
        return oodExceptionNo;
    }

    public void setOodExceptionNo(String oodExceptionNo) {
        this.oodExceptionNo = oodExceptionNo;
    }

    public String getOodExceptionType() {
        return oodExceptionType;
    }

    public void setOodExceptionType(String oodExceptionType) {
        this.oodExceptionType = oodExceptionType;
    }

    public String getOodExceptionContent() {
        return oodExceptionContent;
    }

    public void setOodExceptionContent(String oodExceptionContent) {
        this.oodExceptionContent = oodExceptionContent;
    }

    public String getCreateUserId() {
        return createUserId;
    }

    public void setCreateUserId(String createUserId) {
        this.createUserId = createUserId;
    }

    public String getCreateUserName() {
        return createUserName;
    }

    public void setCreateUserName(String createUserName) {
        this.createUserName = createUserName;
    }

    public String getModifyUserId() {
        return modifyUserId;
    }

    public void setModifyUserId(String modifyUserId) {
        this.modifyUserId = modifyUserId;
    }

    public String getModifyUserName() {
        return modifyUserName;
    }

    public void setModifyUserName(String modifyUserName) {
        this.modifyUserName = modifyUserName;
    }

    public Date getGmtCreate() {
        return gmtCreate;
    }

    public void setGmtCreate(Date gmtCreate) {
        this.gmtCreate = gmtCreate;
    }

    public Date getGmtModify() {
        return gmtModify;
    }

    public void setGmtModify(Date gmtModify) {
        this.gmtModify = gmtModify;
    }

    public Byte getIsDeleted() {
        return isDeleted;
    }

    public void setIsDeleted(Byte isDeleted) {
        this.isDeleted = isDeleted;
    }

    public Byte getVersion() {
        return version;
    }

    public void setVersion(Byte version) {
        this.version = version;
    }

    public String getCreateStartDate() {
        return createStartDate;
    }

    public void setCreateStartDate(String createStartDate) {
        this.createStartDate = createStartDate;
    }

    public String getCreateEndDate() {
        return createEndDate;
    }

    public void setCreateEndDate(String createEndDate) {
        this.createEndDate = createEndDate;
    }

    public String getConsigneeStartDate() {
        return consigneeStartDate;
    }

    public void setConsigneeStartDate(String consigneeStartDate) {
        this.consigneeStartDate = consigneeStartDate;
    }

    public String getConsigneeEndDate() {
        return consigneeEndDate;
    }

    public void setConsigneeEndDate(String consigneeEndDate) {
        this.consigneeEndDate = consigneeEndDate;
    }

    public int getLimitStart() {
        return limitStart;
    }

    public void setLimitStart(int limitStart) {
        this.limitStart = limitStart;
    }

    public int getLimitEnd() {
        return limitEnd;
    }

    public void setLimitEnd(int limitEnd) {
        this.limitEnd = limitEnd;
    }

    @Override
    public String toString() {
        return "WmsOutboundOrderQueryDTO{" +
                "oodId=" + oodId +
                ", oodCode='" + oodCode + '\'' +
                ", oodCustomerId='" + oodCustomerId + '\'' +
                ", oodCustomerCode='" + oodCustomerCode + '\'' +
                ", oodCustomerName='" + oodCustomerName + '\'' +
                ", oodDispatchNo='" + oodDispatchNo + '\'' +
                ", oodWaybillNo='" + oodWaybillNo + '\'' +
                ", oodOriginCode='" + oodOriginCode + '\'' +
                ", oodOrigin='" + oodOrigin + '\'' +
                ", oodDestCode='" + oodDestCode + '\'' +
                ", oodDest='" + oodDest + '\'' +
                ", oodVehicleSpecCode='" + oodVehicleSpecCode + '\'' +
                ", oodVehicleSpecName='" + oodVehicleSpecName + '\'' +
                ", oodVehicleSpecDesc='" + oodVehicleSpecDesc + '\'' +
                ", oodVin='" + oodVin + '\'' +
                ", oodSupplierId='" + oodSupplierId + '\'' +
                ", oodSupplierDriver='" + oodSupplierDriver + '\'' +
                ", oodSupplierPhone='" + oodSupplierPhone + '\'' +
                ", oodVehicleNumber='" + oodVehicleNumber + '\'' +
                ", oodSupplierVsCode='" + oodSupplierVsCode + '\'' +
                ", oodSupplierVsName='" + oodSupplierVsName + '\'' +
                ", oodStatus='" + oodStatus + '\'' +
                ", oodBlId='" + oodBlId + '\'' +
                ", oodWhCode='" + oodWhCode + '\'' +
                ", oodWhName='" + oodWhName + '\'' +
                ", oodZoneCode='" + oodZoneCode + '\'' +
                ", oodZoneName='" + oodZoneName + '\'' +
                ", oodLocationCode='" + oodLocationCode + '\'' +
                ", oodLcoationName='" + oodLcoationName + '\'' +
                ", oodCheckResult='" + oodCheckResult + '\'' +
                ", oodCheckDesc='" + oodCheckDesc + '\'' +
                ", oodRemark='" + oodRemark + '\'' +
                ", oodExceptionNo='" + oodExceptionNo + '\'' +
                ", oodExceptionType='" + oodExceptionType + '\'' +
                ", oodExceptionContent='" + oodExceptionContent + '\'' +
                ", createUserId='" + createUserId + '\'' +
                ", createUserName='" + createUserName + '\'' +
                ", modifyUserId='" + modifyUserId + '\'' +
                ", modifyUserName='" + modifyUserName + '\'' +
                ", gmtCreate=" + gmtCreate +
                ", gmtModify=" + gmtModify +
                ", isDeleted=" + isDeleted +
                ", version=" + version +
                ", createStartDate='" + createStartDate + '\'' +
                ", createEndDate='" + createEndDate + '\'' +
                ", consigneeStartDate='" + consigneeStartDate + '\'' +
                ", consigneeEndDate='" + consigneeEndDate + '\'' +
                ", limitStart=" + limitStart +
                ", limitEnd=" + limitEnd +
                '}';
    }
}
