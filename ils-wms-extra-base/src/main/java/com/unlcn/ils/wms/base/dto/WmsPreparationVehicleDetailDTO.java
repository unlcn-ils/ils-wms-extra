package com.unlcn.ils.wms.base.dto;

import java.io.Serializable;
import java.util.Date;

public class WmsPreparationVehicleDetailDTO implements Serializable {
    private String createUserName;
    private Date gmtCreate;
    private String modifyUserName;
    private Date gmtUpdate;
    private Byte isDeleted;
    private String versions;
    private String driverId;
    private String driverName;
    private Long vdId;
    private Long vdPpId;
    private String vdVin;
    private String vdVehicleName;
    private String vdVehicleDesc;
    private String vdWaybillNo;
    private String vdOutstockStatus;
    private String wndMaterialCode;
    private String alSourceLocCode;
    private String alSourceZoneCode;
    private String hoHandoverNumber;

    public String getHoHandoverNumber() {
        return hoHandoverNumber;
    }

    public void setHoHandoverNumber(String hoHandoverNumber) {
        this.hoHandoverNumber = hoHandoverNumber;
    }

    public String getAlSourceZoneCode() {
        return alSourceZoneCode;
    }

    public void setAlSourceZoneCode(String alSourceZoneCode) {
        this.alSourceZoneCode = alSourceZoneCode;
    }

    public String getWndMaterialCode() {
        return wndMaterialCode;
    }

    public void setWndMaterialCode(String wndMaterialCode) {
        this.wndMaterialCode = wndMaterialCode;
    }

    public String getAlSourceLocCode() {
        return alSourceLocCode;
    }

    public void setAlSourceLocCode(String alSourceLocCode) {
        this.alSourceLocCode = alSourceLocCode;
    }

    public Long getVdId() {
        return vdId;
    }

    public void setVdId(Long vdId) {
        this.vdId = vdId;
    }

    public Long getVdPpId() {
        return vdPpId;
    }

    public void setVdPpId(Long vdPpId) {
        this.vdPpId = vdPpId;
    }

    public String getVdVin() {
        return vdVin;
    }

    public void setVdVin(String vdVin) {
        this.vdVin = vdVin;
    }

    public String getVdVehicleName() {
        return vdVehicleName;
    }

    public void setVdVehicleName(String vdVehicleName) {
        this.vdVehicleName = vdVehicleName;
    }

    public String getVdVehicleDesc() {
        return vdVehicleDesc;
    }

    public void setVdVehicleDesc(String vdVehicleDesc) {
        this.vdVehicleDesc = vdVehicleDesc;
    }

    public String getVdWaybillNo() {
        return vdWaybillNo;
    }

    public void setVdWaybillNo(String vdWaybillNo) {
        this.vdWaybillNo = vdWaybillNo;
    }

    public String getVdOutstockStatus() {
        return vdOutstockStatus;
    }

    public void setVdOutstockStatus(String vdOutstockStatus) {
        this.vdOutstockStatus = vdOutstockStatus;
    }

    public String getDriverId() {
        return driverId;
    }

    public void setDriverId(String driverId) {
        this.driverId = driverId;
    }

    public String getDriverName() {
        return driverName;
    }

    public void setDriverName(String driverName) {
        this.driverName = driverName;
    }

    public String getCreateUserName() {
        return createUserName;
    }

    public void setCreateUserName(String createUserName) {
        this.createUserName = createUserName;
    }

    public Date getGmtCreate() {
        return gmtCreate;
    }

    public void setGmtCreate(Date gmtCreate) {
        this.gmtCreate = gmtCreate;
    }

    public String getModifyUserName() {
        return modifyUserName;
    }

    public void setModifyUserName(String modifyUserName) {
        this.modifyUserName = modifyUserName;
    }

    public Date getGmtUpdate() {
        return gmtUpdate;
    }

    public void setGmtUpdate(Date gmtUpdate) {
        this.gmtUpdate = gmtUpdate;
    }

    public Byte getIsDeleted() {
        return isDeleted;
    }

    public void setIsDeleted(Byte isDeleted) {
        this.isDeleted = isDeleted;
    }

    public String getVersions() {
        return versions;
    }

    public void setVersions(String versions) {
        this.versions = versions;
    }

    @Override
    public String toString() {
        return "WmsPreparationVehicleDetailDTO{" +
                "createUserName='" + createUserName + '\'' +
                ", gmtCreate=" + gmtCreate +
                ", modifyUserName='" + modifyUserName + '\'' +
                ", gmtUpdate=" + gmtUpdate +
                ", isDeleted=" + isDeleted +
                ", versions='" + versions + '\'' +
                ", driverId='" + driverId + '\'' +
                ", driverName='" + driverName + '\'' +
                '}';
    }
}
