package com.unlcn.ils.wms.backend.bo.pickup;

import java.util.Date;
import java.util.List;

/**
 * @Author ：Ligl
 * @Date : 2017/7/22.
 */
public class TmsPickupInfoDetailBO {
    private Long id;
    private String waybillCode;
    private String vin;
    private String vehicle;
    private String destProvince;
    private String destCity;
    private String warehouseId;
    private String warehouseName;
    private String motorNo;
    private Integer checkStatus;
    private Integer pickupStatus;
    private Date pickupTime;
    private String pickupUserId;
    private String checkStatusText;
    private String pickupStatusText;
    private String generateTime;
    private List<TmsPickupExcpBO> excpList;
    private String cacheWarehouse;


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getWaybillCode() {
        return waybillCode;
    }

    public void setWaybillCode(String waybillCode) {
        this.waybillCode = waybillCode;
    }

    public String getVin() {
        return vin;
    }

    public void setVin(String vin) {
        this.vin = vin;
    }

    public String getVehicle() {
        return vehicle;
    }

    public void setVehicle(String vehicle) {
        this.vehicle = vehicle;
    }

    public String getDestProvince() {
        return destProvince;
    }

    public void setDestProvince(String destProvince) {
        this.destProvince = destProvince;
    }

    public String getWarehouseId() {
        return warehouseId;
    }

    public void setWarehouseId(String warehouseId) {
        this.warehouseId = warehouseId;
    }

    public String getWarehouseName() {
        return warehouseName;
    }

    public void setWarehouseName(String warehouseName) {
        this.warehouseName = warehouseName;
    }

    public String getMotorNo() {
        return motorNo;
    }

    public void setMotorNo(String motorNo) {
        this.motorNo = motorNo;
    }

    public Integer getCheckStatus() {
        return checkStatus;
    }

    public void setCheckStatus(Integer checkStatus) {
        this.checkStatus = checkStatus;
    }

    public Integer getPickupStatus() {
        return pickupStatus;
    }

    public void setPickupStatus(Integer pickupStatus) {
        this.pickupStatus = pickupStatus;
    }

    public Date getPickupTime() {
        return pickupTime;
    }

    public void setPickupTime(Date pickupTime) {
        this.pickupTime = pickupTime;
    }

    public List<TmsPickupExcpBO> getExcpList() {
        return excpList;
    }

    public void setExcpList(List<TmsPickupExcpBO> excpList) {
        this.excpList = excpList;
    }

    public String getCheckStatusText() {
        return checkStatusText;
    }

    public void setCheckStatusText(String checkStatusText) {
        this.checkStatusText = checkStatusText;
    }

    public String getPickupStatusText() {
        return pickupStatusText;
    }

    public void setPickupStatusText(String pickupStatusText) {
        this.pickupStatusText = pickupStatusText;
    }

    public String getGenerateTime() {
        return generateTime;
    }

    public void setGenerateTime(String generateTime) {
        this.generateTime = generateTime;
    }

    public String getPickupUserId() {
        return pickupUserId;
    }

    public void setPickupUserId(String pickupUserId) {
        this.pickupUserId = pickupUserId;
    }

    public String getCacheWarehouse() {
        return cacheWarehouse;
    }

    public void setCacheWarehouse(String cacheWarehouse) {
        this.cacheWarehouse = cacheWarehouse;
    }

    public String getDestCity() {
        return destCity;
    }

    public void setDestCity(String destCity) {
        this.destCity = destCity;
    }

    @Override
    public String toString() {
        return "TmsPickupInfoDetailBO{" +
                "id=" + id +
                ", waybillCode='" + waybillCode + '\'' +
                ", vin='" + vin + '\'' +
                ", vehicle='" + vehicle + '\'' +
                ", destProvince='" + destProvince + '\'' +
                ", destCity='" + destCity + '\'' +
                ", warehouseId='" + warehouseId + '\'' +
                ", warehouseName='" + warehouseName + '\'' +
                ", motorNo='" + motorNo + '\'' +
                ", checkStatus=" + checkStatus +
                ", pickupStatus=" + pickupStatus +
                ", pickupTime=" + pickupTime +
                ", pickupUserId='" + pickupUserId + '\'' +
                ", checkStatusText='" + checkStatusText + '\'' +
                ", pickupStatusText='" + pickupStatusText + '\'' +
                ", generateTime='" + generateTime + '\'' +
                ", excpList=" + excpList +
                ", cacheWarehouse='" + cacheWarehouse + '\'' +
                '}';
    }
}
