package com.unlcn.ils.wms.web.vo.outbound;

import cn.huiyunche.commons.domain.PageVo;

import java.util.Date;

/**
 * created by linbao on 2017-09-28
 * 出库作业
 */
public class WmsOutboundTaskFormVO extends PageVo {

    /**
     * 备料单号
     */
    private String otPreparationMaterialNo;

    /**
     * 车型
     */
    private String otVehicleSpecName;

    /**
     * 装车道
     */
    private String otEstimateLane;

    /**
     * 状态
     */
    private String otStatus;

    /**
     * 创建开始时间
     */
    private String startCreateTime;

    /**
     * 创建结束时间
     */
    private String endCreateTime;

    /**
     * 备料确认开始时间
     */
    private String startBlConfirmTime;

    /**
     * 备料确认结束时间
     */
    private String endBlConfirmTime;

    /**
     * 车架号
     */
    private String otVin;

    /**
     * id
     */
    private Long otId;

    /**
     * 备料任务号
     */
    private String otTaskNo;

    /**
     * 创建时间
     */
    private Date gmtCreate;

    /**
     * 库区
     */
    private String otZoneName;

    /**
     * 库位
     */
    private String otLcoationName;

    /**
     * 备料司机
     */
    private String otDriver;

    /**
     * 备料司机名称
     */
    private String otDriverName;

    /**
     * 任务开始时间
     */
    private Date otTaskStartTime;

    /**
     * 任务结束时间
     */
    private Date otTaskEndTime;

    /**
     * 备料确认时间
     */
    private Date otBlConfirmTime;

    /**
     * 交接单号
     */
    private String hoHandoverNumber;

    /**
     * 打印状态
     */
    private Integer printStatus;

    /**
     * 打印开始时间
     */
    private String printStartTime;

    /**
     * 打印截至时间
     */
    private String printEndTime;

    /**
     * 取消原因
     */
    private String cancleReason;

    /**
     * 退库状态, 0-正常, 1-已退库
     */
    private Integer otQuitFlag;

    /**
     * 发运状态
     */
    private Integer otOutboundFlag;

    public Integer getOtOutboundFlag() {
        return otOutboundFlag;
    }

    public void setOtOutboundFlag(Integer otOutboundFlag) {
        this.otOutboundFlag = otOutboundFlag;
    }

    public String getOtDriverName() {
        return otDriverName;
    }

    public void setOtDriverName(String otDriverName) {
        this.otDriverName = otDriverName;
    }

    public String getCancleReason() {
        return cancleReason;
    }

    public void setCancleReason(String cancleReason) {
        this.cancleReason = cancleReason;
    }

    public String getOtPreparationMaterialNo() {
        return otPreparationMaterialNo;
    }

    public void setOtPreparationMaterialNo(String otPreparationMaterialNo) {
        this.otPreparationMaterialNo = otPreparationMaterialNo;
    }

    public String getOtVehicleSpecName() {
        return otVehicleSpecName;
    }

    public void setOtVehicleSpecName(String otVehicleSpecName) {
        this.otVehicleSpecName = otVehicleSpecName;
    }

    public String getOtEstimateLane() {
        return otEstimateLane;
    }

    public void setOtEstimateLane(String otEstimateLane) {
        this.otEstimateLane = otEstimateLane;
    }

    public String getOtStatus() {
        return otStatus;
    }

    public void setOtStatus(String otStatus) {
        this.otStatus = otStatus;
    }

    public String getStartCreateTime() {
        return startCreateTime;
    }

    public void setStartCreateTime(String startCreateTime) {
        this.startCreateTime = startCreateTime;
    }

    public String getEndCreateTime() {
        return endCreateTime;
    }

    public void setEndCreateTime(String endCreateTime) {
        this.endCreateTime = endCreateTime;
    }

    public String getStartBlConfirmTime() {
        return startBlConfirmTime;
    }

    public void setStartBlConfirmTime(String startBlConfirmTime) {
        this.startBlConfirmTime = startBlConfirmTime;
    }

    public String getEndBlConfirmTime() {
        return endBlConfirmTime;
    }

    public void setEndBlConfirmTime(String endBlConfirmTime) {
        this.endBlConfirmTime = endBlConfirmTime;
    }

    public String getOtVin() {
        return otVin;
    }

    public void setOtVin(String otVin) {
        this.otVin = otVin;
    }

    public Long getOtId() {
        return otId;
    }

    public void setOtId(Long otId) {
        this.otId = otId;
    }

    public String getOtTaskNo() {
        return otTaskNo;
    }

    public void setOtTaskNo(String otTaskNo) {
        this.otTaskNo = otTaskNo;
    }

    public Date getGmtCreate() {
        return gmtCreate;
    }

    public void setGmtCreate(Date gmtCreate) {
        this.gmtCreate = gmtCreate;
    }

    public String getOtZoneName() {
        return otZoneName;
    }

    public void setOtZoneName(String otZoneName) {
        this.otZoneName = otZoneName;
    }

    public String getOtLcoationName() {
        return otLcoationName;
    }

    public void setOtLcoationName(String otLcoationName) {
        this.otLcoationName = otLcoationName;
    }

    public String getOtDriver() {
        return otDriver;
    }

    public void setOtDriver(String otDriver) {
        this.otDriver = otDriver;
    }

    public Date getOtTaskStartTime() {
        return otTaskStartTime;
    }

    public void setOtTaskStartTime(Date otTaskStartTime) {
        this.otTaskStartTime = otTaskStartTime;
    }

    public Date getOtTaskEndTime() {
        return otTaskEndTime;
    }

    public void setOtTaskEndTime(Date otTaskEndTime) {
        this.otTaskEndTime = otTaskEndTime;
    }

    public Date getOtBlConfirmTime() {
        return otBlConfirmTime;
    }

    public void setOtBlConfirmTime(Date otBlConfirmTime) {
        this.otBlConfirmTime = otBlConfirmTime;
    }

    public String getHoHandoverNumber() {
        return hoHandoverNumber;
    }

    public void setHoHandoverNumber(String hoHandoverNumber) {
        this.hoHandoverNumber = hoHandoverNumber;
    }

    public Integer getPrintStatus() {
        return printStatus;
    }

    public void setPrintStatus(Integer printStatus) {
        this.printStatus = printStatus;
    }

    public String getPrintStartTime() {
        return printStartTime;
    }

    public void setPrintStartTime(String printStartTime) {
        this.printStartTime = printStartTime;
    }

    public String getPrintEndTime() {
        return printEndTime;
    }

    public void setPrintEndTime(String printEndTime) {
        this.printEndTime = printEndTime;
    }

    public Integer getOtQuitFlag() {
        return otQuitFlag;
    }

    public void setOtQuitFlag(Integer otQuitFlag) {
        this.otQuitFlag = otQuitFlag;
    }

    @Override
    public String toString() {
        return "WmsOutboundTaskFormVO{" +
                "otPreparationMaterialNo='" + otPreparationMaterialNo + '\'' +
                ", otVehicleSpecName='" + otVehicleSpecName + '\'' +
                ", otEstimateLane='" + otEstimateLane + '\'' +
                ", otStatus='" + otStatus + '\'' +
                ", startCreateTime='" + startCreateTime + '\'' +
                ", endCreateTime='" + endCreateTime + '\'' +
                ", startBlConfirmTime='" + startBlConfirmTime + '\'' +
                ", endBlConfirmTime='" + endBlConfirmTime + '\'' +
                ", otVin='" + otVin + '\'' +
                ", otId=" + otId +
                ", otTaskNo='" + otTaskNo + '\'' +
                ", gmtCreate=" + gmtCreate +
                ", otZoneName='" + otZoneName + '\'' +
                ", otLcoationName='" + otLcoationName + '\'' +
                ", otDriver='" + otDriver + '\'' +
                ", otDriverName='" + otDriverName + '\'' +
                ", otTaskStartTime=" + otTaskStartTime +
                ", otTaskEndTime=" + otTaskEndTime +
                ", otBlConfirmTime=" + otBlConfirmTime +
                ", hoHandoverNumber='" + hoHandoverNumber + '\'' +
                ", printStatus=" + printStatus +
                ", printStartTime='" + printStartTime + '\'' +
                ", printEndTime='" + printEndTime + '\'' +
                ", cancleReason='" + cancleReason + '\'' +
                ", otQuitFlag=" + otQuitFlag +
                ", otOutboundFlag=" + otOutboundFlag +
                '}';
    }
}
