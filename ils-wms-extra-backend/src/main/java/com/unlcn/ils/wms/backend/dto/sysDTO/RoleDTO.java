package com.unlcn.ils.wms.backend.dto.sysDTO;

/**
 * @Auther linbao
 * @Date 2017-10-16
 */
public class RoleDTO {

    private Integer id;

    private String code;

    private String name;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

}
