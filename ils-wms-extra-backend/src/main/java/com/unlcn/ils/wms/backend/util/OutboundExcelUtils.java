package com.unlcn.ils.wms.backend.util;

import com.unlcn.ils.wms.base.dto.OutboundExcelDTO;
import org.apache.commons.lang3.StringUtils;
import org.apache.poi.hssf.usermodel.*;
import org.apache.poi.hssf.util.HSSFColor;

import java.io.FileOutputStream;
import java.util.List;

public class OutboundExcelUtils {


    public static void export(List<OutboundExcelDTO> outboundExcelDTOList, String filePath) throws Exception {
        // 声明一个工作薄
        HSSFWorkbook wb = new HSSFWorkbook();
        // 生成一个表格
        HSSFSheet sheet = wb.createSheet("出库记录表");
        // 设置表格默认列宽度为15个字节
        sheet.setDefaultColumnWidth((short) 20);
        // 生成一个样式
        HSSFCellStyle style = wb.createCellStyle();
        // 设置这些样式
        style.setFillForegroundColor(HSSFColor.SKY_BLUE.index);
        style.setVerticalAlignment(HSSFCellStyle.VERTICAL_CENTER);//上下居中
        style.setFillPattern(HSSFCellStyle.SOLID_FOREGROUND);
        style.setBorderBottom(HSSFCellStyle.BORDER_THIN);
        style.setBorderLeft(HSSFCellStyle.BORDER_THIN);
        style.setBorderRight(HSSFCellStyle.BORDER_THIN);
        style.setBorderTop(HSSFCellStyle.BORDER_THIN);
        style.setAlignment(HSSFCellStyle.ALIGN_CENTER);
        // 生成一个字体
        HSSFFont font = wb.createFont();
        font.setColor(HSSFColor.VIOLET.index);
        font.setFontHeightInPoints((short) 12);
        font.setBoldweight(HSSFFont.BOLDWEIGHT_BOLD);
        // 把字体应用到当前的样式
        style.setFont(font);
        // 生成并设置另一个样式
        HSSFCellStyle style2 = wb.createCellStyle();
        style2.setFillForegroundColor(HSSFColor.WHITE.index);
        style2.setFillPattern(HSSFCellStyle.SOLID_FOREGROUND);
        style2.setBorderBottom(HSSFCellStyle.BORDER_THIN);
        style2.setBorderLeft(HSSFCellStyle.BORDER_THIN);
        style2.setBorderRight(HSSFCellStyle.BORDER_THIN);
        style2.setBorderTop(HSSFCellStyle.BORDER_THIN);
        style2.setAlignment(HSSFCellStyle.ALIGN_CENTER);
        style2.setVerticalAlignment(HSSFCellStyle.VERTICAL_CENTER);
        // 生成另一个字体
        HSSFFont font2 = wb.createFont();
        font2.setBoldweight(HSSFFont.BOLDWEIGHT_NORMAL);
        // 把字体应用到当前的样式
        style2.setFont(font2);

        // 第三步，在sheet中添加表头第0行,注意老版本poi对Excel的行数列数有限制short
        HSSFRow row = sheet.createRow(0);
        HSSFCell cell = row.createCell((short) 0);
        cell.setCellValue("编号");
        cell.setCellStyle(style);
        cell = row.createCell((short) 1);
        cell.setCellValue("日期");
        cell.setCellStyle(style);
        cell = row.createCell((short) 2);
        cell.setCellValue("出库时间");
        cell.setCellStyle(style);
        cell = row.createCell((short) 3);
        cell.setCellValue("VIN号");
        cell.setCellStyle(style);
        cell = row.createCell((short) 4);
        cell.setCellValue("DN号");
        cell.setCellStyle(style);
        cell = row.createCell((short) 5);
        cell.setCellValue("工厂提车库位");
        cell.setCellStyle(style);
        cell = row.createCell((short) 6);
        cell.setCellValue("出库仓库");
        cell.setCellStyle(style);
        cell = row.createCell((short) 7);
        cell.setCellValue("状态");
        cell.setCellStyle(style);

        for (int i = 0; i < outboundExcelDTOList.size(); i++) {
            if (i >= 2) {
                sheet.setColumnWidth(i + 1, 200 * 30);
            } else {
                sheet.setColumnWidth(i + 1, 200 * 20);
            }
            row = sheet.createRow(i + 1);
            OutboundExcelDTO outboundExcelDTO = outboundExcelDTOList.get(i);
            // 第四步，创建单元格，并设置值
            row.createCell((short) 0).setCellValue((outboundExcelDTO.getNo() == null) ? "" : String.valueOf(outboundExcelDTO.getNo()));
            row.createCell((short) 1).setCellValue(outboundExcelDTO.getDate());
            row.createCell((short) 2).setCellValue(outboundExcelDTO.getOutboundDate());
            row.createCell((short) 3).setCellValue(outboundExcelDTO.getVin());
            row.createCell((short) 4).setCellValue(outboundExcelDTO.getWaybillNo());
            row.createCell((short) 5).setCellValue(outboundExcelDTO.getFactoryWhno());
            cell = row.createCell((short) 6);
            cell.setCellValue(outboundExcelDTO.getWhName());
            row.createCell((short) 7).setCellValue(StringUtils.isBlank(outboundExcelDTO.getOrderFlag()) ? "" : outboundExcelDTO.getOrderFlag());
        }

        // 第六步，将文件存到指定位置
        try {
            FileOutputStream fout = new FileOutputStream(filePath);
            wb.write(fout);
            fout.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

//    public static void main(String[] args) throws Exception {
//        // 第一步，创建一个webbook，对应一个Excel文件
//        HSSFWorkbook wb = new HSSFWorkbook();
//        // 第二步，在webbook中添加一个sheet,对应Excel文件中的sheet
//        HSSFSheet sheet = wb.createSheet("学生表一");
//        // 第三步，在sheet中添加表头第0行,注意老版本poi对Excel的行数列数有限制short
//        HSSFRow row = sheet.createRow((int) 0);
//        // 第四步，创建单元格，并设置值表头 设置表头居中
//        HSSFCellStyle style = wb.createCellStyle();
//        style.setAlignment(HSSFCellStyle.ALIGN_CENTER); // 创建一个居中格式
//
//        HSSFCell cell = row.createCell((short) 0);
//        cell.setCellValue("学号");
//        cell.setCellStyle(style);
//        cell = row.createCell((short) 1);
//        cell.setCellValue("姓名");
//        cell.setCellStyle(style);
//        cell = row.createCell((short) 2);
//        cell.setCellValue("年龄");
//        cell.setCellStyle(style);
//        cell = row.createCell((short) 3);
//        cell.setCellValue("生日");
//        cell.setCellStyle(style);
//
//        // 第五步，写入实体数据 实际应用中这些数据从数据库得到，
//        List list = InboundExcelUtils.getStudent();
//
//        for (int i = 0; i < list.size(); i++) {
//            row = sheet.createRow((int) i + 1);
//            InboundExcelUtils stu = (InboundExcelUtils) list.get(i);
//            // 第四步，创建单元格，并设置值
//            row.createCell((short) 0).setCellValue((double) stu.getId());
//            row.createCell((short) 1).setCellValue(stu.getName());
//            row.createCell((short) 2).setCellValue((double) stu.getAge());
//            cell = row.createCell((short) 3);
//            cell.setCellValue(new SimpleDateFormat("yyyy-mm-dd").format(stu
//                    .getBirth()));
//        }
//
//        // 第六步，将文件存到指定位置
//        try {
//            FileOutputStream fout = new FileOutputStream("d:/students.xls");
//            wb.write(fout);
//            fout.close();
//        }
//        catch (Exception e) {
//            e.printStackTrace();
//        }
//    }
}


