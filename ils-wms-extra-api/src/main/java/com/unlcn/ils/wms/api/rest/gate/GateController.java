package com.unlcn.ils.wms.api.rest.gate;

import cn.huiyunche.commons.exception.BusinessException;
import com.alibaba.fastjson.JSONObject;
import com.google.common.collect.Maps;
import com.mangofactory.swagger.annotations.ApiIgnore;
import com.unlcn.ils.wms.api.vo.GateVO;
import com.unlcn.ils.wms.backend.bo.gateBO.GateBO;
import com.unlcn.ils.wms.backend.service.gate.GateControllerService;
import io.swagger.annotations.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.HashMap;
import java.util.Map;

/**
 * @author laishijian
 * @ClassName: GateController
 * @Description: 闸门控制
 * @date 2017年11月9日 上午9:41:50
 */
@Controller
@RequestMapping("/gateController")
@Api(value = "/gateController", description = "闸门控制")
public class GateController {

    private Logger logger = LoggerFactory.getLogger(GateController.class);

    @Autowired
    private GateControllerService gateControllerService;

    /**
     * 进场闸门控制接口
     *
     * @param gateVO 参数封装
     * @return String    返回类型
     */
    @ApiIgnore
    @ApiOperation(value = "入库根据车牌号开门", notes = "入库根据车牌号开门")
    @ApiResponses(value = {@ApiResponse(code = 400, message = "Invalid id supplied"),
            @ApiResponse(code = 404, message = "inboundOpenToPlateNum not found")})
    @RequestMapping(value = "/inboundOpenToPlateNum", method = RequestMethod.POST, produces = {
            "text/html;charset=UTF-8;",
            "application/json;charset=UTF-8",
            "application/x-www-form-urlencoded;charset=UTF-8"})
    @ResponseBody
    public String inboundOpenToPlateNum(@ApiParam(required = false, value = "查询参数") GateVO gateVO) {
        logger.info("GateController.inboundOpenToPlateNum param: {}", gateVO);
        GateBO gateBO = new GateBO();
        BeanUtils.copyProperties(gateVO, gateBO);
        Map<String, Object> result = Maps.newHashMap();
        try {
            result = gateControllerService.addInboundOpenToPlateNum(gateBO);
        } catch (BusinessException be) {
            logger.error("GateController.inboundOpenToPlateNum error: {}", be);
            result.put("result", "failed");
            result.put("msg", be.getMessage());
        } catch (Exception e) {
            result.put("result", "failed");
            result.put("msg", "系统异常");
        }
        //返回数据以json字符串方式返回
        JSONObject json = new JSONObject(result);
        return json.toJSONString();
    }

    /**
     * 进场卸车
     *
     * @param orderNo
     * @return
     */
    public String inboundOpenToTmsOrderNo(String orderNo) {
        logger.info("GateController.inboundOpenToPlateNum param: {}", orderNo);
        Map<String, Object> result = new HashMap<>();

        try {
            //String msg = gateControllerService.queryInboundOpenToPlateNum(gateBO);
            //result.put("result","success");
            //result.put("msg",msg);
        } catch (BusinessException be) {
            result.put("result", "failed");
            result.put("msg", be.getMessage());
        } catch (Exception e) {
            result.put("result", "failed");
            result.put("msg", "系统异常");
        }
        //返回数据以json字符串方式返回
        JSONObject json = new JSONObject(result);
        return json.toJSONString();
    }

    /**
     * @param @return
     * @return String    返回类型
     * @throws
     * @Title: outboundOpenToPlateNum
     * @Description: 出库库根据车牌号开门
     */
    @ApiIgnore
    @ApiOperation(value = "出库库根据车牌号开门", notes = "出库库根据车牌号开门")
    @ApiResponses(value = {@ApiResponse(code = 400, message = "Invalid id supplied"),
            @ApiResponse(code = 404, message = "outboundOpenToPlateNum not found")})
    @RequestMapping(value = "/outboundOpenToPlateNum", method = RequestMethod.POST, produces = {
            "text/html;charset=UTF-8;",
            "application/json;charset=UTF-8",
            "application/x-www-form-urlencoded;charset=UTF-8"})
    @ResponseBody
    public String outboundOpenToPlateNum(@ApiParam(required = false, value = "查询参数") GateVO gateVO) {
        logger.info("GateController.outboundOpenToPlateNum param: {}", gateVO);
        Map<String, Object> result = new HashMap<>();
        GateBO gateBO = new GateBO();
        BeanUtils.copyProperties(gateVO, gateBO);
        try {
            result = gateControllerService.updateOutboundOpenToPlateNum(gateBO);
        } catch (BusinessException be) {
            logger.error("GateController.outboundOpenToPlateNum error: {}", be);
            result.put("result", "failed");
            result.put("msg", be.getMessage());
        } catch (Exception e) {
            logger.error("GateController.outboundOpenToPlateNum error: {}", "系统异常");
            result.put("result", "failed");
            result.put("msg", "系统异常");
        }
        //返回数据以json字符串方式返回
        JSONObject json = new JSONObject(result);
        return json.toJSONString();
    }
}
