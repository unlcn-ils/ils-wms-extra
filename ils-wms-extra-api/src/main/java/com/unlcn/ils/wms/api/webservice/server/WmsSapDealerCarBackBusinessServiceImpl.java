package com.unlcn.ils.wms.api.webservice.server;

import cn.huiyunche.commons.exception.BusinessException;
import com.google.common.collect.Maps;
import com.google.gson.Gson;
import com.unlcn.ils.wms.api.webservice.dto.ResultDealerCarbackSapDTO;
import com.unlcn.ils.wms.backend.service.webservice.bo.WmsResultDealerCarbackSapBO;
import com.unlcn.ils.wms.backend.service.webservice.bo.WmsresultSapHeaderBO;
import com.unlcn.ils.wms.backend.service.webservice.client.WmsJmSapService;
import com.unlcn.ils.wms.backend.service.webservice.dto.ResultSapHeaderDTO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.context.support.SpringBeanAutowiringSupport;

import java.util.Map;
import java.util.Objects;

/**
 * 经销商退车接口sap回调
 */
public class WmsSapDealerCarBackBusinessServiceImpl {

    private Logger logger = LoggerFactory.getLogger(WmsSapDealerCarBackBusinessServiceImpl.class);

    @Autowired
    private WmsJmSapService wmsJmSapService;

    /**
     * 退车申请接口回调
     * <p>
     */
    public String getDealerCarbackSapMessage(ResultDealerCarbackSapDTO[] resultDealerCarbackSapDTO,
                                             ResultSapHeaderDTO[] resultSapHeaderDTOS) {
        logger.info("WmsSapBusinessServiceImpl.getDealerCarbackSapMessage param{}", resultDealerCarbackSapDTO, resultSapHeaderDTOS);
        //加入spring的上下文
        SpringBeanAutowiringSupport.processInjectionBasedOnCurrentContext(this);
        Map<String, Object> resultInfo = Maps.newHashMap();
        if (Objects.equals(resultDealerCarbackSapDTO, null)) {
            resultInfo.put("returnCode", "E");
            resultInfo.put("returnMsg", "返回结果为空!");
            resultInfo.put("dataId", null);
            return new Gson().toJson(resultInfo);
        }
        String s_body = new Gson().toJson(resultDealerCarbackSapDTO);
        String s_head = new Gson().toJson(resultSapHeaderDTOS);

        for (ResultDealerCarbackSapDTO sapDTO : resultDealerCarbackSapDTO) {
            try {
                WmsResultDealerCarbackSapBO wmsResultDealerCarbackSapBO = new WmsResultDealerCarbackSapBO();
                WmsresultSapHeaderBO wmsresultSapHeaderBO = new WmsresultSapHeaderBO();
                BeanUtils.copyProperties(sapDTO, wmsResultDealerCarbackSapBO);
                BeanUtils.copyProperties(resultSapHeaderDTOS[0], wmsresultSapHeaderBO);//头信息
                resultInfo = wmsJmSapService.updateDealercarbackResult(wmsResultDealerCarbackSapBO, s_body, s_head);
            } catch (BusinessException e) {
                logger.error("WmsSapBusinessServiceImpl.getDealerCarbackSapMessage param{}", e);
                resultInfo.put("returnCode", "E");
                resultInfo.put("returnMsg", e.getMessage());
                resultInfo.put("dataId", sapDTO.getDataId());
                return new Gson().toJson(resultInfo);
            } catch (Exception e) {
                logger.error("WmsSapBusinessServiceImpl.getDealerCarbackSapMessage param{}", e);
                resultInfo.put("returnCode", "E");
                resultInfo.put("returnMsg", "系统内部异常!");
                resultInfo.put("dataId", sapDTO.getDataId());
                return new Gson().toJson(resultInfo);
            }
        }
        return new Gson().toJson(resultInfo);
    }
}
