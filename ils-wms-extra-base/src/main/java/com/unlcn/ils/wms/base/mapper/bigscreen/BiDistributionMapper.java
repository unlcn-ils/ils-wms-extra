package com.unlcn.ils.wms.base.mapper.bigscreen;

import com.unlcn.ils.wms.base.model.bigscreen.BiDistribution;
import com.unlcn.ils.wms.base.model.bigscreen.BiDistributionExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface BiDistributionMapper {
    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table bi_distribution
     *
     * @mbggenerated Wed Nov 08 19:47:33 CST 2017
     */
    int countByExample(BiDistributionExample example);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table bi_distribution
     *
     * @mbggenerated Wed Nov 08 19:47:33 CST 2017
     */
    int deleteByExample(BiDistributionExample example);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table bi_distribution
     *
     * @mbggenerated Wed Nov 08 19:47:33 CST 2017
     */
    int deleteByPrimaryKey(Long id);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table bi_distribution
     *
     * @mbggenerated Wed Nov 08 19:47:33 CST 2017
     */
    int insert(BiDistribution record);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table bi_distribution
     *
     * @mbggenerated Wed Nov 08 19:47:33 CST 2017
     */
    int insertSelective(BiDistribution record);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table bi_distribution
     *
     * @mbggenerated Wed Nov 08 19:47:33 CST 2017
     */
    List<BiDistribution> selectByExample(BiDistributionExample example);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table bi_distribution
     *
     * @mbggenerated Wed Nov 08 19:47:33 CST 2017
     */
    BiDistribution selectByPrimaryKey(Long id);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table bi_distribution
     *
     * @mbggenerated Wed Nov 08 19:47:33 CST 2017
     */
    int updateByExampleSelective(@Param("record") BiDistribution record, @Param("example") BiDistributionExample example);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table bi_distribution
     *
     * @mbggenerated Wed Nov 08 19:47:33 CST 2017
     */
    int updateByExample(@Param("record") BiDistribution record, @Param("example") BiDistributionExample example);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table bi_distribution
     *
     * @mbggenerated Wed Nov 08 19:47:33 CST 2017
     */
    int updateByPrimaryKeySelective(BiDistribution record);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table bi_distribution
     *
     * @mbggenerated Wed Nov 08 19:47:33 CST 2017
     */
    int updateByPrimaryKey(BiDistribution record);
}